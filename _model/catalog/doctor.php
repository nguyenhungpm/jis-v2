<?php
class ModelCatalogDoctor extends Model {

	public function getDoctor($doctor_id) {
		$query = $this->db->query("SELECT  * FROM " . DB_PREFIX . "doctor d LEFT JOIN " . DB_PREFIX . "doctor_description dd WHERE d.doctor_id = '" . (int)$doctor_id . "' AND dd.language_id = '". $this->config->get('config_language_id') ."'");

		return $query->row;
	}
	
	public function getDoctors($data) {
		
		$sql = "SELECT * FROM " . DB_PREFIX . "doctor d LEFT JOIN " . DB_PREFIX . "doctor_description dd ON (d.doctor_id=dd.doctor_id) WHERE d.status=1";
		
		$sort_data = array(
            'd.sort_order',
            'dd.name'
        );
		
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY d.sort_order";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC, LCASE(dd.name) DESC";
        } else {
            $sql .= " ASC, LCASE(dd.name) ASC";
        }
		
		if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }
			
		$query = $this->db->query($sql);
		
		return $query->rows;
	}
	
	public function getTotalDoctors($data) {
		
		// $this->check_db();
		
		$sql = "SELECT * FROM " . DB_PREFIX . "doctor";
			
		$query = $this->db->query($sql);
		
		return $query->num_rows;
	}
}
?>