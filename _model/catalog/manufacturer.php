<?php
class ModelCatalogManufacturer extends Model
{
    public function getManufacturer($manufacturer_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "manufacturer m  WHERE m.manufacturer_id = '" . (int) $manufacturer_id . "'");

        return $query->row;
    }

    public function getManufacturerDescription($manufacturer_id)
    {
        $query = $this->db->query("SHOW TABLES LIKE '" . DB_PREFIX . "manufacturer_description'");

        if (!$query->num_rows) {
            return false;
        }

        $manufacturer_description_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "manufacturer_description WHERE manufacturer_id = '" . (int) $manufacturer_id . "' AND language_id = '" . (int) $this->config->get('config_language_id') . "'");

        if ($query->num_rows) {
            return array(
                'title' => $query->row['title'],
                'position' => $query->row['position'],
                'description' => $query->row['description'],
                'meta_keyword' => $query->row['meta_keyword'],
                'meta_description' => $query->row['meta_description'],
                'meta_title' => $query->row['meta_title'],
            );
        }

        return false;
    }

	public function getManuCats() {
        $sql = "SELECT DISTINCT *, rc.name as industry FROM " . DB_PREFIX . "manucat r INNER JOIN " . DB_PREFIX . "manucat_description rc ON(rc.manucat_id=r.manucat_id) WHERE rc.language_id = '" . (int) $this->config->get('config_language_id') . "'";

        $sql .=" ORDER BY r.sort_order ASC, LCASE(rc.name) DESC ";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getManufacturers($data = array())
    {
        // if ($data) {
		$sql = "SELECT * FROM " . DB_PREFIX . "manufacturer m LEFT JOIN " . DB_PREFIX . "manufacturer_description md ON (m.manufacturer_id = md.manufacturer_id) WHERE md.language_id = '" . (int) $this->config->get('config_language_id') . "'";		
        // $sql = "SELECT *, (SELECT description FROM " . DB_PREFIX . "manufacturer_description md WHERE md.language_id = '" . (int) $this->config->get('config_language_id') . "' AND md.manufacturer_id = m.manufacturer_id LIMIT 1) as description, (SELECT position FROM " . DB_PREFIX . "manufacturer_description md WHERE md.language_id = '" . (int) $this->config->get('config_language_id') . "' AND md.manufacturer_id = m.manufacturer_id LIMIT 1) as position FROM " . DB_PREFIX . "manufacturer m  WHERE 1";

        $sort_data = array(
            'm.sort_order',
            'md.title',
        );

		if (isset($data['filter_manucat_id'])) {
            $sql .= " AND m.manucat_id = " . $data['filter_manucat_id'];
        }

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY m.sort_order";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }
// echo $sql;
        $query = $this->db->query($sql);

        return $query->rows;
        // } else {
        //     $manufacturer_data = $this->cache->get('manufacturer.' . (int)$this->config->get('config_store_id'));
        //
        //     if (!$manufacturer_data) {
        //         $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "manufacturer m  WHERE 1 ORDER BY name");
        //
        //         $manufacturer_data = $query->rows;
        //
        //         $this->cache->set('manufacturer.' . (int)$this->config->get('config_store_id'), $manufacturer_data);
        //     }
        //
        //     return $manufacturer_data;
        // }
    }
}
