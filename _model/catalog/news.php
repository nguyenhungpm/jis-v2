<?php

class ModelCatalogNews extends Model {

    public function setRate($news_id, $data, $point) {
        $this->db->query("UPDATE " . DB_PREFIX . "news SET total_rate = (total_rate + 1), total_point = (total_point + '" . $point . "' ) WHERE news_id = '" . (int) $news_id . "'");
    }

    public function getRate($news_id) {
        $query = $this->db->query("SELECT total_rate, total_point FROM " . DB_PREFIX . "news WHERE news_id = '" . (int) $news_id . "'");
        return $query->row;
    }

    public function getAllquestions($news_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "comments where news_id ='".$news_id."' and status='1' order by asked_date desc");
        return $query->rows;
    }
    public function getTotalQuestions($news_id) {
        $query = $this->db->query("SELECT count(*) as total FROM " . DB_PREFIX . "comments where news_id ='".$news_id."' and status='1'");
        return $query->row['total'];
    }
    public function updateViewed($news_id) {
        $this->db->query("UPDATE " . DB_PREFIX . "news SET viewed = (viewed + 1) WHERE news_id = '" . (int) $news_id . "'");
    }

    public function getNew($news_id) {
        $query = $this->db->query("SELECT DISTINCT *, nd.name AS name, n.image, (SELECT COUNT(*) AS total FROM " . DB_PREFIX . "comments c2 WHERE c2.news_id = n.news_id AND c2.status = '1' GROUP BY c2.news_id) AS total_comments FROM " . DB_PREFIX . "news n INNER JOIN " . DB_PREFIX . "news_description nd ON (n.news_id = nd.news_id) WHERE n.news_id = '" . (int) $news_id . "' AND nd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND nd.name <>'' AND n.status = '1' AND n.date_available <= NOW() ");

        if ($query->num_rows) {
            return array(
                'news_id' => $query->row['news_id'],
                'name' => $query->row['name'],
                'meta_title' => $query->row['meta_title'],
                'short_description' => $query->row['short_description'],
                'description' => $query->row['description'],
                'meta_description' => $query->row['meta_description'],
                'meta_keyword' => $query->row['meta_keyword'],
                'author' => $query->row['author'],
                'photographer' => $query->row['photographer'],
                'designer' => $query->row['designer'],
                'director' => $query->row['director'],
                'image' => $query->row['image'],
                'banner' => $query->row['banner'],
                'main_cat_id' => $query->row['main_cat_id'],
                'date_available' => $query->row['date_available'],
                'sort_order' => $query->row['sort_order'],
                'comment' => $query->row['comment'],
                'approved' => $query->row['approved'],
                'total_comments' => $query->row['total_comments'],
                'status' => $query->row['status'],
                'date_added' => $query->row['date_added'],
                'date_modified' => $query->row['date_modified'],
                'viewed' => $query->row['viewed']
            );
        } else {
            return false;
        }
        print_r($query);
    }

    public function getNews($data = array()) {
        if (!empty($data['filter_name_news']) && $this->config->get('config_fts')==1) {
            $search = ", MATCH(nd.name) AGAINST ('" . $this->db->escape(utf8_strtolower($data['filter_name_news'])) . "' IN NATURAL LANGUAGE MODE) AS relevance, MATCH(nd.description) AGAINST ('" . $this->db->escape(utf8_strtolower($data['filter_name_news'])) . "' IN NATURAL LANGUAGE MODE) AS relevance_ ";
            $sort_search = " relevance DESC, ";
        } else {
            $search = '';
            $sort_search = '';
        }

        $sql = "SELECT n.news_id as news_id " . $search . " FROM " . DB_PREFIX . "news n INNER JOIN " . DB_PREFIX . "news_description nd ON (n.news_id = nd.news_id) WHERE nd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND n.status = '1' AND n.date_available <= NOW() ";

        if (isset($data['filter_name_news']) && $data['filter_name_news']) {
            if($this->config->get('config_fts')==1){
                $sql .= " AND (MATCH(nd.name) AGAINST ('" . $this->db->escape(utf8_strtolower($data['filter_name_news'])) . "' IN NATURAL LANGUAGE MODE)+MATCH(nd.description) AGAINST ('" . $this->db->escape(utf8_strtolower($data['filter_name_news'])) . "' IN NATURAL LANGUAGE MODE)) <> 0 ";
            }else{
                $sql .= " AND (";
                $implode = array();
                $data_sort = array();

                $words = explode(' ', trim(preg_replace('/\s\s+/', ' ', $data['filter_name_news'])));

                foreach ($words as $word) {
                   $implode[] = "nd.name LIKE '%" . $this->db->escape($word) . "%'";
                   $data_sort[] = "(nd.name LIKE '%" . $this->db->escape($word) . "%')";
                }

                if ($implode) {
                   $sql .= " " . implode(" AND ", $implode) . "";
                }
                $sort_search = "(";
                if ($data_sort) {
                   $sort_search .= " " . implode(" + ", $data_sort) . "";
                }
                $sort_search .= ") DESC, ";
                if (!empty($data['filter_description'])) {
                   $sql .= " OR nd.description LIKE '%" . $this->db->escape($data['filter_name_news']) . "%'";
                }
                $sql .= ")";
            }
        }
        if (isset($data['filter_tag']) && $data['filter_tag']) {
            $sql .= " AND n.news_id IN (SELECT nt.news_id FROM " . DB_PREFIX . "news_tag nt WHERE nt.language_id = '" . (int) $this->config->get('config_language_id') . "' AND nt.tag LIKE '%" . $this->db->escape($data['filter_tag']) . "%')";
        }

        if (isset($data['filter_news_category_id']) && $data['filter_news_category_id']) {
            if (isset($data['filter_sub_news_category']) && $data['filter_sub_news_category']) {
                $implode_data = array();

                $this->load->model('catalog/news_category');

                $categories = $this->model_catalog_news_category->getNewsCategoriesByParentId($data['filter_news_category_id']);

                foreach ($categories as $news_category_id) {
                    $implode_data[] = "n2c.news_category_id = '" . (int) $news_category_id . "'";
                }

                $sql .= " AND n.news_id IN (SELECT n2c.news_id FROM " . DB_PREFIX . "news_to_category n2c WHERE " . implode(' OR ', $implode_data) . ")";
            } else {
                $sql .= " AND n.news_id IN (SELECT n2c.news_id FROM " . DB_PREFIX . "news_to_category n2c WHERE n2c.news_category_id = '" . (int) $data['filter_news_category_id'] . "')";
            }
        }

        //$sql .= " ORDER BY n.sort_order ASC, n.date_added DESC";

        $sort_data = array(
            'nd.name',
            'n.viewed',
            'n.sort_order',
            'n.date_added',
            'n.date_available'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            if ($data['sort'] == 'nd.name') {
                $sql .= " ORDER BY " . $sort_search . " LCASE(" . $data['sort'] . ")";
            } else {
                $sql .= " ORDER BY " . $sort_search . $data['sort'];
            }
        } else {
            $sql .= " ORDER BY " . $sort_search . "n.sort_order";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (!isset($data['sitemap']) && (isset($data['start']) || isset($data['limit']))) {
            if (isset($data['start']) < 0) {
                $data['start'] = 0;
            }
            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $news_data = array();
        $query = $this->db->query($sql);

        foreach ($query->rows as $result) {
            $news_data[$result['news_id']] = $this->getNew($result['news_id']);
        }

        return $news_data;
    }

    public function getLatestNews($limit) {
        $news_data = $this->cache->get('news.latest.' . $this->config->get('config_language_id') . '.' . (int) $this->config->get('config_store_id') . '.' . $limit);

        if (!$news_data) {
            $query = $this->db->query("SELECT n.news_id FROM " . DB_PREFIX . "news n WHERE n.status = '1' AND n.date_available <= NOW() ORDER BY n.date_added DESC LIMIT " . (int) $limit);

            foreach ($query->rows as $result) {
                $news_data[$result['news_id']] = $this->getNew($result['news_id']);
            }

            $this->cache->set('news.latest.' . $this->config->get('config_language_id') . '.' . (int) $this->config->get('config_store_id') . '.' . $limit, $news_data);
        }

        return $news_data;
    }

    public function getPopularNews($limit) {
        $news_data = array();

        $query = $this->db->query("SELECT n.news_id FROM " . DB_PREFIX . "news n  WHERE n.status = '1' AND n.date_available <= NOW() ORDER BY n.viewed DESC, n.date_added DESC LIMIT " . (int) $limit);

        foreach ($query->rows as $result) {
            $news_data[$result['news_id']] = $this->getNew($result['news_id']);
        }

        return $news_data;
    }

    public function getNewsRelated($news_id) {
        $news_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_related nr INNER JOIN " . DB_PREFIX . "news n ON (nr.related_id = n.news_id)  WHERE nr.news_id = '" . (int) $news_id . "' AND n.status = '1' AND n.date_available <= NOW() ORDER BY n.sort_order ASC, n.date_added DESC");

        foreach ($query->rows as $result) {
            $news_data[$result['related_id']] = $this->getNew($result['related_id']);
        }

        return $news_data;
    }

    public function getOtherNews($news_category_id, $news_id, $limit) {
        $news_data = array();
        if (empty($news_category_id)) {
            $sql = "SELECT n.news_id FROM " . DB_PREFIX . "news n INNER JOIN " . DB_PREFIX . "news_description nd ON (n.news_id=nd.news_id) WHERE n.status = '1' AND n.date_available <= NOW() AND n.news_id != " . $news_id . " AND n.news_id < " . $news_id . " AND nd.language_id = '" . (int) $this->config->get('config_language_id') . "' ORDER BY n.date_available DESC, n.sort_order ASC LIMIT " . (int) $limit;
        } else {
            $sql = "SELECT n.news_id FROM " . DB_PREFIX . "news n INNER JOIN " . DB_PREFIX . "news_description nd ON (n.news_id=nd.news_id) WHERE n.status = '1' AND n.date_available <= NOW() AND n.news_id IN (SELECT news_id FROM " . DB_PREFIX . "news_to_category WHERE news_category_id = '". $news_category_id ."') AND n.news_id != " . $news_id . " AND n.news_id < " . $news_id . " AND nd.language_id = '" . (int) $this->config->get('config_language_id') . "' ORDER BY n.date_available DESC, n.sort_order ASC LIMIT " . (int) $limit;
            // $sql = "SELECT n.news_id FROM " . DB_PREFIX . "news n  INNER JOIN " . DB_PREFIX . "news_to_category n2c ON (n.news_id = n2c.news_id) WHERE n.status = '1' AND n.date_available <= NOW() AND n2c.news_category_id = '" . (int) $news_category_id . "' AND n.news_id != " . $news_id . " AND n.news_id < " . $news_id . " ORDER BY n.sort_order ASC, n.date_added DESC  LIMIT " . (int) $limit;
        }
        // echo $sql;
        $query = $this->db->query($sql);

        foreach ($query->rows as $result) {
            $news_data[$result['news_id']] = $this->getNew($result['news_id']);
        }

        return $news_data;
    }

    public function getNewsTags($news_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_tag WHERE news_id = '" . (int) $news_id . "' AND language_id = '" . (int) $this->config->get('config_language_id') . "'");

        return $query->rows;
    }

    public function getNewsCategories($news_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_to_category WHERE news_id = '" . (int) $news_id . "'");

        return $query->rows;
    }

    public function getTotalNews($data = array()) {
        if (!empty($data['filter_name_news'])) {
            $search = ", MATCH(nd.name) AGAINST ('" . $this->db->escape($data['filter_name_news']) . "' IN NATURAL LANGUAGE MODE) AS relevance ";
            $sort_search = " relevance DESC, ";
        } else {
            $search = '';
            $sort_search = '';
        }
        $sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "news n INNER JOIN " . DB_PREFIX . "news_description nd ON (n.news_id = nd.news_id)  WHERE nd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND n.status = '1' AND n.date_available <= NOW()";

        if (isset($data['filter_name_news']) && $data['filter_name_news']) {
            if($this->config->get('config_fts')==1){
                $sql .= " AND (MATCH(nd.name) AGAINST ('" . $this->db->escape(utf8_strtolower($data['filter_name_news'])) . "' IN NATURAL LANGUAGE MODE)+MATCH(nd.description) AGAINST ('" . $this->db->escape(utf8_strtolower($data['filter_name_news'])) . "' IN NATURAL LANGUAGE MODE)) <> 0 ";
            }else{
                $sql .= " AND (";
                $implode = array();
                $data_sort = array();

                $words = explode(' ', trim(preg_replace('/\s\s+/', ' ', $data['filter_name_news'])));

                foreach ($words as $word) {
                   $implode[] = "nd.name LIKE '%" . $this->db->escape($word) . "%'";
                   $data_sort[] = "(nd.name LIKE '%" . $this->db->escape($word) . "%')";
                }

                if ($implode) {
                   $sql .= " " . implode(" AND ", $implode) . "";
                }
                $sort_search = "(";
                if ($data_sort) {
                   $sort_search .= " " . implode(" + ", $data_sort) . "";
                }
                $sort_search .= ") DESC, ";
                if (!empty($data['filter_description'])) {
                   $sql .= " OR nd.description LIKE '%" . $this->db->escape($data['filter_name_news']) . "%'";
                }
                $sql .= ")";
            }
        }

        if (isset($data['filter_tag']) && $data['filter_tag']) {
            $sql .= " AND n.news_id IN (SELECT nt.news_id FROM " . DB_PREFIX . "news_tag nt WHERE nt.language_id = '" . (int) $this->config->get('config_language_id') . "' AND nt.tag LIKE '%" . $this->db->escape($data['filter_tag']) . "%')";
        }

        if (isset($data['filter_news_category_id']) && $data['filter_news_category_id']) {
            if (isset($data['filter_sub_news_category']) && $data['filter_sub_news_category']) {
                $implode_data = array();

                $this->load->model('catalog/news_category');

                $categories = $this->model_catalog_news_category->getNewsCategoriesByParentId($data['filter_news_category_id']);

                foreach ($categories as $news_category_id) {
                    $implode_data[] = "n2c.news_category_id = '" . (int) $news_category_id . "'";
                }

                $sql .= " AND n.news_id IN (SELECT n2c.news_id FROM " . DB_PREFIX . "news_to_category n2c WHERE " . implode(' OR ', $implode_data) . ")";
            } else {
                $sql .= " AND n.news_id IN (SELECT n2c.news_id FROM " . DB_PREFIX . "news_to_category n2c WHERE n2c.news_category_id = '" . (int) $data['filter_news_category_id'] . "')";
            }
        }
        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getTotalNewsByCategory($news_category_id) {
        $sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "news n INNER JOIN " . DB_PREFIX . "news_description nd ON (n.news_id = nd.news_id)   INNER JOIN " . DB_PREFIX . "news_to_category nc ON (n.news_id = nc.news_id) WHERE nd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND nc.news_category_id = '" . (int) $news_category_id . "' AND n.status = '1' AND n.date_available <= NOW()";

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function addQuestionAnswer($NewsId,$questionerName,$questionerEmail,$questionerAnswer,$questionStatus){
        $query = $this->db->query("INSERT INTO " . DB_PREFIX . "comments SET news_id = '" . (int)$NewsId . "',user_name = '" . $this->db->escape($questionerName) . "', user_email = '" . $this->db->escape($questionerEmail) . "', question = '" . $this->db->escape($questionerAnswer) . "', status = '" . (int)$questionStatus . "', asked_date = NOW()");

        if($this->config->get('config_alert_mail')=='1'){
                $mail = new Mail();
                $mail->protocol = $this->config->get('config_mail_protocol');
                $mail->parameter = $this->config->get('config_mail_parameter');
                $mail->hostname = $this->config->get('config_smtp_host');
                $mail->username = $this->config->get('config_smtp_username');
                $mail->password = $this->config->get('config_smtp_password');
                $mail->port = $this->config->get('config_smtp_port');
                $mail->timeout = $this->config->get('config_smtp_timeout');
                $mail->setTo($this->config->get('config_email'));
                if(!empty($this->config->get('config_mail_from'))){
                    $mail->setFrom($this->config->get('config_mail_from'));
                }else{
                    $mail->setFrom($this->config->get('config_email'));
                }
                $mail->setSender('Notifier '.$this->config->get('config_name')[$this->config->get('config_language_id')]);
                $mail->setSubject($questionerName." gửi 1 câu hỏi mới trên website");
                $mail->setHTML(strip_tags(html_entity_decode("Người có tên là: " . $questionerName . "<br />Email: ". $questionerEmail . "<br /><br />Nội dung: " . $questionerAnswer, ENT_QUOTES, 'UTF-8')));
                $mail->send();
            }
    }

    public function getNewsRecruitments(){
      $sql = "SELECT * FROM " . DB_PREFIX . "news n LEFT JOIN " . DB_PREFIX . "news_description nd ON (n.news_id = nd.news_id) WHERE nd.language_id = '". (int) $this->config->get('config_language_id') ."' AND n.recruitment = '1' AND n.status = '1' AND n.deadline >= NOW()";
      $query = $this->db->query($sql);
      // echo $sql;
      return $query->rows;
    }

    public function getTopNews($news_category_id){
      $sql = "SELECT news_id FROM " . DB_PREFIX . "news_to_category WHERE news_category_id = '". $news_category_id ."' AND top = '1'";
      $query = $this->db->query($sql);
      return $query->row['news_id'];
    }

}

?>
