<?php

class ModelCatalogRecruitment extends Model {

    public function getRecruitment($recruitment_id) {
        $query = $this->db->query("SELECT DISTINCT *, rc.name as industry FROM " . DB_PREFIX . "recruitment r INNER JOIN " . DB_PREFIX . "recruitment_description rd ON (r.recruitment_id = rd.recruitment_id) LEFT JOIN " . DB_PREFIX . "reccat_description rc ON(rc.reccat_id=r.reccat_id) WHERE r.recruitment_id = '" . (int) $recruitment_id . "' AND rd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND rd.title <>'' AND r.status = '1'");

        return $query->row;
    }

    public function getReccats() {
        $sql = "SELECT reccat_id, name FROM " . DB_PREFIX . "reccat_description WHERE language_id = '" . (int) $this->config->get('config_language_id') . "' AND reccat_id IN (SELECT DISTINCT reccat_id FROM " . DB_PREFIX . "recruitment)";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getLocations() {
        $sql = "SELECT DISTINCT location FROM " . DB_PREFIX . "recruitment_description WHERE language_id = '" . (int) $this->config->get('config_language_id') . "'";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getRecruitments($data) {

		$sql = "SELECT DISTINCT *, rc.name as industry, (SELECT name FROM " . DB_PREFIX . "reccat_description WHERE language_id = '" . (int) $this->config->get('config_language_id') . "' AND reccat_id = r.reccat_id ) as reccat_name FROM " . DB_PREFIX . "recruitment r INNER JOIN " . DB_PREFIX . "recruitment_description rd ON (r.recruitment_id = rd.recruitment_id) INNER JOIN " . DB_PREFIX . "reccat_description rc ON(rc.reccat_id=r.reccat_id) WHERE rd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND rc.language_id = '" . (int) $this->config->get('config_language_id') . "' AND rd.title <>'' AND r.status = '1' ORDER BY r.sort_order DESC, LCASE(rd.title) DESC";

		    if (isset($data['start']) || isset($data['limit'])) {
            if (isset($data['start']) < 0) {
                $data['start'] = 0;
            }
            if ($data['limit'] < 1) {
                $data['limit'] = 10;
            }
            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalRecruitments($data) {

		$sql = "SELECT COUNT(*) as total FROM " . DB_PREFIX . "recruitment i INNER JOIN " . DB_PREFIX . "recruitment_description id ON (i.recruitment_id = id.recruitment_id) WHERE id.language_id = '" . (int) $this->config->get('config_language_id') . "' AND i.status = '1' ORDER BY i.sort_order, LCASE(id.title) ASC";

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getRecruitmentSearch($data) {
        if (!empty($data['filter_name_news']) && $this->config->get('config_fts')) {
            $search = ", MATCH(id.title) AGAINST ('" . $this->db->escape(($data['filter_name_news'])) . "' IN NATURAL LANGUAGE MODE) AS relevance ";
            $sort_search = " relevance DESC, ";
            $where = " AND MATCH(id.title) AGAINST ('" . $this->db->escape(($data['filter_name_news'])) . "' IN NATURAL LANGUAGE MODE) > 0 ";
        } else {
            $search = '';
            $sort_search = '';
            $where = '';
        }
        $sql = "SELECT *" . $search . " FROM " . DB_PREFIX . "recruitment i INNER JOIN " . DB_PREFIX . "recruitment_description id ON (i.recruitment_id = id.recruitment_id) WHERE id.language_id = '" . (int) $this->config->get('config_language_id') . "' AND i.status = '1'";

		if (isset($data['filter_name_news']) && $data['filter_name_news']) {
			if($this->config->get('config_fts')){
				$sql .= " AND (MATCH(id.title) AGAINST ('" . $this->db->escape(utf8_strtolower($data['filter_name_news'])) . "' IN NATURAL LANGUAGE MODE)+MATCH(id.description) AGAINST ('" . $this->db->escape(utf8_strtolower($data['filter_name_news'])) . "' IN NATURAL LANGUAGE MODE)) <> 0 ";
			}else{
				$sql .= " AND (";
				$implode = array();
				$data_sort = array();

				$words = explode(' ', trim(preg_replace('/\s\s+/', ' ', $data['filter_name_news'])));

				foreach ($words as $word) {
				   $implode[] = "id.title LIKE '%" . $this->db->escape($word) . "%'";
				   $data_sort[] = "(id.title LIKE '%" . $this->db->escape($word) . "%')";
				}

				if ($implode) {
				   $sql .= " " . implode(" AND ", $implode) . "";
				}
				$sort_search = "(";
				if ($data_sort) {
				   $sort_search .= " " . implode(" + ", $data_sort) . "";
				}
				$sort_search .= ") DESC, ";
				if (!empty($data['filter_description'])) {
				   $sql .= " OR id.description LIKE '%" . $this->db->escape($data['filter_name_news']) . "%'";
				}
				$sql .= ")";
			}
        }

		$sql .= " ORDER BY " . $sort_search . "i.sort_order, LCASE(id.title) ASC";

        $query = $this->db->query($sql);

        return $query->rows;
    }

}

?>
