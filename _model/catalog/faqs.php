<?php
class ModelCatalogFaqs extends Model {
	public function getQuestions($data = array(), $sort = array()) {
		$sql = "SELECT  *
				FROM " . DB_PREFIX . "faqs
				WHERE question != '' AND answer != ''"
				. (isset($data['displayed']) ? " AND display = 1" : '')
				. (isset($data['faq_id']) ? " AND faq_id = " . (int)$data['faq_id'] : '')

				. (isset($sort['order_by']) ? " ORDER BY {$sort['order_by']} {$sort['order_way']}" : '')
				. (isset($sort['limit']) ? " LIMIT ".(int)$sort['offset'].', '.(int)($sort['limit']) : '');

		$res = $this->db->query($sql);
		return $res->rows;
	}

	public function getOtherQuestions($faq_id) {
		$sql = "SELECT  *
				FROM " . DB_PREFIX . "faqs
				WHERE question != '' AND answer != ''"
				. (isset($data['displayed']) ? " AND display = 1" : '')
				. " AND faq_id < " . (int)$faq_id
				. " ORDER BY create_time DESC LIMIT 10 ";

		$res = $this->db->query($sql);
		return $res->rows;
	}

	public function getQuestionCount($data = array()) {
		$sql = "
			SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "faqs`
        	WHERE question != '' AND answer != ''"
			. (isset($data['displayed']) ? " AND display = 1" : '');

        $res = $this->db->query($sql);

		return $res->row['total'];
	}

    public function addQuestion($question) {
    	$name = $this->db->escape($question["name"]);
    	$phone = $this->db->escape($question["phone"]);
    	$title = $this->db->escape($question["title"]);
    	$email = $this->db->escape($question["email"]);
    	$question = $this->db->escape($question["question"]);

        $sql = "INSERT INTO `" . DB_PREFIX . "faqs`
		            (name, email, create_time, phone, question, title)
        		VALUES
					('$name', '$email', UNIX_TIMESTAMP(NOW()),'$phone','$question','$title')";
		$this->db->query($sql);

       	if ($this->config->get('config_email') != '') {
							$admin_link = HTTPS_SERVER . '/oadmin/index.php?route=catalog/faqs';

							$subject = 'Câu hỏi vừa được gửi đến website '. $this->config->get('config_name')[$this->config->get('config_language_id')];
							$content_notify  = '<b>Tên người gửi:</b> ' .  $name;
							$content_notify .= '<br /><b>Điện thoại:</b> ' . 	$phone;
							$content_notify .= '<br /><b>Thư điện tử:</b> ' .   $email;
							$content_notify .= '<br /><b>Tiêu đề:</b> ' .   $title;
							$content_notify .= '<br /><b>Nội dung: </b><br />'. html_entity_decode($question, ENT_QUOTES, 'UTF-8');
							$content_notify .= '<br/><br/><br/>Đây là email tự động mang tính chất thông báo. Hãy truy cập vào <b><a href="' . $admin_link . '">Vùng quản trị để trả lời câu hỏi</a>';

							$mail = new Mail();
							$mail->protocol = $this->config->get('config_mail_protocol');
							$mail->parameter = $this->config->get('config_mail_parameter');
							$mail->hostname = $this->config->get('config_smtp_host');
							$mail->username = $this->config->get('config_smtp_username');
							$mail->password = $this->config->get('config_smtp_password');
							$mail->port = $this->config->get('config_smtp_port');
							$mail->timeout = $this->config->get('config_smtp_timeout');
							$mail->setTo($this->config->get('config_email'));
							$mail->setFrom($this->config->get('config_mail_from'));
							$mail->setSender($this->config->get('config_name')[$this->config->get('config_language_id')]);
							$mail->setSubject($subject);
							$mail->setHtml($content_notify);
							$mail->send();
        }
    }

}
