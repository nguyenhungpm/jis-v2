<?php

class ModelCatalogInformation extends Model {

    public function getInformation($information_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "information i INNER JOIN " . DB_PREFIX . "information_description id ON (i.information_id = id.information_id) WHERE i.information_id = '" . (int) $information_id . "' AND id.language_id = '" . (int) $this->config->get('config_language_id') . "' AND i.status = '1'");

        return $query->row;
    }

    public function getInformations() {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "information i INNER JOIN " . DB_PREFIX . "information_description id ON (i.information_id = id.information_id) WHERE id.language_id = '" . (int) $this->config->get('config_language_id') . "' AND i.status = '1' ORDER BY i.sort_order, LCASE(id.title) ASC");

        return $query->rows;
    }

    public function getInformationSameCategory($information_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "information i INNER JOIN " . DB_PREFIX . "information_description id ON (i.information_id = id.information_id) WHERE i.category_id = (SELECT category_id FROM " . DB_PREFIX . "information WHERE information_id = '". (int)$information_id ."') AND id.language_id = '" . (int) $this->config->get('config_language_id') . "' AND i.status = '1' ORDER BY i.sort_order, LCASE(id.title) ASC");

        return $query->rows;
    }
    public function getSpecInformations() {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "information i INNER JOIN " . DB_PREFIX . "information_description id ON (i.information_id = id.information_id) WHERE id.language_id = '" . (int) $this->config->get('config_language_id') . "' AND i.status = '1' AND i.home = '1' ORDER BY i.sort_order, LCASE(id.title) ASC");

        return $query->rows;
    }

    public function getInformationHighlights($information_id) {
        $sql = "SELECT i.*, id.description as highlight_description FROM " . DB_PREFIX . "information_highlight i LEFT JOIN " . DB_PREFIX . "information_highlight_description id ON (i.information_highlight_id = id.information_highlight_id) WHERE i.information_id = '" . (int) $information_id . "' AND id.language_id = '" . (int) $this->config->get('config_language_id') . "' ORDER BY sort_order ASC";
        // echo $sql;
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getInformationSearch($data) {
        if (!empty($data['filter_name_news']) && $this->config->get('config_fts')) {
            $search = ", MATCH(id.title) AGAINST ('" . $this->db->escape(($data['filter_name_news'])) . "' IN NATURAL LANGUAGE MODE) AS relevance ";
            $sort_search = " relevance DESC, ";
            $where = " AND MATCH(id.title) AGAINST ('" . $this->db->escape(($data['filter_name_news'])) . "' IN NATURAL LANGUAGE MODE) > 0 ";
        } else {
            $search = '';
            $sort_search = '';
            $where = '';
        }
        $sql = "SELECT *" . $search . " FROM " . DB_PREFIX . "information i INNER JOIN " . DB_PREFIX . "information_description id ON (i.information_id = id.information_id) WHERE id.language_id = '" . (int) $this->config->get('config_language_id') . "' AND i.status = '1'";

		if (isset($data['filter_name_news']) && $data['filter_name_news']) {
			if($this->config->get('config_fts')){
				$sql .= " AND (MATCH(id.title) AGAINST ('" . $this->db->escape(utf8_strtolower($data['filter_name_news'])) . "' IN NATURAL LANGUAGE MODE)+MATCH(id.description) AGAINST ('" . $this->db->escape(utf8_strtolower($data['filter_name_news'])) . "' IN NATURAL LANGUAGE MODE)) <> 0 ";
			}else{
				$sql .= " AND (";
				$implode = array();
				$data_sort = array();

				$words = explode(' ', trim(preg_replace('/\s\s+/', ' ', $data['filter_name_news'])));

				foreach ($words as $word) {
				   $implode[] = "id.title LIKE '%" . $this->db->escape($word) . "%'";
				   $data_sort[] = "(id.title LIKE '%" . $this->db->escape($word) . "%')";
				}

				if ($implode) {
				   $sql .= " " . implode(" AND ", $implode) . "";
				}
				$sort_search = "(";
				if ($data_sort) {
				   $sort_search .= " " . implode(" + ", $data_sort) . "";
				}
				$sort_search .= ") DESC, ";
				if (!empty($data['filter_description'])) {
				   $sql .= " OR id.description LIKE '%" . $this->db->escape($data['filter_name_news']) . "%'";
				}
				$sql .= ")";
			}
        }

		$sql .= " ORDER BY " . $sort_search . "i.sort_order, LCASE(id.title) ASC";

        $query = $this->db->query($sql);

        return $query->rows;
    }

}

?>
