<?php
class ModelCatalogVideo extends Model{     
    public function updateViewed($video_id) {
		$this->db->query("UPDATE " . DB_PREFIX . "video SET viewed = (viewed + 1) WHERE video_id = '" . (int)$video_id . "'");
	}
    
  
    public function getVideo($video_id) {
        
		$query = $this->db->query("SELECT * FROM ". DB_PREFIX . "video s LEFT JOIN " . DB_PREFIX . "video_description sd ON (s.video_id = sd.video_id) WHERE s.video_id = '" . (int)$video_id . "' AND sd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
		
		return $query->row;
	}
        
    public function getVideos() {    
    	$sql = "SELECT * FROM " . DB_PREFIX . "video s LEFT JOIN " . DB_PREFIX . "video_description sd ON (s.video_id = sd.video_id) ";
		
		$sql .= " WHERE 1 AND sd.language_id = '" . (int)$this->config->get('config_language_id') . "'";
                
		$sql .= " GROUP BY s.video_id";
				
		$sql.=" ORDER BY sort_order ASC  ";
		
		//$sql = "SELECT * FROM " . DB_PREFIX ."showroom WHERE status='1' ORDER BY sort_order";				
		$query = $this->db->query($sql);
		
		return $query->rows;
	}
    
}