<?php

class ModelCatalogdocument extends Model {
    public function getdocument($document_id) {
        $query = $this->db->query("SELECT DISTINCT *, rc.name as catname FROM " . DB_PREFIX . "document r INNER JOIN " . DB_PREFIX . "document_description rd ON (r.document_id = rd.document_id) LEFT JOIN " . DB_PREFIX . "doccat_description rc ON(rc.doccat_id=r.doccat_id) WHERE r.document_id = '" . (int) $document_id . "' AND rd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND rd.title <>'' AND r.status = '1'");
        return $query->row;
    }
    public function getdocuments($data) {
        $sql = "SELECT DISTINCT *,rc.name as catname FROM " . DB_PREFIX . "document r
        INNER JOIN " . DB_PREFIX . "document_description rd ON (r.document_id = rd.document_id)
        INNER JOIN " . DB_PREFIX . "doccat_description rc ON(rc.doccat_id=r.doccat_id)
        WHERE rd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND rc.language_id = '" . (int) $this->config->get('config_language_id') . "' AND rd.title <>'' AND r.status = '1'";

        if (!empty($data['filter_name'])) {
        			$sql .= " AND (";
        			$sql .= " rd.title LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
        			$sql .= " OR rd.short_description LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
        			$sql .= " )";
            }
            if (isset($data['doccat_id']) && ($data['doccat_id']>0)){
    			$sql .= " AND " . (int)$data['doccat_id'] . " = r.doccat_id ";
    		}
        $sql .= " ORDER BY r.sort_order DESC, r.date_added DESC, LCASE(rd.title) DESC";

		if (isset($data['start']) || isset($data['limit'])) {
            if (isset($data['start']) < 0) {
                $data['start'] = 0;
            }
            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }
            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getOtherdocuments($data,$id) {
        $sql = "SELECT DISTINCT *, rc.name as catname FROM " . DB_PREFIX . "document r INNER JOIN " . DB_PREFIX . "document_description rd ON (r.document_id = rd.document_id) INNER JOIN " . DB_PREFIX . "doccat_description rc ON(rc.doccat_id=r.doccat_id) WHERE rd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND rc.language_id = '" . (int) $this->config->get('config_language_id') . "' AND r.document_id <> '" . $id . "' AND rd.title <>'' AND r.status = '1' ";

        if (!empty($data['filter_name'])) {
    			$sql .= " AND (";
    			$sql .= " rd.title LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
    			$sql .= " OR rd.requirement LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
    			$sql .= " )";
        }
        if (isset($data['doccat_id']) && ($data['doccat_id']>0)){
			     $sql .= " AND " . (int)$data['doccat_id'] . " = r.doccat_id ";
        }
        $sql .=" ORDER BY r.sort_order DESC, LCASE(rd.title) DESC ";
    		if (isset($data['start']) || isset($data['limit'])) {
            if (isset($data['start']) < 0) {
                $data['start'] = 0;
            }
            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getdocumentCats() {
        $sql = "SELECT DISTINCT *, rc.name as industry FROM " . DB_PREFIX . "doccat r INNER JOIN " . DB_PREFIX . "doccat_description rc ON(rc.doccat_id=r.doccat_id) WHERE rc.language_id = '" . (int) $this->config->get('config_language_id') . "'";

        $sql .=" ORDER BY r.sort_order DESC, LCASE(rc.name) DESC ";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotaldocuments($data) {
        $sql = "SELECT COUNT(*) as total FROM " . DB_PREFIX . "document r
            INNER JOIN " . DB_PREFIX . "document_description rd ON (r.document_id = rd.document_id)
            WHERE rd.language_id = '" . (int) $this->config->get('config_language_id') . "'
            AND r.status = '1' ";

        if (!empty($data['filter_name'])) {
    			$sql .= " AND (";
    			$sql .= " rd.title LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
    			$sql .= " OR rd.short_description LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
    			$sql .= " )";
            }
            if (isset($data['doccat_id']) && ($data['doccat_id']>0)){
    			$sql .= " AND " . (int)$data['doccat_id'] . " = r.doccat_id ";
    		}
        $sql .= " ORDER BY r.sort_order, LCASE(rd.title) ASC";
        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getdocumentSearch($data) {
        if (!empty($data['filter_name_news']) && $this->config->get('config_fts')) {
            $search = ", MATCH(id.title) AGAINST ('" . $this->db->escape(($data['filter_name_news'])) . "' IN NATURAL LANGUAGE MODE) AS relevance ";
            $sort_search = " relevance DESC, ";
            $where = " AND MATCH(id.title) AGAINST ('" . $this->db->escape(($data['filter_name_news'])) . "' IN NATURAL LANGUAGE MODE) > 0 ";
        } else {
            $search = '';
            $sort_search = '';
            $where = '';
        }
        $sql = "SELECT *" . $search . " FROM " . DB_PREFIX . "document i INNER JOIN " . DB_PREFIX . "document_description id ON (i.document_id = id.document_id) WHERE id.language_id = '" . (int) $this->config->get('config_language_id') . "' AND i.status = '1'";

		    if (isset($data['filter_name_news']) && $data['filter_name_news']) {
      			if($this->config->get('config_fts')){
      				$sql .= " AND (MATCH(id.title) AGAINST ('" . $this->db->escape(utf8_strtolower($data['filter_name_news'])) . "' IN NATURAL LANGUAGE MODE)+MATCH(id.description) AGAINST ('" . $this->db->escape(utf8_strtolower($data['filter_name_news'])) . "' IN NATURAL LANGUAGE MODE)) <> 0 ";
      			}else{
      				$sql .= " AND (";
      				$implode = array();
      				$data_sort = array();

      				$words = explode(' ', trim(preg_replace('/\s\s+/', ' ', $data['filter_name_news'])));

      				foreach ($words as $word) {
      				   $implode[] = "id.title LIKE '%" . $this->db->escape($word) . "%'";
      				   $data_sort[] = "(id.title LIKE '%" . $this->db->escape($word) . "%')";
      				}

      				if ($implode) {
      				   $sql .= " " . implode(" AND ", $implode) . "";
      				}
      				$sort_search = "(";
      				if ($data_sort) {
      				   $sort_search .= " " . implode(" + ", $data_sort) . "";
      				}
      				$sort_search .= ") DESC, ";
      				if (!empty($data['filter_description'])) {
      				   $sql .= " OR id.description LIKE '%" . $this->db->escape($data['filter_name_news']) . "%'";
      				}
      				$sql .= ")";
      			}
        }

		    $sql .= " ORDER BY " . $sort_search . "i.sort_order, LCASE(id.title) ASC";
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getdoccat($doccat_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "doccat r INNER JOIN " . DB_PREFIX . "doccat_description rd ON (r.doccat_id = rd.doccat_id) WHERE r.doccat_id = '" . (int) $doccat_id . "' AND rd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND rd.name <>'' AND r.status = '1'");
        return $query->row;
    }

    public function getDocumentByHash($hash) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "document WHERE `hash` = '" . $this->db->escape($hash) . "'");
        return $query->row;
    }
    public function updateDownload($hash) {
      $this->db->query("UPDATE " . DB_PREFIX . "document SET download = (download + 1) WHERE `hash` = '" . $this->db->escape($hash) . "'");
    }

}

?>
