<?php

class ModelCatalogSearch extends Model {

	public function getSearchNews($data = array()) {
		$sort_search = '';
		$implode = array();
		$data_sort = array();

		$words = explode(' ', trim(preg_replace('/\s\s+/', ' ', $data['filter_name'])));

		foreach ($words as $word) {
		   $implode[] = "LOWER(nd.name) LIKE BINARY '%" . $this->db->escape(utf8_strtolower($word)) . "%'";
		   $data_sort[] = "(LOWER(nd.name) LIKE BINARY '%" . $this->db->escape(utf8_strtolower($word)) . "%')";
		}
		$count_search = join("+", $data_sort);

		$sql = "SELECT *, ". $count_search ." as count, nd.name FROM " . DB_PREFIX . "news n INNER JOIN " . DB_PREFIX . "news_description nd ON (n.news_id = nd.news_id) WHERE nd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND n.status = '1' AND n.date_available <= NOW() ";

    if (isset($data['filter_name']) && $data['filter_name']) {
			$sql .= " AND (";

			if ($implode) {
			   $sql .= " ". join(" OR ", $implode) . " ";
			}
			$sort_search = "(". $count_search .") DESC, ";
			if (!empty($data['filter_description'])) {
			   $sql .= " OR nd.description LIKE BINARY '%" . $this->db->escape($data['filter_name']) . "%'";
			}
			$sql .= ")";
    }

  	$sql .= " AND nd.name <> ''";

		$sql .= empty($data['filter_name']) ? "" : " ORDER BY " . $sort_search . "n.date_available DESC";
// echo $sql;
        $news_data = array();
        $query = $this->db->query($sql);

        foreach ($query->rows as $result) {
            $news_data[] = array(
				'news_id' => $result['news_id'],
				'name' => $result['name'],
				'image' => $result['image'],
				'date' => $result['date_available'],
				'short_description' => $result['short_description'],
				'description' => $result['description'],
				'type' => $this->getCategoryByNews($result['news_id']),
				'count' => $result['count'],
			);
        }

        return empty($data['filter_name']) ? array() : $news_data;
    }

	public function getSearchNewsTag($data = array()) {
		$sql = "SELECT *, nd.name FROM " . DB_PREFIX . "news n INNER JOIN " . DB_PREFIX . "news_description nd ON (n.news_id = nd.news_id) WHERE nd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND n.status = '1' AND n.date_available <= NOW() ";

    if (isset($data['filter_tag']) && $data['filter_tag']) {
			$sql .= " AND n.news_id IN (SELECT news_id FROM " . DB_PREFIX . "news_tag WHERE tag = '". $data['filter_tag'] ."' AND language_id = '" . (int) $this->config->get('config_language_id') . "')";
    }

  	$sql .= " AND nd.name <> ''";

		$sql .= " ORDER BY n.date_available DESC";
// echo $sql;
    $news_data = array();
    $query = $this->db->query($sql);

    foreach ($query->rows as $result) {
        $news_data[] = array(
				'news_id' => $result['news_id'],
				'name' => $result['name'],
				'image' => $result['image'],
				'date' => $result['date_available'],
				'short_description' => $result['short_description'],
				'description' => $result['description'],
				'type' => $this->getCategoryByNews($result['news_id']),
			);
    }

    return empty($data['filter_tag']) ? array() : $news_data;
  }

	public function getCategoryByNews($news_id){
		$cat = array();
		$query = $this->db->query("SELECT cd.name FROM " . DB_PREFIX . "news_category_description cd WHERE cd.news_category_id IN (SELECT news_category_id FROM " . DB_PREFIX . "news_to_category WHERE news_id='". $news_id ."') AND cd.language_id = '" . (int) $this->config->get('config_language_id') . "'");
		foreach($query->rows as $rs){
			$cat[] = $rs['name'];
		}
		return join(', ', $cat);
	}

	public function getSearchProduct($data = array()) {
		$sort_search = '';
		$implode = array();
		$data_sort = array();

		$words = explode(' ', trim(preg_replace('/\s\s+/', ' ', $data['filter_name'])));

		foreach ($words as $word) {
		   $implode[] = "LOWER(nd.name) LIKE BINARY '%" . $this->db->escape(utf8_strtolower($word)) . "%'";
		   $data_sort[] = "(LOWER(nd.name) LIKE BINARY '%" . $this->db->escape(utf8_strtolower($word)) . "%')";
		}
		$count_search = join("+", $data_sort);

		$sql = "SELECT *, ". $count_search ." as count, nd.name FROM " . DB_PREFIX . "product n INNER JOIN " . DB_PREFIX . "product_description nd ON (n.product_id = nd.product_id) WHERE nd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND n.status = '1' AND n.date_available <= NOW() ";

        if (isset($data['filter_name']) && $data['filter_name']) {
			$sql .= " AND (";

			if ($implode) {
			   $sql .= " ". join(" OR ", $implode) . " ";
			}
			$sort_search = "(". $count_search .") DESC, ";
			if (!empty($data['filter_description'])) {
			   $sql .= " OR nd.description LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
			}
			$sql .= ")";
        }

        $sql .= " AND nd.name <> ''";

		$sql .= empty($data['filter_name']) ? "" : " ORDER BY " . $sort_search . "n.sort_order DESC";

        $product_data = array();
        $query = $this->db->query($sql);

        foreach ($query->rows as $result) {
            $product_data[] = array(
				'product_id' => $result['product_id'],
				'name' => $result['name'],
				'short_description' => $result['short_description'],
				'description' => $result['description'],
				'type' => $this->getCategoryByProduct($result['product_id']),
				'count' => $result['count'],
			);
        }
        return empty($data['filter_name']) ? array() : $product_data;
    }

	public function getCategoryByProduct($product_id){
		$cat = array();
		$query = $this->db->query("SELECT cd.name FROM " . DB_PREFIX . "category_description cd WHERE cd.category_id IN (SELECT category_id FROM " . DB_PREFIX . "product_to_category WHERE product_id='". $product_id ."') AND cd.language_id = '" . (int) $this->config->get('config_language_id') . "'");
		foreach($query->rows as $rs){
			$cat[] = $rs['name'];
		}
		return join(', ', $cat);
	}

	public function getSearchInformation($data = array()) {
		$this->language->load('product/search');
		$sort_search = '';
		$implode = array();
		$data_sort = array();

		$words = explode(' ', trim(preg_replace('/\s\s+/', ' ', $data['filter_name'])));

		foreach ($words as $word) {
		   $implode[] = "LOWER(nd.title) LIKE BINARY '%" . $this->db->escape(utf8_strtolower($word)) . "%'";
		   $data_sort[] = "(LOWER(nd.title) LIKE BINARY '%" . $this->db->escape(utf8_strtolower($word)) . "%')";
		}
		$count_search = join("+", $data_sort);

		$sql = "SELECT *, ". $count_search ." as count, nd.title as name FROM " . DB_PREFIX . "information n INNER JOIN " . DB_PREFIX . "information_description nd ON (n.information_id = nd.information_id) WHERE nd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND n.status = '1'";

        if (isset($data['filter_name']) && $data['filter_name']) {
			$sql .= " AND (";

			if ($implode) {
			   $sql .= " ". join(" OR ", $implode) . " ";
			}
			$sort_search = "(". $count_search .") DESC, ";
			if (!empty($data['filter_description'])) {
			   $sql .= " OR nd.description LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
			}
			$sql .= ")";
        }

        $sql .= " AND nd.title <> ''";

		$sql .= empty($data['filter_name']) ? "" : " ORDER BY " . $sort_search . "n.sort_order DESC";

        $information_data = array();
        $query = $this->db->query($sql);

        foreach ($query->rows as $result) {
            $information_data[] = array(
				'information_id' => $result['information_id'],
				'name' => $result['name'],
				'short_description' => $result['description'],
				'description' => $result['description'],
				'type' => $this->language->get('type_information'),
				'count' => $result['count'],
			);
        }

        return empty($data['filter_name']) ? array() : $information_data;
    }

	// public function getSearchDirectory($data = array()) {
	// 	$this->language->load('product/search');
	// 	$sort_search = '';
	// 	$implode = array();
	// 	$data_sort = array();

	// 	$words = explode(' ', trim(preg_replace('/\s\s+/', ' ', $data['filter_name'])));

	// 	foreach ($words as $word) {
	// 	   $implode[] = "company_name LIKE '%" . $this->db->escape($word) . "%'";
	// 	   $data_sort[] = "(company_name LIKE '%" . $this->db->escape($word) . "%')";
	// 	}
	// 	$count_search = join("+", $data_sort);

	// 	$sql = "SELECT *, ". $count_search ." as count FROM " . DB_PREFIX . "register WHERE 1";

 //        if (isset($data['filter_name']) && $data['filter_name']) {
	// 		$sql .= " AND (";

	// 		if ($implode) {
	// 		   $sql .= " ". join(" OR ", $implode) . " ";
	// 		}
	// 		$sort_search = "(". $count_search .") DESC";
	// 		$sql .= ")";
 //        }

	// 	$sql .= empty($data['filter_name']) ? "" : " ORDER BY " . $sort_search . "";

 //        $directory_data = array();
 //        $query = $this->db->query($sql);

 //        foreach ($query->rows as $result) {
 //            $directory_data[] = array(
	// 			'register_id' => $result['register_id'],
	// 			'name' => $this->config->get('config_language_id')==2 ? $result['company_name'] : $result['company_name_en'],
	// 			'short_description' => '',
	// 			'description' => '',
	// 			'type' => $this->language->get('type_register'),
	// 			'count' => $result['count'],
	// 		);
 //        }

 //        return empty($data['filter_name']) ? array() : $directory_data;
 //    }

}

?>
