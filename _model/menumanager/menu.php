<?php
class ModelMenumanagerMenu extends Model{
	function menu($group_id,$column = '1')
	{
		$query  = $this->db->query("SELECT * FROM `" . DB_PREFIX . "menu` WHERE group_id = '" . (int)$group_id . "' ORDER BY `parent_id`,`position`");
		$rows = $query->rows;
		$result = $this->format_menu($rows,$column);
		return $this->tree($result);
	}

	function getMenuname($group_id)
	{
		// print_r("SELECT title, title_en FROM `" . DB_PREFIX . "menu_group` WHERE id = '" . (int)$group_id . "' AND language_id='". $this->config->get('config_language_id') ."'");
		$query  = $this->db->query("SELECT title, title_en FROM `" . DB_PREFIX . "menu_group` WHERE id = '" . (int)$group_id . "' AND language_id='". $this->config->get('config_language_id') ."'");
		return $query->row['title'];
	}

	function format_menu($rows,$column = '1')
	{
		$result = array();
		foreach($rows as $row)
		{
			$tmp = array();
			$tmp['id'] = $row['id'];
			$tmp['name'] = $row['title'];
			$tmp['href'] = $row['url'];
			$tmp['column'] = $column;
			$tmp['parent_id'] = $row['parent_id'];
			$tmp['position'] = $row['position'];
			$tmp['class'] = $row['class'];
			$result[] = $tmp;
		}

		return $result;
	}

	function tree($rows,$des='0')
	{

		$parent = 0;
		$result = array();


		foreach($rows as $key=> $row)
		{
			$child = array();
			if($row['parent_id'] == $des)
			{
				$tmp = $row;
				$tmp['children'] = $this->tree($rows,$row['id']);
				$result[] = $tmp;
			}

		}
		return $result;
	}

	private function get_descendants($id) {
		$sql = "SELECT * FROM `" . DB_PREFIX . "menu` WHERE `parent_id` = '".(int)$id."'";
		$query  = $this->db->query($sql);
		$data = $query->rows;
		$result = array();
		if (!empty($data)) {
			foreach ($data as $v) {
				$result[] = $v;
				$result = $this->get_descendants($v);
			}
		}
		return $result;
	}

	function getMenuByGroup2($group_id) {
		$data = array();
		$query = "SELECT * FROM ".DB_PREFIX."menu m LEFT JOIN ".DB_PREFIX."menu_description md ON (m.id=md.menu_id) WHERE group_id='".(int)$group_id."' AND md.language_id = '". $this->config->get('config_language_id') ."' ORDER BY parent_id,position";
		$query = $this->db->query($query);
		foreach($query->rows as $rs){
			$children = array();
			if($rs['parent_id']==0){
				if(empty($rs['title'])) continue;
				foreach($query->rows as $rs_child){
					if($rs_child['parent_id']==$rs['id']){
						if(empty($rs_child['title'])) continue;
						$children[] = array(
							'id' => $rs_child['id'],
							'menu_name' => $rs_child['title'],
							'menu_link' => $rs_child['url'],
						);
					}
				}
				$data[] = array(
					'id' => $rs['id'],
					'menu_name' => $rs['title'],
					'menu_link' => $rs['url'],
					'children' => $children
				);
			}

		}
		// print_r($data);
		return $data;
	}

	function getMenuByGroup($group_id) {
		$query = "SELECT * FROM ".DB_PREFIX."menu m LEFT JOIN ".DB_PREFIX."menu_description md ON (m.id=md.menu_id) WHERE group_id='".(int)$group_id."' AND md.language_id = '". $this->config->get('config_language_id') ."' ORDER BY parent_id,position";
		$query = $this->db->query($query);
		return $query->rows;
	}

	function getMenu($menu_id) {
		$sql = "SELECT * FROM `".DB_PREFIX."menu` WHERE `id`='".(int)$menu_id."'";
		$query = $this->db->query($sql);
		return $query->row;
	}

	function get_label($row) {
		$label =
			'<div class="'.$row['class'].'"><a href="'.$row['url'].'">'.$row['title'].'</a></div>';
		return $label;
	}

}
