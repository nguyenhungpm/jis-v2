<?php

class ModelAccountQuiz extends Model {

    //rate
    public function setRate($quiz_id, $data, $point) {
        $this->db->query("UPDATE " . DB_PREFIX . "quiz SET total_rate = (total_rate + 1), total_point = (total_point + '" . $point . "' ) WHERE quiz_id = '" . (int) $quiz_id . "'");
    }

    public function getRate($quiz_id) {
        $query = $this->db->query("SELECT total_rate, total_point FROM " . DB_PREFIX . "quiz WHERE quiz_id = '" . (int) $quiz_id . "'");
        return $query->row;
    }

    //end rate
    public function addOrder($quiz_path, $data) {
        if (!empty($quiz_path)) {
            $sql = "INSERT INTO " . DB_PREFIX . "order SET name = '" . $this->db->escape($data['name']) . "', tel = '" . $this->db->escape($data['phone2']) . "', email = '" . $this->db->escape($data['email']) . "', address = '" . $this->db->escape($data['address2']) . "', note = '" . $this->db->escape($data['enquiry']) . "', date_added=NOW()";
            $this->db->query($sql);
            $order_id = $this->db->getLastId();
            foreach (explode("_", $quiz_path) as $quiz_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "order_quiz SET order_id='" . $order_id . "', quiz_id='" . $quiz_id . "'");
            }
        } else {
            return false;
        }
    }

    public function updateViewed($quiz_id) {
        $this->db->query("UPDATE " . DB_PREFIX . "quiz SET viewed = (viewed + 1) WHERE quiz_id = '" . (int) $quiz_id . "'");
    }

    public function getQuizNews($quiz_id) {
        $news_relateds = array();
        $this->load->model('catalog/news');

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "quiz_news pn INNER JOIN " . DB_PREFIX . "news n ON (pn.news_id = n.news_id) WHERE pn.quiz_id = '" . (int) $quiz_id . "' AND n.status = '1' AND n.date_available <= NOW()");

        foreach ($query->rows as $result) {
            $news_relateds[$result['news_id']] = $this->model_catalog_news->getNew($result['news_id']);
        }

        return $news_relateds;
    }

    public function getOtherQuiz($data) {
        $quiz_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "quiz_to_group pc INNER JOIN " . DB_PREFIX . "quiz p ON (pc.quiz_id = p.quiz_id) WHERE group_id in (SELECT group_id FROM " . DB_PREFIX . "quiz_to_group WHERE quiz_id = '" . (int) $data['quiz_id'] . "') AND p.quiz_id <> '" . (int) $data['quiz_id'] . "' AND p.status = '1' AND p.date_available <= NOW() LIMIT 15");

        foreach ($query->rows as $result) {
            $quiz_data[$result['quiz_id']] = $this->getQuiz($result['quiz_id']);
        }

        return $quiz_data;
    }

    public function getQuiz($quiz_id) {
        $sql = "SELECT DISTINCT *,p.quiz_id as quiz_id, pd.name AS name, p.image, m.name AS manufacturer FROM " . DB_PREFIX . "quiz p INNER JOIN " . DB_PREFIX . "quiz_description pd ON (p.quiz_id = pd.quiz_id)  LEFT JOIN " . DB_PREFIX . "manufacturer m ON (p.manufacturer_id = m.manufacturer_id) WHERE p.quiz_id = '" . (int) $quiz_id . "' AND pd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND p.status = '1' AND p.date_available <= NOW()";
        $query = $this->db->query($sql);
        if ($query->num_rows) {
            return array(
                'quiz_id' => $query->row['quiz_id'],
                'name' => $query->row['name'],
                'title' => $query->row['title'],
                'meta_title' => $query->row['meta_title'],
                'description' => $query->row['description'],
                'short_description' => $query->row['short_description'],
                'meta_description' => $query->row['meta_description'],
                'meta_keyword' => $query->row['meta_keyword'],
                'tag' => $query->row['tag'],
                'model' => $query->row['model'],
                'image' => $query->row['image'],
                'manufacturer_id' => $query->row['manufacturer_id'],
                'manufacturer' => $query->row['manufacturer'],
                'sort_order' => $query->row['sort_order'],
                'status' => $query->row['status'],
                'date_added' => $query->row['date_added'],
                'date_modified' => $query->row['date_modified'],
                'date_available' => $query->row['date_available'],
                'viewed' => $query->row['viewed'],
                'robots' => $query->row['robots'],
                'price' => $query->row['price'],
                'quantity' => $query->row['quantity'],
                'total_rate' => $query->row['total_rate']
            );
        } else {
            return false;
        }
    }

    public function getQuizs($data = array()) {
        $sort_search = '';
        $sql = "SELECT DISTINCT p.quiz_id";
        if (!empty($data['filter_name']) && $this->config->get('config_fts')) {
            $sql .= ", MATCH(pd.name) AGAINST ('" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "' IN NATURAL LANGUAGE MODE) AS relevance , MATCH(pd.description) AGAINST ('" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "' IN NATURAL LANGUAGE MODE) AS relevance_ ";
            $sort_search = " relevance DESC, ";
        }

        if (!empty($data['filter_group_id'])) {
            if (!empty($data['filter_sub_group'])) {
                $sql .= " FROM " . DB_PREFIX . "group_path cp INNER JOIN " . DB_PREFIX . "quiz_to_group p2c ON (cp.group_id = p2c.group_id)";
            } else {
                $sql .= " FROM " . DB_PREFIX . "quiz_to_group p2c";
            }

            if (!empty($data['filter_filter'])) {
                $sql .= " INNER JOIN " . DB_PREFIX . "quiz_filter pf ON (p2c.quiz_id = pf.quiz_id) INNER JOIN " . DB_PREFIX . "quiz p ON (pf.quiz_id = p.quiz_id)";
            } else {
                $sql .= " INNER JOIN " . DB_PREFIX . "quiz p ON (p2c.quiz_id = p.quiz_id)";
            }
        } else {
            $sql .= " FROM " . DB_PREFIX . "quiz p";
        }

        $sql .= " INNER JOIN " . DB_PREFIX . "quiz_description pd ON (p.quiz_id = pd.quiz_id)  WHERE pd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND p.status = '1' AND p.date_available <= NOW()";

        if (!empty($data['filter_group_id'])) {
            if (!empty($data['filter_sub_group'])) {
                $sql .= " AND cp.path_id = '" . (int) $data['filter_group_id'] . "'";
            } else {
                $sql .= " AND p2c.group_id = '" . (int) $data['filter_group_id'] . "'";
            }

            if (!empty($data['filter_filter'])) {
                $implode = array();

                $filters = explode(',', $data['filter_filter']);

                foreach ($filters as $filter_id) {
                    $implode[] = (int) $filter_id;
                }

                $sql .= " AND pf.filter_id IN (" . implode(',', $implode) . ")";
            }
        }

        if (!empty($data['filter_name']) || !empty($data['filter_tag'])) {
            if($this->config->get('config_fts')){
                $sql .= " AND (";

                if (!empty($data['filter_name'])) {
                    $sql .= " ((MATCH(pd.name) AGAINST ('" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "' IN NATURAL LANGUAGE MODE) + MATCH(pd.description) AGAINST ('" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "' IN NATURAL LANGUAGE MODE)) <> 0) ";
                }

                if (!empty($data['filter_name']) && !empty($data['filter_tag'])) {
                    $sql .= " OR ";
                }

                if (!empty($data['filter_tag'])) {
                    $sql .= "pd.tag LIKE '%" . $this->db->escape($data['filter_tag']) . "%'";
                }

                $sql .= ")";
            }else{
                $sql .= " AND (";
                if (!empty($data['filter_name'])) {
                   $implode = array();
                   $data_sort = array();
                   $words = explode(' ', trim(preg_replace('/\s\s+/', ' ', $data['filter_name'])));
                   foreach ($words as $word) {
                       $implode[] = "pd.name LIKE BINARY '%" . $this->db->escape($word) . "%'";
                       $data_sort[] = "(pd.name LIKE BINARY '%" . $this->db->escape($word) . "%')";
                   }
                   if ($implode) {
                       $sql .= " " . implode(" AND ", $implode) . "";
                   }
                   $sort_search = "(";
                    if ($data_sort) {
                       $sort_search .= " " . implode(" + ", $data_sort) . "";
                    }
                    $sort_search .= ") DESC, ";
                   if (!empty($data['filter_description'])) {
                       $sql .= " OR pd.description LIKE BINARY '%" . $this->db->escape($data['filter_name']) . "%'";
                   }
                }
                if (!empty($data['filter_name']) && !empty($data['filter_tag'])) {
                    $sql .= " OR ";
                }
                if (!empty($data['filter_tag'])) {
                    $sql .= "pd.tag LIKE BINARY '%" . $this->db->escape($data['filter_tag']) . "%'";
                }
                $sql .= ")";
            }

        }

        if (!empty($data['filter_manufacturer_id'])) {
            $sql .= " AND p.manufacturer_id = '" . (int) $data['filter_manufacturer_id'] . "'";
        }

        // $sql .= " GROUP BY p.quiz_id";

        $sort_data = array(
            'pd.name',
            'p.model',
            'p.quantity',
            'p.total_rate',
            'p.date_added'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            if ($data['sort'] == 'pd.name' || $data['sort'] == 'p.model') {
                $sql .= " ORDER BY " . $sort_search . "LCASE(" . $data['sort'] . ")";
            } elseif ($data['sort'] == 'p.price') {
                $sql .= " ORDER BY " . $sort_search . "(CASE WHEN special IS NOT NULL THEN special WHEN discount IS NOT NULL THEN discount ELSE p.price END)";
            } else {
                $sql .= " ORDER BY " . $sort_search . $data['sort'];
            }
        } else {
            $sql .= " ORDER BY " . $sort_search . "p.sort_order";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC, LCASE(pd.name) DESC";
        } else {
            $sql .= " ASC, LCASE(pd.name) ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $quiz_data = array();
        $query = $this->db->query($sql);

        foreach ($query->rows as $result) {
            $quiz_data[$result['quiz_id']] = $this->getQuiz($result['quiz_id']);
        }

        return $quiz_data;
    }

    public function getLatestQuizs($limit) {
        if ($this->customer->isLogged()) {
            $customer_group_id = $this->customer->getCustomerGroupId();
        } else {
            $customer_group_id = $this->config->get('config_customer_group_id');
        }

        $quiz_data = $this->cache->get('quiz.latest.' . (int) $this->config->get('config_language_id') . '.' . (int) $this->config->get('config_store_id') . '.' . $customer_group_id . '.' . (int) $limit);

        if (!$quiz_data) {
            $query = $this->db->query("SELECT p.quiz_id FROM " . DB_PREFIX . "quiz p  WHERE p.status = '1' AND p.date_available <= NOW() ORDER BY p.date_added DESC LIMIT " . (int) $limit);

            foreach ($query->rows as $result) {
                $quiz_data[$result['quiz_id']] = $this->getQuiz($result['quiz_id']);
            }

            $this->cache->set('quiz.latest.' . (int) $this->config->get('config_language_id') . '.' . (int) $this->config->get('config_store_id') . '.' . $customer_group_id . '.' . (int) $limit, $quiz_data);
        }

        return $quiz_data;
    }

    public function getPopularQuizs($limit) {
        $quiz_data = array();

        $query = $this->db->query("SELECT p.quiz_id FROM " . DB_PREFIX . "quiz p  WHERE p.status = '1' AND p.date_available <= NOW() ORDER BY p.viewed, p.date_added DESC LIMIT " . (int) $limit);

        foreach ($query->rows as $result) {
            $quiz_data[$result['quiz_id']] = $this->getQuiz($result['quiz_id']);
        }

        return $quiz_data;
    }

    public function getQuizRelated($quiz_id) {
        $quiz_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "quiz_related pr INNER JOIN " . DB_PREFIX . "quiz p ON (pr.related_id = p.quiz_id)  WHERE pr.quiz_id = '" . (int) $quiz_id . "' AND p.status = '1' AND p.date_available <= NOW()");

        foreach ($query->rows as $result) {
            $quiz_data[$result['related_id']] = $this->getQuiz($result['related_id']);
        }
        $num_related = $max_related - count($quiz_data);

        if($num_related > 0)
        {
            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "quiz_to_group p2c LEFT JOIN " . DB_PREFIX . "quiz p ON (p2c.quiz_id = p.quiz_id) LEFT JOIN " . DB_PREFIX . "quiz_to_store p2s ON (p.quiz_id = p2s.quiz_id) WHERE p2c.group_id IN (SELECT group_id FROM " . DB_PREFIX . "quiz_to_group WHERE quiz_id = '" . (int)$quiz_id . "') AND p.quiz_id != '" . (int)$quiz_id . "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' ORDER BY RAND() LIMIT 0," . $num_related);

            foreach ($query->rows as $result) {
                $quiz_data[$result['quiz_id']] = $this->getQuiz($result['quiz_id']);
            }
        }

        $num_related = $max_related - count($quiz_data);
        if($num_related > 0)
        {

            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "quiz p LEFT JOIN " . DB_PREFIX . "quiz_to_store p2s ON (p.quiz_id = p2s.quiz_id) WHERE p.manufacturer_id IN (SELECT manufacturer_id FROM " . DB_PREFIX . "quiz WHERE quiz_id = '" . (int)$quiz_id . "') AND p.quiz_id != '" . (int)$quiz_id . "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' ORDER BY RAND() LIMIT 0," . $num_related);

            foreach ($query->rows as $result)
                $quiz_data[$result['quiz_id']] = $this->getQuiz($result['quiz_id']);

            return $quiz_data;
        }

        return $quiz_data;
    }

    public function getQuizLayoutId($quiz_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "quiz_to_layout WHERE quiz_id = '" . (int) $quiz_id . "'");

        if ($query->num_rows) {
            return $query->row['layout_id'];
        } else {
            return false;
        }
    }

    public function getGroups($quiz_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "quiz_to_group WHERE quiz_id = '" . (int) $quiz_id . "'");

        return $query->rows;
    }

    public function getQuizImages($quiz_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "quiz_image WHERE quiz_id = '" . (int) $quiz_id . "' ORDER BY sort_order ASC");

        return $query->rows;
    }

    public function getAllQuizs($data = array()) {
        $sql = "SELECT DISTINCT(p.quiz_id) FROM " . DB_PREFIX . "quiz p WHERE p.status = '1' AND p.date_available <= NOW() ORDER BY p.date_added DESC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $quiz_data = array();
        $query = $this->db->query($sql);

        foreach ($query->rows as $result) {
            $quiz_data[$result['quiz_id']] = $this->getQuiz($result['quiz_id']);
        }

        return $quiz_data;
    }
    public function getTotalQuizs($data = array()) {

        $sql = "SELECT COUNT(DISTINCT p.quiz_id) AS total";

        if (!empty($data['filter_group_id'])) {
            if (!empty($data['filter_sub_group'])) {
                $sql .= " FROM " . DB_PREFIX . "group_path cp INNER JOIN " . DB_PREFIX . "quiz_to_group p2c ON (cp.group_id = p2c.group_id)";
            } else {
                $sql .= " FROM " . DB_PREFIX . "quiz_to_group p2c";
            }

            if (!empty($data['filter_filter'])) {
                $sql .= " INNER JOIN " . DB_PREFIX . "quiz_filter pf ON (p2c.quiz_id = pf.quiz_id) INNER JOIN " . DB_PREFIX . "quiz p ON (pf.quiz_id = p.quiz_id)";
            } else {
                $sql .= " INNER JOIN " . DB_PREFIX . "quiz p ON (p2c.quiz_id = p.quiz_id)";
            }
        } else {
            $sql .= " FROM " . DB_PREFIX . "quiz p";
        }

        $sql .= " INNER JOIN " . DB_PREFIX . "quiz_description pd ON (p.quiz_id = pd.quiz_id)  WHERE pd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND p.status = '1' AND p.date_available <= NOW()";

        if (!empty($data['filter_group_id'])) {
            if (!empty($data['filter_sub_group'])) {
                $sql .= " AND cp.path_id = '" . (int) $data['filter_group_id'] . "'";
            } else {
                $sql .= " AND p2c.group_id = '" . (int) $data['filter_group_id'] . "'";
            }

            if (!empty($data['filter_filter'])) {
                $implode = array();

                $filters = explode(',', $data['filter_filter']);

                foreach ($filters as $filter_id) {
                    $implode[] = (int) $filter_id;
                }

                $sql .= " AND pf.filter_id IN (" . implode(',', $implode) . ")";
            }
        }

        if (!empty($data['filter_name']) || !empty($data['filter_tag'])) {
            $sql .= " AND (";

            if (!empty($data['filter_name'])) {
                $implode = array();

                $words = explode(' ', trim(preg_replace('/\s\s+/', ' ', $data['filter_name'])));

                foreach ($words as $word) {
                    $implode[] = "pd.name LIKE BINARY '%" . $this->db->escape($word) . "%'";
                }

                if ($implode) {
                    $sql .= " " . implode(" AND ", $implode) . "";
                }

                if (!empty($data['filter_description'])) {
                    $sql .= " OR pd.description BINARY LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
                }
            }

            if (!empty($data['filter_name']) && !empty($data['filter_tag'])) {
                $sql .= " OR ";
            }

            if (!empty($data['filter_tag'])) {
                $sql .= "pd.tag LIKE BINARY '%" . $this->db->escape(utf8_strtolower($data['filter_tag'])) . "%'";
            }

            if (!empty($data['filter_name'])) {
                $sql .= " OR LCASE(p.model) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
            }

            $sql .= ")";
        }

        if (!empty($data['filter_manufacturer_id'])) {
            $sql .= " AND p.manufacturer_id = '" . (int) $data['filter_manufacturer_id'] . "'";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

}

?>
