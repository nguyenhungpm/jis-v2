<?php

class ModelAccountGroup extends Model {

    public function getGroup($group_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "group c INNER JOIN " . DB_PREFIX . "group_description cd ON (c.group_id = cd.group_id)  WHERE c.group_id = '" . (int) $group_id . "' AND cd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND c.status = '1'");

        return $query->row;
    }

    public function getGroups($parent_id = 0) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "group c INNER JOIN " . DB_PREFIX . "group_description cd ON (c.group_id = cd.group_id)  WHERE c.parent_id = '" . (int) $parent_id . "' AND cd.language_id = '" . (int) $this->config->get('config_language_id') . "'  AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)");

        return $query->rows;
    }

    public function getGroupNews($group_id) {
        $news_relateds = array();
        $this->load->model('catalog/news');

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "group_related pn INNER JOIN " . DB_PREFIX . "news n ON (n.news_id = pn.news_id) WHERE pn.group_id = '" . (int) $group_id . "' AND n.status = '1' AND n.date_available <= NOW()");

        foreach ($query->rows as $result) {
            $news_relateds[$result['news_id']] = $this->model_catalog_news->getNew($result['news_id']);
        }
        return $news_relateds;
    }

    public function getGroupFilters($group_id) {
        $implode = array();

        $query = $this->db->query("SELECT filter_id FROM " . DB_PREFIX . "group_filter WHERE group_id = '" . (int) $group_id . "'");

        foreach ($query->rows as $result) {
            $implode[] = (int) $result['filter_id'];
        }


        $filter_group_data = array();

        if ($implode) {
            $filter_group_query = $this->db->query("SELECT DISTINCT f.filter_group_id, fgd.name, fg.sort_order FROM " . DB_PREFIX . "filter f INNER JOIN " . DB_PREFIX . "filter_group fg ON (f.filter_group_id = fg.filter_group_id) INNER JOIN " . DB_PREFIX . "filter_group_description fgd ON (fg.filter_group_id = fgd.filter_group_id) WHERE f.filter_id IN (" . implode(',', $implode) . ") AND fgd.language_id = '" . (int) $this->config->get('config_language_id') . "' GROUP BY f.filter_group_id ORDER BY fg.sort_order, LCASE(fgd.name)");

            foreach ($filter_group_query->rows as $filter_group) {
                $filter_data = array();

                $filter_query = $this->db->query("SELECT DISTINCT f.filter_id, fd.name FROM " . DB_PREFIX . "filter f INNER JOIN " . DB_PREFIX . "filter_description fd ON (f.filter_id = fd.filter_id) WHERE f.filter_id IN (" . implode(',', $implode) . ") AND f.filter_group_id = '" . (int) $filter_group['filter_group_id'] . "' AND fd.language_id = '" . (int) $this->config->get('config_language_id') . "' ORDER BY f.sort_order, LCASE(fd.name)");

                foreach ($filter_query->rows as $filter) {
                    $filter_data[] = array(
                        'filter_id' => $filter['filter_id'],
                        'name' => $filter['name']
                    );
                }

                if ($filter_data) {
                    $filter_group_data[] = array(
                        'filter_group_id' => $filter_group['filter_group_id'],
                        'name' => $filter_group['name'],
                        'filter' => $filter_data
                    );
                }
            }
        }

        return $filter_group_data;
    }

    public function getGroupLayoutId($group_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "group_to_layout WHERE group_id = '" . (int) $group_id . "'");

        if ($query->num_rows) {
            return $query->row['layout_id'];
        } else {
            return false;
        }
    }

    public function getTotalGroupsByGroupId($parent_id = 0) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "group c  WHERE c.parent_id = '" . (int) $parent_id . "' AND c.status = '1'");

        return $query->row['total'];
    }

    public function getAllGroupsByTop() {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "group c INNER JOIN " . DB_PREFIX . "group_description cd ON (c.group_id = cd.group_id)  WHERE c.top='1' AND cd.language_id = '" . (int) $this->config->get('config_language_id') . "'  AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)");

        return $query->rows;
    }

}

?>
