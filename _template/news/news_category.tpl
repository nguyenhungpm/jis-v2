<?php echo $header; ?>
<style>
  #tf-menu {
    background: #0a0f3c;
  }
  .blog-temp1 {
    margin-top: 100px;
  }
</style>
<section class="blog-temp1">
    <div class="container">
        <div class="row">

          <?php if(count($news) > 0){ ?>
            <div class="col-md-6 fix-colum">
                <div class="tempLeft">
                    <a href="<?php echo $news[0]['href']; ?>" class="hvr-grow"><img src="<?php echo $this->model_tool_image->cropsize($news[0]['image'], 546, 392); ?>" width="100%"></a>
                    <div class="box_temp">
                        <h4><a href="<?php echo $news[0]['href']; ?>"><?php echo $news[0]['name']; ?></a></h4>
                    </div>
                </div>
            </div>
            <?php } ?>
            <div class="col-md-6 fix-colum">


              <?php if(count($news) > 1){ ?>
                <div class="tempRight">
                    <a href="<?php echo $news[1]['href']; ?>"><img src="<?php echo $this->model_tool_image->cropsize($news[1]['image'], 546, 193); ?>" width="100%"></a>
                    <div class="btn-temp">
                        <p><?php echo $news[1]['name']; ?></p>
                        <a href="<?php echo $news[1]['href']; ?>">Chi tiết</a>
                    </div>
                </div>
                <?php } ?>



                <div class="row ">
                    <?php if(count($news) > 2){ ?>
                    <div class="col-md-6 fix-colum_v2">
                        <div class="tempRight">
                            <a href="<?php echo $news[2]['href']; ?>"><img src="<?php echo $this->model_tool_image->cropsize($news[2]['image'], 271, 193); ?>" width="100%"></a>
                            <div class="btn-temp">
                                <p><?php echo $news[2]['name']; ?></p>
                                <a href="<?php echo $news[2]['href']; ?>">Chi tiết</a>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if(count($news) > 3){ ?>
                    <div class="col-md-6 fix-colum_v3">
                        <div class="tempRight">
                            <a href="<?php echo $news[3]['href']; ?>"><img src="<?php echo $this->model_tool_image->cropsize($news[3]['image'], 271, 193); ?>" width="100%"></a>
                            <div class="btn-temp">
                                <p><?php echo $news[3]['name']; ?></p>
                                <a href="<?php echo $news[3]['href']; ?>">Chi tiết</a>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content-temp">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h4 class="head-title-blog"><?php echo $heading_title; ?></h4>
                  <?php for($i=4; $i< count($news); $i++){ ?>
                  <?php if($i%2==0){ ?><div class="row"><?php } ?>
                    <div class="col-md-6">
                        <div class="tLeft-v2">
                            <a href="" class="hvr-grow"><img src="<?php echo $news[$i]['thumb']; ?>" width="100%"></a>
                            <h4><a href="<?php echo $news[$i]['href']; ?>"><?php echo $news[$i]['name']; ?></a></h4>
                            <!-- <p>Entertainment</p> -->
                        </div>
                        <div class="categories-name">
                            <h4><a href="<?php echo $news[$i]['href']; ?>"><?php echo $news[$i]['name']; ?></a></h4>

                            <a class="timer" href=""><?php if(!empty($news[$i]['author'])){ echo $news[$i]['author']. " - "; } ?><time><?php echo $news[$i]['date_added']; ?></time></a>
                            <p><?php echo $news[$i]['short_description']; ?></p>
                        </div>

                    </div>
                  <?php if($i%2==1 || $i == count($news)-1){ ?></div><?php } ?>
                  <?php } ?>
                  <?php echo $pagination; ?>
            </div>

            <div class="col-md-4">
              <h4 class="head-title-blog">
                  Entertainment
              </h4>
              <?php echo $column_right; ?>
            </div>
        </div>
    </div>
</section>
<section class="content-bottom">
    <div class="container">
      <?php echo $content_bottom; ?>
    </div>
</section>

<?php echo $footer; ?>
