<?php echo $header; ?>
<style>
	.zonegutter {
		padding-top: 40px;
		padding-bottom: 40px;
	}
	 .n--m{
		margin:0;
	}
	.n--pb{
		padding-bottom:0;
	}
	.upper{
		text-transform:uppercase;
	}
	.t--c{
		text-align:center;
	}
	.relative{
		position:relative;
	}
	.absolute{
		position:absolute;
	}
	.white{
		color:#fff;
	}
	.cover__banner{
		top:0;
		left:0;
		right:0;
		z-index:-1;
	}
	.cover__banner:after{
		content:"";
		position:absolute;
		left:0;
		right:0;
		top:0;
		bottom:0;
		background:rgba(0,0,0,.35);
		z-index:0;
	}

</style>
<div id="fakediv" class="d-none"></div>

<div class="zonegutter relative cover__wrap cover__wrap--blog" style="background-image: url('https://datax-talent.basecdn.net/cmcts/bg-center.png'); background-position: center; background-repeat: no-repeat; background-size: cover;">
   <div class="container relative t--c">
      <div class="small white" itemscope="" itemtype="http://schema.org/BreadcrumbList">
	  	<?php foreach ($breadcrumbs as $key=>$breadcrumb) { ?>
			<?php echo $key == 0 ? '': '→ '; ?>
         <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
               <span itemprop="name" class="white"><?php echo $breadcrumb['text']; ?></span>
               <meta itemprop="position" content="<?php echo $key+1; ?>">
            </a>
         </span>
		<?php } ?>
      </div>
      <div class="rowgutter n--pb">
         <h1 class="maintitle white n--m upper"><?php echo $this->language->get('text_move_jiss'); ?></h1>
      </div>
   </div>
</div>
<div class="bg-white">
<div id="blog">
<div class="section-body container">
	<div class="col-12 catlist">
		<h1 class="blue n-mt rowgutter"><?php echo $heading_title; ?></h1>
		<?php foreach($news_categories as $cat){ ?>
			<a><?php echo $cat['name'];?></a>
		<?php } ?>
	</div>
	<div class="row clear">
		<?php foreach($news as $key => $news_item){ ?>
		<div class="col-xs-12 col-sm-4 rowgutter">
			<div class="faces_item">
				<div class="faces_body">
					<div class="faces_media">
						<div class="entry-thumbnail">
							<!-- <img width="1638" height="2048" src="<?php echo $news_item['thumb']; ?>" class="attachment-full size-full wp-post-image">  -->
							<img width="1638" height="2048" src="<?php echo $this->model_tool_image->cropsize($news_item['image'], $this->config->get('config_image_news_width'), $this->config->get('config_image_news_height')); ?>" class="attachment-full size-full wp-post-image"> 
						</div>
						<div class="faces_info">
							<p class="faces_name"><?php echo $news_item['author']; ?></p>
							<p class="faces_school"><?php echo $news_item['name'];?></p>
						</div>
					</div>
					<a href="<?php echo $news_item['href'];?>" class="faces_content">
						<p class="faces_nameHover"><?php echo $news_item['author'];?></p>
						<p class="faces_classHover"></p>
						<p class="faces_schoolHover"><?php echo $news_item['name'];?></p>
						<p class="faces_giathuong">
							<?php echo $news_item['short_description'];?>
						</p>
					</a>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</div>

</div>
<?php echo $footer; ?>
