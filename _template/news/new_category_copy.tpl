<?php echo $header; ?>
<?php // echo $content_top; ?>
<style>
#moveDown{
  display: none;
}
</style>
<div class="banner-oteam relative" style="background: url(<?php echo !empty($top_news_info['banner']) ? 'media/'.$top_news_info['banner']:'media/data/member/our-team.jpg'; ?>) center; background-size:cover">
	<div class="odescription">
		<div class="container">
			<div class="row">
			<div class="col-lg-6 gutter">
				<h1 class="htitle"><a href="<?php echo $this->url->link('news/news', 'news_id=' . $top_news_info['news_id']); ?>"><?php echo $top_news_info['name']; ?></a></h1>
				<div class="short_title main-color relative"><?php echo $heading_title; ?></div>
			</div>
			</div>
		</div>
	</div>
</div>
<div class="sub">
	<div class="container clearfix">
		<ul>
			<?php foreach($top_categories as $top_category){ ?>
			<li>
				<a class="<?php echo $top_category['news_category_id']==$this->request->get['cat_id'] ? 'active':''; ?>" href="<?php echo $top_category['href']; ?>"><?php echo $top_category['name']; ?></a>
			</li>
			<?php } ?>
		</ul>
	</div>
</div>
<div class="container">
	<div class="row clearfix">
		<div class="col-lg-1">&nbsp;</div>
		<div class="col-lg-10 main-color rowgutter text-center h4 italic short-description <?php echo empty($description) ? 'no-desc': ''; ?>">
			<?php echo $description; ?>
		</div>
			<div class="col-lg-1">&nbsp;</div>
	</div>
	<div class="row clearfix">
		<?php foreach ($news as $key=>$news_item) { ?>
			<div class="col-lg-4">
				<div class="items">
					<a class="block" href="<?php echo $news_item['href']; ?>"><img src="<?php echo $news_item['thumb']; ?>" alt="<?php echo $news_item['name']; ?>" class="block" /></a>
					<h3><a href="<?php echo $news_item['href']; ?>" class="color-black"><?php echo $news_item['name']; ?></a></h3>
				</div>
			</div>
		<?php } ?>
    <div id="listExpand"></div>
    <form id="expand-news">
      <input type="hidden" name="page" id="page" value="2"/>
      <input type="hidden" name="news_category_id" value="<?php echo $news_category_id; ?>"/>
    </form>
    <script>
      function addNews(id){
        var request = new XMLHttpRequest();
      	request.open('POST', 'index.php?route=news/news_category/addNews', false);
      	var formData = new FormData(document.getElementById(id));
      	request.send(formData);
      	console.log(request);
      	if (request.status == 200) {
      		var json = JSON.parse(request.responseText);
      		console.log('db', json);
          // document.getElementById("listExpand").appendChild('<p>a</p>');
          var d1 = document.getElementById('listExpand');
          if (json['data'].length > 0) {
            json['data'].forEach((item, index) => {
              var element = '<div class="col-lg-4">';
    				      element += '<div class="items">';
                  element += '<a class="block" href="'+ item['href'] +'"><img src="'+ item['thumb'] +'" alt="'+ item['name'] +'" class="block"></a>';
                  element += '<h3><a href="'+ item['href'] +'" class="color-black">'+ item['name'] +'</a></h3>';
                  element += '</div>';
                  element += '</div>';
              d1.insertAdjacentHTML('beforeend', element);
            });
          }
          document.getElementById("page").value = json['page'];
      	} else {
      		console.log('Error: ' + request.statusText)
      	}
      }
    </script>
	</div>
	<div class="row clearfix text-center">
    <div class="clearfix" style="cursor: pointer" id="addNews" onclick="addNews('expand-news')"></div>
	</div>
</div>
<?php echo $footer; ?>
