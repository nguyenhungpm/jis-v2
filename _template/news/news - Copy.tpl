<?php echo $header; ?>
<?php //echo $content_top; ?>
<style>
.news-news .content{
  width: 840px;
  padding: 0 160px 0 80px;
}
.news-news .details{
  padding: 40px 100px 0;
  position: relative;
}
.col-leftnews, .col-rightnews{
  position: absolute;
}
.col-rightnews{
  width: 175px;
  top: 40px;
  right: 30px;
}
.col-leftnews{
  width: 68px;
  left: 40px;
  top: 50px;
}
.h5img {
  /* width: 840px; */
  margin-left: -80px;
  margin-right: 20px;
  float: left;
}
.h5img img{
  float: left;
  max-width: 400px;
}
.h6img {
  /* width: 840px; */
  margin-right: -80px;
  margin-left: 20px;
  float: right;
}
.h6img img{
  float: right;
  max-width: 400px;
}
.h4img {
  width: 840px;
  margin-left: -80px;
}
.content .tableh3img,
.h3img {
  width: 100vw !important;
}
.content table{
  width: 100% !important;
}
.ekip b{
  font-family: 'Noto Serif', serif;
}
</style>
<?php if(!empty($banner)){ ?>
  <div class="banner-oteam relative" style="background: url(<?php echo !empty($banner) ? $banner : 'media/data/member/our-team.jpg'; ?>) center; background-size:cover">
  	<div class="odescription">
  		<div class="container">
  			<div class="row">
  			<div class="col-lg-6 gutter">
  				<h1 class="htitle" style="color: white"><?php echo $heading_title; ?></h1>
          <?php if(!empty($cat_info['meta_title'])){ ?>
    				<div class="short_title main-color relative"><?php echo $cat_info['meta_title']; ?></div>
          <?php } ?>
  			</div>
  			</div>
  		</div>
  	</div>
  </div>
<?php } else { ?>
  <div class="splus-section splus-section-main-profile-video-cover is-topten wrapper_section_video">
      <div class="splussmpvc-wrapper">
          <div class="splussmpvcw-video" style="background-image: url('http://media.senplus.vn/files/library/images/Ve-Senplus-01.jpg');">
              <video id="splussmpvcwv-bg" autoplay="" loop="" muted="" playsinline="playsinline">
                  <source src="media/data/video/video.mp4" type="video/mp4" poster="http://channel.mediacdn.vn/2017/quoctuan-1513805179233.png">
              </video>
          </div>
          <div class="splussmpvcw-info skrollable skrollable-between" id="easing" style="opacity: 1; transform: translateY(0px);" data-1000="opacity:0;transform: translateY(200px);" data-0="opacity:1; transform: translateY(0px);">
              <div class="splussmpvcwi-wrapper clearfix">
                      <span class="splussmpvcwiw-btn video-link" id="splussmpvcwiw-btn" vidurl="media/data/video/video.mp4">
                          <svg xmlns="http://www.w3.org/2000/svg" class="playOutline" viewBox="0 0 43 49" width="42" height="48">
                              <path fill="none" d="M 1.5 45 V 4.1 A 2.5 2.5 0 0 1 5.4 1.9 L 40.2 22.3 a 2.6 2.6 0 0 1 0 4.4 L 5.3 47.2 A 2.5 2.5 0 0 1 1.5 45 Z"></path>
                          </svg>
                          <svg xmlns="http://www.w3.org/2000/svg" class="playStroke" viewBox="0 0 43 49" width="42" height="48">
                              <path fill="none" d="M 1.5 45 V 4.1 A 2.5 2.5 0 0 1 5.4 1.9 L 40.2 22.3 a 2.6 2.6 0 0 1 0 4.4 L 5.3 47.2 A 2.5 2.5 0 0 1 1.5 45 Z"></path>
                          </svg>
                      </span>
                  <span class="splussmpvcwiw-label"><strong><?php echo $title; ?></strong></span>


                  <!--<h1 class="splussmpvcwiw-name">Sơn Tùng  M-TP</h1>-->
                  <p class="splussmpvcwiw-title"></p>
                  <!--<span class="splussmpvcwiw-icon clearfix"><i></i></span>-->
              </div>
          </div>
      </div>
  </div>
<?php } ?>
<div class="container">
	<div class="row clearfix">
		<div class="col-lg-1">&nbsp;</div>
		<div class="col-lg-10 text-center zonegutter">
			<h1 class="short_description"><?php echo $short_description; ?><?php echo $admin_logged ? ' (<a target="_blank" href="'.$edit.'">Sửa</a>)': ''?></h1>
		</div>
		<div class="col-lg-1">&nbsp;</div>
		<div class="col-lg-12 gutter"><div class="line">&nbsp;</div></div>
	</div>
	<div class="row">
	<div class="details clearfix">
		<div class="col-leftnews">
			<div class="sub-title">
				<h4><a href="<?php echo $this->url->link('news/news_category', 'cat_id=' . $cat_info['news_category_id']); ?>"><?php echo $cat_info['name']; ?></a></h4>
				<span><?php echo $cat_info['meta_title']; ?></span>
			</div>
		</div>
		<div class="col-lg-7">
		<div class="content">
			<?php echo $description; ?>
    </div>
    <div class="content_bottom">
      <?php if($news){ ?>
			<div class="gutter">
				<h5 class="main-color">Tin liên quan</h5>
				<ul style="padding-left: 20px;">
					<?php foreach($news as $item){ ?>
						<li><a href="<?php echo $item['href']; ?>"><?php echo $item['name']; ?></a></li>
					<?php } ?>
				</ul>
			</div>
      <?php } ?>

			<div class="gutter ekip">
				<div class="row">
          <?php if($author){ ?>
					<div class="col-lg-4 gutter">
						<span style="font-size: 80%;">Bài viết:</span><br />
						<b><?php echo $author; ?></b>
					</div>
          <?php } ?>
          <?php if($photographer){ ?>
					<div class="col-lg-4 gutter">
						<span style="font-size: 80%;">Ảnh:</span><br />
						<b><?php echo $photographer; ?></b>
					</div>
        <?php } ?>
        <?php if($designer){ ?>
					<div class="col-lg-4 gutter">
						<span style="font-size: 80%;">Thiết kế:</span><br />
						<b><?php echo $designer; ?></b>
					</div>
        <?php } ?>
        <?php if($director){ ?>
					<div class="col-lg-4 gutter">
						<span style="font-size: 80%;">Đạo diễn, Quay và dựng phim:</span><br />
						<b><?php echo $director; ?></b>
					</div>
          <?php } ?>
				</div>
			</div>
      <div class="tag gutter">
      Tags:
      <?php foreach($tags as $key => $tag){ ?>
      <?php echo $key==0 ? '':' | '; ?>
      <a style="color: #333" href="index.php?route=product/search&tag=<?php echo $tag['tag']; ?>"><?php echo $tag['tag']; ?></a>
      <?php } ?>
      </div>
			<div class="line"></div>
			<p class="gutter" style="margin-bottom: 0;">Hãy cùng chia sẻ để lan tỏa niềm cảm hứng này</p>
			<div class="">
				<iframe src="https://www.facebook.com/plugins/like.php?href=<?php echo $url; ?>&width=156&layout=button_count&action=like&size=small&show_faces=true&share=true&height=46&appId=505262819991065" width="156" height="46" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
			</div>

			<?php echo $content_bottom; ?>
		</div>
		</div>
		<div class="col-rightnews">
			<div class="text-right gutter">
				<iframe src="https://www.facebook.com/plugins/like.php?href=<?php echo $url; ?>&width=156&layout=button_count&action=like&size=small&show_faces=true&share=true&height=46&appId=505262819991065" width="156" height="46" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
			</div>
		</div>
	</div>
	</div>
	<div class="row clearfix zonegutter">
		<?php foreach ($other_news as $key=>$news_item) { ?>
			<div class="col-lg-4">
				<div class="items">
					<a class="block" href="<?php echo $news_item['href']; ?>"><img src="<?php echo $news_item['thumb']; ?>" alt="<?php echo $news_item['name']; ?>" class="block" /></a>
					<h3><a href="<?php echo $news_item['href']; ?>" class="color-black"><?php echo $news_item['name']; ?></a></h3>
				</div>
			</div>
		<?php } ?>
	</div>
</div>
<?php echo $footer; ?>
