<?php echo $header; ?>
<div id="fakediv" class="d-none"></div>

<div class="detail-temp1">
    <div class="container">
        <div class="col-12 rowgutter">
            <div class="small white" itemscope itemtype="http://schema.org/BreadcrumbList">
                <?php foreach ($breadcrumbs as $key=>$breadcrumb) { ?>
                    <?php echo $breadcrumb['separator']; ?>
                    <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                        <a itemprop="item" href="<?php echo $breadcrumb['href']; ?>"><span itemprop="name" class="white"><?php echo $breadcrumb['text']; ?></span>
                            <meta itemprop="position" content="<?php echo $key+1; ?>" />
                        </a>
                    </span>
                <?php } ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="content_left">
                    <div class="title-temp2 tempDetail">
                        <h3><?php echo $heading_title; ?><?php echo $admin_logged ? ' (<a target="_blank" href="'.$edit.'">Sửa</a>)': ''?></h3>
                        <ul>

                            <li><a href=""><span class="fa fa-facebook"></span></a></li>
                            <li><a href=""><span class="fa fa-twitter"></span></a></li>
                            <li><a href=""><span class="fa fa-google-plus"></span></a></li>
                            <li><a href=""><span class="fa fa-youtube"></span></a></li>
                        </ul>
                        <div class="text-italic"><?php echo $this->language->get('text_date_posts'); ?>: <span><?php echo $date_added; ?></span></p></div>
                    </div>
                </div>
                <div class="content"><?php echo $description; ?></div>
                <div class="content-bottom rowgutter">

                    <h2 class="h4 title-section title-decoration"><?php echo $this->language->get('text_other_posts'); ?></h2>
                    <div class="row rowgutter">
                      <?php foreach ($other_news as $key=>$news_item) { ?>
                        <div class="col-md-4 col-sm-4">
                            <div class="news-other clear-flex">
                                <div class="img-other">
                                    <a href="<?php echo $news_item['href']; ?>"><img src="<?php echo $news_item['thumb']; ?>" width="100%"></a>
                                </div>
                                <h4 class="bt-name p-1"><a href="<?php echo $news_item['href']; ?>"><?php echo $news_item['name']; ?></a></h4>
                            </div>
                        </div>
                      <?php } ?>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <?php echo $column_right; ?>
            </div>
            <div class="">
              <?php echo $content_bottom; ?>
            </div>
        </div>
    </div>
</div>

<?php echo $footer; ?>
