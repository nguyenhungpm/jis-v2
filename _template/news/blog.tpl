<?php echo $header; ?>
<style>
	.zonegutter {
		padding-top: 40px;
		padding-bottom: 40px;
	}
	 .n--m{
		margin:0;
	}
	.n--pb{
		padding-bottom:0;
	}
	.upper{
		text-transform:uppercase;
	}
	.t--c{
		text-align:center;
	}
	.relative{
		position:relative;
	}
	.absolute{
		position:absolute;
	}
	.white{
		color:#fff;
	}
	.cover__banner{
		top:0;
		left:0;
		right:0;
		z-index:-1;
	}
	.cover__banner:after{
		content:"";
		position:absolute;
		left:0;
		right:0;
		top:0;
		bottom:0;
		background:rgba(0,0,0,.35);
		z-index:0;
	}

</style>
<div id="fakediv" class="d-none"></div>

<div class="zonegutter relative cover__wrap cover__wrap--blog" style="background-image: url('https://datax-talent.basecdn.net/cmcts/bg-center.png'); background-position: center; background-repeat: no-repeat; background-size: cover;">
   <div class="container relative t--c">
      <div class="small white" itemscope="" itemtype="http://schema.org/BreadcrumbList">
	  	<?php foreach ($breadcrumbs as $key=>$breadcrumb) { ?>
			<?php echo $key == 0 ? '': '→ '; ?>
         <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
               <span itemprop="name" class="white"><?php echo $breadcrumb['text']; ?></span>
               <meta itemprop="position" content="<?php echo $key+1; ?>">
            </a>
         </span>
		<?php } ?>
      </div>
      <div class="rowgutter n--pb">
         <h1 class="maintitle white n--m upper"><?php echo $this->language->get('text_move_jiss'); ?></h1>
      </div>
   </div>
</div>
<div class="bg-white">
<div class="container">
	<div class="col-12 catlist">
		<h1 class="blue n-mt rowgutter"><?php echo $heading_title; ?></h1>
		<?php foreach($news_categories as $cat){ ?>
			<a href="<?php echo $this->url->link('news/news_category', '&cat_id=' . $cat['news_category_id']); ?>"><?php echo $cat['name'];?></a>
		<?php } ?>
	</div>
	<div class="row clear">
		<div class="col-xs-12 col-sm-8 border-right">
		<?php if ($news) { ?>
			<div class="topnews">
			<?php foreach ($news as $key=>$news_item) { ?>
				<?php if($key==0) { ?>
					<div class="first-news">
						<div class="thumb_overlay relative">
							<a class="block" href="<?php echo $news_item['href']; ?>"><img src="<?php echo $news_item['thumb']; ?>" alt="<?php echo $news_item['name']; ?>" class="block" /></a>
						</div>

						<div class="text-container rowgutter">
							<h3 class="h3 title-section" itemprop="name"><a href="<?php echo $news_item['href']; ?>" itemprop="url"><?php echo $news_item['name']; ?></a></h3>
							<div class="date h5"><?php echo $news_item['date_added']; ?></div>
							<div class="des h5"><?php echo $news_item['short_description']; ?></div>
						</div>
					</div>
					<div class="line"></div>
				<?php } else { ?>
					<div class="news-item rowgutter card-shadow">
						<div class="clearfix">
							<div class="col-xs-12 col-sm-4">
								<div class="thumb_overlay relative">
									<a class="block" href="<?php echo $news_item['href']; ?>"><img src="<?php echo $news_item['thumb']; ?>" class="block" alt="<?php echo $news_item['name']; ?>" /></a>
								</div>
							</div>
							<div class="col-xs-12 col-sm-8">
								<div class="text-container">
									<h3 class="h3 title-section" itemprop="name"><a href="<?php echo $news_item['href']; ?>" itemprop="url"><?php echo $news_item['name']; ?></a></h3>
									<div class="date h5"><?php echo $news_item['date_added']; ?></div>
									<div class="des h5"><?php echo $news_item['short_description']; ?></div>
								</div>
							</div>
						</div>
					</div>
				<?php } ?>
			<?php } ?>
			</div>
		<?php } ?>
		<?php if ($news) { ?>
			<div class="pagination text-center">
				<?php echo $pagination; ?>
			</div>
		<?php } ?>
		<?php if (!$news) { ?>

		<?php } ?>
		</div>
		<div class="col-xs-12 col-sm-4">
			<?php echo $column_right; ?>
		</div>
	</div>
</div>

</div>
<?php echo $footer; ?>
