<?php echo $header; ?>
<div class="container password"><?php echo $content_top; ?>
  <h1 class="zonegutter no-margin"><?php echo $heading_title; ?></h1>
  <div class="sitemap-info clear row">
	<form action="account/password" method="POST" id="password-form">
	<?php if($this->customer->isLogged()){ ?>
    <div class="col-12 col-tablet-6">
		<h3>Đổi mật khẩu</h3>
		<input type="password" name="old_password" class="col-12 col-tablet-8 no-padding" value="<?php echo $old_password ? $old_password: ''; ?>" placeholder="Nhập mật khẩu hiện tại"/>
		<?php if($error_old_password){ ?><br /><div class="error col-12 no-padding"><?php echo $error_old_password; ?></div><?php } ?>
		<input type="password" name="password" class="col-12 col-tablet-8 no-padding" placeholder="Nhập mật khẩu mới"/>
		<?php if($error_password){ ?><br /><div class="error col-12 no-padding"><?php echo $error_password; ?></div><?php } ?>
		<input type="password" name="confirm" class="col-12 col-tablet-8 no-padding" placeholder="Nhập lại mật khẩu mới"/>
		<?php if($error_confirm){ ?><br /><div class="error col-12 no-padding"><?php echo $error_confirm; ?></div><?php } ?>
		<div class="col-12 no-padding"><a rel="nofollow" onclick="document.getElementById('password-form').submit();" class="button block round text-center">Xác nhận <i class="fa icon-paper-plane">&#xf1d8;</i></a></div>
	</div>
	<?php }else{ ?>
	<div class="col-12 col-tablet-6">
		<h3>Quên mật khẩu</h3>
		<input type="text" name="email" class="col-12 col-tablet-8 no-padding" placeholder="Nhập email của bạn"/>
		<?php if($error_email){ ?><br /><div class="error col-12 no-padding"><?php echo $error_email; ?></div><?php } ?>
		<div class="col-12 no-padding"><a rel="nofollow" onclick="document.getElementById('password-form').submit();" class="button block round text-center">Xác nhận <i class="fa icon-paper-plane">&#xf1d8;</i></a></div>
	</div>
	<?php } ?>
	</form>
  </div>
  <?php echo $content_bottom; ?>
</div>
<?php echo $footer; ?>