<!DOCTYPE html>
<html lang="en">
<head>
<!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home</title>

    <base href="<?php echo $base; ?>" />
    <meta name="description" content="Ethanol is an Agency and Personal Portfolio Template built with bootstrap 3.3.2. This is created for a cause to support my uncle's campaign. Go and Donate at - https://life.indiegogo.com/fundraisers/medical-support-for-a-filipino-overseas-worker--3/x/10058181">
    <meta name="keywords" content="portfolio, agency, bootstrap theme, mobile responsive, template, personal">
    <meta name="author" content="ThemeForces.Com">

<!-- Favicons
    ================================================== -->
    <!-- <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"> -->
    <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css"  href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome/css/font-awesome.css">

<!-- Nivo Lightbox
    ================================================== -->
    <link rel="stylesheet" href="css/nivo-lightbox.css" >
    <link rel="stylesheet" href="css/nivo_lightbox_themes/default/default.css">
    <link rel="stylesheet"  href="css/animate.css">

<!-- Slider
    ================================================== -->
    <link href="css/owl.carousel.min.css" rel="stylesheet" media="screen">
    <link href="css/owl.theme.css" rel="stylesheet" media="screen">

<!-- Stylesheet
    ================================================== -->
    <link rel="stylesheet" type="text/css"  href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/responsive.css">
    <link rel="stylesheet" type="text/css" href="css/hover-min.css">

<!-- Fonts
    ================================================== -->
    <link href="fonts/elegantIcon/elegantIcon.css" type="text/css" rel="stylesheet">
    <link href="fonts/WebFont/style.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" src="js/modernizr.custom.js"></script>
    <style>
    #oclock{
      position: absolute;
      right: 50px;
      top: 20px;
      border: 1px solid;
      padding: 6px 18px;
      border-radius: 5px;
      min-width: 70px;
      text-align: center;
    }
    .start {
    	z-index: 99;
        position: fixed;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, 0.5);
        opacity: 0;
        visibility: hidden;
        transform: scale(1.1);
        transition: visibility 0s linear 0.25s, opacity 0.25s 0s, transform 0.25s;
    }
    .scroll-modal {
      overflow-y: scroll;
      overflow-x: hidden;
      text-align: left;
      height: calc(100% - 105px);
    }
    .modal-content {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        background-color: white;
        padding: 1rem 1.5rem;
        width: 80%;
        border-radius: 0.5rem;
        height: 80vh;
        text-align: left;
    }
    .close-button {
      position: absolute;
      right: 5px;
      text-align: center;
      top: 5px;
      width: 25px;
      text-decoration: none;
      font-weight: bold;
      border-radius: 50%;
      box-shadow: 1px 1px 3px #000;
      opacity: 1;
      height: 25px;
      display: flex;
      align-items: center;
      justify-content: center;
    }
    .close-button:hover {
        background-color: darkgray;
    }
    .show-modal {
        opacity: 1;
        visibility: visible;
        transform: scale(1.0);
        transition: visibility 0s linear 0s, opacity 0.25s 0s, transform 0.25s;
    }
    .inputGroup label{
    	margin-left:10px;
    }
    .show-modal h3{
    	margin:0
    }
    .row--question{
    	clear: both;
    }
    </style>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>

    <div class="header_post">
        <div class="container">
            <div class="menu_training_post">
                <div class="logo_tnp">
                    <a href=""><img src="img/logo-3.png" ></a>
                </div>
                <ul>
                    <li><a href="index.html">Trang chủ</a></li>
                    <li><a href="account/info">Trang thành viên</a></li>
                    <li><a href="">Đăng xuất</a></li>
                </ul>
            </div>
        </div>
    </div>


    <section class="body_tnp">
        <div class="container">
            <div class="video_tnp">
                <iframe   width="100%" src="https://www.youtube.com/embed/<?php echo $group_info['youtube']; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
            <div class="content_tnp">
                <h4><?php echo $group_info['name']; ?></h4>
                <form id="form_contact" name="form_contact" method="post" action="">
                  <input type="hidden" name="group_id" value="<?php echo $group_id; ?>"/>
                <div class="tnp_box">
                  <?php echo html_entity_decode($group_info['description'], ENT_QUOTES, 'UTF-8'); ?>
                    <div class="btn_tnp col">

                        <a href="javascript:void(0)" class="btn__1 trigger">Trả lời câu hỏi</a>
                        <a href="account/info" class="btn__2">Danh sách bài kiểm tra</a>

                        <div id="openModal" class="start">

                          <div class="modal-content">
                            <a href="javascript:void(0)" title="Close" class="close-button">&times;</a>
                            <div id="oclock"></div>
                            <h4>Bạn trả lời câu hỏi bên dưới</h4>
                            <div class="scroll-modal" id="scroll-modal">
                              <?php foreach($quizs as $quiz){ ?>
                                <div class="col_radio_input_more">
                                  <span><?php echo $quiz['name']; ?></span>
                                  <?php foreach ($quiz['questions'] as $question) { ?>
                                  <input type="radio" value="<?php echo $question['question']; ?>" name="quest[<?php echo $question['quiz_id']; ?>]" id="question<?php echo $question['question_id']; ?>">
                                  <label for="question<?php echo $question['question_id']; ?>"><?php echo $question['question']; ?></label><br>
                                  <?php } ?>
                                </div>
                              <?php } ?>
                            </div>
                            <div class="finish_btn">
                                <a href="javascript:void(0)" onclick="submit_contact('form_contact')" class="finish">Hoàn thành</a>
                            </div>
                          </div>
                        </div>



                    <div id="popup1" class="overlayv2">
                        <div class="popup">
                            <h4>Danh sách kiểm tra của bạn</h4>
                            <a class="close2" href="#">&times;</a>
                            <div class="contentv2">
                             <p> Màu xanh là hoàn thành, màu đỏ chưa hoàn thành</p>
                             <div class="time_test">
                                 <p>Thời gian test: <span>30/09/2019</span> <time>04:30pm</time></p>
                                 <ul class="result">
                                    <li><p>% trả lời đúng: <span>99</span></p></li>
                                    <li><p>Tổng số câu hỏi: <span>8</span></p></li>
                                    <li><strong>Không đạt</strong></li>
                                </ul>
                              </div>

                              <div class="time_test">
                                 <p>Thời gian test: <span>30/09/2019</span> <time>04:30pm</time></p>
                                 <ul class="result">
                                    <li><p>% trả lời đúng: <span>99</span></p></li>
                                    <li><p>Tổng số câu hỏi: <span>8</span></p></li>
                                    <li><strong>Không đạt</strong></li>
                                </ul>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>

        <div class="btn_prev">
            <a href=""><span class="lnr lnr-chevron-left"></span> xem video trước</a>
            <a href=""> xem video tiếp<span class="lnr lnr-chevron-right"></span> </a>
        </div>
    </div>

    <div class="list_videos">
        <h4 class="title_videosList">Danh sách videos</h4>
        <div class="list_videos_slide owl-carousel">
            <div class="videos_item">
                <a href=""><img src="img/s2.jpg" width="100%"></a>
                <h4><a href="">Quản lý tài chính<span>Videos 40</span></a></h4>
            </div>
            <div class="videos_item">
                <a href=""><img src="img/s1.jpg" width="100%"></a>
                <h4><a href="">Quản lý tài chính<span>Videos 41</span></a></h4>
            </div>
            <div class="videos_item">
                <a href=""><img src="img/img-bn.jpg" width="100%"></a>
                <h4><a href="">Quản lý tài chính<span>Videos 42</span></a></h4>
            </div>
            <div class="videos_item">
                <a href=""><img src="img/s3.jpg" width="100%"></a>
                <h4><a href="">Quản lý tài chính<span>Videos 43</span></a></h4>
            </div>
        </div>
    </div>
</div>
</section>





<!-- scroll-BacktoTop -->
<a href="#" class="zoa-btn scroll_top"><img src="img/left_32.png"></a>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.1.11.1.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script type="text/javascript" src="js/bootstrap.js"></script>
<script type="text/javascript" src="js/owl.carousel.min.js"></script><!-- Owl Carousel Plugin -->
<script type="text/javascript" src="js/SmoothScroll.js"></script>
<!-- Parallax Effects -->
<!-- <script type="text/javascript" src="js/wow.min.js"></script> -->
<script type="text/javascript" src="js/skrollr.js"></script>
<script type="text/javascript" src="js/imagesloaded.js"></script>
<!-- Portfolio Filter -->
<script type="text/javascript" src="js/jquery.isotope.js"></script>
<!-- LightBox Nivo -->
<script type="text/javascript" src="js/nivo-lightbox.min.js"></script>
<!-- Contact page-->
<script type="text/javascript" src="js/jqBootstrapValidation.js"></script>
<!-- <script type="text/javascript" src="js/contact.js"></script> -->
<script type="text/javascript" src="js/wow.min.js"></script>
<!-- Customer-chat -->
<!-- <script src='http://code.jquery.com/jquery-1.11.3.min.js'></script>
    <script src="js/index.js"></script> -->
<!-- Javascripts
    ================================================== -->
<script>
    if($(window).width()>1199){
        new WOW().init();
    }
    var modal = document.querySelector(".start");
    var trigger = document.querySelector(".trigger");
    var start = document.querySelector(".btn__1");
    var closeButton = document.querySelector(".close-button");

    function startCount() {
       console.log('hungnv');
       // document.getElementById('scroll-modal').style.display = 'none';
      // document.getElementById('content_test').style.display = 'block';
      var now1 = new Date().getTime();
      var deadline = now1 + <?php echo $time_test; ?>*60*1000;
      var x = setInterval(function() {
        var now = new Date().getTime();
        var t = deadline - now;
        var minutes = Math.floor((t % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((t % (1000 * 60)) / 1000);
        document.getElementById("oclock").innerHTML = minutes + ":" + seconds;
        if (t < 0) {
            clearInterval(x);
            document.getElementById("oclock").innerHTML = "Thời gian làm bài đã hết. Vui lòng gửi bài";
            document.getElementById('scroll-modal').style.display = 'none';
        }
      }, 1000);
    }

    function toggleModal() {
       modal.classList.toggle("show-modal");
    }

    function windowOnClick(event) {
       if (event.target === modal) {
           toggleModal();
       }
    }

    trigger.addEventListener("click", toggleModal);
    start.addEventListener("click", startCount);
    closeButton.addEventListener("click", toggleModal);
    window.addEventListener("click", windowOnClick);

    function submit_contact(id){
  	   var request = new XMLHttpRequest();
    	request.open('POST', 'index.php?route=account/test/submit', false);
    	var formData = new FormData(document.getElementById(id));
    	request.send(formData);
    	// console.log(request);
    	if (request.status == 200) {
    		var json = JSON.parse(request.responseText);
    		console.log('db', json, json.error.length);
    		// var listFields = ['name', 'number', 'role', 'company', 'address', 'knowfrom', 'note'];
    		// listFields.forEach((field) => {
    		// 	if(typeof json.error[`${field}`] != 'undefined'){
    		// 		document.getElementById(`error_${field}`).innerHTML = '<div class="error">'+ json.error[`${field}`] +'</div>';
    		// 		document.getElementById(`${field}`).focus();
    		// 	} else {
        //     var modal = document.querySelector(".modal");
        //     modal.classList.toggle("show-modal");
        //     document.getElementById('send_answer').style.display = 'none';
        //     document.getElementById('oclock').style.display = 'none';
    		// 		document.getElementById(`error_${field}`).innerHTML = '';
    		// 	}
    		// });

    		if(json['error'].length == 0){
    			document.getElementById('scroll-modal').innerHTML = '<h3>Cám ơn bạn đã làm bài kiểm tra. Chúng tôi sẽ gửi kết quả sớm nhất tới bạn.</h3>';
          window.setTimeout( function(){
            var modal = document.querySelector(".start");
            modal.classList.toggle("show-modal");
          }, 2 );
    		}
    	} else {
    		alert('Đăng ký bị lỗi. Vui lòng gọi tới hotline để được hỗ trợ tốt nhất');
    		console.log('Error: ' + request.statusText)
    	}
     }
</script>

</body>
</html>
