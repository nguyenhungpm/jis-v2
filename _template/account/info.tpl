<!DOCTYPE html>
<html lang="en">
<head>
<!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>User</title>
    <base href="<?php echo $base; ?>" />
    <meta name="description" content="Ethanol is an Agency and Personal Portfolio Template built with bootstrap 3.3.2. This is created for a cause to support my uncle's campaign. Go and Donate at - https://life.indiegogo.com/fundraisers/medical-support-for-a-filipino-overseas-worker--3/x/10058181">
    <meta name="keywords" content="portfolio, agency, bootstrap theme, mobile responsive, template, personal">
    <meta name="author" content="ThemeForces.Com">

<!-- Favicons
    ================================================== -->
    <!-- <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"> -->
    <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css"  href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome/css/font-awesome.css">

<!-- Nivo Lightbox
    ================================================== -->
    <link rel="stylesheet" href="css/nivo-lightbox.css" >
    <link rel="stylesheet" href="css/nivo_lightbox_themes/default/default.css">
    <link rel="stylesheet"  href="css/animate.css">

<!-- Slider
    ================================================== -->
    <link href="css/owl.carousel.min.css" rel="stylesheet" media="screen">
    <link href="css/owl.theme.css" rel="stylesheet" media="screen">

<!-- Stylesheet
    ================================================== -->
    <link rel="stylesheet" type="text/css"  href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/responsive.css">

<!-- Fonts
    ================================================== -->
    <link href="fonts/elegantIcon/elegantIcon.css" type="text/css" rel="stylesheet">
    <link href="fonts/WebFont/style.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" src="js/modernizr.custom.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>
    <div class="menu-user">
        <nav id="menu" role="navigation">
            <div class="searchUser">
                <form>
                    <input type="text" name="q" id="search-terms" placeholder="Search...">
                    <button type="submit" name="submit" value="Go" class="search-icon"><i class="fa fa-fw fa-search"></i></button>
                </form>
            </div>
            <div class="top-menu">
                <h4 class="title-mn-user">Phần chia sẻ </h4>
                <ul>
                    <li>
                        <ul  class="fixTopmenu">
                            <li><a href="#" id="flip"><span class="icon_documents_alt"></span>Blog chia sẻ<span class="fa fa-plus right-mn"></span></a>
                                <ul id="panel">
                                    <li><a href="">Các danh mục</a></li>
                                    <li><a href="">Các bài viết</a></li>
                                    <li><a href="">Các chiến dịch</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li><a href="#"><span class="icon_wallet"></span>Các chiến dịch</a></li>
                </ul>
            </div>
            <div class="top-menu">
                <h4 class="title-mn-user">Hệ thống đào tạo</h4>
                <ul>
                    <ul  class="fixTopmenu">
                            <li><a href="javascript:void(0)" id="flip-2"><span class="icon_documents_alt"></span>Trang đào tạo<span class="fa fa-plus right-mn"></span></a>
                                <ul id="panel-2">
                                  <?php foreach($groups as $group){ ?>
                                    <li><a href="<?php echo $group['href']; ?>"><?php echo $group['name']; ?></a></li>
                                  <?php } ?>
                                </ul>
                            </li>
                        </ul></li>
                    <li><a href="#"><span class="icon_wallet"></span>Các chiến dịch</a></li>
                    <li><a href="#"><span class="icon_documents_alt"></span>Blog chia sẻ</a></li>
                    <li><a href="#"><span class="icon_wallet"></span>Các chiến dịch</a></li>
                </ul>
            </div>
            <div class="top-menu">
                <h4 class="title-mn-user">Tài khoản</h4>
                <ul>
                    <li><a href="#"><span class="fa fa-user-o"></span>Nâng cấp tài khoản</a></li>
                    <li><a href="#"><span class="icon_documents_alt"></span>Xác minh tài khoản</a></li>
                    <li><a href="#"><span class="icon_key_alt"></span>Đổi mật khẩu</a></li>
                    <li><a href="#"><span class="icon_wallet"></span>Tổng tuyến dưới</a></li>
                </ul>
            </div>

            <div class="top-menu">
                <h4 class="title-mn-user">Giao dịch</h4>
                <ul>
                    <li><a href="#"><span class="lnr lnr-line-spacing"></span>Giao dịch trả thưởng</a></li>
                    <li><a href="#"><span class="lnr lnr-diamond"></span>Rút tiền</a></li>
                    <li><a href="#"><span class="lnr lnr-question-circle"></span>Hướng dẫn sử dụng</a></li>

                </ul>
            </div>
        </nav>
    </div>
    <div id="menu-overlay"></div>
    <div class="page-wrap hide">
        <button id="menu-toggle"></button>
    </div>
    <div class="logo-menuUser">
        <div class="logo">
            <a href="#"><img src="img/logo.png" style="width:65px"></a>
            <a class="backTohome" href=""><span class="arrow_back"></span>Trang chủ</a>
        </div>
        <div class="items-rightUser">
            <div class="dropdown drop-user">
                <button  type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img src="img/avt.jpeg">
                </button>
                <div class="dropdown-menu drop-items" aria-labelledby="dropdownMenuButton">
                    <div class="flex-account">
                        <div class="avtUser">
                            <a href=""><img src="img/avt.jpeg" style="width:100%"></a>
                        </div>
                        <div class="itemsUser">
                            <ul>
                                <li><a class="yourname" href=""><?php echo $this->customer->getLastName(); ?></a></li>
                                <li><a href="#"><span class="fa fa-user"></span><?php echo $this->customer->getEmail(); ?></a></li>
                                <li><a href="#"><span class=" icon_question"></span>Trợ giúp</a></li>
                                <li><a href="#" onclick="logout('account/info')"><span class="fa fa-sign-in"></span>Đăng xuất</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="dropdown setting-drop hide">
              <button class="lnr lnr-cog" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              </button>
              <div class="dropdown-menu setting-drop-show" aria-labelledby="dropdownMenu2">
                <button  type="button">Action</button>
                <button  type="button">Another action</button>
                <button  type="button">Something else here</button>
            </div>
        </div>
        <div class="dropdown hide">
            <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="fa fa-bell"></span>
            </a>
            <div class="dropdown-menu notification-drop" aria-labelledby="dropdownMenuLink">
                <a class="dropdown-item" href="#">Action</a><br>
                <a class="dropdown-item" href="#">Another action</a><br>
                <a class="dropdown-item" href="#">Something else here</a>
            </div>
        </div>
    </div>
</div>

<div class="wrap-ver2" <?php echo !$this->customer->isLogged() ? 'style="background:#f7f7f7"':''; ?> >
    <div class="contentUser">
          <?php if($this->customer->isLogged()){ ?>
          <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="item-leftUser">
                        <a href="" class="avt-1"><img src="img/avt.jpeg" > <span class="icon_camera_alt"></span></a>
                    </div>
                    <div class="ctnUser">
                        <h4 class="yourname-1"><?php echo $this->customer->getLastName(); ?> <span>Web Developer</span></h4>
                        <ul class="ul">
                            <li><a href=""><span class="fa fa-envelope"></span><?php echo $this->customer->getEmail(); ?></a></li>
                            <li><a href=""><span class="fa fa-facebook-official"></span>nguyenvana123@gmail.com</a></li>
                            <li><a href=""><span class="social_instagram"></span>nguyenvana123</a></li>
                            <li><a href=""><span class="icon_phone"></span>+84 (4) 123 456 789</a></li>
                            <li><a href=""><span class="icon_mobile"></span>+84 (4) 123 456 789</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-8">
                    <div class="bgRight">
                        <h4 class="titleUser-right">Trang thành viên <span>trang tổng hợp thông tin thành viên.</span></h4>
                        <div class="itRight-1">
                            <div class="img-avt">
                                <a href=""><img src="img/avt.jpeg" >
                                </div>
                                <div class="date-time">
                                    <h4><?php echo $this->customer->getLastName(); ?></h4>
                                    <a href="" class="fix-date"><span class="icon_clock_alt"></span>15:45pm</a>
                                    <a href=""><span class="fa fa-calendar-o"></span>20/06/2019</a>
                                </div>
                            </div>
                            <div class="row">
                                <?php foreach($groups as $group){ ?>
                                  <div class="col-md-6">
                                    <div class="downline bgr-1">
                                        <h4><a href="<?php echo $group['href']; ?>"><?php echo $group['name']; ?></a></h4>
                                        <a href="">999</a>
                                    </div>
                                  </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
          <?php }else{ ?>
            <?php echo $account; ?>
          <?php } ?>


        <!-- scroll-BacktoTop -->
        <a href="#" class="zoa-btn scroll_top"><img src="img/left_32.png"></a>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery.1.11.1.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script type="text/javascript" src="js/bootstrap.js"></script>

        <script type="text/javascript" src="js/owl.carousel.js"></script><!-- Owl Carousel Plugin -->

        <script type="text/javascript" src="js/SmoothScroll.js"></script>

        <!-- Parallax Effects -->
        <script type="text/javascript" src="js/skrollr.js"></script>
        <script type="text/javascript" src="js/imagesloaded.js"></script>

        <!-- Portfolio Filter -->
        <script type="text/javascript" src="js/jquery.isotope.js"></script>

        <!-- LightBox Nivo -->
        <script type="text/javascript" src="js/nivo-lightbox.min.js"></script>

        <!-- Contact page-->
        <script type="text/javascript" src="js/jqBootstrapValidation.js"></script>
        <script type="text/javascript" src="js/contact.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<!-- Javascripts
    ================================================== -->
    <script type="text/javascript" src="js/main.js"></script>

    <script type="text/javascript">
//Scroll_backtoTop
$(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
        $('.scroll_top').fadeIn();
    } else {
        $('.scroll_top').fadeOut();
    }
});
$('.scroll_top').on('click', function() {
    $("html, body").animate({
        scrollTop: 0
    }, 1000);
    return false;
});
$(window).scroll(function() {
    var scroll = $(window).scrollTop();
    if (scroll > 500) {
        $(".intro").hide();
        $(".scroll_down").hide();
    }
});
  function logout(url){
    var xmlhttp= window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == XMLHttpRequest.DONE) {
            if (xmlhttp.status == 200) {
                window.location.href = url;
            } else {
                console.log('Error: ' + xmlhttp.statusText);
            }
        }
    }
    xmlhttp.open("POST","index.php?route=account/logout",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlhttp.send("");
  }
</script>
</body>
</html>
