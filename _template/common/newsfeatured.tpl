<?php if ($position == 'content_top'  or $position == 'content_bottom') { ?>
<h2 class="hide title_under text-center"><?php echo $heading_title; ?></h2>
<div class="clear zonegutter col-xs-12">
<?php $k=0;foreach ($newss as $news) { ?>
	<div class="col-xs-12 col-sm-4 n--p bg-white">
		<?php if($k!=1){ ?>
		<a class="vnews" href="<?php echo $news['href']; ?>"><img class="block" alt="<?php echo $news['name']; ?>" src="<?php echo $news['thumb']; ?>"></a>
		<div class="col-xs-12 desc">
			<h3 class="n-mb"><a href="<?php echo $news['href']; ?>"><?php echo $news['name']; ?></a></h3>
			<div class="gutter n-mt grey small uppercase"><?php echo $news['date_added']; ?> // <span class="strong"><?php echo $news['newscat'];?></span>
			</div>
			<?php echo $news['short_description'];?>
		</div>
		<?php }else{ ?>
		<div class="col-xs-12 desc">
			<h3 class="n-mb"><a href="<?php echo $news['href']; ?>"><?php echo $news['name']; ?></a></h3>
			<div class="gutter n-mt grey small uppercase"><?php echo $news['date_added']; ?> // <span class="strong"><?php echo $news['newscat'];?></span>
			</div>	
			<?php echo $news['short_description'];?>
		</div>
		<a href="<?php echo $news['href']; ?>"><img class="block" alt="<?php echo $news['name']; ?>" src="<?php echo $news['thumb']; ?>"></a>
		<?php } ?>
	</div>
<?php $k++;} ?>
</div>
<?php }else{ ?>
<div class="othernews">
	<h3 class="h4 strong no-margin rowgutter em"><?php echo $heading_title; ?></h3>
	<?php foreach ($newss as $news) { ?>
		<div class="clear item">
			<a class="col-3 no-padding" href="<?php echo $news['href'];?>"><img class="block" src="<?php echo $news['thumb'];?>" alt="<?php echo $news['name'];?>"/></a>
			<a class="col-9 color-black no-padding-right" href="<?php echo $news['href'];?>"><?php echo $news['name'];?></a>
		</div>
	<?php } ?>
</div> 
<?php } ?>