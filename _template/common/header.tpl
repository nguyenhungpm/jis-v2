<!DOCTYPE html>
<html lang="vi-VN" itemscope itemtype="http://schema.org/website" class="no-js">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<base href="<?php echo $base; ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no,maximum-scale=1, user-scalable=no">
    <title><?php echo $title; ?></title>
    
    <?php if (isset($google_verify)) { ?>
        <?php echo $google_verify; ?>
    <?php } ?>
    <?php if (isset($script_header)) { ?>
        <?php echo $script_header; ?>
    <?php } ?>
    <?php foreach ($metanames as $metaname) { ?>
        <meta name="<?php echo $metaname['name']; ?>" content="<?php echo $metaname['content']; ?>" />
    <?php } ?>
    <?php foreach ($metapros as $metapro) { ?>
        <meta property="<?php echo $metapro['property']; ?>" content="<?php echo $metapro['content']; ?>" />
    <?php } ?>
    <?php foreach ($links as $link) { ?>
        <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
    <?php } ?>
    <?php if ($icon) { ?>
        <link href="<?php echo $icon; ?>" rel="icon" />
    <?php } ?>
    <link href="wwwroot/templates/website/release/css/index-page.min.css?v=5" rel="stylesheet" />
    <link href="wwwroot/templates/website/release/css/extra.css" rel="stylesheet" />
    <?php foreach ($styles as $style) { ?>
        <link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
    <?php } ?>
</head>

<body class="<?php echo $class; ?>">
    <?php if (isset($script_body)) { ?>
        <?php echo $script_body; ?>
    <?php } ?>
    <nav id="nav-mobile" class="visible-xs">
        <div class="nav-header bg-primary">
            <div id="menu-btn" class=""> <span class="bar1"></span>
                <span class="bar2"></span>
                <span class="bar3"></span>
            </div>
            <div class="logo-container">
                <a href="" class="">
                    <img src="static/img/logo-mobile.png">
                    <!-- <img src="<?php echo $logo; ?>"> -->
                </a>
            </div>
            <a href="javascript:" id="search-btn" class="search-btn-action"> <i class="demo-icon ecs-search-1"></i>
            </a>
        </div>
        <div class="nav-body bg-white visible-xs">
            <nav class="nav-content">
                <?php echo $top_menu; ?>
            </nav>
            <?php echo $language; ?>
        </div>
        <div class="nav-footer bg-white visible-xs pre-search-container">
            <form action="tim-kiem" method="get" autocomplete="off" novalidate="novalidate" class="search-container flex-center">
                <input type="text" placeholder="Tìm kiếm..." class="form-control" name="keyword" oninput="makekeyword(this,'#pre-search-mobile')" data-id="#pre-search-mobile">
                <button type="submit" class="btn"><i class="demo-icon ecs-search-1"></i>
                </button> <a href="javascript:" class="search-btn-action"><i class="demo-icon ecs-cross-out"></i></a>
            </form>
            <div class="text-container pre-search" id="pre-search-mobile"></div>
        </div>
    </nav>
    <header id="main-search" class="hidden-xs site-header style-4" role="banner">
        <div class="top-panel invert">
            <div class="container">
                <div class="top-panel__container">
                    <div class="contact-block contact-block--header_top_panel">
                        <div class="contact-block__inner">
                            <div class="contact-block__item contact-block__item--icon">
                                <?php echo $language; ?>
                            </div>
                            &nbsp;&nbsp;|&nbsp;&nbsp;
                            <div class="contact-block__item contact-block__item--icon">
                                <div class="social-list social-list--header social-list--icon">
                                    <ul id="social-list-1" class="social-list__items inline-list">
                                        <?php if($this->config->get('config_facebook')) { ?>
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom"><a target="_blank" href="<?php echo $this->config->get('config_facebook'); ?>"><span class="screen-reader-text">Facebook</span></a></li>
                                        <?php } ?>
                                        <?php if($this->config->get('config_twitter')) { ?>
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom"><a target="_blank" href="<?php echo $this->config->get('config_twitter'); ?>"><span class="screen-reader-text">Twitter</span></a></li>
                                        <?php } ?>
                                        <?php if($this->config->get('config_youtube')) { ?>
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom"><a target="_blank" href="<?php echo $this->config->get('config_youtube'); ?>"><span class="screen-reader-text">Youtube</span></a></li>
                                        <?php } ?>
                                        <?php if($this->config->get('config_pinterest')) { ?>
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom"><a target="_blank" href="<?php echo $this->config->get('config_pinterest'); ?>"><span class="screen-reader-text">Instagram</span></a></li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                            <span class="col">
                                &nbsp;&nbsp;|&nbsp;&nbsp;
                            </span>
                            <div class="contact-block__item contact-block__item--icon">
                                <div class="contact-block__value-wrap">
                                    <span class="contact-block__text">
                                        <?php if(!empty($customer_id)) { ?>
                                            <a href="information/document"><?php echo $this->language->get('text_internal_zone'); ?></a> (<a href="/account/logout" ><?php echo $this->language->get('text_logout'); ?></a>)
                                        <?php } else { ?>
                                            <a href="javascript:;" data-toggle="modal" data-target="#modalContact"><?php echo $this->language->get('text_internal_zone'); ?></a>
                                        <?php } ?>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="top-panel__wrap-items">
                        <div class="pre-search-container">
                            <form action="/tim-kiem" method="get" autocomplete="off" novalidate="novalidate" class="search-container flex-center">
                                <button class="btn"><i class="demo-icon ecs-search-2"></i></button>
                                <input type="text" class="form-control" placeholder="Bạn muốn tìm gì?" name="keyword" oninput="makekeyword(this,'#pre-search-desktop')" data-id="#pre-search-desktop">
                            </form>
                            <div class="pre-search" id="pre-search-desktop">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- .top-panel -->
        <div class="header-container" id="header-menu">
            <div class="header-container_wrap container">
                <div class="header-container__flex-wrap">
                    <div class="header-container__flex">
                        <div class="site-branding">
                            <div class="site-logo site-logo--image">
                                <a class="site-logo__link" href="." rel="home">
                                    <img src="<?php echo $logo; ?>" alt="Finscore" class="site-link__img" style="height:78px;" id="logo">
                                    <i class="demo-icon ecs-home" style="display: none;" id="icon-home"></i>
                                </a>
                            </div>
                        </div>
                        <div class="header-nav-wrapper">

                            <nav id="site-navigation" class="main-navigation main-menu-style-1" role="navigation">
                                <div class="jet-menu-container">
                                    <div class="jet-menu-inner">
                                        <?php echo $top_menu?>
                                    </div>
                                </div>
                            </nav>
                            <!-- #site-navigation -->
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- .header-container -->
    </header>
