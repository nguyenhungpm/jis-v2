<style>
	.logo-container img{
		height: 90px;
	}
</style>
<div class="wrap_list_buttons_sticky">
	<ul class="list_buttons_sticky">
		<li>
			<a class="icon-btn" href="information/visit" title="" target="_blank" id="link-noi-bo">
				<img width="347" height="346" src="static/icon-visit.png" alt="icon button">
			</a>
		</li>
		<li>
			<a class="icon-btn" href="information/contact" title="" target="_blank" id="link-noi-bo">
				<img alt="icon button" src="static/icon-register.png">
			</a>
		</li>
	</ul>
</div>
<a target="_blank" rel="noopener noreferrer" class="support__zalo fixed" href="https://zalo.me/<?php echo $this->config->get('config_zalo'); ?>">Zalo</a>
<a target="_blank" rel="noopener noreferrer" class="support__facebook fixed" href="https://m.me/1608793659376772">Facebook</a>
<div id="phonering-alo-phoneIcon" class="fixed phonering-alo-phone phonering-alo-green phonering-alo-show">
    <div class="phonering-alo-ph-img-circle"><a class="pps-btn-img " title="Liên hệ" href="tel:<?php echo $this->config->get('config_hotline'); ?>"><img src="static/icon-call.png" alt="Liên hệ" width="32" class="img-responsive"></a></div>
</div>

<div id="scrollup-container" class="text-center p-4">
        <a href="javascript:" id="scrollup"> <i class="demo-icon ecs-up-open-big" aria-hidden="true"></i></a>
        <a href="" id="backtohome"> <i class="demo-icon ecs-up-open-big" aria-hidden="true"></i>
            <span class="text-uppercase e-block"><?php echo $this->language->get('text_backtohome'); ?></span>
        </a>
    </div>
	<div class="before-footer">
		<div class="footer-header container flex-center flex-between unclearfix p-2 bg-white">
			<div class="logo-container">
				<a href="">
					<img itemprop="logo" src="<?php echo $footer_logo; ?>" alt="Logo footer">
				</a>
			</div>
			<div class="logo-container" style="float: right;">
				<a href="">
					<img itemprop="logo" src="static/logo2.jpg" alt="Logo footer">
				</a>
			</div>
		</div>
	</div>
    <footer class="bg-primary">
        <div class="footer-body p-4 text-white">
            <div class="container">
                <div class="row clearfix">
					<div class="col col-sm-6 col-md-5" itemscope itemtype="http://schema.org/LocalBusiness">
                        <h2 class="h5 title-section text-bold text-uppercase"><?php echo $this->language->get('text_contact_information'); ?></h2>
                        <p><?php echo $this->language->get('text_telephone'); ?>: <a href="tel:<?php echo $telephone; ?>"><?php echo $telephone; ?></a> / <a href="tel:<?php echo $hotline; ?>"><?php echo $hotline; ?></a></p>
                        <p><?php echo $this->language->get('text_address'); ?>: <a target="_blank" rel="nofollow"><?php echo $address; ?></a></p>
                        <p><?php echo $this->language->get('text_email'); ?>: <?php echo join(", ", $email); ?></p>
                    </div>
                    <div class="col col-sm-6 col-md-4">
						<iframe style="height: 120px; width: 100%" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3725.3042741366617!2d105.76462471533169!3d20.980436794801225!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3134533ae99b525f%3A0x8f71715cd31ebe69!2sJapanese%20International%20School%20(JIS)!5e0!3m2!1sen!2s!4v1676371126342!5m2!1sen!2s" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
					</div>
                    <div class="col col-sm-6 col-md-3" itemscope itemtype="http://schema.org/LocalBusiness">
						<div class="social-container" style="float: right;"> 
						<?php if($this->config->get('config_facebook')) { ?>
							<a href="<?php echo $this->config->get('config_facebook'); ?>" target="_blank" rel="nofollow"><i class="demo-icon ecs-facebook"></i></a>
						<?php } ?>
						<?php if($this->config->get('config_youtube')) { ?>
							<a href="<?php echo $this->config->get('config_youtube'); ?>" target="_blank" rel="nofollow"><i class="demo-icon ecs-youtube-play"></i></a>
						<?php } ?>
						<?php if($this->config->get('config_pinterest')) { ?>
							<a href="<?php echo $this->config->get('config_pinterest'); ?>" target="_blank" rel="nofollow"><i class="demo-icon ecs-instagram"></i></a>
						<?php } ?>
						</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-footer container flex-center flex-between unclearfix p-1"> 
            <a href="https://ecsgroup.com.vn/">&#169; 2022 | <?php echo $this->language->get('text_copyright'); ?></a>
            <h3 class="h4 text-white title-section">Japanese International School</h3>
        </div>
    </footer>
    <div id="loading">
        <div id="loading-content">
            <svg version="1.1" class="svg-loader" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100px" height="100px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">
                <path opacity="0.2" d="M20.201,5.169c-8.254,0-14.946,6.692-14.946,14.946c0,8.255,6.692,14.946,14.946,14.946 s14.946-6.691,14.946-14.946C35.146,11.861,28.455,5.169,20.201,5.169z M20.201,31.749c-6.425,0-11.634-5.208-11.634-11.634 c0-6.425,5.209-11.634,11.634-11.634c6.425,0,11.633,5.209,11.633,11.634C31.834,26.541,26.626,31.749,20.201,31.749z"></path>
                <path d="M26.013,10.047l1.654-2.866c-2.198-1.272-4.743-2.012-7.466-2.012h0v3.312h0 C22.32,8.481,24.301,9.057,26.013,10.047z">
                    <animateTransform attributeType="xml" attributeName="transform" type="rotate" from="0 20 20" to="360 20 20" dur="0.5s" repeatCount="indefinite"></animateTransform>
                </path>
            </svg>
        </div>
    </div>
	<div class="modal fade" id="modalContact" tabindex="-1" role="dialog" aria-labelledby="modalContactTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
		<form action="/account/login" method="POST">
		<div class="modal-content position-relative py-3">
			<div class="modal-header no-border">
				<h5 class="modal-title text-center fw-bold m-auto" id="modalContactLongTitle"><?php echo $this->language->get('text_pls_registry'); ?></h5>
				<button type="button" class="close position-absolute box-shadow" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body m-auto">
				<div class="clearfix row align-items-center mb-3">
					<div class="col-12 col-lg-3">
						<strong><?php echo $this->language->get('text_username'); ?> <span class="color-red">*</span></strong>
					</div>
					<div class="col-12 col-lg-9">
						<input type="text" id="email" name="email" class="form-control no-radius">
					</div>
				</div>
				<div class="clearfix row align-items-center mb-3 rowgutter">
					<div class="col-12 col-lg-3">
						<strong><?php echo $this->language->get('text_password'); ?> <span class="color-red">*</span></strong>
					</div>
					<div class="col-12 col-lg-9">
						<input type="password" id="password" name="password" class="form-control no-radius">
					</div>
				</div>
				<div class="clearfix row align-items-center" >
					<div class="col-12 col-lg-12" id="error_login" style="color: red"></div>
				</div>
			</div>
			<div class="modal-footer no-border">
				<button type="button" class="btn btn-primary bg-red no-border no-radius px-5" onclick="$('#modalContact').modal('toggle');$('#modalRegistry').modal('toggle');"><?php echo $this->language->get('text_registry'); ?></button>
				<button type="button" class="btn btn-primary bg-red no-border no-radius px-5" onclick="login()"><?php echo $this->language->get('text_login'); ?></button>
			</div>
		</div>
		</form>
		</div>
	</div>
	<div class="modal fade" id="modalRegistry" tabindex="-1" role="dialog" aria-labelledby="modalRegistryTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
		<form name="register_form" id="register_form">
		<div class="modal-content position-relative py-3">
			<div class="modal-header no-border">
				<h5 class="modal-title text-center fw-bold m-auto" id="modalRegistryLongTitle"><?php echo $this->language->get('text_pls_login'); ?></h5>
				<button type="button" class="close position-absolute box-shadow" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body m-auto" id="body-registry">
				<div class="clearfix row align-items-center mb-3">
					<div class="col-12 col-lg-4">
						<strong><?php echo $this->language->get('text_email'); ?> <span class="color-red">*</span></strong>
					</div>
					<div class="col-12 col-lg-8">
						<input type="text" id="reg_email" name="reg_email" class="form-control no-radius">
					</div>
				</div>
				<div class="clearfix row align-items-center mb-3 rowgutter">
					<div class="col-12 col-lg-4">
						<strong><?php echo $this->language->get('text_password'); ?> <span class="color-red">*</span></strong>
					</div>
					<div class="col-12 col-lg-8">
						<input type="password" id="reg_password" name="reg_password" class="form-control no-radius">
					</div>
				</div>
				<div class="clearfix row align-items-center mb-3">
					<div class="col-12 col-lg-4">
						<strong><?php echo $this->language->get('text_confirm_password'); ?> <span class="color-red">*</span></strong>
					</div>
					<div class="col-12 col-lg-8">
						<input type="password" id="reg_confirm" name="reg_confirm" class="form-control no-radius">
					</div>
				</div>
				<div class="clearfix row align-items-center mb-3 rowgutter">
					<div class="col-12 col-lg-4">
						<strong><?php echo $this->language->get('text_lastname'); ?> <span class="color-red">*</span></strong>
					</div>
					<div class="col-12 col-lg-8">
						<input type="text" id="reg_lastname" name="reg_lastname" class="form-control no-radius">
					</div>
				</div>
				<div class="clearfix row align-items-center mb-3">
					<div class="col-12 col-lg-4">
						<strong><?php echo $this->language->get('text_telephone'); ?> <span class="color-red">*</span></strong>
					</div>
					<div class="col-12 col-lg-8">
						<input type="text" id="reg_telephone" name="reg_telephone" class="form-control no-radius">
					</div>
				</div>
				<div class="clearfix row align-items-center" >
					<div class="col-12 col-lg-12" id="error_registry" style="color: red"></div>
				</div>
			</div>
			<div class="modal-footer no-border" id="btn-registry">
				<button type="button" class="btn btn-primary bg-red no-border no-radius px-5" onclick="registry()"><?php echo $this->language->get('text_registry'); ?></button>
			</div>
		</div>
		</form>
		</div>
	</div>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="wwwroot/templates/website/release/scripts/index-page.min.js" defer="defer" type="text/javascript"></script>
    <script>
        $(document).ready(function() {
			$(".nav li[data-id='trang-chu']").addClass("active");
		});

		function registry() {
			var email = document.getElementById('reg_email').value;
			var password = document.getElementById('reg_password').value;
			var confirm = document.getElementById('reg_confirm').value;
			var lastname = document.getElementById('reg_lastname').value;
			var telephone = document.getElementById('reg_telephone').value;
			// $('#modalContact').modal('toggle');
			$('#loading').show()
			request('post', '/account/register', {email, password, confirm, lastname, telephone}, rs => {
				console.log('hungnv2 -> rs:', rs);
				if (rs.length == 0) {
					// location.href = "information/document";		
					$("#body-registry").html('<span>Đăng ký thành công</span>');
					document.getElementById("btn-registry").style.display = 'none';
				} else {
					document.getElementById('error_registry').innerHTML = rs.join('<br />')
				}
				$('#loading').hide();
			})
		}
		function login() {
			var email = document.getElementById('email').value;
			var password = document.getElementById('password').value;
			// $('#modalContact').modal('toggle');
			$('#loading').show()
			request('post', '/account/login', {'email': email, 'password': password}, rs => {
				console.log('hungnv2 -> rs:', rs);
				if (rs.length == 0) {
					location.href = "information/document";
				} else {
					document.getElementById('error_login').innerHTML = rs.join('<br />')
				}
				$('#loading').hide();
			})
			
		}
    </script>
	<?php foreach ($scripts as $script) { ?>
	<script src="<?php echo $script; ?>"></script>
	<?php } ?>
	<?php foreach ($exscripts as $exscript) { ?>
	<script>
	<?php echo $exscript; ?>
	</script>
	<script>
		document.querySelectorAll('.content img').forEach(item => {
			var w = item.style.getPropertyValue('width')
			if (parseInt(w) < 400) {
				item.style.setProperty('width', w, 'important');
				item.style.setProperty('height', 'auto', 'important');
			}
			return item
		});
		document.querySelectorAll('.newscontent img').forEach(item => {
			var w = item.style.getPropertyValue('width')
			if (parseInt(w) < 400) {
				item.style.setProperty('width', w, 'important');
				item.style.setProperty('height', 'auto', 'important');
			}
			return item
		});
	</script>
	<?php } ?>
	<?php if (isset($script_footer)) { ?>
		<?php echo $script_footer; ?>
	<?php } ?>
    <script>
        (function(w, d, u) {
                    var s = d.createElement('script');
                    s.async = true;
                    s.src = u + '?' + (Date.now() / 60000 | 0);
                    var h = d.getElementsByTagName('script')[0];
                    h.parentNode.insertBefore(s, h);
                })(window, document, 'https://cdn.bitrix24.com/b8942105/crm/site_button/loader_3_ta5v0p.js');
    </script>
</body>

</html>