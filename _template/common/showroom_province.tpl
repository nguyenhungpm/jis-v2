<?php echo $header; ?>
<div class="container m-container">
	<div class="small breadcrumbs">
		<i class="icon-icons-47"></i> <a href="."><?php echo $text_home?></a>
		/ <a href="common/showroom"><?php echo $heading_title?></a>				
		/ <a href="<?php echo $province_url; ?>"><?php echo $province_name; ?></a>				
	</div>
<div class="rowgutter row clear">
<div class="col-xs-12 col-sm-8 left-col">
<div class="local">	
	<div class="row clear none">
		<div class="col-6">
				<h1 class="n--m h2 blue"><?php echo $province_name; ?></h1>
		</div>
		<div class="col-6">
			<div id="showroomdrop" class="right">
			<select id="showroom" onchange="optionCheck()">
				<?php foreach($provincies as $province){ ?>
					<?php if($province['province_id']==$province_id){ ?>
						<option value="<?php echo $province['province_id']?>" selected><?php echo $province['name']?></option>
					<?php }else{ ?>
						<option value="<?php echo $province['province_id']?>"><?php echo $province['name']?></option>
					<?php } ?>
				<?php } ?>
			</select>
			</div>
			<script type="text/javascript">
			function optionCheck(){
				var option = document.getElementById("showroom").value;
				<?php foreach($provincies as $province){ ?>
				if(option == "<?php echo $province['province_id'];?>"){
					window.location = "<?php echo $province['name_url']."&province_id=".$province['province_id'];?>";
				}
				<?php } ?>
			}
			</script>
		</div>
	</div>

	<div id="showroomzone">
		<div class="section">
		<?php foreach($districts as $sr){ ?>
			<div class="row clear n--m">
				<div class="col-3">
					<ul>
						<?php for($i=0; $i< count($districts); $i++){ ?>
						<li><a href="<?php echo $districts[$i]['href']; ?>"><?php echo $districts[$i]['name']; ?></a></li>
						<?php if($i==count($districts)-1){ ?>
					</ul>
				</div>
							<?php } elseif(($i+1) % round(count($districts)/4)==0){ ?>
						</ul>
			</div>
			<div class="col-3">
				<ul>
				<?php } ?>
					<?php } ?>
			</div>
		<?php } ?>
		</div>
	</div>
	</div>
</div>
<div class="col-xs-12 col-sm-4 rowgutter">
  <?php echo $column_right; ?> 
</div>
</div>
</div>
<?php echo $content_bottom; ?>
<?php echo $footer; ?>