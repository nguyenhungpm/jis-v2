<?php echo $header; ?>
<div class="rowgutter breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
	<div class="container">
		    <?php foreach ($breadcrumbs as $key=>$breadcrumb) { ?>
		    	<?php echo $breadcrumb['separator']; ?><span itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo $breadcrumb['href']; ?>"><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
		    	    <meta itemprop="position" content="<?php echo $key+1; ?>" />
		    	</a></span>
		    <?php } ?>
	</div>
</div>
<div class="container">
	  <h1 class="color-black"><?php echo $heading_title; ?></h1>
	  <?php echo $text_message; ?>
		<div class="text-right rowgutter">
			<a href="<?php echo $continue; ?>" class="button"><?php echo $button_continue; ?></a>
	  </div>
</div>
<?php echo $footer; ?>
