<?php echo $header; ?>
    <!-- Main layout -->
    <main id="index-page" class="bg-gray">
        <div id="fakediv" class="d-none"></div>
        <h1 class="hidden">Japanese International School</h1>
        <?php echo $slideside; ?>

        <?php echo $content_top; ?>

        <?php echo $register; ?>
        <?php echo $content_bottom; ?>
        
    </main>
    <!-- End main layout -->
<?php echo $footer; ?>
