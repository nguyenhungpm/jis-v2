<?php echo $header; ?>
<div class="child-cover">
	<img src="static/banner-showroom.jpg" class="full-width" alt="<?php echo $heading_title; ?>" />
</div>
<div class="container m-container">
	<div class="small grey rowgutter hide">
		<i class="icon-icons-47"></i> <a href=".">Trang chủ</a> » <a href="common/showroom"><?php echo $heading_title; ?></a>
	</div>
	<?php if(empty($provinceid) && empty($brand) && empty($limit) ){ ?>
			<div class="rowgutter">
				<?php echo $content_top; ?>
			</div>
	<?php } ?>
	<div class="row clear">
	   <div class="col-xs-12 rowgutter">
		<div class="filter-showroom">
			<h1 class="n-mt cat-heading h2 blue left"><?php echo $heading_title; ?></h1>
			<select id="district_id" class="flat small right">
				<option>--Quận/ huyện--</option>
				<?php foreach($districtes as $district){ ?>
						<?php if($district['district_id']==$district_id){ ?>
							<option selected="selected" value="<?php echo $district['district_id']; ?>"><?php echo $district['name']; ?></option>
						<?php } else { ?>
							<option value="<?php echo $district['district_id']; ?>"><?php echo $district['name']; ?></option>
						<?php } ?>
				<?php } ?>
			</select>
			<select id="province" class="flat small right">
				<option>--Tìm theo tỉnh / thành--</option>
				<?php foreach ($showrooms as $showroom) { ?>
					<optgroup label="<?php echo $showroom['name']; ?>">
					<?php foreach($showroom['province'] as $province){ ?>
						<?php if($province['province_id']==$provinceid) { ?>
					  		<option selected="selected" value="<?php echo $province['province_id'];?>"><?php echo $province['name'];?></option>
						<?php }else{ ?>
							<option value="<?php echo $province['province_id'];?>"><?php echo $province['name'];?></option>
					  	<?php } ?>
					<?php } ?>
					</optgroup>
				<?php } ?>
			</select>
			<select class="flat small right" id="manufacturer">
				<option>--Tìm theo sản phẩm--</option>
				<?php foreach($pros as $pro) { ?>
					<?php if($pro['product_id']==$pro_id) { ?>
						<option selected="selected" value="<?php echo $pro['product_id'];?>"><?php echo $pro['name']; ?></option>
					<?php }else{  ?>
						<option value="<?php echo $pro['product_id'];?>"><?php echo $pro['name'];?></option>
					<?php } ?>
				<?php } ?>
			</select>
		</div>
		
	    	<?php if (isset($dealers)&&(count($dealers)>0)) { ?>
				<table class="full-width">
				<thead>
						<tr class="strong bg-gray">
								<td>Tên đại lý</td>
								<td>Địa chỉ</td>
						</tr>
				</thead>
				<?php foreach ($dealers as $dealer) { ?>
						<tr>
								<td class="strong">
									<?php if(strlen($dealer['email'])>4){ ?>
											<a href="<?php echo $dealer['email']; ?>" rel="nofollow" target="_blank"><?php echo $dealer['name']; ?></a>
									<?php } else {
										echo $dealer['name'];
									} ?>
								</td>
								<td><?php echo $dealer['address']; ?></td>
						</tr>
				<?php } ?>
				</table>
				<div class="pagination col-xs-12"><?php echo $pagination; ?></div>
			<?php } else { ?>
				<div class="text-center grey zonegutter ">Không có cửa hàng nào tại khu vực này. Hãy chọn khu vực lân cận</div>
			<?php } ?>
		</div>
	</div>
	<?php echo $content_bottom; ?>
	</div>
<?php echo $footer; ?>
