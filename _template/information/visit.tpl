<?php echo $header; ?>
<div id="fakediv" class="d-none"></div>
<section class="section section-whychoose" id="benefits" style="background-image: url('/static/program.jpg'); background-position: center; background-repeat: no-repeat; background-size: cover;">
   <div class="container">
      <div class="whychoose__head section__head">
         <h2 class="section--title"><?php echo $this->language->get('text_enrollment'); ?></h2>
         <div class="section--line"></div>
      </div>
   </div>
</section>
<nav class="vsc-top-nav ">
    <div class="container">
        <ul class="nav">
            <?php foreach($news_tab as $key => $news) { ?>
                <?php if($key == 2) { ?>
                    <li class="page_item nav-item page-item-"><a href="information/contact" class="nav-link" id="register"><?php echo $this->language->get('text_enrollment_registration'); ?></a></li>
                <?php } ?>
                <li class="page_item nav-item page-item- <?php echo $news['admission_id'] == $admission_id || $news['admission_id'] == '3' ? 'current_page_item' : ''; ?> nav-item"><a class="nav-link" href="<?php echo $news['href'];?>" aria-current="page"><?php echo $news['name'];?></a></li>
            <?php } ?>
            <!-- <li class="page_item nav-item page-item-"><a class="nav-link">Câu hỏi thường gặp</a></li> -->
        </ul>
    </div>
</nav>
<div class="register">
   <div id="banner">
      <div class="banner-item" style="background-image: url(static/img/toa-nha-trung-hoc.v2.jpg)"></div>
   </div>
   <div class="container">
      <div class="register-form mb-5" id="register-form">
         <div class="row mt-5 flex-lg-row-reverse">
            <div class="col-lg-6">
               <div class="">
                  <div>
                     <h3><strong><span style="font-size: 12pt;"><?php echo $this->language->get('text_welcome_jis'); ?></span></strong></h3>
                     <p><span style="font-size: 12pt;"><?php echo $this->language->get('text_in_development'); ?></span></p>
                     <p><span style="font-size: 12pt;"><?php echo $this->language->get('text_if_information'); ?></span></p>
                  </div>
               </div>
            </div>
            <div class="col-lg-6">
               <div class="">
                  <img width="2185" height="1283" class="img-fluid" src="static/img/so-do.jpg" alt="image content">
               </div>
            </div>
         </div>
         <div class="row mt-5">
            <div class="col-lg-6">
               <div class="scroll-sticky sticky-top">
                  <h3><strong><span style="font-size: 12pt;"><?php echo $this->language->get('text_time_visit'); ?>: </span></strong><br>
                     <span style="font-size: 12pt;"><?php echo $this->language->get('text_all_day'); ?></span>
                  </h3>
                  <p><span style="font-size: 12pt;"><?php echo $this->language->get('text_visit_desc'); ?></span></p>
                  <p><span style="font-size: 12pt;"><?php echo $this->language->get('text_visit_hotline'); ?></span></p>
               </div>
            </div>
            <div class="col-lg-6">
               <div class="scroll-sticky sticky-top" id="wrap_form">
                  <div role="form" class="wpcf7" id="wpcf7-f47469-o1" lang="vi" dir="ltr">
                     <div class="screen-reader-response" aria-live="polite"></div>
                     <form action="" method="post" class="wpcf7-form form form__register form__register__booktour" novalidate="novalidate" id="form-visit">
                        <div class="row">
                           <div class="col-md-12 d-flex flex-column-reverse">
                              <div>
                                 <h2 class="h3"><?php echo $this->language->get('text_note'); ?></h2>
                                 <div class="form-group">
                                    <span class="wpcf7-form-control-wrap ghi_chu"><textarea name="note" cols="40" rows="1" class="wpcf7-form-control wpcf7-textarea form-control" id="note" aria-invalid="false" placeholder="Nếu có"></textarea></span>
                                 </div>
                                 <p></p>
                              </div>
                              <div>
                                 <h2 class="h3"><?php echo $this->language->get('text_choose_tour'); ?></h2>
                                 <div class="form-group">
                                    <span class="wpcf7-form-control-wrap ghi_chu"><input type="datetime-local" name="time" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" id="time" aria-required="true" aria-invalid="false"></span>
                                 </div>
                                 <p></p>
                              </div>
                              <div>
                                 <h2 class="h3"><?php echo $this->language->get('text_parents_information'); ?></h2>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="form-group">
                                          <span class="wpcf7-form-control-wrap ho_ten_hs"><input type="text" name="parent" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" id="student" aria-required="true" aria-invalid="false" placeholder="Họ và tên *"></span>
                                       </div>
                                       <p></p>
                                    </div>
                                    <p></p>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <span class="wpcf7-form-control-wrap dien_thoai_ph"><input type="tel" name="parent_tel" value="" size="40" maxlength="140" minlength="10" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel form-control" id="parent_tel" aria-required="true" aria-invalid="false" placeholder="Số điện thoại *" min="0"></span>
                                       </div>
                                       <p></p>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <span class="wpcf7-form-control-wrap email_ph"><input type="email" name="parent_email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control" id="parent_email" aria-required="true" aria-invalid="false" placeholder="Email *"></span>
                                       </div>
                                       <p></p>
                                    </div>
                                    <p></p>
                                 </div>
                                 <p></p>
                              </div>
                              <p></p>
                           </div>
                        </div>
                        <p style="font-style: italic;" id="required">* <?php echo $this->language->get('text_please_registering'); ?></p>
                        <p id="success-msg" style="color: #13cb07; display: none"><?php echo $this->language->get('text_you_sincerely'); ?></p>
                        <div class="text-center mt-3 mb-5">
                           <a onclick="postToGoogle()" class="btn btn-red-bg btn-lg px-5"><?php echo $this->language->get('text_registration_submission'); ?></a>
                        </div>
                        <div class="wpcf7-response-output wpcf7-display-none" aria-hidden="true"></div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--section section-content-template -->
   <!--end section-content-template -->
</div>
<?php echo $footer; ?>