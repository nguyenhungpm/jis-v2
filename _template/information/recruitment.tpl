<?php echo $header; ?>
<div id="fakediv" class="d-none"></div>
<section class="section job-detail" id="job-detail">
   <div class="container">
      <div class="sidebar-article">
         <div class="desc-job">
            <p class="desc salary"><span class="name">Thu nhập:</span> <?php echo $benefit; ?></p>
            <p class="desc location"><span class="name"><?php echo $this->language->get('text_location'); ?>:</span> <a class="blank" href="https://careers.cmcts.com.vn/jobs?office=126"><?php echo $location; ?></a></p>
            <p class="desc date"><span class="name">Hạn nộp hồ sơ: </span><?php echo $date_end; ?></p>
         </div>
         <div class="ctas">
            <a href="#appform" onclick="Apply.focus();" class="btn-apply">Ứng tuyển vị trí này</a>
         </div>
         <div class="box-share cell art">
            <div class="line-share"></div>
            <p class="name">Chia sẻ:</p>
            <a href="https://www.facebook.com/sharer/sharer.php?u=https://careers.cmcts.com.vn/job/1552&amp;t=Giám đốc sản xuất phần mềm" target="_blank" class="btn-share -fb first"><span class="-ap icon-facebook2"></span> Facebook</a>
            <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=https://careers.cmcts.com.vn/job/1552" class="btn-share -mail  end" target="_blank"><span class="-ap icon-linkedin2"></span> Linkedin</a>
            <div class="clearfix"></div>
         </div>
         <div class="list-related" id="related">
            <div class="name">Công việc liên quan</div>
            <div class="item"><a href="https://careers.cmcts.com.vn/job/tro-ly-pho-tong-giam-doc-1154">Trợ lý Phó tổng giám đốc</a><b>1,000</b> — <b>1,500</b> <span>USD</span><span></span></div>
            <a href="/#alljobs" class="btn-viewall nav-link">View all jobs <span class="-ap icon-arrow-right3"></span></a>
         </div>
      </div>
      <article id="article" class="article">
         <div class="article-header">
			<div class="small white" itemscope itemtype="http://schema.org/BreadcrumbList">
				<?php foreach ($breadcrumbs as $key=>$breadcrumb) { ?>
					<?php echo $breadcrumb['separator']; ?><span itemprop="itemListElement" itemscope
		itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo $breadcrumb['href']; ?>"><span itemprop="name" class="white"><?php echo $breadcrumb['text']; ?></span>
						<meta itemprop="position" content="<?php echo $key+1; ?>" />
					</a></span>
				<?php } ?>
		    </div>
            <h1 class="detail-title"><?php echo $heading_title; ?></h1>
         </div>
         <div class="content-article">
            <div class="detail-article">
               <?php echo $requirement; ?>
            </div>
         </div>
         <div id="appform">
            <div class="wrapper">
               <h1>Nộp đơn ứng tuyển công việc này</h1>
               <div class="form col-lg-12">
                  <form method="POST" enctype="multipart/form-data" action="apply">
                     <input type="hidden" name="utm_source" id="f-utm-source">
                     <input type="hidden" name="utm_medium" id="f-utm-medium">
                     <input type="hidden" name="utm_campaign" id="f-utm-campaign">
                     <input type="hidden" name="utm_content" id="f-utm-content">
                     <input type="hidden" name="utm_term" id="f-utm-term">
                     <input type="hidden" name="referral_id" id="f-referral-id">
                     <input type="hidden" name="job_id" value="1552"><input type="hidden" name="token" value="SKHYW9CUHGJHQ8STR3M42JKLS7MJRXN8"><input type="hidden" name="sig" value="c48168da332f2462df62afb8f299fbd2"><input type="hidden" name="redirect_uri" value="https://careers.cmcts.com.vn/careers/apply/1552"><input type="hidden" name="response_type" value="uri">
                     <div class="row xo">
                        <div class="label">Họ &amp; tên bạn <span class="red">*</span></div>
                        <div class="input"><input type="text" name="name" placeholder="Họ &amp; tên bạn"></div>
                     </div>
                     <div class="row">
                        <div class="label">Địa chỉ email <span class="red">*</span></div>
                        <div class="input"><input type="text" name="email" placeholder="Địa chỉ email"></div>
                     </div>
                     <input data-cid="offices" type="hidden" name="offices" value="126">
                     <div class="row">
                        <div class="label">Số điện thoại  <span class="red">*</span></div>
                        <div class="input"><input type="text" name="phone" placeholder="Số điện thoại"></div>
                     </div>
                     <div class="rows">
                     </div>
                     <div class="rows cr-wrapper collapsed">
                        <div class="row-collapse hidden">
                           <div class="btn-collapse center label pointer" id="js-btn-collapsed">+ More information</div>
                        </div>
                     </div>
                     <div class="attachments"></div>
                     <div class="row">
                        <div class="upload">
                           <div class="text full-mask">
                              <input type="file" name="file" accept="application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/vnd.openxmlformats-officedocument.presentationml.presentation, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel, application/vnd.ms-powerpoint,
                                 text/plain, application/pdf, image/*">
                              CV của bạn *								
                              <div class="more">
                                 Click để chọn &amp; tải lên CV của bạn
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="label">Mã bảo mật *</div>
                        <div class="data">
                           <div class="input"><input type="text" name="captcha" placeholder="Nhập mã bảo mật bên dưới"></div>
                           <br>
                           <img class="pointer" id="js-recover-captcha" title="Click to refresh" src="https://careers.cmcts.com.vn/captcha?ts=1671585437" onclick="AP.sys.captchaReload(this);">
                        </div>
                     </div>
                     <div class="submit button -success row" onclick="Apply.submit(this);">Nộp đơn ứng tuyển</div>
                     <!-- <div class='cancel' onclick='Apply.cancel(this);'>
                        <span class='-ap icon-arrow-left'></span> &nbsp; Bỏ qua
                        </div> -->
                  </form>
               </div>
            </div>
         </div>
      </article>
      <div class="clearfix"></div>
   </div>
</section>
<div class="hide" id="apply">
	<div class="job_form bg--white">
			<h3 class="red n--m"><?php echo $entry_apply_title; ?></h3>
			<p class="grey n--mt"><?php echo $entry_apply_desc; ?></p>
			<div class="row clear">
				<div class="col--6 col__md--6">
					<label for="job_name" class="required block"><?php echo $entry_name; ?></label>
					<input type="text" class="full--width" id="job_name" name="job_name" value="" />
					<div class="error" id="job_name_error" ></div>
				</div>
				<div class="col--6 col__md--6">
					<label for="job_phone" class="required block"><?php echo $entry_phone; ?></label>
					<input type="text" class="full--width" id="job_phone" name="job_phone" value="" />
					<div class="error" id="job_phone_error" ></div>
				</div>
			</div>
			<label for="job_email" class="required block"><?php echo $entry_email; ?></label>
			<input type="text" class="full--width" id="job_email" name="job_email" value="" />
			<div class="error" id="job_email_error" ></div>
			<label for="file" class="required block"><?php echo $entry_file; ?> <span class="grey small"> (<?php echo $entry_file_allow; ?>)</span></label>
			<input type="file" name="file" id="file" />
			<input type="hidden" name="code" value="" id="code" />
			<input type="hidden" name="position" value="<?php echo $heading_title; ?>" id="position" />
			<div id="uploading" class="red"></div>
			<div id="loading" class="hide">
				<a onclick="job.closefile();" class="red h3 strong n--m right">X</a>
				<div id="content_ajax"></div>
			</div>
			<label for="job_intro" class="block clearfix"><?php echo $entry_intro; ?></label>
			<textarea name="job_intro" id="job_intro" class="full--width" rows="3"></textarea>
			<div id="job_message" class="hide"></div>
			<a href="javascript:;" onclick="job.closepopup();" class="grey upper right close__pop"><?php echo $button_close; ?></a>
			<button id="jobs__submit" class="bg--red btn white"><i class="icon-paper-plane"></i> <?php echo $button_submit; ?></button>
	</div>
</div>
<?php echo $footer; ?>
