<?php echo $header; ?>
<div class="container">
	<div class="clear row">
    <div class="col-xs-12 col-sm-8 border-right">
			<h1 class="n--m rowgutter title_module_heading darkred"><?php echo $heading_title;?></h1>

	<?php if($pictures){?>
	<div class="row clear">
	<?php foreach($pictures as $vd){ ?>
	<div class="col-xs-12 col-sm-6 gallery-item rowgutter text-center">
			<iframe width="560" height="315" style="height:315px !important" src="<?php echo $vd['src']; ?>" frameborder="0" class="iframe_youtube"></iframe>
			<h3><?php echo $vd['title'];?></h3>

	</div>
	<?php } ?>
	</div>

	<?php }else{ ?>
	<?php echo $text_empty; ?>
	<?php } ?>
	<?php echo $content_bottom; ?>
    </div>
	<div class="col-xs-12 col-sm-4">
		<?php echo $column_right; ?>
	</div>
</div>
</div>
<?php echo $footer; ?>
