<div class="form" id="form_register">
    <div class="form__container">
        <h3 class="form__title"><?php echo $this->language->get('text_enrollment_registration'); ?></h3>
        <div class="form__main">
            <div class="fluentform fluentform_wrapper_3">
                <form name="form" target="_self" action="http://osl.local/information/contact" id="fluentform_3" class="fluent_form_3" method="post">
                <div class="ff-el-group  ff-custom_html form__desc" data-name="custom_html-3_1">
                    <p><?php echo $this->language->get('text_parents_counselling'); ?></p>
                </div>
                <div class="ff-t-container ff-column-container row">
                    <div class="ff-t-cell col-lg-6">
                        <div class="ff-el-group">
                            <div class="ff-el-input--label ff-el-is-required"><label><?php echo $this->language->get('text_student_name'); ?></label> </div>
                            <div class=""><input type="text" name="student" class="ff-el-form-control form__content" placeholder="Nguyễn Văn A" id="student"></div>
                        </div>
                    </div>
                    <div class="ff-t-cell col-lg-6">
                        <div class="ff-el-group">
                            <div class="ff-el-input--label ff-el-is-required"><label><?php echo $this->language->get('text_student_birth'); ?></label> </div>
                            <div class=""><input type="date" name="student_year" class="ff-el-form-control form__content" placeholder="2007" id="student_year"></div>
                        </div>
                    </div>
                </div>
                <div class="ff-el-group form__patents">
                    <div class="ff-el-input--label ff-el-is-required"><label><?php echo $this->language->get('text_address'); ?></label> </div>
                    <div class="ff-el-input--content"><input type="text" name="address" class="ff-el-form-control form__content" placeholder="Số nhà X, đường Y, quận Z, phường T, thành phố Hà Nội"  id="address"></div>
                </div>
                <div class="ff-t-container ff-column-container row">
                    <div class="ff-t-cell col-lg-6">
                        <div class="ff-el-group">
                            <div class="ff-el-input--label ff-el-is-required"><label><?php echo $this->language->get('text_father_name'); ?></label> </div>
                            <div class=""><input type="text" name="father" class="ff-el-form-control form__content" placeholder="Họ và tên cha" id="father"></div>
                        </div>
                    </div>
                    <div class="ff-t-cell col-lg-6">
                        <div class="ff-el-group">
                            <div class="ff-el-input--label ff-el-is-required"><label><?php echo $this->language->get('text_father_number'); ?></label> </div>
                            <div class=""><input type="text" name="father_tel" class="ff-el-form-control form__content" placeholder="09xxxxxxxx" id="father_tel"></div>
                        </div>
                    </div>
                </div>
                <div class="ff-t-container ff-column-container row">
                    <div class="ff-t-cell col-lg-6">
                        <div class="ff-el-group">
                            <div class="ff-el-input--label ff-el-is-required"><label><?php echo $this->language->get('text_father_email'); ?></label> </div>
                            <div class=""><input type="text" name="father_email" class="ff-el-form-control form__content" placeholder="abc@abc.com" id="father_email"></div>
                        </div>
                    </div>
                    <div class="ff-t-cell col-lg-6">
                        <div class="ff-el-group">
                            <div class="ff-el-input--label ff-el-is-required"><label><?php echo $this->language->get('text_father_occupation'); ?></label> </div>
                            <div class=""><input type="text" name="father_job" class="ff-el-form-control form__content" placeholder="Giám đốc" id="father_job"></div>
                        </div>
                    </div>
                </div>
                <div class="ff-t-container ff-column-container row">
                    <div class="ff-t-cell col-lg-6">
                        <div class="ff-el-group">
                            <div class="ff-el-input--label ff-el-is-required"><label><?php echo $this->language->get('text_father_year'); ?></label> </div>
                            <div class=""><input type="date" name="father_year" class="ff-el-form-control form__content" placeholder="2007" id="father_year"></div>
                        </div>
                    </div>
                    <div class="ff-t-cell col-lg-6">
                    </div>
                </div>
                <div class="ff-t-container ff-column-container row">
                    <div class="ff-t-cell col-lg-6">
                        <div class="ff-el-group">
                            <div class="ff-el-input--label ff-el-is-required"><label><?php echo $this->language->get('text_mother_name'); ?></label> </div>
                            <div class=""><input type="text" name="mother" class="ff-el-form-control form__content" placeholder="Họ và tên mẹ" id="mother"></div>
                        </div>
                    </div>
                    <div class="ff-t-cell col-lg-6">
                        <div class="ff-el-group">
                            <div class="ff-el-input--label ff-el-is-required"><label><?php echo $this->language->get('text_mother_number'); ?></label> </div>
                            <div class=""><input type="text" name="mother_tel" class="ff-el-form-control form__content" placeholder="09xxxxxxxx" id="mother_tel"></div>
                        </div>
                    </div>
                </div>
                <div class="ff-t-container ff-column-container row">
                    <div class="ff-t-cell col-lg-6">
                        <div class="ff-el-group">
                            <div class="ff-el-input--label ff-el-is-required"><label><?php echo $this->language->get('text_mother_email'); ?></label> </div>
                            <div class=""><input type="text" name="mother_email" class="ff-el-form-control form__content" placeholder="abc@abc.com" id="mother_email"></div>
                        </div>
                    </div>
                    <div class="ff-t-cell col-lg-6">
                        <div class="ff-el-group">
                            <div class="ff-el-input--label ff-el-is-required"><label><?php echo $this->language->get('text_mother_occupation'); ?></label> </div>
                            <div class=""><input type="text" name="mother_job" class="ff-el-form-control form__content" placeholder="Giám đốc" id="mother_job"></div>
                        </div>
                    </div>
                </div>
                <div class="ff-t-container ff-column-container row">
                    <div class="ff-t-cell col-lg-6">
                        <div class="ff-el-group">
                            <div class="ff-el-input--label ff-el-is-required"><label><?php echo $this->language->get('text_mother_year'); ?></label> </div>
                            <div class=""><input type="date" name="mother_year" class="ff-el-form-control form__content" placeholder="2007" id="mother_year"></div>
                        </div>
                    </div>
                    <div class="ff-t-cell col-lg-6">
                    </div>
                </div>
                <div class="ff-t-container ff-column-container row">
                    <div class="ff-t-cell col-lg-6">
                        <div class="ff-el-group">
                            <div class="ff-el-input--label ff-el-is-required"><label><?php echo $this->language->get('text_program_registration'); ?></label> </div>
                            <div class="ff-el-input--content">
                                <select name="class" id="class" class="ff-el-form-control form__content">
                                    <option value="">- Select -</option>
                                    <!-- <?php foreach($informations as $infor) { ?>
                                    <option value="<?php echo $infor['title']; ?>"><?php echo $infor['title']; ?></option>
                                    <?php } ?> -->
                                    <option value="Mầm non Quốc tế Nhật Bản"><?php echo $this->language->get('text_japanese_kindergarten'); ?></option>
                                    <option value="Mầm non Quốc tế IEYC"><?php echo $this->language->get('text_ieyc_kindergarten'); ?></option>
                                    <option value="Mầm non Song ngữ"><?php echo $this->language->get('text_bilingual_kindergarten'); ?></option>
                                    <option value="Phổ thông Quốc tế Nhật Bản"><?php echo $this->language->get('text_japanese_school'); ?></option>
                                    <option value="Phổ thông Quốc tế Cambridge"><?php echo $this->language->get('text_cambridge_education'); ?></option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="ff-t-cell col-lg-6">
                        <div class="ff-el-group">
                            <div class="ff-el-input--label ff-el-is-required"><label><?php echo $this->language->get('text_class_registration'); ?></label> </div>
                            <div class=""><input type="text" name="detail_class" class="ff-el-form-control form__content" placeholder="Lớp 7" id="detail_class"></div>
                        </div>
                    </div>
                </div>
                <p style="font-style: italic;" id="required">* <?php echo $this->language->get('text_please_registering'); ?></p>
                <p id="success-msg" style="color: #13cb07; display: none"><?php echo $this->language->get('text_you_sincerely'); ?></p>
                <input type="hidden" name="name" value="Hùng">
                <input type="hidden" name="email" value="hungnv157@gmail.com">
                <input type="hidden" name="email2" value="hungnv157@gmail.com">
                <input type="hidden" name="phone" value="0347120757">
                <input type="hidden" name="enquiry" value="test">
                <div class="ff-el-group ff-text-left ff_submit_btn_wrapper"><a onclick="postToGoogle()" class="ff-btn ff-btn-submit ff-btn-md form__btn ff_btn_style"><?php echo $this->language->get('text_enrollment_registration'); ?></a></div>
                </form>
                <div id="fluentform_3_errors" class="ff-errors-in-stack ff_form_instance_3_1 ff-form-loading_errors ff_form_instance_3_1_errors"></div>
                <div id="success-msg" style="display: none;"><?php echo $this->language->get('text_success_registration'); ?></div>
            </div>
            <script type="text/javascript">
                window.fluent_form_ff_form_instance_3_1 = {"id":"3","settings":{"layout":{"labelPlacement":"top","helpMessagePlacement":"with_label","errorMessagePlacement":"inline","asteriskPlacement":"asterisk-right"},"restrictions":{"denyEmptySubmission":{"enabled":false}}},"form_instance":"ff_form_instance_3_1","form_id_selector":"fluentform_3","rules":{"input_text":{"required":{"value":true,"message":"This field is required"}},"input_text_3":{"required":{"value":true,"message":"This field is required"}},"input_text_1":{"required":{"value":true,"message":"This field is required"}},"input_text_4":{"required":{"value":true,"message":"This field is required"}},"input_text_5":{"required":{"value":false,"message":"This field is required"}},"dropdown":{"required":{"value":true,"message":"This field is required"}}}};
                        
            </script>
        </div>
    </div>
</div>