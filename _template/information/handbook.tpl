<h2 class="h4 title-section title-decoration"><?php echo $this->language->get('text_documents_you'); ?></h2>
<div class="rowgutter">
    <div class="download">
        <a href="cam-nang" target="_blank"><i class="demo-icon ecs-pdf-file"></i><?php echo $this->language->get('text_handbook'); ?></a>
    </div>
    <div class="download">
        <a href="/media/file/don-xin-nhap-hoc-2023-2024.pdf" target="_blank"><i class="demo-icon ecs-pdf-file"></i><?php echo $this->language->get('text_registration_letter'); ?></a>
    </div>
    <div class="download">
        <a href="/media/file/chinh-sach-hoc-phi-2023-2024.v2.pdf" target="_blank"><i class="demo-icon ecs-pdf-file"></i><?php echo $this->language->get('text_policy_fee'); ?></a>
    </div>
</div>
