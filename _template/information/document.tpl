<?php echo $header; ?>
<div id="fakediv" class="d-none"></div>
<div class="sub__header white small">
<div class="container gutter t--c">
		<div class="medium white" itemscope itemtype="https://schema.org/BreadcrumbList">
		<?php foreach ($breadcrumbs as $key=>$breadcrumb) { ?>
			<?php echo $breadcrumb['separator']; ?><span itemprop="itemListElement" itemscope
		itemtype="https://schema.org/ListItem"><a itemprop="item" href="<?php echo $breadcrumb['href']; ?>"><span class="white" itemprop="name"><?php echo $breadcrumb['text']; ?></span>
				<meta itemprop="position" content="<?php echo $key+1; ?>" />
			</a></span>
		<?php } ?>
		</div>
		<h1 class="white h3 n--m"><?php echo $heading_title; ?></h1>
</div>
</div>
<div class="container">
		<div class="row">
			<div class="col-lg-8 rowgutter col-12">
					<input id="regdoc__source" type="hidden" value="<?php echo $heading_title; ?>" />
					<input id="regdoc__notify" type="hidden" value="<?php echo $notify; ?>" />
					<input id="regdoc__id" type="hidden" value="<?php echo $document_id; ?>" />
					<img src="<?php echo $image; ?>" alt="<?php echo $heading_title; ?>" class="block cover--doc" />
          <h1  class="h2"><?php echo $heading_title; ?><?php echo $admin_logged ? ' (<a target="_blank" href="'.$edit.'">Sửa</a>)': ''?></h1>
          <?php echo $short_description; ?>
          <?php echo $description; ?>
					<div class="gutter">
						<br />
						<?php if($invisible == '0') { ?>
								<a target="_blank" href="insights/download&hash=<?php echo $hash; ?>" class="bg--red btn white cta__download"><?php echo $text_linkdown; ?></a>
						<?php }else{ ?>
								<button id="cta__request" onclick="Regdoc();" class="bg--red btn white"><?php echo $text_request; ?></button>
						<?php } ?>
					</div>

			</div>
      <div class="col-12 col-lg-4 rowgutter rightcol">
        <ul class="list--inline t--c strong small cat__list upper">
    			<li><a class="ani <?php echo $all_active; ?>" href="insights"><?php echo $text_alldocs; ?></a></li>
	    		<?php foreach($cats as $cat) { ?>
	    			<li><a class="ani <?php echo $cat['active']; ?>" href="<?php echo $cat['href']; ?>"><?php echo $cat['name']; ?></a></li>
	    		<?php } ?>
    		</ul>
        <div class="rowgutter">
				<?php if(count($documents)>0) {?>
          <h2 class="h4 title-section title-decoration"><?php echo $text_others . ' ' . $catname; ?></h2>
				<?php foreach($documents as $item) { ?>
						<a href="<?php echo $item['href']; ?>" class="other__doc block gutter">
							<img src="<?php echo $item['thumb']; ?>" alt="<?php echo $item['name']; ?>" class="right other__thumb" />
							<span class="medium"><?php echo $item['name']; ?></span>
						</a>
				<?php } ?>
				<?php } ?>
      </div>
      </div>
		</div>
</div>
<div class="hide bg--white" id="regdoc__inquiry">
      <a href="javascript:;" onclick="closeRegdoc();" class="grey upper right close__pop">X</a>
      <div id="regdoc__heading" class="h3 n--m"><?php echo $text_inquiry; ?></div>
      <p class="grey small n--m"><?php echo $entry_inquiry_desc; ?></p>
      <hr />
      <div class="row clear">
        <div class="col--12 col__md--7 n--pr gutter">
          <div class="input__wrap relative">
            <input type="text" class="full--width" id="regdoc__name" name="regdoc__name" value="" />
            <label for="regdoc__name" class="required block"><?php echo $entry_name; ?></label>
          </div>
          <div class="error small" id="regdoc__name__error" ></div>
        </div>
        <div class="col--12 col__md--5 n--pl gutter">
          <div class="input__wrap relative">
            <input type="text" class="full--width" id="regdoc__phone" name="regdoc__phone" value="" />
            <label for="regdoc__phone" class="required block"><?php echo $entry_phone; ?></label>
          </div>
          <div class="error small" id="regdoc__phone__error" ></div>
        </div>
        <div class="col--12 col__md--5 n--pr gutter">
          <div class="input__wrap relative">
              <input type="text" class="full--width" id="regdoc__email" name="regdoc__email" value="" />
              <label for="regdoc__email" class="required block"><?php echo $entry_email; ?></label>
          </div>
          <div class="error small" id="regdoc__email__error" ></div>
        </div>
        <div class="col--12 col__md--7 n--pl gutter">
					<div class="input__wrap relative">
              <input type="text" class="full--width" id="regdoc__title" name="regdoc__title"></textarea>
              <label for="regdoc__title" class="block"><?php echo $entry_title; ?></label>
          </div>
          <div class="error small" id="regdoc__title__error" ></div>
        </div>
        <div class="col--12 col__md--12 gutter">
					<div class="input__wrap relative">
              <input type="text" class="full--width" id="regdoc__office" name="regdoc__office" value="" />
              <label for="regdoc__office" class="block"><?php echo $entry_office; ?></label>
          </div>
          <div class="error small" id="regdoc__office__error" ></div>
        </div>
      </div>
      <div class="gutter">
          <div id="regdoc__loading"></div>
          <div id="regdoc__alert"></div>
          <button id="regdoc__submit" class="bg--red btn white"><i class="icon-paper-plane"></i> <?php echo $button_submit; ?></button>
      </div>
</div>
<?php echo $footer; ?>
