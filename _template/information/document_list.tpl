<?php echo $header; ?>
<div id="fakediv" class="d-none"></div>
<div class="sub__header white small">
<div class="container gutter t--c">
		<div class="medium white" itemscope itemtype="https://schema.org/BreadcrumbList">
		<?php foreach ($breadcrumbs as $key=>$breadcrumb) { ?>
			<?php echo $breadcrumb['separator']; ?><span itemprop="itemListElement" itemscope
		itemtype="https://schema.org/ListItem"><a itemprop="item" href="<?php echo $breadcrumb['href']; ?>"><span class="white" itemprop="name"><?php echo $breadcrumb['text']; ?></span>
				<meta itemprop="position" content="<?php echo $key+1; ?>" />
			</a></span>
		<?php } ?>
		</div>
		<h1 class="white h3 n--m"><?php echo $heading_title; ?></h1>
</div>
</div>
<div class="container zonegutter">
		<ul class="list--inline t--c strong medium cat__list upper">
			<li><a class="ani <?php echo $all_active; ?>" href="insights"><?php echo $text_alldocs; ?></a></li>
		<?php foreach($cats as $cat) { ?>
			<li><a class="ani <?php echo $cat['active']; ?>" href="<?php echo $cat['href']; ?>"><?php echo $cat['name']; ?></a></li>
		<?php } ?>
		</ul>
		<div class="row">

			<?php foreach ($documents as $key=>$doc) { ?>
			<div class="col-12 col-lg-4 rowgutter">
				<div class="casestudy__holder casestudy__holder--doc relative">
						<a href="<?php echo $doc['href']; ?>">
							<figure class="news__thumb overflow">
								<img width="<?php echo $width; ?>" height="<?php echo $height; ?>" src="<?php echo $doc['thumb']; ?>" data-src="<?php echo $doc['thumb']; ?>" alt="<?php echo $doc['name']; ?>" class="block lazyload full--width" />
							</figure>
						</a>
						<div class="casestudy__footer absolute">
								<a class="strong blue" href="<?php echo $doc['href']; ?>"><?php echo $text_readmore; ?><span class="arrow--right"><i class="icon-right-1"></i></span></a>
						</div>
						<div class="bg--white casestudy__header relative">
								<span class="casestudy__tag inline--block upper"><?php echo $doc['catname']; ?></span>
							  <h3 class="h5 n--m lh13"><a href="<?php echo $doc['href']; ?>"><?php echo $doc['name']; ?></a></h3>
								<div class="small gutter">
										<?php echo $doc['short_description']; ?>
								</div>
						</div>
					</div>
			</div>
			<?php } ?>
			<div class="pagination small">
					<?php echo $pagination; ?>
			</div>
		</div>
</div>
<?php echo $footer; ?>
