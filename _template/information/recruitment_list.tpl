<?php echo $header; ?>
<div id="fakediv" class="d-none"></div>
<?php // echo $slideside; ?>

<section class="section section-whychoose" id="benefits" style="background-image: url('/static/tuyen-dung.jpg'); background-position: center; background-repeat: no-repeat; background-size: cover;">
   <div class="container">
      <div class="whychoose__head section__head">
         <h2 class="section--title"><?php echo $this->language->get('text_why_jis'); ?></h2>
         <div class="section--line"></div>
      </div>
      <div class="whychoose-content">
         <div class="whychoose__item">
            <div class="item--image">
               <img src="https://datax-talent.basecdn.net/cmcts/icon/dinh-huong.png" alt="">
            </div>
            <h3 class="item--title"><?php echo $this->language->get('text_future_navigation'); ?></h3>
            <p class="item--desc"><?php echo $this->language->get('text_energetic_cultures'); ?></p>
         </div>
         <div class="whychoose__item">
            <div class="item--image">
               <img src="https://datax-talent.basecdn.net/cmcts/icon/thanh-tich.png" alt="">
            </div>
            <h3 class="item--title"><?php echo $this->language->get('text_achievement_credit'); ?></h3>
            <p class="item--desc"><?php echo $this->language->get('text_receive_proactivity'); ?></p>
         </div>
         <div class="whychoose__item">
            <div class="item--image">
               <img src="https://datax-talent.basecdn.net/cmcts/icon/suc-khoe.png" alt="">
            </div>
            <h3 class="item--title"><?php echo $this->language->get('text_comprehensive_benefits'); ?></h3>
            <p class="item--desc"><?php echo $this->language->get('text_various_perks'); ?></p>
         </div>
         <div class="whychoose__item">
            <div class="item--image">
               <img src="https://datax-talent.basecdn.net/cmcts/icon/hanh-phuc.png" alt="">
            </div>
            <h3 class="item--title"><?php echo $this->language->get('text_work_satisfaction'); ?></h3>
            <p class="item--desc"><?php echo $this->language->get('text_an_environment'); ?></p>
         </div>
         <div class="clearfix"></div>
      </div>
   </div>
</section>

<section class="section section-jobs" id="alljobs">
   <div class="container">
      <div class="jobs__head section__head">
		 <h1 class="maintitle white n--m section--title"><?php echo $heading_title; ?></h1>
         <div class="section--line"></div>
      </div>
      <div class="content-sec">
         <div id="jobs-search" class="search-form section--form">
            <div class="box-search">
               <div class="form-group box-input -keyword">
                  <input type="text" placeholder="Job title..." name="search-string" data-action="search" id="job-search" class="form-control">
               </div>
               <div class="form-group -button">
                  <button class="btn-search" onclick="Support.search(this);"><?php echo $this->language->get('text_search'); ?></button>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-3">
               <div class="sidebar" id="sidebar">
                  <button class="baseui button fluid main" onclick="Career.filter(this);">
                  <?php echo $this->language->get('text_filter'); ?>
                  </button>
                  <div class="list-category">
                     <h3 class="title-cat"><?php echo $this->language->get('text_sector'); ?></h3>
                     <div class="baseui input icon">
                        <i class="icon ficon-search"></i>
                        <input type="text" name="department-search" id="department-search" placeholder="<?php echo $this->language->get('text_filter_sector'); ?>...">
                     </div>
                     <div id="departments" class="list scrollable">
                        <div class="li">
                           <div class="check-action dpb">
                              <input type="checkbox" class="check_row" onclick="Career.checkAll(this);" data-check="department"><span class="icon"></span><?php echo $this->language->get('text_all'); ?>
                           </div>
                        </div>
                        <?php foreach($reccats as $cat) { ?>
                        <div class="li">
                           <div class="check-action dpb">
                              <input type="checkbox" class="check_row department js-dept-276" name="job-dept" value="<?php echo $cat['reccat_id']; ?>"><span class="icon"></span><?php echo $cat['name']; ?>             
                           </div>
                        </div>
                        <?php } ?>
                     </div>
                  </div>
                  <div class="list-category">
                     <h3 class="title-cat"><?php echo $this->language->get('text_location'); ?></h3>
                     <div class="baseui input icon">
                        <i class="icon ficon-search"></i>
                        <input type="text" name="location-search" id="location-search" placeholder="<?php echo $this->language->get('text_filter_location'); ?>...">
                     </div>
                     <div id="locations" class="list scrollable">
                        <div class="li">
                           <div class="check-action dpb">
                              <input type="checkbox" class="check_row" onclick="Career.checkAll(this);" data-check="location"><span class="icon"></span><?php echo $this->language->get('text_all'); ?>
                           </div>
                        </div>
                        <?php foreach($locations as $loc) { ?>
                        <div class="li">
                           <div class="check-action dpb">
                              <input type="checkbox" class="check_row location js-loc-126" name="job-location" value="<?php echo $loc['location']; ?>"><span class="icon"></span><?php echo $loc['location']; ?>
                           </div>
                        </div>
                        <?php } ?>
                     </div>
                  </div>
                  <div class="list-category">
                     <h3 class="title-cat"><?php echo $this->language->get('text_job_type'); ?></h3>
                     <div class="list">
                        <?php foreach($jobtypes as $key => $jt) { ?>
                        <div class="check-action dpb">
                           <input type="checkbox" class="check_row jobtype" value="<?php echo $key; ?>" name="job-type"><span class="icon"></span><?php echo $jt; ?>
                        </div>
                        <?php } ?>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-9">
               <div class="list-jobs" id="list-jobs">
			      <?php foreach ($recruitments as $key => $recruitment) { ?>
                  <div class="item-job <?php echo $recruitment['hot'] == 1 ? 'hot' : ''; ?>">
                     <div class="cell name-job">
                        <h4 class="title"><a href="<?php echo $recruitment['href']; ?>"><?php echo $recruitment['name']; ?></a></h4>
                        <span class="hot-job"><i class="ficon-bolt"></i>Hot</span>
                     </div>
                     <div class="desc">
                        <p class="info date">
                           <span class="-ap icon-monetization_on"></span><?php echo $recruitment['benefit']; ?> <span class="-ap icon-dot-single"></span>
                           <?php echo $jobtypes[$recruitment['jobtype']]; ?>
                        </p>
                        <p class="info"><span class="-ap icon-location3"></span><a class="_blank" href="https://careers.cmcts.com.vn/jobs?office=126&amp;return=1"><?php echo $recruitment['location']; ?></a></p>
                        <p class="info">
                           <span class="-ap icon-business-card"></span><?php echo $recruitment['reccat_name']; ?> <span class="-ap icon-dot-single"></span>
                           <span class="-ap icon-access_time"></span><?php echo $recruitment['date_end']; ?>       
                        </p>
                     </div>
                     <div class="cta">
                        <a href="<?php echo $recruitment['href']; ?>" class="btn-apply"><?php echo $this->language->get('text_apply_now'); ?></a>
                     </div>
                  </div>
				   <?php } ?>
               </div>
               <div class="pagination small text-center">
                  <?php echo $pagination; ?>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>

<?php echo $footer; ?>
