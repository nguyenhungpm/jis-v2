<?php echo $header; ?>
<div id="fakediv" class="d-none"></div>
<section class="section section-whychoose" id="benefits" style="background-image: url('/static/program.jpg'); background-position: center; background-repeat: no-repeat; background-size: cover;">
   <div class="container">
      <div class="whychoose__head section__head">
         <h2 class="section--title"><?php echo $this->language->get('text_educational_program'); ?></h2>
         <div class="section--line"></div>
      </div>
   </div>
</section>
<nav class="vsc-top-nav ">
    <div class="container">
        <ul class="nav">
			<?php foreach($informations as $information) { ?>
				<li class="page_item nav-item <?php echo $information['information_id'] == $information_id ? 'current_page_item':'';?> nav-item"><a class="nav-link" href="<?php echo $this->url->link('information/information', 'information_id=' . $information['information_id'])?>" aria-current="page"><?php echo $information['title']; ?></a></li>
			<?php } ?>
        </ul>
    </div>
</nav>
<div class="rowgutter grey small uppercase breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
	<div class="container">
		    <i class="icon-icons-47"></i> <?php foreach ($breadcrumbs as $key=>$breadcrumb) { ?>
		    	<?php echo $breadcrumb['separator']; ?><span itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo $breadcrumb['href']; ?>"><span itemprop="name" class="grey"><?php echo $breadcrumb['text']; ?></span>
		    	    <meta itemprop="position" content="<?php echo $key+1; ?>" />
		    	</a></span>
		    <?php } ?>
	</div>
</div>
<div class="container">
<div class="clear row">
    <div class="col-xs-12 col-sm-8 border-right">
		<h1 class="darkblue n-mt"><?php echo $heading_title; ?><?php echo $admin_logged ? ' (<a target="_blank" href="'.$edit.'">Sửa</a>)': ''?></h1>
		<div class="newscontent">
			  <?php echo $description; ?>
		</div>
		<section id="video" class="infor">
			<div class="container-fluid">
				<div class="tab-content">
					<div id="info1" class="tab-pane fade in active">
						<div class="section-body flex-container">
							<div style="width: 100%">
									<div class="text-container">
											<div class="des h5">
												<div class="edu-point">
													<?php foreach($highlight as $key => $hl) { ?>
													<div class="edu-point__item <?php echo $key%2 != 0 ? 'edu-point__item--odd' : ''; ?> edu-point__0<?php echo $key+1; ?>">
														<div class="edu-point__icon">
														<img
															width="56"
															alt=""
															data-src="media/<?php echo $hl['image']; ?>"
															class="icon-imgtag ls-is-cached lazyloaded"
															src="media/<?php echo $hl['image']; ?>"
														/><span class="edu-point__line"></span>
														</div>
														<div class="edu-point__text">
														<p><?php echo $hl['highlight_description']; ?></p>
														</div>
														<span class="<?php echo $key == count($highlight) -1 ? '' : 'edu-point__bridge'; ?>"></span>
													</div>
													<?php } ?>
												</div>
											</div>
									</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<?php echo $column_left; ?>
	</div>
	<div class="col-xs-12 col-sm-4 rowgutter">
		<div class="col-right">
			<?php echo $handbook; ?>
		</div>
		<?php echo $column_right; ?>
	</div>
</div>
</div>
<?php echo $footer; ?>
