<?php echo $header; ?>
<div class="container rowgutter">
        <div class="small breadcrumbs uppercase grey" itemscope itemtype="http://schema.org/BreadcrumbList">
        <i class="icon-icons-47"></i> <?php foreach ($breadcrumbs as $key=>$breadcrumb) { ?>
            <?php echo $breadcrumb['separator']; ?><span itemprop="itemListElement" itemscope
  itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo $breadcrumb['href']; ?>"><span class="grey" itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                <meta itemprop="position" content="<?php echo $key+1; ?>" />
            </a></span>
        <?php } ?>
        </div>
        <div class="row clear">
            <div class="col-xs-12 col-sm-7">
            <h1 class="n-mb darkblue h1 title_center"><span><?php echo $heading_title; ?></span></h1>
            <div class="listfaqs">
                <?php foreach($questions as $faq){ ?>
                    <div class="question-row">
                        <h3 class="n--m question_title roboto"><a class="red" href="<?php echo $faq['href']; ?>"><?php echo $faq['title']; ?></a></h3>
                        <div class="small">bởi <b><?php echo $faq['name']; ?></b>, vào <?php echo $faq['create_time']; ?></div>
                        <p class="question-desc">
                            <?php echo $faq['question']; ?>
                        </p>
                        <a href="<?php echo $faq['href']; ?>" class="view-reply">Xem trả lời &#x2192;</a>
                        <hr />
                    </div>
                <?php } ?>
            </div>
            </div> 
            <div class="col-xs-12 col-sm-5 gutter">
               <div class="faq_module">
                <h2 class="n-mt blue title_module_heading h3 uppercase"><span>Gửi câu hỏi của bạn</span></h2>
        	   <form method="post" name="form-order" id="form-order" action="<?php echo $action;?>">
                    <div class="row clear">
                        <div class="col-6">
        			        <input type="text" class="full-width" name="name" value="<?php echo $name ? $name : ''; ?>" placeholder="Họ và tên *" />
                            <div class="red"><?php echo $error_name ? $error_name : '';?></div>
                        </div>
                    </div>
                    <div class="row clear">
                        <div class="col-6">
                            <input type="text" class="full-width" name="email" value="<?php echo $email ? $email : ''; ?>" placeholder="Email" />
                            <div class="red"><?php echo $error_email ? $error_email : '';?></div>
                        </div>
                        <div class="col-6">
                            <input type="text" class="full-width" name="phone" value="<?php echo $phone ? $phone : ''; ?>" placeholder="Điện thoại" />
                            <div class="red"><?php echo $error_phone ? $error_phone : '';?></div>
                        </div>
                    </div>
                    <div class="row clear">
                        <div class="col-xs-12">
                            <input type="text" class="full-width" name="title" value="<?php echo $title ? $title : ''; ?>" placeholder="Tiêu đề" />
                            <div class="red"><?php echo $error_title ? $error_title : '';?></div>
                            <textarea name="question" class="full-width" placeholder="Nêu nội dung bạn muốn hỏi" rows="4"><?php echo $question ? $question : ''; ?></textarea>
                            <button type="submit" class="bg-blue button white faq_form_submit">Gửi câu hỏi <i class="icon-icons-48"></i></button>
                        </div>
                    </div>		
                </form>
                </div>
            </div>
        </div>
</div>
<?php echo $footer; ?>