<?php echo $header; ?>
<div class="container"><div class="clear row">
    <div class="col-xs-12 col-sm-10 no-padding newslastest">
	  <h1 class="maintitle n-mb"><?php echo $heading_title;?></h1>
    	<?php if($videos){?>
    	<div class="bg-grey border clear">
      	<?php foreach($videos as $video){ ?>
      	<div class="col-xs-12 col-sm-4 gallery-item rowgutter">
      		<div class="relative">
      		<img class="block" alt="<?php echo $video['name'];?>" src="<?php echo $video['thumb'];?>"/>
      		<a class="mediabox" href="<?php echo $video['href'];?>">
      			<span class="play-i absolute"></span>
      		</a>
      		<div class="overname"><a class="mediabox name" href="<?php echo $video['href'];?>"><?php echo $video['name'];?></a></div>
      		</div>
      	</div>
      	<?php } ?>
	   </div>

	<?php }else{ ?>
	   <?php echo $text_empty; ?>
	<?php } ?>
	<?php echo $content_bottom; ?>
  </div>
	<div class="col-xs-12 col-sm-2 n--p">
	   <?php echo $column_right; ?>
	</div>
</div></div>
<?php echo $footer; ?>
