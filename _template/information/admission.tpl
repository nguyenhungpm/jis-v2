<?php echo $header; ?>
<div id="fakediv" class="d-none"></div>
<section class="section section-whychoose" id="benefits" style="background-image: url('/static/program.jpg'); background-position: center; background-repeat: no-repeat; background-size: cover;">
   <div class="container">
      <div class="whychoose__head section__head">
         <h2 class="section--title"><?php echo $this->language->get('text_enrollment'); ?></h2>
         <div class="section--line"></div>
      </div>
   </div>
</section>
<nav class="vsc-top-nav ">
    <div class="container">
        <ul class="nav">
            <?php foreach($news_tab as $key => $news) { ?>
                <?php if($key == 2) { ?>
                    <li class="page_item nav-item page-item-"><a class="nav-link" id="register"><?php echo $this->language->get('text_enrollment_registration'); ?></a></li>
                <?php } ?>
                <li class="page_item nav-item page-item- <?php echo $news['admission_id'] == $admission_id ? 'current_page_item' : ''; ?> nav-item"><a class="nav-link" href="<?php echo $news['href'];?>" aria-current="page"><?php echo $news['name'];?></a></li>
            <?php } ?>
            <!-- <li class="page_item nav-item page-item-"><a class="nav-link" id="link-noi-bo">Câu hỏi thường gặp</a></li> -->
        </ul>
    </div>
</nav>
<div class="detail-temp1">
    <div class="container">
        <div class="col-12 rowgutter">
            <div class="small white" itemscope itemtype="http://schema.org/BreadcrumbList">
                <?php foreach ($breadcrumbs as $key=>$breadcrumb) { ?>
                    <?php echo $breadcrumb['separator']; ?>
                    <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                        <a itemprop="item" href="<?php echo $breadcrumb['href']; ?>"><span itemprop="name" class="white"><?php echo $breadcrumb['text']; ?></span>
                            <meta itemprop="position" content="<?php echo $key+1; ?>" />
                        </a>
                    </span>
                <?php } ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="content_left">
                    <div class="title-temp2 tempDetail">
                        <h3><?php echo $heading_title; ?><?php echo $admin_logged ? ' (<a target="_blank" href="'.$edit.'">Sửa</a>)': ''?></h3>
                        <ul>

                            <li><a href=""><span class="fa fa-facebook"></span></a></li>
                            <li><a href=""><span class="fa fa-twitter"></span></a></li>
                            <li><a href=""><span class="fa fa-google-plus"></span></a></li>
                            <li><a href=""><span class="fa fa-youtube"></span></a></li>
                        </ul>
                    </div>
                </div>
                <div class="content"><?php echo $description; ?></div>
            </div>

            <div class="col-md-4">
                <div class="col-right">
                    <?php echo $handbook; ?>
                </div>
                <?php echo $column_right; ?>
            </div>
        </div>
    </div>

    <div>
        <?php echo $register; ?>
    </div>    
</div>

<?php echo $footer; ?>
