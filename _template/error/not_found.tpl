<?php echo $header; ?>
<div class="gutter uppercase grey text-center small breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
	<div class="container">
		    <?php foreach ($breadcrumbs as $key=>$breadcrumb) { ?>
		    	<?php echo $breadcrumb['separator']; ?><span itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo $breadcrumb['href']; ?>"><span itemprop="name" class="grey"><?php echo $breadcrumb['text']; ?></span>
		    	    <meta itemprop="position" content="<?php echo $key+1; ?>" />
		    	</a></span>
		    <?php } ?>
	</div>
</div>
<div class="container zonegutter text-center n-pt">
		<h1 class="uppercase gray"><?php echo $heading_title; ?></h1>
		<p class="gray"><?php echo $text_error; ?></p>
</div>
<?php echo $footer; ?>
