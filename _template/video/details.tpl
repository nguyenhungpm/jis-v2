<?php echo $header; ?>
<div class="container"><div class="clear row rowgutter">
	<div class="col-12 col-tablet-8">
	<div class="bottom20">
	<div class="small breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
		<?php foreach ($breadcrumbs as $key=>$breadcrumb) { ?>
		<?php echo $breadcrumb['separator']; ?><span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo $breadcrumb['href']; ?>"><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
			<meta itemprop="position" content="<?php echo $key+1; ?>" />
		</a></span>
		<?php } ?>
	</div>
	</div>
	<h1 class="color-black no-margin"><?php echo $heading_title; ?></h1>
		<div class="line"></div>
		<div class="zonegutter">
	   		<iframe id="ytplayer" type="text/html" width="710" height="340"
      src="https://www.youtube.com/embed/<?php echo $url_video; ?>?autoplay=1&"
        frameborder="0"></iframe>
      	</div>
	  	<div class="newscontent minigutter">	  
		  <?php echo $description; ?>
	  	</div>

	  
		<div class="hide-print clear">
		<div class="col-12 col-tablet-6 no-padding">
		<div class="shareme">
	  		<br class="show-mobile hide-desktop hide-tablet" />
	    	<a href="https://twitter.com/share?url=<?php echo $url; ?>" target="_blank" class="twitter-icon inline-block" rel="nofollow">Twitter Share</a>
	    	<a href="https://plus.google.com/share?url=<?php echo $url; ?>" target="_blank" class="plus-icon inline-block" rel="nofollow">Google+ Share</a>
	    	<a href="http://facebook.com/share.php?u=<?php echo $url; ?>" target="_blank" class="face-icon inline-block" rel="nofollow">Facebook Share</a>
		</div>
		</div>
		<div class="col-12 col-tablet-6 no-padding">
		<div class="fb-like" data-href="<?php echo $url; ?>" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="false" data-width="320"></div>
		</div>
		</div>
	  <div class="line"></div>
	  <div class="hide-print">
		<?php echo $content_bottom; ?>
   	  </div>
   </div>
	<div class="col-12 col-tablet-4 hide-print">
	   <?php echo $column_right; ?>
	</div>
</div></div>
<?php echo $footer; ?>