<?php echo $header; ?>
<style>
  .tab-pane {
	display: none;
}
.tab-pane.active {
	display: block;
}

#video ul {
	display: flex;
  	justify-content: center;
}
#video li {
	float: left;
  border: 1px solid #343d60;
	transform: skew(-20deg);
	border-radius: 10px;
	margin: 0 10px;
  color: #ed2124;
}
#video li.active,
#video li:hover
{
	background-color: #ed2124;
  border: 1px solid #ed2124;
}
#video li:hover a,
#video li.active a
{
  color: white;
}
#video li a{
	display: inline-block;
	color: #ed2124;
  padding: 10px 34px;
  margin: 0 10px;
	transform: skew(20deg);
}
.read-more {
  color: red;
  border: 1px solid red;
  padding: 12px 15px;
  border-radius: 5px;
  margin-bottom: 20px;
}
</style>
<div id="fakediv" class="d-none"></div>
<section class="section section-whychoose" id="benefits" style="background-image: url('/static/program.jpg'); background-position: center; background-repeat: no-repeat; background-size: cover;">
   <div class="container">
      <div class="whychoose__head section__head">
         <h2 class="section--title"><?php echo $this->language->get('text_school_life'); ?></h2>
         <div class="section--line"></div>
      </div>
   </div>
</section>
<nav class="vsc-top-nav ">
    <div class="container">
        <ul class="nav">
          <?php foreach($products as $product) { ?>
            <li class="page_item nav-item nav-item"><a class="nav-link" href="<?php echo $this->url->link('product/product', 'product_id=' . $product['product_id'])?>" aria-current="page"><?php echo $product['name']; ?></a></li>
          <?php } ?>
            <li class="page_item nav-item current_page_item nav-item"><a class="nav-link" href="videos/videos" aria-current="page"><?php echo $this->language->get('text_photo_video'); ?></a></li>
        </ul>
    </div>
</nav>
<div class="rowgutter grey small uppercase breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
	<div class="container">
		    <i class="icon-icons-47"></i> <?php foreach ($breadcrumbs as $key=>$breadcrumb) { ?>
		    	<?php echo $breadcrumb['separator']; ?><span itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo $breadcrumb['href']; ?>"><span itemprop="name" class="grey"><?php echo $breadcrumb['text']; ?></span>
		    	    <meta itemprop="position" content="<?php echo $key+1; ?>" />
		    	</a></span>
		    <?php } ?>
	</div>
</div>

<div class="container" id="video">
  <div class="clearfix row">
    <ul class="nav nav-tabs clearfix">
      <li class="active"><a data-toggle="tab" href="#videos"><?php echo $this->language->get('text_video'); ?></a></li>
      <li class=""><a data-toggle="tab" href="#images"><?php echo $this->language->get('text_photo'); ?></a></li>
    </ul>

    <div class="tab-content rowgutter">
      <div id="videos" class="tab-pane fade in active">
        <div id="lightgallery">
        <?php foreach ($videos as $member) { ?>
          <a class="col-6 col-lg-4 rowgutter"
              data-lg-size="1280-720"
              data-src="//www.youtube.com/watch?v=<?php echo $member['code']; ?>"
              data-poster="<?php echo $member['thumb']; ?>"
              data-sub-html='<h4><?php echo $member['title']; ?></h4><p><?php echo $member['description']; ?></p>'
          >
              <img
                  class="img-responsive"
                  src="<?php echo $member['thumb']; ?>"
              />
          </a>
        <?php } ?>
        </div>
        <div class="col-lg-12 rowgutter text-center"><span class="read-more"><?php echo $this->language->get('text_more_information'); ?></span></div>
      </div>
      <div id="images" class="tab-pane fade in">
        <?php foreach($pictures as $pic) { ?>
          <div id="light-image-<?php echo $pic['picture_id']; ?>">
          <?php foreach($pic['gallery_images'] as $key => $gallery_images) { ?>
            <a class="col-6 col-lg-4 rowgutter <?php echo $key == 0 ? '' : 'hide'; ?>" href="<?php echo $gallery_images['image']; ?>"><img src="<?php echo $gallery_images['thumb']; ?>" style="height: 220px; width: 100%;"/></a>
          <?php } ?>
          </div>
        <?php } ?>
        <div class="col-lg-12 rowgutter text-center"><span class="read-more"><?php echo $this->language->get('text_more_information'); ?></span></div>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>