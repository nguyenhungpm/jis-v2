<?php echo $header; ?>
<style type="text/css">
  .avatarin{width:120px;height:120px;margin-left:15px}
</style>
<div class="container">
    <div class="row clear">
    <div class="col-xs-12 col-sm-8">
    <h1 class="blue n-mb"><?php echo $heading_title; ?></h1>
    <div class="middle">
    <?php if ($testimonials) { ?>
      <?php foreach ($testimonials as $testimonial) { ?>
      <table class="col-xs-12" width="100%" style="border:none">
      <tr>
      	<td>
            <h3 class="n-mt red roboto"><?php echo $testimonial['title']; ?></h3>
              <?php echo $testimonial['description']; ?>
          </td>
      </tr>
     <tr style="border:none">
		      <td style="font-size: 0.9em; text-align: right;border:none">
                <?php if ($testimonial['rating']) { ?>
                  <img src="static/stars-<?php echo $testimonial['rating'] . '.png'; ?>" style="margin-top: 2px;" />
                  <?php } ?><br>
<i><?php echo $testimonial['name'].' - '.$testimonial['city'].' '.$testimonial['date_added']; ?></i>
            </td>
      </tr>
	   </table>
      <?php } ?>

    	<?php if ( isset($pagination)) { ?>
    		<div class="pagination small"><?php echo $pagination;?></div>

    	<?php }?>

    	<?php if (isset($showall_url)) { ?>
    		<div class="buttons" align="right"><a class="button" href="<?php echo $showall_url;?>" title="<?php echo $showall;?>"><span><?php echo $showall;?></span></a></div>
    	<?php }?>
    <?php } ?>
    </div>
    </div>
    <div class="col-xs-12 col-sm-4 rowgutter">
    <?php echo $column_right; ?>
  </div>
  </div>
  </div>

</div>
<?php echo $footer; ?>
