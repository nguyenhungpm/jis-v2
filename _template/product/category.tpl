<?php echo $header; ?>
<style>
  #tf-menu {
    background: #0a0f3c;
  }
  .blog-temp1 {
    margin-top: 100px;
  }
</style>
<section class="blog-temp1">
    <div class="container">
        <div class="row">
            <div class="col-md-6 fix-colum">
                <div class="tempLeft">
                    <a href="" class="hvr-grow"><img src="<?php echo $this->model_tool_image->cropsize($products[0]['image'], 546, 392); ?>" width="100%"></a>
                    <div class="box_temp">
                        <h4><a href="<?php echo $products[0]['href']; ?>"><?php echo $products[0]['name']; ?></a></h4>
                    </div>
                </div>
            </div>
            <div class="col-md-6 fix-colum">


                <div class="tempRight">
                    <a href=""><img src="<?php echo $this->model_tool_image->cropsize($products[1]['image'], 546, 193); ?>" width="100%"></a>
                    <div class="btn-temp">
                        <p><?php echo $products[1]['name']; ?></p>
                        <a href="<?php echo $products[1]['href']; ?>">Chi tiết</a>
                    </div>
                </div>



                <div class="row ">
                    <div class="col-md-6 fix-colum_v2">
                        <div class="tempRight">
                            <a href=""><img src="<?php echo $this->model_tool_image->cropsize($products[2]['image'], 271, 193); ?>" width="100%"></a>
                            <div class="btn-temp">
                                <p><?php echo $products[2]['name']; ?></p>
                                <a href="<?php echo $products[2]['href']; ?>">Chi tiết</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 fix-colum_v3">
                        <div class="tempRight">
                            <a href=""><img src="<?php echo $this->model_tool_image->cropsize($products[3]['image'], 271, 193); ?>" width="100%"></a>
                            <div class="btn-temp">
                                <p><?php echo $products[3]['name']; ?></p>
                                <a href="<?php echo $products[3]['href']; ?>">Chi tiết</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content-temp">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h4 class="head-title-blog"><?php echo $heading_title; ?></h4>
                  <?php for($i=1; $i< count($products); $i++){ ?>
                  <?php if($i%2==1){ ?><div class="row"><?php } ?>
                    <div class="col-md-6">
                        <div class="tLeft-v2">
                            <a href="" class="hvr-grow"><img src="<?php echo $products[$i]['thumb']; ?>" width="100%"></a>
                            <h4><a href="<?php echo $products[$i]['href']; ?>"><?php echo $products[$i]['name']; ?></a></h4>
                            <!-- <p>Entertainment</p> -->
                        </div>
                        <div class="categories-name">
                            <h4><a href="<?php echo $products[$i]['href']; ?>"><?php echo $products[$i]['name']; ?></a></h4>

                            <a class="timer" href=""><?php if(!empty($products[$i]['author'])){ echo $products[$i]['author']. " - "; } ?><time><?php echo $products[$i]['date_added']; ?></time></a>
                            <p><?php echo $products[$i]['short_description']; ?></p>
                        </div>

                    </div>
                  <?php if($i%2==0 || $i == count($products)-1){ ?></div><?php } ?>
                  <?php } ?>
                  <?php echo $pagination; ?>
            </div>

            <div class="col-md-4">
              <h4 class="head-title-blog">
                  Nổi bật
              </h4>
              <?php echo $column_right; ?>
            </div>
        </div>
    </div>
</section>
<section class="content-bottom">
    <div class="container">
      <?php echo $content_bottom; ?>
    </div>
</section>

<?php echo $footer; ?>
