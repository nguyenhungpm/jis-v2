<?php echo $header; ?>
<div class="rowgutter breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
	<div class="container">
		    <?php foreach ($breadcrumbs as $key=>$breadcrumb) { ?>
		    	<?php echo $breadcrumb['separator']; ?><span itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo $breadcrumb['href']; ?>"><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
		    	    <meta itemprop="position" content="<?php echo $key+1; ?>" />
		    	</a></span>
		    <?php } ?>
	</div>
</div>
<div id="bodywrap"><div class="container"><div class="row clear">
<div class="content">
  <h1 class="title col-xs-12"><?php echo $heading_title2; ?></h1>
	<div class="description">
			<?php echo $manufacturer_description; ?>
	</div>
    <?php if ($products) { ?>
	    <?php $k=0; foreach ($products as $product) {$k++; ?>

		<?php if($k%4==1){ ?>
		<div>
		<?php } ?>

		<div class="col-6 col-desktop-3 col-sm-3 text-center product-item">
	      <?php if ($product['thumb']) { ?>
	      	<a href="<?php echo $product['href']; ?>"><img class="border" src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" /></a>
	      	<?php } ?>
	      <h3 class="name medium"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h3>
	    </div>


		<?php if($k%4==0 || $k==count($products)){ ?>
		</div>
		<div class="col-xs-12"><div class="line dash"></div></div>
		<?php } ?>


	    <?php } ?>
	  <div class="pagination col-xs-12"><?php echo $pagination; ?></div>
  <?php } else { ?>
  <div class="content col-xs-12"><?php echo $text_empty; ?></div>
  <?php }?>
</div>
</div></div></div>
<?php echo $footer; ?>
