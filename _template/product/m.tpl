<?php echo $header; ?>
<div class="banner-oteam relative">
	<img src="media/data/member/our-team.jpg"/>
	<div class="odescription">
		<div class="container">
			<h1 class="htitle">Đội ngũ SenPlus</h1>
			<div class="row">
			<div class="col-lg-5 gutter">
				<p>Chúng tôi hiểu rằng, khi mỗi cá nhân trở thành tinh hoa là khi khách hàng sẽ được cung cấp sản phẩm, dịch vụ chuẩn mực, chân thành và chuyên nghiệp.</p>
				<p>Đầu tư cho Thành tích cá nhân là hướng tới Thành công tập thể.</p>
			</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row clearfix rowgutter">
		<?php foreach ($manufacturers as $key=>$manufacturer) { ?>
			<div class="col-lg-3">
				<div class="gutter">
				<div class="m-items">
					<a class="block" href="<?php echo $manufacturer['href']; ?>"><img src="<?php echo $manufacturer['thumb']; ?>" alt="<?php echo $manufacturer['name']; ?>" class="block" /></a>
					<div class="desc">
						<h3><a href="<?php echo $manufacturer['href']; ?>" class="color-black"><?php echo $manufacturer['name']; ?></a></h3>
						<span><?php echo $manufacturer['description']; ?></span>
					</div>
				</div>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="gutter">
				<div class="m-items">
					<a class="block" href="<?php echo $manufacturer['href']; ?>"><img src="<?php echo $manufacturer['thumb']; ?>" alt="<?php echo $manufacturer['name']; ?>" class="block" /></a>
					<div class="desc">
						<h3><a href="<?php echo $manufacturer['href']; ?>" class="color-black"><?php echo $manufacturer['name']; ?></a></h3>
						<span><?php echo $manufacturer['description']; ?></span>
					</div>
				</div>
				</div>
			</div>
		<?php } ?>
	</div>
	<div class="row">
		<div class="col-lg-12 join-sp">
			<h1 class="htitle">Tham gia SenPlus</h1>
			<p class="main-color gutter">Làm việc tại Senplus có nghĩa là bạn đã sẵn sàng cùng chúng tôi, kể tiếp những câu chuyện truyền cảm hứng về niềm tin, tình yêu và tinh thần mạnh mẽ của Thương hiệu?</p>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 join-sp">
			<h2 class="gutter bold"><?php echo $this->language->get('text_apply_now'); ?></h2>
			<ul class="apply">
				<?php foreach($recruitments as $recruitment){ ?>
				<li>
					<a href="<?php echo $recruitment['href']; ?>">
						<div class="row">
							<div class="col-lg-6"><b><?php echo $recruitment['name']; ?></b></div>
							<div class="col-lg-3"><b><?php echo $recruitment['local']; ?></b></div>
							<div class="col-lg-3"><b><?php echo $recruitment['deadline']; ?></b></div>
						</div>
					</a>
				</li>
				<?php } ?>
			</ul>
		</div>
	</div>
</div>
<?php echo $footer; ?>
