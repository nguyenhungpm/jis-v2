<?php echo $header; ?>
<div class="rowgutter breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
	<div class="container">
		    <?php foreach ($breadcrumbs as $key=>$breadcrumb) { ?>
		    	<?php echo $breadcrumb['separator']; ?><span itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo $breadcrumb['href']; ?>"><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
		    	    <meta itemprop="position" content="<?php echo $key+1; ?>" />
		    	</a></span>
		    <?php } ?>
	</div>
</div>
<div id="bodywrap"><div class="container"><div class="row clear">
<div class="content">
  <div class="col-xs-12 col-desktop-9 col-sm-9">
  <h3 class="title"><?php echo $heading_title; ?></h3>
  <?php if($products){ ?>
	   <span><?php echo $text_add_product; ?>:</span>
  <select id="select">
  <option>---<?php echo $text_select_product; ?>---</option>
  <?php foreach($products as $rs){ ?>
	<option value="<?php echo $rs['product_id']?>"><?php echo $rs['name']?></option>
	<?php } ?>
  </select>
  <script type="">
	var select_element = document.getElementById("select");
	select_element.onchange = function(e){
    if (!e)
        var e = window.event;
    var svalue = this.options[this.selectedIndex].value;
	var url = '<?php echo $url_order?>_'+svalue;
	url = url.replace(/&amp;/gi,"&");
	window.location.href = url;
	}
	</script>
	<?php } ?>
  <?php if($products_url){ ?>
  <div class="cart-info">
       <table>
	    <thead>
          <tr>
            <td class="image"><?php echo $text_image; ?></td>
            <td class="name"><?php echo $text_product_name; ?></td>
            <td class="model"><?php echo $text_model; ?></td>
            <td class="total"></td>
          </tr>
        </thead>
		<tbody>
	    <?php foreach($products_url as $product) { ?>
	    <tr>
    	   <td class="image">
	      <?php if ($product['thumb']) { ?>
	      	<a target="_blank" href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" /></a>
	      	<?php } ?>
	      </td>
	      <td class="name"><a target="_blank" href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></td>
	      <td class="model"><?php echo $product['model']; ?></td>
		  <td class="small text-center"><a onClick="remove_('<?php echo $product['product_id'];?>')"><?php echo $text_delete; ?></a></td>
	    </tr>
	    <?php } ?>
		</tbody>
	  </table>
	</div>
	  <script>
	function remove_(product_id){
	var url = '<?php echo $url_order?>';
	url = url.replace('_'+product_id,"");
	url = url.replace(/&amp;/gi,"&");
	window.location.href = url;
	}
	</script>
	  <?php } ?>


  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
    <h5 class="title"><?php echo $text_information; ?></h5>
    <div class="contactpage">
    <?php if ($error_name) { ?>
    <span class="error"><?php echo $error_name; ?></span>
    <?php } ?>
    <?php if ($error_email) { ?>
    <span class="error"><?php echo $error_email; ?></span>
    <?php } ?>
    <div class="row clear">
    	<div class="col-6 no-left">
    		<input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_name; ?> *" />
    	</div>
    	<div class="col-6 no-right">
	    	<input type="text" name="phone2" value="<?php echo $phone; ?>" placeholder="<?php echo $text_phone; ?>" />
    	</div>
    </div>
    <div class="row clear">
		<div class="col-6 no-left">
		    <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?> *" />
		</div>
		<div class="col-6 no-right">
			<input type="text" name="address2" value="<?php echo $address2; ?>" placeholder="<?php echo $text_address2; ?>" />
		</div>
    </div>
    <textarea name="enquiry" cols="30" rows="4" placeholder="<?php echo $entry_enquiry; ?>"><?php echo $enquiry; ?></textarea>
    <br />
    <?php if ($error_enquiry) { ?>
    <span class="error"><?php echo $error_enquiry; ?></span>
    <?php } ?>
    <?php if ($error_captcha) { ?><br class="show-mobile hide-desktop">
    <span class="error"><?php echo $error_captcha; ?></span>
    <?php } ?>

    <input placeholder="<?php echo $entry_captcha; ?>" type="text" id="captcha" name="captcha" value="<?php echo $captcha; ?>" />
    <img src="index.php?route=information/contact/captcha" alt="" class="right"/>
    <br />
    <input type="submit" style="width:230px;text-transform:uppercasse" value="<?php echo $text_checkout; ?>" class="button right" />
        <br />
            <br />
    </div>

  </form>
  </div>
  <div class="col-xs-12 col-desktop-3 col-sm-3">
    <?php echo $column_right; ?>
  </div>
</div>
</div></div></div>
<?php echo $footer; ?>
