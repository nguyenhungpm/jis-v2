<?php echo $header; ?>
<style>
  #tf-menu {
    background: #0a0f3c;
  }
  .blog-temp1 {
    margin-top: 100px;
  }
</style>
<section class="blog-temp1">
    <div class="container">
			<div class="row">
				<div class="col-md-8">
					<h4 class="head-title-blog"><?php echo $heading_title; ?></h4>
					<?php for($i=1; $i< count($search_rs); $i++){ ?>
					<?php if($i%2==1){ ?><div class="row"><?php } ?>
						<div class="col-md-6">
								<div class="tLeft-v2">
										<a href="" class="hvr-grow"><img src="<?php echo $search_rs[$i]['thumb']; ?>" width="100%"></a>
										<h4><a href="<?php echo $search_rs[$i]['href']; ?>"><?php echo $search_rs[$i]['name']; ?></a></h4>
										<!-- <p>Entertainment</p> -->
								</div>
								<div class="categories-name">
										<h4><a href="<?php echo $search_rs[$i]['href']; ?>"><?php echo $search_rs[$i]['name']; ?></a></h4>

										<a class="timer" href=""><?php if(!empty($search_rs[$i]['author'])){ echo $search_rs[$i]['author']. " - "; } ?><time><?php echo $search_rs[$i]['date_added']; ?></time></a>
										<p><?php echo $search_rs[$i]['short_description']; ?></p>
								</div>

						</div>
					<?php if($i%2==0 || $i == count($search_rs)-1){ ?></div><?php } ?>
					<?php } ?>
					<?php echo $pagination; ?>
				</div>
				<div class="col-md-4">
					<h4 class="head-title-blog">
							Nổi bật
					</h4>
					<?php echo $column_right; ?>
				</div>
		</div>
</section>
<?php echo $footer; ?>
