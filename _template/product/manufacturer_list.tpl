<?php echo $header; ?>
<div id="fakediv" class="d-none"></div>
<div class="rowgutter grey small uppercase breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
	<div class="container">
		    <i class="icon-icons-47"></i> <?php foreach ($breadcrumbs as $key=>$breadcrumb) { ?>
		    	<?php echo $breadcrumb['separator']; ?><span itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo $breadcrumb['href']; ?>"><span itemprop="name" class="grey"><?php echo $breadcrumb['text']; ?></span>
		    	    <meta itemprop="position" content="<?php echo $key+1; ?>" />
		    	</a></span>
		    <?php } ?>
	</div>
</div>
<div id="testimonial" class="p-bot-0 bg-white">
	<div class="section-body container">
	<?php foreach($manucats as $cat) { if (count($cat['manufacturers']) == 0) continue; ?>
		<h2 class="h4 title-section title-decoration"><?php echo $cat['name']; ?></h2>
		<div class="clearfix row">
		<?php foreach($cat['manufacturers'] as $manufacturer) { ?>
			<div class="col-lg-3 rowgutter">
			<a class="item" href="javascript:" data-id="<?php echo $manufacturer['manufacturer_id']; ?>" data-cat_id="<?php echo $cat['manucat_id']; ?>" data-cat_name="<?php echo $cat['name']; ?>" itemprop="liveBlogUpdate" itemscope="" itemtype="http://schema.org/BlogPosting">
				<img src="<?php echo $manufacturer['thumb']; ?>" class="lazy" alt="<?php echo $manufacturer['title']; ?>" itemprop="thumbnail" style="">
				<div class="item-inner flex-center">
					<div class="text">
					<h3 class="h4 text-bold text-primary" itemprop="author"><?php echo $manufacturer['title']; ?></h3>
					<h4 class="h text-bold text-white"><?php echo $manufacturer['position']; ?></h4>
					</div>
				</div>
			</a>
			</div>
		<?php } ?>
		</div>
	<?php } ?>
   </div>
</div>
<div id="testimonial-popup" class="bg-lightgray">
	<div class="popup-header bg-white">
		<div class="flex-center container-fluid unclearfix">
			<h4 class="h3 title-section"><?php echo $this->language->get('text_jis_staff'); ?></h4>
			<button type="button" class="js-close"><i class="demo-icon ecs-close"></i></button>
		</div>
	</div>
	<div class="popup-body container">
		<a href="javascript:" class="p-top-4 p-bot-2 btn-back text-bold js-close" id="cat-name"><?php echo $this->language->get('text_back'); ?></a>
		<div id="testimonial-js">

		</div>
	</div>
</div>
<?php echo $footer; ?>
