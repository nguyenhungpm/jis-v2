<?php echo $header; ?>
<div id="fakediv" class="d-none"></div>
<section class="section section-whychoose" id="benefits" style="background-image: url('/media/<?php echo $category_info['image']; ?>'); background-position: center; background-repeat: no-repeat; background-size: cover;">
   <div class="container">
      <div class="whychoose__head section__head">
         <h2 class="section--title"><?php echo $category_info['name']; ?></h2>
         <div class="section--line"></div>
      </div>
   </div>
</section>
<nav class="vsc-top-nav ">
    <div class="container">
        <ul class="nav">
			<?php foreach($products as $product) { ?>
				<li class="page_item nav-item <?php echo $product['product_id'] == $product_id ? 'current_page_item':'';?> nav-item"><a class="nav-link" href="<?php echo $this->url->link('product/product', 'product_id=' . $product['product_id'])?>" aria-current="page"><?php echo $product['name']; ?></a></li>
            <?php } ?>
            <li class="page_item nav-item nav-item <?php echo $category_info['display_gallery'] == 1 ? '':'hide';?>"><a class="nav-link" href="videos/videos" aria-current="page"><?php echo $this->language->get('text_photo_video'); ?></a></li>
        </ul>
    </div>
</nav>
<div class="rowgutter grey small uppercase breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
	<div class="container">
		    <i class="icon-icons-47"></i> <?php foreach ($breadcrumbs as $key=>$breadcrumb) { ?>
		    	<?php echo $breadcrumb['separator']; ?><span itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo $breadcrumb['href']; ?>"><span itemprop="name" class="grey"><?php echo $breadcrumb['text']; ?></span>
		    	    <meta itemprop="position" content="<?php echo $key+1; ?>" />
		    	</a></span>
		    <?php } ?>
	</div>
</div>
<div class="container">
<div class="clear row">
    <div class="col-xs-12 col-sm-8 border-right">
		<h1 class="darkblue n-mt"><?php echo $heading_title; ?><?php echo $admin_logged ? ' (<a target="_blank" href="'.$edit.'">Sửa</a>)': ''?></h1>
		<div class="newscontent">
			  <?php echo $description; ?>
		</div>
		<?php echo $column_left; ?>
	</div>
	<div class="col-xs-12 col-sm-4 rowgutter">
		<div class="col-right">
			<?php echo $handbook; ?>
		</div>
		<?php echo $column_right; ?>
	</div>
</div>
</div>
<?php echo $footer; ?>
