<?php echo $header; ?>
<div class="rowgutter grey small uppercase breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
	<div class="container">
		    <i class="icon-icons-47"></i> <?php foreach ($breadcrumbs as $key=>$breadcrumb) { ?>
		    	<?php echo $breadcrumb['separator']; ?><span itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo $breadcrumb['href']; ?>"><span itemprop="name" class="grey"><?php echo $breadcrumb['text']; ?></span>
		    	    <meta itemprop="position" content="<?php echo $key+1; ?>" />
		    	</a></span>
		    <?php } ?>
	</div>
</div>
<div class="container">
	<div class="row clear parent">
		<div class="col-xs-12 col-sm-7 rowgutter">
			<h1 class="n-mt blue title_module_heading h3"><?php echo $heading_title; ?></h1>
			<?php if(isset($description)) {?>
			<div class="grey">
				<?php echo $description; ?>
			</div>
			<?php } ?>
			<?php if ($products) { ?>
			<div class="row clear">
			<?php foreach ($products as $product) { ?>
			<div class="col-xs-12 col-sm-6 product-item text-center gutter">
				<a class="block relative thumbc" href="<?php echo $product['href'];?>">
					<img src="<?php echo $product['thumb'];?>" alt="<?php echo $product['name'];?>" class="full-width"/>
				</a>
				<h3 class="h4 strong n-mb"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h3>
			</div>
			<?php } ?>
			<div class="pagination col-xs-12"><?php echo $pagination; ?></div>
			</div>
			<?php } ?>
		</div>
		<div class="col-xs-12 col-sm-5 left-col2 rowgutter">
				<?php echo $column_left; ?>
		</div>
	</div>
</div>
<?php echo $footer; ?>
