<?php echo $header; ?>
<div class="rowgutter small uppercase grey breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
	<div class="container">
		    <i class="icon-icons-47"></i> <?php foreach ($breadcrumbs as $key=>$breadcrumb) { ?>
		    	<?php echo $breadcrumb['separator']; ?><span itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo $breadcrumb['href']; ?>"><span itemprop="name" class="grey"><?php echo $breadcrumb['text']; ?></span>
		    	    <meta itemprop="position" content="<?php echo $key+1; ?>" />
		    	</a></span>
		    <?php } ?>
	</div>
</div>
<div class="container">
<div class="row clearfix">
<div class="col-xs-12 col-sm-7">
	<div class="product-page row clear">
				<div class="col-xs-12">
					<h1><?php echo $heading_title; ?><?php echo $admin_logged ? ' (<a target="_blank" href="'.$edit.'">Sửa</a>)': ''?></h1>
					<img style="width: 100%" src="<?php echo $thumb ;?>" alt="<?php echo $heading_title; ?>" />
					<?php if(strlen($short_description)>3) {?>
						<div class="h4 lh15 mid-color short_description"><?php echo $short_description; ?></div>
						<hr class="line" />
					<?php } ?>
				</div>
						<div class="col-xs-12">
							<?php if (isset($news_relateds)) { ?>
							<div class="othernews right">
								<h3 class="heading col-xs-12"><?php echo $tab_related_news; ?></h3>
								<ul class="n--p n--m">
									<?php foreach ($news_relateds as $news_item) { ?>
										<li><a href="<?php echo $news_item['href']; ?>"><?php echo $news_item['name']; ?></a></li>
									<?php } ?>
								</ul>
							</div>
							<?php } ?>
							<div class="description newscontent">
								<?php echo $description; ?>
							</div>
							<?php if($images){ ?>
								<h3>Thư viện ảnh</h3>
							<?php foreach($images as $image){ ?>
							<a href="<?php echo $image['popup'];?>" class="col-6 rowgutter col-destkop-2 col-sm-3">
								<img src="<?php echo $image['thumb'];?>" class="thumb block" alt="<?php echo $heading_title; ?>" />
							</a>
							<?php } ?>
							<?php } ?>
							<div class="shareme rowgutter small">
								<div class='movie_choice right small'>
								 <?php if($rate['total_rate']==0){
										$rate['total_rate'] = 1;
										$rate['total_point'] = 5;
								 }?>
									 <div id="r1" class="rate_widget" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
										 <div id="s1" onclick="rate('1','product','<?php echo $product_id; ?>')" class="star_1 ratings_stars <?php echo (round($average,0) >= 1) ? 'ratings_vote':''?>"></div>
										 <div id="s2" onclick="rate('2','product','<?php echo $product_id; ?>')" class="star_2 ratings_stars <?php echo (round($average,0) >= 2) ? 'ratings_vote':''?>"></div>
										 <div id="s3" onclick="rate('3','product','<?php echo $product_id; ?>')" class="star_3 ratings_stars <?php echo (round($average,0) >= 3) ? 'ratings_vote':''?>"></div>
										 <div id="s4" onclick="rate('4','product','<?php echo $product_id; ?>')" class="star_4 ratings_stars <?php echo (round($average,0) >= 4) ? 'ratings_vote':''?>"></div>
										 <div id="s5" onclick="rate('5','product','<?php echo $product_id; ?>')" class="star_5 ratings_stars <?php echo (round($average,0) >= 5) ? 'ratings_vote':''?>"></div>
										 <span id="point_rs" class="text-right clearfix block">Có <span itemprop="reviewCount"><?php echo $rate['total_rate']; ?></span> bầu chọn /
												 điểm trung bình: <span itemprop="ratingValue"><?php echo round($average,1); ?></span>
										</span>
									 </div>
									 <div id="total_vote"></div>
									 <input type="hidden" id="point" value=""/>
								 </div>

								<a href="https://plus.google.com/share?url=<?php echo $url; ?>" target="_blank" class="plus-icon inline-block round" rel="nofollow"><i class="icn i-plus"></i> Google+</a>
								<a href="http://facebook.com/share.php?u=<?php echo $url; ?>" target="_blank" class="face-icon round inline-block" rel="nofollow"><i class="icn i-scl-facebook"></i> Chia sẻ</a>
					</div>
							<?php echo $content_bottom; ?>
						</div>
					</div>
</div>
<div class="col-xs-12 col-sm-5 rowgutter left-col2">
		<?php echo $column_left; ?>
</div>
</div>
		<?php if ($products) { ?>
		<div class="rowgutter relative">
			<div class="title_under_center text-center"><h3 class="h4 uppercase n-mb mid-color"><?php echo $tab_related; ?></h3><span class="decor"><span class="inner"></span></span></div>
			<div class="row clear product-home" id="related_product">
			<?php foreach ($products as $key=>$product) { ?>
				<div class="product-item col-xs-6 col-sm-3">
						<a class="block relative thumbc" href="<?php echo $product['href'];?>"><img src="<?php echo $product['thumb'];?>" alt="<?php echo $product['name'];?>" class="full-width"/></a>
	          <h3 class="h4 text-center n-mb"><a class="t-d-none" href="<?php echo $product['href'];?>"><?php echo $product['name'];?></a></h3>
				</div>
			<?php } ?>
			</div>
		</div>
		<?php } ?>
</div>
<?php echo $footer; ?>
