<!-- Slider or background header -->
<div id="main-slider">
		<div class="slider-1item index-slider owl-carousel owl-theme">
			<?php foreach ($banners as $key=> $banner) { ?>
				<a class="item" href="<?php echo $banner['link']; ?>">
					<img data-src="<?php echo $banner['image']; ?>" class="owl-lazy">
				</a>
			<?php } ?>
		</div>
</div>
<!-- End slider or background header -->