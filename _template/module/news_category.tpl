<?php if($position == 'content_top' || $position == 'content_bottom'){ ?>
<!-- Works Section
================================================== -->
<div id="tf-works">
    <div class="container">
        <div class="section-header">
            <h2 class="wow fadeInUp top__element" data-wow-delay="0.3s" data-wow-duration="1.5s">Các tin tức khác</h2>
            <h5><em>We design and build functional and beautiful websites</em></h5>
            <div class="fancy"><img src="img/spr.png" alt=""></div>
        </div>

        <!-- TABS VERTICAL -->

        <div class="tabs-v">
          <?php foreach ($categories as $key => $category) { ?>
            <input type="radio" name="tabs-v" id="tab<?php echo $category['news_category_id']; ?>-v" <?php echo $key==0 ? 'checked': ''; ?>>
            <label for="tab<?php echo $category['news_category_id']; ?>-v">
                <img src="img/icon-4.png">
                <p><?php echo $category['name']; ?></p>
            </label>
            <div class="tab-content">
                <div>
                    <div class="txt-tab">
                        <div>
                            <h3><?php echo $category['name']; ?></h3>
                            <p><?php echo $category['description']; ?></p>
                            <div class="my-btn-container"><a class="my-btn" href="<?php echo $category['href']; ?>">Các bài viết</a></div>
                        </div>
                    </div>
                    <div class="img-tab1 fix-img">
                        <img src="<?php echo $category['thumb']; ?>" alt="<?php echo $category['name']; ?>">
                    </div>
                </div>
            </div>
          <?php } ?>
        </div>
    </div>
</div>
<?php }else{ ?>
<div class="categories--Right">
    <h2 class="h4 title-section title-decoration"><?php echo $heading_title; ?></h2>
    <ul class="rowgutter">
        <?php foreach ($categories as $key => $category) { ?>
        <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
        <?php } ?>
    </ul>
</div>
<?php } ?>
