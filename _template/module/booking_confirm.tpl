<form action="module/booking/booking" id="confirm_booking_form" method="post">
	<div class="form-group">
		<div class="row">
			<div class="col-md-6">
				<label for="origins">Điểm đón</label>
				<input type="text" class="form-control autocomplete" id="origins" value="<?php echo $booking['origin']; ?>" name="origins"> 
			</div>
			<div class="col-md-6">
				<label for="destinations">Điểm đến</label>
				<input type="text" class="form-control autocomplete" id="destinations" value="<?php echo $booking['destination']; ?>" name="destinations"> 
			</div>
		</div>
  </div>
	<div class="form-group">
		<div class="row">
			<div class="col-md-6">
				<label for="time">Thời gian</label>
				<input type="text" class="form-control" id="time" value="<?php echo $booking['time']; ?>" name="time"> 
			</div>
			<div class="col-md-6">
				<label for="origins">Chọn loại xe</label>
				<select class="form-control" name="type_car">
					<option disabled selected value="">Chọn loại xe</option>
					<?php foreach($cars as $car){ ?>
					<option <?php echo $car['type_car_id']== $booking['type_car'] ? 'selected': ''; ?> value="<?php echo $car['seat']?>"><?php echo $car['name']?></option>
					<?php }?>
				</select>
			</div>
		</div>
  </div>
	<div class="form-group">
	<div class="row">
		<div class="col-md-6">
			<span class="hidden-md visible-xs-block title_form">Phương thức di chuyển</span>
			<fieldset>
				<div class="toggle">
					<input type="radio" name="method" value="1" id="1chieu" <?php echo $booking['method']=='1' ? 'checked': ''; ?> />
					<label for="1chieu">1 chiều</label>
					<input type="radio" name="method" value="2" id="2chieu" <?php echo $booking['method']=='2' ? 'checked': ''; ?> />
					<label for="2chieu">2 chiều</label>
				</div>
			</fieldset>
		</div>
		<div class="col-md-6">
			<span class="hidden-md visible-xs-block title_form">Loại hình</span>
			<fieldset>
				<div class="toggle">
					<input type="radio" name="transfer_type" value="tinh" id="tinh" <?php echo $booking['transfer_type']=='tinh' ? 'checked': ''; ?> />
					<label for="tinh">Đi tỉnh</label>
					<input type="radio" name="transfer_type" value="tienchuyen" id="tienchuyen" <?php echo $booking['transfer_type']=='tienchuyen' ? 'checked': ''; ?>/>
					<label for="tienchuyen">Tiện chuyến</label>
				</div>
			</fieldset>
		</div>
  </div>
  </div>
	<div class="form-group">
		Cước phí: <span><?php echo number_format($booking['price'],0,",","."); ?> vnđ</span><br />
		Quãng đường: <span><?php echo $booking['distance']; ?></span>
		<input name="price" value="<?php echo $booking['price'];?>" type="hidden"/>
		<input name="distance" value="<?php echo $booking['distance'];?>" type="hidden"/>
	</div>
	<div class="form-group">
		<div class="row">
			<div class="col-md-6">
				<label for="name">Họ và tên</label>
				<input type="text" class="form-control" id="name" name="name" />
			</div>
			<div class="col-md-6">
				<label for="telephone">Số điện thoại</label>
				<input type="text" class="form-control" id="telephone" name="telephone" />
			</div>
		</div>
	</div>
	<div class="form-group">
    <label for="note">Ghi chú</label>
		<textarea class="form-control" id="note" name="note"></textarea>
  </div>
</form>