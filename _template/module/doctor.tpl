<div class="zonegutter doctor relative">
<div class="container">
	<h2 class="h1 uppercase text-center darkred title_under_center"><?php echo $heading_title; ?></h2>
	<div id="doctors" class="row clear">
	<?php $k=0; foreach ($doctors as $doctor) { ?>
		<div class="col-xs-12 col-sm-3 zonegutter">
			<div class="doctor-item text-center">
				<img class="inline-block circle " alt="<?php echo $doctor['name']; ?>" src="<?php echo $doctor['thumb']; ?>" />
				<h3 class="n-mb"><?php echo $doctor['name']; ?></h3>
				<?php if(strlen($doctor['description'])>2){?>
				<div class="h4 gray"><?php echo $doctor['description'];?></div>
				<?php } ?>
			</div>
		</div>
	<?php $k++;} ?>
	</div>
</div>
<?php if(count($doctors)>4){ ?>
	<a href="javascript:;" class="PrevDoctor absolute"></a>
	<a href="javascript:;" class="NextDoctor absolute"></a>
<?php } ?>
</div>
