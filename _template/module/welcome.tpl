<?php if($position == 'content_bottom' && $layout_id =='1'){ ?>

<!-- Process Section
    ================================================== -->
    <div id="tf-process">
        <div class="container"> <!-- container -->
            <div class="section-header">
                <h2 class="wow fadeInUp top__element" data-wow-delay="0.3s" data-wow-duration="1.5s">Một vài lý do vì sao gần 2 triệu người đã chọn </h2>
                <h5><em>We design and build functional and beautiful websites</em></h5>
                <div class="fancy"><img src="img/spr.png" alt=""></div> 
            </div>
        </div><!-- end container -->
        <div class="gray-bg"> <!-- fullwidth gray background -->
            <div class="container"><!-- container -->
                <div id="process" class="row"> <!-- row -->
                    <div class="col-md-6">
                        <?php foreach($this->config->get('config_introduce') as $intro){ ?>
                        <div class="media process">
                            <div class="media-right media-middle wow zoomIn animated">
                                <i class="<?php echo $intro['icon']; ?>"></i>
                            </div>
                            <div class="media-body media-v2 media-v3">
                                <h4 class="media-heading"><a href=""><?php echo $intro['title']; ?></a></h4>
                                <p><?php echo $intro['description']; ?></p>
                            </div>
                        </div>
                        <?php } ?>
                    </div>

                    <div class="col-md-6 pers">
                        <div class="video-iframe">
                            <img src="img/Mac.png" >
                            <iframe  src="https://www.youtube.com/embed/<?php echo $this->config->get('config_introduce_video'); ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                    </div>
                </div> <!-- end row -->
            </div><!-- end container -->
        </div>  <!-- end fullwidth gray background -->
    </div>
<?php } else{ ?>
<?php echo $message; ?>
<?php } ?>