<?php if ($position == 'content_top'  or $position == 'content_bottom') { ?>

<section id="productearly" class="bg-white">
	<div class="container">
			<div class="section-header text-center w-70 p-bot-4">
					<h2 class="h0 title-section text-second p-bot-4"><?php echo $newsfeatured_title; ?></h2>
			</div>
			<div class="row clearfix">
				<div class="col-12 col-lg-8">
					<a href="<?php echo $newss[0]['href']; ?>" class="img-container">
						<img data-src="<?php echo $this->model_tool_image->cropsize($newss[0]['image'], 860, 450); ?>" src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" class="lazy" alt="<?php echo $newss[0]['name']; ?>" itemprop="image">
					</a>
				</div>
				<div class="col-12 col-lg-4">
					<div class="text-container">
						<h3 class="h3 title-section" itemprop="name"><a href="<?php echo $newss[0]['href']; ?>" itemprop="url"><?php echo $newss[0]['name']; ?></a></h3>
						<div class="date h5"><?php echo $newss[0]['date_added']; ?></div>
						<div class="des h5 ">
							<?php echo $newss[0]['short_description']; ?>
						</div>
					</div>
				</div>
			</div>
			<br />
			<div class="section-body flex-container col-3item">
				<?php foreach ($newss as $key => $news) {if($key == 0) continue; ?>
					<div class="item-product" itemscope itemtype="http://schema.org/Product">
						<a href="<?php echo $news['href']; ?>" class="img-container">
								<img data-src="<?php echo $news['thumb']; ?>" src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" class="lazy" alt="<?php echo $news['name']; ?>" itemprop="image">
						</a>
						<div class="text-container">
								<h3 class="h3 title-section" itemprop="name"><a href="<?php echo $news['href']; ?>" itemprop="url"><?php echo $news['name']; ?></a></h3>
								<div class="date h5"><?php echo $news['date_added']; ?></div>
								<div class="des h5 ">
									<?php echo $news['short_description']; ?>
								</div>
						</div>
					</div>
				<?php } ?>
			</div>
			<div class="section-footer text-center m-top-8">
				<a href="news/blog" class="" style="font-size: 26px;">
					<?php echo $this->language->get('text_more_information'); ?> &nbsp;
					<div class="chevron-arrow-right"></div>
					<div class="chevron-arrow-right"></div>
					<div class="chevron-arrow-right"></div>
				</a>
			</div>
	</div>
</section>	
<!-- /#blogstyle -->
<?php }else if($position=='column_right'){ ?>
	<h2 class="h4 title-section title-decoration"><?php echo $heading_title; ?></h2>
  	<div class="rowgutter news-right">
		<?php foreach ($newss as $news) { ?>
		<div class="post_item clearfix">
			<div class="post_img">
				<a href="<?php echo $news['href'];?>"><img src="<?php echo $news['thumb'];?>" width="100%"></a>
			</div>
			<div class="post_content1">
				<p><a href="<?php echo $news['href'];?>"><?php echo $news['name'];?></a></p>
				<span><?php echo $news['date_added'];?></span>
			</div>
		</div>
		<?php } ?>
  	</div>
<?php }else{ ?>
<div class="othernews">
	<h3 class="h4 strong no-margin rowgutter"><?php echo $heading_title; ?></h3>
	<?php foreach ($newss as $news) { ?>
		<div class="clear item">
			<a class="col-3 no-padding" href="<?php echo $news['href'];?>"><img class="block" src="<?php echo $news['thumb'];?>" alt="<?php echo $news['name'];?>"/></a>
			<a class="col-9 color-black no-padding-right" href="<?php echo $news['href'];?>"><?php echo $news['name'];?></a>
		</div>
	<?php } ?>
</div>
<?php } ?>
