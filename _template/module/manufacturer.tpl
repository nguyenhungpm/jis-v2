<div class="loicih">
<div class="zonegutter container">
  <h2 class="h1 uppercase text-center darkred title_under_center"><?php echo $heading_title; ?></h2>
    <?php if ($loiichs) { ?>
    <div class="loiich-box relative">
    <div class="loiich_col absolute"></div>
		<?php foreach ($loiichs as $key=>$loiich) { ?>
			<div class="row clear rowgutter relative">
            <div class="loiich_circle circle absolute"></div>
            <div class="col-xs-12 col-sm-6 <?php if($key%2==0){echo 'right'; } ?>">
                <div class="loiich_img"><img src="<?php echo $loiich['thumb']; ?>" class="round" alt="<?php echo $loiich['name']; ?>" /></div>
            </div>
            <div class="col-xs-12 col-sm-6">
              <div class="loiich_wrap rowgutter <?php if($key%2==0){echo 'loiich_left'; }else{echo 'loiich_right';} ?>"">
              	<h3><?php echo $loiich['name']; ?></h3>
                <?php echo $loiich['description']; ?>
              </div>
            </div>
      </div>
		<?php } ?>
        <div class="text-center"><a class="dathang strong button round relative red" href="contact.html">ĐẶT HÀNG</a></div>
    </div>
    <?php } ?>
</div>
</div>
