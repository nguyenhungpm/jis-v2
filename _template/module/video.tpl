<style>
@import url('https://fonts.googleapis.com/css2?family=Dancing+Script:wght@400;500;600;700&display=swap');
</style>
<style>
div,
span,
p,
blockquote,
cite,
i {
  margin: 0;
  padding: 0;
  border: 0;
  font-size: 100%;
  font: inherit;
  vertical-align: baseline;
}
blockquote {
  quotes: none;
}
blockquote:before,
blockquote:after {
  content: "";
  content: none;
}
.g-0 {
  --bs-gutter-x: 0;
}
.g-0 {
  --bs-gutter-y: 0;
}
@media (min-width: 1200px) {
  .col-xl-6 {
    -webkit-box-flex: 0;
    -moz-box-flex: 0;
    -ms-flex: 0 0 auto;
    flex: 0 0 auto;
    width: 50%;
  }
}
.justify-content-center {
  -webkit-box-pack: center !important;
  -moz-box-pack: center !important;
  -ms-flex-pack: center !important;
  justify-content: center !important;
}
.p-0 {
  padding: 0 !important;
}
.fp-section {
  position: relative;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}
.fp-auto-height.fp-section {
  height: auto !important;
}
blockquote {
  display: block;
  position: relative;
  text-align: center;
}
blockquote i {
  display: block;
  font-size: 7rem;
  line-height: 0.8;
  color: #272f7c;
}
blockquote p {
  font-family: "Dancing Script", sans-serif;
  margin: 2rem 0 0 0;
}
blockquote p span {
  font-weight: 900;
}
blockquote .divider {
  display: block;
  height: 15px;
  margin: 1.4rem 0 0 0;
  background-repeat: no-repeat;
  background-position: center;
  background-image: url("https://mixdesign.club/themeforest/punchy/img/svg/divider-small.svg");
}
blockquote cite {
  display: block;
  font: normal 400 1.2rem/1.5 "Montserrat", sans-serif;
  margin: 1.8rem 0 0 0;
  color: #999999;
}
blockquote cite span {
  display: block;
}
@media only screen and (min-width: 768px) {
  blockquote i {
    font-size: 7rem;
  }
  blockquote p {
    font-size: 45px;
  }
  blockquote cite {
    font-size: 1.4rem;
  }
}
.inner {
  position: relative;
  background-color: #ffffff;
}
.inner__content {
  position: relative;
  overflow: hidden;
}
.content-block {
  position: relative;
  width: 100%;
  height: auto;
  padding: 10rem 10%;
}
@media only screen and (min-width: 1200px) {
  .content-block {
    position: absolute;
    left: 0;
    top: 50%;
    bottom: auto;
    -webkit-transform: translateY(-50%);
    -moz-transform: translateY(-50%);
    -ms-transform: translateY(-50%);
    transform: translateY(-50%);
    padding: 0 10rem;
  }
  .content-block.auto-height {
    position: relative;
    top: auto;
    left: auto;
    -webkit-transform: translateX(0);
    -moz-transform: translateX(0);
    -ms-transform: translateX(0);
    transform: translateX(0);
    padding: 10rem;
  }
}
@media only screen and (min-width: 1400px) {
  .content-block {
    padding: 0 14rem;
  }
  .content-block.auto-height {
    padding: 12rem 14rem;
  }
  .content-block.centered {
    padding: 12rem 5rem;
  }
}
.quote-object {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  padding: 3rem 10%;
}
@media only screen and (min-width: 1200px) {
  .quote-object {
    padding: 3rem;
  }
}
@media only screen and (min-width: 1400px) {
  .quote-object {
    padding: 4rem;
  }
}
.quote-morphing-object {
  fill: #f7f7f7;
}
</style>
<div class="section fp-auto-height inner fp-section active fp-completely" data-anchor="blockquote" style="height: 545px;">
	<div class="container-fluid p-0">
	<div class="row g-0 justify-content-center">
		<div class="col-12 col-lg-7 inner__content">
		<div class="quote-object" id="quote-morphing">
			<svg class="quote-morphing-object" width="100%" height="100%" viewBox="0 0 600 600" xml:space="preserve">
			<path class="quote-morphing-path" d="M585.1,322.9798780999505c0,148.3 -72.30262154532818,260.4657928182239 -220.69388289987376,260.4657928182239S28.566862145478364,524.9595378183038,28.566862145478364,376.6595378183038S227.9898701000783,13.6,376.2898701000783,13.6S585.1,174.67987809995046,585.1,322.9798780999505z"></path>
			</svg>
		</div>
		<div class="content-block auto-height centered">
			<div class="blockquote-content">
			<blockquote cite="">
				<i class="ecs-quote-left-1"></i>
				<p><?php echo $video_description; ?></p>
				<span class="divider"></span>
			</blockquote>
			</div>
		</div>
		</div>
		<div class="col-12 col-lg-5 inner__content">
			<iframe class="col-12 col-lg-10" height="370" style="margin-top: 50px" src="https://www.youtube.com/embed/<?php echo $video_featured; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		</div>

	</div>
	</div>
</div>