<?php if ($position == 'content_top'  or $position == 'content_bottom') { ?>
<section id="blog" class="bg-gray">
  <h2 class="section-header text-center h0 title-section text-second p-bot-4 container"><?php echo $cat_name; ?></h2>
  <div class="section-body container">
    <div class="masonry-css">
      <?php foreach($newss as $key => $news){ ?>
        <div class="item" itemprop="liveBlogUpdate" itemscope itemtype="http://schema.org/BlogPosting"> <i class="demo-icon ecs-flag"></i>
            <a href="tin-tuc/goi-y-trai-nghiem-tai-dong-nam-a" class="cat text-uppercase"><?php echo $news['category_info']['name'];?></a>
            <h3 class="title-section h3"><a href="<?php echo $news['href']; ?>" itemprop="headline"><?php echo $news['name'];?></a></h3>
            <div class="des h5 <?php echo $key % 3 == 0 || $key % 3 == 2 ? '' : 'd-none'; ?>" itemprop="description"><?php echo $news['short_description'];?></div>
            <a href="<?php echo $news['href']; ?>" class="img-container" itemprop="url">
                <img data-src="<?php echo $this->model_tool_image->cropsize($news['image'], 450, 450); ?>" src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" class="lazy" alt="<?php echo $news['name'];?>" itemprop="thumbnail">
            </a>
            <div class="aurthor flex-center p-2">
              <img src="/static/img/award_prize_icon.png">
              <h4 class="h4 text-primary title-section" itemprop="author"><?php echo $news['author'];?><span class="h text-bold text-gray"><?php echo $news['designer'];?></span></h4>
              
            </div>
            <div class="des h5 <?php echo $key % 3 == 0 || $key % 3 == 2 ? 'd-none' : ''; ?>" itemprop="description"><?php echo $news['short_description'];?></div>
        </div>
      <?php } ?>
    </div>
    
    <div class="section-footer text-center m-top-8">
      <a href="<?php echo $cathref; ?>" class="" style="font-size: 26px;">
        <?php echo $this->language->get('text_more_information'); ?> &nbsp;
        <div class="chevron-arrow-right"></div>
        <div class="chevron-arrow-right"></div>
        <div class="chevron-arrow-right"></div>
      </a>
    </div>
  </div>
</section>
<?php } else if ($position == 'column_left' or $position == 'column_right') { ?>
  <h2 class="h4 title-section title-decoration"><a href="<?php echo $cathref; ?>"><?php echo $cat_name; ?></a></h2>
  <div class="rowgutter">
  <?php foreach($newss as $key => $news){ ?>
      <div class="faces_item">
          <div class="faces_body">
              <div class="faces_media">
                  <div class="entry-thumbnail">
                      <img width="1638" height="2048" src="<?php echo $this->model_tool_image->cropsize($news['image'], 1638, 2048); ?>" class="attachment-full size-full wp-post-image"> 
                  </div>
                  <div class="faces_info">
                      <p class="faces_name"><?php echo $news['author'];?></p>
                      <p class="faces_school"><?php echo $news['designer'];?></p>
                  </div>
              </div>
              <a href="<?php echo $news['href']; ?>" class="faces_content">
                  <p class="faces_nameHover"><?php echo $news['author'];?></p>
                  <p class="faces_classHover"></p>
                  <p class="faces_schoolHover"><?php echo $news['designer'];?></p>
                  <p class="faces_giathuong">
                    <?php echo $news['short_description'];?>
                  </p>
              </a>
          </div>
      </div>
  <?php } ?>
  </div>
<?php } else if ($position == 'before_footer') {?>
<div class="box-news-cat">
  <h3 class="box-heading"><?php echo $cat_name; ?></h3>
  <div class="box-content">
    <div class="box-product-left">
      <?php foreach ($newss as $news) { ?>
      <div>
        <?php if ($news['thumb']) { ?>
        <div class="image"><a href="<?php echo $news['href']; ?>"><img src="<?php echo $news['thumb']; ?>" alt="<?php echo $news['name']; ?>" <?php echo $news['width']; ?> <?php echo $news['height']; ?> /></a></div>
        <?php } ?>
        <div class="name"><a href="<?php echo $news['href']; ?>"><?php echo $news['name']; ?></a></div>

         <?php echo $news['short_description'];?>
         <div class="clrfix"></div>
      </div>
      <?php } ?>
    </div>
    <a class="orther" href="<?php echo $cathref; ?>">Xem toàn bộ</a>
  </div>
</div>
<?php } ?>
