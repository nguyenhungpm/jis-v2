<div class="whyus relative" style="background-image:url(<?php echo $whyus_banner; ?>);">
	<div class="container">
		<h2 class="h1 title_under_center yellow h1 text-center"><?php echo $heading_title; ?></h2>
		<div class="row clear white">
			<div class="col-xs-12 col-sm-3">
				<img src="<?php echo $whyus_image1; ?>" class="left" alt="<?php echo $whyus_number1; ?>"/>
				<h3 class="whyus_number"><?php echo $whyus_number1; ?></h3>
				<p class="whyus_content"><?php echo $whyus_content1; ?></p>
			</div>
			<div class="col-xs-12 col-sm-3">
				<img src="<?php echo $whyus_image2; ?>" class="left" alt="<?php echo $whyus_number2; ?>"/>
				<h3 class="whyus_number"><?php echo $whyus_number2; ?></h3>
				<p class="whyus_content"><?php echo $whyus_content2; ?></p>
			</div>
			<div class="col-xs-12 col-sm-3">
				<img src="<?php echo $whyus_image3; ?>" class="left" alt="<?php echo $whyus_number3; ?>"/>
				<h3 class="whyus_number"><?php echo $whyus_number3; ?></h3>
				<p class="whyus_content"><?php echo $whyus_content3; ?></p>
			</div>
			<div class="col-xs-12 col-sm-3">
				<img src="<?php echo $whyus_image4; ?>" class="left" alt="<?php echo $whyus_number4; ?>"/>
				<h3 class="whyus_number"><?php echo $whyus_number4; ?></h3>
				<p class="whyus_content"><?php echo $whyus_content4; ?></p>
			</div>
		</div>
	</div>
</div>
