<div class="news-form">
    <h4 class="head-title-blog">đăng ký nhận tin</h4>
    <p>Vui lòng để lại email của bạn. Chúng tôi sẽ cập nhật những tin tức quan trọng nhất của HP102 tới bạn.</p>
    <form class="form_newsletter" action="#" method="post">
        <input type="email" value="" placeholder="Nhập email của bạn" name="EMAIL" id="mail" class="newsletter-input form-control">
        <button id="subscribe" class="button_mini btn btn-gradient" type="submit">
            Đăng ký
        </button>
    </form>
</div>


<div class="td_block_wrap">
  <div class="td-block-title-wrap">
    <h4 class="block-title td-block-title"><span class="td-pulldown-size">ĐĂNG KÝ NHẬN TIN</span></h4>
  </div>
  <div class="td-social-list">
    <p>Vui lòng để lại email của bạn. Chúng tôi sẽ cập nhật những tin tức quan trọng nhất của HP102 tới bạn.</p>
    <div id="frm_subscribe" class="relative">
		<input type="text" value="" name="subscribe_email" placeholder="<?php echo $text_email; ?>" id="subscribe_email" class="full-width black round" /><br /><br />
		<input style="float: right" type="submit" value="Đăng ký" class="wpcf7-form-control wpcf7-submit">
	</div>
	<div class="text-left" id="subscribe_result"></div>
  </div>
</div>
<style type="text/css">
	.bullet-lists a {
	    background: none;
	    color: #999;
	    border: none;
	}
	ul.list-inline.bullet-lists li a {
	    text-transform: capitalize;
	    color: #222;
	}
	ul li {
	    list-style: none;
	}
	.list-inline > li {
	    display: inline-block;
	}
	.list-inline {
	    padding-left: 0;
	    margin-left: -5px;
	    list-style: none;
	}
	ul.list-inline.bullet-lists li {
	    background: #F8F8F8;
	    padding: 5px 10px;
	    margin-bottom: 5px;
	    margin-right: 2px;
	    border: 1px solid #F4F4F4;
	    border-radius: 3px;
	    transition: all 0.3s;
	    margin-left: 0;
	}
	ul.list-inline.bullet-lists li:hover {
	    background: #16a0ca;
	    color: #fff;
	}
	ul.list-inline.bullet-lists li a {
	    text-transform: capitalize;
	    color: #222;
	}
	ul.list-inline.bullet-lists li:hover a {
	    color: #fff;
	}
	ul.bullet-lists {
	    margin-top: 20px;
	}
</style>
<div class="td_block_wrap">
  <div class="td-block-title-wrap">
    <h4 class="block-title td-block-title"><span class="td-pulldown-size">TAGS</span></h4>
  </div>
  <div class="td-tag-list">
    <ul class="list-inline bullet-lists">
      <?php foreach($tags as $tag){ ?>
	    <li><a href="#"><?php echo $tag['tag']?></a></li>
      <?php } ?>
    </ul>
  </div>
</div>
