<?php if (count($languages) > 1) { ?>
<form action="<?php echo $action; ?>" method="post" id="formlang" enctype="multipart/form-data" style="display:inline-block;">
  <div id="language" class="inline-block">
    <?php 
    foreach ($languages as $language) { ?>
      <a onclick="$('input[name=\'language_code\']').attr('value', '<?php echo $language['code']; ?>'); $(this).parent().parent().submit();" class="<?php echo $this->language->get('code') == $language['code'] ? 'active':'inline-block white'; ?>">
        <img src="media/flags/<?php echo $language['image']; ?>" alt="<?php echo $language['name']; ?>" class="flags"/>
        <?php // echo $language['code']; ?>
  		</a>
    <?php } ?>
    <input type="hidden" name="language_code" id="idlang"  value="" />
    <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
  </div>
</form>
<?php } ?>
