<!-- video -->
<section id="video" class="infor bg-black">
		<div class="container-fluid">
				<h2 class="section-header text-center h0 title-section text-second p-bot-4"><?php echo $this->language->get('text_educational_program'); ?></h2>
				<div id='stsv-02' class="d-none">  <h3 class="tde"><span><?php echo $this->language->get('text_educational_program'); ?></span></h3></div>


				<ul class="nav nav-tabs clearfix">
					<?php foreach($informations as $key => $info){ ?>
					<li class="<?php echo $key == 0 ? 'active' : ''; ?>"><a data-toggle="tab" href="#info<?php echo $key; ?>"><?php echo $info['title']; ?></a></li>
					<?php } ?>
				</ul>

				<div class="tab-content">
					<?php foreach($informations as $key => $info){ ?>
					<div id="info<?php echo $key; ?>" class="tab-pane fade in <?php echo $key == 0 ? 'active' : ''; ?>">
						<div class="section-body flex-container">
							<div class="col">
									<iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $info['code_youtube']; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							</div>
							<div class="col">
									<div class="text-container text-white">
											<h3 class="title-section h2 m-bot-4">#<?php echo $info['robots']; ?></h3>
											
											<div class="des h5">
												<div class="edu-point">
													<?php foreach($info['highlight'] as $key => $highlight) { ?>
													<div class="edu-point__item <?php echo $key%2 != 0 ? 'edu-point__item--odd' : ''; ?> edu-point__0<?php echo $key+1; ?>">
														<div class="edu-point__icon">
														<img
															width="56"
															alt=""
															data-src="media/<?php echo $highlight['image']; ?>"
															class="icon-imgtag ls-is-cached lazyloaded"
															src="media/<?php echo $highlight['image']; ?>"
														/><span class="edu-point__line"></span>
														</div>
														<div class="edu-point__text">
														<p><?php echo $highlight['highlight_description']; ?></p>
														</div>
														<span class="<?php echo $key == count($info['highlight']) -1 ? '' : 'edu-point__bridge'; ?>"></span>
													</div>
													<?php } ?>
												</div>

												<a class="btn-primary btn-outline btn btn-big" href="<?php echo $info['href'];?>"><?php echo $this->language->get('text_more'); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
												<a href="information/contact" class="btn-primary btn-outline btn btn-big"><?php echo $this->language->get('text_sign_school'); ?></a>

											</div>
									</div>
							</div>
						</div>
					</div>
					<?php } ?>
				</div>

		</div>
</section>
<!-- /#video -->