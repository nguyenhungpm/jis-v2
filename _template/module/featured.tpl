<?php if($position=='column_right'){ ?>
<div class="box">
	<h3 class="title_red n--m uppercase"><?php echo $heading_title; ?></h3>
		<?php foreach ($products as $product) { ?>
				<div class="rowgutter clearfix">
				<div class="text-center gutter border">
			      	<?php if ($product['thumb']) { ?>
			      	<a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" /></a>
			      	<?php } ?>
			      	<h3 class="name h4 n-mb"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h3>
							<span class="color-red strong block"><?php echo $product['price']; ?></span>
							<br />
		    </div>
				</div>
	    <?php } ?>
</div>
<?php }else{ ?>

<!-- productearly -->
<section id="productearly" class="bg-gray">
		<div class="container">
				<div class="section-header text-center w-70 p-bot-4">
						<h2 class="h0 title-section text-second p-bot-4"><?php echo $featured_product_subtitle; ?></h2>
						<div class="des h5">
								<p><?php echo $featured_product_desc; ?></p>
						</div>
				</div>
				<div class="section-body flex-container col-3item">
					<?php foreach ($products as $product) { ?>
						<div class="item-product" itemscope itemtype="http://schema.org/Product">
								<a href="<?php echo $product['href']; ?>" class="img-container">
										<img data-src="<?php echo $product['thumb']; ?>" src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" class="lazy" alt="<?php echo $product['name']; ?>" itemprop="image">
								</a>
								<div class="text-container">
										<a href="javascript:" class="label">
												<img src="wwwroot/resources/upload/img/label/11-2019/tagiconic-min.jpg" alt="The Iconic">
										</a>
										<h3 class="h3 title-section" itemprop="name"><a href="<?php echo $product['href']; ?>" itemprop="url"><?php echo $product['name']; ?></a></h3>
										<div class="date h5">08/04/2020 - 13/04/2020</div>
										<div class="hidden price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
												<div class="new-price h3" itemprop="price" content="33690000.00">33.690.000 VNĐ</div>
												<div class="old-price h4">35.890.000 VNĐ</div>
										</div>
										<div class="rate title-section">
												<div class="rate-item">
														<label for="">Local Experience</label>
														<div class="rage"> <span class="active"></span>
																<span class="active"></span>
																<span class="active"></span>
																<span></span>
																<span></span>
														</div>
												</div>
												<div class="rate-item">
														<label for="">Me Time</label>
														<div class="rage"> <span class="active"></span>
																<span class="active"></span>
																<span class="active"></span>
																<span></span>
																<span></span>
														</div>
												</div>
												<div class="rate-item">
														<label for="">First Time</label>
														<div class="rage"> <span class="active"></span>
																<span class="active"></span>
																<span class="active"></span>
																<span></span>
																<span></span>
														</div>
												</div>
										</div>
								</div>
						</div>
					<?php } ?>
				</div>
				<div class="section-footer text-center m-top-8"> <a href="promotion-p10" class="btn-black btn-outline btn btn-big">Xem th&#234;m</a>
				</div>
		</div>
</section>
<!-- /#productearly -->
<?php } ?>
