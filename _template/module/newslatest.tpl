<?php if ($position == 'adv_top'  or $position == 'content_bottom') { ?>
<div class="rowgutter bg-white">
<div class="container">
	<h2 class="h1 uppercase text-center darkred title_under_center"><?php echo $heading_title; ?></h2>
	<div class="clear row">
		<?php foreach ($newss as $news) { ?>
			<div class="col-6 col-sm-2">
				<a href="<?php echo $news['href']; ?>"><img class="block circle" alt="<?php echo $news['name']; ?>" src="<?php echo $news['thumb']; ?>" /></a>
				<div class="news_content_box">
					<h3 class="n-mb h4 strong text-center"><a class="red" href="<?php echo $news['href']; ?>"><?php echo $news['name']; ?></a></h3>
					<p class="grey hide em n--m"><?php echo $news['date_added'];?></p>
					<p class="desc h5 text-center"><?php echo $news['short_description'];?></p>
				</div>
			</div>
		<?php } ?>
	</div>
</div>
</div>
<?php } elseif ($position == 'column_left') { ?>
<div class="col-xs-12 col-sm-4">
<div class="newslatest">
	<h3 class="h4 n-mt uppercase"><a href="news/blog"><?php echo $heading_title; ?></a></h3>
	<ul>
	<?php foreach ($newss as $news) { ?>
		<li>
				<h3 class="n--m h5 lh11"><a class="darkblue" href="<?php echo $news['href'];?>"><?php echo $news['name'];?></a></h3>
				<span class="grey h6"><?php echo $news['date_added']; ?></span>

		</li>
	<?php } ?>
	</ul>
</div>
</div>
<?php }else{ ?>
<div id="td_uid_1_5d5e6881560b7" class="tdc-row">
  <div class="vc_row td_uid_16_5d5e6881560f1_rand  wpb_row td-pb-row">
    <style scoped>
      /* custom css */
      .td_uid_16_5d5e6881560f1_rand {
        min-height: 0;
      }
    </style>
    <div class="vc_column td_uid_17_5d5e6881562db_rand  wpb_column vc_column_container tdc-column td-pb-span12">
      <style scoped>
        /* custom css */
        .td_uid_17_5d5e6881562db_rand {
          vertical-align: baseline;
        }
      </style>
      <div class="wpb_wrapper">
        <div class="td_block_wrap td_block_big_grid_3 td_uid_19_5d5e688156c3c_rand td-grid-style-1 td-hover-1 td-big-grids td-pb-border-top td_block_template_1" data-td-block-uid="td_uid_19_5d5e688156c3c">
          <div id=td_uid_19_5d5e688156c3c class="td_block_inner">
            <div class="td-big-grid-wrapper">
              <div class="td_module_mx5 td-animation-stack td-big-grid-post-0 td-big-grid-post td-big-thumb">
                <div class="td-module-thumb">
                  <a href="<?php echo $newss[0]['href']?>" rel="bookmark" class="td-image-wrap" title="<?php echo $newss[0]['name']?>">
                    <img width="534" height="462" class="entry-thumb" src="<?php echo $this->model_tool_image->cropsize($newss[0]['image'], 534, 462); ?>" alt="" title="<?php echo $newss[0]['name']?>" />
                  </a>
                </div>
                <div class="td-meta-info-container">
                  <div class="td-meta-align">
                    <div class="td-big-grid-meta">
                      <a href="<?php echo $this->url->link('news/news_category', 'cat_id=' . $newss[0]['category_info']['news_category_id']); ?>" class="td-post-category"><?php echo $newss[0]['category_info']['name']; ?></a>
                      <h3 class="entry-title td-module-title"><a href="<?php echo $newss[0]['href']?>" rel="bookmark" title="<?php echo $newss[0]['name']?>"><?php echo $newss[0]['name']?></a></h3>
                    </div>
                    <div class="td-module-meta-info"><span class="td-post-date"><time class="entry-date updated td-module-date"><?php echo $newss[0]['date_added']?></time></span></div>
                  </div>
                </div>
              </div>
              <div class="td-big-grid-scroll">
                <div class="td_module_mx11 td-animation-stack td-big-grid-post-1 td-big-grid-post td-medium-thumb">
                  <div class="td-module-thumb">
                    <a href="<?php echo $newss[1]['href']?>" rel="bookmark" class="td-image-wrap" title="<?php echo $newss[1]['name']?>">
                      <img width="533" height="261" class="entry-thumb" src="<?php echo $this->model_tool_image->cropsize($newss[1]['image'], 533, 261); ?>" alt="" title="<?php echo $newss[1]['name']?>" />
                    </a>
                  </div>
                  <div class="td-meta-info-container">
                    <div class="td-meta-align">
                      <div class="td-big-grid-meta">
                        <a href="<?php echo $this->url->link('news/news_category', 'cat_id=' . $newss[1]['category_info']['news_category_id']); ?>" class="td-post-category"><?php echo $newss[0]['category_info']['name']; ?></a>
                        <h3 class="entry-title td-module-title"><a href="<?php echo $newss[1]['href']?>" rel="bookmark" title="<?php echo $newss[1]['name']?>"><?php echo $newss[1]['name']?></a></h3>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="td_module_mx6 td-animation-stack td-big-grid-post-2 td-big-grid-post td-small-thumb">
                  <div class="td-module-thumb">
                    <a href="<?php echo $newss[2]['href']?>" rel="bookmark" class="td-image-wrap" title="<?php echo $newss[2]['name']?>">
                      <img width="265" height="198" class="entry-thumb" src="<?php echo $this->model_tool_image->cropsize($newss[2]['image'], 265, 198); ?>" alt="" title="<?php echo $newss[2]['name']?>" />
                    </a>
                  </div>
                  <div class="td-meta-info-container">
                    <div class="td-meta-align">
                      <div class="td-big-grid-meta">
                        <a href="<?php echo $this->url->link('news/news_category', 'cat_id=' . $newss[2]['category_info']['news_category_id']); ?>" class="td-post-category"><?php echo $newss[2]['category_info']['name']; ?></a>
                        <h3 class="entry-title td-module-title"><a href="<?php echo $newss[2]['href']?>" rel="bookmark" title="<?php echo $newss[2]['name']?>"><?php echo $newss[2]['name']?></a></h3>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="td_module_mx6 td-animation-stack td-big-grid-post-3 td-big-grid-post td-small-thumb">
                  <div class="td-module-thumb">
                    <a href="<?php echo $newss[3]['href']?>" rel="bookmark" class="td-image-wrap" title="<?php echo $newss[3]['name']?>">
                      <img width="265" height="198" class="entry-thumb" src="<?php echo $this->model_tool_image->cropsize($newss[3]['image'], 265, 198); ?>" alt="" title="<?php echo $newss[3]['name']?>" />
                    </a>
                  </div>
                  <div class="td-meta-info-container">
                    <div class="td-meta-align">
                      <div class="td-big-grid-meta">
                        <a href="<?php echo $this->url->link('news/news_category', 'cat_id=' . $newss[3]['category_info']['news_category_id']); ?>" class="td-post-category"><?php echo $newss[3]['category_info']['name']; ?></a>
                        <h3 class="entry-title td-module-title"><a href="<?php echo $newss[3]['href']?>" rel="bookmark" title="<?php echo $newss[3]['name']?>"><?php echo $newss[3]['name']?></a></h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php } ?>
