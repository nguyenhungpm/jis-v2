<h3 class="h4 no-margin rowgutter color-black"><?php echo $heading_title; ?></h3>
<div id="order">
	<form name="order_form_hosting" id="order_form_hosting" onsubmit="submitOrderHosting('order_form_hosting', event)">
		<div id="frm_order" class="relative">
			<input type="hidden" value="" name="order_domain"/>
			<input type="text" value="" name="order_name" placeholder="<?php echo $text_name; ?>"><br />
			<div class="required" id="order_form_hostingerror1"><?php echo $error_name; ?></div>
			<input type="text" value="" name="order_email" placeholder="<?php echo $text_email; ?>"><br />
			<div class="required" id="order_form_hostingerror5"><?php echo $error_email; ?></div>
			<textarea name="order_content" placeholder="<?php echo $text_content; ?>"></textarea><br />
			<div class="clear"><input placeholder="<?php echo $text_captcha; ?>" type="text" name="captcha" id="captcha" class="left" /> <img src="index.php?route=information/contact/captcha" class="right" alt="captcha" /></div>
			<div class="required" id="order_form_hostingerror7"><?php echo $error_captcha; ?></div>				
			<input type="hidden" name="captcha_status" value="0" />
			<div class="clearfix"><a onclick="submitOrderHosting('order_form_hosting', event)" class="submit-button strong uppercase block text-center bg-green color-white"><?php echo $text_submit; ?></a></div>
		</div>
	</form>
	<div class="text-center" id="order_result"></div>
</div>
<script>
[].forEach.bind(document.getElementsByClassName("required"), function (itm) {
	itm.style.display = "none";
})();
function submitOrderHosting(id, event){
	event.preventDefault();
	var request = new XMLHttpRequest();
	// POST to httpbin which returns the POST data as JSON
	request.open('POST', 'index.php?route=module/contact/add', /* async = */ false);
	var formData = new FormData(document.getElementById(id));
	request.send(formData);
	
	var rs = request.responseText.replace('"', '');
	var rs = rs.split(",");
	var k=0;
	rs.splice(-1,1);
	if (request.status == 200) {
	
		[].forEach.bind(document.getElementsByClassName("required"), function (itm) {
			itm.style.display = "none";
		})();

		for(i=1; i<9; i++){
			var n = i.toString();
			if (rs.indexOf(n) >= 0) {
				if(document.getElementById(id+'error'+i)){
				document.getElementById(id+'error'+i).style.display = "block";
				}
				k +=1;
			}
		}
		if(k==0){
			document.getElementById('order').innerHTML = '<div class="success color-white"><?php echo $text_success; ?></div>';
		}
		
	} else {
		console.log('Error: ' + request.statusText);
	}
}
</script>