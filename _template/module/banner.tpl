<!-- location -->
<section id="index-1" class="bg-default">
		<div class="container">
				<div class="section-header text-center p-bot-4 w-70">
						<h2 class="h0 title-section text-second p-2"><?php echo $banner_title?></h2>
				</div>
				<div class="section-body flex-center flex-between">
					<?php foreach ($banners as $key=> $banner) { ?>
						<a href="<?php echo $banner['link'];?>">
							<span> </span>
							<img src="<?php echo $banner['image'];?>">
						</a>
					<?php } ?>
				</div>
		</div>
</section>
<!-- /#location -->
