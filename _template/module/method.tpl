<div id="method" class="relative text-center rowgutter" style="background: url(media/<?php echo $med_image; ?>) center center no-repeat">
	<div class="med_content white">
	<div class="container gutter">
		<div class="med_box">
			<h2 class="h1 uppercase n-mb med_title"><?php echo $med_heading_title; ?></h2>
			<p class="med_message center-e"><?php echo $med_content; ?></p>
			<div class="rowgutter">
				<a href="common/showroom" class="bg-red med_button_red">Hệ thống nhà thuốc</a>
				<a href="contact.html" class="white med_button_grey">Đặt hàng ngay</a>
			</div>
		</div>
	</div>
	</div>
	<div class="bg-red rowgutter med_footer absolute">
		<h2 class="uppercase med_sub_title n--m white"><?php echo $med_sub_title; ?></h2>
	</div>
</div>