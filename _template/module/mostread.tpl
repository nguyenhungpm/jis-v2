<!-- blog -->
<section id="blog" class="bg-gray">
		<h2 class="section-header text-center h0 title-section text-second p-bot-4 container">Gương mặt JISer</h2>
		<div class="section-body container">
			<div class="masonry-css">
				<?php foreach($newss as $key => $news){ ?>
					<div class="item" itemprop="liveBlogUpdate" itemscope itemtype="http://schema.org/BlogPosting"> <i class="demo-icon ecs-flag"></i>
							<a href="tin-tuc/goi-y-trai-nghiem-tai-dong-nam-a" class="cat text-uppercase"><?php echo $news['category_info']['name'];?></a>
							<h3 class="title-section h3"><a href="<?php echo $news['href']; ?>" itemprop="headline"><?php echo $news['name'];?></a></h3>
							<div class="des h5 <?php echo $key % 3 == 0 || $key % 3 == 2 ? '' : 'd-none'; ?>" itemprop="description"><?php echo $news['short_description'];?></div>
							<a href="<?php echo $news['href']; ?>" class="img-container" itemprop="url">
									<img data-src="<?php echo $this->model_tool_image->cropsize($news['image'], 450, 450); ?>" src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" class="lazy" alt="<?php echo $news['name'];?>" itemprop="thumbnail">
							</a>
							<div class="aurthor flex-center p-2">
								<img src="<?php echo $news['thumb'];?>">
								<h4 class="h4 text-primary title-section" itemprop="author">H&#226;n Vũ<span class="h text-bold text-gray">#HighByTheBeach-ElNido&amp;Coron</span></h4>
							</div>
							<div class="des h5 <?php echo $key % 3 == 0 || $key % 3 == 2 ? 'd-none' : ''; ?>" itemprop="description"><?php echo $news['short_description'];?></div>
					</div>
				<?php } ?>
			</div>
			
			<div class="section-footer text-center m-top-8">
				<a href="promotion-p10" class="" style="font-size: 26px;">
					<?php echo $this->language->get('text_more_information'); ?> &nbsp;
					<div class="chevron-arrow-right"></div>
					<div class="chevron-arrow-right"></div>
					<div class="chevron-arrow-right"></div>
				</a>
			</div>
		</div>
		
</section>
<!-- /#blog -->