
<section class="categories">
    <div class="section-header ">
        <h2 class="wow fadeInUp top__element" data-wow-delay="0.3s" data-wow-duration="1.5s">các danh mục</h2>
        <h5><em>We design and build functional and beautiful websites</em></h5>
        <div class="fancy"><img src="img/spr.png" alt="" width="auto"></div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4">
                <nav class="nav-sidebar">
                    <ul class="nav tabs">
                        <?php foreach ($categories as $key => $category) { ?>
                        <li class="<?php echo $key==0 ? 'active': ''; ?>"><a href="#tab<?php echo $category['category_id']; ?>" data-toggle="tab"><span class="<?php echo $category['icon']; ?>"></span><?php echo $category['name']; ?></a></li>
                        <?php } ?>                      
                    </ul>
                </nav>

            </div>
            <!-- tab content -->
            <div class="col-md-8 col-sm-8">
                <div class="tab-content tabs-category">
                    <?php foreach ($categories as $key => $category) { ?>
                    <div class="tab-pane <?php echo $key==0 ? 'active': ''; ?> text-style" id="tab<?php echo $category['category_id']; ?>">
                        <div class="row">
                            <div class="col-md-4">
                                <h4 class="title-v1"><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></h4>
                                <ul  class="bottom-it">
                                    <?php foreach ($category['children'] as $key => $children) { ?>
                                    <li><a href="<?php echo $children['href']; ?>"><?php echo $children['name']; ?></a></li>
                                    <?php } ?>
                                </ul>
                            </div>

                            <div class="col-md-8 left-bd">
                                <?php foreach ($category['products'] as $key => $product) { ?>
                                <div class=" flex-items">  
                                    <div class="img-content">
                                        <a href="category.html"><img src="<?php echo $product['thumb']; ?>"></a>
                                    </div>
                                    <div class="title-tabs">
                                        <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                                        <a href="<?php echo $product['href']; ?>" class="view-more">Bấm xem</a>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>   
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>