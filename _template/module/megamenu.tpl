<div id="contain-sidebar">
<div id="fake"></div>
<div class="clear" id="sidebar">
	<div class="main-menu relative text-center">
		<?php if($this->config->get('config_language_id') == '2'){ ?>
			<!--div class="zonegutter n-pb"><a class="color-white inline-block " href="form/register">Đăng kí tham gia</a></div-->
		<?php } ?>
		<?php foreach($megamenu as $mg){ ?>
			<div class="rowgutter sidebar left">
				<a class="inline-block uppercase strong" href="<?php echo $mg['href'];?>">
					<img src="<?php echo $mg['icon'];?>" alt="<?php echo $mg['name'];?>"/><br />
					<span><?php echo $mg['name'];?></span>
				</a>
				<?php if(!empty($mg['child'])){?>
				<div class="absolute hide bg-white detail-sub">
					<ul class="n--m n--p">
					<?php foreach($mg['child'] as $child1){?>
					<li <?php echo !empty($child1['child']) ? 'class="sub relative clear" onclick="toggle(this, \'open\')"' : 'class="clear"'; ?>>
						<a href="<?php echo $child1['href'];?>"><?php echo $child1['name'];?></a>
						<?php if(!empty($child1['child'])){?>
						<ul class="n--p n--m hide clear">
							<?php foreach($child1['child'] as $child2){?>
							<li class="block clear">
								<a href="<?php echo $child2['href']; ?>">
									<img class="" src="<?php echo $child2['icon']; ?>" />
									<span class=""><?php echo $child2['name']; ?></span>
								</a>
							</li>
							<?php } ?>
						</ul>
						<?php } ?>
					</li>
					<?php } ?>
					</ul>
				</div>
				<?php } ?>
			</div>
		<?php } ?>
	</div>
</div>
</div>
