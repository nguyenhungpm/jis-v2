<div id="solution" class="bg-white ">
<div class="container">
<div class="row clear zonegutter">
	<h2 class="h1 uppercase text-center red m-mt"><?php echo $sol_heading_title; ?></h2>
	<div class="col-xs-12 col-sm-4 rowgutter">
		<div class="boxes bg-white">
			<div class="rowgutter text-center">
				<span class="circle inline-block big-icon"><img src="media/<?php echo $sol_image; ?>" alt ="" /></span>
			</div>
			<p class="clearfix"><?php echo $sol_content; ?></p>
		</div>
	</div>
	<div class="col-xs-12 col-sm-4 rowgutter">
		<div class="boxes bg-white">
			<div class="rowgutter text-center">
				<span class="circle inline-block big-icon"><img src="media/<?php echo $sol_image1; ?>" alt ="" /></span>
			</div>
			<p class="clearfix"><?php echo $sol_content1; ?></p>
		</div>
	</div>
	<div class="col-xs-12 col-sm-4 rowgutter">
		<div class="boxes bg-white">
			<div class="rowgutter text-center">
				<span class="circle inline-block big-icon"><img src="media/<?php echo $sol_image2 ?>" alt ="" /></span>
			</div>
			<p class="clearfix"><?php echo $sol_content2; ?></p>
		</div>
	</div>
</div>
</div>
</div>