<!-- testimonial -->
<section id="testimonial" class="p-bot-0 <?php echo $route=='common/home' ? 'bg-white':''; ?>">
		<h2 class="section-header text-center h0 title-section text-second p-bot-4 container">Phụ huynh n&#243;i về JIS </h2>
		<div class="section-body container-fluid flex-container col-5item">
				<a class="item" href="javascript:" data-id="69" itemprop="liveBlogUpdate" itemscope itemtype="http://schema.org/BlogPosting">
						<img data-src="wwwroot/resources/upload/img/news/11-2019/testimonio-26.jpg" src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" class="lazy" alt="Testimonio 5" itemprop="thumbnail">
						<div class="item-inner flex-center">
								<img src="wwwroot/resources/upload/img/banner/11-2019/2-testimonio-13.png">
								<div class="text">
										<h3 class="h4 text-bold text-primary" itemprop="author">Vũ Quỳnh Ch&#226;m</h3>
										<h4 class="h text-bold text-white">Uncode the Shangri-La 10/2019</h4>
								</div>
						</div>
				</a>
				<a class="item" href="javascript:" data-id="68" itemprop="liveBlogUpdate" itemscope itemtype="http://schema.org/BlogPosting">
						<img data-src="wwwroot/resources/upload/img/news/11-2019/img6517.jpeg" src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" class="lazy" alt="Testimonio 4" itemprop="thumbnail">
						<div class="item-inner flex-center">
								<img src="wwwroot/resources/upload/img/banner/11-2019/3-2-7523751113086251459775871300045003740938240n-copy.png">
								<div class="text">
										<h3 class="h4 text-bold text-primary" itemprop="author">Vũ Ngọc</h3>
										<h4 class="h text-bold text-white">Road trip to Ladakh 09/2019</h4>
								</div>
						</div>
				</a>
				<a class="item" href="javascript:" data-id="13" itemprop="liveBlogUpdate" itemscope itemtype="http://schema.org/BlogPosting">
						<img data-src="wwwroot/resources/upload/img/news/11-2019/testimonio-07.jpg" src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" class="lazy" alt="Testimonio 2" itemprop="thumbnail">
						<div class="item-inner flex-center">
								<img src="wwwroot/resources/upload/img/banner/11-2019/testimonio-12.png">
								<div class="text">
										<h3 class="h4 text-bold text-primary" itemprop="author">Nguyễn Thị Thanh Hiền</h3>
										<h4 class="h text-bold text-white">Tropical Vibes - El Nido 05/2019</h4>
								</div>
						</div>
				</a>
				<a class="item" href="javascript:" data-id="12" itemprop="liveBlogUpdate" itemscope itemtype="http://schema.org/BlogPosting">
						<img data-src="wwwroot/resources/upload/img/news/11-2019/testimonio-03.jpg" src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" class="lazy" alt="Testimonio 1" itemprop="thumbnail">
						<div class="item-inner flex-center">
								<img src="wwwroot/resources/upload/img/banner/11-2019/testimonio-01.png">
								<div class="text">
										<h3 class="h4 text-bold text-primary" itemprop="author">L&#234; Ngọc &#193;nh</h3>
										<h4 class="h text-bold text-white">Train to Fenghuang 2018</h4>
								</div>
						</div>
				</a>
				<a class="item" href="javascript:" data-id="12" itemprop="liveBlogUpdate" itemscope itemtype="http://schema.org/BlogPosting">
						<img data-src="wwwroot/resources/upload/img/news/11-2019/testimonio-03.jpg" src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" class="lazy" alt="Testimonio 1" itemprop="thumbnail">
						<div class="item-inner flex-center">
								<img src="wwwroot/resources/upload/img/banner/11-2019/testimonio-01.png">
								<div class="text">
										<h3 class="h4 text-bold text-primary" itemprop="author">L&#234; Ngọc &#193;nh</h3>
										<h4 class="h text-bold text-white">Train to Fenghuang 2018</h4>
								</div>
						</div>
				</a>
		</div>
</section>
<div id="testimonial-popup" class="bg-lightgray">
		<div class="popup-header bg-white">
				<div class="flex-center container-fluid unclearfix">
						<h4 class="h3 title-section">Phụ huynh n&#243;i về JIS </h4>
						<button type="button" class="js-close"><i class="demo-icon ecs-close"></i>
						</button>
				</div>
		</div>
		<div class="popup-body container"> <a href="javascript:" class="p-top-4 p-bot-2 btn-back text-bold js-close"><i class="demo-icon ecs-arrow-long-left-round"></i> Back to gallery</a>
				<div id="testimonial-js"></div>
		</div>
</div>
<!-- /#testimonial -->