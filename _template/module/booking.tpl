<?php if($position == 'column_right' or $position == 'column_left'){ ?>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr@latest/dist/flatpickr.css">
	<div class="for-form col-xs-12 border-yellow">
		<form class="form-horizontal" id="short-transfer-form" action="/en/transfers/new" method="GET">
			<div class="switcher form-group">
				<div class="col-xs-6">
					<input id="one_way" name="transfer_type" type="radio" value="tinh" checked>
					<label for="one_way">
						<span class="icon-route"></span>Xe đi tỉnh
					</label>
				</div>
				<div class="col-xs-6">
					<input id="hourly" name="transfer_type" type="radio" value="tienchuyen">
					<label for="hourly">
						<span class="icon-clock"></span>Xe tiện chuyến
					</label>
				</div>
			</div>
			<div class="form-group input-wrapper-from">
				<div class="col-xs-12">
					<div class="input-from">
						<span class="button-input-clear icon-cancel"></span>
						<span class="input-icon icon-cab"></span>
						<input id="from" class="form-control inputTransferPoint autocomplete" name="origins" value="" placeholder="Điểm đón: tỉnh thành, khách sạn, địa điểm khác" required>
						<div id="swap-locations" class="swap-locations-button">
							<i class="swap-locations-button-icon icon-loop-alt"></i>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group input-wrapper-to">
				<div class="col-xs-12">
					<div class="input-to">
						<span class="button-input-clear icon-cancel"></span>
						<span class="input-icon icon-direction"></span>
						<input id="to" class="form-control inputTransferPoint autocomplete" name="destinations" value="" placeholder="Điểm đến: tỉnh thành, khách sạn, địa điểm khác" required>
					</div>
				</div>
			</div>
			<div class="form-group input-wrapper-to">
				<div class="col-xs-12 col-md-4">
					<select class="form-control" name="type_car">
						<option disabled selected value="">Chọn loại xe</option>
						<?php foreach($cars as $car){ ?>
						<option value="<?php echo $car['type_car_id']?>"><?php echo $car['name']?></option>
						<?php }?>
					</select>
				</div>
				<div class="col-xs-12 col-md-4">
					<span class="hidden-md visible-xs-block title_form">Chọn thời gian</span>
					<input name="time" class="form-control flatpickr" type="text" placeholder="Chọn thời gian" data-id="datetime">
				</div>
				<div class="col-xs-12 col-md-4">
					<span class="hidden-md visible-xs-block title_form">Chọn phương thức di chuyển</span>
					<fieldset>
						<div class="toggle">
							<input type="radio" name="method" value="1" id="sizeWeight" checked="checked" />
							<label for="sizeWeight">1 chiều</label>
							<input type="radio" name="method" value="2" id="sizeDimensions" />
							<label for="sizeDimensions">2 chiều</label>
						</div>
					</fieldset>
					<style>
						.title_form{
							padding: 7px 0 5px 0;
						}
						fieldset {
							margin: 0;
							box-sizing: border-box;
							display: block;
							border: none;
							min-width: 0;
						}
						fieldset legend {
							margin: 0 0 1.5rem;
							padding: 0;
							width: 100%;
							float: left;
							display: table;
							font-size: 1.5rem;
							line-height: 140%;
							font-weight: 600;
							color: #333;
						}
						fieldset legend + * {
							clear: both;
						}

						body:not(:-moz-handler-blocked) fieldset {
							display: table-cell;
						}

						/* TOGGLE STYLING */
						.toggle {
							box-sizing: border-box;
							font-size: 0;
							display: flex;
							flex-flow: row nowrap;
							justify-content: flex-start;
							align-items: stretch;
						}
						.toggle input {
							width: 0;
							height: 0;
							position: absolute;
							left: -9999px;
						}
						.toggle input + label {
							margin: 0;
							padding: 1rem 2rem;
							box-sizing: border-box;
							position: relative;
							display: inline-block;
							border: solid 1px #DDD;
							background-color: #FFF;
							font-size: 1rem;
							line-height: 140%;
							font-weight: 600;
							text-align: center;
							box-shadow: 0 0 0 rgba(255, 255, 255, 0);
							transition: border-color .15s ease-out,  color .25s ease-out,  background-color .15s ease-out, box-shadow .15s ease-out;
							/* ADD THESE PROPERTIES TO SWITCH FROM AUTO WIDTH TO FULL WIDTH */
							/*flex: 0 0 50%; display: flex; justify-content: center; align-items: center;*/
							/* ----- */
						}
						.col-left .toggle input + label{
							padding: 4px 9px;
						}
						.col-left .for-form{margin-top: 0}
						.toggle input + label:first-of-type {
							border-radius: 3px 0 0 3px;
							border-right: none;
						}
						.toggle input + label:last-of-type {
							border-radius: 0 3px 3px 0;
							border-left: none;
						}
						.toggle input:hover + label {
							border-color: #213140;
						}
						.toggle input:checked + label {
							background-color: #4B9DEA;
							color: #FFF;
							box-shadow: 0 0 10px rgba(102, 179, 251, 0.5);
							border-color: #4B9DEA;
							z-index: 1;
						}
						.toggle input:focus + label {
							outline: dotted 1px #CCC;
							outline-offset: .45rem;
						}
						@media (max-width: 800px) {
							.toggle input + label {
								padding: .75rem .25rem;
								flex: 0 0 50%;
								display: flex;
								justify-content: center;
								align-items: center;
							}
						}

						/* STYLING FOR THE STATUS HELPER TEXT FOR THE DEMO */
						.status {
							margin: 0;
							font-size: 1rem;
							font-weight: 400;
						}
						.status span {
							font-weight: 600;
							color: #B6985A;
						}
						.status span:first-of-type {
							display: inline;
						}
						.status span:last-of-type {
							display: none;
						}
						@media (max-width: 800px) {
							.status span:first-of-type {
								display: none;
							}
							.status span:last-of-type {
								display: inline;
							}
						}

					</style>
					
				</div>
			</div>
			
			<div class="form-group submit-btn-wrapper">
				<div class="col-xs-12 col-md-6">
					<span id="result"></span>
				</div>
				<div class="col-xs-6 col-md-3" style="padding-right: 0;">
					<a style="border-radius: 2px;" onclick="calDistance('short-transfer-form', event)" class="btn btn-block btn-red text-uppercase bold btn-long-text btn-lg">
						<span>Tính giá</span>
					</a>
				</div>
				<div class="col-xs-6 col-md-3">
					<a style="border-radius: 2px;" data-toggle="modal" data-target="#exampleModalCenter" onclick="confirm()" class="btn btn-block btn-red text-uppercase bold btn-long-text btn-lg">
						<span>Đặt xe</span>
					</a>
				</div>
			</div>

		</form>
	</div>

<?php } ?>
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Đặt xe</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="booking">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
        <button type="button" class="btn btn-primary" onclick="booking('confirm_booking_form', event)">Xác nhận đặt chuyến</button>
      </div>
    </div>
  </div>
</div>

<script>
function confirm(){
	
	var request = new XMLHttpRequest();
	// POST to httpbin which returns the POST data as JSON
	request.open('POST', 'index.php?route=module/booking/confirm', /* async = */ false);
	request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	request.send('');
	if (request.status == 200) {
		var json = JSON.parse(request.responseText);
		$('#booking').html(json['html']);
		
		// alert("Chức năng đặt chuyến đang được nâng cấp. Vui lòng gọi tổng đài để được tư vấn tốt nhất"); 
	} else {
		console.log('Error: ' + request.statusText);
	}
	
	
	
	
	// event.preventDefault();
	
	// if (request.status == 200) {
		// var json = JSON.parse(request.responseText);
		// document.getElementById('rs').innerHTML = json['success'];
	// } else {
		// document.getElementById('rs').innerHTML = 'Đặt xe bị lỗi. Vui lòng gọi tới hotline để được hỗ trợ tốt nhất';
		// console.log('Error: ' + request.statusText);
	// }
}
function booking(id, event){
	event.preventDefault();
	var request = new XMLHttpRequest();
	// POST to httpbin which returns the POST data as JSON
	request.open('POST', 'index.php?route=module/booking/booking', /* async = */ false);
	var formData = new FormData(document.getElementById(id));
	request.send(formData);

	// console.log(request);
	
	// var rs = request.responseText.replace('"', '');
	if (request.status == 200) {
		var json = JSON.parse(request.responseText);
		console.log('db', json);
		if(json['err']['telephone']){
			alert(json['err']['telephone']);
		}else{
			alert(json['success']);
			$('#exampleModalCenter').modal('hide');
		}
	} else {
		alert('Đặt xe bị lỗi. Vui lòng gọi tới hotline để được hỗ trợ tốt nhất');
		console.log('Error: ' + request.statusText);
	}
}

function calDistance(id, event){
	event.preventDefault();
	var request = new XMLHttpRequest();
	// POST to httpbin which returns the POST data as JSON
	request.open('POST', 'index.php?route=module/booking/distance', /* async = */ false);
	var formData = new FormData(document.getElementById(id));
	request.send(formData);

	console.log(request);
	
	// var rs = request.responseText.replace('"', '');
	if (request.status == 200) {
		var json = JSON.parse(request.responseText);
		console.log(json);
		if(json.status == 'OK'){
			document.getElementById('result').innerHTML = 'Khoảng cách: <span class="strong highlight">'+ json.distance +'</span><br />Thành tiền: <span class="strong highlight">' + json.price + '</span>';
		}else{
			document.getElementById('result').innerHTML = 'Bạn nhập địa điểm đến hoặc đi không đúng';
		}
	} else {
		document.getElementById('result').innerHTML = 'Bạn nhập địa điểm đến hoặc đi không đúng';
		console.log('Error: ' + request.statusText);
	}
}
</script>
<script>
  var autocomplete;
  var inputs = document.getElementsByClassName('autocomplete');

  function initAutocomplete() {
	for (var i = 0; i < inputs.length; i++) {
	autocomplete = new google.maps.places.Autocomplete(
		/** @type {!HTMLInputElement} */(inputs[i]),
		{types: ['geocode']});
	}
  }

  function geolocate() {
	if (navigator.geolocation) {
	  navigator.geolocation.getCurrentPosition(function(position) {
		var geolocation = {
		  lat: position.coords.latitude,
		  lng: position.coords.longitude
		};
		var circle = new google.maps.Circle({
		  center: geolocation,
		  radius: position.coords.accuracy
		});
		autocomplete.setBounds(circle.getBounds());
	  });
	}
  }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $key_map; ?>&libraries=places&callback=initAutocomplete" async defer></script>
<script src="static/js/flatpickr/flatpickr.min.js"></script>
<script src="static/js/flatpickr/confirmDate.min.js"></script>
<script src="static/js/flatpickr/weekSelect.min.js"></script>
<script src="static/js/flatpickr/rangePlugin.min.js"></script>
<script src="static/js/flatpickr/minMaxTimePlugin.min.js"></script>
<script src="static/js/flatpickr/orgflatpickr.min.js"></script>
