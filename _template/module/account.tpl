<!-- Why Us/Features Section
    ================================================== -->
    <div id="tf-features-2">
        <div class="container">
            <div class="section-header">
                <h2 class="wow fadeInUp top__element" data-wow-delay="0.3s" data-wow-duration="1.5s">Đăng nhập hệ thống</h2>
                <!-- <h5><em>We design and build functional and beautiful websites</em></h5> -->
                <div class="fancy"><img src="img/spr.png" alt=""></div>
            </div>
        </div>
        <div id="feature" class="gray-bg"> <!-- fullwidth gray background -->
            <div class="container"> <!-- container -->
                <div class="row" role="tabpanel"> <!-- row -->
                    <div class="col-md-4 pers"> <!-- tab menu col 4 -->
                        <ul class="features nav nav-pills nav-stacked" role="tablist">
                            <li role="presentation" class="active ">  <!-- feature tab menu #1 -->
                                <a href="#f1" aria-controls="f1" role="tab" data-toggle="tab">
                                    <span class="fa fa-desktop"></span>
                                    Đăng nhập hệ thống<br><small>bấm vào đây để đăng nhập</small>
                                </a>
                            </li>
                            <li role="presentation"> <!-- feature tab menu #2 -->
                                <a href="#f2" aria-controls="f2" role="tab" data-toggle="tab">
                                    <span class="fa fa-pencil"></span>
                                    Chưa có tài khoản ?<br><small>đăng kí tại đây.</small>
                                </a>
                            </li>
                            <li role="presentation"> <!-- feature tab menu #3 -->
                                <a href="#f3" aria-controls="f3" role="tab" data-toggle="tab">
                                    <span class=" fa icon_shield"></span>
                                    Quên mật khẩu ?<br><small>lấy lại mật khẩu tại đây.</small>
                                </a>
                            </li>
                        </ul>
                    </div><!-- end tab menu col 4 -->
                    <div class="col-md-8"> <!-- right content col 6 -->
                      <script>
                        function logout(url){
                          var xmlhttp= window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
                          xmlhttp.onreadystatechange = function () {
                              if (xmlhttp.readyState == XMLHttpRequest.DONE) {
                                  if (xmlhttp.status == 200) {
                                      window.location.href = url;
                                  } else {
                                      console.log('Error: ' + xmlhttp.statusText);
                                  }
                              }
                          }
                          xmlhttp.open("POST","index.php?route=account/logout",true);
                          xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                          xmlhttp.send("");
                        }
                        function submitAccount(id, event, url, route){
                          event.preventDefault();
                          var request = new XMLHttpRequest();
                          // POST to httpbin which returns the POST data as JSON
                          request.open('POST', 'index.php?route=' + route, /* async = */ false);
                          var formData = new FormData(document.getElementById(id));
                          request.send(formData);

                          var rs = request.responseText.replace('"', '');
                          console.log(rs);
                          var rs = rs.split(",");
                          var k=0;
                          rs.splice(-1,1);
                          if (request.status == 200) {

                            [].forEach.bind(document.getElementsByClassName("required"), function (itm) {
                              itm.style.display = "none";
                            })();

                            for(i=1; i<9; i++){
                              var n = i.toString();
                              if (rs.indexOf(n) >= 0) {
                                if(document.getElementById(id+'error'+i)){
                                document.getElementById(id+'error'+i).style.display = "block";
                                }
                                k +=1;
                              }
                            }
                            if(k==0){
                              // document.getElementById('result').innerHTML = '<div class="success color-white">Thành công!</div>';
                              window.location.href = url;
                            }

                          } else {
                            console.log('Error: ' + request.statusText);
                          }
                        }

                        </script>
                        <!-- Tab panes -->
                        <div class="tab-content features-content"> <!-- tab content wrapper -->
                            <div role="tabpanel" class="tab-pane fade in active bg-fadeIn" id="f1"> <!-- feature #1 content open -->
                                <div id="login" class="test bg-grey roomy-60 fix">
                                    <div class="head_title fix">
                                        <h2 class="text-uppercase title-form">đăng nhập hệ thống</h2>
                                        <?php /* ?>
                                        <p>Đăng nhập nhanh với:</p>
                                        <ul class="login-app">
                                            <li><a class="fb" href=""><span class="fa fa-facebook"></span>Facebook</a></li>
                                            <li><a class="gmail" href=""><span class="fa fa-google-plus"></span>Gmail</a></li>
                                        </ul>
                                        <?php */ ?>
                                        <p>Bạn điền <strong>tên đăng nhập</strong> hoặc <strong>email</strong> và <strong> mật khẩu</strong>
                                        </p>
                                    </div>
                                    <form action="" class="form-login" id="login_form" method="post">
                                        <div class="flex-form">
                                            <input type="text" placeholder="Email" name="email" required>
                                            <input type="password" placeholder="Password" name="password" required>
                                        </div>
                                        <button class="button" onclick="submitAccount('login_form', event, 'account/info', 'account/login')">Đăng nhập</button>
                                        <label>
                                            <input type="checkbox" checked="checked" name="remember"> Ghi nhớ đăng nhập
                                        </label>
                                        <span class="psw"><a href="#">Quên mật khẩu?</a></span>
                                    </form>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade bg-fadeIn" id="f2" > <!-- feature #2 content -->
                                <div class="form-signup">
                                    <form method="post" name="register_form" id="register_form" >
                                        <h2>ĐĂNG KÝ MIỄN PHÍ</h2>
                                        <!-- <p>Vui lòng điền vào mẫu này để tạo một tài khoản.</p> -->

                                        <div class="flex-form-2">
                                            <label for="email"><b>Email*</b></label>
                                            <input type="text" placeholder="Enter Email" name="email" required>
                                        </div>
                                        <div class="flex-input">
                                            <div class="flex-form-2 ver2">
                                                <label for="psw"><b>Họ tên</b></label>
                                                <input type="text" placeholder="Enter Name" name="lastname" required>
                                            </div>
                                            <div class="flex-form-2 ver2">
                                                <label for="psw-repeat"><b>Số điện thoại*</b></label>
                                                <input type="text" placeholder="Enter Telephone" name="telephone" required>
                                            </div>
                                        </div>
                                        <div class="flex-input">
                                            <div class="flex-form-2 ver2">
                                                <label for="psw"><b>Mật khẩu*</b></label>
                                                <input type="password" placeholder="Enter Password" name="password" required>
                                            </div>
                                            <div class="flex-form-2 ver2">
                                                <label for="psw-repeat"><b>Nhập lại mật khẩu*</b></label>
                                                <input type="password" placeholder="Repeat Password" name="confirm" required>
                                            </div>
                                        </div>
                                        <label>
                                            <input type="checkbox" checked="checked" name="remember" > Lưu mật khẩu*
                                        </label>
                                        <p>Bằng cách tạo một tài khoản, bạn đồng ý với<a href="#" > Điều khoản & Quyền riêng tư của chúng tôi.</a>.</p>
                                        <div class="clearfix">
                                            <button onclick="submitAccount('register_form', event, 'account/info', 'account/register')" class="signupbtn">Đăng ký ngay</button>
                                            <!-- <button type="button" class="cancelbtn">Trở lại</button> -->
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="f3"> <!-- feature #3 content -->
                                <div class="col-padding">
                                    <div class="heading-block">
                                        <h2>LẤY MẬT KHẨU MỚI</h2>
                                        <span>Bạn nhập <strong>email</strong> và <strong>mã xác thực</strong>. Hệ thống sẽ gửi mật khẩu mới vào email cho bạn.</span>
                                    </div>
                                    <div class="widget-subscribe-form-result"></div>
                                    <form name="loginForm"  action="" method="post">
                                        <div class="col_full">
                                            <input class="sm-form-control required" placeholder="Email" type="">
                                        </div>
                                        <input name="_multiple_" type="hidden" value="2">

                                        <div class="col_half-1">
                                            <div class="col_full">
                                                <img id="CaptchaImage2" src="img/code-demo.png">
                                                <button type="button" name="resetCaptchaDownload" class="btn btn-circle btn-primary" onclick=""><i class="fa fa-refresh"></i> Refresh</button>
                                            </div>
                                            <div class="col_half col_last">
                                                <input type="text"  name="CaptchaInputText2" placeholder="Nhập ký tự xác thực">
                                            </div>
                                        </div>
                                        <div class="clear">
                                            <button type="submit"  value="submit"><i class="icon-email2"></i> Lấy mật khẩu mới</button>
                                            <input name="" type="hidden" value="">
                                        </div>
                                    </form>
                                </div>
                            </div>
<!--  <div role="tabpanel" class="tab-pane fade" id="f4">
<h4>Mobile Application</h4>
<p>Vel donec et scelerisque vestibulum. Condimentum aliquam, mollit magna velit nec, tempor cursus vitae sit aliquet neque purus. Ultrices lacus proin conubia dictum tempus, tempor pede vitae faucibus, sem auctor, molestie diam dictum aliquam. Dolor leo, ridiculus est ut cubilia nec, fermentum arcu praesent.</p>
<img src="img/tab04.png" class="img-responsive" alt="...">
</div>
<div role="tabpanel" class="tab-pane fade" id="f5">
<h4>Relaible Company Analysis</h4>
<p>Vel donec et scelerisque vestibulum. Condimentum aliquam, mollit magna velit nec, tempor cursus vitae sit aliquet neque purus. Ultrices lacus proin conubia dictum tempus, tempor pede vitae faucibus, sem auctor, molestie diam dictum aliquam. Dolor leo, ridiculus est ut cubilia nec, fermentum arcu praesent.</p>
<img src="img/tab05.png" class="img-responsive" alt="...">
</div> -->
</div> <!-- end tab content wrapper -->
</div><!-- end right content col 6 -->
</div> <!-- end row -->
</div> <!-- end container -->
</div><!-- end fullwidth gray background -->
</div>
