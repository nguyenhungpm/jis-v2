<!-- producthot -->
<section id="producthot" class="bg-white">
		<div class="container">
				<h2 class="section-header text-center h0 title-section text-second p-bot-4"><?php echo $title; ?></h2>
				<div class="section-body slider-3item owl-carousel owl-theme owl-flex" id="producthot-slider">
					<?php foreach($products as $product){ ?>
						<div class="item-product" itemscope itemtype="http://schema.org/Product">
								<a href="<?php echo $product['href'];?>" class="img-container">
										<img data-src="<?php echo $product['thumb'];?>" class="owl-lazy" alt="<?php echo $product['name'];?>" itemprop="image">
								</a>
								<div class="text-container">
										<a href="javascript:" class="label">
												<img src="wwwroot/resources/upload/img/label/11-2019/tageasy-go-min.jpg" alt="Easy Go">
										</a>
										<h3 class="h3 title-section" itemprop="name"><a href="<?php echo $product['href'];?>" itemprop="url"><?php echo $product['name'];?></a></h3>
										<div class="date h5">04/03/2020 - 10/03/2020</div>
										<div class="hide price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
												<div class="new-price h3" itemprop="price" content="12690000">12.690.000 VNĐ</div>
												<div class="old-price h4">13.190.000 VNĐ</div>
										</div>
										<div class="rate title-section">
												<div class="rate-item">
														<label for="">Local Experience</label>
														<div class="rage"> <span class="active"></span>
																<span class="active"></span>
																<span class="active"></span>
																<span class="active"></span>
																<span></span>
														</div>
												</div>
												<div class="rate-item">
														<label for="">Me Time</label>
														<div class="rage"> <span class="active"></span>
																<span class="active"></span>
																<span></span>
																<span></span>
																<span></span>
														</div>
												</div>
												<div class="rate-item">
														<label for="">First Time</label>
														<div class="rage"> <span class="active"></span>
																<span class="active"></span>
																<span></span>
																<span></span>
																<span></span>
														</div>
												</div>
										</div>
								</div>
						</div>
						<?php } ?>
				</div>
		</div>
</section>
<!-- /#producthot -->