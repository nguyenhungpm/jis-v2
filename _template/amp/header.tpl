<!doctype html>
<html amp lang="<?php echo $lang; ?>">
<head>
<meta charset="utf-8">
<title><?php echo $title; ?></title>
<?php foreach ($links as $link) { ?>
	<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1, user-scalable=no" />
    <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
	<script async src="https://cdn.ampproject.org/v0.js"></script>
	<?php if (!empty($google_analytics)) { ?>
	<script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
	<?php } ?>
    <style amp-custom>
        body,
        html {
        	height: 100%;
        	margin:0;
        	padding:0;
        	line-height:1.46em;
            font-family: Arial, sans-serif;
			animation: none;
        }
        .ft-cp{background:#eee;text-align:center;padding:5px}
        a{color:#0073aa}
        a:hover{text-decoration:underline}
        #wrap{padding:0 10px}
        .sapo {
            font-weight: 700;
        }
        .related a{
            text-decoration: none
        }
        .relate_box {
            padding-bottom: 15px
        }
                
        .related {
            background-color: #f5f5f5;
            margin: 16px 2%;
            display: block;
            color: #111;
            height: 75px;
            padding: 0;
            width: 96%
        }
       
        
        .related>span {
            font-size: 16px;
            font-weight: 400;
            display: block;
            vertical-align: top;
            margin: 8px
        }
        
        .related:hover {
            background-color: #ccc
        }                
		.relate_box p, .menu-ft p{padding: 0 15px; margin: 0;}
          
        .relate_box a{text-decoration:none}
        .relate_box ul{
			padding-left: 35px;
		}
		
		.relate_box li{padding-right: 15px;}
		        
        h1.heading {
            font-size: 24px
        }
        .hotline{color:red;font-weight:bold;font-size:16px;text-align:center;display:block;padding:5px}
        .content_text p {
            font-size: 14px
        }
        
        .img_box {
            width: 100%
        }        
    </style>

</head>
<body>
<div id="wrap">