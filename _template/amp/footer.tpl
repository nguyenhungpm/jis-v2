</div>
<a href="tel:<?php  echo $hotline; ?>" class="hotline"><?php echo $hotline; ?></a>
<div class="ft-cp">
	<p>© <?php echo $store; ?>. All rights reserved.</p>
</div>
<?php if (!empty($google_analytics)) { ?>
	<amp-analytics type="googleanalytics">
		<script type="application/json">
		{
		  "vars": {
			"account": "<?php echo $google_analytics; ?>"
		  },
		  "triggers": {
			"trackPageview": {
			  "on": "visible",
			  "request": "pageview"
			}
		  }
		}
		</script>
	</amp-analytics>
<?php } ?> 
</body>
</html>