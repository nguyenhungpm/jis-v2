<?php
class Language {
	private $default = 'english';
	private $directory;
	private $db;
	private $data = array();

	public function __construct($directory, $registry = array()) {
		$this->directory = $directory;
		$this->db = $registry->get('db');
		$this->config = $registry->get('config');
	}

	public function get($key) {
		$translated_text = $key;
		if (isset($this->data[$key])) {
			$translated_text = $this->data[$key];
		} else {
			$sql = "SELECT * FROM " . DB_PREFIX . "dictionary d LEFT JOIN " . DB_PREFIX . "dictionary_description dd ON (d.dictionary_id = dd.dictionary_id) WHERE d.keyword = '". $key ."' AND dd.language_id = '" . $this->config->get('config_language_id') . "'";
			$rs = $this->db->query($sql);
			$translated_text = !empty($rs->row['name']) ? $rs->row['name'] : $key;
		}
		return $translated_text;
		// return (isset($this->data[$key]) ? $this->data[$key] : $key);
	}

	public function load($filename) {
		$file = DIR_LANGUAGE . $this->directory . '/' . $filename . '.php';

		if (file_exists($file)) {
			$_ = array();

			require($file);

			$this->data = array_merge($this->data, $_);

			return $this->data;
		}

		// $file = DIR_LANGUAGE . $this->default . '/' . $filename . '.php';

		// if (file_exists($file)) {
		// 	$_ = array();

		// 	require($file);

		// 	$this->data = array_merge($this->data, $_);

		// 	return $this->data;
		// } else {
		// 	trigger_error('Error: Could not load language ' . $filename . '!');
		// //	exit();
		// }
	}
}
?>
