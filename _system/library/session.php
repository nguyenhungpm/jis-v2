<?php
class Session {
	public $data = array();

	public function __construct() {
		if (!session_id()) {
			ini_set('session.use_only_cookies', 'On');
			ini_set('session.use_trans_sid', 'Off');
			ini_set('session.cookie_httponly', 'On');
			ini_set('session.gc_maxlifetime', 7200);

// 			session_set_cookie_params(0, '/');
			session_set_cookie_params(7200, '/');
			session_start();
		}

		$this->data =& $_SESSION;
	}

	function getId() {
		return session_id();
	}
}
?>
