<?php
// Heading
$_['heading_title']    = 'Redirect 301';

// Text
$_['column_action']    = 'Chức năng';
$_['text_success']     = 'Hoàn tất: Bạn đã cập nhật dữ liệu thành công!';

// Entry
$_['entry_old_url']    = 'URL cũ:';
$_['entry_new_url']    = 'URL mới:';

// Error
$_['error_permission'] = 'Cảnh báo: Bạn không được phép thay đổi chức năng!';
?>