<?php
// Heading
$_['heading_title']      = 'Tùy chọn hiển thị giao diện';
$_['entry_catalog_limit'] 	    = 'Số biểu ghi của 1 trang sản phẩm:<br /><span class="help">Xác định như thế nào các website nhiều sẽ được hiển thị trên mỗi trang dạng danh sách sản phẩm</span>';
$_['entry_news_limit'] 	    = 'Số biểu ghi của 1 trang tin tức:<br /><span class="help">Xác định như thế nào các website nhiều sẽ được hiển thị trên mỗi trang tin tức</span>';
$_['entry_admin_limit']   	    = 'Số dòng / 1 trang (Quản trị):<br /><span class="help">Xác định như thế nào nhiều mặt hàng admin được hiển thị trên mỗi trang (các đơn đặt hàng, khách hàng, vv)</span>';
$_['entry_product_date'] = 'Hiển thị ngày cho sản phẩm chuẩn Open Graph';
$_['entry_product_price'] = 'Hiển thị giá sản phẩm';
$_['entry_product_status'] = 'Hiển thị trạng thái hết hàng';
$_['text_option'] = 'Tuỳ chọn';
$_['text_configuration'] = 'Cấu hình';
$_['error_limit']        = 'Bạn chưa nhập tham số';
$_['text_select']              = 'Chọn';
$_['text_no']          ='Không';
$_['button_save']          ='Lưu';
// Error
$_['text_success']      = 'Thành công: Đã lưu lại cấu hình thành công!';
$_['error_permission']  = 'Cảnh báo: Bạn không có quyền sửa đổi chức năng này!';
?>
