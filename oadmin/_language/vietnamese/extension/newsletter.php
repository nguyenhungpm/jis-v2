<?php
// Heading
$_['heading_title']    = 'Đăng kí nhận bản tin';

// Text
$_['text_success']     = 'Thành công: Bạn vừa chỉnh sửa phần đăng kí nhận bản tin!';
$_['text_export']     = 'Xuất Excel';
$_['column_date_added']     = 'Ngày đăng kí';

// Column
$_['column_name']      = 'Language';
$_['column_email']     = 'Email';
$_['column_action']    = 'Action';

// Entry
$_['entry_name']       = 'Language';
$_['entry_code']       = 'Email';



// Error
$_['error_email_exist']= 'Email Id already exist';
$_['error_email']      = 'Invalid Email Format';
$_['error_permission'] = 'Warning: You do not have permission to modify newsletter subscribers!';
?>