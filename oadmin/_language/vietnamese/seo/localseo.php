<?php
// Heading
$_['heading_title']     		  = 'Cấu hình Local SEO';
$_['text_configuration']     		  = 'Cấu hình';
$_['text_yes']                = 'Có';
$_['text_no']                = 'Không';
$_['entry_status']            = 'Trạng thái';
$_['entry_type_seo']          = 'Ngành/Nghề:';
$_['entry_name_seo']          = 'Tên nhà cung cấp:';
$_['entry_street_seo']        = 'Địa chỉ:';
$_['entry_region_seo']        = 'Quận/ Huyện:';
$_['entry_locality_seo']      = 'Tỉnh/Thành phố:';
$_['entry_postalcode_seo']    = 'Mã bưu điện:';
$_['button_save']    = 'Lưu';
$_['text_success']             = 'Thành công: Thiết lập của bạn đã được thay đổi!';
$_['error_warning']             = 'Cảnh báo: Dữ liệu bắt buộc chưa được nhập vào. Kiểm tra các ô trống!';
$_['error_permission']        = 'Cảnh báo: Bạn không được phép thay đổi các cài đặt!';
?>
