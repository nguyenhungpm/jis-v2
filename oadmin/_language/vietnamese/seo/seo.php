<?php
// Heading
$_['heading_title']     		  = 'Thiết lập SEO';
$_['text_configuration']     		  = 'Cấu hình';
$_['text_yes']                = 'Có';
$_['text_no']                 = 'Không';
$_['entry_status']            = 'Trạng thái';
$_['text_enabled']            = 'Mở';
$_['text_disable']            = 'Tắt';
$_['entry_xml']               = 'XML sitemap';
$_['entry_title']             = 'Meta title<span class="help">Chỉ dành cho trang chủ</span>';
$_['entry_meta_description']  = 'Meta description<span class="help">Chỉ dành cho trang chủ</span>';
$_['entry_meta_keywords']  = 'Meta keywords<span class="help">Chỉ dành cho trang chủ</span>';
$_['entry_meta_robots']       = 'Giá trị Robots mặc định<span class="help">Dành cho toàn bộ website</span>';
$_['entry_seo_url']           = 'Dùng SEO URL\'s:<br /><span class="help">Để sử dụng URL SEO thì apache phải được cài đặt module mod-rewrite và bạn cần phải đổi tên htaccess.txt thành .htaccess</span>';
$_['button_save']  =   'Lưu';
$_['text_success']            = 'Thành công: Thiết lập của bạn đã được thay đổi!';
$_['error_warning']           = 'Cảnh báo: Dữ liệu bắt buộc chưa được nhập vào. Kiểm tra các ô trống!';
$_['error_permission']        = 'Cảnh báo: Bạn không được phép thay đổi các cài đặt!';
?>
