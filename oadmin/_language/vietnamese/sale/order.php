<?php
// Heading
$_['heading_title']          = 'Thông tin Đặt hàng';

// Text
$_['text_success']      = 'Thành công: thông tin đã được thay đổi!';
$_['text_enabled']      = 'Đã xác nhận';
$_['text_disabled']      = 'Chưa xem';
$_['entry_customer']      = 'Khách hàng';
$_['entry_phone']      = 'Điện thoại';
$_['entry_content']      = 'Nội dung';
$_['entry_status']      = 'Trạng thái';
$_['entry_date_added']      = 'Ngày gửi';

// Error
$_['error_warning']          = 'Cảnh báo: Dữ liệu bắt buộc chưa được nhập vào. Kiểm tra các ô trống!';
$_['error_permission']       = 'Cảnh báo: Bạn không được phép thay đổi các sản phẩm!';
$_['error_name']             = 'Tên sản phẩm phải lớn hơn 3 và nhỏ hơn 255 ký tự!';
$_['error_model']            = 'Model sản phẩm phải lớn hơn 3 và nhỏ hơn 64 ký tự!';
?>
