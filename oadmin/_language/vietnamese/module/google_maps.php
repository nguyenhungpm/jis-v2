<?php
// Heading
$_['heading_title']			= 'Google Maps Markers';


// Text
$_['text_about_title']		= 'About Google Maps Markers';
$_['text_module']			= 'Modules';
$_['text_success']			= 'Thành công: Bạn đã sửa đổi Google Maps Markers!';
$_['text_content_top']		= 'Content Top';
$_['text_content_bottom']	= 'Content Bottom';
$_['text_column_left']		= 'Column Left';
$_['text_column_right']		= 'Column Right';
$_['text_yes']				= 'Có';
$_['text_no']				= 'Không';


// Buttons
$_['button_addmap']			= 'Vị trí mới';


// Map Marker Entry
$_['entry_mapid']			= 'Vị trí ID<br /><span class="help">Enter a unique integer value.</span>';
$_['entry_mapalias']		= 'Location alias<br /><span class="help">(Optional) Enter an alias of your map marker.</span>';
$_['entry_address']			= 'Địa chỉ<br /><span class="help">(Optional) Nhập địa chỉ.</span>';
$_['entry_latlong']			= 'Vĩ độ, Kinh độ<br /><span class="help">ví dụ: 40.626488,22.94842</span>';
$_['entry_balloonwidth']	= 'Ballon width';
$_['entry_ballon_text']		= 'Balloon Text';


// Map Module Entry
$_['entry_settigns']		= 'Cài đặt Module Map';
$_['entry_mts']				= 'Vị trí IDs<br /><span class="help">Enter the IDs of the map<br />you want to display, separated<br />by commas.<br />For example: 1, 2, 3</span>';
$_['entry_widthheight']		= 'Bản đồ (Chiều rộng x Chiều cao)<br /><span class="help">example: 200px or 100% or auto</span>';
$_['entry_zoom']			= 'Map zoom';
$_['entry_maptype']			= 'Map type';
$_['entry_theme_box']		= 'Khung chủ đề';
$_['entry_theme_show_box']	= 'Hiển thị khung';
$_['entry_theme_box_title']	= 'Tiêu đề khung';
$_['entry_options']			= 'Tùy chọn';
$_['entry_layout']			= 'Bố cục';
$_['entry_position']		= 'Vị trí';
$_['entry_status']			= 'Trạng thái';
$_['entry_sort_order']		= 'Thứ  tự';


// Confirm
$_['confirm_mapid']			= '	Bạn có chắc chắn muốn xóa bản đồ với tọa độ? ';


// Error
$_['error_permission']		= 'Cảnh báo: Quyền hạn bị từ chối!';
$_['error_mapid']			= 'Error: Sai vị trí ID!';
$_['error_latlong']			= 'Error: Sai vĩ độ hoặc Kinh độ!';
$_['error_width']      		= 'Yêu cầu độ rộng';
$_['error_height']     		= 'Yêu cầu  chiều cao!';
$_['error_ids']     		= 'Bạn cần chọn ít nhất 1 ID!';
