<?php
// Heading
$_['heading_title']       = 'Tìm đại lý';

// Text
$_['text_module']         = 'Mô đun';
$_['text_success']        = 'Thành công: bạn đã sửa xong mô đun giới thiệu!';
$_['text_content_top']    = 'Đầu nội dung';
$_['text_content_bottom'] = 'Cuối nội dung';
$_['text_column_left']    = 'Cột trái';
$_['text_clear']    	  = 'Xóa ảnh';
$_['text_image_manager']  = 'File manager';
$_['text_column_right']   = 'Cột phải';

// Entry
$_['entry_description']   = 'Nội dung:';
$_['entry_layout']        = 'Bố cục:';
$_['entry_position']      = 'Ví trí:';
$_['entry_status']        = 'Trạng thái:';
$_['entry_sort_order']    = 'Thứ tự:';

// Error
$_['error_permission']    = 'Warning: You do not have permission to modify this module!';
?>
