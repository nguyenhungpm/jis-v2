<?php
// Heading
$_['heading_title']       = 'Tại sao chọn chúng tôi';

// Text
$_['text_module']         = 'Mô đun';
$_['text_success']        = 'Thành công: Bạn đã sửa thành công mô đun tại sao chọn chúng tôi!';
$_['text_content_top']    = 'Đầu nội dung';
$_['text_content_bottom'] = 'Cuối nội dung';
$_['text_column_left']    = 'Cột trái';
$_['text_column_right']   = 'Cột phải';
$_['text_image_manager']  = 'Image manager';

// Entry
$_['entry_description']   = 'Nội dung:';
$_['entry_layout']        = 'Bố cục:';
$_['entry_position']      = 'Ví trí:';
$_['entry_status']        = 'Trạng thái:';
$_['entry_sort_order']    = 'Thứ tự:';

// Error
$_['error_permission']    = 'Warning: You do not have permission to modify why us module!';
?>