<?php
// header
$_['heading_title']  = 'OSD.VN SimpleCMS V3 | Đăng nhập vào màn hình quản trị';

// Text
$_['text_heading']   = 'Đăng nhập vào màn hình quản trị';
$_['text_login']     = 'Bạn hãy điền thông tin đăng nhập.';
$_['text_forgotten'] = 'Quên mật khẩu?';

// Entry
$_['entry_username'] = 'Tên đăng nhập';
$_['entry_password'] = 'Mật khẩu của bạn';
$_['entry_captcha']  = 'Mã bảo mật';
$_['error_captcha']  = 'Mã bạn khai báo không chính xác, xin hãy thử lại!';
// Button
$_['button_login']   = 'Đăng nhập';

// Error
$_['error_login']    = 'Sai tên đăng nhập hoặc mật khẩu!';
$_['error_token']    = 'Đăng nhập hết hạn. Hãy đăng nhập lại!';
?>
