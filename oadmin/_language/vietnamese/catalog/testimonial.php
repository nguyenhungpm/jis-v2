<?php
// Heading
$_['heading_title']     	= 'Đánh giá';

// Text
$_['text_success']      	= 'Success: Bạn đã sửa thành công mô-đun Đánh giá!';
$_['text_enabled']      	= 'Enabled';
$_['text_disabled']     	= 'Disabled';
$_['text_module_settings']   	= 'Cài đặt';

// Column
$_['column_title']      	= 'Tiêu đề';
$_['column_name']			= 'Tên';
$_['column_city']			= 'City';
$_['column_status']		= 'Trạng thái';
$_['column_action']     	= 'Thao tác';
$_['column_date_added'] 	= 'Ngày thêm';
$_['column_description'] 	= 'Nội dung';

// Entry
$_['entry_title']       	= 'Tiêu đề:';
$_['entry_status']     		= 'Trạng thái:';
$_['entry_description'] 	= 'Nội dung:';
$_['entry_date_added']  	= 'Ngày thêm:';
$_['entry_name']  		= 'Tên:';
$_['entry_city']			= 'Thành phố';
$_['entry_rating']        	= 'Xếp hạng:';
$_['entry_good']          	= 'Tốt';
$_['entry_bad']           	= 'Kém';
$_['entry_install_first']  	= 'Hãy cài đặt mô-đun Đánh giá trước';
$_['entry_email']        	= 'E-Mail:';

// Error 
$_['error_permission']  	= 'Warning: Bạn không đủ thẩm quyền !';
$_['error_name']       		= 'Name must be greater than 3 and less than 64 characters!';
$_['error_description'] 	= 'Message must be greater than 3 characters!';
?>
