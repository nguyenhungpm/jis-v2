<?php

$_['heading_title']    = 'Hỏi đáp';
$_['text_module']         = 'Modules';

$_['error_permission'] = 'Cảnh báo: Bạn không có quyền sửa đổi mô-đun!';
$_['success'] = 'Cài đặt đã được lưu';

$_['pq_questions'] = 'Hỏi đáp';
$_['pq_settings'] = 'Các cài đặt';
$_['pq_sidebar'] = 'Sidebar';

$_['pq_link_questions'] = '<a href="%s">Các câu hỏi</a>';
$_['pq_total_questions'] = '<a href="%s">Tổng số câu hỏi:</a>';
$_['pq_total_questions_unanswered'] = '<a href="%s">câu hỏi chưa được trả lời</a>';

$_['pq_layout'] = 'Giao diện';
$_['pq_position'] = 'Vị trí';
$_['pq_status'] = 'Tình trạng';
$_['pq_sort_order'] = 'Thứ tự sắp xếp';
$_['pq_content_top'] = 'Content top';
$_['pq_content_bottom'] = 'Content bottom';
$_['pq_column_left'] = 'Column left';
$_['pq_column_right'] = 'Column right';

$_['text_id'] = 'ID';
$_['text_product'] = 'Sản phẩm';
$_['text_save'] = 'Save';
$_['text_delete'] = 'Xóa';
$_['text_question'] = 'Câu hỏi';
$_['text_answer'] = 'Trả lời';
$_['text_display'] = 'Hiển thị';
$_['text_email'] = 'Email';

$_['text_questions_per_page'] = 'Câu hỏi mỗi trang';
$_['text_questions_per_page_desc'] = 'Số lượng câu hỏi của mỗi trang (0 = tất cả trên một)';
$_['text_max_question_length'] = 'Chiều dài câu hỏi lớn nhất';
$_['text_max_question_length_desc'] = 'Chiều dài câu hỏi tối đa trong chars (frontend-chỉ, 0 = không hạn chế)';
$_['text_email_for_notices'] = 'E-mail cho thông báo';
$_['text_email_for_notices_desc'] = 'E-mail địa chỉ để gửi "câu hỏi mới được thêm vào" thông báo (trống = không gửi thông báo';

$_['text_no_questions_added'] = 'Chưa có câu hỏi nào được gửi';

$_['pq_mail_text_subject'] = '[%s] Câu hỏi của bạn về %s trả lời!';
$_['pq_mail_text_question_answered'] = 'Câu hỏi của bạn đến %s về %s đã được trả lời!';

$_['pqs_mail_text_subject'] = '[%s] câu hỏi của bạn đã trả lời!';
$_['pqs_mail_text_question_answered'] = 'Câu hỏi của bạn tại %s đã được trả lời!';

$_['mail_text_greeting'] = 'Xin chào %s,';
$_['mail_text_product'] = 'Sản phẩm:';
$_['mail_text_question'] = 'Câu hỏi:';
$_['mail_text_answer'] = 'Trả lời:';
$_['mail_text_sincerely'] = 'Trân trọng';

?>