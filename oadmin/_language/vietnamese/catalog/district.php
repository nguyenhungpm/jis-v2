<?php
// Heading
$_['heading_title']          = 'Quận/Huyện';

// Text
$_['text_success']           = 'Thành công. Quận huyện đã được cập nhật thành công!';
$_['text_default']           = 'Mặc định';
$_['text_image_manager']     = 'Quản lý ảnh';
$_['text_browse']            = 'Browse Files';
$_['text_clear']             = 'Clear Image';

// Column
$_['column_name']            = 'Quận huyện';
$_['column_sort_order']      = 'Th? t?';
$_['column_action']          = 'Hành động';
$_['column_image']          = 'Mã quận/huyện';

// Entry
$_['entry_name']             = 'T�n danh m?c tin t?c:';
$_['entry_meta_keyword'] 	 = 'Meta Tag Keywords:';
$_['entry_meta_description'] = 'Meta Tag Description:';
$_['entry_description']      = 'M� t?:';
$_['entry_parent']           = 'C?p danh m?c:';
$_['entry_store']            = 'C?a h�ng:';
$_['entry_keyword']          = 'T? kh�a SEO:<br/><span class="help">Gi� tr? n�y ph?i duy nh?t.</span>';
$_['entry_image']            = 'H�nh ?nh:';
$_['entry_top']              = 'L�n tr�n:<br/><span class="help"Hi?n th? trong thanh menu ph�a tr�n. Ch? ho?t d?ng cho c�c danh m?c l�n d?u trang.</span>';
$_['entry_column']           = 'C?t:<br/><span class="help">S? c?t d? s? d?ng cho 3 danh m?c. Ch? ho?t d?ng cho c�c lo?i danh m?c d?u trang.</span>';
$_['entry_sort_order']       = 'Th? t?:';
$_['entry_status']           = 'Tr?ng th�i:';
$_['entry_layout']           = 'Ghi d� giao di?n:';

// Error 
$_['error_warning']          = 'C?nh b�o: Y�u c?u nh?p th�ng tin b?c bu?c!';
$_['error_permission']       = 'C?nh b�o: B?n kh�ng du?c ph�p c?p nh?t danh m?c tin t?c!';
$_['error_name']             = 'T�n danh m?c tin t?c ph?i c� t? 3 d?n 32 k� t?!';
?>