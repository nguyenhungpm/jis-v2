<?php
// Heading
$_['heading_title']      = 'Đội ngũ';

// Text
$_['text_success']       = 'Thành công: đội ngũ đã được thay đổi!';
$_['text_default']       = 'Mặc định';
$_['text_image_manager'] = 'Quản lý hình';
$_['text_browse']        = 'Duyệt file';
$_['text_clear']         = 'Xóa Ảnh';
$_['text_percent']       = 'Tỷ lệ phần trăm';
$_['text_amount']        = 'Số lượng Cố định';
$_['text_module_status']     = 'Module is installed';

$_['entry_meta_description'] = 'Meta Description:';
$_['entry_meta_keyword']     = 'Meta Keywords:';
$_['entry_description']      = 'Mô tả:';
$_['entry_meta_title'] = 'Meta Title:';
// Column
$_['column_name']        = 'Tiêu đề';
$_['column_sort_order']  = 'Thứ tự';
$_['column_action']      = 'Thao tác';

// Entry
$_['entry_name']         = 'Tiêu đề:';
$_['entry_store']        = 'Website:';
$_['entry_keyword']      = 'SEO URL:';
$_['entry_image']        = 'Hình ảnh:';
$_['entry_sort_order']   = 'Thứ tự:';
$_['entry_type']         = 'Loại:';

// Error
$_['error_permission']   = 'Cảnh báo: Bạn không được phép thay đổi chức năng này!';
$_['error_name']         = 'Tên phải lớn hơn 3 và nhỏ hơn 12 ký tự!';
$_['error_product']      = 'Cảnh báo: không thể xóa, vì đội ngũ này đã được kết nối với %s!';
?>
