<?php
// Heading
$_['heading_title']          = 'Nhóm bài viết';
$_['heading_title_add']      = 'Thêm nhóm bài viết';

 
// Text
$_['text_success']           = 'Thành công: Nhóm bài viết đã được thay đổi!';
$_['text_default']           = 'Mặc định';
$_['text_image_manager']     = 'Quản lý hình ảnh';
$_['text_browse']            = 'Duyệt file';
$_['text_clear']             = 'Xóa ảnh';

// Column
$_['column_name']            = 'Tên nhóm bài viết';
$_['column_sort_order']      = 'Thứ tự';
$_['column_action']          = 'Thao tác';
$_['column_status']             = 'Status';
// Entry
$_['entry_name']             = 'Tên Danh mục:';
$_['entry_meta_keyword']     = 'Meta Keyword';
$_['entry_meta_description'] = 'Meta Description:';
$_['entry_description']      = 'Mô tả nhóm bài viết:';
$_['entry_parent']           = 'Danh mục cha:';
$_['entry_filter']           = 'Filters:<br /><span class="help">(Autocomplete)</span>';
$_['entry_store']            = 'Website:';
$_['entry_keyword']          = 'SEO URL';
$_['entry_image']            = 'Hình ảnh:';
$_['entry_top']              = 'Phía trên:<br/><span class="help">Hiển thị trong thanh menu phía trên. Chỉ có tác dụng đối với các danh mục cấp cao nhất.</span>';
$_['entry_column']           = 'Cột:<br/><span class="help">Số thứ tự cột để sử dụng cho 3 danh mục phía dưới. Chỉ có tác dụng đối với các danh mục cấp cao nhất.</span>';
$_['entry_sort_order']       = 'Thứ tự:';
$_['entry_status']           = 'Trạng thái:';
$_['entry_layout']           = 'Bố cục:';

// Error 
$_['error_warning']          = 'Cảnh báo: Dữ liệu bắt buộc chưa được nhập vào. Kiểm tra các ô trống!';
$_['error_permission']       = 'Cảnh báo: Bạn không được phép thay đổi các nhóm bài viết!';
$_['error_name']             = 'Tên Danh mục phải lớn hơn 2 và ít hơn 32 ký tự!';
?>