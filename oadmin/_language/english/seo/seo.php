<?php
// Heading
$_['heading_title']     	  = 'Global SEO Settings';
$_['text_yes']                = 'Yes';
$_['text_no']                 = 'No';
$_['text_or']                 = 'Or';
$_['entry_status']            = 'Status';
$_['text_enabled']            = 'Enable';
$_['text_disable']            = 'Diabled';
$_['entry_xml']               = 'XML sitemap';
$_['entry_google_analytics']  = 'Google Analytics Code<span class="help">CODE only</span>';
$_['entry_google_searchconsole']  = 'Google Search Console<span class="help">Verify with Meta tag method</span>';
$_['entry_title']             = 'Meta title<span class="help">For homepage only</span>';
$_['entry_meta_description']  = 'Meta description<span class="help">For homepage only</span>';
$_['entry_meta_keywords']  = 'Meta keywords<span class="help">For homepage only</span>';
$_['entry_meta_robots']       = 'Meta robots tag <span class="help">For all of pages</span>';
$_['entry_seo_url']           = 'SEO URL:<br /><span class="help">Your Apache server must enable mod-rewrite and you must rename htaccess.txt to .htaccess</span>';
$_['button_save']  =   'Save';
$_['text_success']           = 'Success: Your changes have been saved!';
$_['error_warning']        		= 'Warning: Please check the empty fields!';
$_['error_permission'] = 'Warning: You are not allowed to change section!';

?>
