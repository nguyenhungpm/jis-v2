<?php
// Heading
$_['heading_title']     		= 'Local SEO';
$_['text_yes']               	 = 'Yes';
$_['text_no']                	= 'No';
$_['entry_status']            = 'Status';
$_['entry_type_seo']          = 'Service';
$_['entry_name_seo']          = 'Name of provider';
$_['entry_street_seo']        = 'Address';
$_['entry_region_seo']        = 'City';
$_['entry_locality_seo']      = 'State';
$_['entry_postalcode_seo']    = 'Postal code';
$_['button_save']    = 'Save';
$_['text_success']             = 'Success: Your changes have been saved!';
$_['error_warning']        		= 'Warning: Please check the empty fields!';
$_['error_permission'] = 'Warning: You are not allowed to change section!';
?>
