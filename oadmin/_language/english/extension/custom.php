<?php
// Heading
$_['heading_title']      = 'Custom Script & CSS';

// Text
$_['entry_header'] = 'In header<span class="help small">This content will be generated in &lt;head&gt; and &lt;/head&gt;tags </span>';
$_['entry_footer'] = 'In Footer<span class="help small">This content will be generated before &lt;/body&gt;</span>';
$_['text_success']       = 'Your changes have been saved!';
// Error
$_['error_permission'] = 'Warning: You do not have permission to modify newsletter subscribers!';
?>
