<?php
// Heading
$_['heading_title']       = 'Newsletter Subcribe box';

// Text
$_['text_module']         = 'Module';
$_['text_content_menu']    = 'Content Menu';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';

// Entry
$_['entry_layout']        = 'Layout:';
$_['entry_position']      = 'Position:';
$_['entry_name']      = 'Name:';
$_['entry_status']        = 'Status:';
$_['entry_sort_order']    = 'Sort Order:';
// Error
$_['error_permission'] 			= 'Warning: You do not have permission to modify the this module!';
?>
