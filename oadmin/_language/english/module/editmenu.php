<?php
// Heading
$_['heading_title']       = 'Opencart Top Menu Editer';

// Text
$_['text_module']         = 'menu editer';
$_['text_success']        = 'Success: You have modified module menu editer!';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';

// Entry
$_['entry_layout']        = 'Store:';
$_['entry_position']      = 'Top Menu:';
$_['entry_status']        = 'Status:';
$_['entry_sort_order']    = 'Columns:';

// Error
$_['error_permission']    = 'Warning: You do not have permission to modify module account!';
$_['error_duplicate_store']    = 'Warning: You can\'t set 2 menu on 1 store !';

?>