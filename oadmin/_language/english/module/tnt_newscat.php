<?php
// Heading
$_['heading_title']       = 'News by Category';

// Text
$_['text_module']         = 'Module';
$_['text_content_menu']    = 'Content Menu';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';

// Entry
$_['entry_cat']        = 'Category';
$_['entry_limit']        = 'Limit';
$_['entry_limitdescription']        = 'Characters to show';
$_['entry_image']        = 'Image';
$_['entry_description']        = 'Description';
$_['entry_imagestatus']        = 'Show image';
$_['entry_leading']        = '# leading items';

$_['entry_layout']        = 'Layout';
$_['entry_position']      = 'Position';
$_['entry_name']      = 'Name';
$_['entry_status']        = 'Status';
$_['entry_sort_order']    = 'Sort Order';
// Error
$_['error_permission'] 			= 'Warning: You do not have permission to modify the this module!';
?>
