<?php 
$_['heading_title']        				= 'Đơn đặt hàng'; 
$_['heading_title_contact_list']        = 'Contact List'; 
$_['heading_title_contact_details']        = 'Contact Details';
$_['heading_title_contact_reply']        = 'Contact Reply';
$_['heading_title_contact']          	= 'Contact Information'; 
$_['heading_title_setting']           	= 'Contact Us - Settings'; 

$_['text_contact']              = 'Contact';
$_['text_module']              = 'Module'; 
 
$_['reply_message']              = 'Reply has been sent.'; 

$_['text_status']          = 'Status';
$_['text_enabled']         = 'Enabled';
$_['text_disabled']        = 'Disabled';
$_['text_manage']          = 'Manage';
$_['text_view']            = 'View Details';



//button
$_['button_approve']       = 'Approve';
$_['button_disapprove']    = 'Disapprove';
$_['button_view']          = 'Xem';
$_['button_delete']        = 'Xóa';
$_['button_cancel']        = 'Hủy';
$_['button_save']          = 'Lưu';
$_['button_reply']         = 'Trả lời';
$_['button_send']          = 'Gửi';
$_['button_read']          = 'Đánh dấu đã xem';
$_['button_csv']           = 'Export excel';





// Column
$_['text_setting']          = 'Settings';
$_['text_customer_view']    = 'Customer View';

$_['column_name']           = 'Contact Name';
$_['column_select']         = 'Select';
$_['column_email']          = 'E-Mail';
$_['column_description']    = 'Message';
$_['column_status']         = 'Status'; 
$_['text_no_results']       = 'No Result Found';
$_['column_date_added']     = 'Date Added';
$_['column_comment']        = 'Comment';
$_['column_description']    = 'Message';
$_['column_points']         = 'Points';
$_['column_ip']             = 'Address';
$_['column_total']          = 'Total Accounts';
$_['column_action']         = 'Action';




// Error
$_['error_permission']     = 'Warning: You do not have permission to modify Contact to DB extensions!';
$_['update_success']       = 'Module has been updated Contact to DB module';
$_['error_app_id']         = 'App ID Required!';
$_['error_app_secret']     = 'App Secret Required!';

?>
