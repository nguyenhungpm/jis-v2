<?php
// Heading
$_['heading_title']           = 'Cảm nhận của khách hàng';

// Text
$_['text_module']             = 'Modules';
$_['text_success']     	      = 'Thành công: Bạn đã chỉnh sửa thành công!';
$_['text_content_top']        = 'Content Top';
$_['text_content_bottom']     = 'Content Bottom';
$_['text_column_left']        = 'Column Left';
$_['text_column_right']       = 'Column Right';
$_['text_edit_testimonials']  = 'Xem đánh giá';

// Entry
$_['entry_layout']            = 'Layout:';
$_['entry_position']          = 'Vị trí:';
$_['entry_status']            = 'Trạng thái:';
$_['entry_sort_order']        = 'Sort Order:';
$_['entry_limit']             = 'Giới hạn:';
$_['entry_title']             = 'Title:';
$_['entry_character_limit']   = 'Số kí tự hiển thị:';
$_['entry_admin_approved']    = 'Admin phê duyệt:';
$_['entry_default_rating']    = 'Đánh giá mặc định:';
$_['entry_good']          	= 'Good';
$_['entry_bad']           	= 'Bad';
$_['entry_random']      	= 'Random:';
$_['entry_send_to_admin']     = 'Gửi đánh giá cho E-Mail Admin :';
$_['entry_all_page_limit']    = 'Giới hạn số bài đánh giá:';


// Error
$_['error_permission']        = 'Warning: You do not have permission to modify module testimonial!';
?>