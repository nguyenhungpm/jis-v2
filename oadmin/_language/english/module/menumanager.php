<?php
// Heading
$_['heading_title']       = 'Menu Manager';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module menu manager!';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';
$_['text_menu_info']   = 'Info';
$_['text_current_group']   = 'Current Group';



// Entry
$_['entry_layout']        = 'Layout:';
$_['entry_position']      = 'Position:';
$_['entry_status']        = 'Status:';
$_['entry_sort_order']    = 'Sort Order:';
$_['entry_menu_info']   = '<p>Drag the menu list to re-order, and click <b>Update Menu</b> to save the position.</p><p>To add a menu item, use the form below.</p>';
// Error
$_['error_permission']    = 'Warning: You do not have permission to modify module menumanager!';
?>