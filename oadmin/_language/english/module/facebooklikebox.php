<?php
// Heading

$_['heading_title']       = '<img src="view/javascript/cambrian/arrow.gif" /> <span style="color:#449DD0; font-weight:bold">Cambrianinfosystems Facebook Like Box<a style="color:#A6D9FF; href="http://cambrianinfosystems.com/">cambrianinfosystems.com</a></span>';

$_['heading_title2']       = '<img src="view/javascript/cambrian/arrow.gif" /> <span style="color:#449DD0; font-weight:bold">Cambrianinfosystems Facebook Like Box<a style="color:#A6D9FF; href="http://cambrianinfosystems.com/">cambrianinfosystems.com</a></span>';


$_['title']       = 'Cambrianinfosystems Facebook Like Box';
// Entry
$_['entry_facebookurl']       	= 'Facebook Url:<br /><span class="help">(Your Page URL.<br>
e.g. https://www.facebook.com/FacebookDevelopers )</span>';
$_['entry_facebooktitle']    	= 'Facebook Title';
$_['entry_facebook_containor_border_color']    	= 'Container Border Color:<br /><span class="help">click inside input and choose color</span>';
$_['entry_facebook_logo_color']    	= 'Logo Color:<br /><span class="help">click inside input and choose color</span>';
$_['entry_facebook_display_side']    	= 'Display - Side:<br /><span class="help">left/right side</span>';
$_['entry_facebook_margin_top']    	= 'Margin from top:<br /><span class="help">Percent</span>';


// Columns

$_['column_colorscheme']       	= 'Color Scheme:';
$_['column_force_wall']         	= 'Force Wall:<br /><span class="help">To specifies the stream contains posts by the Page or just check-ins from friends.</span>';
$_['column_header']        		= 'Header:<br /><span class="help">Specifies whether to display the Facebook header at the top of the plugin.</span>';
$_['column_show_border']        	= 'Show Border:';
$_['column_show_faces']       	= 'Show Faces:<br /><span class="help">Specifies whether to display profile photos of people who like the page.</span>';
$_['column_stream']       	 	= 'Stream:<br /><span class="help">Specifies whether to display a stream of the latest posts by the Page.</span>';
$_['column_div']         		= 'Div (W x H) and Resize Type:';
$_['column_layout']        		= 'Layout:';
$_['column_position']      		= 'Position:';
$_['column_status']        		= 'Status:';
$_['column_sort_order']    		= 'Sort Order:';
$_['column_module_type']    	= 'Module Type:';


// Text
$_['text_module']         		= 'Modules';
$_['text_success']       		= 'Success: You have modified module Facebook Like Box!';
$_['text_content_top']    		= 'Content Top';
$_['text_content_bottom'] 		= 'Content Bottom';
$_['text_column_left']    		= 'Column Left';
$_['text_column_right']   		= 'Column Right';
$_['text_colorscheme_light']  	= 'Light';
$_['text_colorscheme_dark']   	= 'Dark';
$_['text_yes']    				= 'Yes';
$_['text_no']    				= 'No';
$_['text_facebook_display_right']         		= 'Right Side';
$_['text_facebook_display_left']         		= 'Left Side';
$_['text_facebook_module']         				= 'Module';
$_['text_facebook_slidebox']         			= 'Slide Box';


// Error 
$_['error_permission']    = 'Warning: You do not have permission to modify module facebook like box!';
$_['error_div']         = 'Div width &amp; height dimensions required!';

 $_['text_licence_info'] 					= 'To activate module please provide your email ID.';
 $_['entry_transaction_id'] 					= 'Order ID';
  $_['entry_transaction_email'] 					= 'Email ID';

$_['text_info']           = '<p>Developed by <a href="http://cambrianinfosystems.com/" target="_blank" alt="cambrianinfosystems"><img src="view/javascript/cambrian/logo.png" alt="cambrianinfosystems"></a></p> 
  <p>
<strong>Support Email:</strong> support@cambrianinfosystems.com</p>

<p><strong>Development Request</strong>: Info@cambrianinfosystems.com</p>

 Log a Support Ticket Here: <a href="http://support.cambrianinfosystems.com/" target="_blank" alt="cambrianinfosystems Shop" >http://support.cambrianinfosystems.com/</a></p>';

?>