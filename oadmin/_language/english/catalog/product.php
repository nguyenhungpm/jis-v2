<?php
// Heading
$_['heading_title']          = 'Products'; 

// Text  
$_['text_success']           = 'Success: You have modified products!';
$_['text_plus']              = '+';
$_['text_minus']             = '-';
$_['text_default']           = 'Default';
$_['text_image_manager']     = 'Image Manager';
$_['text_browse']            = 'Browse';
$_['text_clear']             = 'Clear';
$_['text_option']            = 'Option';
$_['text_option_value']      = 'Option Value';
$_['text_percent']           = 'Percentage';
$_['text_amount']            = 'Fixed Amount';

// Column
$_['column_name']            = 'Product Name';
$_['column_model']           = 'Model';
$_['column_image']           = 'Image';
$_['column_price']           = 'Price';
$_['column_quantity']        = 'Quantity';
$_['column_status']          = 'Status';
$_['column_action']          = 'Action';
$_['text_outofstock']        = 'Out of stock';
$_['text_instock']        	 = 'In Stock';

$_['heading_title_add']     = 'Add new product';

// Entry
$_['entry_name']             = 'Product Name:';
$_['entry_meta_keyword'] 	 = 'Meta Tag Keywords:';
$_['entry_meta_description'] = 'Meta Tag Description:';
$_['entry_description']      = 'Description:';
$_['entry_keyword']          = 'SEO URL:<br /><span class="help">Do not use spaces instead replace spaces with - and make sure the url is globally unique.</span>';
$_['entry_model']            = 'Model:';
$_['entry_sku']              = 'SKU:<br/><span class="help">Stock Keeping Unit</span>';

$_['column_category']        = 'Category';
$_['entry_location']         = 'Location:';
$_['entry_shipping']         = 'Requires Shipping:'; 
$_['entry_manufacturer']     = 'Manufacturer:<br /><span class="help">(Autocomplete)</span>';
$_['entry_date_available']   = 'Date Available:';
$_['entry_quantity']         = 'Quantity:';
$_['entry_minimum']          = 'Minimum Quantity:<br/><span class="help">Force a minimum ordered amount</span>';
$_['entry_stock_status']     = 'Out Of Stock Status:<br/><span class="help">Status shown when a product is out of stock</span>';
$_['entry_price']            = 'Price:';
$_['entry_dimension']        = 'Dimensions (L x W x H):';
$_['entry_image']            = 'Thumbnail';
$_['entry_customer_group']   = 'Customer Group:';
$_['entry_date_start']       = 'Date Start:';
$_['entry_date_end']         = 'Date End:';
$_['entry_priority']         = 'Priority:';
$_['entry_attribute']        = 'Attribute:';
$_['entry_attribute_group']  = 'Attribute Group:';
$_['entry_text']             = 'Text:';
$_['entry_option']           = 'Option:';
$_['entry_option_value']     = 'Option Value:';
$_['entry_required']         = 'Required:';
$_['entry_status']           = 'Status:';
$_['entry_sort_order']       = 'Sort Order:';
$_['entry_category']         = 'Categories';
$_['entry_filter']           = 'Filters:<br /><span class="help">(Autocomplete)</span>';
$_['entry_download']         = 'Downloads:<br /><span class="help">(Autocomplete)</span>';
$_['entry_related_products'] = 'Related Products:<br /><span class="help">(Autocomplete)</span>';
$_['entry_related_news']     = 'Related News:<br /><span class="help">(Autocomplete)</span>';
$_['entry_tag']          	 = 'Product Tags:<br /><span class="help">comma separated</span>';
$_['entry_reward']           = 'Reward Points:';
$_['entry_layout']           = 'Layout Override:';
$_['entry_profile']          = 'Profile:';
$_['entry_all_category']     = 'All categories';
$_['entry_short_description']='Short Description';
$_['text_gallery']			 = 'Photo gallery';

$_['text_length_day']        = 'Day';
$_['text_length_week']       = 'Week';
$_['text_length_month']      = 'Month';
$_['text_length_month_semi'] = 'Semi Month';
$_['text_length_year']       = 'Year';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify products!';
$_['error_name']             = 'Product Name must be greater than 3 and less than 255 characters!';
$_['error_model']            = 'Product Model must be greater than 3 and less than 64 characters!';
?>