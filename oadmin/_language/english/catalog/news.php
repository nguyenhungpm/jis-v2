<?php
// Heading
$_['heading_title']          = 'Articles';
$_['heading_title_addnew']   = 'Add new article';

// Text
$_['text_success']           = 'Success: You have modified aritles';
$_['text_default']           = 'Default';
$_['text_image_manager']     = 'Image manager';
$_['text_browse']            = 'Browse';
$_['text_clear']             = 'Clear';

// Column
$_['column_name']            = 'Name';
$_['column_image']           = 'Thumbnail';
$_['column_short_description']      = 'Short description';
$_['column_date_added']           = 'Date added';
$_['column_viewed']           = 'Views';
$_['column_order']          = 'Order';
$_['column_category']       = 'Category';
$_['column_status']          = 'Status';
$_['column_action']          = 'Action';

// Entry
$_['entry_name']             = 'Title';
$_['entry_short_description']      = 'Short description';
$_['entry_meta_keyword'] 	 	= 'Meta Keywords';
$_['entry_meta_description'] = 'Meta Description';
$_['entry_description']      = 'Description';
$_['entry_store']            = 'Website:';
$_['entry_keyword']          = 'SEO URL:';
$_['entry_date_available']   = 'Date Available';
$_['entry_image']            = 'Thumbnail';
$_['entry_related_news']     = 'Related news';
$_['entry_approved']         = 'Approved';
$_['entry_status']           = 'Status';
$_['entry_sort_order']       = 'Sort Order';
$_['entry_category']         = 'Choose category';
$_['entry_related']          = 'Related news';
$_['entry_tag']          	 = 'Tags';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify artiles!';
$_['error_name']             = 'Article Name must be greater than 3 and less than 255 characters!';
?>