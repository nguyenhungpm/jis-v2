<?php
// Heading
$_['heading_title']          = 'News Category';

// Text
$_['text_success']           = 'Success: You have modified categories!';
$_['text_default']           = 'Default';
$_['text_image_manager']     = 'Image Manager';
$_['text_browse']            = 'Browse';
$_['text_clear']             = 'Clear';

// Column
$_['column_name']            = 'Category Name';
$_['column_sort_order']      = 'Sort Order';
$_['column_action']          = 'Action';

// Entry
$_['entry_name']             = 'News Category Name';
$_['entry_meta_keyword'] 	 = 'Meta Keywords';
$_['entry_meta_description'] = 'Meta Description';
$_['entry_description']      = 'Description';
$_['entry_parent']           = 'Parent';
$_['entry_store']            = 'Website:';
$_['entry_keyword']          = 'SEO URL';
$_['entry_image']            = 'Image';
$_['entry_sort_order']       = 'Sort Order';
$_['entry_status']           = 'Status';

// Error 
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify categories!';
$_['error_name']             = 'Category Name must be between 2 and 32 characters!';
?>