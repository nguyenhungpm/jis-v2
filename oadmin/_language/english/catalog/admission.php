<?php
// Heading
$_['heading_title']     = 'Admission';

// Text
$_['text_success']      = 'Success: You have modified admission!';
$_['text_default']      = 'Default';

// Column
$_['column_title']      = 'Admission Title';
$_['column_sort_order']	= 'Sort Order';
$_['column_action']     = 'Action';

// Entry
$_['entry_title']       = 'Admission Title';
$_['entry_description'] = 'Description';
$_['entry_keyword']     = 'SEO URL';
$_['entry_meta_keyword']     = 'Meta keyword';
$_['entry_meta_description']     = 'Meta Description';
$_['entry_status']      = 'Status:';
$_['entry_sort_order']  = 'Sort Order';

// Error 
$_['error_warning']     = 'Warning: Please check the form carefully for errors!';
$_['error_permission']  = 'Warning: You do not have permission to modify admission!';
$_['error_title']       = 'Admission Title must be between 3 and 64 characters!';
$_['error_description'] = 'Description must be more than 3 characters!';
$_['error_account']     = 'Warning: This admission page cannot be deleted as it is currently assigned as the store account terms!';
$_['error_checkout']    = 'Warning: This admission page cannot be deleted as it is currently assigned as the store checkout terms!';
$_['error_affiliate']   = 'Warning: This admission page cannot be deleted as it is currently assigned as the store affiliate terms!';
$_['error_store']       = 'Warning: This admission page cannot be deleted as its currently used by %s stores!';
?>