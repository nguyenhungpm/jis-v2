<?php
// Heading
$_['heading_title']      = 'Photo Galleries';

// Text
$_['text_success']       = 'Success: You have modified gallery images!';
$_['text_default']       = 'Default';
$_['text_image_manager'] = 'Image Manager';
$_['text_browse']        = 'Browse';
$_['text_clear']         = 'Clear';

// Column
$_['column_name']        = 'Album Name';
$_['column_status']      = 'Status';
$_['column_sort_order']  = 'Sort Order';
$_['column_action']      = 'Action';

// Entry
$_['entry_name']         = 'Album Name';
$_['entry_title']        = 'Title';
$_['entry_description']  = 'Description';
$_['entry_link']         = 'Link';
$_['heading_title_add']  = 'Add new album';
$_['entry_sort_order']	 = 'Sort Order';
$_['entry_image']        = 'Image';
$_['entry_status']       = 'Status';
$_['button_add_picture'] = 'Add Image';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify gallery images!';
$_['error_name']         = 'Gallery Name must be between 3 and 64 characters!';
$_['error_title']        = 'Gallery Title must be between 2 and 64 characters!';
$_['error_default']      = 'Warning: This layout cannot be deleted as it is currently assigned as the default store layout!';
$_['error_product']      = 'Warning: This layout cannot be deleted as it is currently assigned to %s products!';
$_['error_category']     = 'Warning: This layout cannot be deleted as it is currently assigned to %s categories!';
$_['error_information']  = 'Warning: This layout cannot be deleted as it is currently assigned to %s information pages!';
?>