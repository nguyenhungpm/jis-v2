<?php echo $header; ?>
<div class="content-wrapper" id="content">
  <section class="content-header">
    <h1><?php echo $heading_title; ?><small>(Chi tiết)</small></h1>
    <ol class="breadcrumb">
      <?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
        <li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
      <?php $k++; } ?>
    </ol>
  </section>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="content">
  <div class="box box-default">
    <div class="box-header">
        <div class="pull-right">
          <a class="btn btn-default" href="<?php echo $cancel; ?>"><i class="fa fa-reply"></i> <?php echo $button_cancel; ?></a>
          <a class="btn btn-primary" onclick="$('#form').submit();"><i class="fa fa-check" aria-hidden="true"></i> <?php echo $button_save; ?></a>
        </div>
      </div>
    <div class="box-body">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="form">
          <tr>
            <td><span class="required">*</span> <?php echo $entry_name; ?></td>
            <td><input type="text" name="name" value="<?php echo $name; ?>" size="100" />
              <?php if ($error_name) { ?>
              <span class="error"><?php echo $error_name; ?></span>
              <?php } ?></td>
          </tr>
          <tr>
            <td><?php echo $entry_status; ?></td>
            <td><select name="status">
                <?php if ($status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
          </tr>
        </table>
        <table id="images" class="list">
          <thead>
            <tr>
              <td class="left"><?php echo $entry_title; ?></td>
              <td class="left">Mô tả</td>
              <td class="left"><?php echo $entry_link; ?></td>
              <td class="left"><?php echo $entry_image; ?></td>
              <td></td>
            </tr>
          </thead>
          <?php $image_row = 0; ?>
          <?php foreach ($video_images as $video_image) { ?>
          <tbody id="image-row<?php echo $image_row; ?>">
            <tr>
			<td class="left"><?php foreach ($languages as $language) { ?>
                <input type="text" name="video_image[<?php echo $image_row; ?>][video_image_description][<?php echo $language['language_id']; ?>][title]" value="<?php echo isset($video_image['video_image_description'][$language['language_id']]['title']) ? $video_image['video_image_description'][$language['language_id']]['title'] : ''; ?>" />
                <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />
                <?php if (isset($error_video_image[$image_row][$language['language_id']])) { ?>
                <span class="error"><?php echo $error_video_image[$image_row][$language['language_id']]; ?></span>
                <?php } ?>
                <?php } ?></td>
			<td class="left"><?php foreach ($languages as $language) { ?>
                <textarea type="text" name="video_image[<?php echo $image_row; ?>][video_image_description][<?php echo $language['language_id']; ?>][description]"><?php echo isset($video_image['video_image_description'][$language['language_id']]['description']) ? $video_image['video_image_description'][$language['language_id']]['description'] : ''; ?></textarea>
                <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />
                <?php if (isset($error_video_image[$image_row][$language['language_id']])) { ?>
                <span class="error"><?php echo $error_video_image[$image_row][$language['language_id']]; ?></span>
                <?php } ?>
                <?php } ?></td>
           <?php $link_row = 0; ?>
           <td class="left"><?php foreach ($languages as $language) { ?>
            <input type="text" name="video_image[<?php echo $image_row; ?>][video_image_description][<?php echo $language['language_id']; ?>][link]" value="<?php echo (isset($video_image['video_image_description'][$language['language_id']]['link'])) ? $video_image['video_image_description'][$language['language_id']]['link'] : ''; ?>" />
            <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />
            <?php $link_row++; ?>
           <?php } ?>
           </td>
              <td class="left">
           <?php $language_row = 0; ?>
           <?php foreach ($languages as $language) { ?>
           <div class="image"><img src="<?php echo $video_image['video_image_description'][$language['language_id']]['thumb']; ?>" alt="" id="thumb<?php echo $image_row; ?><?php echo $language_row; ?>" />
                  <input type="hidden" name="video_image[<?php echo $image_row; ?>][video_image_description][<?php echo $language['language_id']; ?>][image]" value="<?php echo (isset($video_image['video_image_description'][$language['language_id']]['image'])) ? $video_image['video_image_description'][$language['language_id']]['image'] : ''; ?>" id="image<?php echo $image_row; ?><?php echo $language_row; ?>"  />
                  <br />
                  <a onclick="image_upload('image<?php echo $image_row; ?><?php echo $language_row; ?>', 'thumb<?php echo $image_row; ?><?php echo $language_row; ?>');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#thumb<?php echo $image_row; ?><?php echo $language_row; ?>').attr('src', '<?php echo $no_image; ?>'); $('#image<?php echo $image_row; ?><?php echo $language_row; ?>').attr('value', '');"><?php echo $text_clear; ?></a>
              <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />
           </div>
           <?php $language_row++; ?>
           <?php } ?>     
              </td>
              <td class="left"><a onclick="$('#image-row<?php echo $image_row; ?>').remove();" class="btn btn-primary"><?php echo $button_remove; ?></a></td>
            </tr>
          </tbody>
          <?php $image_row++; ?>
          <?php } ?>
          <tfoot>
            <tr>
              <td colspan="4"></td>
              <td class="left"><a onclick="addImage();" class="btn btn-primary">Thêm video</a></td>
            </tr>
          </tfoot>
        </table>
      </form>
    </div>
  </div>
  </div>
</div>
<script type="text/javascript"><!--
var image_row = <?php echo $image_row; ?>;

function addImage() {
    html  = '<tbody id="image-row' + image_row + '">';
	html += '<tr>';
    html += '<td class="left">';
	<?php foreach ($languages as $language) { ?>
	html += '<input type="text" name="video_image[' + image_row + '][video_image_description][<?php echo $language['language_id']; ?>][title]" value="" /> <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />';
    <?php } ?>
	html += '</td>';
    html += '<td class="left">';
	<?php foreach ($languages as $language) { ?>
	html += '<textarea name="video_image[' + image_row + '][video_image_description][<?php echo $language['language_id']; ?>][description]"></textarea> <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />';
    <?php } ?>
	html += '</td>';
    html += '<td class="left">';		
	<?php foreach ($languages as $language) { ?>
	html += '<input type="text" name="video_image[' + image_row + '][video_image_description][<?php echo $language['language_id']; ?>][link]" value="" /><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />';		
    <?php } ?>
	html += '</td>';
    html += '<td class="left">';	
    <?php $language_row = 0; ?>	
	<?php foreach ($languages as $language) { ?>
	html += '<div class="image"><img src="<?php echo $no_image; ?>" alt="" id="thumb' + image_row + '<?php echo $language_row; ?>" />';
	html += '<input type="hidden" name="video_image[' + image_row + '][video_image_description][<?php echo $language['language_id']; ?>][image]" value="" id="image' + image_row + '<?php echo $language_row; ?>" /><br />';
	html += '<a onclick="image_upload(\'image' + image_row + '<?php echo $language_row; ?>\', \'thumb' + image_row + '<?php echo $language_row; ?>\');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$(\'#thumb' + image_row + '<?php echo $language_row; ?>\').attr(\'src\', \'<?php echo $no_image; ?>\'); $(\'#image' + image_row + '<?php echo $language_row; ?>\').attr(\'value\', \'\');"><?php echo $text_clear; ?></a>';
	html += '<img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br /></div>';
	<?php $language_row++; ?>
    <?php } ?>
	html += '</td>';	
	html += '<td class="left"><a onclick="$(\'#image-row' + image_row  + '\').remove();" class="btn btn-primary"><?php echo $button_remove; ?></a></td>';
	html += '</tr>';
	html += '</tbody>'; 
	
	$('#images tfoot').before(html);
	
	image_row++;
}
//--></script>
<script type="text/javascript"><!--
function image_upload(field, thumb) {
	$('#dialog').remove();
	
	$('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');
	
	$('#dialog').dialog({
		title: '<?php echo $text_image_manager; ?>',
		close: function (event, ui) {
			if ($('#' + field).attr('value')) {
				$.ajax({
					url: 'index.php?route=common/filemanager/image&token=<?php echo $token; ?>&image=' + encodeURIComponent($('#' + field).attr('value')),
					dataType: 'text',
					success: function(data) {
						$('#' + thumb).replaceWith('<img src="' + data + '" alt="" id="' + thumb + '" />');
					}
				});
			}
		},	
		bgiframe: false,
		width: 700,
		height: 400,
		resizable: false,
		modal: false
	});
};
//--></script> 
<?php echo $footer; ?>