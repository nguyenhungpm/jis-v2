<?php echo $header; ?>
<div id="content" class="content-wrapper">
	<section class="content-header">
		<h1><?php echo $heading_title; ?><small>(Trang chi tiết)</small></h1>
		<ol class="breadcrumb">
			<?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
				<li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
			<?php $k++; } ?>
		</ol>
	</section>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<div class="content">
	<div id="fake"></div>
	<div class="box box-default" id="toolbar">
		<div class="box-header">
			<div class="pull-right">
				<a href="<?php echo $cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i> <?php echo $button_cancel; ?></a>
			</div>
		</div>
	</div>
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
		<div class="row">
			<div class="col-md-8">
				<div class="box box-danger">
					<div class="box-body">
						<div class="form-group required">
							<?php foreach ($products as $key=>$product) { ?>
						    <div class="row--question">
						    <h3 class="yellow"><?php echo $product['name']; ?></h3>
						    <?php if($product['thumb']){ ?>
						      <img class="right block" src="<?php echo $product['thumb']; ?>" />
						    <?php } ?>
						    <div class="question">
						        <?php foreach ($product['questions'] as $question) { ?>
						          <div class="inputGroup">
						            <input class="currentid" type="hidden" value="<?php echo $key; ?>" />
						            <input <?php echo $question['dungsai']==1 ? 'checked':'';?> type="radio" name="quest[<?php echo $question['product_id']; ?>]" id="quest<?php echo $question['question_id']; ?>" value="<?php echo $question['question']; ?>" />
						            <label class="<?php echo ($product['answer'] != 0 && $product['answer']==$question['question'] && $question['dungsai']==1) ? 'success':''; ?>" for="quest<?php echo $question['question_id']; ?>"><?php echo $question['question']; ?></label>
						          </div>
						        <?php } ?>
										<span>Đáp án user chọn: <b><?php echo $product['answer']; ?></b></span>
						    </div>
						    </div>
						  <?php } ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="box box-danger">
					<div class="box-body">
						<div class="form-group">
							<label class="control-label">Họ và tên</label>
							<input readonly type="text" name="name" value="<?php echo $answer_submit_info['name']; ?>" size="100" class="form-control"/>
						</div>
						<div class="form-group">
							<label class="control-label">Năm sinh</label>
							<input readonly type="text" name="number" value="<?php echo $answer_submit_info['number']; ?>" size="100" class="form-control"/>
						</div>
						<div class="form-group">
							<label class="control-label">Giới tính</label>
							<input readonly type="text" name="role" value="<?php echo $answer_submit_info['role']; ?>" size="100" class="form-control"/>
						</div>
						<div class="form-group">
							<label class="control-label">Tên trường lớp hiện tại</label>
							<input readonly type="text" name="company" value="<?php echo $answer_submit_info['company']; ?>" size="100" class="form-control"/>
						</div>
						<div class="form-group">
							<label class="control-label">Skype</label>
							<input readonly type="text" name="skype" value="<?php echo $answer_submit_info['skype']; ?>" size="100" class="form-control"/>
						</div>
						<div class="form-group">
							<label class="control-label">Facebook</label>
							<input readonly type="text" name="facebook" value="<?php echo $answer_submit_info['facebook']; ?>" size="100" class="form-control"/>
						</div>
						<div class="form-group">
							<label class="control-label">Địa chỉ</label>
							<input readonly type="text" name="address" value="<?php echo $answer_submit_info['address']; ?>" size="100" class="form-control"/>
						</div>
						<div class="form-group">
							<label class="control-label">Biết tới qua kênh?</label>
							<input readonly type="text" name="knowfrom" value="<?php echo $answer_submit_info['knowfrom']; ?>" size="100" class="form-control"/>
						</div>
						<div class="form-group">
							<label class="control-label">Ghi chú</label>
							<textarea readonly type="text" name="note" class="form-control" rows="5"><?php echo $answer_submit_info['note']; ?></textarea>
						</div>
            <h4>Thông tin phụ huynh (học sinh dưới 17 tuổi)</h4>
						<div class="form-group">
							<label class="control-label">Tên phụ huynh</label>
							<input readonly type="text" name="parent" value="<?php echo $answer_submit_info['parent']; ?>" size="100" class="form-control"/>
						</div>
						<div class="form-group">
							<label class="control-label">Điện thoại</label>
							<input readonly type="text" name="phone" value="<?php echo $answer_submit_info['phone']; ?>" size="100" class="form-control"/>
						</div>
						<div class="form-group">
							<label class="control-label">Email</label>
							<input readonly type="text" name="email" value="<?php echo $answer_submit_info['email']; ?>" size="100" class="form-control"/>
						</div>
					</div>
				</div>
			</div>
		</div>
    </form>
</div>
</div>
<?php echo $footer; ?>
