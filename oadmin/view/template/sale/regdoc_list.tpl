<?php echo $header; ?>
<div class="content-wrapper">
	<section class="content-header">
		<h1><?php echo $heading_title; ?><small>(Danh sách)</small></h1>
		<ol class="breadcrumb">
			<?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
				<li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
			<?php $k++; } ?>
		</ol>
	</section>
	<?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
    <?php if ($success) { ?>
        <div class="success"><?php echo $success; ?></div>
    <?php } ?>
	<div class="content">
		<div id="fake"></div>
		<div class="box box-default">
		<div class="box-header" id="toolbar">
				<span class="help pull-left">Click vào 'Chưa xem' tại dòng tương ứng để đánh dấu đã xử lý xong</span>
				<div class="pull-right">
					<a onclick="$('form').submit();" class="btn btn-default btn-sm"><i class="fa fa-trash" aria-hidden="true"></i> <?php echo $button_delete; ?></a>
				</div>
		</div>
		<div class="box-body">
            <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                <table class="table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <td width="1" style="text-align: center;">
								<input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" />
							</td>
                            <td class="text-left">Thông tin cá nhân</td>
														<td class="text-left">Chức danh</td>
                            <td class="left">Đơn vị</td>
                            <td class="left">Tên tài liệu</td>
                            <td class="right">Trạng thái</td>
                            <td class="left">Ngày đăng ký</td>
                        </tr>
                    </thead>
                    <tbody>
					<?php if ($docregs) { ?>
						<?php foreach ($docregs as $regdoc) { ?>
							<?php if($regdoc['status']==0) {?>
								<tr style="background:lightyellow;font-weight:bold;">
							<?php }else{?>
								<tr>
							<?php } ?>
								<td><input type="checkbox" name="selected[]" value="<?php echo $regdoc['docreg_id']; ?>" /></td>
								<td>
									<?php echo $regdoc['name']; ?><br />
									<?php echo $regdoc['email']; ?> | <?php echo $regdoc['phone']; ?>
								</td>
								<td><?php echo $regdoc['title']; ?></td>
								<td><?php echo $regdoc['office']; ?></td>
								<td><?php echo $regdoc['source']; ?></td>
								<td><a class="columnstatus" id="status-<?php echo $regdoc['docreg_id']; ?>"><?php echo $regdoc['status']==1 ? $text_enabled : $text_disabled ; ?></a></td>
								<td><?php echo date('d-m-Y h:i',strtotime($regdoc['date_added'])); ?></td>
							</tr>
						<?php } ?>
					<?php } else { ?>
						<tr>
							<td class="center" colspan="7"><?php echo $text_no_results; ?></td>
						</tr>
					<?php } ?>
                    </tbody>
                </table>
            </form>
            <div class="pagination"><?php echo $pagination; ?></div>
        </div>
        </div>
	</div>
</div>

<script type="text/javascript"><!--
$('.columnstatus').click(function() {
	var object_id=$(this).attr('id');
	$.ajax({
		url: 'index.php?route=sale/regdoc/updatestatus&token=<?php echo $token; ?>',
		type: 'get',
		data: {object_id:object_id},
		dataType: 'html',
		success: function(html) {
			if(html!=''){
				$('#'+object_id).html(html);
			}
		}
	});
});
//--></script>
<?php echo $footer; ?>
