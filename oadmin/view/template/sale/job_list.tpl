<?php echo $header; ?>
<div class="content-wrapper">
	<section class="content-header">
		<h1><?php echo $heading_title; ?><small>(Danh sách)</small></h1>
		<ol class="breadcrumb">
			<?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
				<li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
			<?php $k++; } ?>
		</ol>
	</section>
	<?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
    <?php if ($success) { ?>
        <div class="success"><?php echo $success; ?></div>
    <?php } ?>
	<div class="content">
		<div id="fake"></div>
		<div class="box box-default">
		<div class="box-header" id="toolbar">
				<div class="pull-right">
					<a onclick="$('form').submit();" class="btn btn-default btn-sm"><i class="fa fa-trash" aria-hidden="true"></i> <?php echo $button_delete; ?></a>
				</div>
		</div>
		<div class="box-body">
            <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                <table class="table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <td width="1" style="text-align: center;">
								<input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" />
							</td>
                            <td class="text-left">Ứng viên</td>
                            <td class="text-left">Email</td>
                            <td class="left">Điện thoại</td>
                            <td class="text-left">Giới thiệu</td>
                            <td class="left">CV</td>
                            <td class="left">Vị trí</td>
                            <td class="right">Trạng thái</td>
                            <td class="text-left">Ngày gửi</td>
                        </tr>
                    </thead>
                    <tbody>
					<?php if ($jobs) { ?>
						<?php foreach ($jobs as $job) { ?>
							<?php if($job['status']==0) {?>
								<tr style="background:lightyellow;font-weight:bold;">
							<?php }else{?>
								<tr>
							<?php } ?>
								<td><input type="checkbox" name="selected[]" value="<?php echo $job['job_id']; ?>" /></td>
								<td><?php echo $job['job_name']; ?></td>
								<td><?php echo $job['job_email']; ?></td>
								<td><?php echo $job['job_phone']; ?></td>
								<td><?php echo $job['job_intro']; ?></td>
								<td>
									<?php if($job['filename']){ ?>
									<a title="<?php echo $job['file']; ?>" href="../down.php?filename=<?php echo $job['filename']; ?>&realfilename=<?php echo $job['file']; ?>">Tải về</a>
									<?php } ?>
								</td>
								<td><?php echo $job['position']; ?></td>
								<td><a class="columnstatus" id="status-<?php echo $job['job_id']; ?>"><?php echo $job['status']==1 ? $text_enabled : $text_disabled ; ?></a></td>
								<td><?php echo date('d-m-Y h:i',strtotime($job['date_added'])); ?></td>
							</tr>
						<?php } ?>
					<?php } else { ?>
						<tr>
							<td class="center" colspan="8"><?php echo $text_no_results; ?></td>
						</tr>
					<?php } ?>
                    </tbody>
                </table>
            </form>
            <div class="pagination"><?php echo $pagination; ?></div>
        </div>
        </div>
	</div>
</div>

<script type="text/javascript"><!--
$('.columnstatus').click(function() {
	var object_id=$(this).attr('id');
	$.ajax({
		url: 'index.php?route=sale/job/updatestatus&token=<?php echo $token; ?>',
		type: 'get',
		data: {object_id:object_id},
		dataType: 'html',
		success: function(html) {
			if(html!=''){
				$('#'+object_id).html(html);
			}
		}
	});
});
//--></script>
<?php echo $footer; ?>
