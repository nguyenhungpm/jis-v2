<?php echo $header; ?>
<div class="content-wrapper">
	<section class="content-header">
		<h1><?php echo $heading_title; ?><small>(Danh sách)</small></h1>
		<ol class="breadcrumb">
			<?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
				<li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
			<?php $k++; } ?>
		</ol>
	</section>
	<?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
    <?php if ($success) { ?>
        <div class="success"><?php echo $success; ?></div>
    <?php } ?>
	<div class="content">
		<div id="fake"></div>
		<div class="box box-default">
		<div class="box-header" id="toolbar">
				<div class="pull-right">
					<a onclick="$('form').submit();" class="btn btn-default btn-sm"><i class="fa fa-trash" aria-hidden="true"></i> <?php echo $button_delete; ?></a>
				</div>
		</div>
		<div class="box-body">
            <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                <table class="table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <td width="1" style="text-align: center;">
															<input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" />
														</td>
														<td class="text-left">Người học</td>
														<td class="text-left">Phụ huynh</td>
                            <td class="text-left">Lời nhắn</td>
                            <td class="left">Biết qua kênh</td>
                            <td class="right">Trạng thái</td>
                            <td class="left">Ngày gửi</td>
                        </tr>
                    </thead>
                    <tbody>
					<?php if ($answer_submits) { ?>
						<?php foreach ($answer_submits as $answer_submit) { ?>
							<?php if($answer_submit['status']==0) {?>
								<tr style="background:lightyellow;font-weight:bold;">
							<?php }else{?>
								<tr>
							<?php } ?>
								<td><input type="checkbox" name="selected[]" value="<?php echo $answer_submit['answer_submit_id']; ?>" /></td>
								<td>
									<a href="<?php echo HTTPS_SERVER . 'index.php?route=sale/answer_submit/update&answer_submit_id='. $answer_submit['answer_submit_id'] .'&token=' . $this->session->data['token']; ?>">
										<?php echo $answer_submit['name']; ?> (<?php echo $answer_submit['role']; ?>)<br />
										Ngày sinh: <?php echo $answer_submit['number']; ?><br />
										Trường: <?php echo $answer_submit['company']; ?><br />
										Facebook: <?php echo $answer_submit['facebook']; ?><br />
										Skype: <?php echo $answer_submit['skype']; ?><br />
										Địa chỉ: <?php echo $answer_submit['address']; ?>
									</a>
								</td>
								<td>
									Tên phụ huynh: <?php echo $answer_submit['parent']; ?><br />
									Email: <?php echo $answer_submit['email']; ?><br />
									Điện thoại: <?php echo $answer_submit['phone']; ?>

								</td>
								<td><?php echo $answer_submit['note']; ?></td>
								<td><?php echo $answer_submit['knowfrom']; ?></td>
								<td><a class="columnstatus" id="status-<?php echo $answer_submit['answer_submit_id']; ?>"><?php echo $answer_submit['status']==1 ? $text_enabled : $text_disabled ; ?></a></td>
								<td><?php echo date('d-m-Y h:i',strtotime($answer_submit['date_added'])); ?></td>
							</tr>
						<?php } ?>
					<?php } else { ?>
						<tr>
							<td class="center" colspan="8"><?php echo $text_no_results; ?></td>
						</tr>
					<?php } ?>
                    </tbody>
                </table>
            </form>
            <div class="pagination"><?php echo $pagination; ?></div>
        </div>
        </div>
	</div>
</div>

<script type="text/javascript"><!--
$('.columnstatus').click(function() {
	var object_id=$(this).attr('id');
	$.ajax({
		url: 'index.php?route=sale/answer_submit/updatestatus&token=<?php echo $token; ?>',
		type: 'get',
		data: {object_id:object_id},
		dataType: 'html',
		success: function(html) {
			if(html!=''){
				$('#'+object_id).html(html);
			}
		}
	});
});
//--></script>
<?php echo $footer; ?>
