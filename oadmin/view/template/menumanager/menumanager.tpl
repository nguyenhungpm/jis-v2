<?php echo $header; ?>
<script>
	var _BASE_URL = '<?php echo  html_entity_decode($this->url->link('extension/menumanager', 'token=' . $this->session->data['token'], 'SSL')); ?>';
	var current_group_id = <?php echo $group_id; ?>;
</script>
<link rel="stylesheet" href="view/stylesheet/menumanager.css">
<script type="text/javascript" src="view/javascript/menumanager/jquery.1.4.1.min.js"></script>
<script type="text/javascript" src="view/javascript/menumanager/interface-1.2.js"></script>
<script type="text/javascript" src="view/javascript/menumanager/inestedsortable.js"></script>
<script type="text/javascript" src="view/javascript/menumanager/menu.js"></script>

 <style>
    .ui-dialog .ui-state-error { padding: .3em; }
    .validateTips { border: 1px solid transparent; padding: 0.3em; }
</style>

<script>
	$(document).ready(function(){
		$('#add-informations').click(function(){
			$( "#dialog-informations" ).dialog({
	            modal: true,
	            buttons: {
	                Ok: function() {
	                	$.ajax({
	                		type:"POST",
	                		data: $("#form-information").serialize(),
	                		url : "<?php echo  html_entity_decode($this->url->link('extension/menumanager/quick_add', 'token=' . $this->session->data['token'], 'SSL')); ?>",
	                		success: function(data){
	                			$("#dialog-information").dialog( "close" );
	                			location.reload();
	                		}
	                	});
	                }
	            }
	        });
		});
		$('#add-categories').click(function(){
			 $( "#dialog-category" ).dialog({
	            modal: true,
	            buttons: {
	                OK: function() {
	                	$.ajax({
	                		type:"POST",
	                		data: $("#form-category").serialize(),
	                		url : "<?php echo  html_entity_decode($this->url->link('extension/menumanager/quick_add', 'token=' . $this->session->data['token'], 'SSL')); ?>",
	                		success: function(data){
	                			$("#dialog-category").dialog( "close" );
	                			location.reload();
	                		}
	                	});

	                }
	            }
	        });
		});
		$('#add-news_categories').click(function(){
			 $( "#dialog-news_category" ).dialog({
	            modal: true,
	            buttons: {
	                OK: function() {
	                	$.ajax({
	                		type:"POST",
	                		data: $("#form-news_category").serialize(),
	                		url : "<?php echo  html_entity_decode($this->url->link('extension/menumanager/quick_add', 'token=' . $this->session->data['token'], 'SSL')); ?>",
	                		success: function(data){
	                			$("#dialog-news_category").dialog( "close" );
	                			location.reload();
	                		}
	                	});

	                }
	            }
	        });
		});

	});
</script>
<div class="content-wrapper" id="content">
	<section class="content-header">
		<h1><?php echo $heading_title; ?></h1>
		<ol class="breadcrumb">
			<?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
				<li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
			<?php $k++; } ?>
		</ol>
	</section>
	<?php if ($error_warning) { ?>
	<div class="warning"><?php echo $error_warning; ?></div>
	<?php } ?>

	<div class="content">
		<div class="box box-default">
			<div class="box-header">
				<div class="pull-right">
				<p>- Kéo, thả danh sách menu để sắp xếp lại, sau đó click <b>Cập nhật menu</b> để lưu lại vị trí thứ tự của menu.</p>
				<p>- Sử dụng các form bên dưới để thêm menu mới.</p>
				</div>
			</div>
		</div>
    <div class="row">
    	<aside class="col-md-3">
			<div class="box box-danger">
				<div class="mbox">
					<h2>Nhóm hiện tại</h2>
				</div>
				<div class="box-body" id="edit-group">
					<?php foreach($languages as $language){ ?>
					<span><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> Nhóm menu <?php echo $language['name'];?></span>
					<input class="form-control" readonly type="text" value="<?php echo isset($current_menu[$language['language_id']]['title']) ? $current_menu[$language['language_id']]['title'] : ''; ?>" name="title_group_<?php echo $language['language_id'];?>" id="menu-group-title_edit_<?php echo $language['language_id'];?>"><br />
					<?php } ?>
					<span id="edit-group-input"><?php echo $group_title; ?></span>
					<input type="hidden" name="title_en" value="<?php echo $group_title_en; ?>"/>
					(ID: <b><?php echo $group_id; ?></b>)
					<div>
						<a id="change-group" data-language_id="<?php echo $code_lang; ?>">Sửa</a>
						<a style="display:none" id="save-group" data-language_id="<?php echo $code_lang; ?>" data-group_id="<?php echo $group_id; ?>">Lưu</a>
						<?php if ($group_id > 1) : ?>
						&middot; <a id="delete-group" href="#">Xóa</a>
						<?php endif; ?>
					</div>
				</div>
			</div>

			<div class="box box-danger">
				<div class="mbox">
					<h2>Thêm nhanh menu từ các danh mục</h2>
				</div>
				<div class="box-body">
					<form id="form-quick-add-menu" method="post" action="<?php echo  html_entity_decode($this->url->link('extension/menumanager/qaddmenu', '&token=' . $this->session->data['token'], 'SSL')); ?>">
						<p class="buttons">
							<input type="hidden" id="group_id" name="group_id" value="<?php echo $group_id; ?>">
							<button id="add-news_categories" type="button" class="button green small">Thêm nhóm tin tức</button>
							<button id="add-categories" type="button" class="button green small">Thêm nhóm sản phẩm</button>
							<button id="add-informations" type="button" class="button green small">Bài viết giới thiệu</button>
						</p>
					</form>
				</div>
			</div>

			<div class="box box-danger">
				<div class="mbox">
					<h2>Thêm menu tự do</h2>
				</div>
				<div class="box-body">
					<form id="form-add-menu" method="post" action="<?php echo  html_entity_decode($this->url->link('extension/menumanager/addmenu', '&token=' . $this->session->data['token'], 'SSL')); ?>">
						<p class="hide">
							<label for="menu-title">Tiêu đề</label>
							<input class="form-control" type="text" name="title" id="menu-title">
						</p>
						<?php foreach($languages as $language){ ?>
						<p>
							<label for="menu-name<?php echo $language['language_id'];?>"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> Tiêu đề <?php echo $language['name'];?></label>
							<input class="form-control" type="text" name="name[<?php echo $language['language_id'];?>]" id="menu-name<?php echo $language['language_id'];?>">
						</p>
						<?php } ?>
						<p>
							<label for="menu-url">URL</label>
							<input class="form-control" type="text" name="url" id="menu-url" value="" class="ui-autocomplete-input" autocomplete="off">
						</p>
						<p>
							<label for="menu-class">Class</label>
							<input class="form-control" type="text" name="class" id="menu-class">
						</p>
						<p class="buttons">
							<input type="hidden" id="group_id" name="group_id" value="<?php echo $group_id; ?>">
							<button id="add-menu" type="submit" class="button green small">Thêm vào menu</button>
						</p>
					</form>
				</div>
			</div>
			</aside>
      	<div id="main" class="col-md-9">
			<div class="box box-danger">
				<div class="box-header">
				<ul id="menu-group">
					<?php if(!empty($menu_groups)):?>
					<?php foreach ((array)$menu_groups as $group) : ?>
					<li id="group-<?php echo $group['id']; ?>">
						<a href="<?php echo  html_entity_decode($this->url->link('extension/menumanager', 'group_id='.$group['id'].'&token=' . $this->session->data['token'], 'SSL')); ?>">
							<?php echo $group['title']; ?>
						</a>
					</li>
					<?php endforeach; ?>
					<?php endif;?>
					<li id="add-group"><a data-language_id="<?php echo $code_lang; ?>" href="<?php echo  $this->url->link('extension/menumanager/addgroup', 'token=' . $this->session->data['token'], 'SSL'); ?>" title="Add Group">+</a></li>
				</ul>
				</div>

				<div class="box-body">
				<form method="post" id="form-menu" action="<?php echo  html_entity_decode($this->url->link('extension/menumanager/updatemenupos', '&token=' . $this->session->data['token'], 'SSL')); ?>">
					<div class="ns-row" id="ns-header">
						<div class="ns-actions">Actions</div>
						<div class="ns-sort">Thứ tự</div>
						<!--div class="ns-url">URL</div-->
						<div class="ns-blank">_blank</div>
						<div class="ns-nofollow">nofollow</div>
						<div class="ns-title">Tên</div>
					</div>
					<?php echo $menu_ul; ?>
					<div id="ns-footer">
						<button type="submit" class="button green small" id="btn-save-menu">Cập nhật menu</button>
					</div>
				</form>
				</div>
			</div>
		</div>
    </div>
    <div id="dialog-news_category" title="Thêm nhóm tin tức" style="display:none;" >
	    <form style="height:300px;" id="form-news_category">
	    	<input type="hidden" name="menu_group_id" value="<?php echo $group_id; ?>">
		    <fieldset>
		    	<?php foreach($news_categories as $cat): ?>
		    	<div><input type="checkbox" name="news_category[]" value="<?php echo $cat['news_category_id']; ?>"><label><?php echo $cat['name']; ?></label></div>
		    	<?php endforeach; ?>
		    </fieldset>
	    </form>
	</div>
    <div id="dialog-category" title="Thêm nhóm sản phẩm" style="display:none;" >
	    <form style="height:300px;" id="form-category">
	    	<input type="hidden" name="menu_group_id" value="<?php echo $group_id; ?>">
		    <fieldset>
		    	<?php foreach($categories as $cat): ?>
		    	<div><input type="checkbox" name="category[]" value="<?php echo $cat['category_id']; ?>"><label><?php echo $cat['name']; ?></label></div>
		    	<?php endforeach; ?>
		    </fieldset>
	    </form>
	</div>
	<div id="dialog-informations" title="Thêm trang thông tin" style="display:none;" >
	    <p class="validateTips">Một số trang đặc biệt.</p>
	    <form style="height:300px;" id="form-information">
	    	<input type="hidden" name="menu_group_id" value="<?php echo $group_id; ?>">
		    <fieldset>

		    	<div><input type="checkbox" name="page_route[]" value="information/contact"><label>Contact Us</label></div>
		    	<div><input type="checkbox" name="page_route[]" value="information/sitemap"><label>Site Map</label></div>
		    	<div><input type="checkbox" name="page_route[]" value="product/manufacturer"><label>Brands</label></div>
		   	</fieldset>
		  <p class="validateTips">Các trang thông tin.</p>
		    <fieldset>
		    	<?php foreach($informations as $info): ?>
			    	<div><input type="checkbox" name="information[]" value="<?php echo $info['information_id']; ?>"><label><?php echo $info['title']; ?></label></div>
			    <?php endforeach; ?>
		    </fieldset>
	    </form>
	</div>
  </div>
</div>
<?php echo $footer; ?>
