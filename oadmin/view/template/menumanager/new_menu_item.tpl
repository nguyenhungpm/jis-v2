<h2>Edit Menu Item</h2>
<form method="post" action="<?php echo $action; ?>">
	<p class="hide">
		<label for="edit-menu-title">Title</label>
		<input class="form-control" type="text" name="title" id="edit-menu-title" value="<?php echo $row['title']; ?>">
	</p>
	<?php foreach($languages as $language){$language_id = $language['language_id']; ?>
	<p>
		<label for="menu-name<?php echo $language['language_id'];?>"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> Tiêu đề <?php echo $language['name'];?></label>
		<input class="form-control" type="text" value="<?php echo !empty($row['name'][$language_id]) ? $row['name'][$language_id] : '';?>" name="name[<?php echo $language['language_id'];?>]" id="menu-name<?php echo $language['language_id'];?>">
	</p>
	<?php } ?>
	<p>
		<label for="edit-menu-url">URL</label>
		<input class="form-control" type="text" name="url" id="edit-menu-url" value="<?php echo $row['url']; ?>">
	</p>
	<p>
		<label for="edit-menu-class">Class</label>
		<input class="form-control" type="text" name="class" id="edit-menu-class" value="<?php  echo $row['class']; ?>">
	</p>
	<?php  if ($row['parent_id'] == 0) : //only top level menu can be moved ?>
	<p>
		<label for="select_group_id">Group</label>
		<?php //echo selectList('group_id', $menu_groups, $row['group_id']); ?>
	</p>
	<input type="hidden" name="old_group_id" value="<?php echo $row['group_id']; ?>">
	<?php endif; ?>
	<input type="hidden" name="menu_id" value="<?php echo $row['id']; ?>">
</form>
