<h2>Thêm Nhóm</h2>
<form method="post" id="forn_menu_group" action="<?php echo html_entity_decode($this->url->link('extension/menumanager/addgroup', 'token=' . $this->session->data['token'], 'SSL')); ?>">
	<?php foreach($languages as $language){ ?>
	<div class="form-group">
		<label for="menu-group-title" class="label-control"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> Nhóm menu <?php echo $language['name'];?></label>
		<input class="form-control" type="text" name="title_group[<?php echo $language['language_id'];?>]" id="menu-group-title_<?php echo $language['language_id'];?>">
	</div>
	<?php } ?>
</form>