<?php echo $header; ?>
<style type="text/css">
  #timdaily_module .row{padding:10px 0;}
  #timdaily_module .row:hover{background:#eee}
</style>
<div class="content-wrapper" id="content">
  <section class="content-header">
    <h1><?php echo $heading_title; ?><small>(<?php echo $text_configuration; ?>)</small></h1>
    <ol class="breadcrumb">
      <?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
        <li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
      <?php $k++; } ?>
    </ol>
  </section>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="content">
    <div class="box box-default">
    <div class="box-header">
         <div class="pull-right">
             <a onclick="location = '<?php echo $cancel; ?>';" class="btn btn-default"><i class="fa fa-reply"></i> <?php echo $button_cancel; ?></a>
               <a onclick="$('#form').submit();" class="btn btn-primary"><i class="fa fa-check" aria-hidden="true"></i> <?php echo $button_save; ?></a>
         </div>
    </div>
    <div class="box-body" id="timdaily_module">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <div class="row">
            <div class="col-sm-2"><strong>Ảnh đại diện</strong></div>
            <div class="col-sm-2">
                <div class="form-group text-center">
                <div class="image">
                    <a onclick="image_upload('image', 'thumb');">
                      <img src="<?php echo $timdaily_thumb; ?>" alt="" id="thumb" />
                    </a>
                    <a class="pull-left"  onclick="image_upload('image', 'thumb');">Chọn</a>
                    <a class="pull-right" onclick="$('#thumb').attr('src', '<?php echo $no_image; ?>'); $('#image').attr('value', '');">Xóa</a>
                    <input type="hidden" name="timdaily_image" value="<?php echo $timdaily_image; ?>" id="image" />
                </div>
                </div>
            </div>
        </div>
        <?php $module_row = 1; ?>
          <?php foreach ($modules as $module) { ?>
          <table class="list">
            <tbody id="module-row<?php echo $module_row; ?>">
            <tr>
              <td><?php echo $entry_layout; ?></td>
              <td><select name="timdaily_module[<?php echo $module_row; ?>][layout_id]">
                  <?php foreach ($layouts as $layout) { ?>
                  <?php if ($layout['layout_id'] == $module['layout_id']) { ?>
                  <option value="<?php echo $layout['layout_id']; ?>" selected="selected"><?php echo $layout['name']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select></td>
              <td><?php echo $entry_position; ?><select name="timdaily_module[<?php echo $module_row; ?>][position]">
                  <?php foreach($positions as $position){ ?>
          <option value="<?php echo $position['value'];?>" <?php echo $position['value']==$module['position'] ? 'selected':'';?>><?php echo $position['text'];?></option>
          <?php } ?>
                </select></td>
              <td><?php echo $entry_status; ?></td>
              <td><select name="timdaily_module[<?php echo $module_row; ?>][status]">
                  <?php if ($module['status']) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
              <td><?php echo $entry_sort_order; ?></td>
              <td><input type="text" name="timdaily_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $module['sort_order']; ?>" size="3" /></td>
              <td class="left"><a onclick="$('#module-row<?php echo $module_row; ?>').remove();" class="btn btn-sm btn-default" title="<?php echo $button_remove; ?>"><i class="fa fa-minus"></i></a></td>
            </tr>
          </tbody>
          </table>
          <?php $module_row++; ?>
          <?php } ?>
          <span onclick="addModule();" class="pull-right btn btn-primary btn-sm" id="module-add"><?php echo $button_add_module; ?>&nbsp;<i class="fa fa-plus"></i></span>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
<?php $module_row = 1; ?>
var module_row = <?php echo $module_row; ?>;
function addModule() {
  html += '<div id="tab-module-' + module_row + '" class="contenttab">';
  html += '  <table class="form">';
  html += '    <tr>';
  html += '      <td>layout <select name="timdaily_module[' + module_row + '][layout_id]">';
  <?php foreach ($layouts as $layout) { ?>
  html += '           <option value="<?php echo $layout['layout_id']; ?>"><?php echo addslashes($layout['name']); ?></option>';
  <?php } ?>
  html += '      </select></td>';
  html += '      <td><?php echo $entry_position; ?>';
  html += '      <select name="timdaily_module[' + module_row + '][position]">';
  <?php foreach($positions as $position){ ?>
  html += '      <option value="<?php echo $position['value'];?>"><?php echo $position['text'];?></option>';
  <?php } ?>
  html += '      </select></td>';
  html += '      <td><?php echo $entry_status; ?> ';
  html += '      <select name="timdaily_module[' + module_row + '][status]">';
  html += '        <option value="1"><?php echo $text_enabled; ?></option>';
  html += '        <option value="0"><?php echo $text_disabled; ?></option>';
  html += '      </select></td>';
  html += '      <td><?php echo $entry_sort_order; ?> ';
  html += '      <input type="text" name="timdaily_module[' + module_row + '][sort_order]" value="" size="3" /></td>';
  html += '    <td class="left"><a onclick="$(\'#module-row' + module_row + '\').remove();" class="btn btn-sm btn-default" title="<?php echo $button_remove; ?>"><i class="fa fa-minus"></i></a><?php echo $button_remove; ?></a></td>';
  html += '    </tr>';
  html += '  </table>';
  html += '</div>';

  $('#form').prepend(html);
  module_row++;
}
function image_upload(field, thumb) {

  $('#dialog').remove();

  $('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');

  $('#dialog').dialog({
    title: '<?php echo $text_image_manager; ?>',
    close: function (event, ui) {
      if ($('#' + field).attr('value')) {
        $.ajax({
          url: 'index.php?route=common/filemanager/image&token=<?php echo $token; ?>&image=' + encodeURIComponent($('#' + field).attr('value')),
          dataType: 'text',
          success: function(text) {
            $('#' + thumb).replaceWith('<img src="' + text + '" alt="" id="' + thumb + '" />');
          }
        });
      }
    },
    bgiframe: false,
    width: 1002,
    height: 600,
    resizable: false,
    modal: false
  });
};
//--></script>
<?php echo $footer; ?>
