<?php echo $header; ?>
<div class="content-wrapper">
  <section class="content-header">
		<h1><?php echo $heading_title; ?><small>(<?php echo $text_configuration; ?>)</small></h1>
		<ol class="breadcrumb">
			<?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
				<li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
			<?php $k++; } ?>
		</ol>
	</section>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<div class="content">
  <div class="box box-default">
  <div class="box-header">
    <div class="pull-right">
		<a onclick="location = '<?php echo $cancel; ?>';" class="btn btn-default"><i class="fa fa-reply"></i> <?php echo $button_cancel; ?></a>
		<a onclick="$('#form').submit();" class="btn btn-primary"><i class="fa fa-check" aria-hidden="true"></i> <?php echo $button_save; ?></a>
  </div>
  </div>
  <div class="box-body">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
      <table class="form">
        <tr>
          <td>Hashtag</td>
          <td><input type="text" class="form-control" name="video_hashtag" value="<?php echo $video_hashtag; ?>" /></td>
        </tr>
        <tr>
          <td>Tiêu đề</td>
          <td>
					<?php foreach ($languages as $key=> $language) { ?>
						<img <?php echo count($languages) ==1 ? 'class="hide"':''; ?> src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?>
						<input class="form-control" type="text" name="video_title[<?php echo $language['language_id']; ?>]" value="<?php echo $video_title[$language['language_id']];?>" />
					<?php } ?>
					</td>
        </tr>
        <tr>
          <td>Nội dung mô tả</td>
          <td>
					<?php foreach ($languages as $key=> $language) { ?>
						<img <?php echo count($languages) ==1 ? 'class="hide"':''; ?> src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?>
						<textarea rows="5" class="form-control" name="video_description[<?php echo $language['language_id']; ?>]"><?php echo $video_description[$language['language_id']];?></textarea>
					<?php } ?>
					</td>
        </tr>
        <tr>
          <td>Mã video nổi bật</td>
          <td><input type="text" class="form-control" name="video_featured" value="<?php echo $video_featured; ?>" /></td>
        </tr>
      </table>
			<table id="module" class="list">
        <thead>
          <tr>
            <td class="left"><?php echo $entry_limit; ?></td>
		    <td class="left"><?php echo $entry_limitdescription; ?></td>
            <td class="left"><?php echo $entry_image; ?></td>
            <td class="left"><?php echo $entry_layout; ?></td>
            <td class="left"><?php echo $entry_position; ?></td>
            <td class="left"><?php echo $entry_status; ?></td>
            <td class="right"><?php echo $entry_sort_order; ?></td>
            <td></td>
          </tr>
        </thead>
        <?php $module_row = 0; ?>
        <?php foreach ($modules as $module) { ?>
        <tbody id="module-row<?php echo $module_row; ?>">
          <tr>
            <td class="left"><input type="text" name="video_module[<?php echo $module_row; ?>][limit]" value="<?php echo $module['limit']; ?>" size="3" /></td>
            <td class="left"><input type="text" name="video_module[<?php echo $module_row; ?>][limitdescription]" value="<?php echo $module['limitdescription']; ?>" size="3" /></td>
            <td class="left"><input type="text" name="video_module[<?php echo $module_row; ?>][image_width]" value="<?php echo $module['image_width']; ?>" size="3" />
              <input type="text" name="video_module[<?php echo $module_row; ?>][image_height]" value="<?php echo $module['image_height']; ?>" size="3" />
              <?php if (isset($error_image[$module_row])) { ?>
              <span class="error"><?php echo $error_image[$module_row]; ?></span>
              <?php } ?></td>
                <td class="left"><select name="video_module[<?php echo $module_row; ?>][layout_id]">
                <?php foreach ($layouts as $layout) { ?>
                <?php if ($layout['layout_id'] == $module['layout_id']) { ?>
                <option value="<?php echo $layout['layout_id']; ?>" selected="selected"><?php echo $layout['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select></td>
            <td class="left"><select name="video_module[<?php echo $module_row; ?>][position]">
                <?php foreach($positions as $position){ ?>
					<option value="<?php echo $position['value'];?>" <?php echo $position['value']==$module['position'] ? 'selected':'';?>><?php echo $position['text'];?></option>
				  <?php } ?>
              </select></td>
            <td class="left"><select name="video_module[<?php echo $module_row; ?>][status]">
                <?php if ($module['status']) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
            <td class="right"><input type="text" name="video_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $module['sort_order']; ?>" size="3" /></td>
            <td class="left"><a onclick="$('#module-row<?php echo $module_row; ?>').remove();" class="btn btn-sm btn-default" title="<?php echo $button_remove; ?>"><i class="fa fa-minus"></i></a></td>
          </tr>
        </tbody>
        <?php $module_row++; ?>
        <?php } ?>
        <tfoot>
          <tr>
            <td colspan="7"></td>
            <td class="left"><a onclick="addModule();" class="btn btn-primary"><i class="fa fa-plus"></i> <?php echo $button_add_module; ?></a></td>
          </tr>
        </tfoot>
      </table>
    </form>
  </div>
</div>
</div>
<script type="text/javascript" src="view/javascript/ckeditor/ckeditor.js"></script>
<script type="text/javascript"><!--
<?php $module_row = 0; ?>
<?php foreach ($modules as $module) { ?>
CKEDITOR.replace('video_module-<?php echo $module_row; ?>-description', {
  filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
  filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
  filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
  filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
  filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
  filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
});
<?php } ?>
<?php $module_row++; ?>
</script>
<script type="text/javascript"><!--
var module_row = <?php echo $module_row; ?>;

function addModule() {
	html  = '<tbody id="module-row' + module_row + '">';
	html += '  <tr>';
	html += '    <td class="left"><input type="text" name="video_module[' + module_row + '][limit]" value="5" size="3" /></td>';
	html += '    <td class="left"><input type="text" name="video_module[' + module_row + '][limitdescription]" value="200" size="3" /></td>';
	html += '    <td class="left"><input type="text" name="video_module[' + module_row + '][image_width]" value="80" size="3" /> <input type="text" name="video_module[' + module_row + '][image_height]" value="80" size="3" /></td>';
	html += '    <td class="left"><select name="video_module[' + module_row + '][layout_id]">';
	<?php foreach ($layouts as $layout) { ?>
	html += '      <option value="<?php echo $layout['layout_id']; ?>"><?php echo addslashes($layout['name']); ?></option>';
	<?php } ?>
	html += '    </select></td>';
	html += '    <td class="left"><select name="video_module[' + module_row + '][position]">';
	<?php foreach($positions as $position){ ?>
	html += '      <option value="<?php echo $position['value'];?>"><?php echo $position['text'];?></option>';
	<?php } ?>
	html += '    </select></td>';
	html += '    <td class="left"><select name="video_module[' + module_row + '][status]">';
    html += '      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
    html += '      <option value="0"><?php echo $text_disabled; ?></option>';
    html += '    </select></td>';
	html += '    <td class="right"><input type="text" name="video_module[' + module_row + '][sort_order]" value="" size="3" /></td>';
	html += '    <td class="left"><a onclick="$(\'#module-row' + module_row + '\').remove();" class="btn btn-sm btn-default" title="<?php echo $button_remove; ?>"><i class="fa fa-minus"></i></a><?php echo $button_remove; ?></a></td>';
	html += '  </tr>';
	html += '</tbody>';

	$('#module tfoot').before(html);

	module_row++;
}
//--></script>
<?php echo $footer; ?>
