<?php echo $header; ?>
<div class="content-wrapper" id="content">
  <section class="content-header">
		<h1><?php echo $heading_title; ?><small>(<?php echo $text_configuration; ?>)</small></h1>
		<ol class="breadcrumb">
			<?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
				<li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
			<?php $k++; } ?>
		</ol>
	</section>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="content">
    <div class="box box-default">
	  <div class="box-header">
         <div class="pull-right">
		         <a onclick="location = '<?php echo $cancel; ?>';" class="btn btn-default"><i class="fa fa-reply"></i> <?php echo $button_cancel; ?></a>
		           <a onclick="$('#form').submit();" class="btn btn-primary"><i class="fa fa-check" aria-hidden="true"></i> <?php echo $button_save; ?></a>
	       </div>
    </div>
    <div class="box-body">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <!-- start image -->
        <table id="images" class="list">
          <thead>
            <tr>
              <td class="left"><?php echo $Name; ?></td>
              <td class="left"><?php echo $Phone; ?></td>
              <td class="left"><?php echo $Email; ?></td>
              <td class="left"><?php echo $Facebook; ?></td>
              <td class="left"><?php echo $Skype; ?></td>
              <td class="left"><?php echo $nivo_image ?></td>
              <td></td>
            </tr>
          </thead>
          <?php $image_row = 0; ?>
          <?php foreach ($images as $image) { ?>
          <tbody id="image-row<?php echo $image_row; ?>">
            <tr>
                <td class="left"><input type="text" name="image_avatar[<?php echo $image_row; ?>][name]" value="<?php echo  isset($image['name']) ? $image['name'] : ''; ?>" /></td>
                <td class="left"><input type="text" name="image_avatar[<?php echo $image_row; ?>][phone]" value="<?php echo  isset($image['phone']) ? $image['phone'] : ''; ?>" /></td>
                <td class="left"><input type="text" name="image_avatar[<?php echo $image_row; ?>][email]" value="<?php echo  isset($image['email']) ? $image['email'] : ''; ?>" /></td>
                <td class="left"><input type="text" name="image_avatar[<?php echo $image_row; ?>][facebook]" value="<?php echo  isset($image['facebook']) ? $image['facebook'] : ''; ?>" /></td>
                <td class="left"><input type="text" name="image_avatar[<?php echo $image_row; ?>][skype]" value="<?php echo  isset($image['skype']) ? $image['skype'] : ''; ?>" /></td>
              <td class="left"><div class="image">
                  <img src="<?php echo $image['thumb']; ?>" alt="" id="thumb<?php echo $image_row; ?>" />
                  <input type="hidden" name="image_avatar[<?php echo $image_row; ?>][image]" value="<?php echo $image['image']; ?>" id="image<?php echo $image_row; ?>"  />
                  <br />
                  <a onclick="image_upload('image<?php echo $image_row; ?>', 'thumb<?php echo $image_row; ?>');"><?php echo $text_browse; ?>
                  </a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#thumb<?php echo $image_row; ?>').attr('src', '<?php echo $no_image; ?>'); $('#image<?php echo $image_row; ?>').attr('value', '');"><?php echo $text_clear; ?></a>
                </div></td>
              <td class="left"><a onclick="$('#image-row<?php echo $image_row; ?>').remove();" class="btn btn-sm btn-default" title="<?php echo $button_remove; ?>"><i class="fa fa-minus"></i></a></td>
            </tr>
          </tbody>
          <?php $image_row++; ?>
          <?php } ?>
          <tfoot>
            <tr>
              <td colspan="5"></td>
              <td class="left"><a onclick="addImage();" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> <?php echo $button_add_image; ?></a></td>
            </tr>
          </tfoot>
        </table>
        <!-- end image -->
        <table id="module" class="list">
          <thead>
            <tr>
              <td class="left"><?php echo $entry_image; ?></td>
              <td class="left"><?php echo $entry_layout; ?></td>
              <td class="left"><?php echo $entry_position; ?></td>
              <td class="left"><?php echo $entry_status; ?></td>
              <td class="right"><?php echo $entry_sort_order; ?></td>
              <td></td>
            </tr>
          </thead>
          <?php $module_row = 0; ?>
          <?php foreach ($modules as $module) { ?>
          <tbody id="module-row<?php echo $module_row; ?>">
            <tr>
              <td class="left"><input type="text" name="supportonline_module[<?php echo $module_row; ?>][image_width]" value="<?php echo $module['image_width']; ?>" size="3" />
                <input type="text" name="supportonline_module[<?php echo $module_row; ?>][image_height]" value="<?php echo $module['image_height']; ?>" size="3" />
                <?php if (isset($error_image[$module_row])) { ?>
                <span class="error"><?php echo $error_image[$module_row]; ?></span>
                <?php } ?></td>
              <td class="left"><select name="supportonline_module[<?php echo $module_row; ?>][layout_id]">
                  <?php foreach ($layouts as $layout) { ?>
                  <?php if ($layout['layout_id'] == $module['layout_id']) { ?>
                  <option value="<?php echo $layout['layout_id']; ?>" selected="selected"><?php echo $layout['name']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select></td>
              <td class="left"><select name="supportonline_module[<?php echo $module_row; ?>][position]">
				  <?php foreach($positions as $position){ ?>
					<option value="<?php echo $position['value'];?>" <?php echo $position['value']==$module['position'] ? 'selected':'';?>><?php echo $position['text'];?></option>
				  <?php } ?>
                </select></td>
              <td class="left"><select name="supportonline_module[<?php echo $module_row; ?>][status]">
                  <?php if ($module['status']) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
              <td class="right"><input type="text" name="supportonline_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $module['sort_order']; ?>" size="3" /></td>
              <td class="left"><a onclick="$('#module-row<?php echo $module_row; ?>').remove();" class="btn btn-sm btn-default" title="<?php echo $button_remove; ?>"><i class="fa fa-minus"></i></a></a></td>
            </tr>
          </tbody>
          <?php $module_row++; ?>
          <?php } ?>
          <tfoot>
            <tr>
              <td colspan="5"></td>
              <td class="left"><a onclick="addModule();" class="btn btn-primary"><i class="fa fa-plus"></i> <?php echo $button_add_module; ?></a></td>
            </tr>
          </tfoot>
        </table>
      </form>
    </div>
  </div>
</div>
  <script type="text/javascript"><!--
    var image_row = <?php echo $image_row; ?>;

    function addImage() {
      html  = '<tbody id="image-row' + image_row + '">';
      html += '<tr>';
      html += '<td class="left"><input type="text" name="image_avatar[' + image_row + '][name]" value="" /></td>';
      html += '<td class="left"><input type="text" name="image_avatar[' + image_row + '][phone]" value="" /></td>';
      html += '<td class="left"><input type="text" name="image_avatar[' + image_row + '][email]" value="" /></td>';
      html += '<td class="left"><input type="text" name="image_avatar[' + image_row + '][facebook]" value="" /></td>';
      html += '<td class="left"><input type="text" name="image_avatar[' + image_row + '][skype]" value="" /></td>';
      html += '<td class="left"><div class="image"><img src="<?php echo $no_image; ?>" alt="" id="thumb' + image_row + '" /><input type="hidden" name="image_avatar[' + image_row + '][image]" value="" id="image' + image_row + '" /><br /><a onclick="image_upload(\'image' + image_row + '\', \'thumb' + image_row + '\');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$(\'#thumb' + image_row + '\').attr(\'src\', \'<?php echo $no_image; ?>\'); $(\'#image' + image_row + '\').attr(\'value\', \'\');"><?php echo $text_clear; ?></a></div></td>';
      html += '<td class="left"><a onclick="$(\'#image-row' + image_row  + '\').remove();" class="btn btn-sm btn-default" title="<?php echo $button_remove; ?>"><i class="fa fa-minus"></i></a><?php echo $button_remove; ?></a></td>';
      html += '</tr>';
      html += '</tbody>';

      $('#images tfoot').before(html);

      image_row++;
    }
    //--></script>
  <script type="text/javascript"><!--
    function image_upload(field, thumb) {
      $('#dialog').remove();

      $('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');

      $('#dialog').dialog({
        title: '<?php echo $text_image_manager; ?>',
        close: function (event, ui) {
          if ($('#' + field).attr('value')) {
            $.ajax({
              url: 'index.php?route=common/filemanager/image&token=<?php echo $token; ?>&image=' + encodeURIComponent($('#' + field).attr('value')),
              dataType: 'text',
              success: function(data) {
                $('#' + thumb).replaceWith('<img src="' + data + '" alt="" id="' + thumb + '" />');
              }
            });
          }
        },
        bgiframe: false,
        width: 700,
        height: 400,
        resizable: false,
        modal: false
      });
    };
    //--></script>

  <script type="text/javascript"><!--
    var module_row = <?php echo $module_row; ?>;

    function addModule() {
      html  = '<tbody id="module-row' + module_row + '">';
      html += '  <tr>';
      html += '    <td class="left"><input type="text" name="supportonline_module[' + module_row + '][image_width]" value="125" size="3" /> <input type="text" name="supportonline_module[' + module_row + '][image_height]" value="125" size="3" /></td>';
      html += '    <td class="left"><select name="supportonline_module[' + module_row + '][layout_id]">';
        <?php foreach ($layouts as $layout) { ?>
          html += '<option value="<?php echo $layout['layout_id']; ?>"><?php echo addslashes($layout['name']); ?></option>';
        <?php } ?>
      html += '    </select></td>';
      html += '    <td class="left"><select name="supportonline_module[' + module_row + '][position]">';
      <?php foreach($positions as $position){ ?>
	html += '      <option value="<?php echo $position['value'];?>"><?php echo $position['text'];?></option>';
	<?php } ?>
      html += '    </select></td>';
      html += '    <td class="left"><select name="supportonline_module[' + module_row + '][status]">';
      html += '      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
      html += '      <option value="0"><?php echo $text_disabled; ?></option>';
      html += '    </select></td>';
      html += '    <td class="right"><input type="text" name="supportonline_module[' + module_row + '][sort_order]" value="" size="3" /></td>';
      html += '    <td class="left"><a onclick="$(\'#module-row' + module_row + '\').remove();" class="btn btn-sm btn-default" title="<?php echo $button_remove; ?>"><i class="fa fa-minus"></i></a><?php echo $button_remove; ?></a></td>';
      html += '  </tr>';
      html += '</tbody>';

      $('#module tfoot').before(html);

      module_row++;
    }
    //--></script>

<?php echo $footer; ?>
