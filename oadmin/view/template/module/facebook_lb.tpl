<?php echo $header; ?>
<div class="content-wrapper">
  <section class="content-header">
		<h1><?php echo $heading_title; ?></h1>
		<ol class="breadcrumb">
			<?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
				<li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
			<?php $k++; } ?>
		</ol>
	</section>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<div class="content">
  <div class="box box-default">
   <div class="box-header">
      <div class="pull-right">
  		    <a onclick="$('#form').submit();" class="btn btn-primary"><i class="fa fa-check" aria-hidden="true"></i> <?php echo $button_save; ?></a>
  		      <a onclick="location = '<?php echo $cancel; ?>';" class="btn btn-default"><i class="fa fa-reply"></i> <?php echo $button_cancel; ?></a>
  	</div>
  </div>
  <div class="box-body">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
      <table id="module" class="table table-bordered table-hover dataTable">
        <thead>
          <tr>
            <td class="left"><?php echo $entry_page_url; ?></td>
            <td class="left"><?php echo $entry_dimension; ?></td>
            <td class="left"><?php echo $entry_faces; ?></td>
            <td class="left"><?php echo $entry_stream; ?></td>
            <td class="left"><?php echo $entry_header; ?></td>
            <td class="left"><?php echo $entry_layout; ?></td>
            <td class="left"><?php echo $entry_position; ?></td>
            <td class="left"><?php echo $entry_status; ?></td>
            <td class="right"><?php echo $entry_sort_order; ?></td>
            <td></td>
          </tr>
        </thead>
        <?php $module_row = 0; ?>
        <?php foreach ($modules as $module) { ?>
        <tbody id="module-row<?php echo $module_row; ?>">
          <tr>
			      <td class="left"><input type="text" name="facebook_lb_module[<?php echo $module_row; ?>][page_url]" value="<?php echo $module['page_url']; ?>"/>
			         <?php if (isset($error_page_url[$module_row])) { ?>
              <br /><span class="error"><?php echo $error_page_url[$module_row]; ?></span>
              <?php } ?></td>
            <td class="left">
				          <input type="text" name="facebook_lb_module[<?php echo $module_row; ?>][width]" value="<?php echo $module['width']; ?>" size="3" />
			               <input type="text" name="facebook_lb_module[<?php echo $module_row; ?>][height]" value="<?php echo $module['height']; ?>" size="3"/>
    				<?php if (isset($error_dimension[$module_row])) { ?>
    				<br /><span class="error"><?php echo $error_dimension[$module_row]; ?></span>
    				<?php } ?>
			       </td>
      			</td>
			     <td class="left"><select name="facebook_lb_module[<?php echo $module_row; ?>][show_faces]">
                <?php if ($module['show_faces']) { ?>
                <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                <option value="0"><?php echo $text_no; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_yes; ?></option>
                <option value="0" selected="selected"><?php echo $text_no; ?></option>
                <?php } ?>
              </select></td>
			     <td class="left"><select name="facebook_lb_module[<?php echo $module_row; ?>][show_stream]">
                <?php if ($module['show_stream']) { ?>
                <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                <option value="0"><?php echo $text_no; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_yes; ?></option>
                <option value="0" selected="selected"><?php echo $text_no; ?></option>
                <?php } ?>
              </select></td>
			     <td class="left"><select name="facebook_lb_module[<?php echo $module_row; ?>][show_header]">
                <?php if ($module['show_header']) { ?>
                <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                <option value="0"><?php echo $text_no; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_yes; ?></option>
                <option value="0" selected="selected"><?php echo $text_no; ?></option>
                <?php } ?>
              </select></td>
            <td class="left"><select style="width:80px"  name="facebook_lb_module[<?php echo $module_row; ?>][layout_id]">
                <?php foreach ($layouts as $layout) { ?>
                <?php if ($layout['layout_id'] == $module['layout_id']) { ?>
                <option value="<?php echo $layout['layout_id']; ?>" selected="selected"><?php echo $layout['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select></td>
            <td class="left"><select style="width:80px"  name="facebook_lb_module[<?php echo $module_row; ?>][position]">
                <?php foreach($positions as $position){ ?>
					<option value="<?php echo $position['value'];?>" <?php echo $position['value']==$module['position'] ? 'selected':'';?>><?php echo $position['text'];?></option>
				  <?php } ?>
              </select></td>
            <td class="left"><select name="facebook_lb_module[<?php echo $module_row; ?>][status]">
                <?php if ($module['status']) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
            <td class="right"><input type="text" name="facebook_lb_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $module['sort_order']; ?>" size="3" /></td>
            <td class="left"><a title="<?php echo $button_remove; ?>" onclick="$('#module-row<?php echo $module_row; ?>').remove();" class="btn btn-sm btn-default"><i class="fa fa-minus"></i></a></td>
          </tr>
		  <tr>
		  </tr>
        </tbody>
        <?php $module_row++; ?>
        <?php } ?>
        <tfoot>
          <tr>
            <td colspan="9"></td>
            <td class="left"><a onclick="addModule();" class="btn btn-primary"><i class="fa fa-plus"></i> <?php echo $button_add_module; ?></a></td>
          </tr>
        </tfoot>
      </table>
    </form>
  </div>
</div>
</div>
<script type="text/javascript"><!--
var module_row = <?php echo $module_row; ?>;

function addModule() {
	html  = '<tbody id="module-row' + module_row + '">';
	html += '  <tr>';
	html += '    <td class="left"><input type="text" name="facebook_lb_module[' + module_row + '][page_url]" value="" /></td>';
	html += '    <td class="left"><input type="text" name="facebook_lb_module[' + module_row + '][width]" value="180" size="3" /> <input type="text" name="facebook_lb_module[' + module_row + '][height]" value="250" size="3" /></td>';
	html += '    <td class="left"><select name="facebook_lb_module[' + module_row + '][show_faces]">';
    html += '      <option value="1" selected="selected"><?php echo $text_yes; ?></option>';
    html += '      <option value="0"><?php echo $text_no; ?></option>';
    html += '    </select></td>';
	html += '    <td class="left"><select name="facebook_lb_module[' + module_row + '][show_stream]">';
    html += '      <option value="1"><?php echo $text_yes; ?></option>';
    html += '      <option value="0" selected="selected"><?php echo $text_no; ?></option>';
    html += '    </select></td>';
	html += '    <td class="left"><select name="facebook_lb_module[' + module_row + '][show_header]">';
    html += '      <option value="1"><?php echo $text_yes; ?></option>';
    html += '      <option value="0" selected="selected"><?php echo $text_no; ?></option>';
    html += '    </select></td>';
	html += '    <td class="left"><select name="facebook_lb_module[' + module_row + '][layout_id]">';
	<?php foreach ($layouts as $layout) { ?>
	html += '      <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>';
	<?php } ?>
	html += '    </select></td>';
	html += '    <td class="left"><select name="facebook_lb_module[' + module_row + '][position]">';
	<?php foreach($positions as $position){ ?>
	html += '      <option value="<?php echo $position['value'];?>"><?php echo $position['text'];?></option>';
	<?php } ?>
	html += '    </select></td>';
	html += '    <td class="left"><select name="facebook_lb_module[' + module_row + '][status]">';
    html += '      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
    html += '      <option value="0"><?php echo $text_disabled; ?></option>';
    html += '    </select></td>';
	html += '    <td class="right"><input type="text" name="facebook_lb_module[' + module_row + '][sort_order]" value="" size="3" /></td>';
	html += '    <td class="left"><a onclick="$(\'#module-row' + module_row + '\').remove();" class="btn btn-sm btn-default" title="<?php echo $button_remove; ?>"><i class="fa fa-minus"></i></a><?php echo $button_remove; ?></a></td>';
	html += '  </tr>';
	html += '</tbody>';

	$('#module tfoot').before(html);

	module_row++;
}

//--></script>
<?php echo $footer; ?>
