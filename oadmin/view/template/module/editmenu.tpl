<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table id="module" class="list">
          <thead>
            <tr>
              <td class="left"><?php echo $entry_layout; ?></td>
              <td class="left"><?php echo $entry_position; ?></td>
              <td class="left"><?php echo $entry_sort_order; ?></td>
              <td class="left"><?php echo $entry_status; ?></td>              
              <td></td>
            </tr>
          </thead>
          <?php $module_row = 0; ?>
          <?php foreach ($modules as $module) { ?>
          <tbody id="module-row<?php echo $module_row; ?>">
            <tr>
              <td class="left"><select name="editmenu_vna[<?php echo $module_row; ?>][store_id]">
                  <?php foreach ($stores as $layout) { ?>
                  <?php if ($layout['store_id'] == $module['store_id']) { ?>
                  <option value="<?php echo $layout['store_id']; ?>" selected="selected"><?php echo $layout['name']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $layout['store_id']; ?>"><?php echo $layout['name']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select></td>
              <td class="left"><select name="editmenu_vna[<?php echo $module_row; ?>][header_menu]">
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php foreach ($menu_groups as $group) { ?>
                	<?php if($group['id'] == $module['header_menu']):?>
                		<option value="<?php echo $group['id']; ?>" selected="selected"><?php echo addslashes($group['title']); ?></option>
                	<?php else: ?>
                		<option value="<?php echo $group['id']; ?>"><?php echo addslashes($group['title']); ?></option>
                	<?php endif;?>
				 
				<?php } ?>
                </select></td>
              <td class="left"><input type="text" name="editmenu_vna[<?php echo $module_row; ?>][columns_menu]" value="<?php echo $module['columns_menu']; ?>">
                </td>
              <td class="left"><select name="editmenu_vna[<?php echo $module_row; ?>][status]">
                  <?php if ($module['status']) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
              
              <td class="left"><a onclick="$('#module-row<?php echo $module_row; ?>').remove();" class="button"><?php echo $button_remove; ?></a></td>
            </tr>
          </tbody>
          <?php $module_row++; ?>
          <?php } ?>
          <tfoot>
            <tr>
              <td colspan="4"></td>
              <td class="left"><a onclick="addModule();" class="button"><?php echo $button_add_module; ?></a></td>
            </tr>
          </tfoot>
        </table>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
var module_row = <?php echo $module_row; ?>;

function addModule() {	
	html  = '<tbody id="module-row' + module_row + '">';
	html += '  <tr>';
	html += '    <td class="left"><select name="editmenu_vna[' + module_row + '][store_id]">';
	<?php foreach ($stores as $layout) { ?>
	html += '      <option value="<?php echo $layout['store_id']; ?>"><?php echo addslashes($layout['name']); ?></option>';
	<?php } ?>
	html += '    </select></td>';
	
	html += '    <td class="left"><select name="editmenu_vna[' + module_row + '][header_menu]">';
	html += '      <option value="0"><?php echo $text_disabled; ?></option>';
	<?php foreach ($menu_groups as $group) { ?>
	html += '      <option value="<?php echo $group['id']; ?>"><?php echo addslashes($group['title']); ?></option>';
	<?php } ?>
	html += '    </select></td>';
	
	html += '    <td class="left"><input type="text" name="editmenu_vna[' + module_row + '][columns_menu]">';
	
	html += '    </td>';
	
	html += '    <td class="left"><select name="editmenu_vna[' + module_row + '][status]">';
    html += '      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
    html += '      <option value="0"><?php echo $text_disabled; ?></option>';
    html += '    </select></td>';
	html += '    <td class="left"><a onclick="$(\'#module-row' + module_row + '\').remove();" class="button"><?php echo $button_remove; ?></a></td>';
	html += '  </tr>';
	html += '</tbody>';
	
	$('#module tfoot').before(html);
	
	module_row++;
}
//--></script> 
<?php echo $footer; ?>