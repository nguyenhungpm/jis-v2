<?php echo $header; ?>
<div class="content-wrapper" id="content">
  <section class="content-header">
    <h1><?php echo $heading_title; ?><small>(<?php echo $text_page_details; ?>)</small></h1>
    <ol class="breadcrumb">
      <?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
        <li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
      <?php $k++; } ?>
    </ol>
  </section>
  <?php if (isset($success)) { ?>
    <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="content">
  <div class="box box-default">
    <div class="box-header">
    <div class="pull-right">
    <a href="<?php echo $csvfile; ?>" class="btn btn-primary"><i class="fa fa-cloud-download" aria-hidden="true"></i> <?php echo $button_csv; ?> </a>
    <a onclick="$('#execute').val('markasread');$('#form').submit();" class="btn btn-primary"><i class="fa fa-check" aria-hidden="true"></i> <?php echo $button_read; ?> </a>
    </div>
  <div class="pull-left">
    <a onclick="$('#execute').val('delete');$('#form').submit();" class="btn btn-default"><i class="fa fa-trash" aria-hidden="true"></i> <?php echo $button_delete; ?> </a>
  </div>
    </div>
    <div class="content">
    <form action="<?php echo $execute; ?>" method="post" enctype="multipart/form-data" id="form">
    <input type="hidden" name="execute" id="execute" />

      <table class="list">
        <thead>
          <tr>
           <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
            <td class="left"><?php echo $column_name; ?></td>
            <td class="left"><?php echo $column_email; ?></td>
            <td class="left"><?php echo $column_ip; ?></td>
            <td class="right"><?php echo $column_description; ?></td>
           <td class="right"><?php echo $column_action; ?></td>
          </tr>
        </thead>
        <tbody>
          <?php if ($contact_info) { ?>
          <?php foreach ($contact_info as $contact) { ?>
            <tr>
            <td class="left"> <input type="checkbox" name="selected[]" value="<?php echo $contact['contact_id']; ?>" /></td>
            <td class="left" <?php if ($contact['is_read'] == 0){echo 'style="font-size:16px;"';}else{'style="font-size:14px"';} ?>> <?php echo $contact['firstname']; ?></td>
            <td class="left" <?php if ($contact['is_read'] == 0){echo 'style="font-size:16px;"';}else{'style="font-size:14px"';} ?>> <?php echo $contact['email'] ?></td>
            <td class="left" <?php if ($contact['is_read'] == 0){echo 'style="font-size:16px;"';}else{'style="font-size:14px"';} ?>> <?php echo $contact['ipaddress'] ?></td>
            <td class="right"><?php $contact_message = $contact['enquiry']; $message = substr($contact_message, 0,'80');  echo $message; ?>...</td>
               <td class="right">  
              [ <a href="<?php echo $contact['view'] ?>"> <?php echo $button_view; ?></a> ]
              [ <a href="<?php echo $contact['reply'] ?>"> <?php echo $button_reply; ?></a> ]
              </td> 
          </tr>
          <?php } ?>
          <?php } else { ?>
          <tr>
            <td class="center" colspan="8"><?php echo $text_no_results; ?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
      </form>
    </div>
  </div>
  </div>
</div>
<?php echo $footer; ?>