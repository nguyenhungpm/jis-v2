<?php echo $header; ?>
<style>
	.marked {
		border-left: 5px solid orange;
	}

	.auto-product {
		border: 1px solid #ccc;
		height: 30px;
		width: 300px;
		line-height: 30px;
		padding: 3px 10px;
	}

	.scrollbox {
		height: 300px;
		width: 400px;
	}

	div#featured-product {
		float: left;
		clear: both;
	}

	div#featured-product div {
		display: block;
		cursor: pointer;
	}

	a.m_mover {
		float: left;
		margin-left: 15px;
		text-decoration: none;
		text-align: center;
		outline: none;
	}

	a.m_mover span {
		display: block;
		margin: 5px auto;
		color: #666;
	}

	a.m_mover span:before {
		content: '';
		width: 22px;
		height: 27px;
		display: block;
		margin: 0 auto;
		background: url(view/image/mover_up_bg.png) left top no-repeat;
	}

	a+a.m_mover span:before {
		background-image: url(view/image/mover_down_bg.png);
	}

	a.m_mover:hover span {
		color: #333;
	}

	a.m_mover:hover span:before {
		background-position: center top
	}

	a.m_mover:active span {
		color: #5c9bc8;
	}

	a.m_mover:active span:before {
		background-position: right top
	}
</style>
<div class="content-wrapper" id="content">
	<section class="content-header">
		<h1><?php echo $heading_title; ?><small>(<?php echo $text_page_details; ?>)</small></h1>
		<ol class="breadcrumb">
			<?php $k = 0;
			foreach ($breadcrumbs as $breadcrumb) { ?>
				<li class="<?php echo $k == count($breadcrumbs) - 1 ? 'active' : ''; ?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k == 0 ? '<i class="fa fa-dashboard"></i> ' : ''; ?><?php echo $breadcrumb['text']; ?></a></li>
			<?php $k++;
			} ?>
		</ol>
	</section>
	<?php if ($error_warning) { ?>
		<div class="warning"><?php echo $error_warning; ?></div>
	<?php } ?>
	<div class="content">
		<div class="box box-default">
			<div class="box-header">
				<a class="btn btn-default btn-sm pull-left" href="<?php echo $cancel; ?>"><i class="fa fa-reply"></i> <?php echo $button_close; ?></a>
				<div class="pull-right">
					<a class="btn btn-primary" onclick="$('#form').submit();"><i class="fa fa-check" aria-hidden="true"></i> <?php echo $button_save; ?></a>
				</div>
			</div>
		</div>

		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
			<div class="row">
				<div class="col-md-9">
					<div class="box box-danger">
						<div class="box-body">
							<ul class="nav nav-tabs <?php echo count($languages)==1 ? 'hide':''; ?>">
								<?php foreach ($languages as $key=> $language) { ?>
									<li class="<?php echo $language['language_id'] == $this->config->get('config_language_id') ? 'active':'';?>"><a data-toggle="tab" aria-expanded="true" href="#language<?php echo $language['language_id']; ?>"><img <?php echo count($languages) ==1 ? 'class="hide"':''; ?> src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
								<?php } ?>
							</ul>
							<div class="tab-content">
								<?php foreach ($languages as $key => $language) { ?>
									<div class="tab-pane <?php echo $language['language_id'] == $this->config->get('config_language_id') ? 'active' : ''; ?>" id="language<?php echo $language['language_id']; ?>">
										<!-- <div class="form-group required">
											<label class="control-label"><?php echo $entry_name; ?></label>
											<input class="form-control" type="text" name="name" class="form-control" value="<?php echo $name; ?>" />
											<?php if (isset($error_name[$language['language_id']])) { ?>
												<div class="text-danger"><?php echo $error_name[$language['language_id']]; ?></div>
											<?php } ?>
										</div> -->
										<div class="form-group required">
											<label class="control-label">Tên</label>
											<input class="form-control" name="manufacturer_description[<?php echo $language['language_id']; ?>][title]" id="title<?php echo $language['language_id']; ?>" value="<?php echo isset($manufacturer_description[$language['language_id']]) ? $manufacturer_description[$language['language_id']]['title'] : ''; ?>" />
										</div>
										<div class="form-group">
											<label class="control-label">Vai trò</label>
											<input class="form-control" name="manufacturer_description[<?php echo $language['language_id']; ?>][position]" id="position<?php echo $language['language_id']; ?>" value="<?php echo isset($manufacturer_description[$language['language_id']]) ? $manufacturer_description[$language['language_id']]['position'] : ''; ?>" />
										</div>
										<div class="form-group">
											<label class="control-label"><?php echo $entry_description; ?></label>
											<textarea name="manufacturer_description[<?php echo $language['language_id']; ?>][description]" id="description<?php echo $language['language_id']; ?>"><?php echo isset($manufacturer_description[$language['language_id']]) ? $manufacturer_description[$language['language_id']]['description'] : ''; ?></textarea>
										</div>
									</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="box box-primary">
						<div class="box-body">
							<div class="form-group text-center">
								<div class="image"><img src="<?php echo $thumb; ?>" alt="" id="thumb" /><br />
									<input type="hidden" name="image" value="<?php echo $image; ?>" id="image" />
									<a onclick="image_upload('image', 'thumb');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#thumb').attr('src', '<?php echo $no_image; ?>'); $('#image').attr('value', '');"><?php echo $text_clear; ?></a>
								</div>
							</div>
							<div class="form-group hide">
								<label class="label-control" for="home">Show on homepage?</label>
								<label class="switch pull-right">
									<input type="checkbox" name="home" <?php echo $home ? 'checked' : ''; ?> value="1" />
									<span class="slider round"></span>
								</label>
							</div>
							<div class="form-group">
								<label for="manucat_id" class="control-label">Phân nhóm</label>
								<select name="manucat_id" id="manucat_id" class="form-control">
									<option value="0">- chọn --</option>
									<?php foreach ($manucats as $cat) { ?>
										<?php if($manucat_id==$cat['manucat_id']){ ?>
											<option selected="selected" value="<?php echo $cat['manucat_id']; ?>"><?php echo $cat['name']; ?></option>
										<?php }else { ?>
											<option value="<?php echo $cat['manucat_id']; ?>"><?php echo $cat['name']; ?></option>
										<?php } ?>
									<?php } ?>
								</select>
							</div>
							<div class="form-group">
								<label class="label-control" for="sort_order"><?php echo $entry_sort_order; ?></label>
								<input type="text" name="sort_order" value="<?php echo $sort_order; ?>" size="3" class="form-control" />
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-9 hide">
					<div class="box box-primary">
						<div class="box-header">
							<h3 class="box-title">SEO</h3>
						</div>
						<div class="box-body">
							<div class="tab-content">
								<?php foreach ($languages as $key => $language) { ?>
									<div id="language<?php echo $language['language_id']; ?>">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<input placeholder="Meta title" class="form-control" type="text" name="manufacturer_description[<?php echo $language['language_id']; ?>][meta_title]" value="<?php echo isset($manufacturer_description[$language['language_id']]) ? $manufacturer_description[$language['language_id']]['meta_title'] : ''; ?>" style="width:100%">
													<span class="small">Remain <input style="font-weight:bold;border:none" disabled size="3" id="descseo" value="70" /> characters</span>
												</div>
												<div class="form-group">
													<input placeholder="Meta keyword" class="form-control" name="manufacturer_description[<?php echo $language['language_id']; ?>][meta_keyword]" rows="5" style="width:100%" value="<?php echo isset($manufacturer_description[$language['language_id']]) ? $manufacturer_description[$language['language_id']]['meta_keyword'] : ''; ?>">
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<textarea class="form-control" name="manufacturer_description[<?php echo $language['language_id']; ?>][meta_description]" style="width:100%" rows="5"><?php echo isset($manufacturer_description[$language['language_id']]) ? $manufacturer_description[$language['language_id']]['meta_description'] : ''; ?></textarea>
													<span class="small">Remain <input style="font-weight:bold;border:none" disabled size="3" id="descseo" value="160" /> characters</span>
												</div>
											</div>
										</div>
									</div>
								<?php } ?>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-9">
										<input class="form-control" size="60" type="text" name="keyword" value="<?php echo $keyword; ?>" id="seo_keyword" placeholder="SEO url" />
									</div>
									<div class="col-md-3">
										<input type="text" placeholder="Robots tag" class="form-control" name="robots" value="<?php echo $robots; ?>">
									</div>
								</div>
							</div>
						</div>

					</div>

				</div>
			</div>
		</form>

	</div>
</div>
<script type="text/javascript">
	<!--
	function image_upload(field, thumb) {
		$('#dialog').remove();

		$('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');

		$('#dialog').dialog({
			title: '<?php echo $text_image_manager; ?>',
			close: function(event, ui) {
				if ($('#' + field).attr('value')) {
					$.ajax({
						url: 'index.php?route=common/filemanager/image&token=<?php echo $token; ?>&image=' + encodeURIComponent($('#' + field).val()),
						dataType: 'text',
						success: function(data) {
							$('#' + thumb).replaceWith('<img src="' + data + '" alt="" id="' + thumb + '" />');
						}
					});
				}
			},
			bgiframe: false,
			width: 1100,
			height: 600,
			resizable: false,
			modal: false,
			dialogClass: 'dlg'
		});
	};
	//
	-->
</script>
<script type="text/javascript">
	<!--
	$('#tabs a').tabs();
	//
	-->
</script>
<script type="text/javascript" src="view/javascript/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
	<!--
	<?php foreach ($languages as $language) { ?>
		CKEDITOR.replace('description<?php echo $language['language_id']; ?>', {
			filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
			filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
			filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
			filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
			filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
			filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
		});
	<?php } ?>
	//
	-->
</script>

<script type="text/javascript">
	<!--
	$('input[name=\'product\']').autocomplete({
		delay: 500,
		source: function(request, response) {
			$.ajax({
				url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request.term),
				dataType: 'json',
				success: function(json) {
					response($.map(json, function(item) {
						return {
							label: item.name,
							value: item.product_id
						}
					}));
				}
			});
		},
		select: function(event, ui) {
			$('#featured-product' + ui.item.value).remove();

			$('#featured-product').append('<div id="featured-product' + ui.item.value + '">' + ui.item.label + '<img src="view/image/delete.png" alt="" /><input type="hidden" value="' + ui.item.value + '" /></div>');

			$('#featured-product div:odd').attr('class', 'odd');
			$('#featured-product div:even').attr('class', 'even');

			data = $.map($('#featured-product input'), function(element) {
				return $(element).attr('value');
			});

			$('input[name=\'products\']').attr('value', data.join());

			return false;
		},
		focus: function(event, ui) {
			return false;
		}
	});

	$('#featured-product div img').on('click', function() {
		$(this).parent().remove();

		$('#featured-product div:odd').attr('class', 'odd');
		$('#featured-product div:even').attr('class', 'even');

		data = $.map($('#featured-product input'), function(element) {
			return $(element).attr('value');
		});

		$('input[name=\'products\']').attr('value', data.join());
	});
	$('#featured-product > div').on('click', function() {
		//$this = $(this);
		if ($(this).hasClass('marked'))
			$(this).removeClass('marked');
		else
			$(this).addClass('marked');
	});

	function moveMarkedItemsUp() {
		$('.marked').each(function(index) {
			//$this = $(this);
			$(this).prev().before($(this));
		});

		reColorThenReMap();
	}

	jQuery.fn.reverse = function() {
		return this.pushStack(this.get().reverse(), arguments);
	};

	function moveMarkedItemsDown() {
		$('.marked').reverse().each(function(index) {
			//$this = $(this);
			$(this).next().after($(this));
		});

		reColorThenReMap();
	}

	function reColorThenReMap() {
		$('#featured-product div').removeClass('odd').removeClass('even');
		$('#featured-product div:odd').addClass('odd');
		$('#featured-product div:even').addClass('even');

		data = $.map($('#featured-product input'), function(element) {
			return $(element).attr('value');
		});

		$('input[name=\'products\']').attr('value', data.join());
	}
	//
	-->
</script>
<?php echo $footer; ?>