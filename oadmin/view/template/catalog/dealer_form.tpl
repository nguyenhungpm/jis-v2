<?php echo $header; ?>
<style type="text/css">
  label{cursor: pointer;margin-right: 10px}
</style>
<div class="content-wrapper">
  <section class="content-header">
    <h1><?php echo $heading_title; ?></h1>
    <ol class="breadcrumb">
      <?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
        <li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
      <?php $k++; } ?>
    </ol>
  </section>
  <?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
<div class="content">
  <div class="box box-default">
    <div class="box-header">
      <div class="pull-right"><a onclick="$('#form').submit();" class="btn btn-primary btn-sm"><span><i class="fa fa-floppy-o" aria-hidden="true"></i> <?php echo $button_save; ?></span></a></div>
      <div class="pull-left"><a onclick="location = '<?php echo $cancel; ?>';" class="btn btn-default btn-sm"><span><i class="fa fa-reply"></i> <?php echo $button_cancel; ?></span></a></div>
    </div>
    <div class="box-body">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <div id="tab-general">
          <div id="languages" class="htabs" style="display:none">
            <?php foreach ($languages as $language) { ?>
            <a href="#language<?php echo $language['language_id']; ?>"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a>
            <?php } ?>
          </div>
          <?php foreach ($languages as $language) { ?>
          <div id="language<?php echo $language['language_id']; ?>">
            <table class="form">
                <tr>
                  <td><span class="required">*</span> <?php echo $entry_name; ?></td>
                  <td><input type="text" name="dealer_description[<?php echo $language['language_id']; ?>][name]" id="dealer_description<?php echo $language['language_id']; ?>_name" onkeyup="trim_name(this.id);" size="100" value="<?php echo isset($dealer_description[$language['language_id']]) ? $dealer_description[$language['language_id']]['name'] : ''; ?>" />
                    <?php if (isset($error_name[$language['language_id']])) { ?>
                    <span class="error"><?php echo $error_name[$language['language_id']]; ?></span>
                    <?php } ?></td>
                </tr>
                <tr>
                  <td><?php echo "Địa chỉ"; ?></td>
                  <td><input type="text" name="dealer_description[<?php echo $language['language_id']; ?>][address]" id="dealer_description<?php echo $language['language_id']; ?>_name" onkeyup="trim_name(this.id);" size="100" value="<?php echo isset($dealer_description[$language['language_id']]) ? $dealer_description[$language['language_id']]['address'] : ''; ?>" />
                    <?php if (isset($error_name[$language['language_id']])) { ?>
                    <span class="error"><?php echo $error_name[$language['language_id']]; ?></span>
                    <?php } ?></td>
                </tr>
                </table>
            </div>
            <?php } ?>
        		  <table class="form">
        		        <tr>
                      <td><?php echo 'Số điện thoại'; ?></td>
                      <td><input type="text" name="telephone" value="<?php echo $telephone; ?>" size="20" /></td>
                    </tr>
                    <tr>
                      <td>Website</td>
                      <td><input type="text" name="email" value="<?php echo $email; ?>" size="20" /></td>
                    </tr>
        		        <tr>
                      <td><?php echo $entry_sort_order; ?></td>
                      <td><input type="text" name="sort_order" value="<?php echo $sort_order; ?>" size="5" /></td>
                    </tr>
        			       <tr>
                      <td><?php echo $entry_status; ?></td>
                      <td><select name="status">
                          <?php if ($status) { ?>
                          <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                          <option value="0"><?php echo $text_disabled; ?></option>
                          <?php } else { ?>
                          <option value="1"><?php echo $text_enabled; ?></option>
                          <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                          <?php } ?>
                        </select></td>
                      </tr>
                			<tr style="display:none">
                              <td><?php echo $entry_keyword; ?></td>
                              <td><input type="text" name="keyword" id="seo_keyword" value="<?php echo $keyword; ?>" /></td>
                            </tr>
                			<tr>
                      <td><?php echo "Khu vực"; ?></td>
                      <td>
                  				<select name="province" id="province_id" class="form-control"  style="width:230px;float:left;margin-right:20px">
                  					<option>--Toàn quốc--</option>
                  					<?php foreach($provinces as $rs){ ?>
                  					<?php if($rs['province_id']==$province){ ?>
                  					<option selected value="<?php echo $rs['province_id']?>"><?php echo $rs['name']?></option>
                  					<?php }else{ ?>
                  					<option value="<?php echo $rs['province_id']?>"><?php echo $rs['name']?></option>
                  					<?php } ?>
                  					<?php } ?>
                  				</select>
                          <select name="district" id="district_id" class="form-control" style="width:180px">
                  					<option>--Chọn quận/huyện--</option>
                  					<?php foreach($districts as $rs){ ?>
                    					<?php if($rs['district_id']==$district){ ?>
                    					       <option selected value="<?php echo $rs['district_id']?>"><?php echo $rs['name']?></option>
                    					<?php }else{ ?>
                    					       <option value="<?php echo $rs['district_id']?>"><?php echo $rs['name']?></option>
                    					<?php } ?>
                  					<?php } ?>
                  				</select>
              			  </td>
                    </tr>
                    <tr>
                      <td>Sản phẩm</td>
                      <td>
                        <fieldset>
                          <input type="checkbox" <?php
                                  if(!isset($this->request->get['dealer_id'])){
                                    echo 'checked="checked"';
                                  } ?> id="checkall" onclick="for(c in document.getElementsByName('manids')) document.getElementsByName('manids').item(c).checked = this.checked" /> Tất cả<br/>
                          <input type="hidden" name="product" id="product" value="<?php echo $product; ?>" />
                          <br />
                          <?php if($pros) { ?>
                              <?php foreach($pros as $pro){ ?>
                                <label for="manid<?php echo $pro['product_id']; ?>">
                                  <input class="classtemp" name="manids" type="checkbox" value="<?php echo $pro['product_id']; ?>"
                                  <?php
                                  if(!isset($this->request->get['dealer_id'])){
                                    echo 'checked="checked"';
                                  }
                                  if (in_array($pro['product_id'],explode(',',$product))){
                                    echo 'checked="checked"';
                                  } ?>
                                id="manid<?php echo $pro['product_id']; ?>" /> <?php echo $pro['name']; ?></label>
                              <?php } ?>
                          <?php } ?>
                        </fieldset>
                      </td>
                    </tr>
        		</table>
        </div>
        </form>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
$('#checkall, .classtemp').on('change',function(){
  var valuesArray = $('input[name="manids"]:checked').map(function () {
        return this.value;
        }).get().join(",");

  $('#product').val(valuesArray);
});
$('#tabs a').tabs();
$('#languages a').tabs();
//--></script>
<script type="text/javascript"><!--
$('select[name=\'province\']').bind('change', function() {
	$.ajax({
		url: 'index.php?route=catalog/district/province&province_id=' + this.value + '&token=<?php echo $token; ?>',
		dataType: 'json',
		beforeSend: function() {
			$('select[name=\'province\']').after('<span class="wait">&nbsp;<img src="view/image/loading.gif" alt="" /></span>');
		},
		complete: function() {
			$('.wait').remove();
		},
		success: function(json) {
			html = '<option value="">Chọn/quận huyện</option>';

			if (json != '') {
				for (i = 0; i < json.length; i++) {
        			html += '<option value="' + json[i]['district_id'] + '"';

					if (json[i]['district_id'] == '<?php echo $district; ?>') {
	      				html += ' selected="selected"';
	    			}

	    			html += '>' + json[i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0" selected="selected">--Chọn quận/huyện--</option>';
			}

			$('select[name=\'district\']').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('select[name=\'province\']').trigger('change');
//--></script>
<?php echo $footer; ?>
