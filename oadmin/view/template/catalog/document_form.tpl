<?php echo $header; ?>
<style>
.switch {
  display: inline-block;
  font-size: 20px; /* 1 */
  height: 1em;
	cursor: pointer;
  width: 2em;
  background: #BDB9A6;
  border-radius: 1em;
}

.switch div {
  height: 1em;
  width: 1em;
  border-radius: 1em;
  background: #FFF;
  box-shadow: 0 0.1em 0.3em rgba(0,0,0,0.3);
  -webkit-transition: all 300ms;
     -moz-transition: all 300ms;
          transition: all 300ms;
}

.switch input:checked + div {
  -webkit-transform: translate3d(100%, 0, 0);
     -moz-transform: translate3d(100%, 0, 0);
          transform: translate3d(100%, 0, 0);
}
#maskname{
	background: #eee;
	border: 1px solid #ccc;
	border-radius: 6px;
	min-width: 200px;
	padding: 5px 40px 5px 10px;
	box-shadow: inset 2px 2px 1px #ddd
}
#button-remove{
	margin-left: -40px;
	color:red !important
}
</style>
<link rel="stylesheet" href="//cdn.jsdelivr.net/gh/osdvn/mkmate/finder/css/elfinder.min.css" />
<script src="//cdn.jsdelivr.net/gh/osdvn/mkmate/finder/js/elfinder.min.js"></script>
<script src="asset/initck66.js"></script>
<div class="content-wrapper" id="content">
	<section class="content-header">
		<h1><?php echo $heading_title; ?><small>(<?php echo $text_page_details; ?>)</small></h1>
		<ol class="breadcrumb">
			<?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
			<li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
			<?php $k++; } ?>
		</ol>
	</section>
	<?php if ($error_warning) { ?>
	<div class="warning"><?php echo $error_warning; ?></div>
	<?php } ?>
	<div class="content">
		<div class="box box-default">
			<div class="box-header">
				<div class="pull-right">
					<a onclick="$('#form').submit();" class="btn btn-primary"><i class="fa fa-check" aria-hidden="true"></i> <?php echo $button_save; ?></a>
				</div>
				<div class="pull-left">
					<a href="<?php echo $cancel; ?>" class="btn btn-default btn-sm"><i class="fa fa-reply"></i> <?php echo $button_close; ?></a>
					<?php if(isset($document_id) && !empty($document_id)){ ?>
					<a class="btn btn-default btn-sm" onclick="$('#form_delete').submit();" ><i class="fa fa-trash" aria-hidden="true" ></i> <?php echo $button_delete; ?></a>
					<?php } ?>
				</div>
				<?php if(isset($document_id) && !empty($document_id)){ ?>
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form_delete">
					<input type="hidden" name="selected[]" value="<?php echo $document_id; ?>"/>
				</form>
				<?php } ?>
			</div>
		</div>

		<div class="row">
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
				<div class="col-md-8" id="tab-general">
					<div class="box box-primary">
						<div class="box-body">
							<ul class="nav nav-tabs <?php echo count($languages)==1 ? 'hide':''; ?>">
								<?php foreach ($languages as $key=> $language) { ?>
									<li class="<?php echo $language['language_id'] == $this->config->get('config_language_id') ? 'active':'';?>"><a data-toggle="tab" aria-expanded="true" href="#language<?php echo $language['language_id']; ?>"><img <?php echo count($languages) ==1 ? 'class="hide"':''; ?> src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
								<?php } ?>
							</ul>
							<div class="tab-content">
							<?php foreach ($languages as $language) { ?>
							<div class="tab-pane <?php echo $language['language_id'] == $this->config->get('config_language_id') ? 'active':'';?>" id="language<?php echo $language['language_id']; ?>">
								<div class="form-group">
									<span class="required">*</span> <label class="control-label"><?php echo $entry_title; ?></label>
									<input class="form-control" type="text" name="document_description[<?php echo $language['language_id']; ?>][title]" value="<?php echo isset($document_description[$language['language_id']]) ? $document_description[$language['language_id']]['title'] : ''; ?>" />
									<?php if (isset($error_title[$language['language_id']])) { ?>
									<span class="error"><?php echo $error_title[$language['language_id']]; ?></span>
									<?php } ?>
								</div>
								<div class="form-group">
									<label class="control-label">Mô tả ngắn</label>
									<textarea rows="4" class="form-control" type="text" name="document_description[<?php echo $language['language_id']; ?>][short_description]"><?php echo isset($document_description[$language['language_id']]) ? $document_description[$language['language_id']]['short_description'] : ''; ?></textarea>
								</div>
								<div class="form-group">
									<label class="control-label">Nội dung chi tiết</label>
									<textarea id="description<?php echo $language['language_id']; ?>" class="form-control" type="text" name="document_description[<?php echo $language['language_id']; ?>][description]"><?php echo isset($document_description[$language['language_id']]) ? $document_description[$language['language_id']]['description'] : ''; ?></textarea>
								</div>
								<hr />
								<div class="form-group">
									<label class="control-label">Thông điệp tùy chỉnh hiển thị trong mẫu email thông báo</label>
									<textarea id="message<?php echo $language['language_id']; ?>" class="form-control" type="text" name="document_description[<?php echo $language['language_id']; ?>][message]"><?php echo isset($document_description[$language['language_id']]) ? $document_description[$language['language_id']]['message'] : ''; ?></textarea>
								</div>
							</div>
							<?php } ?>
							</div>
						</div>
					</div>

					<div class="box box-info">
						<div class="box-header with-border">
							<h3 class="box-title">SEO</h3>
						</div>
						<div class="box-body">
							<ul class="nav nav-tabs <?php echo count($languages)==1 ? 'hide':''; ?>">
								<?php foreach ($languages as $key=> $language) { ?>
									<li class="<?php echo $language['language_id'] == $this->config->get('config_language_id') ? 'active':'';?>"><a data-toggle="tab" aria-expanded="true" href="#seo<?php echo $language['language_id']; ?>"><img <?php echo count($languages) ==1 ? 'class="hide"':''; ?> src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
								<?php } ?>
							</ul>
							<div class="tab-content">
							<?php foreach ($languages as $language) { ?>
							<div class="tab-pane <?php echo $language['language_id'] == $this->config->get('config_language_id') ? 'active':'';?>" id="seo<?php echo $language['language_id']; ?>">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<input placeholder="Meta title" class="form-control" onKeyDown="textCounter(this, 'titleseo', 70);"
											onKeyUp="textCounter(this,'titleseo',70)" name="document_description[<?php echo $language['language_id']; ?>][meta_title]" value="<?php echo isset($document_description[$language['language_id']]) ? $document_description[$language['language_id']]['meta_title'] : ''; ?>" />
											<label class="control-label"><span class="small help"><?php echo $text_remain; ?> <input style="font-weight:bold;border:none" disabled size="3" id="titleseo" value="70" /> <?php echo $text_characters; ?></span></label>

										</div>
										<div class="form-group">
											<input placeholder="Meta keywords" class="form-control" name="document_description[<?php echo $language['language_id']; ?>][meta_keyword]" value="<?php echo isset($document_description[$language['language_id']]) ? $document_description[$language['language_id']]['meta_keyword'] : ''; ?>"/>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<textarea placeholder="Meta Description" class="form-control" onKeyDown="textCounter(this,'descseo',160);"
											onKeyUp="textCounter(this,'descseo',160)" name="document_description[<?php echo $language['language_id']; ?>][meta_description]" rows="4"><?php echo isset($document_description[$language['language_id']]) ? $document_description[$language['language_id']]['meta_description'] : ''; ?></textarea>
											<label class="control-label"><span class="small help"><?php echo $text_remain; ?> <input style="font-weight:bold;border:none" disabled size="3" id="descseo" value="160" /> <?php echo $text_characters; ?></span></label>
										</div>
									</div>
								</div>
							</div>
							<?php } ?>
							</div>
							<div class="row">
								<div class="col-md-9">
									<div class="form-group">
										<input type="text" name="keyword" value="<?php echo $keyword; ?>" class="form-control" placeholder="SEO URL" />
									</div>
								</div>
								<div class="col-md-3">
									<input type="text" class="form-control" name="robots" value="<?php echo $robots; ?>" placeholder="robots tag" />
								</div>
							</div>
						</div>
					</div>
				</div>

				<div id="tab-data" class="col-md-4">
					<div class="box box-primary">
						<div class="box-body">
							<div class="form-group text-center">
                Cover
								<div class="image"><img src="<?php echo $thumb; ?>" alt="" id="thumb" /><br />
								  <input type="hidden" name="image" value="<?php echo $image; ?>" id="image" />
								  <a onclick="image_upload('image', 'thumb');">Chọn ảnh</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#thumb').attr('src', '<?php echo $no_image; ?>'); $('#image').attr('value', '');">Xóa ảnh</a></div>
							</div>
							<div class="form-group">
									<div class="text-center">Hình thức lưu trữ tài liệu số</div>
									<div class="row text-center" style="margin:10px">
										<div class="pull-left strong">Self hosted</div>
										<label class="switch">
												<input id="type" name="type" <?php echo ($type==1)?'checked':''; ?> value="1" type="checkbox" /><div></div>
	  								</label>
										<div class="pull-right strong">Liên kết URL</div>
									</div>
							</div>
							<div id="selfhost" class="form-group <?php echo ($type==1)?'hidden':''; ?>">
								<label class="control-label">File nội dung:</label><br />
								<input type="hidden" id="filename" name="filename" value="<?php echo $filename; ?>" />
								<input type="hidden" id="mask" name="mask" value="<?php echo $mask; ?>" />
								<?php if($filename==''){ ?>
									<span id="button-upload" class="btn-sm btn-primary btn"><i class="fa fa-upload" aria-hidden="true"></i> Tải lên</span>
									<span id="maskname" class="hidden strong"><?php echo $mask; ?></span><span class="btn hidden btn-link"  data-toggle="tooltip"  title="Xoá file" id="button-remove"><i class="fa fa-times"></i></span>
								<?php }else {?>
									<span id="button-upload" class="btn-sm btn-primary hidden btn"><i class="fa fa-upload" aria-hidden="true"></i> Tải lên</span>
									<span id="maskname" class="strong"><?php echo $mask; ?></span><span class="btn btn-link" data-toggle="tooltip"  title="Xoá file" id="button-remove"><i class="fa fa-times"></i></span>
								<?php } ?>
							</div>
							<div id="linkdown" class="form-group <?php echo ($type == 0)?'hidden':''; ?>">
								<label class="control-label">URL:</label>
								<input type="text" class="form-control" name="linkdown" value="<?php echo $linkdown; ?>" />
							</div>
						</div>
					</div>
					<div class="box box-info">
						<div class="box-body">
							<div class="form-group">
								<label for="doccat_id" class="control-label">Nhóm</label>
								<select name="doccat_id" id="doccat_id" class="form-control">
									<option value="0">- chọn --</option>
									<?php foreach ($doccats as $cat) { ?>
										<?php if($doccat_id==$cat['doccat_id']){ ?>
											<option selected="selected" value="<?php echo $cat['doccat_id']; ?>"><?php echo $cat['name']; ?></option>
										<?php }else { ?>
											<option value="<?php echo $cat['doccat_id']; ?>"><?php echo $cat['name']; ?></option>
										<?php } ?>
									<?php } ?>
								</select>
							</div>
							<div class="form-group">
								<label class="control-label"><?php echo $entry_sort_order; ?></label>
								<input type="text" name="sort_order" class="pull-right" size="4" value="<?php echo $sort_order; ?>" />
							</div>
							<div class="form-group clearfix">
								<label>Ngày đăng</label>
								<div class="input-group date">
									<div class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</div>
									<input type="text" name="date_added" value="<?php echo $date_added; ?>" class="form-control pull-right datepicker">
								</div>
								<!-- /.input group -->
							</div>
							<div class="form-group clearfix">
								<label class="control-label pull-left">Thông báo khi có lượt tải?</label>
								<label class="switch pull pull-right">
									<input type="checkbox" name="notify" <?php echo $notify ? 'checked':''; ?> value="1">
									<span class="slider round"></span>
								</label>
							</div>
							<div class="form-group clearfix">
								<label class="control-label pull-left">Yêu cầu đăng ký?</label>
								<label class="switch pull pull-right">
									<input type="checkbox" name="invisible" <?php echo $invisible ? 'checked':''; ?> value="1">
									<span class="slider round"></span>
								</label>
							</div>
							<div class="form-group clearfix">
								<label class="control-label pull-left"><?php echo $entry_status; ?></label>
								<label class="switch pull pull-right">
									<input type="checkbox" name="status" <?php echo $status ? 'checked':''; ?> value="1">
									<span class="slider round"></span>
								</label>
							</div>
							<hr />
							<h4 style="margin:0">
									Lượt tải: <span style="color:orange"><?php echo $download; ?></span>
							</h4>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?php
if (isset($this->request->get['document_id'])) {
	$document_id = $this->request->get['document_id'];
}else{
	$document_id= 0;
}
?>
<input name="document_id" id="document_id" type="hidden" value="<?php echo $document_id; ?>" />
<script type="text/javascript"><!--
	$('#tabs a').tabs();
	$('#languages a').tabs();
	$('.datepicker').datepicker({format: 'yyyy-mm-dd',autoclose: true});
//--></script>
<script type="text/javascript">
	$(document).ready(function() {
    	$('.js-example-basic-multiple').select2({
			maximumSelectionLength: 3,
			placeholder: "Chọn tối đa 3 ngành hàng",
		});
	});
</script>
<script type="text/javascript" src="view/javascript/jquery/ajaxupload.js"></script>
<script type="text/javascript"><!--
$(document).ready(function() {
		new AjaxUpload('#button-upload', {
		action: 'index.php?route=catalog/document/upload&token=<?php echo $token; ?>',
		name: 'file',
		autoSubmit: true,
		responseType: 'json',
		onSubmit: function(file, extension) {
			$('#button-upload').after('<img src="view/image/loading.gif" class="loading" style="padding-left: 5px;" />');
			$('#button-upload').attr('disabled', true);
		},
		onComplete: function(file, json) {
			$('#button-upload').attr('disabled', false);

			if (json['success']) {
				//alert(json['success']);
				$('input[id=\'filename\']').attr('value', json['filename']);
				$('input[id=\'mask\']').attr('value', json['mask']);
				$('span[id=\'maskname\']').text(json['mask']);
				$('#button-upload').addClass('hidden');
				$('#button-remove').removeClass('hidden');
				$('#maskname').removeClass('hidden');
			}

			if (json['error']) {
				alert(json['error']);
			}

			$('.loading').remove();
		}
		});
});
//--></script>
<script type="text/javascript">
    $('#button-remove').click(function(e) {
            e.preventDefault();
			var rid = $('#document_id').val();
			var filename = encodeURIComponent($('#filename').val());
            $.ajax({
                'url' : 'index.php?route=catalog/document/remove&token=<?php echo $token; ?>&document_id='+ rid + '&filename=' + filename,
                'type' : 'GET',
                beforeSend: function() {
                },
                error: function() {
                   alert('Error');
                },
                'success' : function(data) {
					alert('Đã xóa thành công');
					$('input[id=\'filename\']').attr('value', '');
					$('input[id=\'mask\']').attr('value', '');
					$('span[id=\'maskname\']').text('');
					$('#maskname').addClass('hidden');
					$('#button-remove').addClass('hidden');
					$('#button-upload').removeClass('hidden');
                }
            });
    });
		$(function () {
		  $('[data-toggle="tooltip"]').tooltip()
		});

		$('#type').on('change', function(){
				if ($('#type').is(':checked')) {
          $(this).prop('checked', true);
          $('#linkdown').removeClass('hidden');
					$('#selfhost').addClass('hidden');
				} else {
          $('#selfhost').removeClass('hidden');
					$('#linkdown').addClass('hidden');
          $(this).prop('checked', false);
				}
		});
		
function image_upload(field, thumb) {
	$('#dialog').remove();
	$('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');
	$('#dialog').dialog({
		title: '<?php echo $text_image_manager; ?>',
		close: function (event, ui) {
			if ($('#' + field).attr('value')) {
				$.ajax({
					url: 'index.php?route=common/filemanager/image&token=<?php echo $token; ?>&image=' + encodeURIComponent($('#' + field).attr('value')),
					dataType: 'text',
					success: function(text) {
						$('#' + thumb).replaceWith('<img src="' + text + '" alt="" id="' + thumb + '" />');
					}
				});
			}
		},
		bgiframe: false,
		width: 1002,
		height: 600,
		resizable: false,
		modal: false
	});
};
</script>
<script type="text/javascript" src="view/javascript/ckeditor/ckeditor.js"></script>
<script type="text/javascript"><!--
<?php foreach ($languages as $language) { ?>
CKEDITOR.replace('description<?php echo $language['language_id']; ?>', {
	filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
});
CKEDITOR.replace('message<?php echo $language['language_id']; ?>', {
	filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
});
<?php } ?>
//--></script>
<?php echo $footer; ?>
