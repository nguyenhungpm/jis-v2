<?php echo $header; ?>
<div id="content" class="content-wrapper">
  <section class="content-header">
    <h1><?php echo $heading_title; ?><small>(Danh sách)</small></h1>
    <ol class="breadcrumb">
      <?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
        <li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
      <?php $k++; } ?>
    </ol>
  </section>
    <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
<div class="content">
  <div class="box box-primary">
    <div class="box-header">
      <div class="pull-left">
        <a onclick="$('#form').submit();" class="btn btn-default"><i class="fa fa-times"></i> <?php echo $button_delete; ?></a>
      </div>
      <div class="pull-right">
          <a href="#myModal" data-toggle="modal" title="Thêm nhóm tài liệu" class="btn btn-primary"><i class="fa fa-plus"></i> Thêm mới</a>
      </div>
    </div>
  <div class="box-body">
    <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
      <table class="list">
        <thead>
          <tr>
            <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
            <td>Tên nhóm</td>
            <td width="1" class="center">Trạng thái</td>
            <td width="90px">Thứ tự</td>
            <td width="1" class="center">Thao tác</td>
          </tr>
        </thead>
        <tbody>
          <?php if ($manucats) { ?>
          <?php foreach ($manucats as $manucat) { ?>
          <?php if(isset($this->request->get['manucat_id']) && $manucat['manucat_id'] == $this->request->get['manucat_id']){ ?>
          <tr id="name<?php echo $manucat['manucat_id']; ?>">
            <td style="text-align: center;"><input type="checkbox" name="selected[]" value="<?php echo $manucat['manucat_id']; ?>"/></td>
            <td class="left">
              <?php foreach ($languages as $key=> $language) { ?>
                <label>Tên nhóm (<?php echo $language['name']; ?>)</label>
                <input class="form-control" placeholder="Tên nhóm(<?php echo $language['name']; ?>)" type="text" name="detail[<?php echo $language['language_id'];?>][name]" value="<?php echo !empty($manucat['detail'][$language['language_id']]['name']) ? $manucat['detail'][$language['language_id']]['name']:''; ?>"/>
              <?php } ?>
              <br /><label>SEO URL <br /></label>
              <input class="form-control" placeholder="SEO URL" type="text" name="keyword" value="<?php echo $manucat['keyword']; ?>"/>
            </td>
            <td class="text-center">
              <select name="status" class="form-control">
                <option value="1" <?php echo $manucat['status']==1 ? 'selected':''; ?>>Bật</option>
                <option value="0" <?php echo $manucat['status']==0 ? 'selected':''; ?>>Tắt</option>
              </select>
            </td>
            <td class="left">
                  <input class="form-control" placeholder="thứ tự" type="text" name="sort_order" value="<?php echo $manucat['sort_order']; ?>"/>
             </td>
            <td class="center"><a onclick="save('<?php echo $manucat['manucat_id']; ?>')" class="btn btn-sm btn-primary">Lưu</a></td>
          </tr>
          <?php }else{ ?>
          <tr id="name<?php echo $manucat['manucat_id']; ?>">
            <td style="text-align: center;"><input type="checkbox" name="selected[]" value="<?php echo $manucat['manucat_id']; ?>"/></td>
            <td class="left"><strong><?php echo !empty($manucat['detail'][$this->config->get('config_language_id')]['name']) ? $manucat['detail'][$this->config->get('config_language_id')]['name']:''; ?></strong></td>
            <td><?php echo $manucat['status'] == 1 ? 'Bật':'Tắt'; ?></td>
            <td><?php echo $manucat['sort_order']; ?></td>
            <td class="center"><a href="<?php echo $manucat['edit']; ?>#name<?php echo $manucat['manucat_id']; ?>">Sửa</a></td>
          </tr>
              <?php } ?>
          <?php } ?>
          <?php } else { ?>
          <tr>
            <td class="center" colspan="5"><?php echo $text_no_results; ?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </form>
  <div class="pagination"><?php echo $pagination; ?></div>
  </div>
  </div>
</div>
<div id="myModal" class="modal fade">
  <div class="modal-dialog form-sale">
        <div class="modal-content">
            <div class="modal-header">
        <button type="btn btn-primary btn-sm" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Thêm nhóm tài liệu</h4>
      </div>
      <div class="modal-body">
      <div class="tab-content">
      <form method="POST" action="" name="form-manucat" id="form-manucat">
          <?php foreach ($languages as $key=> $language) { ?>
          <div class="form-group required">
            <label class="control-label text-left">Nhóm tài liệu</label> <img src="view/image/flags/<?php echo $language['image']; ?>" />
            <input class="form-control" type="text" name="detail[<?php echo $language['language_id'];?>][name]"/>
          </div>
          <?php } ?>
          <div class="row">
            <div class="col-md-8">
              <div class="form-group">
              <label class="control-label text-left">SEO URL</label>
              <input class="form-control" type="text" name="keyword"/>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
              <label class="control-label text-left">Trạng thái</label>
              <select name="status" class="form-control">
                <option value="1">Bật</option>
                <option value="0">Tắt</option>
              </select>
              </div>
            </div>
            <div class="col-md-2">
            <div class="form-group">
              <label class="control-label text-left">Thứ tự</label>
              <input class="form-control" type="text" name="sort_order"/>
            </div>
            </div>
          </div>
          </form>
        </div>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
          <button type="button" id="submit" onclick="submit_form();" class="btn btn-primary">Lưu</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$("#myModal").draggable({
  handle: ".modal-header"
});
function submit_form(){
  $.ajax({
    url: 'index.php?route=catalog/manucat/insert&token=<?php echo $token; ?>',
    type: 'post',
    dataType: 'json',
    data : $("#form-manucat").serialize(),
    complete: function() {
      $('#submit').button('reset');
    },
    success: function(json) {
      var href = 'index.php?route=catalog/manucat&token=<?php echo $token; ?>';
      <?php if(isset($page) && !empty($page)){ ?>
      href += '&page=<?php echo $page; ?>';
      <?php } ?>
      window.location = href;
    },
     error: function(e) {
        console.log(e);
     }

  });
}
function save(id){
  var url = 'index.php?route=catalog/manucat/edit&token=<?php echo $token; ?>&manucat_id=' +  id;
  <?php if(isset($page) && !empty($page)){ ?>
  url += '&page=<?php echo $page; ?>';
  <?php } ?>
  $.ajax({
    url: url,
    type: 'POST',
    data : $("#form").serialize(),
    dataType: 'json',
    success: function(json) {
      var href = 'index.php?route=catalog/manucat&token=<?php echo $token; ?>';
      <?php if(isset($page) && !empty($page)){ ?>
      href += '&page=<?php echo $page; ?>';
      <?php } ?>
      window.location = href;
    }
  });
}
</script>
<?php echo $footer; ?>
