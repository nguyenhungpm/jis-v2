<?php echo $header; ?>
<div class="content-wrapper" id="content">
	<section class="content-header">
		<h1><?php echo $heading_title; ?><small>(<?php echo $text_page_details; ?>)</small></h1>
		<ol class="breadcrumb">
			<?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
				<li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
			<?php $k++; } ?>
		</ol>
	</section>
	<?php if (isset($success)) { ?>
		<div class="success"><?php echo $success; ?></div>
	<?php } ?>
	<?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
	<?php } ?>
	<div class="content">
		<div class="box box-default">
			<div class="box-header">
				<div class="pull-right">
					<button type="button" onclick="$('#form').submit();" class="btn btn-primary"><i class="fa fa-check" aria-hidden="true"></i> <?php echo $button_save; ?></button>
				</div>
				<div class="pull-left">
				<a href="<?php echo $cancel; ?>" class="btn btn-default btn-sm"><i class="fa fa-reply"></i> <?php echo $button_close; ?></a>
				  <?php if(isset($group_id) && !empty($group_id)){ ?><a class="btn btn-default btn-sm" onclick="$('#form_delete').submit();"><i class="fa fa-trash"></i> <?php echo $button_delete; ?></a><?php } ?>
				</div>
				<?php if(isset($group_id) && !empty($group_id)){ ?>
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form_delete">
					<input type="hidden" name="selected[]" value="<?php echo $group_id; ?>"/>
				</form>
				<?php } ?>
			</div>
		</div>

		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
			<div class="row">
				<div class="col-md-9">
					<div class="box box-primary">
						<div class="box-body">
							<ul class="nav nav-tabs <?php echo count($languages)==1 ? 'hide':''; ?>">
								<?php foreach ($languages as $key=> $language) { ?>
									<li class="<?php echo $language['language_id'] == $this->config->get('config_language_id') ? 'active':'';?>"><a data-toggle="tab" aria-expanded="true" href="#language<?php echo $language['language_id']; ?>"><img <?php echo count($languages) ==1 ? 'class="hide"':''; ?> src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
								<?php } ?>
							</ul>
							<div class="tab-content">
							<?php foreach ($languages as $language) { ?>
								<div class="tab-pane <?php echo $language['language_id'] == $this->config->get('config_language_id') ? 'active':'';?>" id="language<?php echo $language['language_id']; ?>">
									<div class="form-group">
										<label class="control-label"><?php echo $entry_name; ?></label><span class="required">*</span>
										<input type="text" class="form-control" name="group_description[<?php echo $language['language_id']; ?>][name]" size="100" value="<?php echo isset($group_description[$language['language_id']]) ? $group_description[$language['language_id']]['name'] : ''; ?>" />
										<?php if (isset($error_name[$language['language_id']])) { ?>
											<span class="error"><?php echo $error_name[$language['language_id']]; ?></span>
										<?php } ?>
									</div>
									<div class="form-group">
										<label for="group_description[<?php echo $language['language_id']; ?>][description]" class="control-label"><?php echo $entry_description; ?></label>
									</div>
									<div class="form-group">
										<textarea name="group_description[<?php echo $language['language_id']; ?>][description]" id="description<?php echo $language['language_id']; ?>"><?php echo isset($group_description[$language['language_id']]) ? $group_description[$language['language_id']]['description'] : ''; ?></textarea>
									</div>
								</div>
							<?php } ?>
							</div>
						</div>
					</div>

					<div class="box box-info">
						<div class="box-header with-border"><h3 class="box-title">SEO</h3></div>
						<div class="box-body">
							<ul class="nav nav-tabs <?php echo count($languages)==1 ? 'hide':''; ?>">
								<?php foreach ($languages as $key=> $language) { ?>
									<li class="<?php echo $language['language_id'] == $this->config->get('config_language_id') ? 'active':'';?>"><a data-toggle="tab" aria-expanded="true" href="#seo<?php echo $language['language_id']; ?>"><img <?php echo count($languages) ==1 ? 'class="hide"':''; ?> src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
								<?php } ?>
							</ul>
							<div class="tab-content">
							<?php foreach ($languages as $language) { ?>
							<div class="tab-pane <?php echo $language['language_id'] == $this->config->get('config_language_id') ? 'active':'';?>" id="seo<?php echo $language['language_id']; ?>">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<input placeholder="SEO title" type="text" class="form-control" name="group_description[<?php echo $language['language_id']; ?>][meta_title]" value="<?php echo isset($group_description[$language['language_id']]) ? $group_description[$language['language_id']]['meta_title'] : ''; ?>" onKeyDown="textCounter(this,70);"
										onKeyUp="textCounter(this,'titleseo',70)"/>
										<span class="small"><?php echo $text_remain;?> <input style="font-weight:bold;border:none" disabled size="2" id="titleseo" value="70" /> <?php echo $text_characters;?></span>
									</div>
									<div class="form-group">
										<input placeholder="Meta keyword" type="text" class="form-control" name="group_description[<?php echo $language['language_id']; ?>][meta_keyword]" value="<?php echo isset($group_description[$language['language_id']]) ? $group_description[$language['language_id']]['meta_keyword'] : ''; ?>" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<textarea placeholder="Meta Description" class="form-control"  name="group_description[<?php echo $language['language_id']; ?>][meta_description]" cols="45" rows="5"><?php echo isset($group_description[$language['language_id']]) ? $group_description[$language['language_id']]['meta_description'] : ''; ?></textarea>
									</div>
								</div>
							</div>
							</div>
							<?php } ?>
							</div>
							<div class="row">
								<div class="col-md-9">
									<div class="form-group">
										<input type="text" placeholder="<?php echo $entry_keyword; ?>" class="form-control" name="keyword" value="<?php echo $keyword; ?>" />
									</div>
								</div>
								<div class="col-md-3">
									<input type="text" class="form-control" name="robots" value="<?php echo $robots; ?>" placeholder="robots tag" />
								</div>
							</div>

						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="box box-info">
						<div class="box-body">
							<div class="form-group">
								<label class="control-label"><?php echo $entry_image; ?></label>
								<div class="image">
									<a class="pull-right" onclick="$('#thumb').attr('src', '<?php echo $no_image; ?>'); $('#image').attr('value', '');"><?php echo $text_clear; ?></a>
									<a onclick="image_upload('image', 'thumb');"><img src="<?php echo $thumb; ?>" alt="" id="thumb" /></a>
									<input type="hidden" name="image" value="<?php echo $image; ?>" id="image" />
								</div>
							</div>
							<div class="form-group">
								<label class="control-form" for="parent_id"><?php echo $entry_parent; ?><span class="help">Autocomplete</span></label>
								<input type="text" name="path" value="<?php echo $path; ?>" size="38" class="form-control" placeholder="type name" />
               					 <input type="hidden" name="parent_id" value="<?php echo $parent_id; ?>" />
							</div>
							<div class="form-group">
								<label class="control-label"><?php echo $entry_sort_order; ?></label>
								<input type="text" name="sort_order" class="form-control" value="<?php echo $sort_order; ?>" size="3" />
							</div>
							<div class="form-group">
								<label class="control-label">Code youtube</label>
								<input type="text" name="youtube" class="form-control" value="<?php echo $youtube; ?>" size="3" />
							</div>
							<div class="form-group">
								<label class="control-label">Thời gian làm bài (đơn vị phút)</label>
								<input type="text" name="time_test" class="form-control" value="<?php echo $time_test; ?>" size="3" />
							</div>
							<div class="form-group">
								<label class="control-label" for="status">
									<?php echo $entry_status ?>
								</label>
								<label class="switch pull-right">
									<input type="checkbox" name="status" <?php echo $status ? 'checked':''; ?> value="1" />
									<span class="slider round"></span>
								</label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
</div>
<script type="text/javascript" src="view/javascript/ckeditor/ckeditor.js"></script>
<script type="text/javascript"><!--
	<?php foreach ($languages as $language) { ?>
		CKEDITOR.replace('description<?php echo $language['language_id']; ?>', {
			filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
			filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
		});
	<?php } ?>
//--></script>
<script type="text/javascript"><!--
	$('input[name=\'path\']').autocomplete({
		delay: 500,
		source: function(request, response) {
			$.ajax({
				url: 'index.php?route=catalog/group/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
				dataType: 'json',
				success: function(json) {
					json.unshift({
						'group_id':  0,
						'name':  '<?php echo $text_none; ?>'
					});

					response($.map(json, function(item) {
						return {
							label: item.name,
							value: item.group_id
						}
					}));
				}
			});
		},
		select: function(event, ui) {
			$('input[name=\'path\']').val(ui.item.label);
			$('input[name=\'parent_id\']').val(ui.item.value);

			return false;
		},
		focus: function(event, ui) {
			return false;
		}
	});
//--></script>
<script type="text/javascript"><!--
	function image_upload(field, thumb) {
		$('#dialog').remove();

		$('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');

		$('#dialog').dialog({
			title: '<?php echo $text_image_manager; ?>',
			close: function (event, ui) {
				if ($('#' + field).attr('value')) {
					$.ajax({
						url: 'index.php?route=common/filemanager/image&token=<?php echo $token; ?>&image=' + encodeURIComponent($('#' + field).val()),
						dataType: 'text',
						success: function(data) {
							$('#' + thumb).replaceWith('<img src="' + data + '" alt="" id="' + thumb + '" />');
						}
					});
				}
			},
			bgiframe: false,
			width: 960,
			height: 550,
			resizable: false,
			modal: false,
			dialogClass: 'dlg'
		});
	};
//--></script>
<script type="text/javascript"><!--
	$('#tabs a').tabs();
	$('#languages a').tabs();
//--></script>
<style type="text/css">
	.markup{
		background: #ddd;
		padding: 3px 5px;
		margin-right: 5px;
		border-radius: 5px;
		position: relative;
	}
	.markup:hover .fa{
		display: block;
	}
	.markup .fa{
		position: absolute;
		display: none;
		right: -5px;
		top: -5px;
		cursor: pointer;
	}
</style>
<?php echo $footer; ?>
