<?php echo $header; ?>
<div class="content-wrapper">
	<section class="content-header">
		<h1><?php echo $heading_title; ?><small>(<?php echo $text_list; ?>)</small></h1>
		<ol class="breadcrumb">
			<?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
				<li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
			<?php $k++; } ?>
		</ol>
	</section>
	<?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
    <?php if ($success) { ?>
        <div class="success"><?php echo $success; ?></div>
    <?php } ?>
	<div class="content">
		<div id="fake"></div>
		<div class="box box-default">
		<div class="box-header" id="toolbar">
            <div class="pull-right">
                <a href="<?php echo $insert; ?>" class="btn btn-primary"><i class="fa fa-file-text" aria-hidden="true"></i> <?php echo $button_insert; ?></a>
            </div>
            <div class="pull-left">
                    <a onclick="$('#form').attr('action', '<?php echo $copy; ?>'); $('#form').submit();" class="btn btn-default btn-sm"><i class="fa fa-files-o" aria-hidden="true"></i> <?php echo $button_copy; ?>
                    </a>
                    <a onclick="$('form').submit();" class="btn btn-default btn-sm"><i class="fa fa-trash" aria-hidden="true"></i> <?php echo $button_delete; ?></a>
            </div>
		</div>
		<div class="box-body">
            <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                <table class="table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <td width="1" style="text-align: center;">
								<input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" />
							</td>
                            <td class="center hidden-xs"><?php echo $column_image; ?></td>
                            <td class="left"><?php if ($sort == 'pd.name') { ?>
                                    <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                                <?php } else { ?>
                                    <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                                <?php } ?></td>
                            <td>Nhóm</td>
                            <td class="left"><?php if ($sort == 'p.status') { ?>
                                    <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                                <?php } else { ?>
                                    <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                                <?php } ?></td>
                            <td class="text-center"><?php echo $column_action; ?></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="filter">
                            <td></td>
                            <td></td>
                            <td>
								<div class="input-group input-group-sm">
                                    <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" class="form-control" />
                                </div>
							</td>
                            <td>
								<div class="input-group input-group-sm">
									<select name="filter_group" class="form-control">
                                        <option value="*">Mọi bài test</option>
                                        <?php foreach ($groups as $group){ ?>
                                        <?php if(isset($filter_group) && $group['group_id']==$filter_group){ ?>
                                            <option selected value="<?php echo $group['group_id']?>"><?php echo $group['name']?></option>
                                        <?php }else { ?>
                                            <option value="<?php echo $group['group_id']?>"><?php echo $group['name']?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
								</div>
                            </td>
                            <td>Thứ tự</td>
                            <td>
                                <div class="input-group input-group-sm">
                                    <select name="filter_status" class="form-control">
                                        <option value="*"></option>
                                        <?php if ($filter_status) { ?>
                                            <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                        <?php } else { ?>
                                            <option value="1"><?php echo $text_enabled; ?></option>
                                        <?php } ?>
                                        <?php if (!is_null($filter_status) && !$filter_status) { ?>
                                            <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                        <?php } else { ?>
                                            <option value="0"><?php echo $text_disabled; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </td>
                            <td align="text-center"><a onclick="filter();" class="btn btn-block btn-default btn-s"><?php echo $button_filter; ?></a></td>
                        </tr>
                        <?php if ($quizs) { ?>
                            <?php foreach ($quizs as $quiz) { ?>
                                <tr>
                                    <td style="text-align: center;"><?php if ($quiz['selected']) { ?>
                                            <input type="checkbox" name="selected[]" value="<?php echo $quiz['quiz_id']; ?>" checked="checked" />
                                        <?php } else { ?>
                                            <input type="checkbox" name="selected[]" value="<?php echo $quiz['quiz_id']; ?>" />
                                        <?php } ?></td>
                                    <td class="center hidden-xs"><img src="<?php echo $quiz['image']; ?>" alt="<?php echo $quiz['name']; ?>" style="padding: 1px; border: 1px solid #DDDDDD;" /></td>
                                    <td class="left"><a href="<?php echo $quiz['href']; ?>"><?php echo $quiz['name']; ?></a></td>
                                    <td class="left"><?php echo $quiz['name_group']; ?></td>
                                    <td class="left"><?php echo $quiz['sort_order']; ?></td>
                                    <td class="left text-center"><a class="columnstatus" id="status-<?php echo $quiz['quiz_id']; ?>"><?php echo $quiz['status']; ?></a></td>
                                    <td class="text-center">
                                        <?php foreach ($quiz['action'] as $action) { ?>
                                            [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        <?php } else { ?>
                            <tr>
                                <td class="center" colspan="5"><?php echo $text_no_results; ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </form>
            <div class="pagination"><?php echo $pagination; ?></div>
        </div>
        </div>
	</div>
</div>
<script type="text/javascript"><!--
function filter() {
        url = 'index.php?route=catalog/quiz&token=<?php echo $token; ?>';

        var filter_name = $('input[name=\'filter_name\']').val();

        if (filter_name) {
            url += '&filter_name=' + encodeURIComponent(filter_name);
        }

        var filter_status = $('select[name=\'filter_status\']').val();

        if (filter_status != '*') {
            url += '&filter_status=' + encodeURIComponent(filter_status);
        }

        var filter_group = $('select[name=\'filter_group\']').val();

        if (filter_group != '*') {
            url += '&filter_group=' + encodeURIComponent(filter_group);
        }

        location = url;
    }
//--></script>
<script type="text/javascript"><!--
    $('#form input').keydown(function (e) {
        if (e.keyCode == 13) {
            filter();
        }
    });
//--></script>
<script type="text/javascript"><!--
    $('input[name=\'filter_name\']').autocomplete({
        delay: 500,
        source: function (request, response) {
            $.ajax({
                url: 'index.php?route=catalog/quiz/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request.term),
                dataType: 'json',
                success: function (json) {
                    response($.map(json, function (item) {
                        return {
                            label: item.name,
                            value: item.quiz_id
                        }
                    }));
                }
            });
        },
        select: function (event, ui) {
            $('input[name=\'filter_name\']').val(ui.item.label);

            return false;
        },
        focus: function (event, ui) {
            return false;
        }
    });

    $('input[name=\'filter_model\']').autocomplete({
        delay: 500,
        source: function (request, response) {
            $.ajax({
                url: 'index.php?route=catalog/quiz/autocomplete&token=<?php echo $token; ?>&filter_model=' + encodeURIComponent(request.term),
                dataType: 'json',
                success: function (json) {
                    response($.map(json, function (item) {
                        return {
                            label: item.model,
                            value: item.quiz_id
                        }
                    }));
                }
            });
        },
        select: function (event, ui) {
            $('input[name=\'filter_model\']').val(ui.item.label);

            return false;
        },
        focus: function (event, ui) {
            return false;
        }
    });
//--></script>
<script type="text/javascript"><!--
				$('.columnstatus').click(function() {
					var object_id=$(this).attr('id');
					$.ajax({
						url: 'index.php?route=catalog/quiz/updatestatus&token=<?php echo $token; ?>',
						type: 'get',
						data: {object_id:object_id},
						dataType: 'html',
						success: function(html) {
							if(html!=''){
								$('#'+object_id).html(html);
							}
						}
					});
				});
				//--></script>
<?php echo $footer; ?>
