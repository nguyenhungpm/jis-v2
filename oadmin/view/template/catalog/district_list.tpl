<?php echo $header; ?>
<div class="content-wrapper" id="content">
  <section class="content-header">
    <h1><?php echo $heading_title; ?></h1>
    <ol class="breadcrumb">
      <?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
        <li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
      <?php $k++; } ?>
    </ol>
  </section>
  <?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box box-default">
    <div class="box-header">
      <div class="pull-right">
        <a onclick="location = '<?php echo $insert; ?>'" class="btn btn-primary btn-sm"><span><i class="fa fa-plus"></i> <?php echo $button_insert; ?></span></a>
      </div>
      <div class="pull-left">
        <a onclick="$('#form').submit();" class="btn btn-default btn-sm"><span><i class="fa fa-trash"></i> <?php echo $button_delete; ?></span></a></div>
    </div>
    <div class="box box-body">
      <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="list">
          <thead>
            <tr>
              <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
              <td class="left"><?php echo $column_name; ?></td>
              <td class="right"><?php echo 'Thứ tự'; ?></td>
              <td class="right"><?php echo $column_action; ?></td>
            </tr>
          </thead>
          <tbody>
            <?php if ($districts) { ?>
            <?php foreach ($districts as $district) { ?>
            <tr>
              <td style="text-align: center;"><?php if ($district['selected']) { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $district['district_id']; ?>" checked="checked" />
                <?php } else { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $district['district_id']; ?>" />
                <?php } ?></td>
              <td class="left"><?php echo $district['name']; ?></td>
              <td class="right"><?php echo $district['sort_order']; ?></td>
              <td class="right"><?php foreach ($district['action'] as $action) { ?>
                [ <a href="<?php echo $district['view']; ?>">Xem</a> ]
                [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                <?php } ?></td>
            </tr>
            <?php } ?>
            <?php } else { ?>
            <tr>
              <td class="center" colspan="4"><?php echo $text_no_results; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </form>
    </div>
  </div>
<?php echo $footer; ?>