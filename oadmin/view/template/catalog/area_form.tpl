<?php echo $header; ?>
<div class="content-wrapper" id="content">
  <section class="content-header">
    <h1><?php echo $heading_title; ?></h1>
    <ol class="breadcrumb">
      <?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
        <li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
      <?php $k++; } ?>
    </ol>
  </section>
  <?php if (isset($success)) { ?>
    <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="content">
  
    <div class="box box-default">
      <div class="box-header">
        <div class="pull-right">
          <button type="button" onclick="$('#form').submit();" class="btn btn-primary"><i class="fa fa-check" aria-hidden="true"></i> <?php echo $button_save; ?></button>
        </div>
        <div class="pull-left">
          <a onclick="location = '<?php echo $cancel; ?>';"  class="btn btn-default btn-sm"><i class="fa fa-reply"></i> <?php echo $button_cancel; ?></a>
        </div>
    </div>
    <div class="box box-body">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <div id="tab-general">
          <div id="languages" class="htabs" style="display:none;">
            <?php foreach ($languages as $language) { ?>
            <a href="#language<?php echo $language['language_id']; ?>"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a>
            <?php } ?>
          </div>
          <?php foreach ($languages as $language) { ?>
          <div id="language<?php echo $language['language_id']; ?>">
            <table class="form">
              <tr>
                <td><span class="required">*</span> <?php echo $entry_name; ?></td>
                <td><input type="text" name="area_description[<?php echo $language['language_id']; ?>][name]" id="area_description<?php echo $language['language_id']; ?>_name" onkeyup="trim_name(this.id);" size="100" value="<?php echo isset($area_description[$language['language_id']]) ? $area_description[$language['language_id']]['name'] : ''; ?>" />
                  <?php if (isset($error_name[$language['language_id']])) { ?>
                  <span class="error"><?php echo $error_name[$language['language_id']]; ?></span>
                  <?php } ?></td>
              </tr>
              </table>
          </div>
          <?php } ?>
		  <table  class="form">
		  <tr>
              <td><?php echo $entry_sort_order; ?></td>
              <td><input type="text" name="sort_order" value="<?php echo $sort_order; ?>" size="10" /></td>
            </tr>
			<tr>
              <td><?php echo $entry_status; ?></td>
              <td><select name="status">
                  <?php if ($status) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
            </tr>
		</table>
        </div>
        </form>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
$('#tabs a').tabs(); 
$('#languages a').tabs();
//--></script>
<?php echo $footer; ?>