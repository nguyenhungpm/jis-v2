<?php echo $header; ?>
<div class="content-wrapper" id="content">
 <section class="content-header">
		<h1><?php echo $heading_title; ?><small>(<?php echo $text_list; ?>)</small></h1>
		<ol class="breadcrumb">
			<?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
				<li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
			<?php $k++; } ?>
		</ol>
	</section>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="content">
		<div class="box box-default">
			<div class="box-header">
				<div class="pull-right">
					<a href="<?php echo $insert; ?>" class="btn btn-primary"><i class="fa fa-file-text" aria-hidden="true"></i> <?php echo $button_insert; ?></a>
				</div>
				<div class="pull-left">
						<a onclick="$('form').submit();" class="btn btn-default btn-sm"><i class="fa fa-trash" aria-hidden="true"></i> <?php echo $button_delete; ?></a>
				</div>
		 </div>

    <div class="box-body">
      <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
       <table class="table table-bordered table-hover dataTable">
          <thead>
            <tr>
              <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
              <td class="left"><?php echo $column_name; ?></td>
              <td class="text-right hidden-xs"><?php echo $column_sort_order; ?></td>
			        <td class="text-right"><?php echo $column_status; ?></td>
              <td class="text-right"><?php echo $column_action; ?></td>
            </tr>
          </thead>
          <tbody>
            <?php if ($groups) { ?>
            <?php foreach ($groups as $group) { ?>
            <tr>
              <td style="text-align: center;"><?php if ($group['selected']) { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $group['group_id']; ?>" checked="checked" />
                <?php } else { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $group['group_id']; ?>" />
                <?php } ?></td>
              <td class="left"><?php echo $group['name']; ?></td>
              <td class="text-right hidden-xs"><?php echo $group['sort_order']; ?></td>
			  <td class="text-right"><a class="columnstatus" id="status-<?php echo $group['group_id']; ?>"><?php echo $group['status']; ?></a></td>
              <td class="text-right"><?php foreach ($group['action'] as $action) { ?>
                <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a>
                <?php } ?></td>
            </tr>
            <?php } ?>
            <?php } else { ?>
            <tr>
              <td class="center" colspan="5"><?php echo $text_no_results; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </form>
      <div class="pagination"><?php echo $pagination; ?></div>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
					$('.columnstatus').click(function() {
						var object_id=$(this).attr('id');
						$.ajax({
							url: 'index.php?route=catalog/group/updatestatus&token=<?php echo $token; ?>',
							type: 'get',
							data: {object_id:object_id},
							dataType: 'html',
							success: function(html) {
								if(html!=''){
									$('#'+object_id).html(html);
								}
							}
						});
					});
					//--></script>
<?php echo $footer; ?>
