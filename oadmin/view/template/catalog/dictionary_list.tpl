<?php echo $header; ?>
<div id="content" class="content-wrapper">
  <section class="content-header">
    <h1><?php echo $heading_title; ?><small>(Danh sách)</small></h1>
    <ol class="breadcrumb">
      <?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
        <li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
      <?php $k++; } ?>
    </ol>
  </section>
    <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
<div class="content">
  <div class="box box-primary">
    <div class="box-header">
      <div class="pull-left">
        <a onclick="$('#form').submit();" class="btn btn-default"><i class="fa fa-times"></i> <?php echo $button_delete; ?></a>
      </div>
      <div class="pull-right">
          <a href="#myModal" data-toggle="modal" title="Thêm nhóm tài liệu" class="btn btn-primary"><i class="fa fa-plus"></i> Thêm mới</a>
      </div>
    </div>
  <div class="box-body">
    <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
      <table class="list">
        <thead>
          <tr>
            <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
            <td>Từ khóa</td>
            <td>Dịch ngôn ngữ</td>
            <td>Tại bố cục</td>
            <td width="1" class="center">Thao tác</td>
          </tr>
        </thead>
        <tbody>
          <?php if ($dictionaries) { ?>
          <?php foreach ($dictionaries as $dictionary) { ?>
          <?php if(isset($this->request->get['dictionary_id']) && $dictionary['dictionary_id'] == $this->request->get['dictionary_id']){ ?>
          <tr id="name<?php echo $dictionary['dictionary_id']; ?>">
            <td style="text-align: center;"><input type="checkbox" name="selected[]" value="<?php echo $dictionary['dictionary_id']; ?>"/></td>
            <td class="left">
              <input class="form-control" placeholder="Từ khóa" type="text" name="keyword" value="<?php echo $dictionary['keyword']; ?>"/>
            </td>
            <td class="left">
              <?php foreach ($languages as $key=> $language) { ?>
              <div class="clearfix">
                [<img src="view/image/flags/<?php echo $language['image']; ?>" />] 
                <input style="width: calc(100% - 30px); float: right" class="form-control" placeholder="Tên nhóm(<?php echo $language['name']; ?>)" type="text" name="detail[<?php echo $language['language_id'];?>][name]" value="<?php echo !empty($dictionary['detail'][$language['language_id']]['name']) ? $dictionary['detail'][$language['language_id']]['name']:''; ?>"/>
              </div>
              <?php } ?>
            </td>
            <td class="text-center">
              <select name="layout_id" class="form-control">
                <?php foreach ($layouts as $layout) { ?>
                  <option value="<?php echo $layout['layout_id']; ?>" <?php echo $layout['layout_id'] == $dictionary['layout_id'] ? 'selected' : ''; ?>><?php echo $layout['name']; ?></option>
                <?php } ?>
              </select>
            </td>
            <td class="center"><a onclick="save('<?php echo $dictionary['dictionary_id']; ?>')" class="btn btn-sm btn-primary">Lưu</a></td>
          </tr>
          <?php }else{ ?>
          <tr id="name<?php echo $dictionary['dictionary_id']; ?>">
            <td style="text-align: center;"><input type="checkbox" name="selected[]" value="<?php echo $dictionary['dictionary_id']; ?>"/></td>
            <td><?php echo $dictionary['keyword']; ?></td>
            <td class="left">
              <?php foreach ($languages as $key=> $language) { ?>
              [<img src="view/image/flags/<?php echo $language['image']; ?>" />] <?php echo !empty($dictionary['detail'][$language['language_id']]['name']) ? $dictionary['detail'][$language['language_id']]['name']:''; ?> <br />
              <?php } ?>
            </td>
            <td><?php echo $dictionary['layout_id']; ?></td>
            <td class="center"><a href="<?php echo $dictionary['edit']; ?>#name<?php echo $dictionary['dictionary_id']; ?>">Sửa</a></td>
          </tr>
              <?php } ?>
          <?php } ?>
          <?php } else { ?>
          <tr>
            <td class="center" colspan="5"><?php echo $text_no_results; ?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </form>
  <div class="pagination"><?php echo $pagination; ?></div>
  </div>
  </div>
</div>
<div id="myModal" class="modal fade">
  <div class="modal-dialog form-sale">
        <div class="modal-content">
            <div class="modal-header">
        <button type="btn btn-primary btn-sm" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Thêm từ điển</h4>
      </div>
      <div class="modal-body">
      <div class="tab-content">
      <form method="POST" action="" name="form-dictionary" id="form-dictionary">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
              <label class="control-label text-left">Từ khóa</label>
              <input class="form-control" type="text" name="keyword"/>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
              <label class="control-label text-left">Tại bố cục</label>
              <select name="layout_id" class="form-control">
                <?php foreach ($layouts as $layout) { ?>
                  <option value="<?php echo $layout['layout_id']; ?>" <?php echo $layout['layout_id'] == $module['layout_id'] ? 'selected' : ''; ?>><?php echo $layout['name']; ?></option>
                <?php } ?>
              </select>
              </div>
            </div>
          </div>
          <?php foreach ($languages as $key=> $language) { ?>
          <div class="form-group required">
            <label class="control-label text-left">Dịch ngôn ngữ </label> <img src="view/image/flags/<?php echo $language['image']; ?>" />
            <input class="form-control" type="text" name="detail[<?php echo $language['language_id'];?>][name]"/>
          </div>
          <?php } ?>
          </form>
        </div>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
          <button type="button" id="submit" onclick="submit_form();" class="btn btn-primary">Lưu</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$("#myModal").draggable({
  handle: ".modal-header"
});
function submit_form(){
  $.ajax({
    url: 'index.php?route=catalog/dictionary/insert&token=<?php echo $token; ?>',
    type: 'post',
    dataType: 'json',
    data : $("#form-dictionary").serialize(),
    complete: function() {
      $('#submit').button('reset');
    },
    success: function(json) {
      var href = 'index.php?route=catalog/dictionary&token=<?php echo $token; ?>';
      <?php if(isset($page) && !empty($page)){ ?>
      href += '&page=<?php echo $page; ?>';
      <?php } ?>
      window.location = href;
    },
     error: function(e) {
        console.log(e);
     }

  });
}
function save(id){
  var url = 'index.php?route=catalog/dictionary/edit&token=<?php echo $token; ?>&dictionary_id=' +  id;
  <?php if(isset($page) && !empty($page)){ ?>
  url += '&page=<?php echo $page; ?>';
  <?php } ?>
  $.ajax({
    url: url,
    type: 'POST',
    data : $("#form").serialize(),
    dataType: 'json',
    success: function(json) {
      var href = 'index.php?route=catalog/dictionary&token=<?php echo $token; ?>';
      <?php if(isset($page) && !empty($page)){ ?>
      href += '&page=<?php echo $page; ?>';
      <?php } ?>
      window.location = href;
    }
  });
}
</script>
<?php echo $footer; ?>
