<?php echo $header; ?>
<div class="content-wrapper" id="content">
	<section class="content-header">
		<h1><?php echo $heading_title; ?><small>(<?php echo $text_page_details; ?>)</small></h1>
		<ol class="breadcrumb">
			<?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
			<li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
			<?php $k++; } ?>
		</ol>
	</section>
	<?php if ($error_warning) { ?>
	<div class="warning"><?php echo $error_warning; ?></div>
	<?php } ?>
	<div class="content">
		<div class="box box-default">
			<div class="box-header">
				<div class="pull-right">
					<a onclick="$('#form').submit();" class="btn btn-primary"><i class="fa fa-check" aria-hidden="true"></i> <?php echo $button_save; ?></a>
				</div>
				<div class="pull-left">
					<a href="<?php echo $cancel; ?>" class="btn btn-default btn-sm"><i class="fa fa-reply"></i> <?php echo $button_close; ?></a>
					<?php if(isset($admission_id) && !empty($admission_id)){ ?>
					<a class="btn btn-default btn-sm" onclick="$('#form_delete').submit();" ><i class="fa fa-trash" aria-hidden="true" ></i> <?php echo $button_delete; ?></a>
					<?php } ?>
				</div>
				<?php if(isset($admission_id) && !empty($admission_id)){ ?>
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form_delete">
					<input type="hidden" name="selected[]" value="<?php echo $admission_id; ?>"/>
				</form>
				<?php } ?>
			</div>
		</div>

		<div class="row">
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
				<div class="col-md-9" id="tab-general">
					<div class="box box-primary">
						<div class="box-body">
							<ul class="nav nav-tabs <?php echo count($languages)==1 ? 'hide':''; ?>">
								<?php foreach ($languages as $key=> $language) { ?>
									<li class="<?php echo $language['language_id'] == $this->config->get('config_language_id') ? 'active':'';?>"><a data-toggle="tab" aria-expanded="true" href="#language<?php echo $language['language_id']; ?>"><img <?php echo count($languages) ==1 ? 'class="hide"':''; ?> src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
								<?php } ?>
							</ul>
							<div class="tab-content">
							<?php foreach ($languages as $language) { ?>
							<div class="tab-pane <?php echo $language['language_id'] == $this->config->get('config_language_id') ? 'active':'';?>" id="language<?php echo $language['language_id']; ?>">
								<div class="form-group">
									<span class="required">*</span> <label class="control-label"><?php echo $entry_title; ?></label>
									<input class="form-control" type="text" name="admission_description[<?php echo $language['language_id']; ?>][title]" value="<?php echo isset($admission_description[$language['language_id']]) ? $admission_description[$language['language_id']]['title'] : ''; ?>" />
									<?php if (isset($error_title[$language['language_id']])) { ?>
									<span class="error"><?php echo $error_title[$language['language_id']]; ?></span>
									<?php } ?>
								</div>
								<div class="form-group">
									<span class="required">*</span> <label class="control-label"><?php echo $entry_description; ?></label>
									<textarea name="admission_description[<?php echo $language['language_id']; ?>][description]" id="description<?php echo $language['language_id']; ?>"><?php echo isset($admission_description[$language['language_id']]) ? $admission_description[$language['language_id']]['description'] : ''; ?></textarea>
									<?php if (isset($error_description[$language['language_id']])) { ?>
									<span class="error"><?php echo $error_description[$language['language_id']]; ?></span>
									<?php } ?>
								</div>
								<div class="form-group hide">
									<span class="required">*</span> <label class="control-label">Mở rộng</label>
									<textarea name="admission_description[<?php echo $language['language_id']; ?>][expand]" id="expand<?php echo $language['language_id']; ?>"><?php echo isset($admission_description[$language['language_id']]) ? $admission_description[$language['language_id']]['expand'] : ''; ?></textarea>
									<?php if (isset($error_description[$language['language_id']])) { ?>
									<span class="error"><?php echo $error_description[$language['language_id']]; ?></span>
									<?php } ?>
								</div>
							</div>
							<?php } ?>
							</div>
						</div>
					</div>

					<div class="box box-info">
						<div class="box-header with-border">
							<h3 class="box-title">SEO</h3>
						</div>
						<div class="box-body">
							<ul class="nav nav-tabs <?php echo count($languages)==1 ? 'hide':''; ?>">
								<?php foreach ($languages as $key=> $language) { ?>
									<li class="<?php echo $language['language_id'] == $this->config->get('config_language_id') ? 'active':'';?>"><a data-toggle="tab" aria-expanded="true" href="#seo<?php echo $language['language_id']; ?>"><img <?php echo count($languages) ==1 ? 'class="hide"':''; ?> src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
								<?php } ?>
							</ul>
							<div class="tab-content">
							<?php foreach ($languages as $language) { ?>
							<div class="tab-pane <?php echo $language['language_id'] == $this->config->get('config_language_id') ? 'active':'';?>" id="seo<?php echo $language['language_id']; ?>">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label">Meta Title</label>
											<input placeholder="Meta title" class="form-control" onKeyDown="textCounter(this, 'titleseo', 70);"
											onKeyUp="textCounter(this,'titleseo',70)" name="admission_description[<?php echo $language['language_id']; ?>][meta_title]" value="<?php echo isset($admission_description[$language['language_id']]) ? $admission_description[$language['language_id']]['meta_title'] : ''; ?>" />
											<label class="control-label"><span class="small help"><?php echo $text_remain; ?> <input style="font-weight:bold;border:none" disabled size="3" id="titleseo" value="70" /> <?php echo $text_characters; ?></span></label>

										</div>
										<div class="form-group">
											<label class="control-label">Meta Keyword</label>
											<input placeholder="Meta keyword" class="form-control" name="admission_description[<?php echo $language['language_id']; ?>][meta_keyword]" value="<?php echo isset($admission_description[$language['language_id']]) ? $admission_description[$language['language_id']]['meta_keyword'] : ''; ?>"/>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label">Meta description</label>
											<textarea placeholder="Meta Description" class="form-control" onKeyDown="textCounter(this,'descseo',160);"
											onKeyUp="textCounter(this,'descseo',160)" name="admission_description[<?php echo $language['language_id']; ?>][meta_description]" rows="4"><?php echo isset($admission_description[$language['language_id']]) ? $admission_description[$language['language_id']]['meta_description'] : ''; ?></textarea>
											<label class="control-label"><span class="small help"><?php echo $text_remain; ?> <input style="font-weight:bold;border:none" disabled size="3" id="descseo" value="160" /> <?php echo $text_characters; ?></span></label>
										</div>
									</div>
								</div>
							</div>
							<?php } ?>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label">SEO URL</label>
										<input type="text" name="keyword" value="<?php echo $keyword; ?>" class="form-control" placeholder="SEO URL" />
									</div>
								</div>
								<div class="col-md-12">
									<label class="control-label">Robots tag</label>
									<input type="text" class="form-control" name="robots" value="<?php echo $robots; ?>" placeholder="robots tag" />
								</div>
								<div class="col-md-3 hide">
									<label class="control-label">Mã video Youtube</label>
									<input type="text" class="form-control" name="code_youtube" value="<?php echo $code_youtube; ?>" placeholder="Code Youtube" />
								</div>
							</div>
						</div>
					</div>
				</div>

				<div id="tab-data" class="col-md-3">
					<div class="box box-info">
						<div class="box-body">
							<div class="form-group hide">
								<label class="control-label pull-left">Danh mục sản phẩm</label>
								<select name="category_id" class="form-control" >
									<option value="0">Chọn danh mục</option>
									<?php foreach($categories as $category){ ?>
									<option value="<?php echo $category['category_id']; ?>" <?php echo $category_id==$category['category_id'] ? 'selected':''; ?>><?php echo $category['name']; ?></option>
									<?php } ?>
								</select>
							</div>
							<div class="form-group clearfix">
								<label class="control-label pull-left"><?php echo $entry_status; ?></label>
								<label class="switch pull pull-right">
									<input type="checkbox" name="status" <?php echo $status ? 'checked':''; ?> value="1">
									<span class="slider round"></span>
								</label>
							</div>
							<div class="form-group clearfix">
								<label class="control-label pull-left">Hiển thị trang chủ</label>
								<label class="switch pull pull-right">
									<input type="checkbox" name="home" <?php echo $home ? 'checked':''; ?> value="1">
									<span class="slider round"></span>
								</label>
							</div>
							<div class="form-group">
								<label class="control-label"><?php echo $entry_sort_order; ?></label>
								<input type="text" name="sort_order" size="4" value="<?php echo $sort_order; ?>" />
							</div>
							<div class="form-group" style="display:none">
								<?php if ($bottom) { ?>
								<input type="checkbox" name="bottom" value="1" checked="checked" />
								<?php } else { ?>
								<input type="checkbox" name="bottom" value="1" />
								<?php } ?>
							</div>
						</div>
					</div>
					
					<div class="box box-default hide">
						<div class="box-header">
							<h3 class="box-title">Hightlight (ảnh 100x100)</h3>
							<a onclick="addImage();" class="btn btn-primary pull-right">Thêm</a>
						</div>
						<div class="box-body">
							<div class="row" id="list-images">
							<?php $image_row = 0;foreach ($admission_highlights as $admission_highlight) { ?>
							<div class="col-md-12 text-center" id="image-row<?php echo $image_row; ?>">
								<div class="image">
									<img src="<?php echo $admission_highlight['thumb']; ?>" alt="" id="thumb<?php echo $image_row; ?>" style="background: #222d32e0;"/>
									<input type="hidden" name="admission_highlight[<?php echo $image_row; ?>][image]" value="<?php echo $admission_highlight['image']; ?>" id="image<?php echo $image_row; ?>" />
										<br />
									<a onclick="image_upload('image<?php echo $image_row; ?>', 'thumb<?php echo $image_row; ?>');">Chọn file</a><br />
									<input class="text-center" type="text" name="admission_highlight[<?php echo $image_row; ?>][sort_order]" value="<?php echo $admission_highlight['sort_order']; ?>" placeholder="Thứ tự" size="8" /><br />
									<textarea class="text-left form-control" name="admission_highlight[<?php echo $image_row; ?>][description]" placeholder="Mô tả"><?php echo $admission_highlight['description']; ?></textarea>
									<a onclick="$('#image-row<?php echo $image_row; ?>').remove();" class="remove"><i class="fa fa-close"></i></a>
								</div>
							</div>
							<?php $image_row++;} ?>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript" src="view/javascript/ckeditor/ckeditor.js"></script>
<script type="text/javascript"><!--
	function image_upload(field, thumb) {
        $('#dialog').remove();

        $('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');

        $('#dialog').dialog({
            title: '<?php echo $text_image_manager; ?>',
            close: function (event, ui) {
                if ($('#' + field).attr('value')) {
                    $.ajax({
                        url: 'index.php?route=common/filemanager/image&token=<?php echo $token; ?>&image=' + encodeURIComponent($('#' + field).attr('value')),
                        dataType: 'text',
                        success: function (text) {
                            $('#' + thumb).replaceWith('<img src="' + text + '" alt="" id="' + thumb + '" style="background: #222d32e0;"/>');
						}
					});
				}
			},
            bgiframe: false,
            width: 960,
            height: 550,
            resizable: false,
            modal: false,
            dialogClass: 'dlg'
		});
	};
	var image_row = <?php echo $image_row; ?>;

	function addImage() {
		html = '<div class="col-md-12 text-center" id="image-row' + image_row + '">';
		html += '	<div class="image">';
		html += '		<img src="<?php echo $no_image; ?>" alt="" id="thumb' + image_row + '" style="background: #222d32e0;"/>';
		html += '		<input type="hidden" name="admission_highlight[' + image_row + '][image]" value="" id="image' + image_row + '" /><br />';
		html += '		<a onclick="image_upload(\'image' + image_row + '\', \'thumb' + image_row + '\');">Chọn file</a><br />';
		html += '		<input class="text-center" type="text" name="admission_highlight[' + image_row + '][sort_order]" value="" placeholder="Thứ tự" size="8" /><br />';
		html += '		<textarea class="text-left form-control" name="admission_highlight[' + image_row + '][description]" placeholder="Mô tả"></textarea>';
		html += '		<a onclick="$(\'#image-row' + image_row + '\').remove();" class="remove"><i class="fa fa-close"></i></a>';
		html += '	</div>';
		html += '</div>';

		$('#list-images').append(html);

		image_row++;
	}

	<?php foreach ($languages as $language) { ?>
		CKEDITOR.replace('description<?php echo $language['language_id']; ?>', {
			filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
			filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
			filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
			filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
			filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
			filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
		});
	<?php } ?>
	<?php foreach ($languages as $language) { ?>
		CKEDITOR.replace('expand<?php echo $language['language_id']; ?>', {
			filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
			filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
			filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
			filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
			filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
			filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
		});
	<?php } ?>
//--></script>
<script type="text/javascript"><!--
	$('#tabs a').tabs();
	$('#languages a').tabs();
//--></script>

<script type="text/javascript">
	function load_select(){
		$.ajax({
            dataType: 'json',
            type: 'POST',
            url: 'index.php?route=seo/seo/autocomplete&token=<?php echo $this->request->get['token']; ?>',
            data: { current : $('input[name=\'robots\']').val()},
            success: function (result) {
            	// console.log(result);
                var html = '<option value="">---Chọn robots---</option>';
                $.each(result, function(index, value){
                	html += '<option value="'+ value +'">'+ value +'</option>';
                });
                $('#filter_robots').html(html);
			}
		});
	}
	$(document).ready(function(){
        load_select();
    });
    function load_robots(e){
    	var text = '<span class="markup" data-value="'+ e.value +'"><i class="fa fa-minus-circle" aria-hidden="true"></i>'+ e.value +'</span>';
    	var data = $('input[name=\'robots\']').val() ? $('input[name=\'robots\']').val().split(","): [];
    	data.push(e.value);
    	var value = data.join(',');
    	$('input[name=\'robots\']').val(value)
    	// console.log(data);
    	$('#value_robots').append(text);
    	load_select();
    }
    $('#value_robots').delegate('.fa-minus-circle', 'click', function() {
		var value = $(this).parent().data('value');
		var data = $('input[name=\'robots\']').val() ? $('input[name=\'robots\']').val().split(","): [];
		var index = data.indexOf(value);
		if (index > -1) {
		    data.splice(index, 1);
		}
		var list = data.join(',');
    	$('input[name=\'robots\']').val(list);
		$(this).parent().remove();
		load_select();
	});
</script>
<style type="text/css">
	.markup{
		background: #ddd;
		padding: 3px 5px;
		margin-right: 5px;
		border-radius: 5px;
		position: relative;
	}
	.markup:hover .fa{
		display: block;
	}
	.markup .fa{
		position: absolute;
		display: none;
		right: -5px;
		top: -5px;
		cursor: pointer;
	}
</style>
<?php echo $footer; ?>
