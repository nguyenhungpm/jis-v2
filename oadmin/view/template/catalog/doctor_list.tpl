<?php echo $header; ?>
<div id="content" class="content-wrapper">
  <section class="content-header">
		<h1><?php echo $heading_title; ?><small>(Danh sách)</small></h1>
		<ol class="breadcrumb">
			<?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
				<li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
			<?php $k++; } ?>
		</ol>
	</section>
    <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>

<div class="content">
	<div class="box box-primary">
    <div class="box-header">
      <div class="pull-left">
        <a onclick="$('#form').submit();" class="btn btn-default"><i class="fa fa-times"></i> <?php echo $button_delete; ?></a>
      </div>
      <div class="pull-right">
          <a href="#myModal" data-toggle="modal" title="Thêm nội dung" class="btn btn-primary"><i class="fa fa-plus"></i> Thêm mới</a>
      </div>
    </div>
  <div class="box-body">
    <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
      <table class="list">
        <thead>
          <tr>
            <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
            <td class="left">Tên</td>
            <td class="hide">Chức danh</td>
            <td class="left">Mô tả</td>
            <td class="center">Thứ tự</td>
            <td class="left">Trạng thái</td>
            <td class="left">Hình ảnh</td>
            <td class="center">Thao tác</td>
          </tr>
        </thead>
        <tbody>
          <?php if ($doctors) { ?>
          <?php foreach ($doctors as $doctor) { ?>
  				<?php if(isset($this->request->get['doctor_id']) && $doctor['doctor_id'] == $this->request->get['doctor_id']){ ?>
          <tr id="name<?php echo $doctor['doctor_id']; ?>">
            <td style="text-align: center;"><input type="checkbox" name="selected[]" value="<?php echo $doctor['doctor_id']; ?>"/></td>
            <td class="left">
        			<?php foreach ($languages as $key=> $language) { ?>
        				<input class="form-control" placeholder="Hiệp hội (<?php echo $language['name']; ?>)" type="text" name="detail[<?php echo $language['language_id'];?>][name]" value="<?php echo !empty($doctor['detail'][$language['language_id']]['name']) ? $doctor['detail'][$language['language_id']]['name']:''; ?>"/>
        			<?php } ?>
            
                <div class="left hide">
				          <input class="form-control" placeholder="Chức danh" type="text" name="chucdanh" value="<?php echo $doctor['chucdanh']; ?>"/></div>
			       </td>
            <td class="text-center"><textarea class="form-control" placeholder="Mô tả ngắn" rows="5" name="description"><?php echo $doctor['description']; ?></textarea></td>
            <td class="text-center"><input class="form-control" placeholder="Thứ tự" type="text" name="sort_order" value="<?php echo $doctor['sort_order']; ?>"/></td>
            <td class="text-center">
              <select name="status" class="form-control">
                <option value="1" <?php echo $doctor['status']==1 ? 'selected':''; ?>>Bật</option>
                <option value="0" <?php echo $doctor['status']==0 ? 'selected':''; ?>>Tắt</option>
              </select>
            </td>
			<td class="center">
				<div class="image">
					<a onclick="image_upload('image<?php echo $doctor['doctor_id']; ?>', 'thumb<?php echo $doctor['doctor_id']; ?>');">
						<img src="<?php echo $doctor['thumb']; ?>" alt="" id="thumb<?php echo $doctor['doctor_id']; ?>" />
					</a>
					<input type="hidden" name="image" value="<?php echo $doctor['image']; ?>" id="image<?php echo $doctor['doctor_id']; ?>" />
				</div>
			</td>
            <td class="center"><a onclick="save('<?php echo $doctor['doctor_id']; ?>')" class="button">Lưu</a></td>
			</tr>
          <?php }else{ ?>
          <tr id="name<?php echo $doctor['doctor_id']; ?>">
            <td style="text-align: center;"><input type="checkbox" name="selected[]" value="<?php echo $doctor['doctor_id']; ?>"/></td>
            <td class="left"><strong><?php echo !empty($doctor['detail'][$this->config->get('config_language_id')]['name']) ? $doctor['detail'][$this->config->get('config_language_id')]['name']:''; ?></strong>
            <div class="hide"><?php echo $doctor['chucdanh']; ?></div></td>
            <td class=""><?php echo $doctor['description']; ?></td>
            <td class="center"><?php echo $doctor['sort_order']; ?></td>
            <td class="center"><?php echo $doctor['status'] == 1 ? 'Bật':'Tắt'; ?></td>
            <td class="center"><img src="<?php echo $doctor['thumb']; ?>" alt="" id="thumb<?php echo $doctor['doctor_id']; ?>" /></td>
            <td class="center"><a href="<?php echo $doctor['edit']; ?>#name<?php echo $doctor['doctor_id']; ?>">Sửa</a></td>
          </tr>
              <?php } ?>
          <?php } ?>
          <?php } else { ?>
          <tr>
            <td class="center" colspan="7"><?php echo $text_no_results; ?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </form>
  <div class="pagination"><?php echo $pagination; ?></div>
  </div>
  </div>
</div>
<div id="myModal" class="modal fade">
  <div class="modal-dialog form-sale">
        <div class="modal-content">
            <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Thêm nội dung</h4>
      </div>
      <div class="modal-body">
      <div class="tab-content">
      <form method="POST" action="" name="form-doctor" id="form-doctor">
    		  <?php foreach ($languages as $key=> $language) { ?>
    		  <div class="form-group required">
                <label class="control-label text-left">Tên</label>
                <input class="form-control" type="text" name="detail[<?php echo $language['language_id'];?>][name]"/>
              </div>
    			<div class="form-group hide">
    				<label class="control-label text-left">Chức danh</label>
    				<input class="form-control" type="text" name="chucdanh"/>
    			</div>
    			<div class="form-group">
    				<label class="control-label text-left">Mô tả ngắn</label>
    				<textarea class="form-control" rows="6" name="description"></textarea>
    			</div>
    			<?php } ?>
    			<div class="row">
    				<div class="col-md-6">
    					<div class="image">
    						<a class="pull-right" onclick="$('#thumb').attr('src', '<?php echo $no_image; ?>'); $('#image').attr('value', '');">Xóa</a>
    						<a onclick="image_upload('image', 'thumb');">
    							<img src="<?php echo $thumb; ?>" alt="" id="thumb" />
    						</a>
    						<input type="hidden" name="image" value="" id="image" />
    					</div>
    				</div>
    				<div class="col-md-6">
    				  <div class="form-group">
    					<label class="control-label text-left">Thứ tự</label>
    					<input class="form-control" type="text" name="sort_order"/>
    				  </div>
    				  <div class="form-group">
    					<label class="control-label text-left">Trạng thái</label>
    					<select name="status" class="form-control">
    					  <option value="1">Bật</option>
    					  <option value="0">Tắt</option>
    					</select>
    				  </div>
    				</div>
    			</div>
          </form>
        </div>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
          <button type="button" id="submit" onclick="submit_form()" data-dismiss="modal" class="btn btn-primary">Lưu</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
function image_upload(field, thumb) {

	$('#dialog').remove();

	$('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');

	$('#dialog').dialog({
		title: 'Quản lí ảnh',
		close: function (event, ui) {
			if ($('#' + field).attr('value')) {
				$.ajax({
					url: 'index.php?route=common/filemanager/image&token=<?php echo $token; ?>&image=' + encodeURIComponent($('#' + field).attr('value')),
					dataType: 'text',
					success: function(text) {
						$('#' + thumb).replaceWith('<img src="' + text + '" alt="" id="' + thumb + '" />');
					}
				});
			}
		},
		bgiframe: false,
		width: 1002,
		height: 600,
		resizable: false,
		modal: false
	});
};
/*
$('#form').submit(function(){
	if($('input[name="news_category"]').val() ==""){
		if (!confirm('Bạn chưa chọn danh mục tin tức! Bạn có chắc chắn bỏ qua?')) {
			return false;
		}
	}
});
*/
//--></script>
<script>
$("#myModal").draggable({
  handle: ".modal-header"
});
function submit_form(){
  $.ajax({
    url: 'index.php?route=catalog/doctor/insert&token=<?php echo $token; ?>',
    type: 'post',
    dataType: 'json',
    data : $("#form-doctor").serialize(),
    complete: function() {
      $('#submit').button('reset');
    },
    success: function(json) {
      var href = 'index.php?route=catalog/doctor&token=<?php echo $token; ?>';
      <?php if(isset($page) && !empty($page)){ ?>
      href += '&page=<?php echo $page; ?>';
      <?php } ?>
      window.location = href;
    }
  });
}
function save(id){
	var url = 'index.php?route=catalog/doctor/edit&token=<?php echo $token; ?>&doctor_id=' +  id;
	<?php if(isset($page) && !empty($page)){ ?>
	url += '&page=<?php echo $page; ?>';
	<?php } ?>
	$.ajax({
		url: url,
		type: 'POST',
    data : $("#form").serialize(),
		dataType: 'json',
		success: function(json) {
			var href = 'index.php?route=catalog/doctor&token=<?php echo $token; ?>';
			<?php if(isset($page) && !empty($page)){ ?>
			href += '&page=<?php echo $page; ?>';
			<?php } ?>
			window.location = href;
		}
	});
}
</script>
<?php echo $footer; ?>
