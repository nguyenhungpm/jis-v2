<?php echo $header; ?>
<div class="content-wrapper" id="content">
  <section class="content-header">
		<h1><?php echo $heading_title; ?><small>(Chi tiết)</small></h1>
		<ol class="breadcrumb">
			<?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
				<li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
			<?php $k++; } ?>
		</ol>
	</section>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="content">
	<div id="fake"></div>
  <div class="box">
    <div class="box-header" id="toolbar">
      <div class="pull-right">
        <a onclick="$('#form').submit();" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu</a>
        <a href="<?php echo $cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i> <?php echo $button_cancel; ?></a>
      </div>
    </div>
    </div>
    <div class="">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
      <div class="row">
        <div class="col-md-9">
          <div class="box box-danger">
            <div class="box-body">
							<ul class="nav nav-tabs <?php echo count($languages)==1 ? 'hide':''; ?>">
								<?php foreach ($languages as $key=> $language) { ?>
									<li class="<?php echo $language['language_id'] == $this->config->get('config_language_id') ? 'active':'';?>"><a data-toggle="tab" aria-expanded="true" href="#language<?php echo $language['language_id']; ?>"><img <?php echo count($languages) ==1 ? 'class="hide"':''; ?> src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
								<?php } ?>
							</ul>

              <div class="tab-content">
								<?php foreach ($languages as $key=> $language) { ?>
									<div class="tab-pane <?php echo $language['language_id'] == $this->config->get('config_language_id') ? 'active':'';?>" id="language<?php echo $language['language_id']; ?>">
                    <div class="form-group required">
											<label class="control-label"><?php echo $entry_title; ?></label>
											<input type="text" class="form-control" name="video_description[<?php echo $language['language_id']; ?>][title]" size="100" value="<?php echo isset($video_description[$language['language_id']]) ? $video_description[$language['language_id']]['title'] : ''; ?>">
											<?php if (isset($error_title[$language['language_id']])) { ?>
												<div class="text-danger"><?php echo $error_title[$language['language_id']]; ?></div>
											<?php } ?>
										</div>
                    
										<div class="form-group">
											<label class="control-label"><?php echo $entry_description; ?></label>
											<textarea name="video_description[<?php echo $language['language_id']; ?>][description]" id="description<?php echo $language['language_id']; ?>"><?php echo isset($video_description[$language['language_id']]) ? $video_description[$language['language_id']]['description'] : ''; ?></textarea>
										</div>
                  </div>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-3">
					<div class="box box-primary">
            <div class="box-header">
							<h3 class="box-title">Cấu hình video</h3>
						</div>
						<div class="box-body">
							<div class="form-group">
                <label class="control-label">Mã Youtube</label>
                <span class="help">Ví dụ:<b>6fSAb6ao18A</b></span>
								<input class="form-control" type="text" name="url" value="<?php echo $url_video; ?>" size="2" />
							</div>
              <div class="form-group">
								<label>Ngày đăng</label>
								<div class="input-group date">
									<div class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</div>
									<input type="text" name="date_add" value="<?php echo $date_add; ?>" class="form-control pull-right date" id="datepicker">
								</div>
								<!-- /.input group -->
							</div>
							<div class="form-group">
                <label class="control-label">Thứ tự</label>
								<input class="form-control" type="text" name="sort_order" value="<?php echo $sort_order; ?>" size="2" />
							</div>
							<div class="form-group">
                <label class="control-label">SEO URL</label>
								<input class="form-control" type="text" name="keyword" value="<?php echo $keyword; ?>" size="2" />
							</div>
              <div class="form-group clearfix">
								<label class="control-label pull-left">Trạng thái</label>
								<label class="switch pull-right">
								  <input type="checkbox" name="status" <?php echo $status ? 'checked':''; ?> value="1">
								  <span class="slider round"></span>
								</label>
							</div>
            </div>
          </div>
        </div>
      </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript"><!--
	$('.date').datepicker({format: 'yyyy-mm-dd'});
    $('.datetime').datetimepicker({
        dateFormat: 'yy-mm-dd',
        timeFormat: 'h:m'
	});
    $('.time').timepicker({timeFormat: 'h:m'});
//--></script>
<script type="text/javascript" src="view/javascript/ckeditor/ckeditor.js"></script>
<script type="text/javascript"><!--
<?php foreach ($languages as $language) { ?>
CKEDITOR.replace('description<?php echo $language['language_id']; ?>', {
	filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
});
<?php } ?>
//--></script>
<script type="text/javascript"><!--
$('#tabs a').tabs();
$('#languages a').tabs();
//--></script>
 <script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script> <script type="text/javascript"><!--
$('.date').datepicker({dateFormat: 'yy-mm-dd'});
$('.datetime').datetimepicker({
  dateFormat: 'yy-mm-dd',
  timeFormat: 'h:m'
});
$('.time').timepicker({timeFormat: 'h:m'});
//--></script>
<?php echo $footer; ?>
