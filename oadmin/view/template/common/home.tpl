<?php echo $header; ?>
<div class="content-wrapper">
	<?php if ($error_image) { ?>
		<div class="warning"><?php echo $error_image; ?></div>
	<?php } ?>
	<?php if ($error_image_cache) { ?>
		<div class="warning"><?php echo $error_image_cache; ?></div>
	<?php } ?>
	<?php if ($error_cache) { ?>
		<div class="warning"><?php echo $error_cache; ?></div>
	<?php } ?>
	<?php if ($error_logs) { ?>
		<div class="warning"><?php echo $error_logs; ?></div>
	<?php } ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
		<h1><?php echo $heading_title; ?></h1>
		<ol class="breadcrumb">
			<?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
				<?php echo $breadcrumb['separator']; ?><li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
			<?php $k++; } ?>
		</ol>
	</section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-lg-4 col-xs-6">
				<!-- small box -->
				<?php if($total_faqs>0) { ?>
				<div class="small-box bg-aqua">
						<a style="display:block;padding:5px 10px;font-size:16px;color:#fff" href="<?php echo $faq_link; ?>">Có <?php echo $total_faqs; ?> câu hỏi mới</a>
				</div>
				<?php } ?>
				<?php if($total_orders>0) { ?>
				<div class="small-box bg-red">
						<a style="display:block;padding:5px 10px;font-size:16px;color:#fff" href="<?php echo $order_link; ?>">Có <?php echo $total_orders; ?> đơn đặt hàng mới</a>
				</div>
				<?php } ?>
			</div>
		</div>
		<!-- Main row -->
	</section>
</div>
<?php echo $footer; ?>
