<div id="footer"><?php echo $text_footer; ?></div>

<script>
    var minutesLabel = document.getElementById("minutes");
    var secondsLabel = document.getElementById("seconds");
    var totalSeconds = 0;
    setInterval(setTime, 1000);
    setInterval(renewToken, 5*60*1000);

    function renewToken() {
        var request = new XMLHttpRequest();
        request.open('POST', '<?php echo HTTPS_CATALOG; ?>index.php?route=common/home/renewToken', /* async = */ false);
        request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        request.send('&price_shipping=1');
        if (request.status == 200) {
            var json = JSON.parse(request.responseText);
            console.log(json);
            var time = new Date().toLocaleTimeString();
            document.getElementById('renewToken').innerHTML = '&nbsp;(Token đã được cập nhật lúc: '+ time +')';
        } else {
            console.log('Error: ' + request.statusText);
        }
    }

    function setTime() {
    ++totalSeconds;
    secondsLabel.innerHTML = pad(totalSeconds % 60);
    minutesLabel.innerHTML = pad(parseInt(totalSeconds / 60));
    }

    function pad(val) {
    var valString = val + "";
    if (valString.length < 2) {
        return "0" + valString;
    } else {
        return valString;
    }
    }
</script>
</body></html>