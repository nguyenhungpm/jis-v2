<aside class="main-sidebar">
    <section class="sidebar">
    <?php /*
		<form action="#" method="get" class="sidebar-form">
			<div class="input-group">
				<input type="text" name="q" class="form-control" placeholder="Search...">
				<span class="input-group-btn">
					<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
					</button>
				</span>
			</div>
		</form>
    */ ?>
		<ul id="menu" class="sidebar-menu" data-widget="tree">
			<li class="header">MAIN NAVIGATION</li>
			<?php foreach ($menus as $menu) { ?>
			<li class="<?php echo $menu['children'] ? 'treeview':''; ?>" id="<?php echo $menu['id']; ?>">
				<a href="<?php echo $menu['children'] ? '#' : $menu['href']; ?>"><?php echo isset($menu['icon']) ? '<i class="fa '.$menu['icon']. '"></i></i>' :'';?> <span><?php echo $menu['name']; ?></span>
					<?php if($menu['children']){ ?>
						<span class="pull-right-container"><i class="fa fa-angle-right pull-right"></i></span>
					<?php } ?>
				</a>
				<?php if($menu['children']){ ?>
				<ul class="treeview-menu">
					<?php foreach($menu['children'] as $children_1){ ?>
					<li class="<?php echo $children_1['children'] ? 'treeview':''; ?>">
						<a href="<?php echo $children_1['children'] ? '#':$children_1['href']; ?>"><i class="fa fa-circle-o"></i> <?php echo $children_1['name'];?>
							<?php if($children_1['children']){ ?>
								<span class="pull-right-container"><i class="fa fa-angle-right pull-right"></i></span>
							<?php } ?>
						</a>
						<?php if($children_1['children']){ ?>
							<ul class="treeview-menu">
								<?php foreach($children_1['children'] as $children_2){ ?>
									<li class="<?php echo $children_2['children'] ? 'treeview':''; ?>">
										<a href="<?php echo $children_2['href'];?>"><?php echo $children_2['name'];?></a>
									</li>
								<?php } ?>
							</ul>
						<?php } ?>
					</li>
					<?php } ?>
				</ul>
				<?php } ?>
			</li>
			<?php } ?>
		</ul>
	</section>
</aside>
