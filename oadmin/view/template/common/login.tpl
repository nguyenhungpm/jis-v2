<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $heading_title; ?></title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <style>
  body {
    background: #d2d6de;
    font-size:13px;
    font-family:Helvetica,Arial;
    margin:0;
    line-height: 1.42857143;
    padding:0;
    text-align:center;
    color:#666
    }
    a{color:#3c8dbc;text-decoration:none}
    a:hover{color:#72afd2}
    .clear{clear:both;}
    .left{float:left}
    label{font-size:12px;color:#888}
    .login-box{max-width:360px;margin:7% auto}
    .footer a{color:#666;text-decoration:none}
    .boxin{
        padding:20px;
        background-color:#fff;
    }
    .relative{position:relative}
    .absolute{position:absolute}
    .border-box{box-sizing:border-box}
    .u-icon,
    .p-icon{
        height:22px;
        width:22px;
        display:block;
        top:5px;
        right:5px;
        opacity:.35
    }
    .small{font-size:11px}
    .u-icon{
        background:url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABUAAAAWCAYAAAAvg9c4AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyRpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoTWFjaW50b3NoKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo2MzBGMDhDNzc2RDkxMUU3QTdCODk5REZGQTQyNjM0OSIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo2MzBGMDhDODc2RDkxMUU3QTdCODk5REZGQTQyNjM0OSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjYzMEYwOEM1NzZEOTExRTdBN0I4OTlERkZBNDI2MzQ5IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjYzMEYwOEM2NzZEOTExRTdBN0I4OTlERkZBNDI2MzQ5Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+d7kNtwAAAPRJREFUeNpiYMAPJIB4DhA/B+L/QPwFiJUYKACGQPwLahgy/gDErUDMQqqB7ED8EIuByPg0qYZmEDAQhvOxaWbCYag9kZa7kGIoN5GG8pJi6DMiDX1DiqE3iTT0IS0MPUlqCrhOIOb3k5P4ZYD4Hg4D9wExMy6NjEQYHgzEPkAsBsSvgHglEO9gGBYAn/dtgdgJiNWAWBRaHvyCps1bQHwAiA8B8V9ClnACcS0QvyUy73+Ellg8uAz0BOKfRBqGjkGuDUU3MJ9Mw9BxLczAeCoZCMO5IEMfUNnQx6C8z0/lFMULqmfeAbEAEL+ngoGCIPMAAgwAlHyTqmTjVKcAAAAASUVORK5CYII=") center center no-repeat;
    }
    .p-icon{
        background:url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAWCAYAAADJqhx8AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyRpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoTWFjaW50b3NoKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo2MzBGMDhDQjc2RDkxMUU3QTdCODk5REZGQTQyNjM0OSIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo2MzBGMDhDQzc2RDkxMUU3QTdCODk5REZGQTQyNjM0OSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjYzMEYwOEM5NzZEOTExRTdBN0I4OTlERkZBNDI2MzQ5IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjYzMEYwOENBNzZEOTExRTdBN0I4OTlERkZBNDI2MzQ5Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+IbycswAAAQpJREFUeNpiYsAOooH4LBD/h+IrQJzKQCTYjKTxFhBfR+IfBGJmfJpXQRUuB2JZJHFxIJ4BlduHS7MNVME2PBYsgarxwya5Aioph8cAbqiaA9gkH0IlCYH3QPwbxmFCkmAh0oBPyPqQDfhFZCwx43LBPyIN+IfsUpCzJwOxARArQcVuQRUwYtH8Hyl6jwPxZUYk0x4B8R8kg3ABUGD/RVYHMuAcA+lgN0gvLAz+kmHAH+RAxJW+04G4EIccCzFe+I8nbaB4ARd4BXMqLkDIAH5kp5JjwFMg/oBPAQuBVGiCJ4D/MRByHjTnMRAy4DMQGwPxHqggIwFDYcncBZZ+bJDKAlLwSyD2BQgwALScVFTs47VNAAAAAElFTkSuQmCC") center center no-repeat;
    }

    h1{font-weight:100;color:#111}
    #password,
    #username{
        width:100%;
        height: 34px;
        padding: 6px 12px;
        font-size: 12px;
        border:1px solid #d2d6de;
        outline:0;
    }
    input.input:active,
    input.input:hover,
    input.input:focus{
        border:1px solid #3c8dbc !important;
        outline:0;
    }
    button{float:right;background:#3c8dbc;border:1px solid #367fa9;height:34px;cursor:pointer;color:#fff;width:120px}
    button:hover{background:#367fa9}
    .p-container{border-top:1px solid #d2d6de;padding-top:20px;margin-top:20px;
        text-align:left;
    }
    .warning{
        padding:4px 10px;
        font-size: 90%;
        color: #c7254e;
        background-color: #f9f2f4;
        border-radius: 4px;
    }
    .error{color:red}
    .login_captcha label{
      cursor:pointer;
      text-align:left;
      padding:5px 0;
      display:block;
      font-weight:700
    }
    .login_captcha input{
      padding:5px 10px;
      height:30px;
      width:70%;
      border:1px solid #ddd;
      float:left;
    }
  </style>
</head>
<body>
<div class="login-box">
<h1><strong>Simple</strong>CMS</h1>
<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
    <div class="boxin">
        <?php if ($success) { ?>
            <div class="success"><?php echo $success; ?></div>
        <?php } ?>
        <?php if ($error_warning) { ?>
            <div class="warning"><?php echo $error_warning; ?></div>
        <?php } ?>
        <p>Đăng nhập vào màn hình điều khiển</p>
        <p class="relative">
            <span class="absolute u-icon"></span>
            <input type="text" name="username" value="<?php echo $username; ?>" id="username" class="border-box input" placeholder="<?php echo $entry_username; ?>">
        </p>
        <p class="relative">
            <span class="absolute p-icon"></span>
            <input class="border-box input" type="password" name="password" placeholder="<?php echo $entry_password; ?>" value="<?php echo $password; ?>" id="password">
        </p>
        <?php if($config_login_captcha == '1'){ ?>
        <p class="relative login_captcha">
            <label for="captcha"><?php echo $entry_captcha; ?></label>
            <input type='text' class="input" name='captcha' value='<?php echo $captcha; ?>' /> <img src="index.php?route=common/login/captcha" alt=""/>
            <?php if ($error_captcha) { ?>
            <div class='error'><?php echo $error_captcha; ?></div>
            <?php } ?>
        </p>
        <?php } ?>
        <p>
            <button type="submit" name="go" id="go"><?php echo $button_login; ?></button>
            <input type="checkbox" name="remember" class="input left" id="remember">
            <label class="left" for="remember">Nhớ đăng nhập</label>
            <div class="clear"></div>
        </p>
        <?php if ($forgotten) { ?>
        <p class="p-container">
                <a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
        </p>
        <?php } ?>
    </div>
	<?php if ($redirect) { ?>
        <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
	<?php } ?>
</form>
    <p class="small footer">
        &copy; 2012 - <?php echo date("Y");?> by DSCo.,Ltd V3.2019.01.08. All rights resvered.</a>
    </p>
</div>
<script>
document.getElementById("username").focus();
</script>
</body></html>
