<?php echo $header; ?>
<div class="content-wrapper">
  <section class="content-header">
    <h1><?php echo $heading_title; ?></h1>
    <ol class="breadcrumb">
      <?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
        <li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
      <?php $k++; } ?>
    </ol>
  </section> 
<?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="content">
  <div class="box box-default">
    <div class="box-header">
      <div class="pull-right">
	  <a href="<?php echo $clear; ?>" class="btn btn-primary"><i class="fa fa-eraser"></i> <?php echo $button_clear; ?></a>
	  </div>
    </div>
    <div class="box-body">
      <textarea wrap="off" style="width:100%; height: 300px; padding: 5px; border: 1px solid #CCCCCC; background: #FFFFFF; overflow: scroll;"><?php echo $log; ?></textarea>
    </div>
  </div>
  </div>
</div>
<?php echo $footer; ?>