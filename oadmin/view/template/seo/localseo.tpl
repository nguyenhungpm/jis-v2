<?php echo $header; ?>
<div class="content-wrapper">
  <section class="content-header">
    <h1><?php echo $heading_title; ?><small>(<?php echo $text_configuration; ?>)</small></h1>
    <ol class="breadcrumb">
      <?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
        <li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
      <?php $k++; } ?>
    </ol>
  </section>
  <div id="alert"></div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="content" id="content">
  <div class="box box-primary">
    <div class="box-header">
      <div class="pull-right">
        <a onclick="$('#form').submit();" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> <?php echo $button_save; ?></a>
      </div>
    </div>
    <div class="box-body">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="form">
              <td><?php echo $entry_status; ?></td>
              <td><div class="clearfix">
								<label class="switch">
								  <input type="checkbox" name="config_local" <?php echo $config_local ? 'checked':''; ?> value="1" />
								  <span class="slider round"></span>
								</label>
					</div></td>
            </tr>
			<tr>
              <td><?php echo $entry_type_seo; ?></td>
              <td><input type="text" name="config_type_seo" value="<?php echo $config_type_seo; ?>" size="30" /></td>
            </tr>
            <tr>
              <td><?php echo $entry_name_seo; ?></td>
              <td><input type="text" name="config_name_seo" value="<?php echo $config_name_seo; ?>" size="30" /></td>
            </tr>
            <tr>
              <td><?php echo $entry_street_seo; ?></td>
              <td><input type="text" name="config_street_seo" value="<?php echo $config_street_seo; ?>" size="30" /></td>
            </tr>
            <tr>
              <td><?php echo $entry_region_seo; ?></td>
              <td><input type="text" name="config_region_seo" value="<?php echo $config_region_seo; ?>" size="30" /></td>
            </tr>
            <tr>
              <td><?php echo $entry_locality_seo; ?></td>
              <td><input type="text" name="config_locality_seo" value="<?php echo $config_locality_seo; ?>" size="30" /></td>
            </tr>
            <tr>
              <td><?php echo $entry_postalcode_seo; ?></td>
              <td><input type="text" name="config_postalcode_seo" value="<?php echo $config_postalcode_seo; ?>" size="30" /></td>
            </tr>
          </table>
        </div>
      </form>
    </div>
  </div>
</div>
</div>
<?php echo $footer; ?>
