<?php echo $header; ?>
<div class="content-wrapper">
  <section class="content-header">
		<h1><?php echo $heading_title; ?><small>(Chi tiết)</small></h1>
		<ol class="breadcrumb">
			<?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
				<li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
			<?php $k++; } ?>
		</ol>
	</section>
  <div class="content">
    <div class="box box-default">
	     <div class="box-header">
        <div class="pull-right">
  		  <a href="<?php echo $cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i> <?php echo $button_cancel; ?></a>
  		  <a onclick="$('#form').submit();" class="btn btn-primary"><i class="fa fa-check" aria-hidden="true"></i> <?php echo $button_save; ?></a>
  	  </div>
    </div>
    <div class="box-body">
      <form action="<?php echo $action; ?>" method="post" id="form">
        <table class="form">
		<tr>
			<td><?php echo $entry_exceptions; ?></td>
			<td><textarea name="exceptions" style="width:380px;height:200px;padding:5px"><?php echo $exceptions; ?></textarea>
		</td>
		</tr>
        </table>
      </form>
    </div>
  </div>
</div>
<?php echo $footer; ?>
