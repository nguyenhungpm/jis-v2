<?php echo $header; ?>
<div class="content-wrapper">
  <section class="content-header">
    <h1><?php echo $heading_title; ?><small>(<?php echo $text_configuration; ?>)</small></h1>
    <ol class="breadcrumb">
      <?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
        <li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
      <?php $k++; } ?>
    </ol>
  </section>
  <div id="alert"></div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="content" id="content">
  <div class="box box-primary">
    <div class="box-header">
      <div class="pull-right">
        <a onclick="$('#form').submit();" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> <?php echo $button_save; ?></a>
      </div>
    </div>
    <div class="box-body">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
          <table class="form">
            <tr>
              <td><?php echo $entry_seo_url; ?></td>
              <td><div class="form-group clearfix">
								<label class="switch">
								  <input type="checkbox" name="config_seo_url" <?php echo $config_seo_url ? 'checked':''; ?> value="1" />
								  <span class="slider round"></span>
								</label>
					</div></td>
            </tr>
            <tr>
              <td><span class="required">*</span> <?php echo $entry_title; ?></td>
              <td>
        			<?php foreach ($languages as $language) { ?>
                    <?php $id=$language['language_id']; ?>
        			  <?php if(count($languages) > 1){ ?><img <?php echo count($languages) ==1 ? 'class="hide"':''; ?> src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><?php } ?><input class="form-control" type="text" name="config_title[<?php echo $id; ?>]" value="<?php echo $config_title[$id]; ?>" size="40" />
                        <?php if ($error_title) { ?>
                        <span class="error"><?php echo $error_title; ?></span>
                        <?php } ?>
        			<?php } ?>
				      </td>
            </tr>
            <tr>
              <td><?php echo $entry_meta_description; ?></td>
              <td>
      		    <?php foreach ($languages as $language) { ?>
                  <?php $id=$language['language_id']; ?>
      			         <?php if(count($languages) > 1){ ?><img <?php echo count($languages) ==1 ? 'class="hide"':''; ?> src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><?php } ?><textarea class="form-control" name="config_meta_description[<?php echo $id; ?>]" cols="40" rows="5"><?php echo $config_meta_description[$id]; ?></textarea>
      			   <?php } ?>
      			  </td>
            </tr>
            <tr>
              <td><?php echo $entry_meta_keywords; ?></td>
              <td>
      		    <?php foreach ($languages as $language) { ?>
                  <?php $id=$language['language_id']; ?>
      			         <?php if(count($languages) > 1){ ?><img <?php echo count($languages) ==1 ? 'class="hide"':''; ?> src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><?php } ?><textarea class="form-control" name="config_meta_keyword[<?php echo $id; ?>]" cols="40" rows="5"><?php echo $config_meta_keyword[$id]; ?></textarea>
      			   <?php } ?>
      			  </td>
            </tr>
            <tr>
                <td>XML Sitemap</td>
                <td>
                  <label class="switch">
                  <input type="checkbox" name="google_sitemap_status" <?php echo $google_sitemap_status ? 'checked':''; ?> value="1" />
                  <span class="slider round"></span>
                  </label>
                  <br />
                  <textarea class="form-control" class="form-control" rows="2" disabled=""><?php echo $data_feed2; ?></textarea>
                  <?php echo $text_or; ?>
                  <textarea class="form-control" class="form-control" rows="2" disabled=""><?php echo $data_feed; ?></textarea>
              </td>

            </tr>
            <tr>
              <td><?php echo $entry_google_analytics; ?>
              </td>
              <td><input class="form-control" name="config_google_analytics" value="<?php echo $config_google_analytics; ?>" size="30"/></td>
            </tr>
            <tr>
              <td>
                  <?php echo $entry_google_searchconsole; ?>
              </td>
              <td><input class="form-control" type="text" name="config_google_verify" value="<?php echo $config_google_verify; ?>" size="70" /></td>
            </tr>
            <tr>
              <td><?php echo $entry_meta_robots; ?></td>
              <td>
                <input type="text" name="config_meta_robots" value="<?php echo !empty($config_meta_robots) ? $config_meta_robots:''; ?>" class="form-control" />
              </td>
            </tr>
          </table>
          
      </form>
    </div>
  </div>
</div>
</div>
<?php echo $footer; ?>
