<?php echo $header; ?>
<div class="content-wrapper">
  <section class="content-header">
    <h1><?php echo $heading_title; ?><small>(<?php echo $text_configuration; ?>)</small></h1>
  </section>
  <div id="alert"></div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="content" id="content">
  <div class="box box-primary">
    <div class="box-header">
      <div class="pull-right">
        <a href="<?php echo $cancel; ?>" class="btn btn-default btn-sm"><i class="fa fa-reply" aria-hidden="true"></i> <?php echo $button_close; ?></a>
        <a onclick="$('#form').submit();" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> <?php echo $button_save; ?></a>
      </div>
    </div>
    <div class="box-body">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">

        <div id="tab-image">
          <table class="form">
            <tr>
              <td><?php echo $entry_logo; ?></td>
              <td><div class="thumb pull-left"><img src="<?php echo $logo; ?>" alt="" id="thumb-logo" />
                  <input type="hidden" name="config_logo" value="<?php echo $config_logo; ?>" id="logo" />
                  <br />
                  <a onclick="image_upload('logo', 'thumb-logo');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#thumb-logo').attr('src', '<?php echo $no_image; ?>'); $('#logo').attr('value', '');"><?php echo $text_clear; ?></a></div></td>
            </tr>
            <tr>
              <td><?php echo $entry_icon; ?></td>
              <td><div class="thumb pull-left"><img src="<?php echo $icon; ?>" alt="" id="thumb-icon" />
                  <input type="hidden" name="config_icon" value="<?php echo $config_icon; ?>" id="icon" />
                  <br />
                  <a onclick="image_upload('icon', 'thumb-icon');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#thumb-icon').attr('src', '<?php echo $no_image; ?>'); $('#icon').attr('value', '');"><?php echo $text_clear; ?></a></div></td>
            </tr>
             <tr>
              <td>Footer Logo</td>
              <td><div class="thumb pull-left"><img src="<?php echo $footer_logo; ?>" alt="" id="thumb-footer-logo" />
                  <input type="hidden" name="config_footer_logo" value="<?php echo $config_footer_logo; ?>" id="footer-logo" />
                  <br />
                  <a onclick="image_upload('footer-logo', 'thumb-footer-logo');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#thumb-footer-logo').attr('src', '<?php echo $no_image; ?>'); $('#footer-logo').attr('value', '');"><?php echo $text_clear; ?></a></div></td>
            </tr>
            <tr>
              <td><span class="required">*</span> <?php echo $entry_image_category; ?></td>
              <td><input type="text" name="config_image_category_width" value="<?php echo $config_image_category_width; ?>" size="3" />
                x
                <input type="text" name="config_image_category_height" value="<?php echo $config_image_category_height; ?>" size="3" />
                <?php if ($error_image_category) { ?>
                <span class="error"><?php echo $error_image_category; ?></span>
                <?php } ?></td>
            </tr>
            <tr>
              <td><span class="required">*</span> <?php echo $entry_image_news; ?></td>
              <td><input type="text" name="config_image_news_width" value="<?php echo $config_image_news_width; ?>" size="3" />
                x
                <input type="text" name="config_image_news_height" value="<?php echo $config_image_news_height; ?>" size="3" />
                <?php if ($error_image_news) { ?>
                <span class="error"><?php echo $error_image_news; ?></span>
                <?php } ?></td>
            </tr>
            <tr>
              <td><span class="required">*</span> <?php echo $entry_image_thumb; ?></td>
              <td><input type="text" name="config_image_thumb_width" value="<?php echo $config_image_thumb_width; ?>" size="3" />
                x
                <input type="text" name="config_image_thumb_height" value="<?php echo $config_image_thumb_height; ?>" size="3" />
                <?php if ($error_image_thumb) { ?>
                <span class="error"><?php echo $error_image_thumb; ?></span>
                <?php } ?></td>
            </tr>
            <tr>
              <td><span class="required">*</span> <?php echo $entry_image_popup; ?></td>
              <td><input type="text" name="config_image_popup_width" value="<?php echo $config_image_popup_width; ?>" size="3" />
                x
                <input type="text" name="config_image_popup_height" value="<?php echo $config_image_popup_height; ?>" size="3" />
                <?php if ($error_image_popup) { ?>
                <span class="error"><?php echo $error_image_popup; ?></span>
                <?php } ?></td>
            </tr>
            <tr>
              <td><span class="required">*</span> <?php echo $entry_image_product; ?></td>
              <td><input type="text" name="config_image_product_width" value="<?php echo $config_image_product_width; ?>" size="3" />
                x
                <input type="text" name="config_image_product_height" value="<?php echo $config_image_product_height; ?>" size="3" />
                <?php if ($error_image_product) { ?>
                <span class="error"><?php echo $error_image_product; ?></span>
                <?php } ?></td>
            </tr>

            <tr>
              <td><span class="required">*</span> <?php echo $entry_image_related; ?></td>
              <td><input type="text" name="config_image_related_width" value="<?php echo $config_image_related_width; ?>" size="3" />
                x
                <input type="text" name="config_image_related_height" value="<?php echo $config_image_related_height; ?>" size="3" />
                <?php if ($error_image_related) { ?>
                <span class="error"><?php echo $error_image_related; ?></span>
                <?php } ?></td>
            </tr>
            <tr><td colspan="2"><strong><?php echo $entry_gallery_settings; ?></strong></td></tr>
            <tr>
              <td><?php echo $entry_image_gallery; ?></td>
              <td><input type="text" name="config_gallery_width" value="<?php echo $config_gallery_width; ?>" size="3" />
                x
                <input type="text" name="config_gallery_height" value="<?php echo $config_gallery_height; ?>" size="3" />
              </td>
            </tr>
            <tr>
              <td><?php echo $entry_image_album; ?></td>
              <td><input type="text" name="config_album_width" value="<?php echo $config_album_width; ?>" size="3" />
                x
                <input type="text" name="config_album_height" value="<?php echo $config_album_height; ?>" size="3" />
              </td>
            </tr>
            <tr>
              <td><?php echo $entry_image_album_related; ?></td>
              <td><input type="text" name="config_album_related_width" value="<?php echo $config_album_related_width; ?>" size="3" />
                x
                <input type="text" name="config_album_related_height" value="<?php echo $config_album_related_height; ?>" size="3" />
              </td>
            </tr>
          </table>
        </div>

      </form>
    </div>
  </div>
</div>
</div>
<script type="text/javascript"><!--
function image_upload(field, thumb) {

	$('#dialog').remove();

	$('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');

	$('#dialog').dialog({
		title: '<?php echo $text_image_manager; ?>',
		close: function (event, ui) {
			if ($('#' + field).attr('value')) {
				$.ajax({
					url: 'index.php?route=common/filemanager/image&token=<?php echo $token; ?>&image=' + encodeURIComponent($('#' + field).attr('value')),
					dataType: 'text',
					success: function(text) {
						$('#' + thumb).replaceWith('<img src="' + text + '" alt="" id="' + thumb + '" />');
					}
				});
			}
		},
		bgiframe: false,
		width: 1002,
		height: 600,
		resizable: false,
		modal: false
	});
};
//--></script>
<?php echo $footer; ?>
