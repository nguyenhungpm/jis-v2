<?php echo $header; ?>
<div id="content" class="content-wrapper">
  <section class="content-header">
		<h1><?php echo $heading_title; ?><small>(<?php echo $text_list; ?>)</small></h1>
		<ol class="breadcrumb">
			<?php $k=0; foreach ($breadcrumbs as $breadcrumb) { ?>
				<li class="<?php echo $k==count($breadcrumbs)-1 ? 'active':'';?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $k==0 ? '<i class="fa fa-dashboard"></i> ':'';?><?php echo $breadcrumb['text']; ?></a></li>
			<?php $k++; } ?>
		</ol>
	</section>
    <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>

<div class="content">
	<div class="box box-primary">
    <div class="box-header">
      <div class="pull-left">
        <a onclick="$('form').submit();" class="btn btn-default btn-sm"><i class="fa fa-times"></i> <?php echo $button_delete; ?></a>
      </div>
      <div class="pull-right">
		      <a onclick="location = '<?php echo $export; ?>'" class="btn btn-primary"><i class="fa fa-cloud-download"></i> <?php echo $text_export; ?></a>
      </div>
    </div>
  <div class="box-body">
    <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
      <table class="list">
        <thead>
          <tr>
            <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
            <td class="left"><?php echo $column_email; ?></td>
            <td class="left"><?php echo $column_date_added; ?></td>
            <td class="center"></td>
          </tr>
        </thead>
        <tbody>
          <?php if ($emails) { ?>
          <?php foreach ($emails as $email) { ?>
          <tr>
            <td style="text-align: center;">
              <input type="checkbox" name="selected[]" value="<?php echo $email['id']; ?>"/>
             </td>
            <td><?php echo $email['email']; ?></td>
            <td class="left"><?php echo $email['date_added']; ?></td>
            <td class="center">
      				<?php if(isset($this->request->get['id']) && $email['id'] == $this->request->get['id']){ ?>
      				<a onclick="save('<?php echo $email['id']; ?>')" class="button"><?php echo $button_save; ?></a>
      				<?php }else{ ?>
      				<a style="display:none" href="<?php echo $email['edit']; ?>">Sửa</a>
      				<?php } ?>
      			</td>
          </tr>
          <?php } ?>
          <?php } else { ?>
          <tr>
            <td class="center" colspan="4"><?php echo $text_no_results; ?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </form>
  <div class="pagination"><?php echo $pagination; ?></div>
  </div>
  </div>
</div>
<script>
function save(id){
	var url = 'index.php?route=extension/newsletter/edit&token=<?php echo $token; ?>&id=' +  id + '&email=' + $("input[name='email']").val();
	<?php if(isset($page) && !empty($page)){ ?>
	url += '&page=<?php echo $page; ?>';
	<?php } ?>
	$.ajax({
		url: url,
		type: 'POST',
		dataType: 'json',
		success: function(json) {
			var href = 'index.php?route=extension/newsletter&token=<?php echo $token; ?>';
			<?php if(isset($page) && !empty($page)){ ?>
			href += '&page=<?php echo $page; ?>';
			<?php } ?>
			window.location = href;
		}
	});
}
</script>
<?php echo $footer; ?>
