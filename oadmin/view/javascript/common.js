
function getURLVar(key) {
	var value = [];

	var query = String(document.location).split('?');

	if (query[1]) {
		var part = query[1].split('&');

		for (i = 0; i < part.length; i++) {
			var data = part[i].split('=');

			if (data[0] && data[1]) {
				value[data[0]] = data[1];
			}
		}

		if (value[key]) {
			return value[key];
			} else {
			return '';
		}
	}
}

$(document).ready(function() {
	route = getURLVar('route');

	if (!route) {
		$('#dashboard').addClass('selected');
		} else {
		part = route.split('/');

		url = part[0];

		if (part[1]) {
			url += '/' + part[1];
		}

	$('a[href*=\'route=' + url + '&\']').parents('li').addClass('active');
	$('a[href*=\'route=' + url + '&\']').parents('li[id]').addClass('active');
	}
});

function textCounter(field,cnt, maxlimit) {
	var cntfield = document.getElementById(cnt)
	if (field.value.length > maxlimit)
	field.value = field.value.substring(0, maxlimit);
	else
	cntfield.value = maxlimit - field.value.length;
}
$(document).ready(function(){
	// Confirm Delete
	$('#form').submit(function(){
		if ($(this).attr('action').indexOf('delete',1) != -1) {
			if (!confirm('Xóa/Gỡ bỏ sẽ không phục hồi lại được! Bạn có chắc chắn muốn thực hiện việc này?')) {
				return false;
			}
		}
	});
	$('#form_delete').submit(function(){
		if ($(this).attr('action').indexOf('delete',1) != -1) {
			if (!confirm('Xóa/Gỡ bỏ sẽ không phục hồi lại được! Bạn có chắc chắn muốn thực hiện việc này?')) {
				return false;
			}
		}
	});
	// Confirm Uninstall
	$('a').click(function(){
		if ($(this).attr('href') != null && $(this).attr('href').indexOf('uninstall', 1) != -1) {
			if (!confirm('Xóa/Gỡ bỏ sẽ không phục hồi lại được! Bạn có chắc chắn muốn thực hiện việc này?')) {
				return false;
			}
		}
	});
});
$(document).ready(function(){
	var keyword = $("input[name=keyword]");
	if(!keyword.val()){
		$("input[name^='product_description'],input[name^='category_description'],input[name^='news_category_description'],input[name^='news_description'],input[name^='model']").on('keyup blur',function(){
			var SEOlink = $("input[name^='product_description'],input[name^='category_description'],input[name^='news_category_description'],input[name^='news_description'],input[name^='information_description']").val();
			// var SEOlink = $(this).val();
			SEOlink = SEOlink.replace(/^\s+|\s+$/g, ''); // trim
			SEOlink = SEOlink.toLowerCase();
			// remove accents, swap, etc
			var from = "đĐÀàÁáẠạẢảÃãÂâẦầẤấẬậẨẩẪẫĂăẮắẴẵẶặẰằẲẳÈèÉéẸẹẺẻẼẽÊêỀềẾếỆệỂểỄễÌìÍíỊịỈỉĨĩÒòÓóỌọỎỏÕõÔôỘộỐốỖỗỒồỔổƠơỜờỚớỢợỞởỠỡỲỳÝýỴỵỶỷỸỹÙùÚúỤụỦủŨũƯưỪừỨứỰựỬửỮữnrrd·/_,:;";
			var to   = "ddaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaeeeeeeeeeeeeeeeeeeeeeeiiiiiiiiiiooooooooooooooooooooooooooooooooooyyyyyyyyyyuuuuuuuuuuuuuuuuuuuuuunrrd------";
			for (var i=0, l=from.length ; i<l ; i++) {
				SEOlink = SEOlink.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
			}
			SEOlink = SEOlink.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
			.replace(/\s+/g, '-') // collapse whitespace and replace by -
			.replace(/-+/g, '-'); // collapse dashes

			// will fill .html if here is information page, news details page, productpage
			if($("input[name^='product_description'],input[name^='information_description'],input[name^='news_description']").length){
				SEOlink += '.html';
			}
			keyword.val(SEOlink);
		});
	}
	$(".text-danger").each(function() {
		var element = $(this).parent();

		if (element.hasClass('form-group')) {
			element.addClass('has-error');
		}
	});
});
// Highlight any found errors
document.addEventListener('DOMContentLoaded', function(){
if(document.getElementById('toolbar')){
var myNavBar = {

    flagAdd: true,
    elements: [],
    init: function (elements) {
        this.elements = elements;
    },
    add : function() {
        if(this.flagAdd) {
            for(var i=0; i < this.elements.length; i++) {
                document.getElementById(this.elements[i]).className += " fixed";
            }
            this.flagAdd = false;
        }
    },
    remove: function() {
        for(var i=0; i < this.elements.length; i++) {
            document.getElementById(this.elements[i]).className =
                    document.getElementById(this.elements[i]).className.replace( /(?:^|\s)fixed(?!\S)/g , '' );
        }
        this.flagAdd = true;
    }
};

myNavBar.init(["toolbar"]);

function offSetManager(){

    var yOffset = 109;
    var currYOffSet = window.pageYOffset;
	// console.log(currYOffSet);

    if(yOffset < currYOffSet) {
        myNavBar.add();
		if(!document.getElementById('fake').classList.contains('fixed')){
			document.getElementById('fake').className += "fixed";''
		}
    }else /* if(currYOffSet == yOffset) */{
        myNavBar.remove();
		document.getElementById('fake').className = document.getElementById('fake').className.replace( /(?:^|\s)fixed(?!\S)/g , '' );
    }
}

window.onscroll = function(e) {
    offSetManager();
}
offSetManager();
}
});
