<?php

class ModelSalejob extends Model {
    public function getjob($job_id) {
        $sql = "SELECT * FROM " . DB_PREFIX . "job WHERE job_id = '". $job_id ."' ORDER BY date_added DESC";
        $query = $this->db->query($sql);
        return $query->row;
    }

  	public function updateStatus($job_id, $column_name, $value){
  		$this->db->query("UPDATE " . DB_PREFIX . "job SET " . $column_name . " = '" . (int)$value . "' WHERE job_id = '" . (int)$job_id . "'");
  	}

    public function getjobs($data) {
        $sql = "SELECT * FROM " . DB_PREFIX . "job WHERE 1 ";
        $sql .= " ORDER BY date_added DESC ";
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }
            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }
            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getTotaljobs($data) {
        $query = $this->db->query("SELECT COUNT(*) as total FROM " . DB_PREFIX . "job WHERE 1 ");
        return $query->row['total'];
    }

    public function getTotalUnreadjobs() {
        $query = $this->db->query("SELECT COUNT(*) as total FROM " . DB_PREFIX . "job WHERE status=0 ");
        return $query->row['total'];
    }
	  public function editjobs($data){
		     $query = $this->db->query("UPDATE `" . DB_PREFIX . "job` SET `content`= '". $data['content'] . "',`status`= '". $data['status'] . "',`note`= '". $data['note'] . "' WHERE job_id = '" . $data['job_id'] . "'");
	  }
  	public function deletejob($job_id){
  		  $query = $this->db->query("DELETE FROM `" . DB_PREFIX . "job` WHERE job_id = '" . $job_id . "'");
  	}
    public function getfilebyCode($code){
        $query = $this->db->query("SELECT filename FROM `" . DB_PREFIX . "upload` WHERE code = '" . $code . "'");
        return $query->row['filename'];
    }

}

?>
