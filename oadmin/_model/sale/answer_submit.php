<?php

class ModelSaleAnswerSubmit extends Model {
    public function getanswer_submit($answer_submit_id) {
        $sql = "SELECT * FROM " . DB_PREFIX . "answer_submit WHERE answer_submit_id = '". $answer_submit_id ."' ORDER BY date_added DESC";
        $query = $this->db->query($sql);

        return $query->row;
    }

	public function updateStatus($answer_submit_id, $column_name, $value){
		$this->db->query("UPDATE " . DB_PREFIX . "answer_submit SET " . $column_name . " = '" . (int)$value . "' WHERE answer_submit_id = '" . (int)$answer_submit_id . "'");
	}

    public function getAnswerSubmits($data) {
        $sql = "SELECT * FROM " . DB_PREFIX . "answer_submit WHERE 1 ORDER BY date_added DESC";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getAnswerSubmit($answer_submit_id) {
        $sql = "SELECT * FROM " . DB_PREFIX . "answer_submit WHERE answer_submit_id = '". $answer_submit_id ."'";
        $query = $this->db->query($sql);

        return $query->row;
    }

    public function getTotalAnswerSubmits($data) {
        $query = $this->db->query("SELECT COUNT(*) as total FROM " . DB_PREFIX . "answer_submit WHERE 1 ");

        return $query->row['total'];
    }

    public function getTotalUnreadAnswerSubmits() {
        $query = $this->db->query("SELECT DISTINCT COUNT(*) as total FROM " . DB_PREFIX . "answer_submit WHERE status='0'");

        return $query->row['total'];
    }

	public function editAnswerSubmit($data){
		$query = $this->db->query("UPDATE `" . DB_PREFIX . "answer_submit` SET `content`= '". $data['content'] . "',`status`= '". $data['status'] . "',`note`= '". $data['note'] . "' WHERE answer_submit_id = '" . $data['answer_submit_id'] . "'");
	}

	public function deleteanswer_submit($answer_submit_id){
		$query = $this->db->query("DELETE FROM `" . DB_PREFIX . "answer_submit` WHERE answer_submit_id = '" . $answer_submit_id . "'");
	}

}

?>
