<?php

class ModelSaleOrder extends Model {
    public function getorder($order_id) {
        $sql = "SELECT * FROM " . DB_PREFIX . "order WHERE order_id = '". $order_id ."' ORDER BY date_added DESC";
        $query = $this->db->query($sql);

        return $query->row;
    }

	public function updateStatus($order_id, $column_name, $value){
		$this->db->query("UPDATE " . DB_PREFIX . "order SET " . $column_name . " = '" . (int)$value . "' WHERE order_id = '" . (int)$order_id . "'");
	}

    public function getOrders($data) {
        $sql = "SELECT * FROM " . DB_PREFIX . "order WHERE 1 ORDER BY date_added DESC";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalorders($data) {
        $query = $this->db->query("SELECT COUNT(*) as total FROM " . DB_PREFIX . "order WHERE 1 ");

        return $query->row['total'];
    }

    public function getNeworders() {
        $query = $this->db->query("SELECT COUNT(*) as total FROM " . DB_PREFIX . "order WHERE status=0");

        return $query->row['total'];
    }

	public function editorders($data){
		$query = $this->db->query("UPDATE `" . DB_PREFIX . "order` SET `content`= '". $data['content'] . "',`status`= '". $data['status'] . "',`note`= '". $data['note'] . "' WHERE order_id = '" . $data['order_id'] . "'");
	}

	public function deleteorder($order_id){
		$query = $this->db->query("DELETE FROM `" . DB_PREFIX . "order` WHERE order_id = '" . $order_id . "'");
	}

}

?>
