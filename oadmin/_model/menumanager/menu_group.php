<?php
class ModelMenumanagerMenuGroup extends Model
{
	function getMenuGroups()
	{
		$sql = "SELECT * FROM `" . DB_PREFIX . "menu_group` WHERE language_id = '". $this->config->get('config_language_id') ."'";
		$query = $this->db->query($sql);
		return $query->rows;
	}
	
	function save_group($title_group){
		// $this->session->data['menu'] = $title_group;
		$group_id = 0;
		foreach($title_group as $language_id => $title){
			if($group_id == 0){
				$query = $this->db->query("INSERT INTO `".DB_PREFIX."menu_group` SET `title`='".$title."', `language_id`='".$language_id."'");
				$group_id = $this->db->getLastId();
			}else{
				$query = $this->db->query("INSERT INTO `".DB_PREFIX."menu_group` SET `title`='".$title."', `language_id`='".$language_id."', id='". $group_id ."'");
			}
		}
		// $sql = "INSERT INTO `".DB_PREFIX."menu_group` SET `title`='".$title."', `title_en`='".$title_en."'";
		// $query = $this->db->query($sql);
		return $group_id;
	}
	
	function get_current_menu_group($group_id){
		$data = array();
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "menu_group` WHERE `id`='".(int)$group_id."'");
		foreach($query->rows as $result){
			$data[$result['language_id']] = array(
				'id' => $result['id'],
				'title' => $result['title']
			);
		}
		return $data;
	}
	
	function get_menu_group($group_id)
	{
		$sql = "SELECT * FROM `" . DB_PREFIX . "menu_group` WHERE `id`='".(int)$group_id."' AND language_id = '". $this->config->get('config_language_id') ."'";
		$query = $this->db->query($sql);
		return $query->row;
	}
	
	function update_menu_group($group_id,$title_group){
		$this->delete_menu_group($group_id);
		foreach($title_group as $language_id => $title){
			$query = $this->db->query("INSERT INTO `".DB_PREFIX."menu_group` SET `title`='".$title."', `language_id`='".$language_id."', `id`='".(int)$group_id."'");
		}
	}
	
	function delete_menu_group($group_id)
	{
		$sql="DELETE FROM `".DB_PREFIX."menu_group` WHERE `id`='".(int)$group_id."'";
		$query = $this->db->query($sql);
	}
}
?>