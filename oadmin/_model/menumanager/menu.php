<?php
class ModelMenumanagerMenu extends Model
{
	function getMenuByGroup($group_id){
		$data = array();
		$query = $this->db->query("SELECT * FROM ".DB_PREFIX."menu WHERE group_id='".(int)$group_id."' ORDER BY parent_id,position");
		foreach($query->rows as $rs){
			$name = array();
			$query2 = $this->db->query("SELECT * FROM ".DB_PREFIX."menu_description WHERE menu_id = '". $rs['id'] ."'");
			foreach($query2->rows as $val){
				$name[$val['language_id']] = $val['title'];
			}
			$data[] = array(
				'id' => $rs['id'],
				'parent_id' => $rs['parent_id'],
				'title' => $rs['title'],
				'url' => $rs['url'],
				'name' => $name,
				'label' => join(' | ',$name),
				'class' => $rs['class'],
				'position' => $rs['position'],
				'group_id' => $rs['group_id'],
			);
		}
		// return $query->rows;
		return $data;
	}

	function insertMenu($data)
	{
		$tmp= $this->get_last_position($data['group_id']);
		$data['position'] = $tmp['max_pos'] +1;
		$sql="INSERT INTO `".DB_PREFIX."menu` SET `title`='".$data['title']."',`class`='".$data['class']."',`url`='".$data['url']."',`group_id`='".$data['group_id']."',`position`='".$data['position']."'";
		$query = $this->db->query($sql);
		$menu_id = $this->db->getLastId();
		foreach ($data['name'] as $language_id => $value) {
			$this->db->query("INSERT INTO ".DB_PREFIX."menu_description SET title = '". $this->db->escape($value) ."', language_id = '". $language_id ."', menu_id = '". $menu_id ."'");
		}
		return $menu_id;
	}

	function get_last_position($group_id) {
		$sql = sprintf(
				'SELECT MAX(%s) as `max_pos` FROM %s WHERE %s = %s AND %s = 0',
				'position',
				DB_PREFIX."menu",
				'group_id',
				$group_id,
				'parent_id'
		);
		$query = $this->db->query($sql);
		return $query->row;
	}

	/*function getMenu($menu_id)
	{
		$sql = "SELECT * FROM `".DB_PREFIX."menu` WHERE `id`='".(int)$menu_id."'";
		$query = $this->db->query($sql);
		return $query->row;
	}*/

	function getMenu($menu_id){
		$data = array();
		$query = $this->db->query("SELECT * FROM ".DB_PREFIX."menu WHERE id = '". $menu_id ."'");

		$data = $query->row;

		$query2 = $this->db->query("SELECT * FROM ".DB_PREFIX."menu_description WHERE menu_id = '". $menu_id ."'");
		foreach ($query2->rows as $key => $value) {
			$data['name'][$value['language_id']] = $value['title'];
		}
		$data['label'] = join(' | ', $data['name']);

		// $data['name'] = $query2->rows;

		return $data;
	}

	function update_menu($menu_id,$data)
	{
		$sql = "UPDATE `".DB_PREFIX."menu` SET `title`='".$data['title']."',`url`='".$data['url']."',`class`='".$data['class']."'";

		if(isset($data['position']))
		{
			$sql .= ",`position`='".$data['position']."'";
		}
		if(isset($data['group_id']))
		{
			$sql .= ",`group_id`='".$data['group_id']."'";
		}
		if(isset($data['parent_id']))
		{
			$sql .= ",`parent_id`='".$data['parent_id']."'";
		}
			$sql .= " WHERE `id`='".(int)$menu_id."'";
		if($this->db->query($sql))
		{
			$this->db->query("DELETE FROM ".DB_PREFIX."menu_description WHERE menu_id = '". $menu_id ."'");
			foreach ($data['name'] as $language_id => $value) {
				$this->db->query("INSERT INTO ".DB_PREFIX."menu_description SET title = '". $this->db->escape($value) ."', language_id = '". $language_id ."', menu_id = '". $menu_id ."'");
			}
			return true;
		}else{
			return false;
		}
	}

	function delete_menu($menu_id)
	{
		$sql = "DELETE FROM `".DB_PREFIX."menu` WHERE `id`='".(int)$menu_id."'";
		$this->db->query($sql);
		$sql = "DELETE FROM `".DB_PREFIX."menu` WHERE `parent_id`='".(int)$menu_id."'";
		$this->db->query($sql);
		$this->db->query("DELETE FROM `".DB_PREFIX."menu_description` WHERE `menu_id`='".(int)$menu_id."'");
	}

	function get_label($row) {
		$label =
		'<div class="ns-row">' .
		'<div title="'.$row['url'].'" class="ns-title">'.$row['label'].'</div>' .
		//'<div class="ns-url">'.$row['url'].'</div>' .
		'<div class="ns-sort"><input type="text" name="sort_order['.$row['id'].']" />'.$row['class'].'</div>' .
		'<div class="ns-blank text-center"><input value="1" type="checkbox" name="blank['.$row['id'].']"/></div>' .
		'<div class="ns-nofollow text-center"><input value="1" type="checkbox" name="nofollow['.$row['id'].']"/></div>' .
		'<div class="ns-actions">' .
		'<a href="'.$this->url->link('extension/menumanager/update_menu_item', 'menu_id='.$row['id'].'&token=' . $this->session->data['token'], 'SSL').'" class="edit-menu" title="Edit">' .
		'<img src="'.HTTP_IMAGE.'menumanager/edit.png" alt="Edit" />' .
		'</a>' .
		'<a href="#" class="delete-menu" title="Delete">' .
		'<img src="'.HTTP_IMAGE.'menumanager/cross.png" alt="Delete" />' .
		'</a>' .
		'<input type="hidden" name="menu_id" value="'.$row['id'].'" />' .
		'</div>' .
		'</div>';
		return $label;
	}

	public function save_position($data) {
		if (isset($data['easymm'])) {
			$easymm = $data['easymm'];
			$this->update_position(0, $easymm);
		}
		// $this->session->data['db'] = $data;
	}

	private function update_position($parent, $children) {
		$i = 1;
		foreach ($children as $k => $v) {
			$id = (int)$children[$k]['id'];
			$data['parent_id'] = $parent;
			$data['position'] = $i;
			$sql="UPDATE `".DB_PREFIX."menu` SET `parent_id`='".$parent."',`position`='".$i."' WHERE `id`='".(int)$id."'";
			$this->db->query($sql);
			if (isset($children[$k]['children'][0])) {
				$this->update_position($id, $children[$k]['children']);
			}
			$i++;
		}
	}


}
