<?php
class ModelDesignVideo extends Model {
	public function addVideo($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "video SET name = '" . $this->db->escape($data['name']) . "', status = '" . (int)$data['status'] . "'");
	
		$video_id = $this->db->getLastId();
	
		if (isset($data['video_image'])) {
			foreach ($data['video_image'] as $video_image) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "video_image SET video_id = '" . (int)$video_id . "'");
				
				$video_image_id = $this->db->getLastId();
				
				foreach ($video_image['video_image_description'] as $language_id => $video_image_description) {				
					$this->db->query("INSERT INTO " . DB_PREFIX . "video_image_description SET video_image_id = '" . (int)$video_image_id . "', language_id = '" . (int)$language_id . "', video_id = '" . (int)$video_id . "', title = '" .  $this->db->escape($video_image_description['title']) . "', description = '" .  $this->db->escape($video_image_description['description']) . "', link = '" .  $this->db->escape($video_image_description['link']) . "', image = '" .  $this->db->escape($video_image_description['image']) . "'");
				}
			}
		}		
	}
	
	public function editVideo($video_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "video SET name = '" . $this->db->escape($data['name']) . "', status = '" . (int)$data['status'] . "' WHERE video_id = '" . (int)$video_id . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "video_image WHERE video_id = '" . (int)$video_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "video_image_description WHERE video_id = '" . (int)$video_id . "'");
			
		if (isset($data['video_image'])) {
			foreach ($data['video_image'] as $video_image) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "video_image SET video_id = '" . (int)$video_id . "'");
				
				$video_image_id = $this->db->getLastId();
				
				foreach ($video_image['video_image_description'] as $language_id => $video_image_description) {				
					$this->db->query("INSERT INTO " . DB_PREFIX . "video_image_description SET video_image_id = '" . (int)$video_image_id . "', language_id = '" . (int)$language_id . "', video_id = '" . (int)$video_id . "', title = '" .  $this->db->escape($video_image_description['title']) . "', description = '" .  $this->db->escape($video_image_description['description']) . "', link = '" .  $this->db->escape($video_image_description['link']) . "', image = '" .  $this->db->escape($video_image_description['image']) . "'");
				}
			}
		}			
	}
	
	public function deleteVideo($video_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "video WHERE video_id = '" . (int)$video_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "video_image WHERE video_id = '" . (int)$video_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "video_image_description WHERE video_id = '" . (int)$video_id . "'");
	}
	
	public function getVideo($video_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "video WHERE video_id = '" . (int)$video_id . "'");
		
		return $query->row;
	}
		
	public function getVideos($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "video";
		
		$sort_data = array(
			'name',
			'status'
		);	
		
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY name";	
		}
		
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}					

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
		
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}		
		
		$query = $this->db->query($sql);

		return $query->rows;
	}
		
	public function getVideoImages($video_id) {
		$video_image_data = array();
		
		$video_image_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "video_image WHERE video_id = '" . (int)$video_id . "'");
		
		foreach ($video_image_query->rows as $video_image) {
			$video_image_description_data = array();
			 
			$video_image_description_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "video_image_description WHERE video_image_id = '" . (int)$video_image['video_image_id'] . "' AND video_id = '" . (int)$video_id . "'");
			
			foreach ($video_image_description_query->rows as $video_image_description) {	
			
				if (!empty($video_image_description['image']) && file_exists(DIR_IMAGE . $video_image_description['image'])) {
					$image = $video_image_description['image'];
				} else {
					$image = 'no_image.jpg';
				}					
					
				$video_image_description_data[$video_image_description['language_id']] = array(
				'title' => $video_image_description['title'], 
				'description' => $video_image_description['description'], 
				'link'  => $video_image_description['link'], 
				'image' => $image,
				'thumb' => $this->model_tool_image->resize($image, 100, 100));
			}
		
			$video_image_data[] = array(
				'video_image_description' => $video_image_description_data	
			);
		}
		
		return $video_image_data;
	}
		
	public function getTotalVideos() {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "video");
		
		return $query->row['total'];
	}	
}
?>