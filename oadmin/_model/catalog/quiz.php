<?php

class ModelCatalogQuiz extends Model {

    public function checkKeywords($data = array()) {

        if (!empty($data['quiz_id'])) {
          $sql = "SELECT COUNT(keyword) AS total FROM " . DB_PREFIX . "url_alias WHERE keyword = '" . $data['keyword'] . "' AND NOT(query = 'quiz_id=" . (int) $data['quiz_id'] . "')";
        } else {
            $sql = "SELECT COUNT('keyword') AS total FROM " . DB_PREFIX . "url_alias WHERE keyword = '" . $data['keyword'] . "'";
        }

		$query = $this->db->query($sql);
		return $query->row['total'];

    }

    public function getQuizNews($quiz_id) {
        $quiz_filter_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "quiz_news WHERE quiz_id = '" . (int) $quiz_id . "'");

        foreach ($query->rows as $result) {
            $quiz_filter_data[] = $result['news_id'];
        }

        return $quiz_filter_data;
    }

    public function getNameQuiz($quiz_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "quiz_description n LEFT JOIN " . DB_PREFIX . "quiz nd ON (n.quiz_id = nd.quiz_id) WHERE nd.quiz_id = '" . (int) $quiz_id . "'");

        return $query->row;
    }

    // public function addQuiz($data) {
    // $this->db->query("INSERT INTO " . DB_PREFIX . "quiz SET model = '" . $this->db->escape($data['model']) . "', sku = '" . $this->db->escape($data['sku']) . "', upc = '" . $this->db->escape($data['upc']) . "', ean = '" . $this->db->escape($data['ean']) . "', jan = '" . $this->db->escape($data['jan']) . "', isbn = '" . $this->db->escape($data['isbn']) . "', mpn = '" . $this->db->escape($data['mpn']) . "', location = '" . $this->db->escape($data['location']) . "', quantity = '" . (int)$data['quantity'] . "', minimum = '" . (int)$data['minimum'] . "', subtract = '" . (int)$data['subtract'] . "', stock_status_id = '" . (int)$data['stock_status_id'] . "', date_available = '" . $this->db->escape($data['date_available']) . "', robots = '" . $this->db->escape($data['robots']) . "', manufacturer_id = '" . (int)$data['manufacturer_id'] . "', shipping = '" . (int)$data['shipping'] . "', price = '" . (float)$data['price'] . "', points = '" . (int)$data['points'] . "', weight = '" . (float)$data['weight'] . "', weight_class_id = '" . (int)$data['weight_class_id'] . "', length = '" . (float)$data['length'] . "', width = '" . (float)$data['width'] . "', height = '" . (float)$data['height'] . "', length_class_id = '" . (int)$data['length_class_id'] . "', status = '" . (int)$data['status'] . "', tax_class_id = '" . $this->db->escape($data['tax_class_id']) . "', sort_order = '" . (int)$data['sort_order'] . "', date_added = NOW()");
    public function addQuiz($data) {
		if(!isset($data['status'])){$data['status']=0;}
		if(!isset($data['quantity'])){$data['quantity']=0;}
        $this->db->query("INSERT INTO " . DB_PREFIX . "quiz SET model = '" . $this->db->escape($data['model'])  . "', quantity = '" . (int)$data['quantity'] . "', date_available = '" . $this->db->escape($data['date_available']) . "', robots = '" . $this->db->escape($data['robots']) . "', manufacturer_id = '" . (int)$data['manufacturer_id'] . "', price = '" . (float) $data['price'] . "',status = '" . (int) $data['status'] . "', sort_order = '" . (int) $data['sort_order'] . "', date_added = NOW()");

        $quiz_id = $this->db->getLastId();

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "quiz SET image = '" . $this->db->escape(html_entity_decode($data['image'], ENT_QUOTES, 'UTF-8')) . "' WHERE quiz_id = '" . (int) $quiz_id . "'");
        }

        foreach ($data['quiz_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "quiz_description SET quiz_id = '" . (int) $quiz_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "',  short_description = '" . $this->db->escape($value['short_description']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "', description = '" . $this->db->escape($value['description']) . "', tag = '" . $this->db->escape($value['tag']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "'");
        }

        if (isset($data['quiz_news'])) {
            foreach ($data['quiz_news'] as $news_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "quiz_news SET quiz_id = '" . (int) $quiz_id . "', news_id = '" . (int) $news_id . "'");
            }
        }

        if (isset($data['quiz_special'])) {
            foreach ($data['quiz_special'] as $quiz_special) {
                if ((int) $quiz_special['priority']) {
                    $quantity_special = $data['price'] - ($data['price'] * ($quiz_special['priority'] * 0.01));
                    $this->db->query("INSERT INTO " . DB_PREFIX . "quiz_special SET quiz_id = '" . (int) $quiz_id . "', customer_group_id = '" . (int) $quiz_special['customer_group_id'] . "', priority = '" . (int) $quiz_special['priority'] . "', price = '" . (float) $quantity_special . "', date_start = '" . $this->db->escape($quiz_special['date_start']) . "', date_end = '" . $this->db->escape($quiz_special['date_end']) . "'");
                } else {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "quiz_special SET quiz_id = '" . (int) $quiz_id . "', customer_group_id = '" . (int) $quiz_special['customer_group_id'] . "', priority = '" . (int) $quiz_special['priority'] . "', price = '" . (float) $quiz_special['price'] . "', date_start = '" . $this->db->escape($quiz_special['date_start']) . "', date_end = '" . $this->db->escape($quiz_special['date_end']) . "'");
                }
            }
        }

        if (isset($data['quiz_image'])) {
            foreach ($data['quiz_image'] as $quiz_image) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "quiz_image SET quiz_id = '" . (int) $quiz_id . "', image = '" . $this->db->escape(html_entity_decode($quiz_image['image'], ENT_QUOTES, 'UTF-8')) . "', dungsai = '" . (int) $quiz_image['dungsai'] . "', question = '" . $quiz_image['question'] . "', sort_order = '" . (int) $quiz_image['sort_order'] . "'");
            }
        }

        if (isset($data['group'])) {
            $group = explode(',', $data['group']);
            foreach ($group as $group_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "quiz_to_group SET quiz_id = '" . (int) $quiz_id . "', group_id = '" . (int) $group_id . "'");
            }
        }

        if (isset($data['quiz_related'])) {
            foreach ($data['quiz_related'] as $related_id) {
                $this->db->query("DELETE FROM " . DB_PREFIX . "quiz_related WHERE quiz_id = '" . (int) $quiz_id . "' AND related_id = '" . (int) $related_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "quiz_related SET quiz_id = '" . (int) $quiz_id . "', related_id = '" . (int) $related_id . "'");
                $this->db->query("DELETE FROM " . DB_PREFIX . "quiz_related WHERE quiz_id = '" . (int) $related_id . "' AND related_id = '" . (int) $quiz_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "quiz_related SET quiz_id = '" . (int) $related_id . "', related_id = '" . (int) $quiz_id . "'");
            }
        }

        if ($data['keyword']) {
            if ((!isset($data['seo']) || isset($data['seo']) && $data['seo'] == 1)) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'quiz_id=" . (int) $quiz_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
            }
        }

        $this->cache->delete('caseo_url');
        $this->cache->delete('quiz');

        return $quiz_id;

    }

public function editQuiz($quiz_id, $data) {
		if(!isset($data['status'])){$data['status']=0;}
		if(!isset($data['quantity'])){$data['quantity']=0;}
        $this->db->query("UPDATE " . DB_PREFIX . "quiz SET model = '" . $this->db->escape($data['model']) . "', date_available = '" . $this->db->escape($data['date_available']) . "', robots = '" . $this->db->escape($data['robots']) . "', manufacturer_id = '" . (int)$data['manufacturer_id']  . "', quantity = '" . (int)$data['quantity'] . "', price = '" . (float) $data['price'] . "', status = '" . (int) $data['status'] . "', sort_order = '" . (int) $data['sort_order'] . "', date_modified = NOW() WHERE quiz_id = '" . (int) $quiz_id . "'");

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "quiz SET image = '" . $this->db->escape(html_entity_decode($data['image'], ENT_QUOTES, 'UTF-8')) . "' WHERE quiz_id = '" . (int) $quiz_id . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "quiz_description WHERE quiz_id = '" . (int) $quiz_id . "'");

        foreach ($data['quiz_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "quiz_description SET quiz_id = '" . (int) $quiz_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "', short_description = '" . $this->db->escape($value['short_description']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', description = '" . $this->db->escape($value['description']) . "', tag = '" . $this->db->escape($value['tag']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "quiz_news WHERE quiz_id = '" . (int) $quiz_id . "'");

        if (isset($data['quiz_news'])) {
            foreach ($data['quiz_news'] as $news_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "quiz_news SET quiz_id = '" . (int) $quiz_id . "', news_id = '" . (int) $news_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "quiz_special WHERE quiz_id = '" . (int) $quiz_id . "'");

        if (isset($data['quiz_special'])) {
            foreach ($data['quiz_special'] as $quiz_special) {
                if ((int) $quiz_special['priority']) {
                    $quantity_special = $data['price'] - ($data['price'] * ($quiz_special['priority'] * 0.01));
                    $this->db->query("INSERT INTO " . DB_PREFIX . "quiz_special SET quiz_id = '" . (int) $quiz_id . "', customer_group_id = '" . (int) $quiz_special['customer_group_id'] . "', priority = '" . (int) $quiz_special['priority'] . "', price = '" . (float) $quantity_special . "', date_start = '" . $this->db->escape($quiz_special['date_start']) . "', date_end = '" . $this->db->escape($quiz_special['date_end']) . "'");
                } else {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "quiz_special SET quiz_id = '" . (int) $quiz_id . "', customer_group_id = '" . (int) $quiz_special['customer_group_id'] . "', priority = '" . (int) $quiz_special['priority'] . "', price = '" . (float) $quiz_special['price'] . "', date_start = '" . $this->db->escape($quiz_special['date_start']) . "', date_end = '" . $this->db->escape($quiz_special['date_end']) . "'");
                }
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "quiz_image WHERE quiz_id = '" . (int) $quiz_id . "'");

        if (isset($data['quiz_image'])) {
            foreach ($data['quiz_image'] as $quiz_image) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "quiz_image SET quiz_id = '" . (int) $quiz_id . "', image = '" . $this->db->escape(html_entity_decode($quiz_image['image'], ENT_QUOTES, 'UTF-8')) . "', dungsai = '" . (int) $quiz_image['dungsai'] . "', question = '" . $quiz_image['question'] . "', sort_order = '" . (int) $quiz_image['sort_order'] . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "quiz_to_group WHERE quiz_id = '" . (int) $quiz_id . "'");

        if (isset($data['group'])) {
			$group = explode(',', $data['group']);
            foreach ($group as $group_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "quiz_to_group SET quiz_id = '" . (int) $quiz_id . "', group_id = '" . (int) $group_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "quiz_related WHERE quiz_id = '" . (int) $quiz_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "quiz_related WHERE related_id = '" . (int) $quiz_id . "'");

        if (isset($data['quiz_related'])) {
            foreach ($data['quiz_related'] as $related_id) {
                $this->db->query("DELETE FROM " . DB_PREFIX . "quiz_related WHERE quiz_id = '" . (int) $quiz_id . "' AND related_id = '" . (int) $related_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "quiz_related SET quiz_id = '" . (int) $quiz_id . "', related_id = '" . (int) $related_id . "'");
                $this->db->query("DELETE FROM " . DB_PREFIX . "quiz_related WHERE quiz_id = '" . (int) $related_id . "' AND related_id = '" . (int) $quiz_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "quiz_related SET quiz_id = '" . (int) $related_id . "', related_id = '" . (int) $quiz_id . "'");
            }
        }


//        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'quiz_id=" . (int) $quiz_id . "'");
//
//        if ($data['keyword']) {
//            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'quiz_id=" . (int) $quiz_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
//        }
        if ((!isset($data['seo']) || isset($data['seo']) && $data['seo'] == 1)) {
            $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'quiz_id=" . (int) $quiz_id . "'");

            if ($data['keyword']) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'quiz_id=" . (int) $quiz_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
            }
        }
        $this->cache->delete('caseo_url');
        $this->cache->delete('quiz');
    }

    public function copyQuiz($quiz_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "quiz p LEFT JOIN " . DB_PREFIX . "quiz_description pd ON (p.quiz_id = pd.quiz_id) WHERE p.quiz_id = '" . (int) $quiz_id . "' AND pd.language_id = '" . (int) $this->config->get('config_language_id') . "'");

        if ($query->num_rows) {
            $data = array();

            $data = $query->row;

            $data['sku'] = '';
            $data['upc'] = '';
            $data['viewed'] = '0';
            $data['keyword'] = '';
            $data['status'] = '0';

            $data = array_merge($data, array('quiz_description' => $this->getQuizDescriptions($quiz_id)));
            $data = array_merge($data, array('quiz_image' => $this->getQuizImages($quiz_id)));
            $data = array_merge($data, array('quiz_related' => $this->getQuizRelated($quiz_id)));
            $data = array_merge($data, array('quiz_special' => $this->getQuizSpecials($quiz_id)));
            $data = array_merge($data, array('quiz_group' => $this->getQuizGroups($quiz_id)));
            $this->addQuiz($data);
        }
    }

    public function deleteQuiz($quiz_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "quiz WHERE quiz_id = '" . (int) $quiz_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "quiz_description WHERE quiz_id = '" . (int) $quiz_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "quiz_image WHERE quiz_id = '" . (int) $quiz_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "quiz_related WHERE quiz_id = '" . (int) $quiz_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "quiz_related WHERE related_id = '" . (int) $quiz_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "quiz_special WHERE quiz_id = '" . (int) $quiz_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "quiz_to_group WHERE quiz_id = '" . (int) $quiz_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "quiz_news WHERE quiz_id = '" . (int) $quiz_id . "'");
        // $this->db->query("DELETE FROM " . DB_PREFIX . "review WHERE quiz_id = '" . (int) $quiz_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'quiz_id=" . (int) $quiz_id . "'");

        $this->cache->delete('quiz');
        $this->cache->delete('caseo_url');
    }


	public function updateStatus($quiz_id, $column_name, $value){
		$this->db->query("UPDATE " . DB_PREFIX . "quiz SET " . $column_name . " = '" . (int)$value . "' WHERE quiz_id = '" . (int)$quiz_id . "'");
	}

    public function getQuiz($quiz_id) {
        $query = $this->db->query("SELECT DISTINCT *, (SELECT DISTINCT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'quiz_id=" . (int) $quiz_id . "' LIMIT 1) AS keyword FROM " . DB_PREFIX . "quiz p LEFT JOIN " . DB_PREFIX . "quiz_description pd ON (p.quiz_id = pd.quiz_id) WHERE p.quiz_id = '" . (int) $quiz_id . "' AND pd.language_id = '" . (int) $this->config->get('config_language_id') . "'");

        return $query->row;
    }

    public function getQuizs($data = array()) {

//        osdvn
        if (isset($data['display_name']) && $data['display_name'] == true) {
            if (isset($data['filter_group']) && !is_null($data['filter_group'])) {
                $name_group = ",pd.quiz_id as quiz_id ,pd.name as name, cd.name as name_group";
                $select_group = " LEFT JOIN " . DB_PREFIX . "quiz_to_group  p2c ON (p.quiz_id = p2c.quiz_id) LEFT JOIN " . DB_PREFIX . "group_description cd ON (p2c.group_id = cd.group_id) ";
                $filter_group = " AND cd.group_id = '" . $this->db->escape($data['filter_group']) . "' ";
            } else {
                $name_group = ",pd.quiz_id as quiz_id,pd.name as name, cd.name as name_group";
                $select_group = " LEFT JOIN " . DB_PREFIX . "quiz_to_group  p2c ON (p.quiz_id = p2c.quiz_id) LEFT JOIN " . DB_PREFIX . "group_description cd ON (p2c.group_id = cd.group_id) ";
                $filter_group = "";
            }
        } else {
            $select_group = "";
            $filter_group = "";
            $name_group = "";
        }
//        end

        $sql = "SELECT * " . $name_group . " FROM " . DB_PREFIX . "quiz p LEFT JOIN " . DB_PREFIX . "quiz_description pd ON (p.quiz_id = pd.quiz_id) " . $select_group;

        if (!empty($data['filter_group_id'])) {
            $sql .= " LEFT JOIN " . DB_PREFIX . "quiz_to_group p2c ON (p.quiz_id = p2c.quiz_id)";
        }

        $sql .= " WHERE pd.language_id = '" . (int) $this->config->get('config_language_id') . "' " . $filter_group;

        if (!empty($data['filter_name'])) {
            $sql .= " AND pd.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!empty($data['filter_model'])) {
            $sql .= " AND p.model LIKE '" . $this->db->escape($data['filter_model']) . "%'";
        }

        if (!empty($data['filter_price'])) {
            $sql .= " AND p.price LIKE '" . $this->db->escape($data['filter_price']) . "%'";
        }

        if (isset($data['filter_quantity']) && !is_null($data['filter_quantity'])) {
            $sql .= " AND p.quantity = '" . $this->db->escape($data['filter_quantity']) . "'";
        }

        if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
            $sql .= " AND p.status = '" . (int) $data['filter_status'] . "'";
        }

        $sql .= " GROUP BY p.quiz_id";

        $sort_data = array(
            'pd.name',
            'p.model',
            'p.price',
            'p.quantity',
            'p.status',
            'p.sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY pd.name";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getQuizsByGroupId($group_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "quiz p LEFT JOIN " . DB_PREFIX . "quiz_description pd ON (p.quiz_id = pd.quiz_id) LEFT JOIN " . DB_PREFIX . "quiz_to_group p2c ON (p.quiz_id = p2c.quiz_id) WHERE pd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND p2c.group_id = '" . (int) $group_id . "' ORDER BY pd.name ASC");

        return $query->rows;
    }

    public function getQuizDescriptions($quiz_id) {
        $quiz_description_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "quiz_description WHERE quiz_id = '" . (int) $quiz_id . "'");

        foreach ($query->rows as $result) {
            $quiz_description_data[$result['language_id']] = array(
                'name' => $result['name'],
                'description' => $result['description'],
                'meta_keyword' => $result['meta_keyword'],
                'short_description' => $result['short_description'],
                'meta_description' => $result['meta_description'],
                'tag' => $result['tag'],
                'meta_title' => $result['meta_title']
            );
        }

        return $quiz_description_data;
    }

    public function getQuizGroups($quiz_id) {
        $quiz_group_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "quiz_to_group WHERE quiz_id = '" . (int) $quiz_id . "'");

        foreach ($query->rows as $result) {
            $quiz_group_data[] = $result['group_id'];
        }

        return $quiz_group_data;
    }

    public function getQuizFilters($quiz_id) {
        $quiz_filter_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "quiz_filter WHERE quiz_id = '" . (int) $quiz_id . "'");

        foreach ($query->rows as $result) {
            $quiz_filter_data[] = $result['filter_id'];
        }

        return $quiz_filter_data;
    }

    public function getQuizAttributes($quiz_id) {
        return false;
    }

    public function getQuizOptions($quiz_id) {
        return false;
    }

    public function getQuizImages($quiz_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "quiz_image WHERE quiz_id = '" . (int) $quiz_id . "'");

        return $query->rows;
    }

    public function getQuizDiscounts($quiz_id) {
        return false;
    }

    public function getQuizSpecials($quiz_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "quiz_special WHERE quiz_id = '" . (int) $quiz_id . "' ORDER BY priority, price");

        return $query->rows;
    }

    public function getQuizRewards($quiz_id) {
        return false;
    }

    public function getQuizDownloads($quiz_id) {
        return false;
    }

    public function getQuizStores($quiz_id) {
        return false;
    }

    public function getQuizRelated($quiz_id) {
        $quiz_related_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "quiz_related WHERE quiz_id = '" . (int) $quiz_id . "'");

        foreach ($query->rows as $result) {
            $quiz_related_data[] = $result['related_id'];
        }

        return $quiz_related_data;
    }

    public function getProfiles($quiz_id) {
        return false;
    }

    public function getTotalQuizs($data = array()) {
        $sql = "SELECT COUNT(DISTINCT p.quiz_id) AS total FROM " . DB_PREFIX . "quiz p LEFT JOIN " . DB_PREFIX . "quiz_description pd ON (p.quiz_id = pd.quiz_id)";

        if (!empty($data['filter_group_id'])) {
            $sql .= " LEFT JOIN " . DB_PREFIX . "quiz_to_group p2c ON (p.quiz_id = p2c.quiz_id)";
        }

        $sql .= " WHERE pd.language_id = '" . (int) $this->config->get('config_language_id') . "'";

        if (!empty($data['filter_name'])) {
            $sql .= " AND pd.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!empty($data['filter_group'])) {
            $sql .= " AND p.quiz_id IN (SELECT quiz_id FROM " . DB_PREFIX . "quiz_to_group WHERE group_id = '" . (int)$data['filter_group'] . "')";
        }

        if (!empty($data['filter_model'])) {
            $sql .= " AND p.model LIKE '" . $this->db->escape($data['filter_model']) . "%'";
        }

        if (!empty($data['filter_price'])) {
            $sql .= " AND p.price LIKE '" . $this->db->escape($data['filter_price']) . "%'";
        }

        if (isset($data['filter_quantity']) && !is_null($data['filter_quantity'])) {
            $sql .= " AND p.quantity = '" . $this->db->escape($data['filter_quantity']) . "'";
        }

        if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
            $sql .= " AND p.status = '" . (int) $data['filter_status'] . "'";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getTotalQuizsByTaxClassId($tax_class_id) {
		return FALSE;
    }

    public function getTotalQuizsByStockStatusId($stock_status_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "quiz WHERE stock_status_id = '" . (int) $stock_status_id . "'");

        return $query->row['total'];
    }

    public function getTotalQuizsByDownloadId($download_id) {
        return false;
    }

    public function getTotalQuizsByManufacturerId($manufacturer_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "quiz WHERE manufacturer_id = '" . (int) $manufacturer_id . "'");

        return $query->row['total'];
    }

    public function getTotalQuizsByAttributeId($attribute_id) {
        return false;
    }

    public function getTotalQuizsByOptionId($option_id) {
        return false;
    }

}
?>
