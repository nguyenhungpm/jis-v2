<?php
class ModelCatalogAdmission extends Model {
	public function addAdmission($data) {
		if(!isset($data['status'])){$data['status']=0;}
		if(!isset($data['home'])){$data['home']=0;}
		$this->db->query("INSERT INTO " . DB_PREFIX . "admission SET sort_order = '" . (int)$data['sort_order'] . "', robots = '" . $this->db->escape($data['robots']) . "', code_youtube = '" . $this->db->escape($data['code_youtube']) . "', bottom = '" . (isset($data['bottom']) ? (int)$data['bottom'] : 0) . "', status = '" . (int)$data['status'] . "', home = '" . (int)$data['home'] . "', category_id = '" . (int)$data['category_id'] . "'");

		$admission_id = $this->db->getLastId();

		foreach ($data['admission_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "admission_description SET admission_id = '" . (int)$admission_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) .  "', description = '" . $this->db->escape($value['description']) . "', expand = '" . $this->db->escape($value['expand']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}

        if (isset($data['admission_highlight'])) {
            foreach ($data['admission_highlight'] as $admission_highlight) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "admission_highlight SET admission_id = '" . (int) $admission_id . "', image = '" . $this->db->escape(html_entity_decode($admission_highlight['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int) $admission_highlight['sort_order'] . "', description = '" . $this->db->escape($admission_highlight['description']) . "'");
            }
        }

		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'admission_id=" . (int)$admission_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}

		$this->cache->delete('admission');
	}

	public function editAdmission($admission_id, $data) {
		if(!isset($data['status'])){$data['status']=0;}
		if(!isset($data['home'])){$data['home']=0;}
		$this->db->query("UPDATE " . DB_PREFIX . "admission SET sort_order = '" . (int)$data['sort_order'] . "', robots = '" . $this->db->escape($data['robots']) . "', code_youtube = '" . $this->db->escape($data['code_youtube']) . "', bottom = '" . (isset($data['bottom']) ? (int)$data['bottom'] : 0) . "', status = '" . (int)$data['status'] . "', home = '" . (int)$data['home'] . "', category_id = '" . (int)$data['category_id'] . "' WHERE admission_id = '" . (int)$admission_id . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "admission_description WHERE admission_id = '" . (int)$admission_id . "'");

		foreach ($data['admission_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "admission_description SET admission_id = '" . (int)$admission_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) .  "', description = '" . $this->db->escape($value['description']) . "', expand = '" . $this->db->escape($value['expand']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}

		
        if (isset($data['admission_highlight'])) {
			$this->db->query("DELETE FROM " . DB_PREFIX . "admission_highlight WHERE admission_id = '" . (int) $admission_id . "'");
            foreach ($data['admission_highlight'] as $admission_highlight) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "admission_highlight SET admission_id = '" . (int) $admission_id . "', image = '" . $this->db->escape(html_entity_decode($admission_highlight['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int) $admission_highlight['sort_order'] . "', description = '" . $this->db->escape($admission_highlight['description']) . "'");
            }
        }


		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'admission_id=" . (int)$admission_id. "'");

		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'admission_id=" . (int)$admission_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}

		$this->cache->delete('admission');
	}

	public function deleteAdmission($admission_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "admission WHERE admission_id = '" . (int)$admission_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "admission_highlight WHERE admission_id = '" . (int) $admission_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "admission_description WHERE admission_id = '" . (int)$admission_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'admission_id=" . (int)$admission_id . "'");

		$this->cache->delete('admission');
	}

	public function getAdmission($admission_id) {
		$query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'admission_id=" . (int)$admission_id . "') AS keyword FROM " . DB_PREFIX . "admission WHERE admission_id = '" . (int)$admission_id . "'");

		return $query->row;
	}

    public function getAdmissionHighlights($admission_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "admission_highlight WHERE admission_id = '" . (int) $admission_id . "'");

        return $query->rows;
    }

	public function getAdmissions($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "admission i LEFT JOIN " . DB_PREFIX . "admission_description id ON (i.admission_id = id.admission_id) WHERE id.language_id = '" . (int)$this->config->get('config_language_id') . "'";

			$sort_data = array(
				'id.title',
				'i.sort_order'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY id.title";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			$admission_data = $this->cache->get('admission.' . (int)$this->config->get('config_language_id'));

			if (!$admission_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "admission i LEFT JOIN " . DB_PREFIX . "admission_description id ON (i.admission_id = id.admission_id) WHERE id.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY id.title");

				$admission_data = $query->rows;

				$this->cache->set('admission.' . (int)$this->config->get('config_language_id'), $admission_data);
			}

			return $admission_data;
		}
	}

	public function getAdmissionDescriptions($admission_id) {
		$admission_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "admission_description WHERE admission_id = '" . (int)$admission_id . "'");

		foreach ($query->rows as $result) {
			$admission_description_data[$result['language_id']] = array(
				'title'       => $result['title'],
				'description' => $result['description'],
				'expand' => $result['expand'],
				'meta_description' => $result['meta_description'],
				'meta_keyword' => $result['meta_keyword'],
				'meta_title' => $result['meta_title']
			);
		}

		return $admission_description_data;
	}


	public function getTotalAdmissions() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "admission");

		return $query->row['total'];
	}
}
?>
