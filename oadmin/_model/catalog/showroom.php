<?php
class ModelCatalogShowroom extends Model {
	public function getProvincies() {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "province`");
		
		return $query->rows;
	}
	
	public function getDistrict($province_id,$data)
	{
		$sql="SELECT * FROM `" . DB_PREFIX . "district` WHERE `province_id`=".$province_id;
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}				

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}	
		$query=$this->db->query($sql);
		return $query->rows;
		/* return $sql; */
	}
	
	public function getTotalDistrict($province_id){
		$sql="SELECT COUNT(*) as total FROM `" . DB_PREFIX . "district` WHERE `province_id`=".$province_id;
		$query=$this->db->query($sql);
		return $query->row['total'];
	}
	
	public function getDealer($district_id)
	{
		$sql="SELECT * FROM `" . DB_PREFIX . "dealer` WHERE `districtid`=".$district_id;
		$query=$this->db->query($sql);
		return $query->rows;
	}
	
	public function getDealerDetail($dealer_id)
	{
		$sql="SELECT * FROM `" . DB_PREFIX . "dealer` WHERE `dealer_id`=".$dealer_id;
		$query=$this->db->query($sql);
		return $query->rows;
		/* return $sql; */
	}
	
	public function addDealer($data,$district_id)
	{
		$sql="INSERT INTO `" . DB_PREFIX . "dealer`(`name`, `address`, `telephone`, `email`, `districtid`) VALUES ('" . $this->db->escape($data['name-dealer']) .  "','" . $this->db->escape($data['address-dealer']) .  "','" . $this->db->escape($data['telephone-dealer']) .  "','" . $this->db->escape($data['email-dealer']) .  "','" . $district_id .  "')";
		
		$this->db->query($sql);
		/* return $sql; */
	}
	
	public function getProvinceByDistrict($district_id)
	{
		$sql="SELECT `provinceid` FROM `oc_district` WHERE districtid='".$district_id."'";
		$query=$this->db->query($sql);
		return $query->row['provinceid'];
	}
	
	public function editDealer($dealer_id, $data, $district_id) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "dealer` WHERE `dealer_id`=".$dealer_id);
		$sql="INSERT INTO `" . DB_PREFIX . "dealer`(`dealer_id`, `name`, `address`, `telephone`, `email`, `districtid`) VALUES ('".$dealer_id."','" . $this->db->escape($data['name-dealer']) .  "','" . $this->db->escape($data['address-dealer']) .  "','" . $this->db->escape($data['telephone-dealer']) .  "','" . $this->db->escape($data['email-dealer']) .  "','" . $district_id .  "')";
		
		$this->db->query($sql);
	}
	
	public function deleteDealer($dealer_id) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "dealer` WHERE `dealer_id`=".$dealer_id);
	}
	
}