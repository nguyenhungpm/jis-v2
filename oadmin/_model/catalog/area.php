<?php
class ModelCatalogArea extends Model {
	
	public function getAreas(){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "area a LEFT JOIN " . DB_PREFIX . "area_description ad ON (a.area_id=ad.area_id) WHERE ad.language_id='" . (int)$this->config->get('config_language_id') . "'");
		return $query->rows;
	}
	public function getArea($area_id){
		$query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'area_id=" . (int)$area_id . "') AS keyword FROM " . DB_PREFIX . "area WHERE area_id = '" . (int)$area_id . "'");
		
		return $query->row;
	}
	public function getAreaDescriptions($area_id) {
		$area_description_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "area_description WHERE area_id = '" . (int)$area_id . "'");
		
		foreach ($query->rows as $result) {
			$area_description_data[$result['language_id']] = array(
				'name'  => $result['name']
			);
		}
		return $area_description_data;
	}
	public function addArea($data){
		$this->db->query("INSERT INTO " . DB_PREFIX . "area SET sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "'");
		$area_id = $this->db->getLastId();
		foreach ($data['area_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "area_description SET area_id = '" . (int)$area_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "'");
		}
	}
	public function editArea($area_id, $data){
		$this->db->query("UPDATE " . DB_PREFIX . "area SET sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "' WHERE area_id = '" . (int)$area_id . "'");
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "area_description WHERE area_id = '" . (int)$area_id . "'");
		foreach ($data['area_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "area_description SET area_id = '" . (int)$area_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "'");
		}
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'area_id=" . (int)$area_id. "'");
		
		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'area_id=" . (int)$area_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}
	}
	public function deleteArea($area_id) {		
		$this->db->query("DELETE FROM " . DB_PREFIX . "area_description WHERE area_id = '" . (int)$area_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "area WHERE area_id = '" . (int)$area_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'area_id=" . (int)$area_id . "'");

	}
}
?>