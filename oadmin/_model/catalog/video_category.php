<?php
class ModelCatalogVideoCategory extends Model {
	public function addVideoCategory($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "video_category SET sort_order = '" . (int)$data['sort_order'] . "', bottom = '" . (isset($data['bottom']) ? (int)$data['bottom'] : 0) . "', status = '" . (int)$data['status'] . "'");

		$video_category_id = $this->db->getLastId(); 

		foreach ($data['video_category_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "video_category_description SET video_category_id = '" . (int)$video_category_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', description = '" . $this->db->escape($value['description']) . "'");
		}

		if (isset($data['video_category_store'])) {
			foreach ($data['video_category_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "video_category_to_store SET video_category_id = '" . (int)$video_category_id . "', store_id = '" . (int)$store_id . "'");
			}
		}

		if (isset($data['video_category_layout'])) {
			foreach ($data['video_category_layout'] as $store_id => $layout) {
				if ($layout) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "video_category_to_layout SET video_category_id = '" . (int)$video_category_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout['layout_id'] . "'");
				}
			}
		}

		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'video_category_id=" . (int)$video_category_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}

		$this->cache->delete('video_category');
	}

	public function editVideoCategory($video_category_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "video_category SET sort_order = '" . (int)$data['sort_order'] . "', bottom = '" . (isset($data['bottom']) ? (int)$data['bottom'] : 0) . "', status = '" . (int)$data['status'] . "' WHERE video_category_id = '" . (int)$video_category_id . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "video_category_description WHERE video_category_id = '" . (int)$video_category_id . "'");

		foreach ($data['video_category_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "video_category_description SET video_category_id = '" . (int)$video_category_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', description = '" . $this->db->escape($value['description']) . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "video_category_to_store WHERE video_category_id = '" . (int)$video_category_id . "'");

		if (isset($data['video_category_store'])) {
			foreach ($data['video_category_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "video_category_to_store SET video_category_id = '" . (int)$video_category_id . "', store_id = '" . (int)$store_id . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "video_category_to_layout WHERE video_category_id = '" . (int)$video_category_id . "'");

		if (isset($data['video_category_layout'])) {
			foreach ($data['video_category_layout'] as $store_id => $layout) {
				if ($layout['layout_id']) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "video_category_to_layout SET video_category_id = '" . (int)$video_category_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout['layout_id'] . "'");
				}
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'video_category_id=" . (int)$video_category_id. "'");

		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'video_category_id=" . (int)$video_category_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}

		$this->cache->delete('video_category');
	}

	public function deleteVideoCategory($video_category_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "video_category WHERE video_category_id = '" . (int)$video_category_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "video_category_description WHERE video_category_id = '" . (int)$video_category_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "video_category_to_store WHERE video_category_id = '" . (int)$video_category_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "video_category_to_layout WHERE video_category_id = '" . (int)$video_category_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'video_category_id=" . (int)$video_category_id . "'");

		$this->cache->delete('video_category');
	}	

	public function getVideoCategory($video_category_id) {
		$query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'video_category_id=" . (int)$video_category_id . "') AS keyword FROM " . DB_PREFIX . "video_category WHERE video_category_id = '" . (int)$video_category_id . "'");

		return $query->row;
	}

	public function getVideoCategories($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "video_category i LEFT JOIN " . DB_PREFIX . "video_category_description id ON (i.video_category_id = id.video_category_id) WHERE id.language_id = '" . (int)$this->config->get('config_language_id') . "'";

			$sort_data = array(
				'id.title',
				'i.sort_order'
			);		

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY id.title";	
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}		

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}	

			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			$video_category_data = $this->cache->get('video_category.' . (int)$this->config->get('config_language_id'));

			if (!$video_category_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "video_category i LEFT JOIN " . DB_PREFIX . "video_category_description id ON (i.video_category_id = id.video_category_id) WHERE id.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY id.title");

				$video_category_data = $query->rows;

				$this->cache->set('video_category.' . (int)$this->config->get('config_language_id'), $video_category_data);
			}	

			return $video_category_data;			
		}
	}

	public function getVideoCategoryDescriptions($video_category_id) {
		$video_category_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "video_category_description WHERE video_category_id = '" . (int)$video_category_id . "'");

		foreach ($query->rows as $result) {
			$video_category_description_data[$result['language_id']] = array(
				'title'       => $result['title'],
				'description' => $result['description']
			);
		}

		return $video_category_description_data;
	}

	public function getVideoCategoryStores($video_category_id) {
		$video_category_store_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "video_category_to_store WHERE video_category_id = '" . (int)$video_category_id . "'");

		foreach ($query->rows as $result) {
			$video_category_store_data[] = $result['store_id'];
		}

		return $video_category_store_data;
	}

	public function getVideoCategoryLayouts($video_category_id) {
		$video_category_layout_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "video_category_to_layout WHERE video_category_id = '" . (int)$video_category_id . "'");

		foreach ($query->rows as $result) {
			$video_category_layout_data[$result['store_id']] = $result['layout_id'];
		}

		return $video_category_layout_data;
	}

	public function getTotalVideoCategories() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "video_category");

		return $query->row['total'];
	}	

	public function getTotalVideoCategoriesByLayoutId($layout_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "video_category_to_layout WHERE layout_id = '" . (int)$layout_id . "'");

		return $query->row['total'];
	}	
}
?>