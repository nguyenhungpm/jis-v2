<?php
class ModelCatalogdictionary extends Model {

	public function adddictionary($data) {

		$this->db->query("INSERT INTO " . DB_PREFIX . "dictionary SET
		sort_order='". (int)$data['sort_order'] ."',
		keyword='". $data['keyword'] ."',
		layout_id = '". (int)$data['layout_id'] ."'");
		$dictionary_id = $this->db->getLastId();
		if(!empty($data['detail'])){
			foreach($data['detail'] as $language_id => $detail){
				$this->db->query("INSERT INTO " . DB_PREFIX . "dictionary_description SET dictionary_id = '". $dictionary_id ."', language_id = '". $language_id ."', name = '". $this->db->escape($detail['name']) ."'");
			}
		}

		// $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'dictionary_id=" . (int)$dictionary_id. "'");
		// if ($data['keyword']) {
		// 	$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE keyword = '" . $this->db->escape($data['keyword']) . "'");
		// 	$exist = $query->num_rows;
		// 	if($exist>0){
		// 		$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'dictionary_id=" . (int) $dictionary_id . "', keyword = '" . $dictionary_id .'-' . $this->db->escape($data['keyword']) . "'");
		// 	}else{
		// 		$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'dictionary_id=" . (int) $dictionary_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		// 	}
		// }

		$this->cache->delete('dictionary');
		$this->cache->delete('url_alias');
	}

	public function editdictionary($dictionary_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "dictionary SET
		sort_order='". (int)$data['sort_order'] . "',
		keyword='". $data['keyword'] . "',
		layout_id = '". (int)$data['layout_id'] ."'
		WHERE dictionary_id = '" . (int)$dictionary_id . "'");
		if(!empty($data['detail'])){
			$this->db->query("DELETE FROM " . DB_PREFIX . "dictionary_description WHERE dictionary_id = '". $dictionary_id ."'");
			foreach($data['detail'] as $language_id => $detail){
				$this->db->query("INSERT INTO " . DB_PREFIX . "dictionary_description SET dictionary_id = '". $dictionary_id ."', language_id = '". $language_id ."', name = '". $this->db->escape($detail['name']) ."'");
			}
		}
		// $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'dictionary_id=" . (int)$dictionary_id. "'");
		// if ($data['keyword']) {
		// 	$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE keyword = '" . $this->db->escape($data['keyword']) . "'");
		// 	$exist = $query->num_rows;
		// 	if($exist>0){
		// 		$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'dictionary_id=" . (int) $dictionary_id . "', keyword = '" . $dictionary_id .'-' . $this->db->escape($data['keyword']) . "'");
		// 	}else{
		// 		$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'dictionary_id=" . (int) $dictionary_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		// 	}
		// }
		return true;
	}

	public function deletedictionary($dictionary_id) {
			$this->db->query("DELETE FROM " . DB_PREFIX . "dictionary WHERE dictionary_id = '" . (int)$dictionary_id . "'");
			$this->db->query("DELETE FROM " . DB_PREFIX . "dictionary_description WHERE dictionary_id = '" . (int)$dictionary_id . "'");
	}

	public function getdictionary($dictionary_id) {
			$query = $this->db->query("SELECT  * FROM " . DB_PREFIX . "dictionary WHERE dictionary_id = '" . (int)$dictionary_id . "'");
			return $query->row;
	}

	public function getDetail($dictionary_id){
			$data = array();
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "dictionary_description WHERE dictionary_id = '". $dictionary_id ."'");
			foreach($query->rows as $rs){
				$data[$rs['language_id']] = array(
					'name' => $rs['name']
				);
			}
			return $data;
	}
	public function getKeyword($dictionary_id){
			$query = $this->db->query("SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'dictionary_id=" . (int)$dictionary_id . "'");
			if($query->num_rows){
				return $query->row['keyword'];
			}else{
				return false;
			}
	}
	public function getNamesCats() {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "dictionary_description WHERE language_id = '". (int) $this->config->get('config_language_id') ."'");
			return $query->rows;
	}
	public function getdictionaries($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "dictionary ORDER BY sort_order DESC";
			if (isset($data['start']) || isset($data['limit'])) {
	        if ($data['start'] < 0) {
	            $data['start'] = 0;
	        }

	        if ($data['limit'] < 1) {
	            $data['limit'] = 20;
	        }

	        $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
	    }
			$query = $this->db->query($sql);
			return $query->rows;
	}

	public function getTotaldictionaries($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "dictionary";
			$query = $this->db->query($sql);
			return $query->num_rows;
	}
}
?>
