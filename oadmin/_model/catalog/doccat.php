<?php
class ModelCatalogdoccat extends Model {

	public function adddoccat($data) {

		$this->db->query("INSERT INTO " . DB_PREFIX . "doccat SET
		sort_order='". (int)$data['sort_order'] ."',
		status = '". (int)$data['status'] ."'");
		$doccat_id = $this->db->getLastId();
		if(!empty($data['detail'])){
			foreach($data['detail'] as $language_id => $detail){
				$this->db->query("INSERT INTO " . DB_PREFIX . "doccat_description SET doccat_id = '". $doccat_id ."', language_id = '". $language_id ."', name = '". $this->db->escape($detail['name']) ."'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'doccat_id=" . (int)$doccat_id. "'");
		if ($data['keyword']) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE keyword = '" . $this->db->escape($data['keyword']) . "'");
			$exist = $query->num_rows;
			if($exist>0){
				$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'doccat_id=" . (int) $doccat_id . "', keyword = '" . $doccat_id .'-' . $this->db->escape($data['keyword']) . "'");
			}else{
				$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'doccat_id=" . (int) $doccat_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
			}
		}

		$this->cache->delete('doccat');
		$this->cache->delete('url_alias');
	}

	public function editdoccat($doccat_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "doccat SET
		sort_order='". (int)$data['sort_order'] . "',
		status = '". (int)$data['status'] ."'
		WHERE doccat_id = '" . (int)$doccat_id . "'");
		if(!empty($data['detail'])){
			$this->db->query("DELETE FROM " . DB_PREFIX . "doccat_description WHERE doccat_id = '". $doccat_id ."'");
			foreach($data['detail'] as $language_id => $detail){
				$this->db->query("INSERT INTO " . DB_PREFIX . "doccat_description SET doccat_id = '". $doccat_id ."', language_id = '". $language_id ."', name = '". $this->db->escape($detail['name']) ."'");
			}
		}
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'doccat_id=" . (int)$doccat_id. "'");
		if ($data['keyword']) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE keyword = '" . $this->db->escape($data['keyword']) . "'");
			$exist = $query->num_rows;
			if($exist>0){
				$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'doccat_id=" . (int) $doccat_id . "', keyword = '" . $doccat_id .'-' . $this->db->escape($data['keyword']) . "'");
			}else{
				$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'doccat_id=" . (int) $doccat_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
			}
		}
		return true;
	}

	public function deletedoccat($doccat_id) {
			$this->db->query("DELETE FROM " . DB_PREFIX . "doccat WHERE doccat_id = '" . (int)$doccat_id . "'");
			$this->db->query("DELETE FROM " . DB_PREFIX . "doccat_description WHERE doccat_id = '" . (int)$doccat_id . "'");
	}

	public function getdoccat($doccat_id) {
			$query = $this->db->query("SELECT  * FROM " . DB_PREFIX . "doccat WHERE doccat_id = '" . (int)$doccat_id . "'");
			return $query->row;
	}

	public function getDetail($doccat_id){
			$data = array();
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "doccat_description WHERE doccat_id = '". $doccat_id ."'");
			foreach($query->rows as $rs){
				$data[$rs['language_id']] = array(
					'name' => $rs['name']
				);
			}
			return $data;
	}
	public function getKeyword($doccat_id){
			$query = $this->db->query("SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'doccat_id=" . (int)$doccat_id . "'");
			if($query->num_rows){
				return $query->row['keyword'];
			}else{
				return false;
			}
	}
	public function getNamesCats() {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "doccat_description WHERE language_id = '". (int) $this->config->get('config_language_id') ."'");
			return $query->rows;
	}
	public function getdoccats($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "doccat ORDER BY sort_order DESC";
			if (isset($data['start']) || isset($data['limit'])) {
	        if ($data['start'] < 0) {
	            $data['start'] = 0;
	        }

	        if ($data['limit'] < 1) {
	            $data['limit'] = 20;
	        }

	        $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
	    }
			$query = $this->db->query($sql);
			return $query->rows;
	}

	public function getTotaldoccats($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "doccat";
			$query = $this->db->query($sql);
			return $query->num_rows;
	}
}
?>
