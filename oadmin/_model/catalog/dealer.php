<?php
class ModelCatalogDealer extends Model {

	public function changeSort($arrSort) {
        if (count($arrSort) > 0) {
            foreach ($arrSort as $key => $value) {
                $this->db->query("UPDATE " . DB_PREFIX . "dealer SET sort_order ='" . (int) $value . "'  WHERE dealer_id = '" . (int) $key . "'");
            }
        }
    }

	public function getDealers($data=array()){
		$sql = "SELECT * FROM " . DB_PREFIX . "dealer p LEFT JOIN " . DB_PREFIX . "dealer_description pd ON (p.dealer_id=pd.dealer_id) WHERE pd.language_id='" . (int)$this->config->get('config_language_id') . "'";
		if(!empty($data['district_id'])){
			$sql .= " AND p.district_id='". $data['district_id'] ."'";
		}
		if(!empty($data['filter_name'])){
			$sql .= " AND lower(pd.name) LIKE BINARY '%". $this->db->escape(utf8_strtolower($data['filter_name'])) ."%'";
		}
		if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getDealer($dealer_id){
		$query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'dealer_id=" . (int)$dealer_id . "') AS keyword FROM " . DB_PREFIX . "dealer WHERE dealer_id = '" . (int)$dealer_id . "'");

		return $query->row;
	}
	public function getDealerDescriptions($dealer_id) {
		$dealer_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "dealer_description WHERE dealer_id = '" . (int)$dealer_id . "'");

		foreach ($query->rows as $result) {
			$dealer_description_data[$result['language_id']] = array(
				'name'  => $result['name'],
				'address'  => $result['address']
			);
		}
		return $dealer_description_data;
	}
	public function addDealer($data){
		$this->db->query("INSERT INTO " . DB_PREFIX . "dealer SET telephone = '" . $data['telephone'] . "', email = '" . $data['email'] . "', product = '" . $data['product'] . "', sort_order = '" . (int)$data['sort_order'] . "', district_id = '" . (int)$data['district'] . "', province_id = '" . (int)$data['province'] . "', status = '" . (int)$data['status'] . "'");
		$dealer_id = $this->db->getLastId();
		foreach ($data['dealer_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "dealer_description SET dealer_id = '" . (int)$dealer_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', address = '" . $this->db->escape($value['address']) . "'");
		}
		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'dealer_id=" . (int)$dealer_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}
	}
	public function editDealer($dealer_id, $data){
		$this->db->query("UPDATE " . DB_PREFIX . "dealer SET telephone = '" . $data['telephone'] . "', email = '" . $data['email'] . "', product = '" . $data['product']. "', sort_order = '" . (int)$data['sort_order'] . "', district_id = '" . (int)$data['district'] . "', province_id = '" . (int)$data['province'] . "', status = '" . (int)$data['status'] . "' WHERE dealer_id = '" . (int)$dealer_id . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "dealer_description WHERE dealer_id = '" . (int)$dealer_id . "'");
		foreach ($data['dealer_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "dealer_description SET dealer_id = '" . (int)$dealer_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', address = '" . $this->db->escape($value['address']) . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'dealer_id=" . (int)$dealer_id. "'");

		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'dealer_id=" . (int)$dealer_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}
	}
	public function deleteDealer($dealer_id) {
		/* $query = $this->db->query("SELECT dealer_id FROM " . DB_PREFIX . "dealer WHERE dealer_id = '" . (int)$dealer_id . "'"); */
		/* foreach ($query->rows as $result) {
			$this->deleteArea($result['news_category_id']);
		} */

		$this->db->query("DELETE FROM " . DB_PREFIX . "dealer_description WHERE dealer_id = '" . (int)$dealer_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "dealer WHERE dealer_id = '" . (int)$dealer_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'dealer_id=" . (int)$dealer_id . "'");

	}

	public function getTotalDealers($data=array()) {
      $sql = "SELECT count(*) as total FROM " . DB_PREFIX . "dealer p LEFT JOIN " . DB_PREFIX . "dealer_description pd ON (p.dealer_id=pd.dealer_id) WHERE pd.language_id='" . (int)$this->config->get('config_language_id') . "'";
			if(!empty($data['district_id'])){
				$sql .= " AND p.district_id='". $data['district_id'] ."'";
			}
			if(!empty($data['filter_name'])){
				$sql .= " AND lower(pd.name) LIKE BINARY '%". $this->db->escape(utf8_strtolower($data['filter_name'])) ."%'";
			}
			$query = $this->db->query($sql);

			return $query->row['total'];
	}

	public function getTotalNewsCategoriesByImageId($image_id) {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "news_category WHERE image_id = '" . (int)$image_id . "'");

		return $query->row['total'];
	}

	public function getTotalNewsCategoriesByLayoutId($layout_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "news_category_to_layout WHERE layout_id = '" . (int)$layout_id . "'");

		return $query->row['total'];
	}

}
?>
