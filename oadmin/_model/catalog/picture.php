<?php
class ModelCatalogPicture extends Model {
	public function addPicture($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "picture SET name = '" . $this->db->escape($data['name']) . "', status = '" . (int)$data['status'] . "', sort_order = '" . (int)$data['sort_order'] . "', date_added = NOW()");

		$picture_id = $this->db->getLastId();

		if (isset($data['picture_image'])) {
			foreach ($data['picture_image'] as $picture_image) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "picture_image SET picture_id = '" . (int)$picture_id . "'");

				$picture_image_id = $this->db->getLastId();

				foreach ($picture_image['picture_image_description'] as $language_id => $picture_image_description) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "picture_image_description SET picture_image_id = '" . (int)$picture_image_id . "', language_id = '" . (int)$language_id . "', picture_id = '" . (int)$picture_id . "', title = '" .  $this->db->escape($picture_image_description['title']) . "', description = '" .  $this->db->escape($picture_image_description['description']) . "', link = '" .  $this->db->escape($picture_image_description['link']) . "', image = '" .  $this->db->escape($picture_image_description['image']) . "'");
				}
			}
		}
	}

	public function editPicture($picture_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "picture SET name = '" . $this->db->escape($data['name']) . "', status = '" . (int)$data['status'] . "', sort_order = '" . (int)$data['sort_order'] . "' WHERE picture_id = '" . (int)$picture_id . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "picture_image WHERE picture_id = '" . (int)$picture_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "picture_image_description WHERE picture_id = '" . (int)$picture_id . "'");

		if (isset($data['picture_image'])) {
			foreach ($data['picture_image'] as $picture_image) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "picture_image SET picture_id = '" . (int)$picture_id . "'");

				$picture_image_id = $this->db->getLastId();

				foreach ($picture_image['picture_image_description'] as $language_id => $picture_image_description) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "picture_image_description SET picture_image_id = '" . (int)$picture_image_id . "', language_id = '" . (int)$language_id . "', picture_id = '" . (int)$picture_id . "', title = '" .  $this->db->escape($picture_image_description['title']) . "', description = '" .  $this->db->escape($picture_image_description['description']) . "', link = '" .  $this->db->escape($picture_image_description['link']) . "', image = '" .  $this->db->escape($picture_image_description['image']) . "'");
				}
			}
		}
	}

	public function deletePicture($picture_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "picture WHERE picture_id = '" . (int)$picture_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "picture_image WHERE picture_id = '" . (int)$picture_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "picture_image_description WHERE picture_id = '" . (int)$picture_id . "'");
	}

	public function getPicture($picture_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "picture WHERE picture_id = '" . (int)$picture_id . "'");

		return $query->row;
	}

	public function getPictures($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "picture";

		$sort_data = array(
			'name',
			'sort_order DESC, date_added',
			'status'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		// echo $sql;
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getPictureImages($picture_id) {
		$picture_image_data = array();

		$picture_image_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "picture_image WHERE picture_id = '" . (int)$picture_id . "'");

		foreach ($picture_image_query->rows as $picture_image) {
			$picture_image_description_data = array();

			$picture_image_description_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "picture_image_description WHERE picture_image_id = '" . (int)$picture_image['picture_image_id'] . "' AND picture_id = '" . (int)$picture_id . "'");

			foreach ($picture_image_description_query->rows as $picture_image_description) {

				if (!empty($picture_image_description['image']) && file_exists(DIR_IMAGE . $picture_image_description['image'])) {
					$image = $picture_image_description['image'];
				} else {
					$image = 'no_image.jpg';
				}

				$picture_image_description_data[$picture_image_description['language_id']] = array(
				'title' => $picture_image_description['title'],
				'description' => $picture_image_description['description'],
				'link'  => $picture_image_description['link'],
				'image' => $image,
				'thumb' => $this->model_tool_image->resize($image, 100, 100));
			}

			$picture_image_data[] = array(
				'picture_image_description' => $picture_image_description_data
			);
		}

		return $picture_image_data;
	}

	public function getTotalPictures() {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "picture");

		return $query->row['total'];
	}
}
?>
