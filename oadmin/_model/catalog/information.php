<?php
class ModelCatalogInformation extends Model {
	public function addInformation($data) {
		if(!isset($data['status'])){$data['status']=0;}
		if(!isset($data['home'])){$data['home']=0;}
		$this->db->query("INSERT INTO " . DB_PREFIX . "information SET sort_order = '" . (int)$data['sort_order'] . "', robots = '" . $this->db->escape($data['robots']) . "', code_youtube = '" . $this->db->escape($data['code_youtube']) . "', bottom = '" . (isset($data['bottom']) ? (int)$data['bottom'] : 0) . "', status = '" . (int)$data['status'] . "', home = '" . (int)$data['home'] . "', category_id = '" . (int)$data['category_id'] . "'");

		$information_id = $this->db->getLastId();

		foreach ($data['information_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "information_description SET information_id = '" . (int)$information_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) .  "', description = '" . $this->db->escape($value['description']) . "', expand = '" . $this->db->escape($value['expand']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}

        if (isset($data['information_highlight'])) {
            foreach ($data['information_highlight'] as $information_highlight) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "information_highlight SET information_id = '" . (int) $information_id . "', image = '" . $this->db->escape(html_entity_decode($information_highlight['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int) $information_highlight['sort_order'] . "'");

				$information_highlight_id = $this->db->getLastId();
				foreach ($information_highlight['description'] as $language_id => $description) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "information_highlight_description SET information_highlight_id = '" . (int) $information_highlight_id . "', language_id = '" . (int)$language_id . "', description = '" . $this->db->escape($description['description']) . "'");
            	}
            }
        }

		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'information_id=" . (int)$information_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}

		$this->cache->delete('information');
	}

	public function editInformation($information_id, $data) {
		if(!isset($data['status'])){$data['status']=0;}
		if(!isset($data['home'])){$data['home']=0;}
		$this->db->query("UPDATE " . DB_PREFIX . "information SET sort_order = '" . (int)$data['sort_order'] . "', robots = '" . $this->db->escape($data['robots']) . "', code_youtube = '" . $this->db->escape($data['code_youtube']) . "', bottom = '" . (isset($data['bottom']) ? (int)$data['bottom'] : 0) . "', status = '" . (int)$data['status'] . "', home = '" . (int)$data['home'] . "', category_id = '" . (int)$data['category_id'] . "' WHERE information_id = '" . (int)$information_id . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "information_description WHERE information_id = '" . (int)$information_id . "'");

		foreach ($data['information_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "information_description SET information_id = '" . (int)$information_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) .  "', description = '" . $this->db->escape($value['description']) . "', expand = '" . $this->db->escape($value['expand']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}

		
        if (isset($data['information_highlight'])) {
			$this->db->query("DELETE FROM " . DB_PREFIX . "information_highlight WHERE information_id = '" . (int) $information_id . "'");
            foreach ($data['information_highlight'] as $information_highlight) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "information_highlight SET information_id = '" . (int) $information_id . "', image = '" . $this->db->escape(html_entity_decode($information_highlight['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int) $information_highlight['sort_order'] . "'");
				
				$information_highlight_id = $this->db->getLastId();
				$this->db->query("DELETE FROM " . DB_PREFIX . "information_highlight_description WHERE information_highlight_id = '" . (int) $information_highlight_id . "'");
				foreach ($information_highlight['description'] as $language_id => $description) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "information_highlight_description SET information_highlight_id = '" . (int) $information_highlight_id . "', language_id = '" . (int)$language_id . "', description = '" . $this->db->escape($description['description']) . "'");
            	}
            }
        }


		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'information_id=" . (int)$information_id. "'");

		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'information_id=" . (int)$information_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}

		$this->cache->delete('information');
	}

	public function deleteInformation($information_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "information WHERE information_id = '" . (int)$information_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "information_highlight WHERE information_id = '" . (int) $information_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "information_highlight_description WHERE information_highlight_id IN (SELECT information_highlight_id FROM " . DB_PREFIX . "information_highlight WHERE information_id = '" . (int)$information_id . "')");
		$this->db->query("DELETE FROM " . DB_PREFIX . "information_description WHERE information_id = '" . (int)$information_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'information_id=" . (int)$information_id . "'");

		$this->cache->delete('information');
	}

	public function getInformation($information_id) {
		$query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'information_id=" . (int)$information_id . "') AS keyword FROM " . DB_PREFIX . "information WHERE information_id = '" . (int)$information_id . "'");

		return $query->row;
	}

    public function getInformationHighlights($information_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "information_highlight WHERE information_id = '" . (int) $information_id . "'");

        return $query->rows;
    }

	public function getInformationHighlightDescriptions($information_highlight_id) {
		$information_highlight_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "information_highlight_description WHERE information_highlight_id = '" . (int)$information_highlight_id . "'");

		foreach ($query->rows as $result) {
			$information_highlight_description_data[$result['language_id']] = array(
				'description' => $result['description'],
			);
		}

		return $information_highlight_description_data;
	}

	public function getInformations($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "information i LEFT JOIN " . DB_PREFIX . "information_description id ON (i.information_id = id.information_id) WHERE id.language_id = '" . (int)$this->config->get('config_language_id') . "'";

			$sort_data = array(
				'id.title',
				'i.sort_order'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY id.title";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			$information_data = $this->cache->get('information.' . (int)$this->config->get('config_language_id'));

			if (!$information_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "information i LEFT JOIN " . DB_PREFIX . "information_description id ON (i.information_id = id.information_id) WHERE id.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY id.title");

				$information_data = $query->rows;

				$this->cache->set('information.' . (int)$this->config->get('config_language_id'), $information_data);
			}

			return $information_data;
		}
	}

	public function getInformationDescriptions($information_id) {
		$information_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "information_description WHERE information_id = '" . (int)$information_id . "'");

		foreach ($query->rows as $result) {
			$information_description_data[$result['language_id']] = array(
				'title'       => $result['title'],
				'description' => $result['description'],
				'expand' => $result['expand'],
				'meta_description' => $result['meta_description'],
				'meta_keyword' => $result['meta_keyword'],
				'meta_title' => $result['meta_title']
			);
		}

		return $information_description_data;
	}


	public function getTotalInformations() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "information");

		return $query->row['total'];
	}
}
?>
