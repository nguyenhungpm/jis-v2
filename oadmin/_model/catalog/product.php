<?php

class ModelCatalogProduct extends Model {

    public function checkKeywords($data = array()) {

        if (!empty($data['product_id'])) {
          $sql = "SELECT COUNT(keyword) AS total FROM " . DB_PREFIX . "url_alias WHERE keyword = '" . $data['keyword'] . "' AND NOT(query = 'product_id=" . (int) $data['product_id'] . "')";
        } else {
            $sql = "SELECT COUNT('keyword') AS total FROM " . DB_PREFIX . "url_alias WHERE keyword = '" . $data['keyword'] . "'";
        }

        $query = $this->db->query($sql);
        return $query->row['total'];

    }

    public function getProductNews($product_id) {
        $product_filter_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_news WHERE product_id = '" . (int) $product_id . "'");

        foreach ($query->rows as $result) {
            $product_filter_data[] = $result['news_id'];
        }

        return $product_filter_data;
    }

    public function getNameProduct($product_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "product_description n LEFT JOIN " . DB_PREFIX . "product nd ON (n.product_id = nd.product_id) WHERE nd.product_id = '" . (int) $product_id . "'");

        return $query->row;
    }

    // public function addProduct($data) {
    // $this->db->query("INSERT INTO " . DB_PREFIX . "product SET model = '" . $this->db->escape($data['model']) . "', sku = '" . $this->db->escape($data['sku']) . "', upc = '" . $this->db->escape($data['upc']) . "', ean = '" . $this->db->escape($data['ean']) . "', jan = '" . $this->db->escape($data['jan']) . "', isbn = '" . $this->db->escape($data['isbn']) . "', mpn = '" . $this->db->escape($data['mpn']) . "', location = '" . $this->db->escape($data['location']) . "', quantity = '" . (int)$data['quantity'] . "', minimum = '" . (int)$data['minimum'] . "', subtract = '" . (int)$data['subtract'] . "', stock_status_id = '" . (int)$data['stock_status_id'] . "', date_available = '" . $this->db->escape($data['date_available']) . "', robots = '" . $this->db->escape($data['robots']) . "', manufacturer_id = '" . (int)$data['manufacturer_id'] . "', shipping = '" . (int)$data['shipping'] . "', price = '" . (float)$data['price'] . "', points = '" . (int)$data['points'] . "', weight = '" . (float)$data['weight'] . "', weight_class_id = '" . (int)$data['weight_class_id'] . "', length = '" . (float)$data['length'] . "', width = '" . (float)$data['width'] . "', height = '" . (float)$data['height'] . "', length_class_id = '" . (int)$data['length_class_id'] . "', status = '" . (int)$data['status'] . "', tax_class_id = '" . $this->db->escape($data['tax_class_id']) . "', sort_order = '" . (int)$data['sort_order'] . "', date_added = NOW()");
    public function addProduct($data) {
        if(!isset($data['status'])){$data['status']=0;}
        if(!isset($data['quantity'])){$data['quantity']=0;}
        $this->db->query("INSERT INTO " . DB_PREFIX . "product SET model = '" . $this->db->escape($data['model'])  . "', quantity = '" . (int)$data['quantity'] . "', date_available = '" . $this->db->escape($data['date_available']) . "', robots = '" . $this->db->escape($data['robots']) . "', manufacturer_id = '" . (int)$data['manufacturer_id'] . "', price = '" . (float) $data['price'] . "',status = '" . (int) $data['status'] . "', sort_order = '" . (int) $data['sort_order'] . "', date_added = NOW()");

        $product_id = $this->db->getLastId();

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "product SET image = '" . $this->db->escape(html_entity_decode($data['image'], ENT_QUOTES, 'UTF-8')) . "' WHERE product_id = '" . (int) $product_id . "'");
        }

        foreach ($data['product_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET product_id = '" . (int) $product_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape(trim($value['name'])) . "', title = '" . $this->db->escape(trim($value['title'])) . "',  short_description = '" . $this->db->escape($value['short_description']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "', description = '" . $this->db->escape($value['description']) . "', tag = '" . $this->db->escape($value['tag']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', include = '" . $this->db->escape($value['include']) . "', not_include = '" . $this->db->escape($value['not_include']) . "'");
        }

        if (isset($data['product_news'])) {
            foreach ($data['product_news'] as $news_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_news SET product_id = '" . (int) $product_id . "', news_id = '" . (int) $news_id . "'");
            }
        }

        if (isset($data['product_special'])) {
            foreach ($data['product_special'] as $product_special) {
                if ((int) $product_special['priority']) {
                    $quantity_special = $data['price'] - ($data['price'] * ($product_special['priority'] * 0.01));
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int) $product_id . "', customer_group_id = '" . (int) $product_special['customer_group_id'] . "', priority = '" . (int) $product_special['priority'] . "', price = '" . (float) $quantity_special . "', date_start = '" . $this->db->escape($product_special['date_start']) . "', date_end = '" . $this->db->escape($product_special['date_end']) . "'");
                } else {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int) $product_id . "', customer_group_id = '" . (int) $product_special['customer_group_id'] . "', priority = '" . (int) $product_special['priority'] . "', price = '" . (float) $product_special['price'] . "', date_start = '" . $this->db->escape($product_special['date_start']) . "', date_end = '" . $this->db->escape($product_special['date_end']) . "'");
                }
            }
        }

        if (isset($data['product_image'])) {
            foreach ($data['product_image'] as $product_image) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int) $product_id . "', image = '" . $this->db->escape(html_entity_decode($product_image['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int) $product_image['sort_order'] . "', title = '" . $this->db->escape($product_image['title']) . "', description = '" . $this->db->escape($product_image['description']) . "'");
            }
        }
				
				if (isset($data['product_schedule'])) {
            foreach ($data['product_schedule'] as $product_schedule) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_schedule SET product_id = '" . (int) $product_id . "', tips = '" . $this->db->escape($product_schedule['tips']) . "', sort_order = '" . (int) $product_schedule['sort_order'] . "', title = '" . $this->db->escape($product_schedule['title']) . "', time = '" . $this->db->escape($product_schedule['time']) . "', detail = '" . serialize($product_schedule['detail']) . "'");
            }
        }
				
				if (isset($data['product_testimonial'])) {
            foreach ($data['product_testimonial'] as $product_testimonial) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "testimonial SET product_id = '" . (int) $product_id . "', name = '" . $this->db->escape($product_testimonial['name']) . "', title = '" . $this->db->escape($product_testimonial['title']) . "', description = '" . $this->db->escape($product_testimonial['description']) . "', image = '" . $this->db->escape(html_entity_decode($product_testimonial['image'], ENT_QUOTES, 'UTF-8')) . "', avatar = '" . $this->db->escape(html_entity_decode($product_testimonial['avatar'], ENT_QUOTES, 'UTF-8')) . "'");
            }
        }

        if (isset($data['category'])) {
            $category = explode(',', $data['category']);
            foreach ($category as $category_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int) $product_id . "', category_id = '" . (int) $category_id . "'");
            }
        }

        if (isset($data['product_related'])) {
            foreach ($data['product_related'] as $related_id) {
                $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int) $product_id . "' AND related_id = '" . (int) $related_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int) $product_id . "', related_id = '" . (int) $related_id . "'");
                $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int) $related_id . "' AND related_id = '" . (int) $product_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int) $related_id . "', related_id = '" . (int) $product_id . "'");
            }
        }

        if ($data['keyword']) {
            if ((!isset($data['seo']) || isset($data['seo']) && $data['seo'] == 1)) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'product_id=" . (int) $product_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
            }
        }

        $this->cache->delete('product');

return $product_id;

    }

public function editProduct($product_id, $data) {
        if(!isset($data['status'])){$data['status']=0;}
        if(!isset($data['quantity'])){$data['quantity']=0;}
        $this->db->query("UPDATE " . DB_PREFIX . "product SET model = '" . $this->db->escape($data['model']) . "', date_available = '" . $this->db->escape($data['date_available']) . "', robots = '" . $this->db->escape($data['robots']) . "', manufacturer_id = '" . (int)$data['manufacturer_id']  . "', quantity = '" . (int)$data['quantity'] . "', price = '" . (float) $data['price'] . "', status = '" . (int) $data['status'] . "', sort_order = '" . (int) $data['sort_order'] . "', date_modified = NOW() WHERE product_id = '" . (int) $product_id . "'");

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "product SET image = '" . $this->db->escape(html_entity_decode($data['image'], ENT_QUOTES, 'UTF-8')) . "' WHERE product_id = '" . (int) $product_id . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int) $product_id . "'");

        foreach ($data['product_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET product_id = '" . (int) $product_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape(trim($value['name'])) . "', title = '" . $this->db->escape(trim($value['title'])) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "', short_description = '" . $this->db->escape($value['short_description']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', description = '" . $this->db->escape($value['description']) . "', tag = '" . $this->db->escape($value['tag']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', include = '" . $this->db->escape($value['include']) . "', not_include = '" . $this->db->escape($value['not_include']) . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_news WHERE product_id = '" . (int) $product_id . "'");

        if (isset($data['product_news'])) {
            foreach ($data['product_news'] as $news_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_news SET product_id = '" . (int) $product_id . "', news_id = '" . (int) $news_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int) $product_id . "'");

        if (isset($data['product_special'])) {
            foreach ($data['product_special'] as $product_special) {
                if ((int) $product_special['priority']) {
                    $quantity_special = $data['price'] - ($data['price'] * ($product_special['priority'] * 0.01));
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int) $product_id . "', customer_group_id = '" . (int) $product_special['customer_group_id'] . "', priority = '" . (int) $product_special['priority'] . "', price = '" . (float) $quantity_special . "', date_start = '" . $this->db->escape($product_special['date_start']) . "', date_end = '" . $this->db->escape($product_special['date_end']) . "'");
                } else {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int) $product_id . "', customer_group_id = '" . (int) $product_special['customer_group_id'] . "', priority = '" . (int) $product_special['priority'] . "', price = '" . (float) $product_special['price'] . "', date_start = '" . $this->db->escape($product_special['date_start']) . "', date_end = '" . $this->db->escape($product_special['date_end']) . "'");
                }
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int) $product_id . "'");

        if (isset($data['product_image'])) {
            foreach ($data['product_image'] as $product_image) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int) $product_id . "', image = '" . $this->db->escape(html_entity_decode($product_image['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int) $product_image['sort_order'] . "', title = '" . $this->db->escape($product_image['title']) . "', description = '" . $this->db->escape($product_image['description']) . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_schedule WHERE product_id = '" . (int) $product_id . "'");

        if (isset($data['product_schedule'])) {
					// $this->session->data['d'] = $data['product_schedule'];
            foreach ($data['product_schedule'] as $product_schedule) {
							// $this->session->data['d'] = $product_schedule['detail'];
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_schedule SET product_id = '" . (int) $product_id . "', tips = '" . $this->db->escape($product_schedule['tips']) . "', sort_order = '" . (int) $product_schedule['sort_order'] . "', title = '" . $this->db->escape($product_schedule['title']) . "', time = '" . $this->db->escape($product_schedule['time']) . "', detail = '" . serialize($product_schedule['detail']) . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "testimonial WHERE product_id = '" . (int) $product_id . "'");

        if (isset($data['product_testimonial'])) {
            foreach ($data['product_testimonial'] as $product_testimonial) {
							// $this->session->data['d'] = $product_testimonial['detail'];
                $this->db->query("INSERT INTO " . DB_PREFIX . "testimonial SET product_id = '" . (int) $product_id . "', name = '" . $this->db->escape($product_testimonial['name']) . "', title = '" . $this->db->escape($product_testimonial['title']) . "', description = '" . $this->db->escape($product_testimonial['description']) . "', image = '" . $this->db->escape(html_entity_decode($product_testimonial['image'], ENT_QUOTES, 'UTF-8')) . "', avatar = '" . $this->db->escape(html_entity_decode($product_testimonial['avatar'], ENT_QUOTES, 'UTF-8')) . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int) $product_id . "'");

        if (isset($data['category'])) {
            $category = explode(',', $data['category']);
            foreach ($category as $category_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int) $product_id . "', category_id = '" . (int) $category_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int) $product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE related_id = '" . (int) $product_id . "'");

        if (isset($data['product_related'])) {
            foreach ($data['product_related'] as $related_id) {
                $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int) $product_id . "' AND related_id = '" . (int) $related_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int) $product_id . "', related_id = '" . (int) $related_id . "'");
                $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int) $related_id . "' AND related_id = '" . (int) $product_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int) $related_id . "', related_id = '" . (int) $product_id . "'");
            }
        }


//        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id=" . (int) $product_id . "'");
//
//        if ($data['keyword']) {
//            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'product_id=" . (int) $product_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
//        }
        if ((!isset($data['seo']) || isset($data['seo']) && $data['seo'] == 1)) {
            $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id=" . (int) $product_id . "'");

            if ($data['keyword']) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'product_id=" . (int) $product_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
            }
        }

    }

    public function copyProduct($product_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id = '" . (int) $product_id . "' AND pd.language_id = '" . (int) $this->config->get('config_language_id') . "'");

        if ($query->num_rows) {
            $data = array();

            $data = $query->row;

            $data['sku'] = '';
            $data['upc'] = '';
            $data['viewed'] = '0';
            $data['keyword'] = '';
            $data['status'] = '0';

            $data = array_merge($data, array('product_description' => $this->getProductDescriptions($product_id)));
            $data = array_merge($data, array('product_image' => $this->getProductImages($product_id)));
            $data = array_merge($data, array('product_schedule' => $this->getProductSchedules($product_id)));
            $data = array_merge($data, array('product_testimonial' => $this->getProductTestimonials($product_id)));
            $data = array_merge($data, array('product_related' => $this->getProductRelated($product_id)));
            $data = array_merge($data, array('product_special' => $this->getProductSpecials($product_id)));
            $data = array_merge($data, array('product_category' => $this->getProductCategories($product_id)));
            $this->addProduct($data);
        }
    }

    public function deleteProduct($product_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "product WHERE product_id = '" . (int) $product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int) $product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_schedule WHERE product_id = '" . (int) $product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "testimonial WHERE product_id = '" . (int) $product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int) $product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int) $product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE related_id = '" . (int) $product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int) $product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int) $product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_news WHERE product_id = '" . (int) $product_id . "'");
        // $this->db->query("DELETE FROM " . DB_PREFIX . "review WHERE product_id = '" . (int) $product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id=" . (int) $product_id . "'");

        $this->cache->delete('product');
    }


    public function updateStatus($product_id, $column_name, $value){
        $this->db->query("UPDATE " . DB_PREFIX . "product SET " . $column_name . " = '" . (int)$value . "' WHERE product_id = '" . (int)$product_id . "'");
    }

    public function getProduct($product_id) {
        $query = $this->db->query("SELECT DISTINCT *, (SELECT DISTINCT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id=" . (int) $product_id . "' LIMIT 1) AS keyword FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id = '" . (int) $product_id . "' AND pd.language_id = '" . (int) $this->config->get('config_language_id') . "'");

        return $query->row;
    }

    public function getProducts($data = array()) {

//        osdvn
        if (isset($data['display_name']) && $data['display_name'] == true) {
            if (isset($data['filter_category']) && !is_null($data['filter_category'])) {
                $name_category = ",pd.product_id as product_id ,pd.name as name, cd.name as name_category";
                $select_category = " LEFT JOIN " . DB_PREFIX . "product_to_category  p2c ON (p.product_id = p2c.product_id) LEFT JOIN " . DB_PREFIX . "category_description cd ON (p2c.category_id = cd.category_id) ";
                $filter_category = " AND cd.category_id = '" . $this->db->escape($data['filter_category']) . "' ";
            } else {
                $name_category = ",pd.product_id as product_id,pd.name as name, cd.name as name_category";
                $select_category = " LEFT JOIN " . DB_PREFIX . "product_to_category  p2c ON (p.product_id = p2c.product_id) LEFT JOIN " . DB_PREFIX . "category_description cd ON (p2c.category_id = cd.category_id) ";
                $filter_category = "";
            }
        } else {
            $select_category = "";
            $filter_category = "";
            $name_category = "";
        }
//        end

        $sql = "SELECT * " . $name_category . " FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) " . $select_category;

        if (!empty($data['filter_category_id'])) {
            $sql .= " LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id)";
        }

        $sql .= " WHERE pd.language_id = '" . (int) $this->config->get('config_language_id') . "' " . $filter_category;

        if (!empty($data['filter_name'])) {
            $sql .= " AND pd.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!empty($data['filter_model'])) {
            $sql .= " AND p.model LIKE '" . $this->db->escape($data['filter_model']) . "%'";
        }

        if (!empty($data['filter_price'])) {
            $sql .= " AND p.price LIKE '" . $this->db->escape($data['filter_price']) . "%'";
        }

        if (isset($data['filter_quantity']) && !is_null($data['filter_quantity'])) {
            $sql .= " AND p.quantity = '" . $this->db->escape($data['filter_quantity']) . "'";
        }

        if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
            $sql .= " AND p.status = '" . (int) $data['filter_status'] . "'";
        }

        $sql .= " GROUP BY p.product_id";

        $sort_data = array(
            'pd.name',
            'p.model',
            'p.price',
            'p.quantity',
            'p.status',
            'p.sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY pd.name";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getProductsByCategoryId($category_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id) WHERE pd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND p2c.category_id = '" . (int) $category_id . "' ORDER BY pd.name ASC");

        return $query->rows;
    }

    public function getProductDescriptions($product_id) {
        $product_description_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int) $product_id . "'");

        foreach ($query->rows as $result) {
            $product_description_data[$result['language_id']] = array(
                'name' => $result['name'],
                'title' => $result['title'],
                'description' => $result['description'],
                'meta_keyword' => $result['meta_keyword'],
                'short_description' => $result['short_description'],
                'meta_description' => $result['meta_description'],
                'tag' => $result['tag'],
                'include' => $result['include'],
                'not_include' => $result['not_include'],
                'meta_title' => $result['meta_title']
            );
        }

        return $product_description_data;
    }

    public function getProductCategories($product_id) {
        $product_category_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int) $product_id . "'");

        foreach ($query->rows as $result) {
            $product_category_data[] = $result['category_id'];
        }

        return $product_category_data;
    }

    public function getProductFilters($product_id) {
        $product_filter_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_filter WHERE product_id = '" . (int) $product_id . "'");

        foreach ($query->rows as $result) {
            $product_filter_data[] = $result['filter_id'];
        }

        return $product_filter_data;
    }

    public function getProductAttributes($product_id) {
        return false;
    }

    public function getProductOptions($product_id) {
        return false;
    }

    public function getProductImages($product_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int) $product_id . "'");

        return $query->rows;
    }

    public function getProductSchedules($product_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_schedule WHERE product_id = '" . (int) $product_id . "' ORDER BY sort_order asc");

        return $query->rows;
    }

    public function getProductTestimonials($product_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "testimonial WHERE product_id = '" . (int) $product_id . "'");

        return $query->rows;
    }

    public function getProductDiscounts($product_id) {
        return false;
    }

    public function getProductSpecials($product_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int) $product_id . "' ORDER BY priority, price");

        return $query->rows;
    }

    public function getProductRewards($product_id) {
        return false;
    }

    public function getProductDownloads($product_id) {
        return false;
    }

    public function getProductStores($product_id) {
        return false;
    }

    public function getProductRelated($product_id) {
        $product_related_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int) $product_id . "'");

        foreach ($query->rows as $result) {
            $product_related_data[] = $result['related_id'];
        }

        return $product_related_data;
    }

    public function getProfiles($product_id) {
        return false;
    }

    public function getTotalProducts($data = array()) {
        $sql = "SELECT COUNT(DISTINCT p.product_id) AS total FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)";

        if (!empty($data['filter_category_id'])) {
            $sql .= " LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id)";
        }

        $sql .= " WHERE pd.language_id = '" . (int) $this->config->get('config_language_id') . "'";

        if (!empty($data['filter_name'])) {
            $sql .= " AND pd.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!empty($data['filter_category'])) {
            $sql .= " AND p.product_id IN (SELECT product_id FROM " . DB_PREFIX . "product_to_category WHERE category_id = '" . (int)$data['filter_category'] . "')";
        }

        if (!empty($data['filter_model'])) {
            $sql .= " AND p.model LIKE '" . $this->db->escape($data['filter_model']) . "%'";
        }

        if (!empty($data['filter_price'])) {
            $sql .= " AND p.price LIKE '" . $this->db->escape($data['filter_price']) . "%'";
        }

        if (isset($data['filter_quantity']) && !is_null($data['filter_quantity'])) {
            $sql .= " AND p.quantity = '" . $this->db->escape($data['filter_quantity']) . "'";
        }

        if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
            $sql .= " AND p.status = '" . (int) $data['filter_status'] . "'";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getTotalProductsByTaxClassId($tax_class_id) {
        return FALSE;
    }

    public function getTotalProductsByStockStatusId($stock_status_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE stock_status_id = '" . (int) $stock_status_id . "'");

        return $query->row['total'];
    }

    public function getTotalProductsByDownloadId($download_id) {
        return false;
    }

    public function getTotalProductsByManufacturerId($manufacturer_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE manufacturer_id = '" . (int) $manufacturer_id . "'");

        return $query->row['total'];
    }

    public function getTotalProductsByAttributeId($attribute_id) {
        return false;
    }

    public function getTotalProductsByOptionId($option_id) {
        return false;
    }

}
?>
