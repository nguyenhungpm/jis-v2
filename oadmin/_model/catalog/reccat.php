<?php
class ModelCatalogReccat extends Model {

	public function addreccat($data) {
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "reccat SET sort_order='". (int)$data['sort_order'] ."', status = '". (int)$data['status'] ."'");
		$reccat_id = $this->db->getLastId();
		if(!empty($data['detail'])){
			foreach($data['detail'] as $language_id => $detail){
				$this->db->query("INSERT INTO " . DB_PREFIX . "reccat_description SET reccat_id = '". $reccat_id ."', language_id = '". $language_id ."', name = '". $this->db->escape($detail['name']) ."'");
			}
		}
	}
	
	public function editRecCat($reccat_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "reccat SET sort_order='". (int)$data['sort_order'] . "', status = '". (int)$data['status'] ."' WHERE reccat_id = '" . (int)$reccat_id . "'");
		if(!empty($data['detail'])){
			$this->db->query("DELETE FROM " . DB_PREFIX . "reccat_description WHERE reccat_id = '". $reccat_id ."'");
			foreach($data['detail'] as $language_id => $detail){
				$this->db->query("INSERT INTO " . DB_PREFIX . "reccat_description SET reccat_id = '". $reccat_id ."', language_id = '". $language_id ."', name = '". $this->db->escape($detail['name']) ."'");
			}
		}
		return true;
	}
	
	public function deleteRecCat($reccat_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "reccat WHERE reccat_id = '" . (int)$reccat_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "reccat_description WHERE reccat_id = '" . (int)$reccat_id . "'");
	}
	
	public function getRecCat($reccat_id) {
		$query = $this->db->query("SELECT  * FROM " . DB_PREFIX . "reccat WHERE reccat_id = '" . (int)$reccat_id . "'");

		return $query->row;
	}
	
	public function getDetail($reccat_id){
		$data = array();
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "reccat_description WHERE reccat_id = '". $reccat_id ."'");
		foreach($query->rows as $rs){
			$data[$rs['language_id']] = array(
				'name' => $rs['name']
			);
		}
		return $data;
	}
	public function getNamesCats() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "reccat_description WHERE language_id = '". (int) $this->config->get('config_language_id') ."'");
		return $query->rows;

	}
	public function getRecCats($data) {
		
		$sql = "SELECT * FROM " . DB_PREFIX . "reccat ORDER BY sort_order DESC";
		if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }
			
		$query = $this->db->query($sql);
		
		return $query->rows;
	}
	
	public function getTotalRecCats($data) {
		$sql = "SELECT * FROM " . DB_PREFIX . "reccat";
			
		$query = $this->db->query($sql);
		
		return $query->num_rows;
	}
}
?>