<?php
class ModelCatalogdocument extends Model {
	public function adddocument($data) {
		if(!isset($data['status'])){$data['status']=0;}
		if(!isset($data['type'])){$data['type']=0;}
		if(!isset($data['notify'])){$data['notify']=0;}
		if(!isset($data['invisible'])){$data['invisible']=0;}
		$this->db->query("INSERT INTO " . DB_PREFIX . "document SET
					sort_order = '" . (int)$data['sort_order'] . "',
					doccat_id = '" . (int)$data['doccat_id'] . "',
					hash = '" . uniqid() . "',
					notify = '" . (int)$data['notify'] . "',
					image = '" . $this->db->escape($data['image']) . "',
					invisible = '" . (int)$data['invisible'] . "',
					robots = '" . $this->db->escape($data['robots']) . "',
					mask = '" . $this->db->escape($data['mask']) . "',
					filename = '" . $this->db->escape($data['filename']) . "',
					type = '" . (int)$data['type'] . "',
					linkdown = '" . $this->db->escape($data['linkdown']) . "',
					status = '" . (int)$data['status'] . "',
					date_added = '". date('Y-m-d',strtotime($data['date_added'])) ."'");

		$document_id = $this->db->getLastId();

		foreach ($data['document_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "document_description SET
					document_id = '" . (int)$document_id . "',
					language_id = '" . (int)$language_id . "',
					title = '" . $this->db->escape($value['title']) .  "',
					short_description = '" . $this->db->escape($value['short_description']) . "',
					description = '" . $this->db->escape($value['description']) . "',
					message = '" . $this->db->escape($value['message']) . "',
					meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}


		if ($data['keyword']) {
          $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE keyword = '" . $this->db->escape($data['keyword']) . "'");
          $exist = $query->num_rows;
          if($exist>0){
              $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'document_id=" . (int) $document_id . "', keyword = '" . $document_id .'-' . $this->db->escape($data['keyword']) . "'");
          }else{
              $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'document_id=" . (int) $document_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
          }
		}


		$this->cache->delete('document');
		$this->cache->delete('seo_url');
	}

	public function editdocument($document_id, $data) {
		if(!isset($data['status'])){$data['status']=0;}
		if(!isset($data['type'])){$data['type']=0;}
		if(!isset($data['notify'])){$data['notify']=0;}
		if(!isset($data['invisible'])){$data['invisible']=0;}
		$this->db->query("UPDATE " . DB_PREFIX . "document SET
					sort_order = '" . (int)$data['sort_order'] . "',
					doccat_id = '" . (int)$data['doccat_id'] . "',
					robots = '" . $this->db->escape($data['robots']) . "',
					image = '" . $this->db->escape($data['image']) . "',
					notify = '" . (int)$data['notify'] . "',
					invisible = '" . (int)$data['invisible'] . "',
					mask = '" . $this->db->escape($data['mask']) . "',
					filename = '" . $this->db->escape($data['filename']) . "',
					`type` = '" . (int)$data['type'] . "',
					linkdown = '" . $this->db->escape($data['linkdown']) . "',
					status = '" . (int)$data['status'] . "',
					date_added = '". date('Y-m-d',strtotime($data['date_added'])) ."'
					WHERE document_id = '" . (int)$document_id . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "document_description WHERE document_id = '" . (int)$document_id . "'");

		foreach ($data['document_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "document_description SET
					document_id = '" . (int)$document_id . "',
					language_id = '" . (int)$language_id . "',
					title = '" . $this->db->escape($value['title']) . "',
					short_description = '" . $this->db->escape($value['short_description']) . "',
					description = '" . $this->db->escape($value['description']) . "',
					message = '" . $this->db->escape($value['message']) . "',
					meta_description = '" . $this->db->escape($value['meta_description']) . "',
					meta_title = '" . $this->db->escape($value['meta_title']) . "',
					meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}


		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'document_id=" . (int)$document_id. "'");


		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'document_id=" . (int)$document_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}


		$this->cache->delete('document');
		$this->cache->delete('seo_url');
	}

	public function deletedocument($document_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "document WHERE document_id = '" . (int)$document_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "document_description WHERE document_id = '" . (int)$document_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'document_id=" . (int)$document_id . "'");

		$this->cache->delete('document');
		$this->cache->delete('seo_url');
	}

	public function getdocument($document_id) {
		$query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'document_id=" . (int)$document_id . "') AS keyword FROM " . DB_PREFIX . "document WHERE document_id = '" . (int)$document_id . "'");

		return $query->row;
	}

	public function getdocuments($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "document i LEFT JOIN " . DB_PREFIX . "document_description id ON (i.document_id = id.document_id) WHERE id.language_id = '" . (int)$this->config->get('config_language_id') . "'";

			$sort_data = array(
				'i.sort_order',
				'id.title',
				'i.date_added',
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY r.sort_order";
			}

			if (isset($data['order']) && ($data['order'] == 'ASC')) {
				$sql .= " ASC";
			} else {
				$sql .= " DESC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			$document_data = $this->cache->get('document.' . (int)$this->config->get('config_language_id'));

			if (!$document_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "document i LEFT JOIN " . DB_PREFIX . "document_description id ON (i.document_id = id.document_id) WHERE id.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY id.title");

				$document_data = $query->rows;

				$this->cache->set('document.' . (int)$this->config->get('config_language_id'), $document_data);
			}

			return $document_data;
		}
	}

	public function getdocumentDescriptions($document_id) {
		$document_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "document_description WHERE document_id = '" . (int)$document_id . "'");

		foreach ($query->rows as $result) {
			$document_description_data[$result['language_id']] = array(
				'title'       => $result['title'],
				'description' => $result['description'],
				'message' => $result['message'],
				'short_description' => $result['short_description'],
				'meta_description' => $result['meta_description'],
				'meta_keyword' => $result['meta_keyword'],
				'meta_title' => $result['meta_title']
			);
		}

		return $document_description_data;
	}


	public function getTotaldocuments() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "document");
		return $query->row['total'];
	}
}
?>
