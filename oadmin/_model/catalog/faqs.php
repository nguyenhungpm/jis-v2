<?php
class ModelCatalogFaqs extends Model {
	
    public function getQuestionCount($data = array()) {
        $sql = "SELECT count(*) AS total
        		FROM `" . DB_PREFIX . "faqs` q
				WHERE 1 = 1"
				. (isset($data['unanswered']) ? " AND q.answer_time = 0" : '');

        if (!$res = $this->db->query($sql))
        	return $this->showErrors();
        
        return $res->row['total'];
    }
    
	public function getQuestions($sort = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "faqs
			  	WHERE 1=1 ORDER BY create_time DESC LIMIT ". $sort['limit'];

		$res = $this->db->query($sql);
		return $res->rows;
	}

	public function deleteQuestion($faq_id) {
				
		$sql = "DELETE FROM `" . DB_PREFIX . "faqs`
				WHERE faq_id = " . (int)$faq_id;

		$this->db->query($sql);
		return true;
	}

    public function editQuestion($question2s) {

    	foreach($question2s as $key=>$value){
    		$question = $question2s[$key];
    	}
    	//var_dump($question);
    	if($question['disp']=='on'){
    		$display_value= '1';
    	}else{
    		$display_value= '0';
    	}
    	if($question['emailalert']=='on'){
    		$emailalert= '1';
    	}else{
    		$emailalert= '0';
    	}
    	$faq_id = (int)$question['faq_id'];
    	
    	$question_value = $question['question'];
    	$answer_value = $question['answer'];
    	$sql = "SELECT name as customer_name, faq_id, email as customer_email, answer_time,phone,email, question, answer FROM `" . DB_PREFIX . "faqs` "
        	.  " WHERE faq_id = '" . $faq_id . "'";
		
        $res = $this->db->query($sql);
        $question_data = $res->row;
		
    	$sql2 = "UPDATE `" . DB_PREFIX . "faqs` "
    		. " SET	answer_time = UNIX_TIMESTAMP(NOW()), question ='". $this->db->escape($question_value). "' , answer ='".$this->db->escape($answer_value) . "' , display ='". $display_value . "'"
        	. " WHERE faq_id = '" . $faq_id . "'";

    	$this->db->query($sql2);
    	
		if ($question_data["answer_time"] == 0 
		&& empty($question_data["answer"]) 
		&& $emailalert == 1
		&& filter_var($question_data["customer_email"], FILTER_VALIDATE_EMAIL)) {
			$store_name = $this->config->get('config_name')[$this->config->get('config_language_id')];
			$store_url = HTTP_CATALOG;
			$this->language->load('catalog/faqs');
			$subject = sprintf($this->language->get('pqs_mail_text_subject'), $this->config->get('config_name')[$this->config->get('config_language_id')]);
			
			$content_email = sprintf($this->language->get('mail_text_greeting'), $question_data['customer_name'])."<br />";
			$content_email .= sprintf($this->language->get('pqs_mail_text_question_answered'),"<a href='".HTTP_CATALOG."'>".$store_name."</a>");
			$content_email .= html_entity_decode($question_data["answer"], ENT_QUOTES, 'UTF-8');
			$content_email .= "<br /><br />Xin hãy xem chi tiết <a href='". $store_url ."faqs/" . $question_data['faq_id'] ."'> tại website của chúng tôi</a><br />";
			$content_email .= $this->language->get('mail_text_sincerely');
			$content_email .= ",<br />Ban quản trị website " . $store_name;
			
			$mail = new Mail(); 
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->hostname = $this->config->get('config_smtp_host');
			$mail->username = $this->config->get('config_smtp_username');
			$mail->password = $this->config->get('config_smtp_password');
			$mail->port = $this->config->get('config_smtp_port');
			$mail->timeout = $this->config->get('config_smtp_timeout');
						
			$mail->setTo($question_data['customer_email']);
			$mail->setFrom($this->config->get('config_mail_from'));
			$mail->setSender($store_name);
			$mail->setSubject($subject);
			$mail->setHtml($content_email);
			$mail->send();	
		}
		
        return true;
    }
        
    public function showErrors() {
        $sql = 'SHOW ERRORS';
        $err = $this->db->query($sql);
        
        foreach ($err as $e) {
			echo "<div class='error'>Error: {$e['Message']}</div>";
		}
		return false;
    }
}
