<?php
class ModelCatalogmanucat extends Model {

	public function addmanucat($data) {

		$this->db->query("INSERT INTO " . DB_PREFIX . "manucat SET
		sort_order='". (int)$data['sort_order'] ."',
		status = '". (int)$data['status'] ."'");
		$manucat_id = $this->db->getLastId();
		if(!empty($data['detail'])){
			foreach($data['detail'] as $language_id => $detail){
				$this->db->query("INSERT INTO " . DB_PREFIX . "manucat_description SET manucat_id = '". $manucat_id ."', language_id = '". $language_id ."', name = '". $this->db->escape($detail['name']) ."'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'manucat_id=" . (int)$manucat_id. "'");
		if ($data['keyword']) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE keyword = '" . $this->db->escape($data['keyword']) . "'");
			$exist = $query->num_rows;
			if($exist>0){
				$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'manucat_id=" . (int) $manucat_id . "', keyword = '" . $manucat_id .'-' . $this->db->escape($data['keyword']) . "'");
			}else{
				$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'manucat_id=" . (int) $manucat_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
			}
		}

		$this->cache->delete('manucat');
		$this->cache->delete('url_alias');
	}

	public function editmanucat($manucat_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "manucat SET
		sort_order='". (int)$data['sort_order'] . "',
		status = '". (int)$data['status'] ."'
		WHERE manucat_id = '" . (int)$manucat_id . "'");
		if(!empty($data['detail'])){
			$this->db->query("DELETE FROM " . DB_PREFIX . "manucat_description WHERE manucat_id = '". $manucat_id ."'");
			foreach($data['detail'] as $language_id => $detail){
				$this->db->query("INSERT INTO " . DB_PREFIX . "manucat_description SET manucat_id = '". $manucat_id ."', language_id = '". $language_id ."', name = '". $this->db->escape($detail['name']) ."'");
			}
		}
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'manucat_id=" . (int)$manucat_id. "'");
		if ($data['keyword']) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE keyword = '" . $this->db->escape($data['keyword']) . "'");
			$exist = $query->num_rows;
			if($exist>0){
				$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'manucat_id=" . (int) $manucat_id . "', keyword = '" . $manucat_id .'-' . $this->db->escape($data['keyword']) . "'");
			}else{
				$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'manucat_id=" . (int) $manucat_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
			}
		}
		return true;
	}

	public function deletemanucat($manucat_id) {
			$this->db->query("DELETE FROM " . DB_PREFIX . "manucat WHERE manucat_id = '" . (int)$manucat_id . "'");
			$this->db->query("DELETE FROM " . DB_PREFIX . "manucat_description WHERE manucat_id = '" . (int)$manucat_id . "'");
	}

	public function getmanucat($manucat_id) {
			$query = $this->db->query("SELECT  * FROM " . DB_PREFIX . "manucat WHERE manucat_id = '" . (int)$manucat_id . "'");
			return $query->row;
	}

	public function getDetail($manucat_id){
			$data = array();
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "manucat_description WHERE manucat_id = '". $manucat_id ."'");
			foreach($query->rows as $rs){
				$data[$rs['language_id']] = array(
					'name' => $rs['name']
				);
			}
			return $data;
	}
	public function getKeyword($manucat_id){
			$query = $this->db->query("SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'manucat_id=" . (int)$manucat_id . "'");
			if($query->num_rows){
				return $query->row['keyword'];
			}else{
				return false;
			}
	}
	public function getNamesCats() {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "manucat_description WHERE language_id = '". (int) $this->config->get('config_language_id') ."'");
			return $query->rows;
	}
	public function getmanucats($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "manucat ORDER BY sort_order DESC";
			if (isset($data['start']) || isset($data['limit'])) {
	        if ($data['start'] < 0) {
	            $data['start'] = 0;
	        }

	        if ($data['limit'] < 1) {
	            $data['limit'] = 20;
	        }

	        $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
	    }
			$query = $this->db->query($sql);
			return $query->rows;
	}

	public function getTotalmanucats($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "manucat";
			$query = $this->db->query($sql);
			return $query->num_rows;
	}
}
?>
