<?php
class ModelCatalogGroup extends Model {
    public function addGroup($data) {
        if(!isset($data['status'])){$data['status']=0;}
        $this->db->query("INSERT INTO " . DB_PREFIX . "group SET parent_id = '" . (int)$data['parent_id'] . "', `top` = '" . (isset($data['top']) ? (int)$data['top'] : 0). "', sort_order = '" . (int)$data['sort_order'] . "', youtube = '" . $data['youtube'] . "', time_test = '" . $data['time_test'] . "', robots = '" . $this->db->escape($data['robots']) . "', status = '" . (int)$data['status'] . "', date_modified = NOW(), date_added = NOW()");

        $group_id = $this->db->getLastId();

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "group SET image = '" . $this->db->escape(html_entity_decode($data['image'], ENT_QUOTES, 'UTF-8')) . "' WHERE group_id = '" . (int)$group_id . "'");
        }

        if (isset($data['banner'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "group SET banner = '" . $this->db->escape(html_entity_decode($data['banner'], ENT_QUOTES, 'UTF-8')) . "' WHERE group_id = '" . (int)$group_id . "'");
        }

        foreach ($data['group_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "group_description SET group_id = '" . (int)$group_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape(trim($value['name']))  . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "', description = '" . $this->db->escape($value['description']) ."',meta_title = '" . $this->db->escape($value['meta_title']) . "'");
        }

        // MySQL Hierarchical Data Closure Table Pattern
        $level = 0;

        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "group_path` WHERE group_id = '" . (int)$data['parent_id'] . "' ORDER BY `level` ASC");

        foreach ($query->rows as $result) {
            $this->db->query("INSERT INTO `" . DB_PREFIX . "group_path` SET `group_id` = '" . (int)$group_id . "', `path_id` = '" . (int)$result['path_id'] . "', `level` = '" . (int)$level . "'");

            $level++;
        }

        $this->db->query("INSERT INTO `" . DB_PREFIX . "group_path` SET `group_id` = '" . (int)$group_id . "', `path_id` = '" . (int)$group_id . "', `level` = '" . (int)$level . "'");


        if ($data['keyword']) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'group_id=" . (int)$group_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }

        $this->cache->delete('group');
        $this->cache->delete('seo_url');
    }

    public function getNameQuizgroup($group_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "group_description n LEFT JOIN " . DB_PREFIX . "group nd ON (n.group_id = nd.group_id) WHERE nd.group_id = '" . (int)$group_id . "'");

        return $query->row;
    }

    public function editGroup($group_id, $data) {
        if(!isset($data['status'])){$data['status']=0;}
        $this->db->query("UPDATE " . DB_PREFIX . "group SET parent_id = '" . (int)$data['parent_id'] . "', `top` = '" . (isset($data['top']) ? (int)$data['top'] : 0) . "', sort_order = '" . (int)$data['sort_order'] . "', youtube = '" . $data['youtube'] . "', time_test = '" . $data['time_test'] . "', robots = '" . $this->db->escape($data['robots']) . "', status = '" . (int)$data['status'] . "', date_modified = NOW() WHERE group_id = '" . (int)$group_id . "'");

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "group SET image = '" . $this->db->escape(html_entity_decode($data['image'], ENT_QUOTES, 'UTF-8')) . "' WHERE group_id = '" . (int)$group_id . "'");
        }

        if (isset($data['banner'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "group SET banner = '" . $this->db->escape(html_entity_decode($data['banner'], ENT_QUOTES, 'UTF-8')) . "' WHERE group_id = '" . (int)$group_id . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "group_description WHERE group_id = '" . (int)$group_id . "'");

        foreach ($data['group_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "group_description SET group_id = '" . (int)$group_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape(trim($value['name'])) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "', description = '" . $this->db->escape($value['description']) ."',meta_title = '" . $this->db->escape($value['meta_title']) . "'");
        }

        // MySQL Hierarchical Data Closure Table Pattern
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "group_path` WHERE path_id = '" . (int)$group_id . "' ORDER BY level ASC");

        if ($query->rows) {
            foreach ($query->rows as $group_path) {
                // Delete the path below the current one
                $this->db->query("DELETE FROM `" . DB_PREFIX . "group_path` WHERE group_id = '" . (int)$group_path['group_id'] . "' AND level < '" . (int)$group_path['level'] . "'");

                $path = array();

                // Get the nodes new parents
                $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "group_path` WHERE group_id = '" . (int)$data['parent_id'] . "' ORDER BY level ASC");

                foreach ($query->rows as $result) {
                    $path[] = $result['path_id'];
                }

                // Get whats left of the nodes current path
                $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "group_path` WHERE group_id = '" . (int)$group_path['group_id'] . "' ORDER BY level ASC");

                foreach ($query->rows as $result) {
                    $path[] = $result['path_id'];
                }

                // Combine the paths with a new level
                $level = 0;

                foreach ($path as $path_id) {
                    $this->db->query("REPLACE INTO `" . DB_PREFIX . "group_path` SET group_id = '" . (int)$group_path['group_id'] . "', `path_id` = '" . (int)$path_id . "', level = '" . (int)$level . "'");

                    $level++;
                }
            }
        } else {
            // Delete the path below the current one
            $this->db->query("DELETE FROM `" . DB_PREFIX . "group_path` WHERE group_id = '" . (int)$group_id . "'");

            // Fix for records with no paths
            $level = 0;

            $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "group_path` WHERE group_id = '" . (int)$data['parent_id'] . "' ORDER BY level ASC");

            foreach ($query->rows as $result) {
                $this->db->query("INSERT INTO `" . DB_PREFIX . "group_path` SET group_id = '" . (int)$group_id . "', `path_id` = '" . (int)$result['path_id'] . "', level = '" . (int)$level . "'");

                $level++;
            }

            $this->db->query("REPLACE INTO `" . DB_PREFIX . "group_path` SET group_id = '" . (int)$group_id . "', `path_id` = '" . (int)$group_id . "', level = '" . (int)$level . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'group_id=" . (int)$group_id. "'");

        if ($data['keyword']) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'group_id=" . (int)$group_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }

        $this->cache->delete('group');
        $this->cache->delete('seo_url');
    }

    public function deleteGroup($group_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "group_path WHERE group_id = '" . (int)$group_id . "'");

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "group_path WHERE path_id = '" . (int)$group_id . "'");

        foreach ($query->rows as $result) {
            $this->deleteGroup($result['group_id']);
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "group WHERE group_id = '" . (int)$group_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "group_description WHERE group_id = '" . (int)$group_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "quiz_to_group WHERE group_id = '" . (int)$group_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'group_id=" . (int)$group_id . "'");

        $this->cache->delete('group');
        $this->cache->delete('seo_url');
    }

    // Function to repair any erroneous groups that are not in the group path table.
    public function repairGroups($parent_id = 0) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "group WHERE parent_id = '" . (int)$parent_id . "'");

        foreach ($query->rows as $group) {
            // Delete the path below the current one
            $this->db->query("DELETE FROM `" . DB_PREFIX . "group_path` WHERE group_id = '" . (int)$group['group_id'] . "'");

            // Fix for records with no paths
            $level = 0;

            $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "group_path` WHERE group_id = '" . (int)$parent_id . "' ORDER BY level ASC");

            foreach ($query->rows as $result) {
                $this->db->query("INSERT INTO `" . DB_PREFIX . "group_path` SET group_id = '" . (int)$group['group_id'] . "', `path_id` = '" . (int)$result['path_id'] . "', level = '" . (int)$level . "'");

                $level++;
            }

            $this->db->query("REPLACE INTO `" . DB_PREFIX . "group_path` SET group_id = '" . (int)$group['group_id'] . "', `path_id` = '" . (int)$group['group_id'] . "', level = '" . (int)$level . "'");

            $this->repairGroups($group['group_id']);
        }
    }
    public function updateStatus($group_id, $column_name, $value){
        $this->db->query("UPDATE " . DB_PREFIX . "group SET " . $column_name . " = '" . (int)$value . "' WHERE group_id = '" . (int)$group_id . "'");
    }
    public function getGroup($group_id) {
        $query = $this->db->query("SELECT DISTINCT *, (SELECT GROUP_CONCAT(cd1.name ORDER BY level SEPARATOR ' &gt; ') FROM " . DB_PREFIX . "group_path cp LEFT JOIN " . DB_PREFIX . "group_description cd1 ON (cp.path_id = cd1.group_id AND cp.group_id != cp.path_id) WHERE cp.group_id = c.group_id AND cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' GROUP BY cp.group_id) AS path, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'group_id=" . (int)$group_id . "') AS keyword FROM " . DB_PREFIX . "group c LEFT JOIN " . DB_PREFIX . "group_description cd2 ON (c.group_id = cd2.group_id) WHERE c.group_id = '" . (int)$group_id . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'");

        return $query->row;
    }

    public function getGroups($data) {
        $sql = "SELECT cp.group_id AS group_id, GROUP_CONCAT(cd1.name ORDER BY cp.level SEPARATOR ' &gt; ') AS name, cd1.name as name_cat, c.status, c.parent_id, (SELECT sort_order FROM " . DB_PREFIX . "group WHERE group_id = cp.group_id) as sort_order FROM " . DB_PREFIX . "group_path cp LEFT JOIN " . DB_PREFIX . "group c ON (cp.path_id = c.group_id) LEFT JOIN " . DB_PREFIX . "group_description cd1 ON (c.group_id = cd1.group_id) LEFT JOIN " . DB_PREFIX . "group_description cd2 ON (cp.group_id = cd2.group_id) WHERE cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'";

        if (!empty($data['filter_name'])) {
            $sql .= " AND cd2.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
        }

        $sql .= " GROUP BY cp.group_id ORDER BY name";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getGroupDescriptions($group_id) {
        $group_description_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "group_description WHERE group_id = '" . (int)$group_id . "'");

        foreach ($query->rows as $result) {
            $group_description_data[$result['language_id']] = array(
                'name'               => $result['name'],
                'meta_keyword'       => $result['meta_keyword'],
                'meta_description'   => $result['meta_description'],
                'description'        => $result['description'],
                'meta_title'         => $result['meta_title']
            );
        }

        return $group_description_data;
    }

    public function getGroupFilters($group_id) {
        return false;
    }

    public function getGroupStores($group_id) {
        return false;
    }

    public function getTotalGroups() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "group");

        return $query->row['total'];
    }

    public function getTotalGroupsByImageId($image_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "group WHERE image_id = '" . (int)$image_id . "'");

        return $query->row['total'];
    }

    public function getGroupByParent($group_id = 0){
        $sql = "SELECT * FROM " . DB_PREFIX . "group c LEFT JOIN " . DB_PREFIX . "group_description cd ON (c.group_id = cd.group_id) WHERE cd.language_id = '". $this->config->get('config_language_id') ."'";
        if($group_id == 0){
            $sql .= " AND c.parent_id = '". $group_id ."'";
        }else{
            $sql .= " AND c.parent_id IN (SELECT group_id FROM " . DB_PREFIX . "group_path WHERE path_id = '". $group_id ."')";
        }
        $query = $this->db->query($sql);
        return $query->rows;
    }

}
?>
