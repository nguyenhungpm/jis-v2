<?php
class ModelCatalogNewsCategory extends Model {
	public function addNewsCategory($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "news_category SET parent_id = '" . (int)$data['parent_id'] . "', `top` = '" . (isset($data['top']) ? (int)$data['top'] : 0) . "', `common` = '" . (isset($data['common']) ? (int)$data['common'] : 0) . "', `need_login` = '" . (isset($data['need_login']) ? (int)$data['need_login'] : 0) . "', sort_order = '" . (int)$data['sort_order'] . "', robots = '" . $this->db->escape($data['robots']) . "', status = '" . (int)$data['status'] . "', date_modified = NOW(), date_added = NOW()");

		$news_category_id = $this->db->getLastId();

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "news_category SET image = '" . $this->db->escape($data['image']) . "' WHERE news_category_id = '" . (int)$news_category_id . "'");
		}

		foreach ($data['news_category_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "news_category_description SET news_category_id = '" . (int)$news_category_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "', description = '" . $this->db->escape($value['description']) ."', short_description = '" . $this->db->escape($value['short_description']) ."', meta_title = '" . $this->db->escape($value['meta_title']) . "'");
		}

		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'news_category_id=" . (int)$news_category_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}

		$this->cache->delete('news_category');
	}

	public function getNameNewcategory($news_category_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "news_category_description n LEFT JOIN " . DB_PREFIX . "news_category nd ON (n.news_category_id = nd.news_category_id) WHERE nd.news_category_id = '" . (int)$news_category_id . "'");

		return $query->row;
	}

	public function getCategoryName($news_id){
		$data = array();
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_to_category WHERE news_id = '". $news_id ."'");
		foreach($query->rows as $rs){
			$data[] = $this->getNewsCategoryDescriptions($rs['news_category_id'])[$this->config->get('config_language_id')]['name'];
		}
		return join(', ', $data);
		// return $data;
	}

	public function editNewsCategory($news_category_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "news_category SET parent_id = '" . (int)$data['parent_id'] . "', `top` = '" . (isset($data['top']) ? (int)$data['top'] : 0) . "', `common` = '" . (isset($data['common']) ? (int)$data['common'] : 0) . "', `need_login` = '" . (isset($data['need_login']) ? (int)$data['need_login'] : 0) . "', sort_order = '" . (int)$data['sort_order'] . "', robots = '" . $this->db->escape($data['robots']) . "', status = '" . (int)$data['status'] . "', date_modified = NOW() WHERE news_category_id = '" . (int)$news_category_id . "'");

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "news_category SET image = '" . $this->db->escape($data['image']) . "' WHERE news_category_id = '" . (int)$news_category_id . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "news_category_description WHERE news_category_id = '" . (int)$news_category_id . "'");

		foreach ($data['news_category_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "news_category_description SET news_category_id = '" . (int)$news_category_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "', description = '" . $this->db->escape($value['description']) ."', short_description = '" . $this->db->escape($value['short_description']) ."', meta_title = '" . $this->db->escape($value['meta_title']) .  "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'news_category_id=" . (int)$news_category_id. "'");

		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'news_category_id=" . (int)$news_category_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}

		$this->cache->delete('news_category');
	}

	public function deleteNewsCategory($news_category_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "news_to_category WHERE news_category_id = '" . (int)$news_category_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "news_category WHERE news_category_id = '" . (int)$news_category_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "news_category_description WHERE news_category_id = '" . (int)$news_category_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'news_category_id=" . (int)$news_category_id . "'");

		$query = $this->db->query("SELECT news_category_id FROM " . DB_PREFIX . "news_category WHERE parent_id = '" . (int)$news_category_id . "'");

		foreach ($query->rows as $result) {
			$this->deleteNewsCategory($result['news_category_id']);
		}

		$this->cache->delete('news_category');
	}

	public function getNewsCategory($news_category_id) {
		$query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'news_category_id=" . (int)$news_category_id . "') AS keyword FROM " . DB_PREFIX . "news_category WHERE news_category_id = '" . (int)$news_category_id . "'");

		return $query->row;
	}

	public function getNewsCategories($parent_id) {
		$news_category_data = $this->cache->get('news_category.' . $this->config->get('config_language_id') . '.' . $parent_id);

		// if (!$news_category_data) {
			$news_category_data = array();

			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_category c LEFT JOIN " . DB_PREFIX . "news_category_description cd ON (c.news_category_id = cd.news_category_id) WHERE c.parent_id = '" . (int)$parent_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY c.sort_order, cd.name ASC");

			foreach ($query->rows as $result) {
				$news_category_data[] = array(
					'news_category_id' => $result['news_category_id'],
					'name'        => $this->getPath($result['news_category_id'], $this->config->get('config_language_id')),
					'status'  	  => $result['status'],
					'sort_order'  => $result['sort_order']
				);

				$news_category_data = array_merge($news_category_data, $this->getNewsCategories($result['news_category_id']));
			}

			$this->cache->set('news_category.' . $this->config->get('config_language_id') . '.' . $parent_id, $news_category_data);
		// }

		return $news_category_data;
	}

	public function getPath($news_category_id) {
		$query = $this->db->query("SELECT name, parent_id FROM " . DB_PREFIX . "news_category c LEFT JOIN " . DB_PREFIX . "news_category_description cd ON (c.news_category_id = cd.news_category_id) WHERE c.news_category_id = '" . (int)$news_category_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY c.sort_order, cd.name ASC");

		$news_category_info = $query->row;

		if ($news_category_info['parent_id']) {
			return $this->getPath($news_category_info['parent_id'], $this->config->get('config_language_id')) . $this->language->get('text_separator') . $news_category_info['name'];
		} else {
			return $news_category_info['name'];
		}
	}

	public function getNewsCategoryDescriptions($news_category_id) {
		$news_category_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_category_description WHERE news_category_id = '" . (int)$news_category_id . "'");

		foreach ($query->rows as $result) {
			$news_category_description_data[$result['language_id']] = array(
				'name'             => $result['name'],
				'meta_keyword'     => $result['meta_keyword'],
				'meta_description' => $result['meta_description'],
				'description'      => $result['description'],
				'short_description'      => $result['short_description'],
				'meta_title'      => $result['meta_title']
			);
		}

		return $news_category_description_data;
	}

	public function getNewsCategoryStores($news_category_id) {
		return false;
	}

	public function getTotalNewsCategories() {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "news_category");

		return $query->row['total'];
	}

	public function getTotalNewsCategoriesByImageId($image_id) {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "news_category WHERE image_id = '" . (int)$image_id . "'");

		return $query->row['total'];
	}

}
?>
