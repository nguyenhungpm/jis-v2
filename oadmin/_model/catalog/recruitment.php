<?php
class ModelCatalogRecruitment extends Model {
	public function addRecruitment($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "recruitment SET sort_order = '" . (int)$data['sort_order'] . "', reccat_id = '" . (int)$data['reccat_id'] . "', jobtype = '" . (int)$data['jobtype'] . "', robots = '" . $this->db->escape($data['robots']) . "', hot = '" . (isset($data['hot']) ? (int)$data['hot'] : 0) . "', internal = '" . (isset($data['internal']) ? (int)$data['internal'] : 0) . "', status = '" . (int)$data['status'] . "', date_end = '". date('Y-m-d',strtotime($data['date_end'])) . "', date_added = '". date('Y-m-d',strtotime($data['date_added'])) ."'");

		$recruitment_id = $this->db->getLastId();

		foreach ($data['recruitment_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "recruitment_description SET recruitment_id = '" . (int)$recruitment_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) .  "', location = '" . $this->db->escape($value['location']) .  "', position = '" . $this->db->escape($value['position']) . "', requirement = '" . $this->db->escape($value['requirement']) . "', benefit = '" . $this->db->escape($value['benefit']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}

		if ($data['keyword']) {
                $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE keyword = '" . $this->db->escape($data['keyword']) . "'");
                $exist = $query->num_rows;
                if($exist>0){
                    $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'recruitment_id=" . (int) $recruitment_id . "', keyword = '" . $recruitment_id .'-' . $this->db->escape($data['keyword']) . "'");
                }else{
                    $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'recruitment_id=" . (int) $recruitment_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
                }
        }

		$this->cache->delete('recruitment');
		$this->cache->delete('seo_url');
	}

	public function editRecruitment($recruitment_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "recruitment SET sort_order = '" . (int)$data['sort_order'] . "', reccat_id = '" . (int)$data['reccat_id'] . "', jobtype = '" . (int)$data['jobtype'] . "', robots = '" . $this->db->escape($data['robots']) . "', hot = '" . (isset($data['hot']) ? (int)$data['hot'] : 0) . "', internal = '" . (isset($data['internal']) ? (int)$data['internal'] : 0) . "', status = '" . (int)$data['status'] . "', date_end = '". date('Y-m-d',strtotime($data['date_end'])) . "', date_added = '". date('Y-m-d',strtotime($data['date_added'])) ."' WHERE recruitment_id = '" . (int)$recruitment_id . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "recruitment_description WHERE recruitment_id = '" . (int)$recruitment_id . "'");

		foreach ($data['recruitment_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "recruitment_description SET recruitment_id = '" . (int)$recruitment_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) .  "', location = '" . $this->db->escape($value['location']) .  "', position = '" . $this->db->escape($value['position']) . "', requirement = '" . $this->db->escape($value['requirement']) . "', benefit = '" . $this->db->escape($value['benefit']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'recruitment_id=" . (int)$recruitment_id. "'");

		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'recruitment_id=" . (int)$recruitment_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}

		$this->cache->delete('recruitment');
		$this->cache->delete('seo_url');
	}

	public function deleteRecruitment($recruitment_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "recruitment WHERE recruitment_id = '" . (int)$recruitment_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "recruitment_description WHERE recruitment_id = '" . (int)$recruitment_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'recruitment_id=" . (int)$recruitment_id . "'");

		$this->cache->delete('recruitment');
		$this->cache->delete('seo_url');
	}

	public function getRecruitment($recruitment_id) {
		$query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'recruitment_id=" . (int)$recruitment_id . "') AS keyword FROM " . DB_PREFIX . "recruitment WHERE recruitment_id = '" . (int)$recruitment_id . "'");

		return $query->row;
	}

	public function getRecruitments($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "recruitment i LEFT JOIN " . DB_PREFIX . "recruitment_description id ON (i.recruitment_id = id.recruitment_id) WHERE id.language_id = '" . (int)$this->config->get('config_language_id') . "'";

			$sort_data = array(
				'i.sort_order',
				'id.title',
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY r.sort_order";
			}

			if (isset($data['order']) && ($data['order'] == 'ASC')) {
				$sql .= " ASC";
			} else {
				$sql .= " DESC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			$recruitment_data = $this->cache->get('recruitment.' . (int)$this->config->get('config_language_id'));

			if (!$recruitment_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "recruitment i LEFT JOIN " . DB_PREFIX . "recruitment_description id ON (i.recruitment_id = id.recruitment_id) WHERE id.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY id.title");

				$recruitment_data = $query->rows;

				$this->cache->set('recruitment.' . (int)$this->config->get('config_language_id'), $recruitment_data);
			}

			return $recruitment_data;
		}
	}

	public function getRecruitmentDescriptions($recruitment_id) {
		$recruitment_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "recruitment_description WHERE recruitment_id = '" . (int)$recruitment_id . "'");

		foreach ($query->rows as $result) {
			$recruitment_description_data[$result['language_id']] = array(
				'title'       => $result['title'],
				'location'       => $result['location'],
				'requirement' => $result['requirement'],
				'benefit' => $result['benefit'],
				'position' => $result['position'],
				'meta_description' => $result['meta_description'],
				'meta_keyword' => $result['meta_keyword'],
				'meta_title' => $result['meta_title']
			);
		}

		return $recruitment_description_data;
	}


	public function getTotalRecruitments() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "recruitment");

		return $query->row['total'];
	}
}
?>
