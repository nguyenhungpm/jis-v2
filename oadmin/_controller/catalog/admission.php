<?php
class ControllerCatalogAdmission extends Controller {
	private $error = array();

	public function index() {
		$this->language->load('catalog/admission');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/admission');

		$this->getList();
	}

	public function insert() {
		$this->language->load('catalog/admission');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/admission');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->response->ClearCache();
			$this->model_catalog_admission->addAdmission($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/admission', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function update() {
		$this->language->load('catalog/admission');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/admission');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->response->ClearCache();
			$this->model_catalog_admission->editAdmission($this->request->get['admission_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/admission', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function delete() {
		$this->language->load('catalog/admission');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/admission');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $admission_id) {
				$this->model_catalog_admission->deleteAdmission($admission_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/admission', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'id.title';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/admission', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->data['insert'] = $this->url->link('catalog/admission/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('catalog/admission/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$this->data['admissions'] = array();

		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		$admission_total = $this->model_catalog_admission->getTotalAdmissions();

		$results = $this->model_catalog_admission->getAdmissions($data);

		foreach ($results as $result) {
			$action = array();

			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('catalog/admission/update', 'token=' . $this->session->data['token'] . '&admission_id=' . $result['admission_id'] . $url, 'SSL')
			);

			$this->data['admissions'][] = array(
				'admission_id' => $result['admission_id'],
				'title'          => $result['title'],
				'sort_order'     => $result['sort_order'],
				'selected'       => isset($this->request->post['selected']) && in_array($result['admission_id'], $this->request->post['selected']),
				'action'         => $action
			);
		}

		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_list'] = $this->language->get('text_list');
		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['column_title'] = $this->language->get('column_title');
		$this->data['column_sort_order'] = $this->language->get('column_sort_order');
		$this->data['column_action'] = $this->language->get('column_action');

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['sort_title'] = $this->url->link('catalog/admission', 'token=' . $this->session->data['token'] . '&sort=id.title' . $url, 'SSL');
		$this->data['sort_sort_order'] = $this->url->link('catalog/admission', 'token=' . $this->session->data['token'] . '&sort=i.sort_order' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $admission_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/admission', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->template = 'catalog/admission_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_default'] = $this->language->get('text_default');
		$this->data['text_page_details'] = $this->language->get('text_page_details');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');

		$this->data['entry_title'] = $this->language->get('entry_title');
		$this->data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$this->data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$this->data['entry_description'] = $this->language->get('entry_description');
		$this->data['entry_keyword'] = $this->language->get('entry_keyword');
		$this->data['text_remain'] = $this->language->get('text_remain');
		$this->data['text_characters'] = $this->language->get('text_characters');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_status'] = $this->language->get('entry_status');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		$this->data['button_close'] = $this->language->get('button_close');
		$this->data['tab_data'] = $this->language->get('tab_data');
		$this->data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['title'])) {
			$this->data['error_title'] = $this->error['title'];
		} else {
			$this->data['error_title'] = array();
		}

		if (isset($this->error['description'])) {
			$this->data['error_description'] = $this->error['description'];
		} else {
			$this->data['error_description'] = array();
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/admission', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		if (!isset($this->request->get['admission_id'])) {
			$this->data['action'] = $this->url->link('catalog/admission/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['admission_id'] = $this->request->get['admission_id'];
            $this->data['delete'] = $this->url->link('catalog/admission/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
			$this->data['action'] = $this->url->link('catalog/admission/update', 'token=' . $this->session->data['token'] . '&admission_id=' . $this->request->get['admission_id'] . $url, 'SSL');
		}

		$this->data['cancel'] = $this->url->link('catalog/admission', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->get['admission_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$admission_info = $this->model_catalog_admission->getAdmission($this->request->get['admission_id']);
		}

		$this->data['token'] = $this->session->data['token'];
    
    $this->load->model('tool/image');
    $this->load->model('catalog/category');
    $this->data['categories'] = $this->model_catalog_category->getCategories($data);

		$this->load->model('localisation/language');

		$this->data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->post['admission_description'])) {
			$this->data['admission_description'] = $this->request->post['admission_description'];
		} elseif (isset($this->request->get['admission_id'])) {
			$this->data['admission_description'] = $this->model_catalog_admission->getAdmissionDescriptions($this->request->get['admission_id']);
		} else {
			$this->data['admission_description'] = array();
		}

		if (isset($this->request->post['keyword'])) {
			$this->data['keyword'] = $this->request->post['keyword'];
		} elseif (!empty($admission_info)) {
			$this->data['keyword'] = $admission_info['keyword'];
		} else {
			$this->data['keyword'] = '';
		}

		if (isset($this->request->post['code_youtube'])) {
			$this->data['code_youtube'] = $this->request->post['code_youtube'];
		} elseif (!empty($admission_info)) {
			$this->data['code_youtube'] = $admission_info['code_youtube'];
		} else {
			$this->data['code_youtube'] = '';
		}	

		if (isset($this->request->post['robots'])) {
			$this->data['robots'] = $this->request->post['robots'];
		} elseif (!empty($admission_info)) {
			$this->data['robots'] = $admission_info['robots'];
		} else {
			$this->data['robots'] = '';
		}	

		if (isset($this->request->post['bottom'])) {
			$this->data['bottom'] = $this->request->post['bottom'];
		} elseif (!empty($admission_info)) {
			$this->data['bottom'] = $admission_info['bottom'];
		} else {
			$this->data['bottom'] = 0;
		}

		if (isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
		} elseif (!empty($admission_info)) {
			$this->data['status'] = $admission_info['status'];
		} else {
			$this->data['status'] = 1;
		}

		if (isset($this->request->post['home'])) {
			$this->data['home'] = $this->request->post['home'];
		} elseif (!empty($admission_info)) {
			$this->data['home'] = $admission_info['home'];
		} else {
			$this->data['home'] = 1;
		}

		if (isset($this->request->post['category_id'])) {
			$this->data['category_id'] = $this->request->post['category_id'];
		} elseif (!empty($admission_info)) {
			$this->data['category_id'] = $admission_info['category_id'];
		} else {
			$this->data['category_id'] = 0;
		}

		if (isset($this->request->post['sort_order'])) {
			$this->data['sort_order'] = $this->request->post['sort_order'];
		} elseif (!empty($admission_info)) {
			$this->data['sort_order'] = $admission_info['sort_order'];
		} else {
			$this->data['sort_order'] = '';
		}

		// Images
		if (isset($this->request->post['admission_highlight'])) {
			$admission_highlights = $this->request->post['admission_highlight'];
			} elseif (isset($this->request->get['admission_id'])) {
			$admission_highlights = $this->model_catalog_admission->getAdmissionHighlights($this->request->get['admission_id']);
			} else {
			$admission_highlights = array();
		}

		$this->data['admission_highlights'] = array();

		foreach ($admission_highlights as $admission_highlight) {
			if ($admission_highlight['image'] && file_exists(DIR_IMAGE . $admission_highlight['image'])) {
				$image = $admission_highlight['image'];
				} else {
				$image = 'no_image.jpg';
			}

			$this->data['admission_highlights'][] = array(
			'image' => $image,
			'thumb' => $this->model_tool_image->resize($image, 100, 100),
			'sort_order' => $admission_highlight['sort_order'],
			'description' => $admission_highlight['description'],
			);
		}

		$this->template = 'catalog/admission_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/admission')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		// foreach ($this->request->post['admission_description'] as $language_id => $value) {
		// 	if ((utf8_strlen($value['title']) < 3) || (utf8_strlen($value['title']) > 64)) {
		// 		$this->error['title'][$language_id] = $this->language->get('error_title');
		// 	}

		// 	if (utf8_strlen($value['description']) < 3) {
		// 		$this->error['description'][$language_id] = $this->language->get('error_description');
		// 	}
		// }

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/admission')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		foreach ($this->request->post['selected'] as $admission_id) {
			if ($this->config->get('config_account_id') == $admission_id) {
				$this->error['warning'] = $this->language->get('error_account');
			}

			if ($this->config->get('config_checkout_id') == $admission_id) {
				$this->error['warning'] = $this->language->get('error_checkout');
			}

			if ($this->config->get('config_affiliate_id') == $admission_id) {
				$this->error['warning'] = $this->language->get('error_affiliate');
			}

		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
}
?>
