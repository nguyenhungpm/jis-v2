<?php
class ControllerCatalogGroup extends Controller {
	private $error = array();

	public function index() {
		$this->language->load('catalog/group');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/group');

		$this->getList();
	}
	public function updatestatus() {
		$this->load->language('catalog/group');
		$this->load->model('catalog/group');
		$output='';
		if(isset($this->request->get['object_id'])){
			$get_request = explode('-',$this->request->get['object_id']);
			if(count($get_request)==2){
				$column_name = $get_request[0];
				$group_id = $get_request[1];
				$result = $this->model_catalog_group->getGroup($group_id);
				if($result[$column_name]){
					$this->model_catalog_group->updateStatus($group_id, $column_name, 0);
				} else {
					$this->model_catalog_group->updateStatus($group_id, $column_name, 1);
				}
				$result = $this->model_catalog_group->getGroup($group_id);
				$output = $result[$column_name] ? $this->language->get('text_enabled') : $this->language->get('text_disabled');
			}
		}
		$this->response->setOutput($output);
	}
	public function insert() {
		$this->language->load('catalog/group');

		$this->document->setTitle($this->language->get('heading_title_add'));

		$this->load->model('catalog/group');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->response->ClearCache();
			$this->model_catalog_group->addGroup($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/group', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function update() {
		$this->language->load('catalog/group');

		$this->load->model('catalog/group');
		if (isset($this->request->get['group_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$news_info = $this->model_catalog_group->getNameQuizgroup($this->request->get['group_id']);
			$this->document->setTitle($news_info['name']);
		}
		else {
			$this->document->setTitle($this->language->get('heading_title'));
		}

		$this->load->model('catalog/group');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->response->ClearCache();
			$this->model_catalog_group->editGroup($this->request->get['group_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/group', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function delete() {
		$this->language->load('catalog/group');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/group');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $group_id) {
				$this->model_catalog_group->deleteGroup($group_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/group', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	public function repair() {
		$this->language->load('catalog/group');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/group');

		if ($this->validateRepair()) {
			$this->model_catalog_group->repairGroups();

			$this->session->data['success'] = $this->language->get('text_success');

			$this->redirect($this->url->link('catalog/group', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/group', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->data['insert'] = $this->url->link('catalog/group/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('catalog/group/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['repair'] = $this->url->link('catalog/group/repair', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$this->data['groups'] = array();

		$data = array(
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		$group_total = $this->model_catalog_group->getTotalGroups();

		$results = $this->model_catalog_group->getGroups($data);

		foreach ($results as $result) {
			$action = array();

			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('catalog/group/update', 'token=' . $this->session->data['token'] . '&group_id=' . $result['group_id'] . $url, 'SSL')
			);

			$this->data['groups'][] = array(
				'group_id' => $result['group_id'],
				'name'        => $result['name'],
				'status'     => ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
				'sort_order'  => $result['sort_order'],
				'selected'    => isset($this->request->post['selected']) && in_array($result['group_id'], $this->request->post['selected']),
				'action'      => $action
			);
		}

		// print_r($this->data['groups']);

		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_list'] = $this->language->get('text_list');
		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_status'] = $this->language->get('column_status');
		$this->data['token'] = $this->session->data['token'];
		$this->data['column_sort_order'] = $this->language->get('column_sort_order');
		$this->data['column_action'] = $this->language->get('column_action');

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_repair'] = $this->language->get('button_repair');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$pagination = new Pagination();
		$pagination->total = $group_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/group', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->template = 'catalog/group_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->document->addStyle('view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css');
		$this->data['text_none'] = $this->language->get('text_none');
		$this->data['text_default'] = $this->language->get('text_default');
		$this->data['text_image_manager'] = $this->language->get('text_image_manager');
		$this->data['text_browse'] = $this->language->get('text_browse');
		$this->data['text_clear'] = $this->language->get('text_clear');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_percent'] = $this->language->get('text_percent');
		$this->data['text_characters'] = $this->language->get('text_characters');

		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$this->data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$this->data['entry_description'] = $this->language->get('entry_description');
		$this->data['entry_parent'] = $this->language->get('entry_parent');
		$this->data['entry_filter'] = $this->language->get('entry_filter');
		$this->data['text_page_details'] = $this->language->get('text_page_details');
		$this->data['entry_keyword'] = $this->language->get('entry_keyword');
		$this->data['entry_image'] = $this->language->get('entry_image');
		$this->data['text_remain'] = $this->language->get('text_remain');
		$this->data['entry_column'] = $this->language->get('entry_column');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_status'] = $this->language->get('entry_status');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_close'] = $this->language->get('button_close');
		$this->data['button_delete'] = $this->language->get('button_delete');

		$this->data['tab_general'] = $this->language->get('tab_general');
		$this->data['tab_data'] = $this->language->get('tab_data');
		$this->data['tab_design'] = $this->language->get('tab_design');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = array();
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/group', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		if (!isset($this->request->get['group_id'])) {
			$this->data['action'] = $this->url->link('catalog/group/insert', 'token=' . $this->session->data['token'], 'SSL');
		} else {
			$this->data['group_id'] = $this->request->get['group_id'];
      $this->data['delete'] = $this->url->link('catalog/group/delete', 'token=' . $this->session->data['token'] , 'SSL');
			$this->data['action'] = $this->url->link('catalog/group/update', 'token=' . $this->session->data['token'] . '&group_id=' . $this->request->get['group_id'], 'SSL');
		}

		$this->data['cancel'] = $this->url->link('catalog/group', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->get['group_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$group_info = $this->model_catalog_group->getGroup($this->request->get['group_id']);
		}

		$this->data['token'] = $this->session->data['token'];

		$this->load->model('localisation/language');

		$this->data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->post['group_description'])) {
			$this->data['group_description'] = $this->request->post['group_description'];
		} elseif (isset($this->request->get['group_id'])) {
			$this->data['group_description'] = $this->model_catalog_group->getGroupDescriptions($this->request->get['group_id']);
		} else {
			$this->data['group_description'] = array();
		}

		if (isset($this->request->post['path'])) {
			$this->data['path'] = $this->request->post['path'];
		} elseif (!empty($group_info)) {
			$this->data['path'] = $group_info['path'];
		} else {
			$this->data['path'] = '';
		}

		$this->data['groups'] = array();

		$this->load->model('catalog/group');
		$results = $this->model_catalog_group->getGroups(0);
		foreach ($results as $result){
			if($result['group_id']==$this->request->get['group_id']){
				continue;
			}
			$this->data['groups'][] = array(
				'group_id' => $result['group_id'],
				'name'        => $result['name'],
			);
		}
		// print_r($this->data['groups']);

		if (isset($this->request->post['parent_id'])) {
			$this->data['parent_id'] = $this->request->post['parent_id'];
		} elseif (!empty($group_info)) {
			$this->data['parent_id'] = $group_info['parent_id'];
		} else {
			$this->data['parent_id'] = 0;
		}

		if (isset($this->request->post['keyword'])) {
			$this->data['keyword'] = $this->request->post['keyword'];
		} elseif (!empty($group_info)) {
			$this->data['keyword'] = $group_info['keyword'];
		} else {
			$this->data['keyword'] = '';
		}

		if (isset($this->request->post['robots'])) {
			$this->data['robots'] = $this->request->post['robots'];
		} elseif (!empty($group_info)) {
			$this->data['robots'] = $group_info['robots'];
		} else {
			$this->data['robots'] = '';
		}

		if (isset($this->request->post['image'])) {
			$this->data['image'] = $this->request->post['image'];
		} elseif (!empty($group_info)) {
			$this->data['image'] = $group_info['image'];
		} else {
			$this->data['image'] = '';
		}
		$this->load->model('tool/image');

		if (isset($this->request->post['image']) && file_exists(DIR_IMAGE . $this->request->post['image'])) {
			$this->data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
		} elseif (!empty($group_info) && $group_info['image'] && file_exists(DIR_IMAGE . $group_info['image'])) {
			$this->data['thumb'] = $this->model_tool_image->resize($group_info['image'], 100, 100);
		} else {
			$this->data['thumb'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
		}

		$this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);


		if (isset($this->request->post['sort_order'])) {
			$this->data['sort_order'] = $this->request->post['sort_order'];
		} elseif (!empty($group_info)) {
			$this->data['sort_order'] = $group_info['sort_order'];
		} else {
			$this->data['sort_order'] = 0;
		}

		if (isset($this->request->post['time_test'])) {
			$this->data['time_test'] = $this->request->post['time_test'];
		} elseif (!empty($group_info)) {
			$this->data['time_test'] = $group_info['time_test'];
		} else {
			$this->data['time_test'] = 30;
		}

		if (isset($this->request->post['youtube'])) {
			$this->data['youtube'] = $this->request->post['youtube'];
		} elseif (!empty($group_info)) {
			$this->data['youtube'] = $group_info['youtube'];
		} else {
			$this->data['youtube'] = '';
		}

		if (isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
		} elseif (!empty($group_info)) {
			$this->data['status'] = $group_info['status'];
		} else {
			$this->data['status'] = 1;
		}

		$this->template = 'catalog/group_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/group')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		foreach ($this->request->post['group_description'] as $language_id => $value) {
			if ((utf8_strlen($value['name']) < 2) || (utf8_strlen($value['name']) > 255)) {
				$this->error['name'][$language_id] = $this->language->get('error_name');
			}
		}

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/group')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateRepair() {
		if (!$this->user->hasPermission('modify', 'catalog/group')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/group');

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_group->getGroups($data);

			foreach ($results as $result) {
				$json[] = array(
					'group_id' => $result['group_id'],
					'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}
}
?>
