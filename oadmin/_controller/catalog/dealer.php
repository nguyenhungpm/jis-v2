<?php
class ControllerCatalogDealer extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/dealer');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/dealer');

		$this->getList();
	}

	public function sort() {

        $this->load->language('catalog/dealer');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('catalog/dealer');

        if ($this->request->server['REQUEST_METHOD'] == 'POST') {

            $sort_order = $this->request->post['sort_order'];

            if (count($sort_order > 0)) {
                $this->model_catalog_dealer->changeSort($sort_order);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['filter_order'])) {
                $url .= '&filter_order=' . (int) $this->request->get['filter_order'];
            }

            if (isset($this->request->get['filter_name'])) {
                $url .= '&filter_name=' . $this->request->get['filter_name'];
            }

            if (isset($this->request->get['filter_status'])) {
                $url .= '&filter_status=' . $this->request->get['filter_status'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->redirect($this->url->link('catalog/dealer', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getList();
    }

	public function insert() {
		$this->load->language('catalog/dealer');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/dealer');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_dealer->addDealer($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->redirect($this->url->link('catalog/dealer', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->getForm();
	}

	public function update() {
		$this->load->language('catalog/dealer');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/dealer');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_dealer->editDealer($this->request->get['dealer_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->redirect($this->url->link('catalog/dealer', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/dealer');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/dealer');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $dealer_id) {
				$this->model_catalog_dealer->deleteDealer($dealer_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$this->redirect($this->url->link('catalog/dealer', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->getList();
	}

	private function getList() {
			$this->data['token'] = $this->session->data['token'];
			if (isset($this->request->get['filter_name'])) {
				$filter_name = $this->request->get['filter_name'];
				} else {
				$filter_name = null;
			}
   		$this->data['breadcrumbs'] = array();
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);
		$this->data['breadcrumbs'][] = array(
       		'text'      => 'Khu vực bán hàng',
			'href'      => $this->url->link('catalog/area', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);

   		if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

		if(isset($this->request->get['province_id'])){
			$query = $this->db->query("SELECT a.district_id as district_id, ad.name as name, a.province_id as province_id FROM " . DB_PREFIX . "district a LEFT JOIN " . DB_PREFIX . "district_description ad ON (a.district_id=ad.district_id) WHERE a.district_id ='". $this->request->get['district_id'] ."'");

			$query_province = $this->db->query("SELECT a.provinceid as province_id, ad.name as name FROM " . DB_PREFIX . "province a LEFT JOIN " . DB_PREFIX . "province_description ad ON (a.provinceid=ad.province_id) WHERE a.provinceid ='". $query->row['province_id'] ."'");

			$name_province = $query->row['name'];
			$url_province = $this->url->link('catalog/dealer', 'token=' . $this->session->data['token'].'&district_id='.$query->row['district_id'], 'SSL');

			$this->data['breadcrumbs'][] = array(
       		'text'      => $query_province->row['name'],
			'href'      => $this->url->link('catalog/district', 'token=' . $this->session->data['token'].'&province_id='.$query_province->row['province_id'], 'SSL'),
      		'separator' => ' :: '
			);

		}else{
			$name_province = 'Tất cả các đại lý';
			$url_province = $this->url->link('catalog/dealer', 'token=' . $this->session->data['token'], 'SSL');
		}

		$this->data['breadcrumbs'][] = array(
       		'text'      => $name_province,
			'href'      => $url_province,
      		'separator' => ' :: '
		);

   		// $this->data['breadcrumbs'][] = array(
       		// 'text'      => $this->language->get('heading_title'),
			// 'href'      => $this->url->link('catalog/dealer', 'token=' . $this->session->data['token'], 'SSL'),
      		// 'separator' => ' :: '
   		// );
		if(isset($this->request->get['district_id'])){
			$this->data['insert'] = $this->url->link('catalog/dealer/insert', 'token=' . $this->session->data['token'].'&district_id='.$this->request->get['district_id'], 'SSL');
		}else{
			$this->data['insert'] = $this->url->link('catalog/dealer/insert', 'token=' . $this->session->data['token'], 'SSL');
		}

		$this->data['delete'] = $this->url->link('catalog/dealer/delete', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['save_sort'] = $this->url->link('catalog/dealer/sort', 'token=' . $this->session->data['token'],'SSL');

		$this->data['dealers'] = array();
		if(isset($this->request->get['district_id'])){
			$district_id = $this->request->get['district_id'];
		}else{
			$district_id = null;
		}

		$data = array(
						'filter_name' => $filter_name,
            'district_id' => $district_id,
            'start' => ($page - 1) * $this->config->get('config_admin_limit'),
            'limit' => $this->config->get('config_admin_limit')
        );

		$dealers_total = $this->model_catalog_dealer->getTotalDealers($data);
		$results = $this->model_catalog_dealer->getDealers($data);
		$url = '';
		if(isset($this->request->get['district_id'])){
			$url .= '&district_id='.$this->request->get['district_id'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}
		foreach ($results as $result) {
			$action = array();

			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('catalog/dealer/update', 'token=' . $this->session->data['token'] . '&dealer_id=' . $result['dealer_id'].$url, 'SSL')
			);

			$this->data['dealers'][] = array(
				'dealer_id' => $result['dealer_id'],
				'name'        => $result['name'],
				'sort_order'  => $result['sort_order'],
				'telephone'  => $result['telephone'],
				'selected'    => isset($this->request->post['selected']) && in_array($result['dealer_id'], $this->request->post['selected']),
				'action'      => $action
			);
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_sort_order'] = $this->language->get('column_sort_order');
		$this->data['column_action'] = $this->language->get('column_action');

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }
		$this->data['filter_name'] = $filter_name;
    $pagination = new Pagination();
    $pagination->total = $dealers_total;
    $pagination->page = $page;
    $pagination->limit = $this->config->get('config_admin_limit');
    $pagination->text = $this->language->get('text_pagination');
    $pagination->url = $this->url->link('catalog/dealer', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

    $this->data['pagination'] = $pagination->render();


		$this->template = 'catalog/dealer_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer',
		);

		$this->response->setOutput($this->render());
	}

	private function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_none'] = $this->language->get('text_none');
		$this->data['text_default'] = $this->language->get('text_default');
		$this->data['text_image_manager'] = $this->language->get('text_image_manager');
		$this->data['text_browse'] = $this->language->get('text_browse');
		$this->data['text_clear'] = $this->language->get('text_clear');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
    	$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_percent'] = $this->language->get('text_percent');
		$this->data['text_amount'] = $this->language->get('text_amount');

		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$this->data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$this->data['entry_description'] = $this->language->get('entry_description');
		$this->data['entry_store'] = $this->language->get('entry_store');
		$this->data['entry_keyword'] = $this->language->get('entry_keyword');
		$this->data['entry_parent'] = $this->language->get('entry_parent');
		$this->data['entry_image'] = $this->language->get('entry_image');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_layout'] = $this->language->get('entry_layout');
		$this->data['entry_top'] = $this->language->get('entry_top');
		$this->data['entry_column'] = $this->language->get('entry_column');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

    	$this->data['tab_general'] = $this->language->get('tab_general');
    	$this->data['tab_data'] = $this->language->get('tab_data');
		$this->data['tab_design'] = $this->language->get('tab_design');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

 		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = array();
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);
		$this->data['breadcrumbs'][] = array(
       		'text'      => 'Khu vực bán hàng',
			'href'      => $this->url->link('catalog/area', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		if(isset($this->request->get['district_id'])){
			$query = $this->db->query("SELECT a.district_id as district_id, ad.name as name, a.province_id as province_id FROM " . DB_PREFIX . "district a LEFT JOIN " . DB_PREFIX . "district_description ad ON (a.district_id=ad.district_id) WHERE a.district_id ='". $this->request->get['district_id'] ."'");

			$query_province = $this->db->query("SELECT a.provinceid as province_id, ad.name as name FROM " . DB_PREFIX . "province a LEFT JOIN " . DB_PREFIX . "province_description ad ON (a.provinceid=ad.province_id) WHERE a.provinceid ='". $query->row['province_id'] ."'");

			$name_province = $query->row['name'];
			$url_province = $this->url->link('catalog/dealer', 'token=' . $this->session->data['token'].'&district_id='.$query->row['district_id'], 'SSL');

			$this->data['breadcrumbs'][] = array(
       		'text'      => $query_province->row['name'],
			'href'      => $this->url->link('catalog/district', 'token=' . $this->session->data['token'].'&province_id='.$query_province->row['province_id'], 'SSL'),
      		'separator' => ' :: '
			);

		}else{
			$name_province = 'Tất cả các đại lý';
			$url_province = $this->url->link('catalog/dealer', 'token=' . $this->session->data['token'], 'SSL');
		}

		$this->data['breadcrumbs'][] = array(
       		'text'      => $name_province,
			'href'      => $url_province,
      		'separator' => ' :: '
		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/dealer', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);

		if (!isset($this->request->get['dealer_id'])) {
			$this->data['action'] = $this->url->link('catalog/dealer/insert', 'token=' . $this->session->data['token'], 'SSL');
		} else {
			$this->data['action'] = $this->url->link('catalog/dealer/update', 'token=' . $this->session->data['token'] . '&dealer_id=' . $this->request->get['dealer_id'], 'SSL');
		}

		$url = '';
		if(isset($this->request->get['district_id'])){
			$url .= '&district_id='.$this->request->get['district_id'];
		}
		$this->data['cancel'] = $this->url->link('catalog/dealer', 'token=' . $this->session->data['token'].$url, 'SSL');

		$this->data['token'] = $this->session->data['token'];

		if (isset($this->request->get['dealer_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
      		$Dealer_info = $this->model_catalog_dealer->getDealer($this->request->get['dealer_id']);
    	}

		//get district
		$this->load->model('catalog/district');
		$this->load->model('catalog/product');
		$this->data['districts'] = array();
		if(isset($Dealer_info)){
			$this->data['districts'] = $this->model_catalog_district->getDistricts($Dealer_info['province_id']);
		} elseif (isset($this->request->get['dealer_id'])) {
			$this->data['districts'] = $Dealer_info['province_id'];
		}

		$this->load->model('catalog/province');
		$this->data['provinces'] = $this->model_catalog_province->getProvinces(null);

		// var_dump($this->data['districts']);
		$this->load->model('localisation/language');

		$this->data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->post['dealer_description'])) {
			$this->data['dealer_description'] = $this->request->post['dealer_description'];
		} elseif (isset($Dealer_info)) {
			$this->data['dealer_description'] = $this->model_catalog_dealer->getDealerDescriptions($this->request->get['dealer_id']);
		} else {
			$this->data['dealer_description'] = array();
		}
		// var_dump($this->data['dealer_description']);


		/* $dealers = $this->model_catalog_dealer->getDealers(0);

		// Remove own id from list
		if (isset($Dealer_info)) {
			foreach ($dealers as $key => $news_category) {
				if ($news_category['dealer_id'] == $Dealer_info['dealer_id']) {
					unset($dealers[$key]);
				}
			}
		}

		$this->data['dealers'] = $dealers; */


		if (isset($this->request->post['keyword'])) {
			$this->data['keyword'] = $this->request->post['keyword'];
		} elseif (isset($Dealer_info)) {
			$this->data['keyword'] = $Dealer_info['keyword'];
		} else {
			$this->data['keyword'] = '';
		}

		if (isset($this->request->post['district_id'])) {
			$this->data['district'] = $this->request->post['district_id'];
		} elseif (isset($this->request->get['district_id'])) {
			$this->data['district'] = $this->request->get['district_id'];
		} elseif (isset($Dealer_info)) {
			$this->data['district'] = $Dealer_info['district_id'];
		} else {
			$this->data['district'] = '';
		}

		if (isset($this->request->post['province_id'])) {
			$this->data['province'] = $this->request->post['province_id'];
		} elseif (isset($this->request->get['province_id'])) {
			$this->data['province'] = $this->request->get['province_id'];
		} elseif (isset($Dealer_info)) {
			$this->data['province'] = $Dealer_info['province_id'];
		} else {
			$this->data['province'] = '';
		}


		if (isset($this->request->post['telephone'])) {
			$this->data['telephone'] = trim($this->request->post['telephone']);
		} elseif (isset($Dealer_info)) {
			$this->data['telephone'] = $Dealer_info['telephone'];
		} else {
			$this->data['telephone'] = '';
		}

		if (isset($this->request->post['email'])) {
			$this->data['email'] = trim($this->request->post['email']);
		} elseif (isset($Dealer_info)) {
			$this->data['email'] = $Dealer_info['email'];
		} else {
			$this->data['email'] = '';
		}


		$this->data['pros'] = $this->model_catalog_product->getProducts();

		if (isset($this->request->post['product'])) {
			$this->data['product'] = trim($this->request->post['product']);
		} elseif (isset($Dealer_info)) {
			$this->data['product'] = $Dealer_info['product'];
		} else {
			$this->data['product'] = '';
		}

		if (isset($this->request->post['sort_order'])) {
			$this->data['sort_order'] = $this->request->post['sort_order'];
		} elseif (isset($Dealer_info)) {
			$this->data['sort_order'] = $Dealer_info['sort_order'];
		} else {
			$this->data['sort_order'] = 0;
		}

		if (isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
		} elseif (isset($Dealer_info)) {
			$this->data['status'] = $Dealer_info['status'];
		} else {
			$this->data['status'] = 1;
		}


		$this->template = 'catalog/dealer_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer',
		);

		$this->response->setOutput($this->render());
	}

	private function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/dealer')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		foreach ($this->request->post['dealer_description'] as $language_id => $value) {
			// if ((strlen(utf8_decode($value['name'])) < 2) || (strlen(utf8_decode($value['name'])) > 255)) {
			if (((strlen(utf8_decode($value['name'])) < 2) || (strlen(utf8_decode($value['name'])) > 255)) && $language_id == 2) {
				$this->error['name'][$language_id] = $this->language->get('error_name');
			}
		}

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	private function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/dealer')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
}
?>
