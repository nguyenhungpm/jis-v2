<?php
class ControllerCatalogRecCat extends Controller {
	private $error = array();
 
	public function index() {
		$this->load->language('catalog/reccat');
		 
		$this->document->setTitle($this->language->get('heading_title'));
 		
		$this->load->model('catalog/reccat');
		
		$this->getList();
	}

	public function insert() {
		$json = array();
		$this->load->language('catalog/reccat');

		$this->document->setTitle($this->language->get('heading_title'));
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->load->model('catalog/reccat');
			$this->model_catalog_reccat->addreccat($this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_add');

			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			
			// $this->redirect($this->url->link('catalog/reccat', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
		$json = 'thanh cong';
		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
	}
	
	public function edit(){
        $json = array();
		$this->load->language('catalog/reccat');
		$this->load->model('catalog/reccat');

		$v = $this->model_catalog_reccat->editRecCat($this->request->get['reccat_id'], $this->request->post);
		if($v){
			$this->session->data['success'] = $this->language->get('text_edit');
		}

		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
	}
	
	public function delete() { 
		$this->load->language('catalog/reccat');

		$this->document->setTitle($this->language->get('heading_title'));		
		
		$this->load->model('catalog/reccat');
		
		if (isset($this->request->post['selected'])) {
      		foreach ($this->request->post['selected'] as $id) {
				$this->model_catalog_reccat->deleteReccat($id);	
			}
						
			$this->session->data['success'] = $this->language->get('text_success');
			
			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			
			$this->redirect($this->url->link('catalog/reccat', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}

		$this->getList();
	}

	private function getList() {
		$this->load->model('localisation/language');
		$this->data['languages'] = $this->model_localisation_language->getLanguages();
		$this->load->model('tool/image');
	
  		$this->data['breadcrumbs'] = array();

  		$this->data['breadcrumbs'][] = array(
       		'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
       		'text'      => $this->language->get('text_home'),
      		'separator' => FALSE
   		);

  		$this->data['breadcrumbs'][] = array(
       		'href'      => $this->url->link('catalog/reccat', 'token=' . $this->session->data['token'], 'SSL'),
       		'text'      => $this->language->get('heading_title'),
      		'separator' => ' :: '
   		);
							
		
		$this->data['insert'] = $this->url->link('catalog/reccat/insert', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['delete'] = $this->url->link('catalog/reccat/delete', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['export'] = $this->url->link('catalog/reccat/export', 'token=' . $this->session->data['token'], 'SSL');	
		
		$this->data['token'] = $this->session->data['token'];
	
		$this->data['text_export'] = $this->language->get('text_export');
		$url = '';
		if(isset($this->request->get['page'])) {
	            $page = $this->request->get['page'];
	            $this->data['page'] = $this->request->get['page'];
				$url .= '&page='. $this->request->get['page'];
		}else{
		        $page = 1;
		}
	
		$this->data['reccats'] = array();
		
		$data = array(
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);
		
		$reccat_total = $this->model_catalog_reccat->getTotalRecCats($data);
		
		$results = $this->model_catalog_reccat->getRecCats($data);

		foreach ($results as $result) {
			$detail = $this->model_catalog_reccat->getDetail($result['reccat_id']);
			
			$this->data['reccats'][] = array(
				'reccat_id' 		=> $result['reccat_id'],
				'sort_order' 	=> $result['sort_order'],
				'detail' 	=> $detail,
				'status' 	=> $result['status'],
				'edit' => $this->url->link('catalog/reccat', 'token=' . $this->session->data['token'] . '&reccat_id=' . $result['reccat_id'] . $url, 'SSL')
			);
		}	
	
		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_reccat'] = $this->language->get('column_reccat');
		
		$this->data['column_action'] = $this->language->get('column_action');

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
 
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		
		$url = "";
		
		
		$pagination = new Pagination();
		$pagination->total = $reccat_total;
		$pagination->page  = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text  = $this->language->get('text_pagination');
		$pagination->url   = $this->url->link('catalog/reccat/', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['sort_name'] = $this->url->link('catalog/reccat/', 'token=' . $this->session->data['token']);
		
		$this->template = 'catalog/reccat_list.tpl';
		$this->children = array(
			'common/header',	
			'common/footer'	
		);
		
		$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
 	}

	private function validateForm() {
		$this->load->model('catalog/reccat');
		
		if (!$this->user->hasPermission('modify', 'catalog/reccat')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
	 	

		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

}
?>