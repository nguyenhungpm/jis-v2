<?php
class ControllerCatalogManucat extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/manucat');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/manucat');

		$this->getList();
	}

	public function insert() {
		$json = array();
		$this->load->language('catalog/manucat');

		$this->document->setTitle($this->language->get('heading_title'));

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->load->model('catalog/manucat');
			$this->model_catalog_manucat->addmanucat($this->request->post);

			$this->session->data['success'] = $this->language->get('text_add');

			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			// $this->redirect($this->url->link('catalog/manucat', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
		$json = 'Thêm thành công';
		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
	}

	public function edit(){
        $json = array();
		$this->load->language('catalog/manucat');
		$this->load->model('catalog/manucat');

		$v = $this->model_catalog_manucat->editmanucat($this->request->get['manucat_id'], $this->request->post);
		if($v){
			$this->session->data['success'] = $this->language->get('text_edit');
		}

		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
	}

	public function delete() {
		$this->load->language('catalog/manucat');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/manucat');

		if (isset($this->request->post['selected'])) {
      		foreach ($this->request->post['selected'] as $id) {
				$this->model_catalog_manucat->deletemanucat($id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			$this->redirect($this->url->link('catalog/manucat', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}

		$this->getList();
	}

	private function getList() {
		$this->load->model('localisation/language');
		$this->data['languages'] = $this->model_localisation_language->getLanguages();
		$this->load->model('tool/image');

  		$this->data['breadcrumbs'] = array();

  		$this->data['breadcrumbs'][] = array(
       		'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
       		'text'      => $this->language->get('text_home'),
      		'separator' => FALSE
   		);

  		$this->data['breadcrumbs'][] = array(
       		'href'      => $this->url->link('catalog/manucat', 'token=' . $this->session->data['token'], 'SSL'),
       		'text'      => $this->language->get('heading_title'),
      		'separator' => ' :: '
   		);


		$this->data['insert'] = $this->url->link('catalog/manucat/insert', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['delete'] = $this->url->link('catalog/manucat/delete', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['export'] = $this->url->link('catalog/manucat/export', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['token'] = $this->session->data['token'];

		$this->data['text_export'] = $this->language->get('text_export');
		$url = '';
		if(isset($this->request->get['page'])) {
        $page = $this->request->get['page'];
        $this->data['page'] = $this->request->get['page'];
				$url .= '&page='. $this->request->get['page'];
		}else{
		      $page = 1;
		}

		$this->data['manucats'] = array();

		$data = array(
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		$manucat_total = $this->model_catalog_manucat->getTotalmanucats($data);

		$results = $this->model_catalog_manucat->getmanucats($data);

		foreach ($results as $result) {
			$detail = $this->model_catalog_manucat->getDetail($result['manucat_id']);
			$keyword_info = $this->model_catalog_manucat->getKeyword($result['manucat_id']);
			if($keyword_info){
				$keyword = $keyword_info;
			}else{
				$keyword = '';
			}

			$this->data['manucats'][] = array(
				'manucat_id' 		=> $result['manucat_id'],
				'sort_order' 	=> $result['sort_order'],
				'detail' 	=> $detail,
				'keyword' 	=> $keyword,
				'status' 	=> $result['status'],
				'edit' => $this->url->link('catalog/manucat', 'token=' . $this->session->data['token'] . '&manucat_id=' . $result['manucat_id'] . $url, 'SSL')
			);
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_manucat'] = $this->language->get('column_manucat');

		$this->data['column_action'] = $this->language->get('column_action');

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}


		$url = "";


		$pagination = new Pagination();
		$pagination->total = $manucat_total;
		$pagination->page  = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text  = $this->language->get('text_pagination');
		$pagination->url   = $this->url->link('catalog/manucat/', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['sort_name'] = $this->url->link('catalog/manucat/', 'token=' . $this->session->data['token']);

		$this->template = 'catalog/manucat_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
 	}

	private function validateForm() {
		$this->load->model('catalog/manucat');

		if (!$this->user->hasPermission('modify', 'catalog/manucat')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}


		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

}
?>
