<?php
class ControllerCatalogProvince extends Controller { 
	private $error = array();
 
	public function index() {
		$this->load->language('catalog/province');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('catalog/province');
		 
		$this->getList();
	}

	public function insert() {
		$this->load->language('catalog/province');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('catalog/province');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_province->addProvince($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');
			
			$this->redirect($this->url->link('catalog/province', 'token=' . $this->session->data['token'], 'SSL')); 
		}

		$this->getForm();
	}

	public function update() {
		$this->load->language('catalog/province');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('catalog/province');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_province->editProvince($this->request->get['provinceid'], $this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success');
			
			$this->redirect($this->url->link('catalog/province', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/province');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('catalog/province');
		
		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $provinceid) {
				$this->model_catalog_province->deleteProvince($provinceid);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$this->redirect($this->url->link('catalog/province', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->getList();
	}

	private function getList() {
   		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => 'Khu vực bán hàng',
			'href'      => $this->url->link('catalog/area', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		if(isset($this->request->get['area_id'])){
			$query = $this->db->query("SELECT a.area_id as area_id, ad.name as name FROM " . DB_PREFIX . "area a LEFT JOIN " . DB_PREFIX . "area_description ad ON (a.area_id=ad.area_id) WHERE a.area_id ='". $this->request->get['area_id'] ."'");
			$name_area = $query->row['name'];
			$url_area = $this->url->link('catalog/province', 'token=' . $this->session->data['token'].'&area_id='.$query->row['area_id'], 'SSL');
		}else{
			$name_area = 'Tất cả các tỉnh thành phố';
			$url_area = $this->url->link('catalog/province', 'token=' . $this->session->data['token'], 'SSL');
		}
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $name_area,
			'href'      => $url_area,
      		'separator' => ' :: '
   		);
		if(isset($this->request->get['district_id'])){
			$this->data['insert'] = $this->url->link('catalog/province/insert', 'token=' . $this->session->data['token'].'&area_id='.$this->request->get['area_id'], 'SSL');
		}else{
			$this->data['insert'] = $this->url->link('catalog/province/insert', 'token=' . $this->session->data['token'], 'SSL');
		}
		
		
		// $this->data['insert'] = $this->url->link('catalog/province/insert', 'token=' . $this->session->data['token'].'&area_id='.$this->request->get['area_id'], 'SSL');
		$this->data['delete'] = $this->url->link('catalog/province/delete', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['provinces'] = array();
		if(isset($this->request->get['area_id'])){
			$area_id = $this->request->get['area_id'];
		}else{
			$area_id = null;
		}
		
		$results = $this->model_catalog_province->getProvinces($area_id);

		foreach ($results as $result) {
			$action = array();
			
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('catalog/province/update', 'token=' . $this->session->data['token'] . '&provinceid=' . $result['provinceid'], 'SSL')
			);
					
			$this->data['provinces'][] = array(
				'provinceid' => $result['provinceid'],
				'name'        => $result['name'],
				'view' => $this->url->link('catalog/district', 'token=' . $this->session->data['token'] . '&province_id=' . $result['provinceid'], 'SSL'),
				'sort_order'  => $result['sort_order'],
				'selected'    => isset($this->request->post['selected']) && in_array($result['provinceid'], $this->request->post['selected']),
				'action'      => $action
			);
		}
		
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_sort_order'] = $this->language->get('column_sort_order');
		$this->data['column_action'] = $this->language->get('column_action');

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
 
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		$this->template = 'catalog/province_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer',
		);
				
		$this->response->setOutput($this->render());
	}

	private function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_none'] = $this->language->get('text_none');
		$this->data['text_default'] = $this->language->get('text_default');
		$this->data['text_image_manager'] = $this->language->get('text_image_manager');
		$this->data['text_browse'] = $this->language->get('text_browse');
		$this->data['text_clear'] = $this->language->get('text_clear');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
    	$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_percent'] = $this->language->get('text_percent');
		$this->data['text_amount'] = $this->language->get('text_amount');
				
		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$this->data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$this->data['entry_description'] = $this->language->get('entry_description');
		$this->data['entry_store'] = $this->language->get('entry_store');
		$this->data['entry_keyword'] = $this->language->get('entry_keyword');
		$this->data['entry_parent'] = $this->language->get('entry_parent');
		$this->data['entry_image'] = $this->language->get('entry_image');		
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_layout'] = $this->language->get('entry_layout');
		$this->data['entry_top'] = $this->language->get('entry_top');
		$this->data['entry_column'] = $this->language->get('entry_column');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

    	$this->data['tab_general'] = $this->language->get('tab_general');
    	$this->data['tab_data'] = $this->language->get('tab_data');
		$this->data['tab_design'] = $this->language->get('tab_design');
		
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
	
 		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = array();
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/province', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		if (!isset($this->request->get['provinceid'])) {
			$this->data['action'] = $this->url->link('catalog/province/insert', 'token=' . $this->session->data['token'], 'SSL');
		} else {
			$this->data['action'] = $this->url->link('catalog/province/update', 'token=' . $this->session->data['token'] . '&provinceid=' . $this->request->get['provinceid'], 'SSL');
		}
		
		$this->data['cancel'] = $this->url->link('catalog/province', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['token'] = $this->session->data['token'];

		if (isset($this->request->get['provinceid']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
      		$Province_info = $this->model_catalog_province->getProvince($this->request->get['provinceid']);
    	}
		
		//get areas
		$this->load->model('catalog/area');
		$this->data['areas'] = $this->model_catalog_area->getAreas();
		
		$this->load->model('localisation/language');
		
		$this->data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->post['province_description'])) {
			$this->data['province_description'] = $this->request->post['province_description'];
		} elseif (isset($Province_info)) {
			$this->data['province_description'] = $this->model_catalog_province->getProvinceDescriptions($this->request->get['provinceid']);
		} else {
			$this->data['province_description'] = array();
		}
		

		/* $provinces = $this->model_catalog_province->getProvinces(0);

		// Remove own id from list
		if (isset($Province_info)) {
			foreach ($provinces as $key => $news_category) {
				if ($news_category['provinceid'] == $Province_info['provinceid']) {
					unset($provinces[$key]);
				}
			}
		}

		$this->data['provinces'] = $provinces; */
		
		if (isset($this->request->post['keyword'])) {
			$this->data['keyword'] = $this->request->post['keyword'];
		} elseif (isset($Province_info)) {
			$this->data['keyword'] = $Province_info['keyword'];
		} else {
			$this->data['keyword'] = '';
		}

		if (isset($this->request->post['area_id'])) {
			$this->data['area'] = $this->request->post['area_id'];
		} elseif (isset($this->request->get['area_id'])) {
			$this->data['area'] = $this->request->get['area_id'];
		} elseif (isset($Province_info)) {
			$this->data['area'] = $Province_info['area_id'];
		} else {
			$this->data['area'] = '';
		}
		
		if (isset($this->request->post['sort_order'])) {
			$this->data['sort_order'] = $this->request->post['sort_order'];
		} elseif (isset($Province_info)) {
			$this->data['sort_order'] = $Province_info['sort_order'];
		} else {
			$this->data['sort_order'] = 0;
		}
		
		if (isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
		} elseif (isset($Province_info)) {
			$this->data['status'] = $Province_info['status'];
		} else {
			$this->data['status'] = 1;
		}
		$this->load->model('design/layout');
		$this->data['layouts'] = $this->model_design_layout->getLayouts();
						
		$this->template = 'catalog/province_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer',
		);
				
		$this->response->setOutput($this->render());
	}

	private function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/province')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		foreach ($this->request->post['province_description'] as $language_id => $value) {
			if ((strlen(utf8_decode($value['name'])) < 2) || (strlen(utf8_decode($value['name'])) > 255)) {
				$this->error['name'][$language_id] = $this->language->get('error_name');
			}
		}
		
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
					
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	private function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/province')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
 
		if (!$this->error) {
			return true; 
		} else {
			return false;
		}
	}
}
?>