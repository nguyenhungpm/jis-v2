<?php
class ControllerCatalogDoccat extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/doccat');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/doccat');

		$this->getList();
	}

	public function insert() {
		$json = array();
		$this->load->language('catalog/doccat');

		$this->document->setTitle($this->language->get('heading_title'));

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->load->model('catalog/doccat');
			$this->model_catalog_doccat->adddoccat($this->request->post);

			$this->session->data['success'] = $this->language->get('text_add');

			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			// $this->redirect($this->url->link('catalog/doccat', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
		$json = 'Thêm thành công';
		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
	}

	public function edit(){
        $json = array();
		$this->load->language('catalog/doccat');
		$this->load->model('catalog/doccat');

		$v = $this->model_catalog_doccat->editdoccat($this->request->get['doccat_id'], $this->request->post);
		if($v){
			$this->session->data['success'] = $this->language->get('text_edit');
		}

		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
	}

	public function delete() {
		$this->load->language('catalog/doccat');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/doccat');

		if (isset($this->request->post['selected'])) {
      		foreach ($this->request->post['selected'] as $id) {
				$this->model_catalog_doccat->deletedoccat($id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			$this->redirect($this->url->link('catalog/doccat', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}

		$this->getList();
	}

	private function getList() {
		$this->load->model('localisation/language');
		$this->data['languages'] = $this->model_localisation_language->getLanguages();
		$this->load->model('tool/image');

  		$this->data['breadcrumbs'] = array();

  		$this->data['breadcrumbs'][] = array(
       		'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
       		'text'      => $this->language->get('text_home'),
      		'separator' => FALSE
   		);

  		$this->data['breadcrumbs'][] = array(
       		'href'      => $this->url->link('catalog/doccat', 'token=' . $this->session->data['token'], 'SSL'),
       		'text'      => $this->language->get('heading_title'),
      		'separator' => ' :: '
   		);


		$this->data['insert'] = $this->url->link('catalog/doccat/insert', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['delete'] = $this->url->link('catalog/doccat/delete', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['export'] = $this->url->link('catalog/doccat/export', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['token'] = $this->session->data['token'];

		$this->data['text_export'] = $this->language->get('text_export');
		$url = '';
		if(isset($this->request->get['page'])) {
        $page = $this->request->get['page'];
        $this->data['page'] = $this->request->get['page'];
				$url .= '&page='. $this->request->get['page'];
		}else{
		      $page = 1;
		}

		$this->data['doccats'] = array();

		$data = array(
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		$doccat_total = $this->model_catalog_doccat->getTotaldoccats($data);

		$results = $this->model_catalog_doccat->getdoccats($data);

		foreach ($results as $result) {
			$detail = $this->model_catalog_doccat->getDetail($result['doccat_id']);
			$keyword_info = $this->model_catalog_doccat->getKeyword($result['doccat_id']);
			if($keyword_info){
				$keyword = $keyword_info;
			}else{
				$keyword = '';
			}

			$this->data['doccats'][] = array(
				'doccat_id' 		=> $result['doccat_id'],
				'sort_order' 	=> $result['sort_order'],
				'detail' 	=> $detail,
				'keyword' 	=> $keyword,
				'status' 	=> $result['status'],
				'edit' => $this->url->link('catalog/doccat', 'token=' . $this->session->data['token'] . '&doccat_id=' . $result['doccat_id'] . $url, 'SSL')
			);
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_doccat'] = $this->language->get('column_doccat');

		$this->data['column_action'] = $this->language->get('column_action');

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}


		$url = "";


		$pagination = new Pagination();
		$pagination->total = $doccat_total;
		$pagination->page  = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text  = $this->language->get('text_pagination');
		$pagination->url   = $this->url->link('catalog/doccat/', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['sort_name'] = $this->url->link('catalog/doccat/', 'token=' . $this->session->data['token']);

		$this->template = 'catalog/doccat_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
 	}

	private function validateForm() {
		$this->load->model('catalog/doccat');

		if (!$this->user->hasPermission('modify', 'catalog/doccat')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}


		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

}
?>
