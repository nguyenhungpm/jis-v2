<?php
class ControllerCatalogQuiz extends Controller {

	private $error = array();

	public function index() {
		$this->language->load('catalog/quiz');
		$this->data['button_preview']           = $this->language->get('button_preview');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/quiz');

		$this->getList();
	}

	public function updatestatus() {
		$this->load->language('catalog/quiz');
		$this->load->model('catalog/quiz');
		$output='';
		if(isset($this->request->get['object_id'])){
			$get_request = explode('-',$this->request->get['object_id']);
			if(count($get_request)==2){
				$column_name = $get_request[0];
				$quiz_id = $get_request[1];
				$result = $this->model_catalog_quiz->getQuiz($quiz_id);
				if($result[$column_name]){
					$this->model_catalog_quiz->updateStatus($quiz_id, $column_name, 0);
					} else {
					$this->model_catalog_quiz->updateStatus($quiz_id, $column_name, 1);
				}
				$result = $this->model_catalog_quiz->getQuiz($quiz_id);
				$output = $result[$column_name] ? $this->language->get('text_enabled') : $this->language->get('text_disabled');
			}
		}
		$this->response->setOutput($output);
	}

	public function insert() {
		$this->language->load('catalog/quiz');

		$this->document->setTitle($this->language->get('heading_title_add'));

		$this->load->model('catalog/quiz');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->response->ClearCache();
			$quiz_id = $this->model_catalog_quiz->addQuiz
			($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_model'])) {
				$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_price'])) {
				$url .= '&filter_price=' . $this->request->get['filter_price'];
			}

			if (isset($this->request->get['filter_quantity'])) {
				$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}


			if (!isset($this->request->get['continue']) && !isset($this->request->get['new'])) {

				$this->redirect($this->url->link('catalog/quiz', 'token=' . $this->session->data['token'] . $url, 'SSL'));

			}
			elseif (isset($this->request->get['continue'])) {
				$this->redirect($this->url->link('catalog/quiz/update', 'token=' . $this->session->data['token'] . $url . '&quiz_id=' . $quiz_id . '&continue=true', 'SSL'));
			}
			else {
				$this->redirect($this->url->link('catalog/quiz/insert', 'token=' . $this->session->data['token'] . '&new=true', 'SSL'));
			}

		}

		$this->getForm();
	}

	public function update() {
		$this->language->load('catalog/quiz');

		$this->load->model('catalog/quiz');
		if (isset($this->request->get['quiz_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$news_info = $this->model_catalog_quiz->getNameQuiz($this->request->get['quiz_id']);
			$this->document->setTitle($news_info['name']);
			} else {
			$this->document->setTitle($this->language->get('heading_title'));
		}


		$this->load->model('catalog/quiz');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

			$this->response->ClearCache();

			$this->model_catalog_quiz->editQuiz($this->request->get['quiz_id'], $this->request->post);


			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_model'])) {
				$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_price'])) {
				$url .= '&filter_price=' . $this->request->get['filter_price'];
			}

			if (isset($this->request->get['filter_quantity'])) {
				$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}


			if (!isset($this->request->get['continue']) && !isset($this->request->get['new'])) {

				$this->redirect($this->url->link('catalog/quiz', 'token=' . $this->session->data['token'] . $url, 'SSL'));

			}
			elseif (isset($this->request->get['new'])) {
				$this->redirect($this->url->link('catalog/quiz/insert', 'token=' . $this->session->data['token'] . '&new=true', 'SSL'));
			}

		}

		$this->getForm();
	}

	public function delete() {
		$this->language->load('catalog/quiz');
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/quiz');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $quiz_id) {
				$this->model_catalog_quiz->deleteQuiz($quiz_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_model'])) {
				$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_price'])) {
				$url .= '&filter_price=' . $this->request->get['filter_price'];
			}

			if (isset($this->request->get['filter_quantity'])) {
				$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/quiz', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	public function copy() {
		$this->language->load('catalog/quiz');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/quiz');

		if (isset($this->request->post['selected']) && $this->validateCopy()) {
			foreach ($this->request->post['selected'] as $quiz_id) {
				$this->model_catalog_quiz->copyQuiz($quiz_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_model'])) {
				$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_price'])) {
				$url .= '&filter_price=' . $this->request->get['filter_price'];
			}

			if (isset($this->request->get['filter_quantity'])) {
				$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/quiz', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
			} else {
			$filter_name = null;
		}

		if (isset($this->request->get['filter_model'])) {
			$filter_model = $this->request->get['filter_model'];
			} else {
			$filter_model = null;
		}

		if (isset($this->request->get['filter_price'])) {
			$filter_price = $this->request->get['filter_price'];
			} else {
			$filter_price = null;
		}

		if (isset($this->request->get['filter_quantity'])) {
			$filter_quantity = $this->request->get['filter_quantity'];
			} else {
			$filter_quantity = null;
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
			} else {
			$filter_status = null;
		}

		if (isset($this->request->get['filter_group'])) {
			$this->data['filter_group'] = $this->request->get['filter_group'];
			$filter_group = $this->request->get['filter_group'];
			} else {
			$this->data['filter_group'] = null;
			$filter_group = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
			} else {
			$sort = 'pd.name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
			} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
			} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/quiz', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->data['insert'] = $this->url->link('catalog/quiz/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['copy'] = $this->url->link('catalog/quiz/copy', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('catalog/quiz/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$this->data['quizs'] = array();

		$data = array(
		'filter_name' => $filter_name,
		'display_name' => true,
		'filter_model' => $filter_model,
		'filter_price' => $filter_price,
		'filter_quantity' => $filter_quantity,
		'filter_status' => $filter_status,
		'filter_group' => $filter_group,
		'sort' => $sort,
		'order' => $order,
		'start' => ($page - 1) * $this->config->get('config_admin_limit'),
		'limit' => $this->config->get('config_admin_limit')
		);

		$this->load->model('tool/image');
		$this->load->model('catalog/group');

		$this->data['groups'] = $this->model_catalog_group->getGroups(NULL);
		$quiz_total = $this->model_catalog_quiz->getTotalQuizs($data);

		$results = $this->model_catalog_quiz->getQuizs($data);

		foreach ($results as $result) {
			$action = array();

			$action[] = array(
			'text' => $this->language->get('text_edit'),
			'href' => $this->url->link('catalog/quiz/update', 'token=' . $this->session->data['token'] . '&quiz_id=' . $result['quiz_id'] . $url, 'SSL')
			);

			if ($result['image'] && file_exists(DIR_IMAGE . $result['image'])) {
				$image = $this->model_tool_image->resize($result['image'], 40, 40);
				} else {
				$image = $this->model_tool_image->resize('no_image.jpg', 40, 40);
			}

			$special = false;

			$quiz_specials = $this->model_catalog_quiz->getQuizSpecials($result['quiz_id']);

			foreach ($quiz_specials as $quiz_special) {
				if (($quiz_special['date_start'] == '0000-00-00' || $quiz_special['date_start'] < date('Y-m-d')) && ($quiz_special['date_end'] == '0000-00-00' || $quiz_special['date_end'] > date('Y-m-d'))) {
					$special = $quiz_special['price'];

					break;
				}
			}

			$this->data['quizs'][] = array(
			'quiz_id' => $result['quiz_id'],
			'name' => $result['name'],
			'name_group' => $result['name_group'],
			'model' => $result['model'],
			'price' => $result['price'],
			'sort_order' => $result['sort_order'],
			'special' => $special,
			'image' => $image,
			'quantity' => $result['quantity'],
			'href' => $this->url->link('catalog/quiz/update', 'token=' . $this->session->data['token'] . '&quiz_id=' . $result['quiz_id'] . $url, 'SSL'),
			'status' => ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
			'selected' => isset($this->request->post['selected']) && in_array($result['quiz_id'], $this->request->post['selected']),
			'action' => $action
			);
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] 	= $this->language->get('text_enabled');
		$this->data['text_list'] 		= $this->language->get('text_list');
		$this->data['text_confuguration'] 		= $this->language->get('text_confuguration');
		$this->data['text_disabled'] 	= $this->language->get('text_disabled');
		$this->data['entry_all_group'] = $this->language->get('entry_all_group');
		$this->data['text_no_results'] 	= $this->language->get('text_no_results');
		$this->data['text_image_manager'] = $this->language->get('text_image_manager');
		$this->data['text_outofstock'] = $this->language->get('text_outofstock');
		$this->data['text_overview'] = $this->language->get('text_overview');
		$this->data['text_instock'] = $this->language->get('text_instock');

		$this->data['column_image'] = $this->language->get('column_image');
		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_group'] = $this->language->get('column_group');
		$this->data['column_price'] = $this->language->get('column_price');
		$this->data['column_quantity'] = $this->language->get('column_quantity');
		$this->data['column_status'] = $this->language->get('column_status');
		$this->data['column_action'] = $this->language->get('column_action');

		$this->data['button_copy'] = $this->language->get('button_copy');
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_filter'] = $this->language->get('button_filter');

		$this->data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
			} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
			} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
			} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['sort_name'] = $this->url->link('catalog/quiz', 'token=' . $this->session->data['token'] . '&sort=pd.name' . $url, 'SSL');
		$this->data['sort_model'] = $this->url->link('catalog/quiz', 'token=' . $this->session->data['token'] . '&sort=p.model' . $url, 'SSL');
		$this->data['sort_price'] = $this->url->link('catalog/quiz', 'token=' . $this->session->data['token'] . '&sort=p.price' . $url, 'SSL');
		$this->data['sort_quantity'] = $this->url->link('catalog/quiz', 'token=' . $this->session->data['token'] . '&sort=p.quantity' . $url, 'SSL');
		$this->data['sort_status'] = $this->url->link('catalog/quiz', 'token=' . $this->session->data['token'] . '&sort=p.status' . $url, 'SSL');
		$this->data['sort_order'] = $this->url->link('catalog/quiz', 'token=' . $this->session->data['token'] . '&sort=p.sort_order' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_group'])) {
			$url .= '&filter_group=' . urlencode(html_entity_decode($this->request->get['filter_group'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $quiz_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/quiz', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['filter_name'] = $filter_name;
		$this->data['filter_model'] = $filter_model;
		$this->data['filter_price'] = $filter_price;
		$this->data['filter_quantity'] = $filter_quantity;
		$this->data['filter_status'] = $filter_status;

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->template = 'catalog/quiz_list.tpl';
		$this->children = array(
		'common/header',
		'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_none'] = $this->language->get('text_none');
		$this->data['text_yes'] = $this->language->get('text_yes');
		$this->data['text_no'] = $this->language->get('text_no');
		$this->data['text_remain'] = $this->language->get('text_remain');
		$this->data['text_characters'] = $this->language->get('text_characters');
		$this->data['text_default'] = $this->language->get('text_default');
		$this->data['text_image_manager'] = $this->language->get('text_image_manager');
		$this->data['text_browse'] = $this->language->get('text_browse');
		$this->data['text_clear'] = $this->language->get('text_clear');
		$this->data['text_option'] = $this->language->get('text_option');
		$this->data['text_option_value'] = $this->language->get('text_option_value');
		$this->data['text_select'] = $this->language->get('text_select');
		$this->data['text_none'] = $this->language->get('text_none');
		$this->data['entry_short_description'] = $this->language->get('entry_short_description');
		$this->data['text_gallery'] = $this->language->get('text_gallery');


		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$this->data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$this->data['entry_description'] = $this->language->get('entry_description');
		$this->data['entry_store'] = $this->language->get('entry_store');
		$this->data['entry_keyword'] = $this->language->get('entry_keyword');
		$this->data['entry_model'] = $this->language->get('entry_model');
		$this->data['entry_sku'] = $this->language->get('entry_sku');
		$this->data['entry_location'] = $this->language->get('entry_location');
		$this->data['entry_minimum'] = $this->language->get('entry_minimum');
		$this->data['entry_manufacturer'] = $this->language->get('entry_manufacturer');
		$this->data['entry_shipping'] = $this->language->get('entry_shipping');
		$this->data['entry_date_available'] = $this->language->get('entry_date_available');
		$this->data['entry_quantity'] = $this->language->get('entry_quantity');
		$this->data['entry_stock_status'] = $this->language->get('entry_stock_status');
		$this->data['entry_price'] = $this->language->get('entry_price');
		$this->data['entry_weight_class'] = $this->language->get('entry_weight_class');
		$this->data['entry_weight'] = $this->language->get('entry_weight');
		$this->data['entry_dimension'] = $this->language->get('entry_dimension');
		$this->data['entry_length'] = $this->language->get('entry_length');
		$this->data['entry_image'] = $this->language->get('entry_image');
		$this->data['entry_group'] = $this->language->get('entry_group');
		$this->data['entry_filter'] = $this->language->get('entry_filter');
		$this->data['entry_related'] = $this->language->get('entry_related');
		$this->data['entry_attribute'] = $this->language->get('entry_attribute');
		$this->data['entry_text'] = $this->language->get('entry_text');
		$this->data['entry_option'] = $this->language->get('entry_option');
		$this->data['entry_option_value'] = $this->language->get('entry_option_value');
		$this->data['entry_required'] = $this->language->get('entry_required');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		$this->data['entry_priority'] = $this->language->get('entry_priority');
		$this->data['entry_tag'] = $this->language->get('entry_tag');
		$this->data['entry_customer_group'] = $this->language->get('entry_customer_group');
		$this->data['entry_related_quizs'] = $this->language->get('entry_related_quizs');
		$this->data['entry_related_news'] = $this->language->get('entry_related_news');
		$this->data['entry_layout'] = $this->language->get('entry_layout');
		$this->data['entry_profile'] = $this->language->get('entry_profile');
		$this->data['text_page_details'] = $this->language->get('text_page_details');

		$this->data['text_length_day'] = $this->language->get('text_length_day');
		$this->data['text_length_week'] = $this->language->get('text_length_week');
		$this->data['text_length_month'] = $this->language->get('text_length_month');
		$this->data['text_length_month_semi'] = $this->language->get('text_length_month_semi');
		$this->data['text_length_year'] = $this->language->get('text_length_year');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_saveadd'] = $this->language->get('button_saveadd');
		$this->data['button_saveclose'] = $this->language->get('button_saveclose');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_close'] = $this->language->get('button_close');
		$this->data['button_add_attribute'] = $this->language->get('button_add_attribute');
		$this->data['button_add_option'] = $this->language->get('button_add_option');
		$this->data['button_add_option_value'] = $this->language->get('button_add_option_value');
		$this->data['button_add_discount'] = $this->language->get('button_add_discount');
		$this->data['button_add_special'] = $this->language->get('button_add_special');
		$this->data['button_add_image'] = $this->language->get('button_add_image');
		$this->data['button_remove'] = $this->language->get('button_remove');
		$this->data['button_add_profile'] = $this->language->get('button_add_profile');



		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
			} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['seo'])) {
			$this->data['error_seo'] = $this->error['seo'];
			} else {
			$this->data['error_seo'] = array();
		}

		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
			} else {
			$this->data['error_name'] = array();
		}

		if (isset($this->error['meta_description'])) {
			$this->data['error_meta_description'] = $this->error['meta_description'];
			} else {
			$this->data['error_meta_description'] = array();
		}

		if (isset($this->error['description'])) {
			$this->data['error_description'] = $this->error['description'];
			} else {
			$this->data['error_description'] = array();
		}


		if ((isset($this->request->get['continue']) || isset($this->request->get['new'])) && (!isset($this->error) || empty($this->error))) {
			$this->data['success'] = $this->language->get('text_success');
		}
		if (isset($this->error['model'])) {

			$this->data['error_model'] = $this->error['model'];
			} else {
			$this->data['error_model'] = '';
		}

		if (isset($this->error['date_available'])) {
			$this->data['error_date_available'] = $this->error['date_available'];
			} else {
			$this->data['error_date_available'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
		'text' => $this->language->get('text_home'),
		'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
		'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
		'text' => $this->language->get('heading_title'),
		'href' => $this->url->link('catalog/quiz', 'token=' . $this->session->data['token'] . $url, 'SSL'),
		'separator' => ' :: '
		);

		if (!isset($this->request->get['quiz_id'])) {
			$this->data['action'] = $this->url->link('catalog/quiz/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
			} else {
			$this->data['quiz_id'] = $this->request->get['quiz_id'];
			$this->data['delete'] = $this->url->link('catalog/quiz/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
			$this->data['action'] = $this->url->link('catalog/quiz/update', 'token=' . $this->session->data['token'] . '&quiz_id=' . $this->request->get['quiz_id'] . $url, 'SSL');
		}

		$this->data['cancel'] = $this->url->link('catalog/quiz', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->get['quiz_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$quiz_info = $this->model_catalog_quiz->getQuiz($this->request->get['quiz_id']);
		}

		$this->data['token'] = $this->session->data['token'];

		// Tin lien quan
		$this->load->model('catalog/quiz');
		if (isset($this->request->post['quiz_news'])) {
			$quiz_newss = $this->request->post['quiz_news'];
			} elseif (isset($this->request->get['quiz_id'])) {
			$quiz_newss = $this->model_catalog_quiz->getQuizNews($this->request->get['quiz_id']);
			} else {
			$quiz_newss = array();
		}

		$this->data['quiz_newss'] = array();

		$this->load->model('catalog/news');
		foreach ($quiz_newss as $news_id) {
			$glossary_info = $this->model_catalog_news->getNew($news_id);

			if (isset($glossary_info) && !empty($glossary_info)) {
				$this->data['quiz_newss'][] = array(
				'news_id' => $glossary_info['news_id'],
				'name' => $glossary_info['name']
				);
			}
		}
		// Ket thuc

		$this->load->model('localisation/language');

		$this->data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->post['quiz_description'])) {
			$this->data['quiz_description'] = $this->request->post['quiz_description'];
			} elseif (isset($this->request->get['quiz_id'])) {
			$this->data['quiz_description'] = $this->model_catalog_quiz->getQuizDescriptions($this->request->get['quiz_id']);
			} else {
			$this->data['quiz_description'] = array();
		}

		if (isset($this->request->post['model'])) {
			$this->data['model'] = $this->request->post['model'];
			} elseif (!empty($quiz_info)) {
			$this->data['model'] = $quiz_info['model'];
			} else {
			$this->data['model'] = '';
		}

		if (isset($this->request->post['keyword'])) {
			$this->data['keyword'] = $this->request->post['keyword'];
			} elseif (!empty($quiz_info)) {
			$this->data['keyword'] = $quiz_info['keyword'];
			} else {
			$this->data['keyword'] = '';
		}

		if (isset($this->request->post['image'])) {
			$this->data['image'] = $this->request->post['image'];
			} elseif (!empty($quiz_info)) {
			$this->data['image'] = $quiz_info['image'];
			} else {
			$this->data['image'] = '';
		}

		$this->load->model('tool/image');

		if (isset($this->request->post['image']) && file_exists(DIR_IMAGE . $this->request->post['image'])) {
			$this->data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
			} elseif (!empty($quiz_info) && $quiz_info['image'] && file_exists(DIR_IMAGE . $quiz_info['image'])) {
			$this->data['thumb'] = $this->model_tool_image->resize($quiz_info['image'], 100, 100);
			} else {
			$this->data['thumb'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
		}

		if (isset($this->request->post['price'])) {
			$this->data['price'] = $this->request->post['price'];
			} elseif (!empty($quiz_info)) {
			$this->data['price'] = $quiz_info['price'];
			} else {
			$this->data['price'] = '';
		}

		if (isset($this->request->post['robots'])) {
			$this->data['robots'] = $this->request->post['robots'];
			} elseif (!empty($quiz_info)) {
			$this->data['robots'] = $quiz_info['robots'];
			} else {
			$this->data['robots'] = '';
		}

		if (isset($this->request->post['date_available'])) {
			$this->data['date_available'] = $this->request->post['date_available'];
			} elseif (!empty($quiz_info['date_available']) && $quiz_info['date_available']!= '0000-00-00 00:00:00') {
			$this->data['date_available'] = date('Y-m-d', strtotime($quiz_info['date_available']));
			} else {
			$this->data['date_available'] = date('Y-m-d', time() - 86400);
		}

		if (isset($this->request->post['quantity'])) {
			$this->data['quantity'] = $this->request->post['quantity'];
			} elseif (!empty($quiz_info)) {
			$this->data['quantity'] = $quiz_info['quantity'];
			} else {
			$this->data['quantity'] = 1;
		}

		if (isset($this->request->post['sort_order'])) {
			$this->data['sort_order'] = $this->request->post['sort_order'];
			} elseif (!empty($quiz_info)) {
			$this->data['sort_order'] = $quiz_info['sort_order'];
			} else {
			$this->data['sort_order'] = 1;
		}

		if (isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
			} elseif (!empty($quiz_info)) {
			$this->data['status'] = $quiz_info['status'];
			} else {
			$this->data['status'] = 1;
		}

		if (isset($this->request->post['group'])) {
            $this->data['group'] = $this->request->post['group'];
        } elseif (isset($quiz_info)) {
            $this->data['group'] = join(',',$this->model_catalog_quiz->getQuizGroups($this->request->get['quiz_id']));
        } else {
            $this->data['group'] = '';
        }

		$this->load->model('catalog/manufacturer');

		if (isset($this->request->post['manufacturer_id'])) {
			$this->data['manufacturer_id'] = $this->request->post['manufacturer_id'];
			} elseif (!empty($quiz_info)) {
			$this->data['manufacturer_id'] = $quiz_info['manufacturer_id'];
			} else {
			$this->data['manufacturer_id'] = 0;
		}

		if (isset($this->request->post['manufacturer'])) {
			$this->data['manufacturer'] = $this->request->post['manufacturer'];
			} elseif (!empty($quiz_info)) {
			$manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($quiz_info['manufacturer_id']);

			if ($manufacturer_info) {
						$this->data['manufacturer'] = $manufacturer_info['name'];
						} else {
						$this->data['manufacturer'] = '';
					}
			} else {
			$this->data['manufacturer'] = '';
		}

		// Groups
		$this->load->model('catalog/group');
		$this->document->addStyle('view/stylesheet/lte/select2.min.css');

		// $this->data['groups'] = $this->model_catalog_group->getGroups(NULL);
		$this->data['groups'] = array();
		foreach($this->model_catalog_group->getGroupByParent(0) as $rs){
			$cats = array();
			foreach($this->model_catalog_group->getGroupByParent($rs['group_id']) as $r){
				$cats[] = array(
					'group_id' => $r['group_id'],
					'name' => $r['name'],
				);
			}
			$this->data['groups'][] = array(
				'group_id' => $rs['group_id'],
				'name' => $rs['name'],
				'group' => $cats,
			);
		}
		// print_r($this->model_catalog_group->getGroups(0));

		if (isset($this->request->post['quiz_group'])) {
			$groups = $this->request->post['quiz_group'];
			} elseif (isset($this->request->get['quiz_id'])) {
			$groups = $this->model_catalog_quiz->getQuizGroups($this->request->get['quiz_id']);
			} else {
			$groups = array();
		}

		$this->data['quiz_groups'] = array();

		foreach ($groups as $group_id) {
			$group_info = $this->model_catalog_group->getGroup($group_id);

			if ($group_info) {
				$this->data['quiz_groups'][] = array(
					'group_id' => $group_info['group_id'],
					'name' => ($group_info['path'] ? $group_info['path'] . ' &gt; ' : '') . $group_info['name']
				);
			}
		}

		if (isset($this->request->post['quiz_special'])) {
			$this->data['quiz_specials'] = $this->request->post['quiz_special'];
			} elseif (isset($this->request->get['quiz_id'])) {
			$this->data['quiz_specials'] = $this->model_catalog_quiz->getQuizSpecials($this->request->get['quiz_id']);
			} else {
			$this->data['quiz_specials'] = array();
		}

		// Images
		if (isset($this->request->post['quiz_image'])) {
			$quiz_images = $this->request->post['quiz_image'];
			} elseif (isset($this->request->get['quiz_id'])) {
			$quiz_images = $this->model_catalog_quiz->getQuizImages($this->request->get['quiz_id']);
			} else {
			$quiz_images = array();
		}

		$this->data['quiz_images'] = array();
		foreach ($quiz_images as $quiz_image) {
			if ($quiz_image['image'] && file_exists(DIR_IMAGE . $quiz_image['image'])) {
				$image = $quiz_image['image'];
				} else {
				$image = 'no_image.jpg';
			}

			$this->data['quiz_images'][] = array(
				'image' => $image,
				'thumb' => $this->model_tool_image->resize($image, 100, 100),
				'question' => $quiz_image['question'],
				'dungsai' => $quiz_image['dungsai'],
				'sort_order' => $quiz_image['sort_order']
			);
		}

		$this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);

		if (isset($this->request->post['quiz_related'])) {
			$quizs = $this->request->post['quiz_related'];
			} elseif (isset($this->request->get['quiz_id'])) {
			$quizs = $this->model_catalog_quiz->getQuizRelated($this->request->get['quiz_id']);
			} else {
			$quizs = array();
		}

		$this->data['quiz_related'] = array();

		foreach ($quizs as $quiz_id) {
			$related_info = $this->model_catalog_quiz->getQuiz($quiz_id);

			if ($related_info) {
				$this->data['quiz_related'][] = array(
				'quiz_id' => $related_info['quiz_id'],
				'name' => $related_info['name']
				);
			}
		}

		$this->load->model('design/layout');

		$this->data['layouts'] = $this->model_design_layout->getLayouts();

		$this->template = 'catalog/quiz_form.tpl';
		$this->children = array(
		'common/header',
		'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/quiz')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		foreach ($this->request->post['quiz_description'] as $language_id => $value) {
			if ((utf8_strlen($value['name']) < 1) || (utf8_strlen($value['name']) > 255)) {
				$this->error['name'][$language_id] = $this->language->get('error_name');
			}
		}

		if (isset($this->request->post['seo']) && $this->request->post['seo'] == 0) {
			$this->error['seo'] = "Từ khóa SEO đã tồn tại";
		}

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		if (!$this->error) {
			return true;
			} else {
			return false;
		}
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/quiz')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true;
			} else {
			return false;
		}
	}

	protected function validateCopy() {
		if (!$this->user->hasPermission('modify', 'catalog/quiz')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true;
			} else {
			return false;
		}
	}

	public function checkKeyword() {
		$json = array();

		if (isset($this->request->post['quiz_id'])) {
			$quiz_id = $this->request->post['quiz_id'];
			} else {
			$quiz_id = 0;
		}
		if (isset($this->request->post['keyword'])) {
			$keyword = $this->request->post['keyword'];
			} else {
			$keyword = '';
		}

		$data = array(
		'keyword' => $keyword,
		'quiz_id' => $quiz_id
		);

		if (strlen($keyword) > 0) {
			$this->load->model('catalog/quiz');
			$results = $this->model_catalog_quiz->checkKeywords($data);
			if ($results == 0) {
				$json['success'] = 'Thành công:Từ khóa mà bạn đã nhập vào được kiểm tra là duy nhất.';
				} else {
				$json['error'] = 'Cảnh báo: Từ khóa mà bạn đã nhập vào đã được sử dụng ở những nơi khác trên trang.';
			}
			$this->response->setOutput(json_encode($json));
		}
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name']) || isset($this->request->get['filter_model']) || isset($this->request->get['filter_group_id'])) {
			$this->load->model('catalog/quiz');

			if (isset($this->request->get['filter_name'])) {
				$filter_name = $this->request->get['filter_name'];
				} else {
				$filter_name = '';
			}

			if (isset($this->request->get['filter_model'])) {
				$filter_model = $this->request->get['filter_model'];
				} else {
				$filter_model = '';
			}

			if (isset($this->request->get['limit'])) {
				$limit = $this->request->get['limit'];
				} else {
				$limit = 20;
			}

			$data = array(
			'filter_name' => $filter_name,
			'filter_model' => $filter_model,
			'start' => 0,
			'limit' => $limit
			);

			$results = $this->model_catalog_quiz->getQuizs($data);

			foreach ($results as $result) {
				$json[] = array(
				'quiz_id' => $result['quiz_id'],
				'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
				'model' => $result['model'],
				'price' => $result['price']
				);
			}
		}

		$this->response->setOutput(json_encode($json));
	}

}

?>
