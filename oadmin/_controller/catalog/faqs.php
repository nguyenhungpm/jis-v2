<?php
class ControllerCatalogFaqs extends Controller {
	private $name = 'faqs';

	private $error = array();

	public function __construct($registry) {
		parent::__construct($registry);
		$this->registry = $registry;
		$this->data = array_merge($this->data, $this->load->language('catalog/faqs'));
		$this->data['token'] = $this->session->data['token'];
	}

	private function setBreadcrumbs() {
  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link("catalog/{$this->name}", 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
	}

	public function editQuestion() {
		$this->load->model("catalog/faqs");
		$this->model_catalog_faqs->editQuestion($this->request->post['pq']);
	}

	public function deleteQuestion() {
		$this->load->model("catalog/{$this->name}");
		$this->model_catalog_faqs->deleteQuestion($this->request->get['faq_id']);
	}

	public function index() {
		$this->load->language("catalog/{$this->name}");
		$this->load->model("catalog/{$this->name}");


 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

        if (isset($this->session->data['success'])) {
                $this->data['success'] = $this->session->data['success'];
                $this->session->data['success'] = '';
        } else {
                $this->data['success'] = '';
        }

		$this->setBreadcrumbs();

        $this->data['action'] = $this->url->link("catalog/{$this->name}", 'token=' . $this->session->data['token'], 'SSL');
		$this->data['cancel'] = $this->url->link('catalog/faqs', 'token=' . $this->session->data['token'], 'SSL');

		$page = (isset($this->request->get['page'])) ? $this->request->get['page'] : 1;
		$total_questions = $this->model_catalog_faqs->getQuestionCount();

		$sort = array(
			'offset' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);
		$this->data['faqs'] = array();
		$this->data['faqs'] = $this->model_catalog_faqs->getQuestions($sort);
		$pagination = new Pagination();
		$pagination->total = $total_questions;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/faqs', 'token=' . $this->session->data['token'] . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['token'] = $this->session->data['token'];


		$this->document->setTitle($this->language->get('heading_title'));


		$this->template = "catalog/faqs.tpl";
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
	}

	private function validate() {
		if (!$this->user->hasPermission('modify', "catalog/{$this->name}")) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}
	}


}
?>
