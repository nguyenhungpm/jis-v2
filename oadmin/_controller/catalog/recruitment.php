<?php
class ControllerCatalogRecruitment extends Controller
{
    private $error = array();

    public function index()
    {
        $this->language->load('catalog/recruitment');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/recruitment');

        $this->getList();
    }

    public function insert()
    {
        $this->language->load('catalog/recruitment');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/recruitment');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->response->ClearCache();
            $this->model_catalog_recruitment->addRecruitment($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->redirect($this->url->link('catalog/recruitment', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getForm();
    }

    public function update()
    {
        $this->language->load('catalog/recruitment');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/recruitment');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->response->ClearCache();
            $this->model_catalog_recruitment->editRecruitment($this->request->get['recruitment_id'], $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->redirect($this->url->link('catalog/recruitment', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getForm();
    }

    public function delete()
    {
        $this->language->load('catalog/recruitment');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/recruitment');

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $recruitment_id) {
                $this->model_catalog_recruitment->deleteRecruitment($recruitment_id);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->redirect($this->url->link('catalog/recruitment', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getList();
    }

    protected function getList()
    {
        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'i.sort_order';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'DESC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false,
        );

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/recruitment', 'token=' . $this->session->data['token'] . $url, 'SSL'),
            'separator' => ' :: ',
        );

        $this->data['insert'] = $this->url->link('catalog/recruitment/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
        $this->data['delete'] = $this->url->link('catalog/recruitment/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

        $this->data['recruitments'] = array();

        $data = array(
            'sort' => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_admin_limit'),
            'limit' => $this->config->get('config_admin_limit'),
        );

        $recruitment_total = $this->model_catalog_recruitment->getTotalRecruitments();

        $results = $this->model_catalog_recruitment->getRecruitments($data);

        foreach ($results as $result) {
            $action = array();

            $action[] = array(
                'text' => $this->language->get('text_edit'),
                'href' => $this->url->link('catalog/recruitment/update', 'token=' . $this->session->data['token'] . '&recruitment_id=' . $result['recruitment_id'] . $url, 'SSL'),
            );
            $this->load->model('catalog/reccat');
            $name = $this->model_catalog_reccat->getDetail($result['reccat_id']);
            if ($name) {
                $name = $name[2]['name'];
            } else {
                $name = '';
            }

            $this->data['recruitments'][] = array(
                'recruitment_id' => $result['recruitment_id'],
                'title' => $result['title'],
                'sort_order' => $result['sort_order'],
                'name' => $name,
                'position' => $result['position'],
                'date_added' => $result['date_added'] == '0000-00-00' ? 'Chưa đặt ngày' : date('d-m-Y', strtotime($result['date_added'])),
                'date_end' => $result['date_end'] == '0000-00-00' ? 'Không có hạn tuyển' : date('d-m-Y', strtotime($result['date_end'])),
                'sort_order' => $result['sort_order'],
                'selected' => isset($this->request->post['selected']) && in_array($result['recruitment_id'], $this->request->post['selected']),
                'action' => $action,
            );
        }

        $this->data['heading_title'] = $this->language->get('heading_title');
        $this->data['text_list'] = $this->language->get('text_list');
        $this->data['text_no_results'] = $this->language->get('text_no_results');

        $this->data['column_title'] = $this->language->get('column_title');
        $this->data['column_sort_order'] = $this->language->get('column_sort_order');
        $this->data['column_action'] = $this->language->get('column_action');

        $this->data['button_insert'] = $this->language->get('button_insert');
        $this->data['button_delete'] = $this->language->get('button_delete');

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }

        $url = '';

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $this->data['sort_title'] = $this->url->link('catalog/recruitment', 'token=' . $this->session->data['token'] . '&sort=id.title' . $url, 'SSL');
        $this->data['sort_sort_order'] = $this->url->link('catalog/recruitment', 'token=' . $this->session->data['token'] . '&sort=i.sort_order' . $url, 'SSL');

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        $pagination = new Pagination();
        $pagination->total = $recruitment_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_admin_limit');
        $pagination->text = $this->language->get('text_pagination');
        $pagination->url = $this->url->link('catalog/recruitment', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

        $this->data['pagination'] = $pagination->render();

        $this->data['sort'] = $sort;
        $this->data['order'] = $order;

        $this->template = 'catalog/recruitment_list.tpl';
        $this->children = array(
            'common/header',
            'common/footer',
        );

        $this->response->setOutput($this->render());
    }

    protected function getForm()
    {
        $this->data['heading_title'] = $this->language->get('heading_title');
        $this->data['text_default'] = $this->language->get('text_default');
        $this->data['text_page_details'] = $this->language->get('text_page_details');
        $this->data['text_enabled'] = $this->language->get('text_enabled');
        $this->data['text_disabled'] = $this->language->get('text_disabled');

        $this->data['entry_title'] = $this->language->get('entry_title');
        $this->data['entry_meta_description'] = $this->language->get('entry_meta_description');
        $this->data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
        $this->data['entry_description'] = $this->language->get('entry_description');
        $this->data['entry_keyword'] = $this->language->get('entry_keyword');
        $this->data['text_remain'] = $this->language->get('text_remain');
        $this->data['text_characters'] = $this->language->get('text_characters');
        $this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
        $this->data['entry_status'] = $this->language->get('entry_status');

        $this->data['button_save'] = $this->language->get('button_save');
        $this->data['button_cancel'] = $this->language->get('button_cancel');

        $this->data['button_close'] = $this->language->get('button_close');
        $this->data['tab_data'] = $this->language->get('tab_data');
        $this->data['button_delete'] = $this->language->get('button_delete');

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        if (isset($this->error['title'])) {
            $this->data['error_title'] = $this->error['title'];
        } else {
            $this->data['error_title'] = array();
        }

        if (isset($this->error['description'])) {
            $this->data['error_description'] = $this->error['description'];
        } else {
            $this->data['error_description'] = array();
        }

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false,
        );

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/recruitment', 'token=' . $this->session->data['token'] . $url, 'SSL'),
            'separator' => ' :: ',
        );

        if (!isset($this->request->get['recruitment_id'])) {
            $this->data['action'] = $this->url->link('catalog/recruitment/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
        } else {
            $this->data['recruitment_id'] = $this->request->get['recruitment_id'];
            $this->data['delete'] = $this->url->link('catalog/recruitment/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
            $this->data['action'] = $this->url->link('catalog/recruitment/update', 'token=' . $this->session->data['token'] . '&recruitment_id=' . $this->request->get['recruitment_id'] . $url, 'SSL');
        }

        $this->data['cancel'] = $this->url->link('catalog/recruitment', 'token=' . $this->session->data['token'] . $url, 'SSL');

        if (isset($this->request->get['recruitment_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $recruitment_info = $this->model_catalog_recruitment->getRecruitment($this->request->get['recruitment_id']);
        }
        // print_r($recruitment_info);

        $this->data['token'] = $this->session->data['token'];

        $this->load->model('localisation/language');

        $this->data['languages'] = $this->model_localisation_language->getLanguages();

        if (isset($this->request->post['recruitment_description'])) {
            $this->data['recruitment_description'] = $this->request->post['recruitment_description'];
        } elseif (isset($this->request->get['recruitment_id'])) {
            $this->data['recruitment_description'] = $this->model_catalog_recruitment->getRecruitmentDescriptions($this->request->get['recruitment_id']);
        } else {
            $this->data['recruitment_description'] = array();
        }

        $this->load->model('catalog/reccat');
        $this->data['reccats'] = $this->model_catalog_reccat->getNamesCats();

        if (isset($this->request->post['date_end'])) {
            $this->data['date_end'] = $this->request->post['date_end'];
        } elseif (isset($recruitment_info)) {
            $this->data['date_end'] = date('Y-m-d', strtotime($recruitment_info['date_end']));
        } else {
            $this->data['date_end'] = date('Y-m-d', time() + 86400);
        }

        if (isset($this->request->post['date_added'])) {
            $this->data['date_added'] = $this->request->post['date_added'];
        } elseif (isset($recruitment_info)) {
            $this->data['date_added'] = date('Y-m-d', strtotime($recruitment_info['date_added']));
        } else {
            $this->data['date_added'] = date('Y-m-d', time() - 86400);
        }

        if (isset($this->request->post['keyword'])) {
            $this->data['keyword'] = $this->request->post['keyword'];
        } elseif (!empty($recruitment_info)) {
            $this->data['keyword'] = $recruitment_info['keyword'];
        } else {
            $this->data['keyword'] = '';
        }

        if (isset($this->request->post['robots'])) {
            $this->data['robots'] = $this->request->post['robots'];
        } elseif (!empty($recruitment_info)) {
            $this->data['robots'] = $recruitment_info['robots'];
        } else {
            $this->data['robots'] = '';
        }

        if (isset($this->request->post['hot'])) {
            $this->data['hot'] = $this->request->post['hot'];
        } elseif (!empty($recruitment_info)) {
            $this->data['hot'] = $recruitment_info['hot'];
        } else {
            $this->data['hot'] = 0;
        }

        if (isset($this->request->post['reccat_id'])) {
            $this->data['reccat_id'] = $this->request->post['reccat_id'];
        } elseif (!empty($recruitment_info)) {
            $this->data['reccat_id'] = $recruitment_info['reccat_id'];
        } else {
            $this->data['reccat_id'] = 0;
        }

        if (isset($this->request->post['jobtype'])) {
            $this->data['jobtype'] = $this->request->post['jobtype'];
        } elseif (!empty($recruitment_info)) {
            $this->data['jobtype'] = $recruitment_info['jobtype'];
        } else {
            $this->data['jobtype'] = 0;
        }

		// echo $this->data['jobtype'];

        if (isset($this->request->post['internal'])) {
            $this->data['internal'] = $this->request->post['internal'];
        } elseif (!empty($recruitment_info)) {
            $this->data['internal'] = $recruitment_info['internal'];
        } else {
            $this->data['internal'] = 0;
        }

        if (isset($this->request->post['status'])) {
            $this->data['status'] = $this->request->post['status'];
        } elseif (!empty($recruitment_info)) {
            $this->data['status'] = $recruitment_info['status'];
        } else {
            $this->data['status'] = 1;
        }

        if (isset($this->request->post['sort_order'])) {
            $this->data['sort_order'] = $this->request->post['sort_order'];
        } elseif (!empty($recruitment_info)) {
            $this->data['sort_order'] = $recruitment_info['sort_order'];
        } else {
            $this->data['sort_order'] = '';
        }

        $this->template = 'catalog/recruitment_form.tpl';
        $this->children = array(
            'common/header',
            'common/footer',
        );

        $this->response->setOutput($this->render());
    }

    protected function validateForm()
    {
        if (!$this->user->hasPermission('modify', 'catalog/recruitment')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        /*
        foreach ($this->request->post['recruitment_description'] as $language_id => $value) {
        if ((utf8_strlen($value['title']) < 3) || (utf8_strlen($value['title']) > 64)) {
        $this->error['title'][$language_id] = $this->language->get('error_title');
        }

        // if (utf8_strlen($value['requirement']) < 3) {
        // $this->error['description'][$language_id] = $this->language->get('error_description');
        // }
        }
         */

        if ($this->error && !isset($this->error['warning'])) {
            $this->error['warning'] = $this->language->get('error_warning');
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

    protected function validateDelete()
    {
        if (!$this->user->hasPermission('modify', 'catalog/recruitment')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }
}
