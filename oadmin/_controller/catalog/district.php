<?php
class ControllerCatalogDistrict extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/district');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/district');

		$this->getList();
	}

	public function insert() {
		$this->load->language('catalog/district');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/district');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_district->addDistrict($this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			$this->redirect($this->url->link('catalog/district', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->getForm();
	}

	public function update() {
		$this->load->language('catalog/district');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/district');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_district->editDistrict($this->request->get['district_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->redirect($this->url->link('catalog/district', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/district');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/district');
		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $district_id) {
				$this->model_catalog_district->deleteDistrict($district_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');
			$this->redirect($this->url->link('catalog/district', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->getList();
	}

	private function getList() {
   		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => 'Khu vực bán hàng',
			'href'      => $this->url->link('catalog/area', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		if(isset($this->request->get['province_id'])){
			$query = $this->db->query("SELECT a.provinceid as province_id, ad.name as name FROM " . DB_PREFIX . "province a LEFT JOIN " . DB_PREFIX . "province_description ad ON (a.provinceid=ad.province_id) WHERE a.provinceid ='". $this->request->get['province_id'] ."'");
			$name_province = $query->row['name'];
			$url_province = $this->url->link('catalog/district', 'token=' . $this->session->data['token'].'&province_id='.$query->row['province_id'], 'SSL');
		}else{
			$name_province = 'Tất cả các quận / huyện';
			$url_province = $this->url->link('catalog/district', 'token=' . $this->session->data['token'], 'SSL');
		}

		$this->data['breadcrumbs'][] = array(
       		'text'      => $name_province,
			'href'      => $url_province,
      		'separator' => ' :: '
		);

   		// $this->data['breadcrumbs'][] = array(
       		// 'text'      => $this->language->get('heading_title'),
			// 'href'      => $this->url->link('catalog/district', 'token=' . $this->session->data['token'], 'SSL'),
      		// 'separator' => ' :: '
   		// );

		$this->data['districts'] = array();
		if(isset($this->request->get['province_id'])){
			$province_id = $this->request->get['province_id'];
		}else{
			$province_id = null;
		}

		$this->data['insert'] = $this->url->link('catalog/district/insert', 'token=' . $this->session->data['token'].'&province_id='.$province_id, 'SSL');
		$this->data['delete'] = $this->url->link('catalog/district/delete', 'token=' . $this->session->data['token'], 'SSL');


		$results = $this->model_catalog_district->getDistricts($province_id);

		foreach ($results as $result) {
			$action = array();

			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('catalog/district/update', 'token=' . $this->session->data['token'] . '&district_id=' . $result['district_id'], 'SSL')
			);

			$this->data['districts'][] = array(
				'district_id' => $result['district_id'],
				'name'        => $result['name'],
				'view' => $this->url->link('catalog/dealer', 'token=' . $this->session->data['token'] . '&district_id=' . $result['district_id'], 'SSL'),
				'sort_order'  => $result['sort_order'],
				'selected'    => isset($this->request->post['selected']) && in_array($result['district_id'], $this->request->post['selected']),
				'action'      => $action
			);
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_sort_order'] = $this->language->get('column_sort_order');
		$this->data['column_action'] = $this->language->get('column_action');

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->template = 'catalog/district_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer',
		);

		$this->response->setOutput($this->render());
	}

	private function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_none'] = $this->language->get('text_none');
		$this->data['text_default'] = $this->language->get('text_default');
		$this->data['text_image_manager'] = $this->language->get('text_image_manager');
		$this->data['text_browse'] = $this->language->get('text_browse');
		$this->data['text_clear'] = $this->language->get('text_clear');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
    	$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_percent'] = $this->language->get('text_percent');
		$this->data['text_amount'] = $this->language->get('text_amount');

		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$this->data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$this->data['entry_description'] = $this->language->get('entry_description');
		$this->data['entry_store'] = $this->language->get('entry_store');
		$this->data['entry_keyword'] = $this->language->get('entry_keyword');
		$this->data['entry_parent'] = $this->language->get('entry_parent');
		$this->data['entry_image'] = $this->language->get('entry_image');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_layout'] = $this->language->get('entry_layout');
		$this->data['entry_top'] = $this->language->get('entry_top');
		$this->data['entry_column'] = $this->language->get('entry_column');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

    	$this->data['tab_general'] = $this->language->get('tab_general');
    	$this->data['tab_data'] = $this->language->get('tab_data');
		$this->data['tab_design'] = $this->language->get('tab_design');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

 		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = array();
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
					'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
					'href'      => $this->url->link('catalog/district', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);

		if (!isset($this->request->get['district_id'])) {
			$this->data['action'] = $this->url->link('catalog/district/insert', 'token=' . $this->session->data['token'], 'SSL');
		} else {
			$this->data['action'] = $this->url->link('catalog/district/update', 'token=' . $this->session->data['token'] . '&district_id=' . $this->request->get['district_id'], 'SSL');
		}

		$this->data['cancel'] = $this->url->link('catalog/district', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['token'] = $this->session->data['token'];

		if (isset($this->request->get['district_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
      		$district_info = $this->model_catalog_district->getDistrict($this->request->get['district_id']);
    	}

		//get provinces
		$this->load->model('catalog/province');
		$this->data['provinces'] = $this->model_catalog_province->getProvinces(null);

		if (isset($this->request->post['parent_id'])) {
			$this->data['parent_id'] = $this->request->post['parent_id'];
		} elseif (isset($district_info)) {
			$this->data['parent_id'] = $district_info['parent_id'];
		} else {
			$this->data['parent_id'] = 0;
		}

		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
		} elseif (isset($district_info)) {
			$this->data['name'] = $district_info['name'];
		} else {
			$this->data['name'] = '';
		}

		if (isset($this->request->post['province_id'])) {
			$this->data['province'] = $this->request->post['province_id'];
		} elseif (isset($this->request->get['province_id'])) {
			$this->data['province'] = $this->request->get['province_id'];
		} elseif (isset($district_info)) {
			$this->data['province'] = $district_info['province_id'];
		} else {
			$this->data['province'] = '';
		}

		if (isset($this->request->post['sort_order'])) {
			$this->data['sort_order'] = $this->request->post['sort_order'];
		} elseif (isset($district_info)) {
			$this->data['sort_order'] = $district_info['sort_order'];
		} else {
			$this->data['sort_order'] = 0;
		}

		if (isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
		} elseif (isset($district_info)) {
			$this->data['status'] = $district_info['status'];
		} else {
			$this->data['status'] = 1;
		}

		$this->load->model('design/layout');
		$this->data['layouts'] = $this->model_design_layout->getLayouts();

		$this->template = 'catalog/district_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer',
		);

		$this->response->setOutput($this->render());
	}

	public function province() {
		$json = array();
		$this->load->model('catalog/district');
		$results = $this->model_catalog_district->getDistricts($this->request->get['province_id']);
		if ($results) {
			$this->load->model('catalog/district');
			foreach ($results as $result) {
				$json[] = array(
					'district_id'       => $result['district_id'],
					'name'              => $result['name']
				);
			}
		}

		$this->response->setOutput(json_encode($json));
	}
	private function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/district')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	private function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/district')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
}
?>
