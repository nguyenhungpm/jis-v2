<?php
class ControllerCatalogDocument extends Controller {
	private $error = array();

	public function index() {
		$this->language->load('catalog/document');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/document');

		$this->getList();
	}

	public function insert() {
		$this->language->load('catalog/document');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/document');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->response->ClearCache();
			$this->model_catalog_document->adddocument($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/document', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function update() {
		$this->language->load('catalog/document');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/document');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->response->ClearCache();
			$this->model_catalog_document->editdocument($this->request->get['document_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/document', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function delete() {
		$this->language->load('catalog/document');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/document');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $document_id) {
				$this->model_catalog_document->deletedocument($document_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/document', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'i.sort_order';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/document', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->data['insert'] = $this->url->link('catalog/document/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('catalog/document/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$this->data['documents'] = array();

		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		$document_total = $this->model_catalog_document->getTotaldocuments();

		$results = $this->model_catalog_document->getdocuments($data);

		foreach ($results as $result) {
			$action = array();

			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('catalog/document/update', 'token=' . $this->session->data['token'] . '&document_id=' . $result['document_id'] . $url, 'SSL')
			);
			$this->load->model('catalog/doccat');
			$name = $this->model_catalog_doccat->getDetail($result['doccat_id']);
			if($name){
				$name = $name[2]['name'];
			}else{
				$name = '';
			}

			$this->data['documents'][] = array(
				'document_id' => $result['document_id'],
				'title'          => $result['title'],
				'sort_order'     => $result['sort_order'],
				'name'			     => $name,
				'download'       => $result['download'],
				'date_added'     => $result['date_added'] == '0000-00-00' ? 'Chưa đặt ngày' : date('d-m-Y', strtotime($result['date_added'])),
				'sort_order'     => $result['sort_order'],
				'selected'       => isset($this->request->post['selected']) && in_array($result['document_id'], $this->request->post['selected']),
				'action'         => $action
			);
		}

		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_list'] = $this->language->get('text_list');
		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['column_title'] = $this->language->get('column_title');
		$this->data['column_sort_order'] = $this->language->get('column_sort_order');
		$this->data['column_action'] = $this->language->get('column_action');

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['sort_title'] = $this->url->link('catalog/document', 'token=' . $this->session->data['token'] . '&sort=id.title' . $url, 'SSL');
		$this->data['sort_sort_order'] = $this->url->link('catalog/document', 'token=' . $this->session->data['token'] . '&sort=i.sort_order' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $document_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/document', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->template = 'catalog/document_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_default'] = $this->language->get('text_default');
		$this->data['text_page_details'] = $this->language->get('text_page_details');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');

		$this->data['entry_title'] = $this->language->get('entry_title');
		$this->data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$this->data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$this->data['entry_description'] = $this->language->get('entry_description');
		$this->data['entry_keyword'] = $this->language->get('entry_keyword');
		$this->data['text_remain'] = $this->language->get('text_remain');
		$this->data['text_characters'] = $this->language->get('text_characters');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_status'] = $this->language->get('entry_status');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		$this->data['button_close'] = $this->language->get('button_close');
		$this->data['tab_data'] = $this->language->get('tab_data');
		$this->data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['title'])) {
			$this->data['error_title'] = $this->error['title'];
		} else {
			$this->data['error_title'] = array();
		}

		if (isset($this->error['description'])) {
			$this->data['error_description'] = $this->error['description'];
		} else {
			$this->data['error_description'] = array();
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/document', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		if (!isset($this->request->get['document_id'])) {
			$this->data['action'] = $this->url->link('catalog/document/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['document_id'] = $this->request->get['document_id'];
            $this->data['delete'] = $this->url->link('catalog/document/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
			$this->data['action'] = $this->url->link('catalog/document/update', 'token=' . $this->session->data['token'] . '&document_id=' . $this->request->get['document_id'] . $url, 'SSL');
		}

		$this->data['cancel'] = $this->url->link('catalog/document', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->get['document_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$document_info = $this->model_catalog_document->getdocument($this->request->get['document_id']);
		}

		$this->data['token'] = $this->session->data['token'];


		$this->load->model('localisation/language');

		$this->data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->post['document_description'])) {
			$this->data['document_description'] = $this->request->post['document_description'];
		} elseif (isset($this->request->get['document_id'])) {
			$this->data['document_description'] = $this->model_catalog_document->getdocumentDescriptions($this->request->get['document_id']);
		} else {
			$this->data['document_description'] = array();
		}
		$this->load->model('catalog/doccat');
		$this->document->addStyle('view/stylesheet/lte/select2.min.css');
		$this->data['doccats'] = $this->model_catalog_doccat->getNamesCats();

    if (isset($this->request->post['date_added'])) {
          $this->data['date_added'] = $this->request->post['date_added'];
    } elseif (isset($document_info)) {
        $this->data['date_added'] = date('Y-m-d', strtotime($document_info['date_added']));
    } else {
        $this->data['date_added'] = date('Y-m-d', time());
    }

		if (isset($this->request->post['image'])) {
				$this->data['image'] = $this->request->post['image'];
		} elseif (isset($document_info)) {
				$this->data['image'] = $document_info['image'];
		} else {
				$this->data['image'] = '';
		}

		$this->load->model('tool/image');

		if (isset($document_info) && $document_info['image'] && file_exists(DIR_IMAGE . $document_info['image'])) {
				$this->data['thumb'] = $this->model_tool_image->resize($document_info['image'], 160, 60);
		} else {
				$this->data['thumb'] = $this->model_tool_image->resize('no_image.jpg', 160, 60);
		}
		$this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 160, 60);

		if (isset($this->request->post['keyword'])) {
			$this->data['keyword'] = $this->request->post['keyword'];
		} elseif (!empty($document_info)) {
			$this->data['keyword'] = $document_info['keyword'];
		} else {
			$this->data['keyword'] = '';
		}

		if (isset($this->request->post['robots'])) {
			$this->data['robots'] = $this->request->post['robots'];
		} elseif (!empty($document_info)) {
			$this->data['robots'] = $document_info['robots'];
		} else {
			$this->data['robots'] = '';
		}

		if (isset($this->request->post['filename'])) {
			$this->data['filename'] = $this->request->post['filename'];
		} elseif (!empty($document_info)) {
			$this->data['filename'] = $document_info['filename'];
		} else {
			$this->data['filename'] = '';
		}

		if (isset($this->request->post['mask'])) {
			$this->data['mask'] = $this->request->post['mask'];
		} elseif (!empty($document_info)) {
			$this->data['mask'] = $document_info['mask'];
		} else {
			$this->data['mask'] = '';
		}

		if (isset($this->request->post['type'])) {
			$this->data['type'] = $this->request->post['type'];
		} elseif (!empty($document_info)) {
			$this->data['type'] = $document_info['type'];
		} else {
			$this->data['type'] = 1;
		}

		if (isset($this->request->post['linkdown'])) {
			$this->data['linkdown'] = $this->request->post['linkdown'];
		} elseif (!empty($document_info)) {
			$this->data['linkdown'] = $document_info['linkdown'];
		} else {
			$this->data['linkdown'] = '';
		}

		if (isset($this->request->post['doccat_id'])) {
			$this->data['doccat_id'] = $this->request->post['doccat_id'];
		} elseif (!empty($document_info)) {
			$this->data['doccat_id'] = $document_info['doccat_id'];
		} else {
			$this->data['doccat_id'] = 0;
		}

		if (isset($this->request->post['notify'])) {
			$this->data['notify'] = $this->request->post['notify'];
		} elseif (!empty($document_info)) {
			$this->data['notify'] = $document_info['notify'];
		} else {
			$this->data['notify'] = 1;
		}

		if (isset($this->request->post['invisible'])) {
			$this->data['invisible'] = $this->request->post['invisible'];
		} elseif (!empty($document_info)) {
			$this->data['invisible'] = $document_info['invisible'];
		} else {
			$this->data['invisible'] = 1;
		}

		if (!empty($document_info)) {
			$this->data['download'] = $document_info['download'];
		} else {
			$this->data['download'] = 0;
		}

		if (isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
		} elseif (!empty($document_info)) {
			$this->data['status'] = $document_info['status'];
		} else {
			$this->data['status'] = 1;
		}

		if (isset($this->request->post['sort_order'])) {
			$this->data['sort_order'] = $this->request->post['sort_order'];
		} elseif (!empty($document_info)) {
			$this->data['sort_order'] = $document_info['sort_order'];
		} else {
			$this->data['sort_order'] = '';
		}

		$this->template = 'catalog/document_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}


	public function upload() {
		$this->language->load('catalog/product');

		$json = array();

		if (!$this->user->hasPermission('modify', 'catalog/document')) {
			$json['error'] = $this->language->get('error_permission');
		}

		if (!isset($json['error'])) {
			if (!empty($this->request->files['file']['name'])) {
				$filename = basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));

				if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 128)) {
					$json['error'] = $this->language->get('error_filename');
				}
				/*
				// Allowed file extension types
				$allowed = array();

				$filetypes = explode("\n", $this->config->get('config_file_extension_allowed'));

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}

				if (!in_array(substr(strrchr($filename, '.'), 1), $allowed)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Allowed file mime types
				$allowed = array();

				$filetypes = explode("\n", $this->config->get('config_file_mime_allowed'));

				foreach ($filetypes as $filetype) {
					$allowed[] = trim($filetype);
				}

				if (!in_array($this->request->files['file']['type'], $allowed)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Check to see if any PHP files are trying to be uploaded

				$content = file_get_contents($this->request->files['file']['tmp_name']);

				if (preg_match('/\<\?php/i', $content)) {
					$json['error'] = $this->language->get('error_filetype');
				}
				*/
				if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
					$json['error'] = $this->language->get('error_upload_' . $this->request->files['file']['error']);
				}
			} else {
				$json['error'] = $this->language->get('error_upload');
			}
		}

		if (!isset($json['error'])) {
			if (is_uploaded_file($this->request->files['file']['tmp_name']) && file_exists($this->request->files['file']['tmp_name'])) {
				//$now = new DateTime();
				$ran = date('Hs',time());
				$ext = $ran . '.'. pathinfo($this->request->files['file']['name'], PATHINFO_EXTENSION);

				$name = preg_replace('/\\.[^.\\s]{3,4}$/', '', $filename);
				$json['mask'] = $filename;

				$shortpath = date("Y").'/'.date("m").'/'.date("d");
				$fullpath = DIR_DOWNLOAD .$shortpath;
				//If the directory doesn't already exists.
				if(!is_dir($fullpath)){
					//Create our directory.
					mkdir($fullpath, 0755, true);
				}
				$json['filename'] = $shortpath .'/'.$name.'_'.$ext;
				//$fullpath =
				move_uploaded_file($this->request->files['file']['tmp_name'], $fullpath .'/'.$name.'_'.$ext);
			}

			$json['success'] = $this->language->get('text_upload');
		}

		$this->response->setOutput(json_encode($json));
	}

	public function remove() {
		$json = array();
		$filename = rawurldecode($this->request->get['filename']);
		$document_id = $this->request->get['document_id'];

		if($document_id>0){
			$this->db->query("UPDATE " . DB_PREFIX . "document SET mask= '', filename= '' WHERE document_id = '". $document_id ."'");
		}

		if(!empty($filename)){
			if (file_exists(DIR_DOWNLOAD .  $filename)) {
				unlink(DIR_DOWNLOAD . $filename);
			}
		}
		$json['success'] = $this->language->get('text_upload');

	}
	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/document')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		/*
		foreach ($this->request->post['document_description'] as $language_id => $value) {
			if ((utf8_strlen($value['title']) < 3) || (utf8_strlen($value['title']) > 64)) {
				$this->error['title'][$language_id] = $this->language->get('error_title');
			}

			// if (utf8_strlen($value['requirement']) < 3) {
				// $this->error['description'][$language_id] = $this->language->get('error_description');
			// }
		}
		*/
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/document')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
}
?>
