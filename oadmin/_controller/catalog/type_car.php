<?php
class ControllerCatalogTypeCar extends Controller {
	private $error = array();
 
	public function index() {
		$this->load->language('catalog/type_car');
		 
		$this->document->setTitle($this->language->get('heading_title'));
 		
		$this->load->model('catalog/type_car');
		
		$this->getList();
	}

	public function insert() {
		$json = array();
		$this->load->language('catalog/type_car');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('catalog/type_car');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_type_car->addTypeCar($this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			
			// $this->redirect($this->url->link('catalog/type_car', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}

		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
	}
	
	public function edit(){
        $json = array();
		$this->load->language('catalog/type_car');
		$this->load->model('catalog/type_car');

		$v = $this->model_catalog_type_car->editTypeCar($this->request->get['type_car_id'], $this->request->post);
		if($v){
			$this->session->data['success'] = $this->language->get('text_success');
		}

		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
	}
	
	public function delete() { 
		$this->load->language('catalog/type_car');

		$this->document->setTitle($this->language->get('heading_title'));		
		
		$this->load->model('catalog/type_car');
		
		if (isset($this->request->post['selected'])) {
      		foreach ($this->request->post['selected'] as $id) {
				$this->model_catalog_type_car->deleteTypeCar($id);	
			}
						
			$this->session->data['success'] = $this->language->get('text_success');
			
			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			
			$this->redirect($this->url->link('catalog/type_car', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}

		$this->getList();
	}

	private function getList() {
		$this->load->model('localisation/language');
		$this->data['languages'] = $this->model_localisation_language->getLanguages();
		$this->load->model('tool/image');
	
  		$this->data['breadcrumbs'] = array();

  		$this->data['breadcrumbs'][] = array(
       		'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
       		'text'      => $this->language->get('text_home'),
      		'separator' => FALSE
   		);

  		$this->data['breadcrumbs'][] = array(
       		'href'      => $this->url->link('catalog/type_car', 'token=' . $this->session->data['token'], 'SSL'),
       		'text'      => $this->language->get('heading_title'),
      		'separator' => ' :: '
   		);
							
		
		$this->data['insert'] = $this->url->link('catalog/type_car/insert', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['delete'] = $this->url->link('catalog/type_car/delete', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['export'] = $this->url->link('catalog/type_car/export', 'token=' . $this->session->data['token'], 'SSL');	
		
		$this->data['token'] = $this->session->data['token'];
	
		$this->data['text_export'] = $this->language->get('text_export');
		$url = '';
		if(isset($this->request->get['page'])) {
	            $page = $this->request->get['page'];
	            $this->data['page'] = $this->request->get['page'];
				$url .= '&page='. $this->request->get['page'];
		}else{
		        $page = 1;
		}
		$this->load->model('tool/image');
		$this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
	
		$this->data['type_cars'] = array();
		
		$data = array(
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);
		
		$type_car_total = $this->model_catalog_type_car->getTotalTypeCars($data);
		
		$results = $this->model_catalog_type_car->getTypeCars($data);

		foreach ($results as $result) {
			if ($result['image'] && file_exists(DIR_IMAGE . $result['image'])) {
                $image = $this->model_tool_image->resize($result['image'], 40, 40);
            } else {
                $image = $this->model_tool_image->resize('no_image.jpg', 40, 40);
            }
			$detail = $this->model_catalog_type_car->getDetail($result['type_car_id']);
			$cer = array();
			$this->data['type_cars'][] = array(
				'type_car_id' 		=> $result['type_car_id'],
				'cost_tienchuyen' 	=> $result['cost_tienchuyen'],
				'cost_tienchuyen_ov' 	=> $result['cost_tienchuyen_ov'],
				'cost_tinh' 	=> $result['cost_tinh'],
				'cost_tinh_ov' 	=> $result['cost_tinh_ov'],
				'seat' 	=> $result['seat'],
				'luggage' 	=> $result['luggage'],
				'origin_image' 	=> $result['image'],
				'image' 	=> $image,
				'cer' 	=> join(", ", $cer),
				'detail' 	=> $detail,
				'sort_order' 	=> $result['sort_order'],
				'link' 	=> $result['link'],
				'status' 	=> $result['status'],
				'edit' => $this->url->link('catalog/type_car', 'token=' . $this->session->data['token'] . '&type_car_id=' . $result['type_car_id'] . $url, 'SSL')
			);
		}	
	
		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_type_car'] = $this->language->get('column_type_car');
		
		$this->data['column_action'] = $this->language->get('column_action');

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
 
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		$url = "";
		
		
		$pagination = new Pagination();
		$pagination->total = $type_car_total;
		$pagination->page  = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text  = $this->language->get('text_pagination');
		$pagination->url   = $this->url->link('catalog/type_car/', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['sort_name'] = $this->url->link('catalog/type_car/', 'token=' . $this->session->data['token']);
		
		$this->template = 'catalog/type_car_list.tpl';
		$this->children = array(
			'common/header',	
			'common/footer'	
		);
		
		$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
 	}

	private function validateForm() {
		
		$this->load->model('catalog/type_car');
		
		if (!$this->user->hasPermission('modify', 'catalog/type_car')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
	 	

		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

}
?>