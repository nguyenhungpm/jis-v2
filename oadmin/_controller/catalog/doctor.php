<?php
class ControllerCatalogDoctor extends Controller {
	private $error = array();
 
	public function index() {
		$this->load->language('catalog/doctor');
		 
		$this->document->setTitle($this->language->get('heading_title'));
 		
		$this->load->model('catalog/doctor');
		
		$this->getList();
	}

	public function insert() {
		$json = array();
		$this->load->language('catalog/doctor');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('catalog/doctor');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_doctor->addDoctor($this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_add');

			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			
			// $this->redirect($this->url->link('catalog/doctor', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}

		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
	}
	
	public function edit(){
        $json = array();
		$this->load->language('catalog/doctor');
		$this->load->model('catalog/doctor');

		$v = $this->model_catalog_doctor->editDoctor($this->request->get['doctor_id'], $this->request->post);
		if($v){
			$this->session->data['success'] = $this->language->get('text_edit');
		}

		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
	}
	
	public function delete() { 
		$this->load->language('catalog/doctor');

		$this->document->setTitle($this->language->get('heading_title'));		
		
		$this->load->model('catalog/doctor');
		
		if (isset($this->request->post['selected'])) {
      		foreach ($this->request->post['selected'] as $id) {
				$this->model_catalog_doctor->deleteDoctor($id);	
			}
						
			$this->session->data['success'] = $this->language->get('text_success');
			
			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			
			$this->redirect($this->url->link('catalog/doctor', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}

		$this->getList();
	}

	private function getList() {
		$this->load->model('localisation/language');
		$this->data['languages'] = $this->model_localisation_language->getLanguages();
		$this->load->model('tool/image');
	
  		$this->data['breadcrumbs'] = array();

  		$this->data['breadcrumbs'][] = array(
       		'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
       		'text'      => $this->language->get('text_home'),
      		'separator' => FALSE
   		);

  		$this->data['breadcrumbs'][] = array(
       		'href'      => $this->url->link('catalog/doctor', 'token=' . $this->session->data['token'], 'SSL'),
       		'text'      => $this->language->get('heading_title'),
      		'separator' => ' :: '
   		);
							
		
		$this->data['insert'] = $this->url->link('catalog/doctor/insert', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['delete'] = $this->url->link('catalog/doctor/delete', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['export'] = $this->url->link('catalog/doctor/export', 'token=' . $this->session->data['token'], 'SSL');	
		
		$this->data['token'] = $this->session->data['token'];
	
		$this->data['text_export'] = $this->language->get('text_export');
		$url = '';
		if(isset($this->request->get['page'])) {
	            $page = $this->request->get['page'];
	            $this->data['page'] = $this->request->get['page'];
				$url .= '&page='. $this->request->get['page'];
		}else{
		        $page = 1;
		}
	
		$this->data['doctors'] = array();
		
		$data = array(
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);
		
		$this->load->model('tool/image');
		$doctor_total = $this->model_catalog_doctor->getTotalDoctors($data);
		
		$results = $this->model_catalog_doctor->getDoctors($data);

		foreach ($results as $result) {
			$detail = $this->model_catalog_doctor->getDetail($result['doctor_id']);
			
			if ($result['image'] != '') {
				$thumb = $this->model_tool_image->resize($result['image'], 100, 100);
			} else {
				$thumb = $this->model_tool_image->resize('no_image.jpg', 100, 100);
			}
			
			$this->data['doctors'][] = array(
				'doctor_id' 		=> $result['doctor_id'],
				'description' 	=> $result['description'],
				'chucdanh' 	=> $result['chucdanh'],
				'image' 	=> $result['image'],
				'detail' 	=> $detail,
				'thumb' 	=> $thumb,
				'sort_order' 	=> $result['sort_order'],
				'status' 	=> $result['status'],
				'edit' => $this->url->link('catalog/doctor', 'token=' . $this->session->data['token'] . '&doctor_id=' . $result['doctor_id'] . $url, 'SSL')
			);
		}	
	
		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_doctor'] = $this->language->get('column_doctor');
		
		$this->data['column_action'] = $this->language->get('column_action');

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
 
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
        if (isset($this->request->post['image']) && file_exists(DIR_IMAGE . $this->request->post['image'])) {
            $this->data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
		} else {
            $this->data['thumb'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
        }
		
		$this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
		
		$url = "";
		
		
		$pagination = new Pagination();
		$pagination->total = $doctor_total;
		$pagination->page  = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text  = $this->language->get('text_pagination');
		$pagination->url   = $this->url->link('catalog/doctor/', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['sort_name'] = $this->url->link('catalog/doctor/', 'token=' . $this->session->data['token']);
		
		$this->template = 'catalog/doctor_list.tpl';
		$this->children = array(
			'common/header',	
			'common/footer'	
		);
		
		$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
 	}

	private function validateForm() {
		
		$this->load->model('catalog/doctor');
		
		if (!$this->user->hasPermission('modify', 'catalog/doctor')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
	 	

		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

}
?>