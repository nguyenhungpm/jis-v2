<?php
class ControllerModuleVideo extends Controller {
	private $error = array(); 
	
	public function index() {   
		$this->load->language('module/video');
		$this->data['positions'] = $this->config->get('config_position');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			 $this->response->ClearCache();
			$this->model_setting_setting->editSetting('video', $this->request->post);		
			
			$this->cache->delete('product');
			
			$this->session->data['success'] = $this->language->get('text_success');
						
			$this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}
				
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_configuration'] = $this->language->get('text_configuration');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_content_top'] = $this->language->get('text_content_top');
		$this->data['text_content_bottom'] = $this->language->get('text_content_bottom');		
		$this->data['text_column_left'] = $this->language->get('text_column_left');
		$this->data['text_column_right'] = $this->language->get('text_column_right');
		
		$this->data['entry_limit'] = $this->language->get('entry_limit');
		$this->data['entry_image'] = $this->language->get('entry_image');
		$this->data['entry_layout'] = $this->language->get('entry_layout');
		$this->data['entry_position'] = $this->language->get('entry_position');
		$this->data['entry_description'] = $this->language->get('entry_description');
		$this->data['entry_imagestatus'] = $this->language->get('entry_imagestatus');
		$this->data['entry_limitdescription'] = $this->language->get('entry_limitdescription');	
		$this->data['entry_leading'] = $this->language->get('entry_leading');
		$this->data['entry_id_cat'] = $this->language->get('entry_id_cat');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_add_module'] = $this->language->get('button_add_module');
		$this->data['button_remove'] = $this->language->get('button_remove');
		
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->error['image'])) {
			$this->data['error_image'] = $this->error['image'];
		} else {
			$this->data['error_image'] = array();
		}
		
  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/video', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('module/video', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->load->model('localisation/language');
		$this->data['languages'] = $this->model_localisation_language->getLanguages();

		$this->data['modules'] = array();
		
		if (isset($this->request->post['video_module'])) {
			$this->data['modules'] = $this->request->post['video_module'];
		} elseif ($this->config->get('video_module')) { 
			$this->data['modules'] = $this->config->get('video_module');
		}			

		if (isset($this->request->post['video_featured'])) {
			$this->data['video_featured'] = $this->request->post['video_featured'];
		} else {
			$this->data['video_featured'] = $this->config->get('video_featured');
		}		

		if (isset($this->request->post['video_hashtag'])) {
			$this->data['video_hashtag'] = $this->request->post['video_hashtag'];
		} else {
			$this->data['video_hashtag'] = $this->config->get('video_hashtag');
		}
		if (isset($this->request->post['video_title'])) {
			$this->data['video_title'] = $this->request->post['video_title'];
		} else {
			$this->data['video_title'] = $this->config->get('video_title');
		}
		
		if (isset($this->request->post['video_description'])) {
			$this->data['video_description'] = $this->request->post['video_description'];
		} else {
			$this->data['video_description'] = $this->config->get('video_description');
		}
    
		if (isset($this->request->post['video_expired'])) {
			$this->data['video_expired'] = $this->request->post['video_expired'];
		} else {
			$this->data['video_expired'] = $this->config->get('video_expired');
		}			
				
		$this->load->model('design/layout');
		
		$this->data['layouts'] = $this->model_design_layout->getLayouts();
		$this->data['layouts'][] = array('layout_id'=>0, 'name' => 'All Pages' );
		$this->template = 'module/video.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'module/video')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (isset($this->request->post['video_module'])) {
			foreach ($this->request->post['video_module'] as $key => $value) {
				if (!$value['image_width'] || !$value['image_height']) {
					$this->error['image'][$key] = $this->language->get('error_image');
				}
			}
		}		
				
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
?>