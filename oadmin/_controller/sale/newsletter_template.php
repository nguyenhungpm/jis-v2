<?php
class ControllerSaleNewsletterTemplate extends Controller {
	private $error = array();
 
	public function index() {
		$this->load->language('sale/newsletter_template');
 
		$this->document->setTitle($this->language->get('heading_title'));
 		
		$this->load->model('sale/newsletter_template');
		$this->model_sale_newsletter_template->check_db();
		
		$this->getList();
	}

	public function insert() {
		$this->load->language('sale/newsletter_template');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('sale/newsletter_template');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_sale_newsletter_template->addTemplate($this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			
			$this->redirect(HTTPS_SERVER . 'index.php?route=sale/newsletter_template&token=' . $this->session->data['token'] . $url);
		}

		$this->getForm();
	}

	public function update() {
		$this->load->language('sale/newsletter_template');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('sale/newsletter_template');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_sale_newsletter_template->editTemplate($this->request->get['template_id'], $this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success');
			
			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			
			$this->redirect(HTTPS_SERVER . 'index.php?route=sale/newsletter_template&token=' . $this->session->data['token'] . $url);
		}

		$this->getForm();
	}

	public function delete() { 
		$this->load->language('sale/newsletter_template');

		$this->document->title = $this->language->get('heading_title');
		
		$this->load->model('sale/newsletter_template');
		
		if (isset($this->request->post['selected'])) {
      		foreach ($this->request->post['selected'] as $template_id) {
				$this->model_sale_newsletter_template->deleteTemplate($template_id);	
			}
						
			$this->session->data['success'] = $this->language->get('text_success');
			
			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			
			$this->redirect(HTTPS_SERVER . 'index.php?route=sale/newsletter_template&token=' . $this->session->data['token'] . $url);
		}

		$this->getList();
	}

	private function getList() {
		
		$this->data['breadcrumbs'] = array();

  		$this->data['breadcrumbs'][] = array(
       		'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
       		'text'      => $this->language->get('text_home'),
      		'separator' => FALSE
   		);

  		$this->data['breadcrumbs'][] = array(
       		'href'      => $this->url->link('sale/newsletter_template', 'token=' . $this->session->data['token'], 'SSL'),
       		'text'      => $this->language->get('heading_title'),
      		'separator' => ' :: '
   		);
	
  		// $this->document->breadcrumbs = array();

   		// $this->document->breadcrumbs[] = array(
       		// 'href'      => HTTPS_SERVER . 'index.php?route=common/home&token=' . $this->session->data['token'],
       		// 'text'      => $this->language->get('text_home'),
      		// 'separator' => FALSE
   		// );

   		// $this->document->breadcrumbs[] = array(
       		// 'href'      => HTTPS_SERVER . 'index.php?route=sale/newsletter_template&token=' . $this->session->data['token'],
       		// 'text'      => $this->language->get('heading_title'),
      		// 'separator' => ' :: '
   		// );
							
		$this->data['insert'] = HTTPS_SERVER . 'index.php?route=sale/newsletter_template/insert&token=' . $this->session->data['token'];
		$this->data['delete'] = HTTPS_SERVER . 'index.php?route=sale/newsletter_template/delete&token=' . $this->session->data['token'];	
	
		$this->data['templates'] = array();
		
		$template_total = $this->model_sale_newsletter_template->getTotalTemplates();
		
		$results = $this->model_sale_newsletter_template->getTemplates();

		foreach ($results as $result) {
			$action = array();
			
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => HTTPS_SERVER . 'index.php?route=sale/newsletter_template/update&token=' . $this->session->data['token'] . '&template_id=' . $result['template_id']
			);		
		
			$this->data['templates'][] = array(
				'template_id' => $result['template_id'],
				'template_name' => $result['template_name'],
				'template'    => $result['template_code'] ,
				'action'      => $action
			);
		}	
	
		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_action'] = $this->language->get('column_action');

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
 
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		

		$this->data['sort_name'] = HTTPS_SERVER . 'index.php?route=sale/newsletter_template&token=' . $this->session->data['token'];
		
		
		$this->template = 'sale/newsletter_template_list.tpl';
		$this->children = array(
			'common/header',	
			'common/footer'	
		);
		
		$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
 	}

	private function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_code'] = $this->language->get('entry_code');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		$this->data['tab_general'] = $this->language->get('tab_general');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

 		if (isset($this->error['error_template_name'])) {
			$this->data['error_template_name'] = $this->error['error_template_name'];
		} else {
			$this->data['error_template_name'] = '';
		}
		
		if (isset($this->error['error_template_code'])) {
			$this->data['error_template_code'] = $this->error['error_template_code'];
		} else {
			$this->data['error_template_code'] = '';
		}

  		$this->document->breadcrumbs = array();

   		$this->document->breadcrumbs[] = array(
       		'href'      => HTTPS_SERVER . 'index.php?route=common/home&token=' . $this->session->data['token'],
       		'text'      => $this->language->get('text_home'),
      		'separator' => FALSE
   		);

   		$this->document->breadcrumbs[] = array(
       		'href'      => HTTPS_SERVER . 'index.php?route=sale/newsletter_template&token=' . $this->session->data['token'],
       		'text'      => $this->language->get('heading_title'),
      		'separator' => ' :: '
   		);
			
		if (!isset($this->request->get['template_id'])) {
			$this->data['action'] = HTTPS_SERVER . 'index.php?route=sale/newsletter_template/insert&token=' . $this->session->data['token'] ;
		} else {
			$this->data['action'] = HTTPS_SERVER . 'index.php?route=sale/newsletter_template/update&token=' . $this->session->data['token'] . '&template_id=' . $this->request->get['template_id'] ;
		}
		
		$this->data['token'] = $this->session->data['token'];
		  
    	$this->data['cancel'] = HTTPS_SERVER . 'index.php?route=sale/newsletter_template&token=' . $this->session->data['token'];

		if (isset($this->request->get['template_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$template_info = $this->model_sale_newsletter_template->getTemplate($this->request->get['template_id']);
		}

		if (isset($this->request->post['template_subject'])) {
			$this->data['template_subject'] = $this->request->post['template_subject'];
		} elseif (isset($template_info)) {
			$this->data['template_subject'] = $template_info['template_subject'];
		} else {
			$this->data['template_subject'] = '';
		}

		if (isset($this->request->post['template_name'])) {
			$this->data['template_name'] = $this->request->post['template_name'];
		} elseif (isset($template_info)) {
			$this->data['template_name'] = $template_info['template_name'];
		} else {
			$this->data['template_name'] = '';
		}
		
		if (isset($this->request->post['template_code'])) {
			$this->data['template_code'] = $this->request->post['template_code'];
		} elseif (isset($template_info)) {
			$this->data['template_code'] = $template_info['template_code'];
		} else {
			$this->data['template_code'] = '';
		}
			
		$this->template = 'sale/newsletter_template_form.tpl';
		$this->children = array(
			'common/header',	
			'common/footer'	
		);
		
		$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression')); 
	}

	private function validateForm() {
		if (!$this->user->hasPermission('modify', 'sale/newsletter_template')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ((strlen(utf8_decode($this->request->post['template_name'])) < 3) || (strlen(utf8_decode($this->request->post['template_name'])) > 64)) {
			$this->error['error_template_name'] = $this->language->get('error_name');
		}
	if ((strlen(utf8_decode($this->request->post['template_code'])) < 3)) {
			$this->error['error_template_code'] = $this->language->get('error_code');
		}
		

		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

}
?>