<?php

class ControllerSaleAnswerSubmit extends Controller {

    private $error = array();

    public function index() {
        $this->language->load('sale/answer_submit');
        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('sale/answer_submit');

        $this->getList();
    }

    public function update() {
  		$this->load->language('sale/answer_submit');

  		$this->document->setTitle($this->language->get('heading_title'));

  		$this->load->model('sale/answer_submit');
  		$this->load->model('catalog/product');

  		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
  			$this->model_sale_answer_submit->editAnswerSubmit($this->request->get['answer_submit_id'], $this->request->post);

  			$this->session->data['success'] = $this->language->get('text_success');

  			$url = '';

  			if (isset($this->request->get['page'])) {
  				$url .= '&page=' . $this->request->get['page'];
  			}

  			if (isset($this->request->get['sort'])) {
  				$url .= '&sort=' . $this->request->get['sort'];
  			}

  			if (isset($this->request->get['order'])) {
  				$url .= '&order=' . $this->request->get['order'];
  			}

  			$this->redirect(HTTPS_SERVER . 'index.php?route=sale/answer_submit&token=' . $this->session->data['token'] . $url);
  		}

  		$this->getForm();
  	}

    private function getForm() {
  		$this->data['heading_title'] = $this->language->get('heading_title');

  		$this->data['entry_name'] = $this->language->get('entry_name');
  		$this->data['entry_code'] = $this->language->get('entry_code');

  		$this->data['button_save'] = $this->language->get('button_save');
  		$this->data['button_cancel'] = $this->language->get('button_cancel');

  		$this->data['tab_general'] = $this->language->get('tab_general');

   		if (isset($this->error['warning'])) {
  			$this->data['error_warning'] = $this->error['warning'];
  		} else {
  			$this->data['error_warning'] = '';
  		}

   		if (isset($this->error['error_template_name'])) {
  			$this->data['error_template_name'] = $this->error['error_template_name'];
  		} else {
  			$this->data['error_template_name'] = '';
  		}

  		if (isset($this->error['error_template_code'])) {
  			$this->data['error_template_code'] = $this->error['error_template_code'];
  		} else {
  			$this->data['error_template_code'] = '';
  		}

    		$this->document->breadcrumbs = array();

     		$this->document->breadcrumbs[] = array(
         		'href'      => HTTPS_SERVER . 'index.php?route=common/home&token=' . $this->session->data['token'],
         		'text'      => $this->language->get('text_home'),
        		'separator' => FALSE
     		);

     		$this->document->breadcrumbs[] = array(
         		'href'      => HTTPS_SERVER . 'index.php?route=sale/answer_submit&token=' . $this->session->data['token'],
         		'text'      => $this->language->get('heading_title'),
        		'separator' => ' :: '
     		);

  		if (!isset($this->request->get['answer_submit_id'])) {
  			$this->data['action'] = HTTPS_SERVER . 'index.php?route=sale/answer_submit/insert&token=' . $this->session->data['token'] ;
  		} else {
  			$this->data['action'] = HTTPS_SERVER . 'index.php?route=sale/answer_submit/update&token=' . $this->session->data['token'] . '&answer_submit_id=' . $this->request->get['answer_submit_id'] ;
  		}

  		$this->data['token'] = $this->session->data['token'];

      $this->data['cancel'] = HTTPS_SERVER . 'index.php?route=sale/answer_submit&token=' . $this->session->data['token'];

  		if (isset($this->request->get['answer_submit_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
  			$answer_submit_info = $this->model_sale_answer_submit->getAnswerSubmit($this->request->get['answer_submit_id']);
  			$this->data['answer_submit_info'] = $this->model_sale_answer_submit->getAnswerSubmit($this->request->get['answer_submit_id']);
  		}

      $data = array(
          'filter_category_id' => 402,
          'filter_sub_category' => true,
          'sort' => 'p.sort_order',
          'order' => 'ASC',
      );
      //
      // $product_total = $this->model_catalog_product->getTotalProducts($data);
      //
      $quest = unserialize($answer_submit_info['quest']);
      $results = $this->model_catalog_product->getProducts($data);
      //
      foreach ($results as $result) {

          $questions = array();
          $i_results = $this->model_catalog_product->getProductImages($result['product_id']);
          foreach ($i_results as $question) {
              $questions[] = array(
                  'dungsai' => $question['dungsai'],
                  'question' => $question['question'],
                  'sort_order' => $question['sort_order'],
                  'product_id' => $question['product_id'],
                  'question_id' => $question['product_image_id']
              );
          }

          $this->data['products'][] = array(
              'product_id' => $result['product_id'],
              'questions' => $questions,
              'name' => $result['name'],
              'answer' => array_key_exists($result['product_id'], $quest) ? $quest[$result['product_id']] : 'Không chọn đáp án',
          );
      }
      //
      // print_r($this->data['products']);

  		$this->data['quest'] = array();
      // if(array_key_exists(2662, unserialize($answer_submit_info['quest']))){
        // echo $answer_submit_info['category_id'];
      // }


  		// if (isset($this->request->post['template_name'])) {
  		// 	$this->data['template_name'] = $this->request->post['template_name'];
  		// } elseif (isset($answer_submit_info)) {
  		// 	$this->data['template_name'] = $answer_submit_info['template_name'];
  		// } else {
  		// 	$this->data['template_name'] = '';
  		// }
      //
  		// if (isset($this->request->post['template_code'])) {
  		// 	$this->data['template_code'] = $this->request->post['template_code'];
  		// } elseif (isset($answer_submit_info)) {
  		// 	$this->data['template_code'] = $answer_submit_info['template_code'];
  		// } else {
  		// 	$this->data['template_code'] = '';
  		// }

  		$this->template = 'sale/answer_submit_form.tpl';
  		$this->children = array(
  			'common/header',
  			'common/footer'
  		);

  		$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
  	}

    protected function getList() {

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';


        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('sale/answer_submit', 'token=' . $this->session->data['token'] . $url, 'SSL'),
            'separator' => ' :: '
        );

        $this->data['answer_submits'] = array();

        $this->load->model('tool/image');
        $this->load->model('sale/answer_submit');

    		$data = array(
    			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
    			'limit' => $this->config->get('config_admin_limit')
    		);

        $this->data['answer_submits'] = $this->model_sale_answer_submit->getAnswerSubmits($data);

        // print_r($this->data['answer_submits']);
        $answer_submit_total = $this->model_sale_answer_submit->getTotalAnswerSubmits($data);
		    $this->data['delete'] = $this->url->link('sale/answer_submit/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
        $this->data['heading_title'] = $this->language->get('heading_title');
		    $this->data['delete'] = $this->url->link('sale/answer_submit/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

        $this->data['text_enabled'] = $this->language->get('text_enabled');
        $this->data['text_disabled'] = $this->language->get('text_disabled');
        $this->data['text_no_results'] = $this->language->get('text_no_results');
        $this->data['text_image_manager'] = $this->language->get('text_image_manager');
        $this->data['button_delete'] = $this->language->get('button_delete');

        $this->data['token'] = $this->session->data['token'];

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }

        $url = '';

        $pagination = new Pagination();
        $pagination->total = $answer_submit_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_admin_limit');
        $pagination->text = $this->language->get('text_pagination');
        $pagination->url = $this->url->link('sale/answer_submit', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

        $this->data['pagination'] = $pagination->render();

        $this->template = 'sale/answer_submit_list.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }

	public function updatestatus() {
		$this->load->language('sale/answer_submit');
		$this->load->model('sale/answer_submit');
		$output='';
		if(isset($this->request->get['object_id'])){
			$get_request = explode('-',$this->request->get['object_id']);
			if(count($get_request)==2){
				$column_name = $get_request[0];
				$answer_submit_id = $get_request[1];
				$result = $this->model_sale_answer_submit->getanswer_submit($answer_submit_id);
				if($result[$column_name]){
					$this->model_sale_answer_submit->updateStatus($answer_submit_id, $column_name, 0);
					} else {
					$this->model_sale_answer_submit->updateStatus($answer_submit_id, $column_name, 1);
				}
				$result = $this->model_sale_answer_submit->getanswer_submit($answer_submit_id);
				$output = $result[$column_name] ? $this->language->get('text_enabled') : $this->language->get('text_disabled');
			}
		}
		$this->response->setOutput($output);
	}

  public function delete(){
		$this->load->model('sale/answer_submit');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $answer_submit_id) {
				$this->model_sale_answer_submit->deleteanswer_submit($answer_submit_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');
			$this->redirect($this->url->link('sale/answer_submit', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->getList();
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'sale/answer_submit')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true;
			} else {
			return false;
		}
	}
}

?>
