<?php 
class ControllerSaleContact extends Controller {
	private $error = array();

	public function index() {
		$this->language->load('sale/contact');
		$this->document->setTitle($this->language->get('heading_title'));

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_default'] = $this->language->get('text_default');
		$this->data['text_newsletter'] = $this->language->get('text_newsletter');
		$this->data['text_customer_all'] = $this->language->get('text_customer_all');	
		$this->data['text_customer'] = $this->language->get('text_customer');	
		$this->data['text_customer_group'] = $this->language->get('text_customer_group');
		$this->data['text_affiliate_all'] = $this->language->get('text_affiliate_all');	
		$this->data['text_affiliate'] = $this->language->get('text_affiliate');	
		$this->data['text_product'] = $this->language->get('text_product');	

		$this->data['entry_store'] = $this->language->get('entry_store');
		$this->data['entry_to'] = $this->language->get('entry_to');
		$this->data['entry_customer_group'] = $this->language->get('entry_customer_group');
		$this->data['entry_customer'] = $this->language->get('entry_customer');
		$this->data['entry_affiliate'] = $this->language->get('entry_affiliate');
		$this->data['entry_product'] = $this->language->get('entry_product');
		$this->data['entry_subject'] = $this->language->get('entry_subject');
		$this->data['entry_message'] = $this->language->get('entry_message');

		$this->data['button_send'] = $this->language->get('button_send');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		$this->data['token'] = $this->session->data['token'];

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('sale/contact', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$this->data['cancel'] = $this->url->link('sale/contact', 'token=' . $this->session->data['token'], 'SSL');

		$this->load->model('setting/store');

		$this->data['stores'] = $this->model_setting_store->getStores();

		//newsletter template 
		$this->load->model('sale/newsletter_template');
		
		$code = '';
		$title = '';
		$this->data['template_selected'] = "";
		
		if(!isset($this->request->get['template_id'])) {
			$this->request->get['template_id'] = 6;
		}

		$this->data['template_selected'] = $this->request->get['template_id'];
		$template_info = $this->model_sale_newsletter_template->getTemplate($this->request->get['template_id']);
		$title = $template_info['template_subject'];
		$code = $template_info['template_code'];
		
		$this->data['message'] = $code;
		$this->data['title'] = $title;
		
		$this->data['templates'] = array();
		
		$this->data['templates'] = $this->model_sale_newsletter_template->getTemplates();
		
		$this->data['url'] = HTTPS_SERVER . 'index.php?route=sale/contact&token=' . $this->session->data['token'];

		$this->template = 'sale/contact.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	public function send() {
		$this->language->load('sale/contact');

		$json = array();

		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			if (!$this->user->hasPermission('modify', 'sale/contact')) {
				$json['error']['warning'] = $this->language->get('error_permission');
			}

			if (!$this->request->post['subject']) {
				$json['error']['subject'] = $this->language->get('error_subject');
			}

			if (!$this->request->post['message']) {
				$json['error']['message'] = $this->language->get('error_message');
			}

			if (!$json) {
				
				$store_name = $this->config->get('config_name')[$this->config->get('config_language_id')];

				// $this->load->model('sale/customer');
				$this->load->model('catalog/news');

				$this->load->model('sale/newssubscribers');

				if (isset($this->request->get['page'])) {
					$page = $this->request->get['page'];
				} else {
					$page = 1;
				}

				$email_total = 0;

				$emails = array();
				
				$send_data = array(
					'start'  => ($page - 1) * 10,
					'limit'  => 10
				);

				switch ($this->request->post['to']) {
					case 'newsletter':						
						$email_total = $this->model_sale_newssubscribers->getTotalEmails($send_data);

						$results = $this->model_sale_newssubscribers->getEmails($send_data);

						foreach ($results as $result) {
							$emails[] = $result['email'];
						}
						$emails = array_unique($emails);
						break;
					// case 'customer':
						// $email_total = $this->model_sale_customer->getTotalCustomers($send_data);

						// $results = $this->model_sale_customer->getCustomers($send_data);

						// foreach ($results as $result) {
							// $emails[] = $result['email'];
						// }		
						// $emails = array_unique($emails);
						// break;
					// case 'all':
					
						// $email_total = $this->model_sale_newssubscribers->getTotalEmails($send_data) + $this->model_sale_customer->getTotalCustomers($send_data);
						// $results = $this->model_sale_newssubscribers->getEmails($send_data);

						// foreach ($results as $result) {
							// $emails[] = $result['email'];
						// }
						
						// $results = $this->model_sale_customer->getCustomers($send_data);

						// foreach ($results as $result) {
							// $emails[] = $result['email'];
						// }
						// $emails = array_unique($emails);
						// break;			
					case 'customize':
						$results = explode(';', $this->request->post['list_customize']);
						foreach ($results as $email) {
							$emails[] = str_replace(" ","",$email);
						}
						$emails = array_unique($emails);
						$email_total = count($emails);
						break;
				}
				
				// $html_ex = '<br /><table align="center" style="background-color:#fff; border-collapse:collapse; margin:0px auto; padding:0px; width:600px"><tbody>';
				// $this->load->model('tool/image');
				// foreach($this->request->post['news_id'] as $news_id){
					// $news_info = $this->model_catalog_news->getNew($news_id);
					// $image = $this->model_tool_image->cropsize($news_info['image'], 130, 80);
					// $html_ex .= '<tr><td style="padding: 5px;padding-left:0;"><img style="width: 100px;display: block" src="'. $image .'"/></td><td><a href="https://suckhoenoitiet.vn/'. $news_info['keyword'] .'" style="color:rgb(191, 0, 22);font-size:15px;font-weight:bold;display:block;margin-bottom:10px; text-decoration: none;">'. $news_info['name'] .'</a><div style="font-size:12px"><span style="color:#999">Ngày:</span> <strong>'. date('d-m-Y', strtotime($news_info['date_available'])) .'</strong></div></td></tr>';
				// }
				// $html_ex .= '</tbody></table>';
				
				// $html = str_replace("#from",date('d-m-Y', strtotime($this->request->post['date_from'])),$this->request->post['message']);
				// $html = str_replace("#to",date('d-m-Y', strtotime($this->request->post['date_to'])),$html);
				// $html = str_replace("#tieude",$this->request->post['subject'],$html);
				// $html = str_replace("##tindachon##",$html_ex,$html);
				
				$html = $this->request->post['message'];
				
				// $emails = array(); 
				// $emails = array('hungnv@osd.vn','dovv@osd.vn','test@suckhoenoitiet.vn');
				// $email_total = count($emails);

				if ($emails) {
					/* $this->session->data['email'] = array_unique($emails); */
					$start = ($page - 1) * 10;
					$end = $start + 10;

					if ($end < $email_total) {
						$json['success'] = sprintf($this->language->get('text_sent'), $start, $email_total);
					} else { 
						$json['success'] = $this->language->get('text_success');
					}				

					if ($end < $email_total) {
						$json['next'] = str_replace('&amp;', '&', $this->url->link('sale/contact/send', 'token=' . $this->session->data['token'] . '&page=' . ($page + 1), 'SSL'));
					} else {
						$json['next'] = '';
					}
// $json['success'] = $html;
					$message  = '<html dir="ltr" lang="en">' . "\n";
					$message .= '  <head>' . "\n";
					$message .= '    <title>' . $this->request->post['subject'] . '</title>' . "\n";
					$message .= '    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">' . "\n";
					$message .= '  </head>' . "\n";
					$message .= '  <body>' . html_entity_decode($html, ENT_QUOTES, 'UTF-8') . '</body>' . "\n";
					$message .= '</html>' . "\n";

					foreach ($emails as $email) {
						$mail = new Mail();	
						$mail->protocol = $this->config->get('config_mail_protocol');
						$mail->parameter = $this->config->get('config_mail_parameter');
						$mail->hostname = $this->config->get('config_smtp_host');
						$mail->username = $this->config->get('config_smtp_username');
						$mail->password = $this->config->get('config_smtp_password');
						$mail->port = $this->config->get('config_smtp_port');
						$mail->timeout = $this->config->get('config_smtp_timeout');				
						$mail->setTo($email);
						$mail->setFrom($this->config->get('config_email'));
						$mail->setSender($store_name);
						$mail->setSubject(html_entity_decode($this->request->post['subject'], ENT_QUOTES, 'UTF-8'));					
						$mail->setHtml($message);
						$mail->send();
					}
				}
			}
		}

		$this->response->setOutput(json_encode($json));	
	}
}
?>