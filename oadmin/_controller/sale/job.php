<?php

class ControllerSalejob extends Controller {

    private $error = array();

    public function index() {
        $this->language->load('sale/job');
        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('sale/job');

        $this->getList();
    }

    protected function getList() {

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';


        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('sale/job', 'token=' . $this->session->data['token'] . $url, 'SSL'),
            'separator' => ' :: '
        );

        $this->data['jobs'] = array();

        $this->load->model('sale/job');

		$data = array(
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

        $results = $this->model_sale_job->getjobs($data);
        foreach ($results as $result) {
            if($result['code']){
                $filename = $this->model_sale_job->getfilebyCode($result['code']);
            }else{
                $filename = false;
            }
            $this->data['jobs'][]=array(
                 'job_id'    => $result['job_id'],
                 'job_name'    => $result['job_name'],
                 'job_phone'    => $result['job_phone'],
                 'job_email'    => $result['job_email'],
                 'job_intro'    => $result['job_intro'],
                 'file'         => $result['file'],
                 'filename'     => $filename,
                 'status'       => $result['status'],
                 'date_added'   => $result['date_added'],
                 'position'     => $result['position'],
            );
        }
        $job_total = $this->model_sale_job->getTotaljobs($data);
		$this->data['delete'] = $this->url->link('sale/job/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

        $this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['delete'] = $this->url->link('sale/job/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

        $this->data['text_enabled'] = $this->language->get('text_enabled');
        $this->data['text_disabled'] = $this->language->get('text_disabled');
        $this->data['text_no_results'] = $this->language->get('text_no_results');
        $this->data['text_image_manager'] = $this->language->get('text_image_manager');
        $this->data['button_delete'] = $this->language->get('button_delete');

        $this->data['token'] = $this->session->data['token'];

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }

        $url = '';

        $pagination = new Pagination();
        $pagination->total = $job_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_admin_limit');
        $pagination->text = $this->language->get('text_pagination');
        $pagination->url = $this->url->link('sale/job', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

        $this->data['pagination'] = $pagination->render();

        $this->template = 'sale/job_list.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }

	public function updatestatus() {
		$this->load->language('sale/job');
		$this->load->model('sale/job');
		$output='';
		if(isset($this->request->get['object_id'])){
			$get_request = explode('-',$this->request->get['object_id']);
			if(count($get_request)==2){
				$column_name = $get_request[0];
				$order_id = $get_request[1];
				$result = $this->model_sale_job->getjob($order_id);
				if($result[$column_name]){
					$this->model_sale_job->updateStatus($order_id, $column_name, 0);
					} else {
					$this->model_sale_job->updateStatus($order_id, $column_name, 1);
				}
				$result = $this->model_sale_job->getjob($order_id);
				$output = $result[$column_name] ? $this->language->get('text_enabled') : $this->language->get('text_disabled');
			}
		}
		$this->response->setOutput($output);
	}

  public function delete(){
        $this->language->load('sale/job');
		$this->load->model('sale/job');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $order_id) {
				$this->model_sale_job->deletejob($order_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');
			$this->redirect($this->url->link('sale/job', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->getList();
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'sale/job')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true;
			} else {
			return false;
		}
	}
}

?>
