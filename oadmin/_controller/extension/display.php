<?php
class ControllerExtensionDisplay extends Controller {
	private $error = array();

	public function index() {
		$this->language->load('extension/display');
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->response->ClearCache();
		$this->model_setting_setting->editSetting('display', $this->request->post);

		$this->session->data['success'] = $this->language->get('text_success');

		$this->redirect($this->url->link('extension/display', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_select'] = $this->language->get('text_select');
		$this->data['text_configuration'] = $this->language->get('text_configuration');
		$this->data['text_none'] = $this->language->get('text_none');
		$this->data['text_yes'] = $this->language->get('text_yes');
		$this->data['text_no'] = $this->language->get('text_no');
	
		$this->data['entry_product_date'] = $this->language->get('entry_product_date');
		$this->data['entry_product_status'] = $this->language->get('entry_product_status');
		$this->data['entry_product_price'] = $this->language->get('entry_product_price');
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['entry_news_limit'] = $this->language->get('entry_news_limit');
		$this->data['entry_catalog_limit'] = $this->language->get('entry_catalog_limit');
		$this->data['entry_admin_limit'] = $this->language->get('entry_admin_limit');


		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('extension/display', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['catalog_limit'])) {
			$this->data['error_catalog_limit'] = $this->error['error_catalog_limit'];
		} else {
			$this->data['error_catalog_limit'] = '';
		}
		if (isset($this->error['admin_limit'])) {
			$this->data['error_admin_limit'] = $this->error['error_admin_limit'];
		} else {
			$this->data['error_admin_limit'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->data['action'] = $this->url->link('extension/display', 'token=' . $this->session->data['token'], 'SSL');

		if(isset($this->session->data['redirect'])){
			$this->data['cancel'] = $this->session->data['redirect'];
		}else{
			$this->data['cancel'] = $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL');
		}

		if (isset($this->request->post['config_dateog'])) {
			$this->data['config_dateog'] = $this->request->post['config_dateog'];
		} else {
			$this->data['config_dateog'] = $this->config->get('config_dateog');
		}

		if (isset($this->request->post['config_product_stock'])) {
			$this->data['config_product_stock'] = $this->request->post['config_product_stock'];
		} else {
			$this->data['config_product_stock'] = $this->config->get('config_product_stock');
		}

		if (isset($this->request->post['config_product_price'])) {
			$this->data['config_product_price'] = $this->request->post['config_product_price'];
		} else {
			$this->data['config_product_price'] = $this->config->get('config_product_price');
		}

		if (isset($this->request->post['config_catalog_limit'])) {
			$this->data['config_catalog_limit'] = $this->request->post['config_catalog_limit'];
		} else {
			$this->data['config_catalog_limit'] = $this->config->get('config_catalog_limit');
		}

		if (isset($this->request->post['config_news_limit'])) {
			$this->data['config_news_limit'] = $this->request->post['config_news_limit'];
		} else {
			$this->data['config_news_limit'] = $this->config->get('config_news_limit');
		}

		if (isset($this->request->post['config_admin_limit'])) {
			$this->data['config_admin_limit'] = $this->request->post['config_admin_limit'];
		} else {
			$this->data['config_admin_limit'] = $this->config->get('config_admin_limit');
		}

		$this->template = 'extension/display.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/display')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
		if (!$this->request->post['config_catalog_limit']) {
			$this->error['catalog_limit'] = $this->language->get('error_limit');
		}

		if (!$this->request->post['config_admin_limit']) {
			$this->error['admin_limit'] = $this->language->get('error_limit');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

}
?>
