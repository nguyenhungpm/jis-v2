<?php
class ControllerExtensionImgconf extends Controller {
	private $error = array();
	
	public function index() {
		$this->language->load('extension/imgconf');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('setting/setting');
		// print_r($this->session->data['d']);
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->response->ClearCache();
			// $this->session->data['d'] = $this->request->post;
			$this->model_setting_setting->editSetting('imgconf', $this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success');
			
			$this->redirect($this->url->link('extension/imgconf', 'token=' . $this->session->data['token'], 'SSL'));
		}
		
		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_close'] = $this->language->get('button_close');
		$this->data['entry_logo'] = $this->language->get('entry_logo');
		$this->data['entry_icon'] = $this->language->get('entry_icon');
		$this->data['entry_image_category'] = $this->language->get('entry_image_category');
		$this->data['entry_image_thumb'] = $this->language->get('entry_image_thumb');
		$this->data['entry_image_popup'] = $this->language->get('entry_image_popup');
		$this->data['entry_image_product'] = $this->language->get('entry_image_product');
		$this->data['entry_image_additional'] = $this->language->get('entry_image_additional');
		$this->data['entry_image_related'] = $this->language->get('entry_image_related'); $this->data['text_image_manager'] = $this->language->get('text_image_manager'); $this->data['text_browse'] = $this->language->get('text_browse');
		
		$this->data['entry_image_related'] = $this->language->get('entry_image_related');
		$this->data['entry_image_gallery'] = $this->language->get('entry_image_gallery');
		$this->data['entry_image_album'] = $this->language->get('entry_image_album');
		$this->data['entry_image_album_related'] = $this->language->get('entry_image_album_related');
		$this->data['entry_gallery_settings'] = $this->language->get('entry_gallery_settings');
		$this->data['text_configuration'] = $this->language->get('text_configuration');

		$this->data['entry_image_news'] = $this->language->get('entry_image_news');
		
		$this->data['text_image_manager'] = $this->language->get('text_image_manager'); $this->data['text_browse'] = $this->language->get('text_browse');
		$this->data['text_clear'] = $this->language->get('text_clear');
		
		$this->data['action'] = $this->url->link('extension/imgconf', 'token=' . $this->session->data['token'], 'SSL');
		
		if(isset($this->session->data['redirect'])){
			$this->data['cancel'] = $this->session->data['redirect'];
			}else{
			$this->data['cancel'] = $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL');
		}
		
		$this->data['token'] = $this->session->data['token'];
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
			} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			
			unset($this->session->data['success']);
			} else {
			$this->data['success'] = '';
		}
		
		if (isset($this->error['image_category'])) {
			$this->data['error_image_category'] = $this->error['image_category'];
			} else {
			$this->data['error_image_category'] = '';
		}
		if (isset($this->error['image_news'])) {
			$this->data['error_image_news'] = $this->error['image_news'];
			} else {
			$this->data['error_image_news'] = '';
		}
		
		if (isset($this->error['image_thumb'])) {
			$this->data['error_image_thumb'] = $this->error['image_thumb'];
			} else {
			$this->data['error_image_thumb'] = '';
		}
		
		if (isset($this->error['image_popup'])) {
			$this->data['error_image_popup'] = $this->error['image_popup'];
			} else {
			$this->data['error_image_popup'] = '';
		}
		
		if (isset($this->error['image_product'])) {
			$this->data['error_image_product'] = $this->error['image_product'];
			} else {
			$this->data['error_image_product'] = '';
		}
		
		if (isset($this->error['image_related'])) {
			$this->data['error_image_related'] = $this->error['error_image_related'];
			} else {
			$this->data['error_image_related'] = '';
		}
		
		$this->load->model('tool/image');
		if (isset($this->request->post['config_logo'])) {
			$this->data['config_logo'] = $this->request->post['config_logo'];
			} else {
			$this->data['config_logo'] = $this->config->get('config_logo');
		}
		
		if ($this->config->get('config_logo') && file_exists(DIR_IMAGE . $this->config->get('config_logo')) && is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
			$this->data['logo'] = $this->model_tool_image->resize($this->config->get('config_logo'), 100, 100);
			} else {
			$this->data['logo'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
		}

		if (isset($this->request->post['config_footer_logo'])) {
			$this->data['config_footer_logo'] = $this->request->post['config_footer_logo'];
			} else {
			$this->data['config_footer_logo'] = $this->config->get('config_footer_logo');
		}
		
		if ($this->config->get('config_footer_logo') && file_exists(DIR_IMAGE . $this->config->get('config_footer_logo')) && is_file(DIR_IMAGE . $this->config->get('config_footer_logo'))) {
			$this->data['footer_logo'] = $this->model_tool_image->resize($this->config->get('config_footer_logo'), 100, 100);
			} else {
			$this->data['footer_logo'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
		}
		
		if (isset($this->request->post['config_icon'])) {
			$this->data['config_icon'] = $this->request->post['config_icon'];
			} else {
			$this->data['config_icon'] = $this->config->get('config_icon');
		}
		
		if ($this->config->get('config_icon') && file_exists(DIR_IMAGE . $this->config->get('config_icon')) && is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
			$this->data['icon'] = $this->model_tool_image->resize($this->config->get('config_icon'), 100, 100);
			} else {
			$this->data['icon'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
		}
		
		$this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
		
		if (isset($this->request->post['config_image_category_width'])) {
			$this->data['config_image_category_width'] = $this->request->post['config_image_category_width'];
			} else {
			$this->data['config_image_category_width'] = $this->config->get('config_image_category_width');
		}
		
		if (isset($this->request->post['config_image_news_width'])) {
			$this->data['config_image_news_width'] = $this->request->post['config_image_news_width'];
			} else {
			$this->data['config_image_news_width'] = $this->config->get('config_image_news_width');
		}
		
		if (isset($this->request->post['config_image_news_height'])) {
			$this->data['config_image_news_height'] = $this->request->post['config_image_news_height'];
			} else {
			$this->data['config_image_news_height'] = $this->config->get('config_image_news_height');
		}
		
		if (isset($this->request->post['config_image_category_height'])) {
			$this->data['config_image_category_height'] = $this->request->post['config_image_category_height'];
			} else {
			$this->data['config_image_category_height'] = $this->config->get('config_image_category_height');
		}
		
		if (isset($this->request->post['config_image_thumb_width'])) {
			$this->data['config_image_thumb_width'] = $this->request->post['config_image_thumb_width'];
			} else {
			$this->data['config_image_thumb_width'] = $this->config->get('config_image_thumb_width');
		}
		
		if (isset($this->request->post['config_image_thumb_height'])) {
			$this->data['config_image_thumb_height'] = $this->request->post['config_image_thumb_height'];
			} else {
			$this->data['config_image_thumb_height'] = $this->config->get('config_image_thumb_height');
		}
		
		if (isset($this->request->post['config_image_popup_width'])) {
			$this->data['config_image_popup_width'] = $this->request->post['config_image_popup_width'];
			} else {
			$this->data['config_image_popup_width'] = $this->config->get('config_image_popup_width');
		}
		
		if (isset($this->request->post['config_image_popup_height'])) {
			$this->data['config_image_popup_height'] = $this->request->post['config_image_popup_height'];
			} else {
			$this->data['config_image_popup_height'] = $this->config->get('config_image_popup_height');
		}
		
		if (isset($this->request->post['config_image_product_width'])) {
			$this->data['config_image_product_width'] = $this->request->post['config_image_product_width'];
			} else {
			$this->data['config_image_product_width'] = $this->config->get('config_image_product_width');
		}
		
		if (isset($this->request->post['config_image_product_height'])) {
			$this->data['config_image_product_height'] = $this->request->post['config_image_product_height'];
			} else {
			$this->data['config_image_product_height'] = $this->config->get('config_image_product_height');
		}
		
		if (isset($this->request->post['config_image_additional_width'])) {
			$this->data['config_image_additional_width'] = $this->request->post['config_image_additional_width'];
			} else {
			$this->data['config_image_additional_width'] = $this->config->get('config_image_additional_width');
		}
		
		if (isset($this->request->post['config_image_additional_height'])) {
			$this->data['config_image_additional_height'] = $this->request->post['config_image_additional_height'];
			} else {
			$this->data['config_image_additional_height'] = $this->config->get('config_image_additional_height');
		}
		
		if (isset($this->request->post['config_image_gallery_width'])) {
			$this->data['config_image_gallery_width'] = $this->request->post['config_image_gallery_width'];
			} else {
			$this->data['config_image_gallery_width'] = $this->config->get('config_image_gallery_width');
		}
		
		if (isset($this->request->post['config_image_gallery_height'])) {
			$this->data['config_image_gallery_height'] = $this->request->post['config_image_gallery_height'];
			} else {
			$this->data['config_image_gallery_height'] = $this->config->get('config_image_gallery_height');
		}
		
		if (isset($this->request->post['config_image_related_width'])) {
			$this->data['config_image_related_width'] = $this->request->post['config_image_related_width'];
			} else {
			$this->data['config_image_related_width'] = $this->config->get('config_image_related_width');
		}
		
		if (isset($this->request->post['config_image_related_height'])) {
			$this->data['config_image_related_height'] = $this->request->post['config_image_related_height'];
			} else {
			$this->data['config_image_related_height'] = $this->config->get('config_image_related_height');
		}
		
		if (isset($this->request->post['config_image_cart_width'])) {
			$this->data['config_image_cart_width'] = $this->request->post['config_image_cart_width'];
			} else {
			$this->data['config_image_cart_width'] = $this->config->get('config_image_cart_width');
		}
		
		if (isset($this->request->post['config_image_cart_height'])) {
			$this->data['config_image_cart_height'] = $this->request->post['config_image_cart_height'];
			} else {
			$this->data['config_image_cart_height'] = $this->config->get('config_image_cart_height');
		}
		
		if (isset($this->request->post['config_gallery_width'])) {
			$this->data['config_gallery_width'] = $this->request->post['config_gallery_width'];
			} else {
			$this->data['config_gallery_width'] = $this->config->get('config_gallery_width');
		}
		
		if (isset($this->request->post['config_gallery_height'])) {
			$this->data['config_gallery_height'] = $this->request->post['config_gallery_height'];
			} else {
			$this->data['config_gallery_height'] = $this->config->get('config_gallery_height');
		}
		
		if (isset($this->request->post['config_album_width'])) {
			$this->data['config_album_width'] = $this->request->post['config_album_width'];
			} else {
			$this->data['config_album_width'] = $this->config->get('config_album_width');
		}
		
		if (isset($this->request->post['config_album_height'])) {
			$this->data['config_album_height'] = $this->request->post['config_album_height'];
			} else {
			$this->data['config_album_height'] = $this->config->get('config_album_height');
		}
		
		if (isset($this->request->post['config_album_related_width'])) {
			$this->data['config_album_related_width'] = $this->request->post['config_album_related_width'];
			} else {
			$this->data['config_album_related_width'] = $this->config->get('config_album_related_width');
		}
		
		if (isset($this->request->post['config_album_related_height'])) {
			$this->data['config_album_related_height'] = $this->request->post['config_album_related_height'];
			} else {
			$this->data['config_album_related_height'] = $this->config->get('config_album_related_height');
		}
		
		//error
		if (empty($this->request->post['config_image_category_width']) || empty($this->request->post['config_image_category_height'])) {
			$this->error['image_category'] = $this->language->get('error_image_category');
		}
		
		if (empty($this->request->post['config_image_thumb_width']) || empty($this->request->post['config_image_thumb_height'])) {
			$this->error['image_thumb'] = $this->language->get('error_image_thumb');
		}
		
		if (empty($this->request->post['config_image_popup_width']) || empty($this->request->post['config_image_popup_height'])) {
			$this->error['image_popup'] = $this->language->get('error_image_popup');
		}
		
		if (empty($this->request->post['config_image_product_width']) || empty($this->request->post['config_image_product_height'])) {
			$this->error['image_product'] = $this->language->get('error_image_product');
		}
		
		if (empty($this->request->post['config_image_related_width']) || empty($this->request->post['config_image_related_height'])) {
			$this->error['image_related'] = $this->language->get('error_image_related');
		}
		$this->template = 'extension/imgconf.tpl';
		$this->children = array(
		'common/header',
		'common/footer'
		);
		
		$this->response->setOutput($this->render());
	}
	
	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/imgconf')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->error) {
			return true;
			} else {
			return false;
		}
	}
}
?>
