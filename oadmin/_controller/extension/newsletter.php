<?php
class ControllerExtensionNewsletter extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/newsletter');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('extension/newsletter');

		$this->getList();
	}

	public function insert() {
		$this->load->language('extension/newsletter');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('extension/newsletter');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_extension_newsletter->addEmail($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			$this->redirect($this->url->link('extension/newsletter', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}

		$this->getForm();
	}

	public function edit(){
        $json = array();
		$this->load->language('extension/newsletter');
		$this->load->model('extension/newsletter');

		$v = $this->model_extension_newsletter->editEmail($this->request->get['id'], $this->request->get['email']);
		if($v){
			$this->session->data['success'] = $this->language->get('text_success');
		}

        $this->response->setOutput(json_encode($json));
	}

	public function update() {
		$this->load->language('extension/newsletter');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('extension/newsletter');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_extension_newsletter->editEmail($this->request->get['id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			$this->redirect($this->url->link('extension/newsletter', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}

		$this->getForm();
	}
	public function export() {

		$this->load->model('extension/newsletter');

		$contents="STT \t Email \n";

		$results = $this->model_extension_newsletter->exportEmails();

		$filename ="Newsletter_subscribers_".date("d-m-Y").".xls";
		if($results) {
			$k=1;
			foreach($results as $result){
				// $contents .= implode("\t",$results)."\n";
				$contents .= $k . "\t" . $result['email'] . "\n";
				$k++;
			}
		}else{
			$contents = $this->language->get('text_no_results');
		}
		header('Content-type: application/ms-excel');
		header('Content-Disposition: attachment; filename='.$filename);
		echo $contents;

	}
	public function delete() {
		$this->load->language('extension/newsletter');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('extension/newsletter');

		if (isset($this->request->post['selected'])) {
      		foreach ($this->request->post['selected'] as $id) {
				$this->model_extension_newsletter->deleteEmail($id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			$this->redirect($this->url->link('extension/newsletter', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}

		$this->getList();
	}

	private function getList() {

  		$this->data['breadcrumbs'] = array();

  		$this->data['breadcrumbs'][] = array(
       		'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
       		'text'      => $this->language->get('text_home'),
      		'separator' => FALSE
   		);

  		$this->data['breadcrumbs'][] = array(
       		'href'      => $this->url->link('extension/newsletter', 'token=' . $this->session->data['token'], 'SSL'),
       		'text'      => $this->language->get('heading_title'),
      		'separator' => ' :: '
   		);


		$this->data['insert'] = $this->url->link('extension/newsletter/insert', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['delete'] = $this->url->link('extension/newsletter/delete', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['export'] = $this->url->link('extension/newsletter/export', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['token'] = $this->session->data['token'];

		$this->data['text_export'] = $this->language->get('text_export');
		$url = '';
		if(isset($this->request->get['page'])) {
	            $page = $this->request->get['page'];
	            $this->data['page'] = $this->request->get['page'];
				$url .= '&page='. $this->request->get['page'];
		}else{
		        $page = 1;
		}

		$this->data['emails'] = array();

		$data = array(
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		$email_total = $this->model_extension_newsletter->getTotalEmails($data);

		$results = $this->model_extension_newsletter->getEmails($data);


		foreach ($results as $result) {
			$action = array();

			// $action[] = array(
				// 'text' => $this->language->get('text_edit'),
				// 'href' => $this->url->link('extension/newsletter/update', 'token=' . $this->session->data['token'] . '&id=' . $result['id'], 'SSL')
			// );

			$this->data['emails'][] = array(
				'id' 		=> $result['id'],
				'email' 	=> $result['email'],
				'phone' 	=> $result['phone'],
				'name' 	=> $result['name'],
				'type' 	=> $result['type'],
				'date_added' 	=> date($this->language->get('date_format_short'), strtotime($result['date_added'])),
				'edit' => $this->url->link('extension/newsletter', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . $url, 'SSL')
				// 'action'    => $action
				// 'action'    => $action
			);
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_list'] = $this->language->get('text_list');

		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_email'] = $this->language->get('column_email');
		$this->data['column_date_added'] = $this->language->get('column_date_added');
		$this->data['column_action'] = $this->language->get('column_action');

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = "";


		$pagination = new Pagination();
		$pagination->total = $email_total;
		$pagination->page  = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text  = $this->language->get('text_pagination');
		$pagination->url   = $this->url->link('extension/newsletter/', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['sort_name'] = $this->url->link('extension/newsletter/', 'token=' . $this->session->data['token']);

		$this->template = 'extension/newsletter_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
 	}

	private function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_code'] = $this->language->get('entry_code');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		$this->data['tab_general'] = $this->language->get('tab_general');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

 		if (isset($this->error['error_email_name'])) {
			$this->data['error_email_name'] = $this->error['error_email_name'];
		} else {
			$this->data['error_email_name'] = '';
		}

 		if (isset($this->error['error_email_exist'])) {
			$this->data['error_email_exist'] = $this->error['error_email_exist'];
		} else {
			$this->data['error_email_exist'] = '';
		}



  		$this->data['breadcrumbs'] = array();

  		$this->data['breadcrumbs'][] = array(
       		'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
       		'text'      => $this->language->get('text_home'),
      		'separator' => FALSE
   		);

  		$this->data['breadcrumbs'][] = array(
       		'href'      => $this->url->link('extension/newsletter', 'token=' . $this->session->data['token'], 'SSL'),
       		'text'      => $this->language->get('heading_title'),
      		'separator' => ' :: '
   		);

		if (!isset($this->request->get['id'])) {
			$this->data['action'] = $this->url->link('extension/newsletter/insert', 'token=' . $this->session->data['token'], 'SSL');
		} else {
			$this->data['action'] = $this->url->link('extension/newsletter/update', 'token=' . $this->session->data['token'] . '&id=' . $this->request->get['id'], 'SSL');
		}

		$this->data['token'] = $this->session->data['token'];

    	$this->data['cancel'] = $this->url->link('extension/newsletter', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->get['id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$email_info = $this->model_extension_newsletter->getEmail($this->request->get['id']);
		}

		if (isset($this->request->post['email_name'])) {
			$this->data['email_name'] = $this->request->post['email_name'];
		} elseif (isset($email_info)) {
			$this->data['email_name'] = $email_info['name'];
		} else {
			$this->data['email_name'] = '';
		}

		if (isset($this->request->post['email_id'])) {
			$this->data['email_id'] = $this->request->post['email_id'];
		} elseif (isset($email_info)) {
			$this->data['email_id'] = $email_info['email_id'];
		} else {
			$this->data['email_id'] = '';
		}

		$this->template = 'extension/newsletter_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
	}

	private function validateForm() {

	$this->load->model('extension/newsletter');

		if (!$this->user->hasPermission('modify', 'extension/newsletter')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

	 if(!filter_var($this->request->post['email_id'],FILTER_VALIDATE_EMAIL)){
			$this->error['error_email_name'] = $this->language->get('error_email');
		}

	 	if(isset($this->request->get['id']) and $this->request->get['id']!=""){

			if($this->model_extension_newsletter->checkmail($this->request->post['email_id'],$this->request->get['id']))
			 $this->error['error_email_exist'] = $this->language->get('error_email_exist');

		}else{

		   if($this->model_extension_newsletter->checkmail($this->request->post['email_id']))
		   $this->error['error_email_exist'] = $this->language->get('error_email_exist');

		}

		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

}
?>
