<?php
class ControllerCommonFooter extends Controller {   
	protected function index() {
		$this->language->load('common/footer');
		
		if(isset($this->session->data['redirect_c']) && $this->session->data['redirect_c'] && $this->session->data['redirect_c'] != $_SERVER['REQUEST_URI']){
			$this->session->data['redirect'] = $this->session->data['redirect_c'];
		}else{
			$this->session->data['redirect'] = '';
		}
		if(isset($this->session->data['token'])){
			$this->data['token'] = $this->session->data['token'];
		}else{
			$this->data['token'] = '';
		}
		$this->session->data['redirect_c'] =  $_SERVER['REQUEST_URI'];

		$this->data['text_footer'] = sprintf($this->language->get('text_footer'), VERSION);

		if (file_exists(DIR_SYSTEM . 'config/svn/svn.ver')) {
			$this->data['text_footer'] .= '.r' . trim(file_get_contents(DIR_SYSTEM . 'config/svn/svn.ver'));
		}

		$this->template = 'common/footer.tpl';

		$this->render();
	}
}
?>