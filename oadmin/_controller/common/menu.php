<?php
class ControllerCommonMenu extends Controller {
	public function index() {
		$this->language->load('common/header');
		$this->data['store'] = HTTP_CATALOG;
		$this->data['logout'] = $this->url->link('common/logout', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['logged'] = sprintf($this->language->get('text_logged'), $this->user->getUserName());
		$this->data['text_logout'] = $this->language->get('text_logout');

		// Menu
		$this->data['menus'][] = array(
			'id'       => 'dashboard',
			'icon'       => 'fa-dashboard',
			'name'	   => $this->language->get('text_dashboard'),
			'href'     => $this->url->link('common/home', 'token=' . $this->session->data['token'], true),
			'children' => array()
		);

		// Catalog
		$catalog = array();
		if ($this->user->hasPermission('access', 'catalog/product')) {
			$product[] = array(
				'name'	   => 'Bài viết',
				'href'     => $this->url->link('catalog/product', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}
		
		if ($this->user->hasPermission('access', 'catalog/category')) {
			$product[] = array(
				'name'	   => 'Nhóm bài viết',
				'href'     => $this->url->link('catalog/category', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}

		$catalog[] = array(
			'name'	   => 'Học đường',
			'href'     => '',
			'children' => $product
		);


		if ($this->user->hasPermission('access', 'catalog/doccat')) {
			$document[] = array(
				'name'	   => 'Nhóm tài liệu',
				'href'     => $this->url->link('catalog/doccat', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}

		if ($this->user->hasPermission('access', 'catalog/document')) {
			$document[] = array(
				'name'	   => 'Tài liệu',
				'href'     => $this->url->link('catalog/document', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}

		if ($this->user->hasPermission('access', 'sale/customer')) {
			$document[] = array(
				'name'	   => 'Người dùng',
				'href'     => $this->url->link('sale/customer', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}

		$catalog[] = array(
			'name'	   => 'Trang nội bộ',
			'href'     => '',
			'children' => $document
		);


		
		if ($this->user->hasPermission('access', 'catalog/quiz')) {
			$catalog[] = array(
				'name'	   => 'Bài thi trắc nghiệm',
				'href'     => $this->url->link('catalog/quiz', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}
		if ($this->user->hasPermission('access', 'catalog/group')) {
			$catalog[] = array(
				'name'	   => 'Nhóm bài thi',
				'href'     => $this->url->link('catalog/group', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}

		$news_menu = array();
		if ($this->user->hasPermission('access', 'catalog/news')) {
			$news_menu[] = array(
				'name'	   => $this->language->get('text_news'),
				'href'     => $this->url->link('catalog/news', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}
		if ($this->user->hasPermission('access', 'catalog/news_category')) {
			$news_menu[] = array(
				'name'	   => 'Nhóm tin tức',
				'href'     => $this->url->link('catalog/news_category', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}

		$catalog[] = array(
			'name'	   => 'Tin tức',
			'href'     => '',
			'children' => $news_menu
		);


		// if ($this->user->hasPermission('access', 'catalog/type_car')) {
		// 	$catalog[] = array(
		// 		'name'	   => 'Loại xe',
		// 		'href'     => $this->url->link('catalog/type_car', 'token=' . $this->session->data['token'], true),
		// 		'children' => array()
		// 	);
		// }
		if ($this->user->hasPermission('access', 'catalog/faqs')) {
			$catalog[] = array(
				'name'	   => 'Hỏi đáp',
				'href'     => $this->url->link('catalog/faqs', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}
/*
		if ($this->user->hasPermission('access', 'catalog/doctor')) {
			$catalog[] = array(
				'name'	   => 'Đối tượng sử dụng',
				'href'     => $this->url->link('catalog/doctor', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}
*/

		$showroom = array();

		if ($this->user->hasPermission('access', 'catalog/dealer')) {
			$showroom[] = array(
				'name'	   => 'Đại lý',
				'href'     => $this->url->link('catalog/dealer', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}
		if ($this->user->hasPermission('access', 'catalog/district')) {
			$showroom[] = array(
				'name'	   =>'Quận/huyện',
				'href'     => $this->url->link('catalog/district', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}
		if ($this->user->hasPermission('access', 'catalog/area')) {
			$showroom[] = array(
				'name'	   => 'Khu vực',
				'href'     => $this->url->link('catalog/area', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}

		// $catalog[] = array(
		// 		'name'	   => 'Hệ thống bán hàng',
		// 		'href'     => '',
		// 		'children' => $showroom
		// );

		if ($this->user->hasPermission('access', 'catalog/admission')) {
			$catalog[] = array(
				'name'	   => 'Tuyển sinh',
				'href'     => $this->url->link('catalog/admission', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}

		if ($this->user->hasPermission('access', 'catalog/information')) {
			$catalog[] = array(
				'name'	   => $this->language->get('text_information'),
				'href'     => $this->url->link('catalog/information', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}
		
		if ($this->user->hasPermission('access', 'catalog/picture')) {
			$catalog[] = array(
				'name'	   => 'Thư viện ảnh',
				'href'     => $this->url->link('catalog/picture', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}

		if ($this->user->hasPermission('access', 'catalog/video')) {
			$catalog[] = array(
				'name'	   => 'Quản lý video',
				'href'     => $this->url->link('catalog/video', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}

		$manu = array();
		if ($this->user->hasPermission('access', 'catalog/manucat')) {
			$manu[] = array(
				'name'	   => 'Phòng ban',
				'href'     => $this->url->link('catalog/manucat', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}

		if ($this->user->hasPermission('access', 'catalog/manufacturer')) {
			$manu[] = array(
				'name'	   => 'Nhân viên',
				'href'     => $this->url->link('catalog/manufacturer', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}
		$catalog[] = array(
			'name'	   => 'Đội ngũ',
			'href'     => '',
			'children' => $manu
		);
		
		if ($this->user->hasPermission('access', 'design/banner')) {
			$catalog[] = array(
				'name'	   => $this->language->get('text_banner'),
				'href'     => $this->url->link('design/banner', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}

		if ($this->user->hasPermission('access', 'common/filemanager')) {
			$catalog[] = array(
				'name'	   => 'Quản lý file',
				'href'     => $this->url->link('common/filemanager', 'display=full&token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}

		if ($catalog) {
			$this->data['menus'][] = array(
				'id'       => 'catalog',
				'icon'       => 'fa-tags',
				'name'	   => 'Nội dung',
				'href'     => '',
				'children' => $catalog
			);
		}

		// Sale
		$sale = array();

		// if ($this->user->hasPermission('access', 'sale/order')) {
		// 	$sale[] = array(
		// 		'name'	   => $this->language->get('text_order'),
		// 		'href'     => $this->url->link('sale/order', 'token=' . $this->session->data['token'], true),
		// 		'children' => array()
		// 	);
		// }
		// if ($this->user->hasPermission('access', 'catalog/testimonial')) {
		// 	$sale[] = array(
		// 		'name'	   => 'Cảm nhận khách hàng',
		// 		'href'     => $this->url->link('catalog/testimonial', 'token=' . $this->session->data['token'], true),
		// 		'children' => array()
		// 	);
		// }
		/*
		if ($this->user->hasPermission('access', 'sale/contact')) {
			$sale[] = array(
				'name'	   => $this->language->get('text_seo_common'),
				'href'     => $this->url->link('sale/contact', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}
		*/
		if ($this->user->hasPermission('access', 'extension/newsletter')) {
			$sale[] = array(
				'name'	   => 'Đăng ký nhận tin',
				'href'     => $this->url->link('extension/newsletter', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}

		if ($this->user->hasPermission('access', 'sale/answer_submit')) {
			$sale[] = array(
				'name'	   => 'Kết quả kiểm tra',
				'href'     => $this->url->link('sale/answer_submit', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}

		// Sale
		$tuyendung = array();

		if ($this->user->hasPermission('access', 'catalog/recruitment')) {
			$tuyendung[] = array(
				'name'	   => 'Tin tuyển dụng',
				'href'     => $this->url->link('catalog/recruitment', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}
		if ($this->user->hasPermission('access', 'sale/job')) {
			$tuyendung[] = array(
				'name'	   => 'Hồ sơ ứng viên',
				'href'     => $this->url->link('sale/job', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}

		if ($this->user->hasPermission('access', 'catalog/reccat')) {
			$tuyendung[] = array(
				'name'	   => 'Nhóm chuyên môn',
				'href'     => $this->url->link('catalog/reccat', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}

		if ($tuyendung) {
			$this->data['menus'][] = array(
				'id'       => 'extension',
				'icon'       => 'fa-handshake-o',
				'name'	   => 'Tuyển dụng',
				'href'     => '',
				'children' => $tuyendung
			);
		}

		if ($sale) {
			$this->data['menus'][] = array(
				'id'       => 'extension',
				'icon'       => 'fa-handshake-o',
				'name'	   => $this->language->get('text_sale'),
				'href'     => '',
				'children' => $sale
			);
		}

		// SEO
		$seo = array();

		if ($this->user->hasPermission('access', 'seo/seo')) {
			$seo[] = array(
				'name'	   => $this->language->get('text_seo_common'),
				'href'     => $this->url->link('seo/seo', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}
		if ($this->user->hasPermission('access', 'seo/redirect')) {
			$seo[] = array(
				'name'	   => $this->language->get('text_redirect'),
				'href'     => $this->url->link('seo/redirect', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}

		if ($seo) {
			$this->data['menus'][] = array(
				'id'       => 'extension',
				'icon'       => 'fa-globe',
				'name'	   => $this->language->get('text_seo'),
				'href'     => '',
				'children' => $seo
			);
		}


		// Extension
		$extension = array();

		if ($this->user->hasPermission('access', 'extension/module')) {
			$extension[] = array(
				'name'	   => $this->language->get('text_module'),
				'href'     => $this->url->link('extension/module', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}

		if ($this->user->hasPermission('access', 'extension/imgconf')) {
			$extension[] = array(
				'name'	   => 'Logo và hình ảnh',
				'href'     => $this->url->link('extension/imgconf', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}

		if ($this->user->hasPermission('access', 'extension/menumanager')) {
			$extension[] = array(
				'name'	   => 'Trình đơn',
				'href'     => $this->url->link('extension/menumanager', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}

		if ($this->user->hasPermission('access', 'extension/display')) {
			$extension[] = array(
				'name'	   => 'Tùy chọn',
				'href'     => $this->url->link('extension/display', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}

		if ($this->user->hasPermission('access', 'design/layout')) {
			$extension[] = array(
				'name'	   => $this->language->get('text_layout'),
				'href'     => $this->url->link('design/layout', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}

		if ($this->user->hasPermission('access', 'extension/custom')) {
			$extension[] = array(
				'name'	   => 'Mã nhúng',
				'href'     => $this->url->link('extension/custom', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}

		if ($extension) {
			$this->data['menus'][] = array(
				'id'       => 'extension',
				'icon'       => 'fa-television',
				'name'	   => $this->language->get('text_extension'),
				'href'     => '',
				'children' => $extension
			);
		}

		// System
		$system = array();

		if ($this->user->hasPermission('access', 'setting/setting')) {
			$system[] = array(
				'name'	   => $this->language->get('text_setting'),
				'href'     => $this->url->link('setting/setting', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}
		if ($this->user->hasPermission('access', 'setting/pagecache')) {
			$system[] = array(
				'name'	   => 'Cache',
				'href'     => $this->url->link('setting/pagecache', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}
		$user = array();

		if ($this->user->hasPermission('access', 'user/user')) {
			$user[] = array(
				'name'	   => $this->language->get('text_users'),
				'href'     => $this->url->link('user/user', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}

		if ($this->user->hasPermission('access', 'user/user_permission')) {
			$user[] = array(
				'name'	   => $this->language->get('text_user_group'),
				'href'     => $this->url->link('user/user_permission', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}

		if ($user) {
			$system[] = array(
				'name'	   => $this->language->get('text_users'),
				'href'     => '',
				'children' => $user
			);
		}
		$localisation = array();

		if ($this->user->hasPermission('access', 'catalog/dictionary')) {
			$localisation[] = array(
				'name'	   => 'Từ điển',
				'href'     => $this->url->link('catalog/dictionary', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}

		if ($this->user->hasPermission('access', 'localisation/language')) {
			$localisation[] = array(
				'name'	   => $this->language->get('text_language'),
				'href'     => $this->url->link('localisation/language', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}

		if ($this->user->hasPermission('access', 'localisation/currency')) {
			$localisation[] = array(
				'name'	   => $this->language->get('text_currency'),
				'href'     => $this->url->link('localisation/currency', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}

		if ($localisation) {
			$system[] = array(
				'name'	   => $this->language->get('text_localisation'),
				'href'     => '',
				'children' => $localisation
			);
		}

		if ($this->user->hasPermission('access', 'tool/error_log')) {
			$system[] = array(
				'name'	   => $this->language->get('text_error_log'),
				'href'     => $this->url->link('tool/error_log', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}

		if ($this->user->hasPermission('access', 'tool/backup')) {
			$system[] = array(
				'name'	   => $this->language->get('text_backup'),
				'href'     => $this->url->link('tool/backup', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
		}

		if ($system) {
			$this->data['menus'][] = array(
				'id'       => 'system',
				'icon'       => 'fa-cog',
				'name'	   => $this->language->get('text_system'),
				'href'     => '',
				'children' => $system
			);
		}

		$this->template = 'common/menu.tpl';

		$this->render();
	}
}
?>
