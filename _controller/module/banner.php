<?php
class ControllerModuleBanner extends Controller {
	protected function index($setting) {
		static $module = 0;

		$this->load->model('design/banner');
		$this->load->model('tool/image');
		$this->data['banners'] = array();
		$results = $this->model_design_banner->getBanner($setting['banner_id']);

		foreach ($results as $result) {
			if (file_exists(DIR_IMAGE . $result['image'])) {
				$this->data['banners'][] = array(
					'title' => $result['title'],
					'link'  => $result['link'],
					'image'  => 'media/' . $result['image'],
					'thumb' => $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height'])
				);
			}
		}

		$this->data['banner_title'] = $this->config->get('banner_title')[$this->config->get('config_language_id')];
    	$this->data['banner_description'] = $this->config->get('banner_description')[$this->config->get('config_language_id')];

		if(isset($this->request->get['route'])){
			$this->data['route'] = $this->request->get['route'];
		}else{
			$this->data['route'] = "common/home";
		}

		// $this->document->addStyle('https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css');
		// $this->document->addScript('https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js');
		// $exScript = "
		// 	$('.owl-carousel').owlCarousel({
		// 	    loop:true,
		// 	    margin:10,
		// 	    responsiveClass:true,
		// 	    autoplay:true,
		// 	    autoplayTimeout:3000,
		// 	    autoplayHoverPause:true,
		// 	    responsive:{
		// 	        0:{
		// 	            items:2,
		// 	            nav:true
		// 	        },
		// 	        600:{
		// 	            items:3,
		// 	            nav:false
		// 	        },
		// 	        1000:{
		// 	            items:4,
		// 	            nav:true,
		// 	            loop:false
		// 	        }
		// 	    }
		// 	})
		// ";
		// $this->document->addExScript($exScript);

		$this->data['module'] = $module++;
		$this->template = 'module/banner.tpl';
		$this->render();
	}
}
?>
