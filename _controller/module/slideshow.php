<?php
class ControllerModuleSlideshow extends Controller {
	protected function index($setting) {
		static $module = 0;
		$this->load->model('design/banner');
		$this->load->model('tool/image');
		$this->data['banners'] = array();
		$this->data['text_explore'] = $this->language->get('text_explore');
		$this->data['text_scroll'] = $this->language->get('text_scroll');
		$this->data['button_view_details'] = $this->language->get('button_view_details');
		$speed = $setting['speed'];
		if($setting['auto']=='1'){
			$auto = 'true';
		}else{
			$auto = 'false';
		}

		if (isset($setting['banner_id'])) {
			$results = $this->model_design_banner->getBanner($setting['banner_id']);

			foreach ($results as $result) {
				if (file_exists(DIR_IMAGE . $result['image'])) {
					$this->data['banners'][] = array(
						'title' => html_entity_decode($result['title'], ENT_QUOTES, 'UTF-8'),
						'description' => $result['description'],
						'link'  => $result['link'],
						'readmore' => $result['readmore'],
						'image' => strpos($result['image'], '.gif') ? 'media/'.$result['image'] : $this->model_tool_image->cropsize($result['image'], $setting['width'], $setting['height']),
						'crop' => $this->model_tool_image->cropsize($result['image'], $setting['width'], $setting['height']),
						'small' => $this->model_tool_image->cropsize($result['small'], 640, 480)
					);
				}
			}
		}

		// $this->document->addScript('/static/css/swiper.min.js');
		$script = "";
		// $this->document->addExScript($script);
		// $this->document->addStyle('/static/css/swiper.min.css');

		// $this->document->addScript('https://cdnjs.cloudflare.com/ajax/libs/slider-pro/1.5.0/js/jquery.sliderPro.min.js');
		$this->document->addStyle('css/owl.carousel.min.css');
		$this->document->addStyle('css/owl.theme.css');
		// $script = "
		// 	$('.slider-pro').sliderPro({
		// 	  fade: true,
		// 	  width: '100%',
		// 	  autoHeight: true,
		// 	  arrows: true,
		// 	  autoplay: ". $auto .",
		// 	  autoplayDelay: '". $speed . "',
		// 	  buttons: false,
		// 	  animationSpeed: 300
		// 	});
		// ";
		// $this->document->addExScript($script);
		$this->data['module'] = $module++;

		$this->template = 'module/slideshow.tpl';

		$this->render();
	}
}
?>
