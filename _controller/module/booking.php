<?php
class ControllerModuleBooking extends Controller {
	protected function index($setting) {
		$this->language->load('module/booking');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['position'] = !empty($setting['position']) ? $setting['position'] :'';
		$this->data['key_map'] = 'AIzaSyCAsAB47nPnc92FCSYKoVfWFGM_ysAGKCI';
		
		$this->load->model('catalog/type_car');
		
		$this->data['cars'] = $this->model_catalog_type_car->getTypeCars(array());
		// print_r($this->data['cars']);
		// print_r($this->session->data['form']);
		
		$this->template = 'module/booking.tpl';
		$this->render();
	}
	
	public function distance(){
		$origins = str_replace(" ","+",$this->request->post['origins']);
		$destinations = str_replace(" ","+",$this->request->post['destinations']);
		// $origins = 'Hồng+Thuận';
		// $destinations = 'Giao+Lac';

		$url = "https://maps.googleapis.com/maps/api/distancematrix/json?key=AIzaSyCAsAB47nPnc92FCSYKoVfWFGM_ysAGKCI&origins=" .$origins. "&destinations=" .$destinations;
		// echo $url;
		ini_set("allow_url_fopen", 1);
		$json = file_get_contents($url);
		$obj = json_decode($json, true);
		
		// $this->session->data['form'] = $this->request->post;
		
		$this->load->model('catalog/type_car');
		
		$this->data['cars'] = $this->model_catalog_type_car->getTypeCars(array());
		
		$status = $obj['status'];
		// print_r($status);
		$json = array();
		if($status == 'OK'){
			$check = $obj['rows'][0]['elements'][0]['status'];
			$distance = ($check=='OK') ? $obj['rows'][0]['elements'][0]['distance']['text'] : 0; 
			$value = ($check=='OK') ? $obj['rows'][0]['elements'][0]['distance']['value'] : 0;
			$type_car = $this->request->post['type_car'];
			$text_cost = 'cost_' . $this->request->post['transfer_type'];
			$unitPrice = $this->model_catalog_type_car->getTypeCar($this->request->post['type_car'])[$text_cost];
			$price = (int)$unitPrice * round($value/1000, 2);
			
			if($this->request->post['transfer_type'] == 'tinh' && $this->request->post['method'] == '2'){
				$price = $price + $price*0.3;
			}elseif($this->request->post['transfer_type'] == 'tienchuyen' && $this->request->post['method'] == '2'){
				$price = $price + $price;
			}
			
			$json = array(
				'destination' => $obj['destination_addresses'][0],
				'origin' => $obj['origin_addresses'][0],
				'distance' => $distance,
				'value' => $value,
				'price' => number_format($price,0,",",".") . ' vnđ',
				'status' => $check
			);
			$this->session->data['booking']['destination'] = $json['destination'];
			$this->session->data['booking']['origin'] = $json['origin'];
			$this->session->data['booking']['distance'] = $json['distance'];
			$this->session->data['booking']['price'] = $price;
			$this->session->data['booking']['time'] = empty($this->request->post['time']) ? '' : $this->request->post['time'];
			$this->session->data['booking']['type_car'] = empty($this->request->post['type_car']) ? '' : $this->request->post['type_car'];
			$this->session->data['booking']['transfer_type'] = empty($this->request->post['transfer_type']) ? '' : $this->request->post['transfer_type'];
			$this->session->data['booking']['method'] = empty($this->request->post['method']) ? '' : $this->request->post['method'];
		}else{
			$json = array(
				'status' => $status
			);
		}
		
		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
        // $this->response->setOutput($json);
	}
	
	public function confirm(){
		$json = array();
		$this->data['booking'] = $this->session->data['booking'];
		$this->load->model('catalog/type_car');
		
		$this->data['cars'] = $this->model_catalog_type_car->getTypeCars(array());
		$this->template = 'module/booking_confirm.tpl';
		$json['html'] = $this->render();
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function booking(){
		$json = array();
		$json['err'] = array();

		$servername = "localhost";
		$username = "vipsedan_2018";
		$password = "Hung@1993";
		$dbname = "vipsedan_2018";

		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);

		// Check connection
		if ($conn->connect_error) {
		    $json['error'] = 'Lỗi kết nối dữ liệu';
		    die("Connection failed: " . $conn->connect_error);
		} 
		mysqli_set_charset($conn,"utf8");
		$json['db'] = $this->request->post;
		
		if(empty($this->request->post['telephone'])){
			$json['err']['telephone'] = 'Quý khách vui lòng nhập số điện thoại.';
		}

		$sql = "INSERT INTO se_booking SET customer = '". $this->request->post['name'] ."', telephone = '". $this->request->post['telephone'] ."', origin = '". $this->request->post['origins'] ."', method = '". $this->request->post['method'] ."', transfer_type = '". $this->request->post['transfer_type'] ."', destination = '". $this->request->post['destinations'] ."', distance = '". $this->request->post['distance'] ."', money = '". $this->request->post['price'] ."', note = '". $this->request->post['note'] ."', type_car = '". (int)$this->request->post['type_car'] ."', date_execute = '". $this->request->post['time'] ."', date_added = NOW()";

		if ($conn->query($sql) === TRUE) {
		    $json['success'] = "Đặt xe thành công. Chúng tôi sẽ liên hệ với quý khách trong thời gian gần nhất";
				$subject = "[Đặt chuyến vipsedan] Khách hàng " . $this->request->post['name'];
				$content = "";
				$content .= "Điểm đón: ". $this->request->post['origins'] . "\r\n";
				$content .= "Điểm đến: ". $this->request->post['destinations'] . "\r\n";
				$content .= "Thời gian: ". $this->request->post['time'] . "\r\n";
				$content .= $this->request->post['transfer_type'] . " - " . $this->request->post['method'] . " chiều\r\n";
				$content .= "Loại xe: ". $this->request->post['type_car'] . "\r\n";
				$content .= "Tên người liên hệ: ". $this->request->post['name'] . "\r\n";
				$content .= "Điện thoại: " . $this->request->post['telephone'] ."\r\n";
				$content .= "Ghi chú: " . $this->request->post['note'] ."\r\n \r\n";
				$mail = new Mail();
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->hostname = $this->config->get('config_smtp_host');
				$mail->username = $this->config->get('config_smtp_username');
				$mail->password = $this->config->get('config_smtp_password');
				$mail->port = $this->config->get('config_smtp_port');
				$mail->timeout = $this->config->get('config_smtp_timeout');
				// $mail->setTo($this->config->get('config_email'));
				$mail->setTo('nguyenhung537@gmail.com');
				if(!empty($this->config->get('config_mail_from'))){
					$mail->setFrom($this->config->get('config_mail_from'));
				}else{
					$mail->setFrom($this->config->get('config_email'));
				}
				$mail->setReplyTo($this->config->get('config_email'));
				$mail->setSender('VipSedan.vn');
				$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
				$mail->setText(strip_tags(html_entity_decode($content, ENT_QUOTES, 'UTF-8')));
				$mail->send();
		} else {
		    $json['error'] = "Error: " . $sql . "<br>" . $conn->error;
		}

		$conn->close();

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
?>