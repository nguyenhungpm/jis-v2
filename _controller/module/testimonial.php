<?php  
class ControllerModuletestimonial extends Controller {

	protected function index($setting) {
		$this->language->load('module/testimonial');

		$this->data['testimonial_title'] = html_entity_decode($setting['testimonial_title'][$this->config->get('config_language_id')], ENT_QUOTES, 'UTF-8');
		$this->data['testimonial_content'] = html_entity_decode($setting['testimonial_content'][$this->config->get('config_language_id')], ENT_QUOTES, 'UTF-8');

      	$this->data['heading_title'] = $this->language->get('heading_title');
      	$this->data['text_more'] = $this->language->get('text_more');
      	$this->data['text_more2'] = $this->language->get('text_more2');
		$this->data['isi_testimonial'] = $this->language->get('isi_testimonial');
		$this->data['show_all'] = $this->language->get('show_all');
		$this->data['showall_url'] = $this->url->link('product/testimonial', '', 'SSL'); 
		$this->data['more'] = $this->url->link('product/testimonial', 'testimonial_id=' , 'SSL'); 
		$this->data['isitesti'] = $this->url->link('product/isitestimonial', '', 'SSL');

		$this->load->model('catalog/testimonial');
		
		$this->data['testimonials'] = array();
		
		if(isset($this->request->get['route'])){
			$this->data['route'] = $this->request->get['route'];
		}else{
			$this->data['route'] = "common/home";
		}
		
		$this->data['total'] = $this->model_catalog_testimonial->getTotalTestimonials();
		$results = $this->model_catalog_testimonial->getTestimonials(0, $setting['testimonial_limit'], (isset($setting['testimonial_random']))?true:false);
		foreach ($results as $result) {
			
			
			$result['description'] = $result['description'];

			if (!isset($setting['testimonial_character_limit']))
				$setting['testimonial_character_limit'] = 0;

			if ($setting['testimonial_character_limit']>0)
			{
				$lim = $setting['testimonial_character_limit'];

				if (mb_strlen($result['description'],'UTF-8')>$lim) 
					$result['description'] = mb_substr($result['description'], 0, $lim-3, 'UTF-8'). ' ' .'<a href="'.$this->data['more']. $result['testimonial_id'] .'" title="'.$this->data['text_more2'].'">'. $this->data['text_more'] . '</a>';

			}
			$this->data['testimonials'][] = array(
				'id'			=> $result['testimonial_id'],											  
				'title'			=> $result['title'],
				'description'	=> $result['description'],
				'name1'			=> $result['name'],
				'date_added'	=> $result['date_added'],
				'city'			=> $result['city']

			);
		}

		$this->document->addScript('static/lightslider.js');
		$script = "
			$(document).ready(function(){
				var slider =$('#Carousel').lightSlider({
					item:3,
					pause:3000,
					slideMargin:30,
					slideMove:2,
					auto:true,
			        loop:true,
			        pauseOnHover:true,
			        pager:true,
			        controls: false,
			        responsive : [
			            {
			                breakpoint:800,
			                settings: {
			                    item:2,
			                    slideMove:1,
			                    slideMargin:6,
			                  }
			            }
			        ]
				});
			 });
		";
		$this->document->addExScript($script);
		$this->id = 'testimonial';
		$this->template = 'module/testimonial.tpl';
		$this->render();
	}
}
?>