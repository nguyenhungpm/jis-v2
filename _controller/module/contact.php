<?php
class ControllerModuleContact extends Controller {
	public function index($data) {
		$this->language->load('module/contact');
      	$this->data['heading_title'] = $this->language->get('heading_title');
      	$this->data['error_email'] = $this->language->get('error_email');
      	$this->data['error_tel'] = $this->language->get('error_tel');
      	$this->data['error_name'] = $this->language->get('error_name');
      	$this->data['error_captcha'] = $this->language->get('error_captcha');
      	$this->data['text_submit'] = $this->language->get('text_submit');
      	$this->data['text_name'] = $this->language->get('text_name');
      	$this->data['text_tel'] = $this->language->get('text_tel');
      	$this->data['text_email'] = $this->language->get('text_email');
      	$this->data['text_content'] = $this->language->get('text_content');
      	$this->data['text_captcha'] = $this->language->get('text_captcha');
      	$this->data['text_success'] = $this->language->get('text_success');
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/contact.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/contact.tpl';
		} else {
			$this->template = 'default/template/module/contact.tpl';
		}

		$this->render();
	}
	
	public function add() {
		
        $json = '';
		
        if (empty($this->request->post['order_name'])) {
            $json .= '1,';
        }

        // if (empty($this->request->post['order_tel'])) {
            // $json .= '3,';
        // }

        if ((utf8_strlen($this->request->post['order_email']) > 96) || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['order_email']) || empty($this->request->post['order_email'])) {
            $json .= '5,';
        }
		
		if (empty($this->session->data['captcha']) || ($this->session->data['captcha'] != $this->request->post['captcha'])) {
			$json .= '7,';
		}
		
		if($json==''){
			$message = '';
			
			if($this->request->post['order_name']){
				$message .= '<span>Khách hàng: '. $this->request->post['order_name'] .'</span><br />';
			}
			
			// if($this->request->post['order_tel']){
				// $message .= '<span>Điện thoại: '. $this->request->post['order_tel'] .'</span><br />';
			// }
						
			if($this->request->post['order_email']){
				$message .= '<span>Email: '. $this->request->post['order_email'] .'</span><br />';
			}
			
			if($this->request->post['order_content']){
				$message .= '<span>Nội dung: '. $this->request->post['order_content'] .'</span><br />';
			}
			
			// if($this->request->post['product_id']){
				// $this->load->model('catalog/product');
				// $name = $this->model_catalog_product->getProduct($this->request->post['product_id'])['name'];
				// $message .= '<span>Sản phẩm: '. $name .'</span><br />';
			// }
			
			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
			$mail->setTo($this->config->get('config_email'));
			$mail->setFrom('no-reply@minmobile.net');
			$mail->setSender(html_entity_decode($this->request->post['order_name'], ENT_QUOTES, 'UTF-8'));
			$mail->setSubject(html_entity_decode('Đặt hàng trước', ENT_QUOTES, 'UTF-8'));
			$mail->setHtml($message);
			$mail->send();
		}

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
}
?>