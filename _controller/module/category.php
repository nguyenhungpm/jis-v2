<?php  
class ControllerModuleCategory extends Controller {
	protected function index($setting) {
		$this->language->load('module/category');

		$this->data['heading_title'] = $this->language->get('heading_title');

		if (isset($this->request->get['path'])) {
			$parts = explode('_', (string)$this->request->get['path']);
		} else {
			$parts = array();
		}
		
		$this->data['position'] = $setting['position'];
		
		$this->load->model('catalog/category');
		$this->load->model('catalog/product');
		$this->data['categories'] = array();

		$categories = $this->model_catalog_category->getCategories();

		foreach ($categories as $category) {
		
			$children_data = array();
			$products = array();

			$children = $this->model_catalog_category->getCategories($category['category_id']);

			foreach ($children as $child) {
				$data = array(
					'filter_category_id'  => $child['category_id'],
					'filter_sub_category' => true
				);

				$children_data[] = array(
					'category_id' => $child['category_id'],
					'name'        => $child['name'],
					'href'        => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])	
				);		
			}

			$data_p = array(
				'filter_category_id' =>$category['category_id'],
				'sort'  => 'p.date_avaiable',
				'order' => 'DESC',
				'start' => 0,
				'limit' => 6
			);

			$results = $this->model_catalog_product->getProducts($data_p);
			foreach ($results as $result) {

				if ($result['image']) {
					$image = $this->model_tool_image->cropsize($result['image'], 100, 66);
				} else {
					$image = false;
				}

				$products[] = array(
					'product_id' => $result['product_id'],
					'thumb'   	 => $image,
					'name'    	 => $result['name'],
					'href'    	 => $this->url->link('product/product', 'product_id=' . $result['product_id']),
				);
			}
			

			$this->data['categories'][] = array(
				'category_id' => $category['category_id'],
				'icon'        => $category['icon'],
				'name'        => $category['name'],
				'children'    => $children_data,
				'products'    => $products,
				'href'        => $this->url->link('product/category', 'path=' . $category['category_id'])
			);	
		}

		$this->template = 'module/category.tpl';

		$this->render();
	}
}
?>