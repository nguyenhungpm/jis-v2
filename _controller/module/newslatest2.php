<?php
class ControllerModulenewslatest2 extends Controller {
	protected function index($setting) {
		$this->language->load('module/newslatest2');
		
      	$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['button_cart'] = $this->language->get('button_cart');
		
		$this->data['position'] = $setting['position'];
		$this->data['featured'] = $setting['featured'];
		$this->data['newcathref'] = $this->url->link('news/news_category', 'catid=' . $setting['catid']);
				
		$this->load->model('catalog/news');
		
		$this->load->model('tool/image');

		$this->load->model('tool/t2vn');
		
		$this->data['newss'] = array();
		
		$data = array(
			'sort'  => 'n.date_available',
			'order' => 'DESC',
			'start' => 0,
			'limit' => $setting['limit']
		);

		$query = $this->db->query("SELECT date_added FROM ever_news_clone ORDER BY date_added DESC LIMIT 1");
		
		if(!empty($query->row['date_added']) && (strtotime(date("Y-m-d H:i:s"))-strtotime($query->row['date_added'])) < (3*24*60*60)){
			
		}else{
			include_once(DIR_SYSTEM . 'library/domxml/simple_html_dom.php');

			$html = file_get_html('http://www.nielsen.com/vn/vi/insights.html?pageNum=1');

			foreach($html->find('h2 a') as $element) {
				if (strpos(strtolower($element->plaintext), strtolower('ThươNg Hiệu')) !== false && empty($this->db->query("SELECT name FROM ever_news_clone WHERE name = '". $this->db->escape($element->plaintext) ."'")->row['name'])) {
					$this->db->query("INSERT INTO ever_news_clone SET name='". $this->db->escape($element->plaintext) ."', href='http://www.nielsen.com". $this->db->escape($element->href) ."', date_added=NOW()");
				}
			}
		}
		
		$query2 = $this->db->query("SELECT * FROM ever_news_clone ORDER BY date_added DESC LIMIT " . $setting['limit']);
		
		$this->data['list'] = $query2->rows;

		$this->template = 'module/newslatest2.tpl';

		$this->render();
	}
	function catchFirstImage($content) {
		  $first_img = ''; 
		  $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $content, $matches);
		  if(isset($matches[1][0])){
			$first_img = $matches[1][0];
			}
		  if(empty($first_img)){ //Defines a default image
			$first_img = "no_image.jpg";
		  }
		  return $first_img;
		} //end function	
}
?>