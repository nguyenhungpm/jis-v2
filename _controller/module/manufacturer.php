<?php
class ControllerModuleManufacturer extends Controller {
	protected function index($setting) {
		static $module = 0;

	    $this->data['heading_title'] = $setting['heading_title'];
		$this->load->model('catalog/manufacturer');
		$this->load->model('tool/image');
		$this->data['loiichs'] = array();


		$manufacturers = $this->model_catalog_manufacturer->getManufacturers();

		if($manufacturers){
			foreach ($manufacturers as $result) {
				if ($result['image']) {
					// $image = 'image/'.$result['image'];
					$image = $this->model_tool_image->cropsize($result['image'], $setting['image_width'], $setting['image_height']);
				}else {
					$image = false;
				}
				$man_description = $this->model_catalog_manufacturer->getManufacturerDescription($result['manufacturer_id']);
				$description = $man_description['description'];
				$description = html_entity_decode($description,ENT_QUOTES, 'UTF-8');
				$this->data['loiichs'][] = array(
					'thumb'   	 => $image,
					'name'    	 => $result['name'],
					'description' => $description
				);
			}
		}
		$this->data['module'] = $module++;
		$this->template = 'module/manufacturer.tpl';
		$this->render();
	}
}
?>
