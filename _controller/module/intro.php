<?php
class ControllerModuleIntro extends Controller {
	protected function index($setting) {
		$this->document->addStyle('/static/css/program.css');
		$this->data['heading_title'] 	= $setting['heading_title'];
		$this->data['content'] 			= html_entity_decode($setting['content'], ENT_QUOTES, 'UTF-8');
		$this->data['intro_link'] 			= $setting['link'];
		$this->data['button'] 			= $setting['button'];
		// $this->data['category_id'] 			= $setting['category_id'];

		$this->load->model('tool/image');
		$this->load->model('catalog/information');

		$informations = $this->model_catalog_information->getSpecInformations();

		$this->data['informations'] = array();
		foreach($informations as $info) {
			// print_r($this->model_catalog_information->getInformationHighlights($info['information_id']));
			$this->data['informations'][] = array(
				'information_id' => $info['information_id'],
				'robots' => !empty($info['robots']) ? str_replace(" # ", " #", str_replace(";", " #", $info['robots'])) : $info['title'],
				'title' => $info['title'],
				'code_youtube' => $info['code_youtube'],
				'href' => $this->url->link('information/information', 'information_id=' . $info['information_id']),
				'highlight' => $this->model_catalog_information->getInformationHighlights($info['information_id'])
			);
		}

		// print_r($this->data['informations']);

		$this->data['intro_image'] = 'media/'.$setting['intro_image'];
		$this->template = 'module/intro.tpl';
		$this->render();
	}
}
?>
