<?php
class ControllerModuleLatest extends Controller {
	protected function index($setting) {
		$this->language->load('module/latest');

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['button_cart'] = $this->language->get('button_cart');
		$this->data['text_readmore'] = $this->language->get('text_readmore');
				
		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$this->data['products'] = array();

		$data = array(
			'sort'  => 'p.total_rate',
			'order' => 'DESC',
			'start' => 0,
			'limit' => $setting['limit']
		);

		$results = $this->model_catalog_product->getProducts($data);
		foreach ($results as $result) {
			if ($result['image']) {
				$image = $this->model_tool_image->cropsize($result['image'], $setting['image_width'], $setting['image_height']);
			} else {
				$image = false;
			}


			$this->data['products'][] = array(
				'product_id' => $result['product_id'],
				'thumb'   	 => $image,
				'name'    	 => $result['name'],
				'total_rate' => $result['total_rate'],
				'href'    	 => $this->url->link('product/product', 'product_id=' . $result['product_id']),
			);
		}


		$this->template = 'module/latest.tpl';


		$this->render();
	}
}
?>
