<?php
class ControllerModuleSupportOnline extends Controller {
	protected function index($setting) {
              $this->language->load('module/supportonline');
		static $module = 0;

		$this->data['width'] = $setting['image_width'];
		$this->data['height'] = $setting['image_height'];
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_phone'] = $this->language->get('text_phone');
		// image
		$this->load->model('tool/image');

		$this->data['images'] = array();

		$image_avatar = array();

		$image_avatar = $this->config->get('image_avatar');

		foreach ($image_avatar as $image_avatar) {
			if($image_avatar['image']){
				$image = $this->model_tool_image->cropsize($image_avatar['image'], $setting['image_width'], $setting['image_height']);
			}  else {
			   $image =$this->model_tool_image->cropsize( 'no_image.jpg', $setting['image_width'], $setting['image_height']);
			}

			$this->data['images'][] = array(
				'name'           => $image_avatar['name'],
				'phone'           => $image_avatar['phone'],
				'email'           => $image_avatar['email'],
				'facebook'           => $image_avatar['facebook'],
				'skype'           => $image_avatar['skype'],
				'avatar'           => $image
			);
		}

		$this->document->addScript('static/sticky.js');
		$script ="
			$(document).ready(function() {
				var width = $(window).width();
				if(width>768){
					 $.stickysidebarscroll('#sidebar',{offset: {top:55, bottom:400}});
				}
			});
		";
		$this->document->addExScript($script);
		$this->data['module'] = $module++;
		$this->template = 'module/supportonline.tpl';

		$this->render();
	}
}
?>
