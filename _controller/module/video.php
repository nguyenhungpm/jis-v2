<?php
class ControllerModuleVideo extends Controller
{
    protected function index($setting)
    {
        $this->language->load('module/video');

        $this->data['heading_title'] = $setting['title'];

        $this->data['position'] = $setting['position'];

        $this->load->model('catalog/video');

        $this->load->model('tool/image');

        $this->load->model('tool/t2vn');

        $this->data['video_hashtag'] = $this->config->get('video_hashtag');
        $this->data['video_featured'] = $this->config->get('video_featured');
        $this->data['video_title'] = $this->config->get('video_title')[$this->config->get('config_language_id')];
        $this->data['video_description'] = html_entity_decode($this->config->get('video_description')[$this->config->get('config_language_id')]);

        $results = $this->model_catalog_video->getVideos();
        $this->data['videos'] = array();
        foreach ($results as $rs) {
            $this->data['videos'][] = array(
                'src' => "//www.youtube.com/embed/" . $rs['url'] . "?showinfo=0",
                'name' => $rs['title'],
                'description' => html_entity_decode($rs['description']),
                'url' => $rs['url'],
                'video_id' => $rs['video_id'],
            );
        }
        // print_r($this->data['videos']);

        $script = "
			$('.panel.panel-default.price').hover(
       function(){ $(this).addClass('featured') },
       function(){ $('.featured').removeClass('featured') },
       function(){ $(this).addClass('featured') }
			)
		";
        $this->document->addExScript($script);

        $this->template = 'module/video.tpl';

        $this->render();
    }
}
