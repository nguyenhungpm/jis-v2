<?php
class ControllerModulenewsfeatured extends Controller
{
    protected function index($setting)
    {
        $this->language->load('module/newsfeatured');

        $this->data['heading_title'] = $this->language->get('heading_title');
        $this->data['text_viewmore'] = $this->language->get('text_viewmore');

        $this->data['button_cart'] = $this->language->get('button_cart');

        $this->data['position'] = $setting['position'];

        $this->load->model('catalog/news');

        $this->load->model('tool/image');

        $this->load->model('tool/t2vn');

        $this->data['newss'] = array();

        $newss = array();
        $date_expired = date(strtotime($this->config->get('newsfeatured_expired')));
        $this->data['newsfeatured_title'] = $this->config->get('newsfeatured_title')[$this->config->get('config_language_id')];
        $this->data['newsfeatured_description'] = $this->config->get('newsfeatured_description')[$this->config->get('config_language_id')];

        if (empty($setting['limit'])) {
            $setting['limit'] = 5;
        }

        if ((time() - (60 * 60 * 24)) < strtotime($this->config->get('newsfeatured_expired'))) {
            $newss = explode(',', $this->config->get('newsfeatured_news'));
            $newss = array_slice($newss, 0, (int) $setting['limit']);
        } else {
            $data = array(
                'sort' => 'n.date_available',
                'order' => 'DESC',
                'start' => 0,
                'limit' => $setting['limit'],
            );
            $results = $this->model_catalog_news->getNews($data);
            foreach ($results as $rs) {
                $newss[] = $rs['news_id'];
            }
        }

        foreach ($newss as $key => $news_id) {
            $news_info = $this->model_catalog_news->getNew($news_id);

            if ($news_info) {
                $width = '';
                $height = '';

                if ($news_info['image'] && $setting['imagestatus'] == 1) {
                    $image = $this->model_tool_image->cropsize($news_info['image'], $setting['image_width'], $setting['image_height']);
                    $imagesmall = $this->model_tool_image->cropsize($news_info['image'], $setting['image_width'] / 2, $setting['image_height'] / 2);
                } elseif ($setting['imagestatus'] == 1) {
                    $firstImgNews = $this->catchFirstImage(html_entity_decode($news_info['description'], ENT_QUOTES, 'UTF-8'));
                    if ($firstImgNews == 'no_image.jpg') {
                        $image = $this->model_tool_image->cropsize('no_image.jpg', $setting['image_width'], $setting['image_height']);
                        $imagesmall = $this->model_tool_image->cropsize('no_image.jpg', $setting['image_width'] / 2, $setting['image_height'] / 2);
                    } else {
                        $image = $firstImgNews;
                        $width = 'width="' . $setting['image_width'] . '"';
                        $height = 'height="' . $setting['image_height'] . '"';
                    }
                } else {
                    $image = false;
                    $imagesmall = false;
                }

                $limit_string = $key == 0 ? 500 : $setting['limitdescription'];
                if ($news_info['short_description'] && $setting['description'] == 1) {
                    $short_description = $this->model_tool_t2vn->cut_string(html_entity_decode($key == 0 ? $news_info['description'] : $news_info['short_description'], ENT_QUOTES, 'UTF-8'), $limit_string);
                } else {
                    $short_description = $this->model_tool_t2vn->cut_string(html_entity_decode($key == 0 ? $news_info['description'] : $news_info['description'], ENT_QUOTES, 'UTF-8'), $limit_string);
                    // $short_description = false;
                }

                if ($this->config->get('config_comment_status')) {
                    $rating = $news_info['rating'];
                } else {
                    $rating = false;
                }

                $this->data['newss'][] = array(
                    'news_id' => $news_info['news_id'],
                    'thumb' => $image,
                    'sthumb' => $imagesmall,
                    'name' => $news_info['name'],
                    'date_added' => date($this->language->get('date_format_month'), strtotime($news_info['date_available'])),
                    'short_description' => $short_description,
                    'rating' => $rating,
                    'image' => $news_info['image'],
                    'width' => $width,
                    'height' => $height,
                    'comments' => sprintf($this->language->get('text_comments'), (int) $news_info['comment']),
                    'href' => $this->url->link('news/news', 'news_id=' . $news_info['news_id']),
                );
            }
        }
        $this->template = 'module/newsfeatured.tpl';
        $this->render();
    }
    public function catchFirstImage($content)
    {
        $first_img = '';
        $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $content, $matches);
        if (isset($matches[1][0])) {
            $first_img = $matches[1][0];
        }
        if (empty($first_img)) { //Defines a default image
            $first_img = "no_image.jpg";
        }
        return $first_img;
    } //end function
}
