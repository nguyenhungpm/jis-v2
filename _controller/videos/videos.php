<?php 
class ControllerVideosVideos extends Controller {  
	public function index() { 
		$this->language->load('videos/videos');
        $this->document->addStyle('static/css/tuyensinh.css');
        $this->document->addStyle('static/css/recruitment.css');
        $this->document->addStyle('https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.12/css/lightgallery.min.css');
        // $this->document->addScript('https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js');
        $this->document->addScript('https://cdn.jsdelivr.net/npm/lightgallery@1.6.12/dist/js/lightgallery.min.js');
        $this->document->addScript('https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js');
        $this->document->addScript('https://cdnjs.cloudflare.com/ajax/libs/lg-thumbnail/1.1.0/lg-thumbnail.min.js');
        $this->document->addScript('https://cdnjs.cloudflare.com/ajax/libs/lg-fullscreen/1.1.0/lg-fullscreen.min.js');
        $this->document->addScript('https://cdnjs.cloudflare.com/ajax/libs/lg-video/1.4.0/lg-video.js');

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->load->model('catalog/video');
		$this->load->model('catalog/product');
		$this->load->model('tool/image');
		$this->load->model('design/banner');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->data['products'] = array();

        $results = $this->model_catalog_product->getOtherProduct(array('product_id' => 310, 'limit' => '15'));

        foreach ($results as $result) {
            if ($result['image']) {
                $image =  $this->model_tool_image->resize($result['image'], $this->config->get('config_image_related_width'), $this->config->get('config_image_related_height'));
            } else {
                $image =  $this->model_tool_image->resize('no_image.jpg', $this->config->get('config_image_related_width'), $this->config->get('config_image_related_height'));
            }

            if ($result['price']>0){
                $price = number_format($result['price'],0,'',' ') . ' đ';
            } else {
                $price = $this->language->get('call_for_price');
            }

            $this->data['products'][] = array(
                'product_id' => $result['product_id'],
                'price' => $price,
                'date_added' => $result['date_available'] != '0000-00-00 00:00:00' ? date('d-m-Y h:i', strtotime($result['date_available'])) : date('d-m-Y h:i', strtotime($result['date_added'])),
                'thumb' => $image,
                'name' => $result['name'],
                'href' => $this->url->link('product/product', 'product_id=' . $result['product_id'])
            );
        }

		$this->data['breadcrumbs'] = array();
        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home'),
            'separator' => false
         );
        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('videos/videos'),
            'separator' => $this->language->get('text_separator')
         );
		$results = $this->model_catalog_video->getVideos(null);
        // print_r($results);
		$this->data['videos'] = array();
		
		if($results){
			foreach($results as $result){
				if($result['url']){
					$thumb = "https://img.youtube.com/vi/". explode("&",$result['url'])[0] ."/0.jpg";
				}else{
					$thumb = $this->model_tool_image->resize('no_image.jpg','205','110');
				}
				
				$this->data['videos'][]= array(
					'video_id' 	=> $result['video_id'],
					'date_add' 	=> date("d/m/Y",strtotime($result['date_add'])),
					'code'          => $result['url'],
					'title'          => $result['title'],
					'description'          => html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'),
					'thumb'         => $thumb,
					'view'          => $this->url->link('videos/videos/member','&video_id=' . $result['video_id'], 'SSL')
				);
			}
		}

        $this->data['pictures'] = array();

        $sc = '';

        $pictures = $this->model_design_banner->getPictureList(array());
        // print_r($pictures);
        foreach ($pictures as $key => $value) {
            $sc .= '$("#light-image-'. $value['picture_id'] .'").lightGallery(); ';
            $pictures = $this->model_design_banner->getPicture($value['picture_id']);
            $gallery_images = array();
            foreach ($pictures as $key => $val) {
                if ($key == 0) {
                    $thumb = $this->model_tool_image->cropsize($val['image'], 400, 220);
                    $image = '/media/' . $val['image'];
                }
                $gallery_images[] = array(
                    'title' => $val['title'],
                    'description' => $val['description'],
                    'image' => '/media/' . $val['image'],
                    'thumb' => $this->model_tool_image->cropsize($val['image'], 400, 220),
                );
            }
            $this->data['pictures'][] = array(
                'picture_id' => $value['picture_id'],
                'title' => $value['name'],
                'gallery_images' => $gallery_images,
                'thumb' => $thumb,
                'image' => $image,
            );
        }

        // print_r($this->data['pictures']);
        $script = '
            $(document).ready(function() {
                $("#lightgallery").lightGallery(); ';
        $script .= $sc;
        $script .= '});';
		$this->document->addExScript($script);

        $this->template = 'video/video.tpl';
			
		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_bottom',
			'common/content_top',
			'common/slideside',
			'common/footer',
			'common/header'
		);
			
		$this->response->setOutput($this->render());										

  	}
  	
        
  	public function member() {
		$this->language->load('module/s_youtube');

        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->load->model('catalog/video');
        
        $this->model_catalog_video->updateViewed($this->request->get['video_id']);
        if (isset($this->request->get['video_id']) && !empty($this->request->get['video_id'])) {
           $video_id = $this->request->get['video_id'];
        } else {
           $video_id = 0;
        }

        $member = $this->model_catalog_video->getVideo($video_id);
	
        $this->data['breadcrumbs'] = array();
        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home'),
            'separator' => false
         );
        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('videos/videos'),
            'separator' => $this->language->get('text_separator')
         );

         if($member){
           $this->data['breadcrumbs'][] = array(
               'text' => $member['title'],
               'href' => $this->url->link('videos/videos/member', 'video_id=' . $video_id),
               'separator' => $this->language->get('text_separator')
           );
		
		//open graph
		$sitename = nl2br($this->config->get('config_name')[$this->config->get('config_language_id')]);
		
		if(strlen($member['description'])>10) {
			$value = preg_replace('/<[^<]+?>/g', ' ', $member['description']);
			$businessDesc = substr($value,0,250);
        	$this->document->setMetapros('og:description',$businessDesc);
        }
		$this->document->setMetapros('og:type', 'article');
        $this->document->setMetapros('og:title', $member['title']);
        $this->document->setMetapros('og:url', $this->url->link('videos/videos/member', 'video_id=' . $video_id));
        $this->document->setMetapros('og:image',  "http://img.youtube.com/vi/".$member['url']."/hqdefault.jpg");
        $this->document->setMetapros('og:site_name', $sitename);
		//open graph
		
         $this->document->setTitle($member['title']);
         $this->data['heading_title'] = $member['title'];
         $this->data['url'] =  $this->url->link('videos/videos/member', 'video_id=' . $video_id);
         $this->data['src'] =  "http://img.youtube.com/vi/".$member['url']."/mqdefault.jpg";
         $this->data['url_video'] = $member['url'];
         
         $this->data['description'] = html_entity_decode($member['description']);
 
         if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/video/details.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/video/details.tpl';
         } else {
            $this->template = 'default/template/video/details.tpl';
         }
         
         $this->children = array(
             'common/column_left',
             'common/column_right',
             'common/slideside',
    		 'common/content_bottom',
    		 'common/content_top',
             'common/footer',
             'common/header'
         );
 
         $this->response->setOutput($this->render());
      }
   }
}
?>