<?php

class ControllerNewsBlog extends Controller {

    public function index() {
        $this->language->load('news/blog');

        $this->load->model('catalog/news');
        $this->load->model('catalog/news_category');
        $this->load->model('tool/image');
		    $this->load->model('tool/t2vn');

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        if (isset($this->request->get['limit'])) {
            $limit = $this->request->get['limit'];
        } else {
            $limit = $this->config->get('config_news_limit');
        }

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home'),
            'separator' => false
        );

        $data = array(
            'sort' => 'n.date_available',
            'order' => 'DESC',
            'start' => ($page - 1) * $limit,
            'limit' => $limit
        );

        $this->data['news_categories'] = $this->model_catalog_news_category->getNewsCategories();
        // print_r($news_categories);

        $news_total = $this->model_catalog_news->getTotalNews($data);
        if ($news_total > 0) {

            $this->document->setTitle($this->language->get('text_news'));
			if($page > 1){
				$this->document->setTitle($this->language->get('text_news').$this->language->get('text_page').$page);
			}else{
				$this->document->setTitle($this->language->get('text_news'));
			}

            $this->data['heading_title'] = $this->language->get('text_news');

            $this->data['text_news'] = $this->language->get('text_news');
            $this->data['text_readmore'] = $this->language->get('text_readmore');
            $this->data['text_empty'] = $this->language->get('text_empty');

            $this->data['text_post_on'] = $this->language->get('text_post_on');
            $this->data['text_readmore'] = $this->language->get('text_readmore');
            $this->data['text_viewed'] = $this->language->get('text_viewed');

            $this->data['button_continue'] = $this->language->get('button_continue');

            $this->data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_news'),
                'href' => $this->url->link('news/blog'),
                'separator' => $this->language->get('text_separator')
            );

            $url = '';

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $this->data['news'] = array();

            $results = $this->model_catalog_news->getNews($data);
            $k=0;
            foreach ($results as $result) {
                $width = '';
                $height = '';
                if ($result['image']) {
                    if($k==0){
                        $image = $this->model_tool_image->cropsize($result['image'], 825, 500);
                    }else{
                        $image = $this->model_tool_image->cropsize($result['image'], $this->config->get('config_image_news_width'), $this->config->get('config_image_news_height'));
                    }
                } else {
                    $firstImgNews = $this->catchFirstImage(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'));
                    if ($firstImgNews == 'no_image.jpg') {
                        $image = $this->model_tool_image->cropsize('no_image.jpg', $this->config->get('config_image_news_width'), $this->config->get('config_image_news_height'));
                    } else {
                        $image = $firstImgNews;
                        $width = 'width="' . $this->config->get('config_image_news_width') . '"';
                        $height = 'height="' . $this->config->get('config_image_news_height') . '"';
                    }
                }

				$limit_text = 350;
				if (empty($result['short_description'])) {
					$short_description = $this->model_tool_t2vn->cut_string(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'), $limit_text);
				} else {
					$short_description = $this->model_tool_t2vn->cut_string(html_entity_decode($result['short_description'], ENT_QUOTES, 'UTF-8'), $limit_text);
				}

                $this->data['news'][] = array(
                    'news_id' => $result['news_id'],
                    'thumb' => $image,
                    'name' => $result['name'],
                    'short_description' => $short_description,
                    'date_added' => $result['date_available'] != '0000-00-00 00:00:00' ? date($this->language->get('date_format_short'), strtotime($result['date_available'])) : date($this->language->get('date_format_short'), strtotime($result['date_added'])),
                    'width' => $width,
                    'height' => $height,
                    'viewed' => $result['viewed'],
                    'href' => $this->url->link('news/news', '&news_id=' . $result['news_id'])
                );
                $k++;
            }

            $url = '';

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $url = '';

            $this->data['limits'] = array();

            $this->data['limits'][] = array(
                'text' => $this->config->get('config_news_limit'),
                'value' => $this->config->get('config_news_limit'),
                'href' => $this->url->link('news/blog', $url . '&limit=' . $this->config->get('config_news_limit'))
            );

            $this->data['limits'][] = array(
                'text' => 25,
                'value' => 25,
                'href' => $this->url->link('news/blog', $url . '&limit=25')
            );

            $this->data['limits'][] = array(
                'text' => 50,
                'value' => 50,
                'href' => $this->url->link('news/blog', $url . '&limit=50')
            );

            $this->data['limits'][] = array(
                'text' => 75,
                'value' => 75,
                'href' => $this->url->link('news/blog', $url . '&limit=75')
            );

            $this->data['limits'][] = array(
                'text' => 100,
                'value' => 100,
                'href' => $this->url->link('news/blog', $url . '&limit=100')
            );

            $url = '';

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $pagination = new Pagination();
            $pagination->total = $news_total;
            $pagination->page = $page;
            $pagination->limit = $limit;
            $pagination->text = $this->language->get('text_pagination');
            $pagination->url = $this->url->link('news/blog', $url . '&page={page}');

            $this->data['pagination'] = $pagination->render();

            $this->data['limit'] = $limit;

            $this->data['continue'] = $this->url->link('common/home');

            $this->template = 'news/blog.tpl';

            $this->children = array(
                'common/column_left',
                'common/column_right',
                'common/content_top',
                'common/content_bottom',
                'common/footer',
                'common/slideside',
                'common/header'
            );

            $this->response->setOutput($this->render());
        } else {
            $url = '';

            if (isset($this->request->get['cat_id'])) {
                $url .= '&cat_id=' . $this->request->get['cat_id'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $this->data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_error'),
                'href' => $this->url->link('news/blog', $url),
                'separator' => $this->language->get('text_separator')
            );

            $this->document->setTitle($this->language->get('text_error'));

            $this->data['heading_title'] = $this->language->get('text_error');

            $this->data['text_error'] = $this->language->get('text_error');

            $this->data['button_continue'] = $this->language->get('button_continue');

            $this->data['continue'] = $this->url->link('common/home');

            $this->template = 'error/not_found.tpl';

            $this->children = array(
                'common/column_left',
                'common/column_right',
                'common/content_top',
                'common/content_bottom',
                'common/footer',
                'common/header'
            );

            $this->response->setOutput($this->render());
        }
    }

    function catchFirstImage($content) {
        $first_img = '';
        $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $content, $matches);
        if (isset($matches[1][0])) {
            $first_img = $matches[1][0];
        }
        if (empty($first_img)) { //Defines a default image
            $first_img = "no_image.jpg";
        }
        return $first_img;
    }

//end function
}

?>
