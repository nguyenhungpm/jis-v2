<?php

class ControllerNewsNews extends Controller
{
    private $error = array();

    public function index()
    {
        $this->load->model('tool/image');
        $this->language->load('news/news');
        $this->load->model('catalog/news');
        $this->data['rate'] = $this->model_catalog_news->getRate($this->request->get['news_id']);
        if ($this->data['rate']['total_rate'] == 0) {
            $this->data['rate']['total_rate'] = 1;
            $this->data['rate']['total_point'] = 5;
        }
        // echo $this->request->get['email'];
        $this->data['average'] = round($this->data['rate']['total_point'] / $this->data['rate']['total_rate']);

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home'),
            'separator' => false
        );

        if ($this->config->get('config_amp_news')) {
            $this->document->addLink($this->url->link('news/news_amp', 'news_id=' . $this->request->get['news_id']), 'amphtml');
        }
        $this->document->addLink($this->url->link('news/news', 'news_id=' . $this->request->get['news_id']), 'canonical');
        $this->document->addStyle('https://fonts.googleapis.com/css?family=Noto+Serif:400,400i,700,700i&amp;subset=vietnamese');

        $exScript = '
          $("img").parents("p").addClass("oimg");
          $("img").parents("h6").addClass("h6img");
          $("img").parents("h5").addClass("h5img");
          $("img").parents("h4").addClass("h4img");
          $("img").parents("h3").addClass("h3img");
          $("h3 img").parents("table").addClass("tableh3img");
          $("table h3 img").parents("h3").addClass("t_h3img");
          $("img").parents("table").addClass("img_table");

          var w = window.innerWidth;
          var left = parseInt((w-1090)/2 + 195);
          console.log(left);
          var style = "margin-left: -" + left + "px;";
          $(".h3img:not(.t_h3img)").attr("style", style);
          $(".tableh3img").attr("style", style);
          // $(".h3img").style.marginLeft = left;
          $(".navbar").addClass("scroll");
          var video = document.getElementById("myVideo");
            var btn = document.getElementById("myBtn");
            function myFunction() {
                if (video.paused) {
                    video.play();
                    btn.innerHTML = "Pause";
                } else {
                    video.pause();
                    btn.innerHTML = "Play";
                }
            }
            $(document).on("ready", function () {
                $(".video-link").click(function () {
                    $(".myVideo").attr("src", $(this).attr("vidUrl"));
                    $(".overlay_video").fadeIn(500, function () {
                        $(".main-vid-box").fadeIn(500);
                        $(".close").fadeIn(500);
                    });
                    $("body").addClass("show_video");
                });

                $(".close, .overlay_video").click(function () {
                    $(".overlay_video").fadeOut(500);
                    $(".myVideo").attr("src", $(this).attr("vidUrl"));
                    $(".main-vid-box").fadeOut(500);
                    $(".close").fadeOut(500);
                    $("body").removeClass("show_video");
                });
              });

              $("nav").removeClass("scroll");

        ';
        // $this->document->addExScript($exScript);



        $this->data['breadcrumbs'][] = array(
          'text' => $this->language->get('text_news'),
          'href' => $this->url->link('news/blog', ''),
          'separator' => $this->language->get('text_separator')
        );

        // OSD with love. Edit in front_end
        $this->load->library('user');
        $this->user = new User($this->registry);

        $this->data['admin_logged'] = false;
        if ($this->user->isLogged()) {
            $this->data['admin_logged'] = true;
            $this->data['edit'] = HTTP_SERVER . "oadmin/index.php?route=catalog/news/update&token=" . $this->session->data['token'] . "&news_id=" . $this->request->get['news_id'];
            $this->data['token'] = $this->session->data['token'];
        }
        // OSD with love. Edit in front_end

        $this->load->model('catalog/news_category');

        if (isset($this->request->get['cat_id'])) {
            $cat_id = '';

            foreach (explode('_', $this->request->get['cat_id']) as $cat_id_item) {
                if (!$cat_id) {
                    $cat_id = $cat_id_item;
                } else {
                    $cat_id .= '_' . $cat_id_item;
                }

                $news_category_info = $this->model_catalog_news_category->getNewsCategory($cat_id_item);
                

                if ($news_category_info) {
                    $this->data['breadcrumbs'][] = array(
                        'text' => $news_category_info['name'],
                        'href' => $this->url->link('news/news_category', 'cat_id=' . $cat_id),
                        'separator' => $this->language->get('text_separator')
                    );
                }
            }
        } elseif (isset($this->request->get['news_id'])) {
            $news_category = $this->model_catalog_news->getNewsCategories($this->request->get['news_id']);
            if (isset($news_category) && !empty($news_category)) {
                if (count($news_category) > 1) {
                    $news_category_info = $this->model_catalog_news_category->getNewsCategory($news_category[1]['news_category_id']);
                } else {
                    $news_category_info = $this->model_catalog_news_category->getNewsCategory($news_category[0]['news_category_id']);
                }
                if (isset($news_category_info) && !empty($news_category_info)) {
                    $this->data['cat_name'] = !empty($news_category_info['meta_title']) ? $news_category_info['meta_title'] : $news_category_info['name'];
                    $this->data['breadcrumbs'][] = array(
                              'text' => $news_category_info['name'],
                              'href' => $this->url->link('news/news_category', 'cat_id=' . $news_category_info['news_category_id']),
                              'separator' => $this->language->get('text_separator')
                          );
                }
            }
        }

        if (isset($this->request->get['filter_name_news']) || isset($this->request->get['filter_tag'])) {
            $url = '';

            if (isset($this->request->get['filter_name_news'])) {
                $url .= '&filter_name_news=' . $this->request->get['filter_name_news'];
            }

            if (isset($this->request->get['filter_tag'])) {
                $url .= '&filter_tag=' . $this->request->get['filter_tag'];
            }

            if (isset($this->request->get['filter_description'])) {
                $url .= '&filter_description=' . $this->request->get['filter_description'];
            }

            if (isset($this->request->get['filter_news_category_id'])) {
                $url .= '&filter_news_category_id=' . $this->request->get['filter_news_category_id'];
            }

            $this->data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_search'),
                'href' => $this->url->link('news/search', $url),
                'separator' => $this->language->get('text_separator')
            );
        }

        if (isset($this->request->get['news_id'])) {
            $news_id = $this->request->get['news_id'];
        } else {
            $news_id = 0;
        }

        $this->load->model('catalog/news');

        $news_info = $this->model_catalog_news->getNew($news_id);

        $this->data['news_info'] = $news_info;

        if ($news_info) {
            $categories = $this->model_catalog_news->getNewsCategories($this->request->get['news_id']);
            if (isset($categories) && !empty($categories)) {
                $category_id = $categories[0]['news_category_id'];
                $this->data['cat_id'] = $categories[0]['news_category_id'];
                $this->load->model('catalog/news_category');
            }

            if ($news_info['main_cat_id'] != 0){
              $main_cat_id = $news_info['main_cat_id'];
            } else {
              // get one of list cat
              $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_to_category WHERE news_id = '". $news_id ."'");
              $main_cat_id = $query->row['news_category_id'];
            }

            $this->data['cat_info'] = $this->model_catalog_news_category->getNewsCategory($main_cat_id);


            $url = '';

            if (isset($this->request->get['cat_id'])) {
                $url .= '&cat_id=' . $this->request->get['cat_id'];
            }

            if (isset($this->request->get['filter_name_news'])) {
                $url .= '&filter_name_news=' . $this->request->get['filter_name_news'];
            }

            if (isset($this->request->get['filter_tag'])) {
                $url .= '&filter_tag=' . $this->request->get['filter_tag'];
            }

            if (isset($this->request->get['filter_description'])) {
                $url .= '&filter_description=' . $this->request->get['filter_description'];
            }

            if (isset($this->request->get['filter_news_category_id'])) {
                $url .= '&filter_news_category_id=' . $this->request->get['filter_news_category_id'];
            }

            $this->data['breadcrumbs'][] = array(
                'text' => $news_info['name'],
                'href' => $this->url->link('news/news', $url . '&news_id=' . $this->request->get['news_id']),
                'separator' => $this->language->get('text_separator')
            );

            if (strlen($news_info['meta_title']) > 1) {
                $title = $news_info['meta_title'];
            } else {
                $title = $news_info['name'];
            }

            $this->data['title'] = $title;

            $this->document->setTitle($title);
            $this->document->setMetanames('description', $news_info['meta_description']);

            if (strlen($news_info['meta_description']) > 1) {
                $meta_description = $news_info['meta_description'];
            } else {
                $meta_description = $news_info['short_description'];
            }

            $sitename = nl2br($this->config->get('config_name')[$this->config->get('config_language_id')]);
            if($this->customer->isLogged()){
              $current_url = $this->url->link('news/news', 'email='. $this->customer->getEmail() .'&news_id=' . $this->request->get['news_id']);
            } else {
              $current_url = $this->url->link('news/news', 'news_id=' . $this->request->get['news_id']);
            }



            if (!empty($news_info['robots'])) {
                $robots = $news_info['robots'];
            } else {
                $robots = $this->config->get('config_meta_robots');
            }
            $this->document->setMetanames('robots', $robots);
            if ($news_info['image']) {
                $image = $this->model_tool_image->cropsize($news_info['image'], 760, 380);
                $this->document->setMetapros('og:image', $image);
            }
            //open graph
            $this->document->setMetapros('og:type', 'article');
            $this->document->setMetapros('og:title', $title);
            $this->document->setMetapros('og:description', $meta_description);
            $this->document->setMetapros('og:url', $current_url);

            $this->document->setMetapros('og:site_name', $sitename);
            //open graph

            $this->document->setMetapros('article:published_time', date('c', strtotime($news_info['date_available'])));
            $this->document->setMetapros('article:modified_time', date('c', strtotime($news_info['date_modified'])));
            $this->document->setMetapros('article:section', $news_info['name']);

            $this->document->setMetanames('keywords', $news_info['meta_keyword']);

            // $this->document->addLink($current_url, 'canonical');

            $this->data['heading_title'] = $news_info['name'];
            $this->data['url'] = $current_url;

            $this->data['text_select'] = $this->language->get('text_select');
            $this->data['text_share'] = $this->language->get('text_share');
            $this->data['text_wait'] = $this->language->get('text_wait');
            $this->data['text_tags'] = $this->language->get('text_tags');
            $this->data['text_write'] = $this->language->get('text_write');
            $this->data['text_post_on'] = $this->language->get('text_post_on');
            $this->data['text_viewed'] = $this->language->get('text_viewed');

            $this->data['entry_name'] = $this->language->get('entry_name');
            $this->data['entry_email'] = $this->language->get('entry_email');
            $this->data['entry_rating'] = $this->language->get('entry_rating');
            $this->data['entry_good'] = $this->language->get('entry_good');
            $this->data['entry_bad'] = $this->language->get('entry_bad');
            $this->data['entry_captcha'] = $this->language->get('entry_captcha');

            $this->data['button_send'] = $this->language->get('button_send');
            $this->data['button_continue'] = $this->language->get('button_continue');
            $this->data['continue'] = $this->url->link('news/blog');

            $this->data['tab_description'] = $this->language->get('tab_description');
            $this->data['tab_related'] = $this->language->get('tab_related');
            $this->data['tab_others'] = $this->language->get('tab_others');

            $this->data['news_id'] = $this->request->get['news_id'];

            if ($news_info['image']) {
                $this->data['popup'] = $this->model_tool_image->cropsize($news_info['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
            } else {
                $this->data['popup'] = '';
            }

            if ($news_info['image']) {
                $this->data['thumb'] = $this->model_tool_image->cropsize($news_info['image'], $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height'));
            } else {
                $this->data['thumb'] = '';
            }

            if ($news_info['banner']) {
                $this->data['banner'] = 'media/'.$news_info['banner'];
            } else {
                $this->data['banner'] = '';
            }
//            $this->data['date_added'] = date($this->language->get('date_format_short'), strtotime($news_info['date_available']));
            $this->data['date_added'] = $news_info['date_available'] != '0000-00-00 00:00:00' ? date($this->language->get('date_format_month'), strtotime($news_info['date_available'])) : date($this->language->get('date_format_month'), strtotime($news_info['date_added']));
            $this->data['short_description'] = html_entity_decode($news_info['short_description'], ENT_QUOTES, 'UTF-8');
            $this->data['description'] = html_entity_decode($news_info['description'], ENT_QUOTES, 'UTF-8');
            $this->data['author'] = $news_info['author'];
            $this->data['designer'] = $news_info['designer'];
            $this->data['photographer'] = $news_info['photographer'];
            $this->data['director'] = $news_info['director'];
            $this->data['viewed'] = $news_info['viewed'];

            $this->data['current_url'] = $this->url->link('news/news', $url . '&news_id=' . $this->request->get['news_id']);


            $this->data['news'] = array();

            $results = $this->model_catalog_news->getNewsRelated($this->request->get['news_id']);

            foreach ($results as $result) {
                if ($result['image']) {
                    $image = $this->model_tool_image->cropsize($result['image'], $this->config->get('config_image_related_width'), $this->config->get('config_image_related_height'));
                } else {
                    $image = false;
                }

                $this->data['news'][] = array(
                    'news_id' => $result['news_id'],
                    'thumb' => $image,
                    'author' => $result['author'],
                    'name' => $result['name'],
                    'viewed' => $result['viewed'],
                    'image' => $result['image'],
                    'date_added' => $result['date_available'] != '0000-00-00 00:00:00' ? date($this->language->get('date_format_month'), strtotime($result['date_available'])) : date($this->language->get('date_format_month'), strtotime($result['date_added'])),
                    'href' => $this->url->link('news/news', $url . '&news_id=' . $result['news_id']),
                );
            }

            $this->data['news_tab'] = array();

            $results2 = $this->model_catalog_news->getNews(array('filter_news_category_id' => $main_cat_id, 'start' => 0, 'limit' => 3));

            foreach ($results2 as $result) {

                $this->data['news_tab'][] = array(
                    'news_id' => $result['news_id'],
                    'name' => $result['name'],
                    'href' => $this->url->link('news/news', $url . '&news_id=' . $result['news_id']),
                );
            }

            // other news
            $this->data['other_news'] = array();
            if (isset($news_category_info)) {
                $results = $this->model_catalog_news->getOtherNews($news_category_info['news_category_id'], $this->request->get['news_id'], 3);
            } else {
                $results = $this->model_catalog_news->getOtherNews('', $this->request->get['news_id'], 10);
            }
            foreach ($results as $result) {
                if ($result['image']) {
                    $image = $this->model_tool_image->cropsize($result['image'], $this->config->get('config_image_related_width'), $this->config->get('config_image_related_height'));
                } else {
                    $image = false;
                }

                $this->data['other_news'][] = array(
                    'news_id' => $result['news_id'],
                    'thumb' => $image,
                    'name' => $result['name'],
                    'date_added' => $result['date_available'] != '0000-00-00 00:00:00' ? date($this->language->get('date_format_short'), strtotime($result['date_available'])) : date($this->language->get('date_format_short'), strtotime($result['date_added'])),
                    'href' => $this->url->link('news/news', $url . '&news_id=' . $result['news_id']),
                );
            }



            $this->data['tags'] = array();

            $results = $this->model_catalog_news->getNewsTags($this->request->get['news_id']);

            foreach ($results as $result) {
                $this->data['tags'][] = array(
                    'tag' => $result['tag'],
                    'href' => $this->url->link('product/search', 'search=' . $result['tag'])
                );
            }

            $this->model_catalog_news->updateViewed($this->request->get['news_id']);

            // print_r($news_category_info);
            // $this->data['news_category_info'] = $news_category_info;
            // if ($news_category_info['news_category_id'] == 23) {
            //     $this->document->addStyle('static/css/recruitment.css');
            //     $this->document->addStyle('static/css/tuyensinh.css');
            //     $this->template = 'news/tuyensinh.tpl';
            // } else {
            //     $this->template = 'news/news.tpl';
            // }
            $this->template = 'news/news.tpl';

            $this->children = array(
                'information/handbook',
                'information/register',
                'common/column_left',
                'common/column_right',
                'common/content_top',
                'common/content_bottom',
                'common/slideside',
                'common/footer',
                'common/header'
            );

            $this->response->setOutput($this->render());
        } else {
            $url = '';

            if (isset($this->request->get['cat_id'])) {
                $url .= '&cat_id=' . $this->request->get['cat_id'];
            }

            if (isset($this->request->get['manufacturer_id'])) {
                $url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
            }

            if (isset($this->request->get['filter_name_news'])) {
                $url .= '&filter_name_news=' . $this->request->get['filter_name_news'];
            }

            if (isset($this->request->get['filter_tag'])) {
                $url .= '&filter_tag=' . $this->request->get['filter_tag'];
            }

            if (isset($this->request->get['filter_description'])) {
                $url .= '&filter_description=' . $this->request->get['filter_description'];
            }

            if (isset($this->request->get['filter_news_category_id'])) {
                $url .= '&filter_news_category_id=' . $this->request->get['filter_news_category_id'];
            }

            $this->data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_error'),
                'href' => $this->url->link('news/news', $url . '&news_id=' . $news_id),
                'separator' => $this->language->get('text_separator')
            );

            $this->document->setTitle($this->language->get('text_error'));

            $this->data['heading_title'] = $this->language->get('text_error');

            $this->data['text_error'] = $this->language->get('text_error');

            $this->data['button_continue'] = $this->language->get('button_continue');

            $this->data['continue'] = $this->url->link('common/home');

            $this->template = 'error/not_found.tpl';

            $this->children = array(
                'common/column_left',
                'common/column_right',
                'common/content_top',
                'common/content_bottom',
                'common/footer',
                'common/header'
            );

            $this->response->setOutput($this->render());
        }
    }

    public function rate()
    {
        $this->load->model('catalog/news');
//            if ($this->request->server['REQUEST_METHOD'] == 'POST') {
        $this->model_catalog_news->setRate($this->request->get['news_id'], $this->request->post, $this->request->get['point']);
//            }
        $rate = $this->model_catalog_news->getRate($this->request->get['news_id']);
        $json = ''; //array();
//          $this->session->data['sql'] = $rate['total_rate'];
//        $json['total_vote'] = 'Tổng số rate: ' . $rate['total_rate'];
        $json = $rate['total_rate'];
//            $json['total_point'] = $rate['total_point'];
//        $json['average'] = 'Trung bình: ' . round($rate['total_point'] / $rate['total_rate'], 1);
        $json .= ',' . round($rate['total_point'] / $rate['total_rate'], 1);
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
}
