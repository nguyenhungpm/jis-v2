<?php
class ControllerFeedGoogleSitemap extends Controller {
	public function index() {
		if ($this->config->get('google_sitemap_status')) {
			$output  = '<?xml version="1.0" encoding="UTF-8"?>';
			$output .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">';
			$this->load->model('catalog/information');
			$this->load->model('catalog/product');
			$this->load->model('catalog/news');
			$this->load->model('tool/image');
			$data = array(
				'sitemap' => true
			);
			$news = $this->model_catalog_news->getNews($data);
			foreach ($news as $new) {
				$output .= '<url>';
				$output .= '<loc>' . $this->url->link('news/news', 'news_id=' . $new['news_id']) . '</loc>';
				$output .= '<changefreq>weekly</changefreq>';
				if(strpos($new['date_modified'],'-0001')===false){
					$output .= '<lastmod>' . date('Y-m-d\TH:i:sP', strtotime($new['date_added'])) . '</lastmod>';
				} else {
					$output .= '<lastmod>' . date('Y-m-d\TH:i:sP', strtotime($new['date_modified'])) . '</lastmod>';
				}
				$output .= '<priority>1.0</priority>';
				if($new['image']){
	                $output .= '<image:image>';
		            $output .= '<image:loc>' . $this->model_tool_image->resize($new['image'],400,400) . '</image:loc>';
		            $output .= '<image:caption>' . $this->escape_text($new['name']) . '</image:caption>';
		            $output .= '<image:title>' . $this->escape_text($new['name']) . '</image:title>';
		            $output .= '</image:image>';
				}
				$output .= '</url>';
			}

		 	$data = array(
                'rental' => '1'
            );
			$products = $this->model_catalog_product->getProducts();
			foreach ($products as $product) {
				$output .= '<url>';
				$output .= '<loc>' . $this->url->link('product/product', 'product_id=' . $product['product_id']) . '</loc>';
				$output .= '<changefreq>weekly</changefreq>';
				if(strpos($product['date_modified'],'-0001')===false){
					$output .= '<lastmod>' . date('Y-m-d\TH:i:sP', strtotime($product['date_added'])) . '</lastmod>';
				} else {
					$output .= '<lastmod>' . date('Y-m-d\TH:i:sP', strtotime($product['date_modified'])) . '</lastmod>';
				}
				$output .= '<priority>1.0</priority>';
				if($product['image']){
	                $output .= '<image:image>';
		            $output .= '<image:loc>' . $this->model_tool_image->resize($product['image'],400,400) . '</image:loc>';
		            $output .= '<image:caption>' . $this->escape_text($product['name']) . '</image:caption>';
		            $output .= '<image:title>' . $this->escape_text($product['name']) . '</image:title>';
		            $output .= '</image:image>';
				}
				$output .= '</url>';
			}

			$informations = $this->model_catalog_information->getInformations();

			foreach ($informations as $information) {
				$output .= '<url>';
				$output .= '<loc>' . $this->url->link('information/information', 'information_id=' . $information['information_id']) . '</loc>';
				$output .= '<changefreq>weekly</changefreq>';
				$output .= '<priority>0.5</priority>';
				$output .= '</url>';
			}

			$output .= '</urlset>';

			$this->response->addHeader('Content-Type: application/xml');
			$this->response->setOutput($output);
		}
	}

	function escape_text($text){
		$text=preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $text);
		return $text;
	}
}
?>
