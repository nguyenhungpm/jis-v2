<?php
class ControllerAmpHeader extends Controller {
	protected function index() {
        $this->data['title'] =  $this->document->getTitle();
		$this->data['lang'] = $this->language->get('code');
		$this->data['links'] = $this->document->getLinks();
		$this->data['google_analytics'] = $this->config->get('config_google_analytics');
		
		$status = true;
		if (isset($this->request->server['HTTP_USER_AGENT'])) {
			$robots = explode("\n", trim($this->config->get('config_robots')));

			foreach ($robots as $robot) {
				if ($robot && strpos($this->request->server['HTTP_USER_AGENT'], trim($robot)) !== false) {
					$status = false;

					break;
				}
			}
		}
		$this->template = 'amp/header.tpl';
		$this->render();
	}
}
?>
