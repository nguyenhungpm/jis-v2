<?php  
class ControllerAmpFooter extends Controller {
	protected function index() {
		
		$this->data['google_analytics'] = $this->config->get('config_google_analytics');
				
		$this->data['store'] = nl2br($this->config->get('config_owner')[$this->config->get('config_language_id')]);
		$this->data['hotline'] = $this->config->get('config_hotline');
		
		// Whos Online
		if ($this->config->get('config_customer_online')) {
			$this->load->model('tool/online');

			if (isset($this->request->server['REMOTE_ADDR'])) {
				$ip = $this->request->server['REMOTE_ADDR'];	
			} else {
				$ip = ''; 
			}

			if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {
				$url = 'http://' . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];	
			} else {
				$url = '';
			}

			if (isset($this->request->server['HTTP_REFERER'])) {
				$referer = $this->request->server['HTTP_REFERER'];	
			} else {
				$referer = '';
			}
			$this->model_tool_online->whosonline($ip, $this->customer->getId(), $url, $referer);
		}		

		$this->template = 'amp/footer.tpl';
		$this->render();
	}
	}
?>