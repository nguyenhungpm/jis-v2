<?php
class ControllerAmpNews extends Controller {
    private $error = array();

    public function index() {
        $this->language->load('news/news');
        $this->load->model('catalog/news');
		
		$this->data['text_related'] = $this->language->get('text_related');
        
        if (isset($this->request->get['news_id'])) {
            $news_id = $this->request->get['news_id'];
        } else {
            $news_id = 0;
        }

        $news_info = $this->model_catalog_news->getNew($news_id);

        $this->data['news_info'] = $news_info;

        if ($news_info) {
            $url = '';

            if (strlen($news_info['meta_title']) > 1) {
                $title = $news_info['meta_title'];
            } else {
                $title = $news_info['name'];
            }

            $this->document->setTitle($title);

            if (strlen($news_info['meta_description']) > 1) {
                $meta_description = $news_info['meta_description'];
            } else {
                $meta_description = $news_info['short_description'];
            }
			
			$this->data['meta_description'] = $meta_description;
			$description = html_entity_decode($news_info['description'], ENT_QUOTES, 'UTF-8');
			$this->data['description'] = $this->response->amphtml($description);

            $this->document->addLink($this->url->link('news/news', 'news_id=' . $this->request->get['news_id']), 'canonical');

            $this->data['heading_title'] = $news_info['name'];

            $this->load->model('tool/image');

            if ($news_info['image']) {
                $this->data['thumb'] = $this->model_tool_image->resize($news_info['image'], $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height'));
            } else {
                $this->data['thumb'] = '';
            }
			
            $this->data['relates'] = array();
            $results = $this->model_catalog_news->getOtherNews('', $this->request->get['news_id'], 10);

            foreach ($results as $result) {
                $this->data['relates'][] = array(
                    'news_id' => $result['news_id'],
                    'name' => $result['name'],
                    'date_added' => $result['date_available'] != '0000-00-00 00:00:00' ? date($this->language->get('date_format_short'), strtotime($result['date_available'])) : date($this->language->get('date_format_short'), strtotime($result['date_added'])),
                    'href' => $this->url->link('news/news', $url . '&news_id=' . $result['news_id']),
                );
            }
            $this->model_catalog_news->updateViewed($this->request->get['news_id']);
			// echo 3;
            
            $this->template = 'amp/product.tpl';

            $this->children = array(
                'amp/footer',
                'amp/header'
            );

            $this->response->setOutput($this->render());
        } else {
            $url = '';

            if (isset($this->request->get['cat_id'])) {
                $url .= '&cat_id=' . $this->request->get['cat_id'];
            }

            if (isset($this->request->get['manufacturer_id'])) {
                $url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
            }

            if (isset($this->request->get['filter_name_news'])) {
                $url .= '&filter_name_news=' . $this->request->get['filter_name_news'];
            }

            if (isset($this->request->get['filter_tag'])) {
                $url .= '&filter_tag=' . $this->request->get['filter_tag'];
            }

            if (isset($this->request->get['filter_description'])) {
                $url .= '&filter_description=' . $this->request->get['filter_description'];
            }

            if (isset($this->request->get['filter_news_category_id'])) {
                $url .= '&filter_news_category_id=' . $this->request->get['filter_news_category_id'];
            }

            $this->data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_error'),
                'href' => $this->url->link('news/news', $url . '&news_id=' . $news_id),
                'separator' => $this->language->get('text_separator')
            );

            $this->document->setTitle($this->language->get('text_error'));

            $this->data['heading_title'] = $this->language->get('text_error');

            $this->data['text_error'] = $this->language->get('text_error');

            $this->data['button_continue'] = $this->language->get('button_continue');

            $this->data['continue'] = $this->url->link('common/home');

            $this->template = 'error/not_found.tpl';

            $this->children = array(
                'common/column_left',
                'common/column_right',
                'common/content_top',
                'common/content_bottom',
                'common/footer',
                'common/header'
            );

            $this->response->setOutput($this->render());
        }
    }

}
?>