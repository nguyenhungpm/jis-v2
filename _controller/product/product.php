<?php
class ControllerProductProduct extends Controller {
    private $error = array();
    public function index() {
        $this->document->addStyle('static/css/tuyensinh.css');
        $this->document->addStyle('static/css/recruitment.css');
        $this->language->load('product/product');
        $this->load->model('tool/image');
        $this->load->model('catalog/product');
        $this->load->model('catalog/category');
        $this->data['rate'] = $this->model_catalog_product->getRate($this->request->get['product_id']);
        if ($this->data['rate']['total_rate'] == 0) {
            $this->data['rate']['total_rate'] = 1;
            $this->data['rate']['total_point'] = 5;
        }
        $this->data['average'] = round($this->data['rate']['total_point'] / $this->data['rate']['total_rate']);
            $this->data['url_order'] = $this->url->link('product/order', 'pd_id=' . $this->request->get['product_id']);
        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home'),
            'separator' => false
        );

        // OSD with love. Edit in front_end
        $this->load->library('user');
        $this->user = new User($this->registry);

        $this->data['admin_logged'] = false;
        if ($this->user->isLogged()) {
            $this->data['admin_logged'] = true;
            $this->data['edit'] = HTTP_SERVER . "oadmin/index.php?route=catalog/product/update&token=" . $this->session->data['token'] . "&product_id=" . $this->request->get['product_id'];
            $this->data['token'] = $this->session->data['token'];
        }
        // OSD with love. Edit in front_end

        $this->load->model('catalog/category');
        $this->data['hotline2'] = $this->config->get('config_hotline');
        // Tin lien quan

        $this->load->model('catalog/news');
        $this->load->model('catalog/product');

        $results_news = array();

        $results_news = $this->model_catalog_product->getProductNews($this->request->get['product_id']);

        foreach ($results_news as $result) {
            $width = '';
            $height = '';
            if ($result['image']) {
                $image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_news_width'), $this->config->get('config_image_news_height'));
            } else {
                $firstImgNews = $this->catchFirstImage(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'));
                if ($firstImgNews == 'no_image.jpg') {
                    $image = $this->model_tool_image->resize('no_image.jpg', $this->config->get('config_image_news_width'), $this->config->get('config_image_news_height'));
                } else {
                    $image = $firstImgNews;
                    $width = 'width="' . $this->config->get('config_image_news_width') . '"';
                    $height = 'height="' . $this->config->get('config_image_news_height') . '"';
                }
            }


            $this->data['news_relateds'][] = array(
                'news_id' => $result['news_id'],
                'thumb' => $image,
                'name' => $result['name'],
                'width' => $width,
                'height' => $height,
                'href' => $this->url->link('news/news', '&news_id=' . $result['news_id'])
            );
        }

        $this->load->model('catalog/manufacturer');

        if (isset($this->request->get['manufacturer_id'])) {
            $this->data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_brand'),
                'href' => $this->url->link('product/manufacturer'),
                'separator' => $this->language->get('text_separator')
            );

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($this->request->get['manufacturer_id']);

            if ($manufacturer_info) {
                $this->data['breadcrumbs'][] = array(
                    'text' => $manufacturer_info['name'],
                    'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . $url),
                    'separator' => $this->language->get('text_separator')
                );
            }
        }

        if (isset($this->request->get['search']) || isset($this->request->get['tag'])) {
            $url = '';

            if (isset($this->request->get['search'])) {
                $url .= '&search=' . $this->request->get['search'];
            }

            if (isset($this->request->get['tag'])) {
                $url .= '&tag=' . $this->request->get['tag'];
            }

            if (isset($this->request->get['description'])) {
                $url .= '&description=' . $this->request->get['description'];
            }

            if (isset($this->request->get['category_id'])) {
                $url .= '&category_id=' . $this->request->get['category_id'];
            }

            if (isset($this->request->get['sub_category'])) {
                $url .= '&sub_category=' . $this->request->get['sub_category'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $this->data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_search'),
                'href' => $this->url->link('product/search', $url),
                'separator' => $this->language->get('text_separator')
            );
        }

        if (isset($this->request->get['product_id'])) {
            $product_id = (int) $this->request->get['product_id'];
        } else {
            $product_id = 0;
        }

        $this->load->model('catalog/product');

        $product_info = $this->model_catalog_product->getProduct($product_id);
        
        if ($product_info) {
            $this->data['category_info'] = $this->model_catalog_category->getCategoryByProduct($product_id);
            // print_r($this->data['category_info']);

            $url = '';
            if (isset($this->request->get['path'])) {
                $url .= '&path=' . $this->request->get['path'];
            }

            if (isset($this->request->get['filter'])) {
                $url .= '&filter=' . $this->request->get['filter'];
            }

            if (isset($this->request->get['manufacturer_id'])) {
                $url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
            }

            if (isset($this->request->get['search'])) {
                $url .= '&search=' . $this->request->get['search'];
            }

            if (isset($this->request->get['tag'])) {
                $url .= '&tag=' . $this->request->get['tag'];
            }

            if (isset($this->request->get['description'])) {
                $url .= '&description=' . $this->request->get['description'];
            }

            if (isset($this->request->get['category_id'])) {
                $url .= '&category_id=' . $this->request->get['category_id'];
            }

            if (isset($this->request->get['sub_category'])) {
                $url .= '&sub_category=' . $this->request->get['sub_category'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $this->data['breadcrumbs'][] = array(
                'text' => $product_info['name'],
                'href' => $this->url->link('product/product', $url . '&product_id=' . $this->request->get['product_id']),
                'separator' => $this->language->get('text_separator')
            );

            if (strlen($product_info['meta_title']) > 1) {
                $title = $product_info['meta_title'];
            } else {
                $title= $product_info['name'];
            }

            if (strlen($product_info['meta_description']) > 1) {
                $meta_description = $product_info['meta_description'];
            } else {
                $meta_description = utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, 300) . '...';
            }

            $this->document->setTitle($title);
            $this->document->setMetapros('description',$product_info['meta_description']);

            $robots = $product_info['robots'];
            $this->document->setMetapros('robots',$robots);
            $this->document->setMetapros('keywords',$product_info['meta_keyword']);
            $current_url = $this->url->link('product/product', 'product_id=' . $this->request->get['product_id']);
            $sitename= nl2br($this->config->get('config_name')[$this->config->get('config_language_id')]);
            //open graph
            $this->document->setMetapros('og:locate',$this->language->get('locate'));
            $this->document->setMetapros('og:type','article');
            $this->document->setMetapros('og:title',$title);
            $this->document->setMetapros('og:description',$meta_description);
            $this->document->setMetapros('og:url',$current_url);

            $image_og = $this->model_tool_image->cropsize($product_info['image'], 1200,630);
            $this->document->setMetapros('og:image',$image_og);
            $this->document->setMetapros('og:site_name',$sitename);
                if($this->config->get('config_dateog')==1){
                    if($product_info['date_available'] != '0000-00-00 00:00:00'){
                        $this->document->setMetapros('article:published_time',date('c', strtotime($product_info['date_available'])));
                    }else{
                        $this->document->setMetapros('article:published_time',date('c', strtotime($product_info['date_added'])));
                    }
                }


            $this->document->setMetapros('article:modified_time',date('c', strtotime($product_info['date_modified'])));
            $this->document->setMetapros('article:section','Sản phẩm');

                $this->data['url'] = $current_url;

                $this->document->addLink($current_url, 'canonical');
                if($this->config->get('config_amp_product')){
                    $this->document->addLink($this->url->link('product/amp_product', 'product_id=' . $this->request->get['product_id']), 'amphtml');
                }

            $this->data['heading_title'] = $product_info['name'];

            $this->data['button_continue'] = $this->language->get('button_continue');

            $this->data['text_model'] = $this->language->get('text_model');
            $this->data['text_out_stock'] = $this->language->get('text_out_stock');
            $this->data['text_outstock'] = $this->language->get('text_outstock');
            $this->data['text_instock'] = $this->language->get('text_instock');
            $this->data['text_buynow'] = $this->language->get('text_buynow');
            $this->data['text_qty'] =  $this->language->get('text_qty');
            $this->data['tab_related_news'] = $this->language->get('tab_related_news');
            $this->data['text_otherproduct'] = $this->language->get('text_otherproduct');
            $this->load->model('catalog/review');

            $this->data['tab_description'] = $this->language->get('tab_description');
            $this->data['tab_related'] = $this->language->get('tab_related');
            $this->data['text_manufacturer'] = $this->language->get('text_manufacturer');

            $this->data['product_id'] = $this->request->get['product_id'];
            $this->data['manufacturer'] = $product_info['manufacturer'];
            $this->data['manufacturers'] = $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $product_info['manufacturer_id']);
            $this->data['model'] = $product_info['model'];
            $this->data['quantity'] = $product_info['quantity'];
            $this->data['include'] = explode(PHP_EOL, $product_info['include']);
						// print_r($this->data['include']);
            $this->data['not_include'] = explode(PHP_EOL, $product_info['not_include']);

            $this->data['stockcontrol'] = $this->config->get('config_product_stock');
            $this->data['displayprice'] = $this->config->get('config_product_price');

            if ($product_info['price']>0){
                $price = number_format($product_info['price'],0,'',' ') . ' đ';
            } else {
                $price = $this->language->get('call_for_price');
            }

            $this->data['price'] = $price;
            $this->data['short_description'] = $product_info['short_description'];


            if ($product_info['image']) {
                $this->data['popup'] = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
            } else {
                $this->data['popup'] = '';
            }

            if ($product_info['image']) {
                $this->data['thumb'] = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height'));
            } else {
                $this->data['thumb'] = '';
            }

            $this->data['images'] = array();

            $results = $this->model_catalog_product->getProductImages($this->request->get['product_id']);

            foreach ($results as $result) {
                $this->data['images'][] = array(
                    'popup' => $this->model_tool_image->resize($result['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height')),
                    'thumb' => $this->model_tool_image->resize($result['image'], $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height')),
                    'image' => $result['image'],
                    'title' => $result['title'],
                    'description' => $result['description'],
                );
            }

            $this->data['schedules'] = array();

            $results = $this->model_catalog_product->getProductSchedules($this->request->get['product_id']);

            foreach ($results as $result) {
                $this->data['schedules'][] = array(
                    'product_schedule_id' => $result['product_schedule_id'],
                    'tips' => $result['tips'],
                    'title' => $result['title'],
                    'time' => $result['time'],
                    'detail' => unserialize($result['detail'])
                );
            }
						
						// print_r($this->data['schedules']);

            $this->data['description'] = html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8');
            $this->data['date_added'] = $product_info['date_available'] != '0000-00-00 00:00:00' ? date($this->language->get('date_format_short'), strtotime($product_info['date_available'])) : date($this->language->get('date_format_short'), strtotime($product_info['date_added']));

            $this->data['products'] = array();

            $results = $this->model_catalog_product->getOtherProduct(array('product_id' => $this->request->get['product_id'], 'limit' => '15'));

            foreach ($results as $result) {
                if ($result['image']) {
                    $image =  $this->model_tool_image->resize($result['image'], $this->config->get('config_image_related_width'), $this->config->get('config_image_related_height'));
                } else {
                    $image =  $this->model_tool_image->resize('no_image.jpg', $this->config->get('config_image_related_width'), $this->config->get('config_image_related_height'));
                }

                if ($result['price']>0){
                    $price = number_format($result['price'],0,'',' ') . ' đ';
                } else {
                    $price = $this->language->get('call_for_price');
                }

                $this->data['products'][] = array(
                    'product_id' => $result['product_id'],
                    'price' => $price,
                    'date_added' => $result['date_available'] != '0000-00-00 00:00:00' ? date('d-m-Y h:i', strtotime($result['date_available'])) : date('d-m-Y h:i', strtotime($result['date_added'])),
                    'thumb' => $image,
                    'name' => $result['name'],
                    'href' => $this->url->link('product/product', 'product_id=' . $result['product_id'])
                );
            }

            $this->data['tags'] = array();

            if ($product_info['tag']) {
                $tags = explode(',', $product_info['tag']);
                foreach ($tags as $tag) {
                    $this->data['tags'][] = array(
                        'tag' => trim($tag),
                        'href' => $this->url->link('product/search', 'tag=' . trim($tag))
                    );
                }
            }

            $this->model_catalog_product->updateViewed($this->request->get['product_id']);

            $this->document->addScript('static/baguetteBox.js');
            $this->document->addStyle('static/baguetteBox.css');
            $this->document->addStyle('wwwroot/templates/website/release/css/productdetail-page.min.css');
            $script_popup = "baguetteBox.run(
            '.gallery', {
                    captions: true,
                    buttons: 'auto',
                    async: false,
                    preload: 2,
                    animation: 'slideIn' // fadeIn or slideIn
            });
            ";
            $this->document->addExScript($script_popup);
            $this->template = 'product/product.tpl';

            $this->children = array(
                'information/handbook',
                'common/column_left',
                'common/column_right',
                'common/content_top',
                'common/content_bottom',
                'common/footer',
                'common/header'
            );

            $this->response->setOutput($this->render());
        } else {
            $url = '';

            if (isset($this->request->get['path'])) {
                $url .= '&path=' . $this->request->get['path'];
            }

            if (isset($this->request->get['filter'])) {
                $url .= '&filter=' . $this->request->get['filter'];
            }

            if (isset($this->request->get['manufacturer_id'])) {
                $url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
            }

            if (isset($this->request->get['search'])) {
                $url .= '&search=' . $this->request->get['search'];
            }

            if (isset($this->request->get['tag'])) {
                $url .= '&tag=' . $this->request->get['tag'];
            }

            if (isset($this->request->get['description'])) {
                $url .= '&description=' . $this->request->get['description'];
            }

            if (isset($this->request->get['category_id'])) {
                $url .= '&category_id=' . $this->request->get['category_id'];
            }

            if (isset($this->request->get['sub_category'])) {
                $url .= '&sub_category=' . $this->request->get['sub_category'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $this->data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_error'),
                'href' => $this->url->link('product/product', $url . '&product_id=' . $product_id),
                'separator' => $this->language->get('text_separator')
            );

            $this->document->setTitle($this->language->get('text_error'));

            $this->data['heading_title'] = $this->language->get('text_error');

            $this->data['text_error'] = $this->language->get('text_error');

            $this->data['button_continue'] = $this->language->get('button_continue');

            $this->data['continue'] = $this->url->link('common/home');

            $this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . '/1.1 404 Not Found');

            $this->template = 'error/not_found.tpl';

            $this->children = array(
                'common/column_left',
                'common/column_right',
                'common/content_top',
                'common/content_bottom',
                'common/footer',
                'common/header'
            );

            $this->response->setOutput($this->render());
        }
    }

    protected function getPath($parent_id, $current_path = '') {
        $category_info = $this->model_catalog_category->getCategory($parent_id);
        if ($category_info) {
            if (!$current_path) {
                $new_path = $category_info['category_id'];
            } else {
                $new_path = $category_info['category_id'] . '_' . $current_path;
            }
            $path = $this->getPath($category_info['parent_id'], $new_path);

            if ($path) {
                return $path;
            } else {
                return $new_path;
            }
        }
    }

    function catchFirstImage($content) {
        $first_img = '';
        $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $content, $matches);
        if (isset($matches[1][0])) {
            $first_img = $matches[1][0];
        }
        if (empty($first_img)) {
            $first_img = "no_image.jpg";
        }
        return $first_img;
    }


    public function rate() {
        $this->load->model('catalog/product');
        $this->model_catalog_product->setRate($this->request->get['product_id'], $this->request->post, $this->request->get['point']);
        $rate = $this->model_catalog_product->getRate($this->request->get['product_id']);
        $json = '';
        $json = $rate['total_rate'];
        $json .= ',' . round($rate['total_point'] / $rate['total_rate'], 1);
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

}

?>
