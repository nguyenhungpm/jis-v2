<?php
class ControllerInformationAdmission extends Controller
{
    public function index()
    {
        $this->document->addStyle('static/css/tuyensinh.css');
        $this->document->addStyle('static/css/recruitment.css');
        $this->language->load('information/admission');

        $this->load->model('catalog/admission');

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home'),
            'separator' => false,
        );

        if (isset($this->request->get['admission_id'])) {
            $admission_id = (int) $this->request->get['admission_id'];
        } else {
            $admission_id = 0;
        }

        $this->data['admission_id'] = $admission_id;

        if ($this->config->get('config_amp_admission')) {
            $this->document->addLink($this->url->link('information/info_amp', 'admission_id=' . $this->request->get['admission_id']), 'amphtml');
        }
        $this->document->addLink($this->url->link('information/admission', 'admission_id=' . $this->request->get['admission_id']), 'canonical');

        // OSD with love. Edit in front_end
        $this->load->library('user');
        $this->user = new User($this->registry);

        $this->data['admin_logged'] = false;
        if ($this->user->isLogged()) {
            $this->data['admin_logged'] = true;
            $this->data['edit'] = HTTP_SERVER . "oadmin/index.php?route=catalog/admission/update&token=" . $this->session->data['token'] . "&admission_id=" . $this->request->get['admission_id'];
            $this->data['token'] = $this->session->data['token'];
        }
        // OSD with love. Edit in front_end
        $admissions = $this->model_catalog_admission->getAdmissionSameCategory($admission_id);

		$this->data['admissions'] = $admissions;

        $admission_info = $this->model_catalog_admission->getAdmission($admission_id);

        if ($admission_info) {
            if (strlen($admission_info['meta_title']) > 1) {
                $this->document->setTitle($admission_info['meta_title']);
            } else {
                $this->document->setTitle($admission_info['title']);
            }
            $this->document->setMetanames('description', $admission_info['meta_description']);
            $this->document->setMetanames('keywords', $admission_info['meta_keyword']);

            if (!empty($admission_info['robots'])) {
                $robots = $admission_info['robots'];
            } else {
                $robots = $this->config->get('config_meta_robots');
            }

            $this->document->setMetanames('robots', $robots);

            $this->data['breadcrumbs'][] = array(
                'text' => $admission_info['title'],
                'href' => $this->url->link('information/admission', 'admission_id=' . $admission_id),
                'separator' => $this->language->get('text_separator'),
            );

            $this->data['heading_title'] = $admission_info['title'];

            $this->data['button_continue'] = $this->language->get('button_continue');

            $this->data['description'] = html_entity_decode($admission_info['description'], ENT_QUOTES, 'UTF-8');

            $this->data['news_tab'] = array();

            $results2 = $this->model_catalog_admission->getAdmissions();
            foreach ($results2 as $result) {
                $this->data['news_tab'][] = array(
                    'admission_id' => $result['admission_id'],
                    'name' => $result['title'],
                    'href' => $this->url->link('information/admission', '&admission_id=' . $result['admission_id']),
                );
            }

            $this->data['highlight'] = $this->model_catalog_admission->getAdmissionHighlights($admission_id);

            $this->data['continue'] = $this->url->link('common/home');

            $this->template = 'information/admission.tpl';

            $this->children = array(
                'information/register',
                'information/handbook',
                'common/column_left',
                'common/column_right',
                'common/slideside',
                'common/content_top',
                'common/content_bottom',
                'common/footer',
                'common/header',
            );

            $this->response->setOutput($this->render());
        } else {
            $this->data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_error'),
                'href' => $this->url->link('information/admission', 'admission_id=' . $admission_id),
                'separator' => $this->language->get('text_separator'),
            );

            $this->document->setTitle($this->language->get('text_error'));

            $this->data['heading_title'] = $this->language->get('text_error');

            $this->data['text_error'] = $this->language->get('text_error');

            $this->data['button_continue'] = $this->language->get('button_continue');

            $this->data['continue'] = $this->url->link('common/home');

            $this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . '/1.1 404 Not Found');

            $this->template = 'error/not_found.tpl';

            $this->children = array(
                'common/column_left',
                'common/column_right',
                'common/content_top',
                'common/content_bottom',
                'common/footer',
                'common/header',
            );

            $this->response->setOutput($this->render());
        }
    }

    public function info()
    {
        $this->load->model('catalog/admission');

        if (isset($this->request->get['admission_id'])) {
            $admission_id = (int) $this->request->get['admission_id'];
        } else {
            $admission_id = 0;
        }

        $admission_info = $this->model_catalog_admission->getAdmission($admission_id);

        if ($admission_info) {
            $output = '<html dir="ltr" lang="en">' . "\n";
            $output .= '<head>' . "\n";
            $output .= '  <title>' . $admission_info['title'] . '</title>' . "\n";
            $output .= '  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">' . "\n";
            $output .= '  <meta name="robots" content="noindex">' . "\n";
            $output .= '</head>' . "\n";
            $output .= '<body>' . "\n";
            $output .= '  <h1>' . $admission_info['title'] . '</h1>' . "\n";
            $output .= html_entity_decode($admission_info['description'], ENT_QUOTES, 'UTF-8') . "\n";
            $output .= '  </body>' . "\n";
            $output .= '</html>' . "\n";

            $this->response->setOutput($output);
        }
    }
}
