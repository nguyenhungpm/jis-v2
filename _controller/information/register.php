<?php
class ControllerInformationRegister extends Controller {
	protected function index() {
		$this->data['action'] = $this->url->link('information/contact');
        $this->document->addStyle('static/css/tuyensinh.css');
		$this->load->model('catalog/information');
		$this->data['informations'] = $this->model_catalog_information->getSpecInformations();
		$script = "
		function postToGoogle() {
			var student_year = $('#student_year').val();
			var address = $('#address').val();
			var student = $('#student').val();
			var father = $('#father').val();
			var father_tel = $('#father_tel').val();
			var father_email = $('#father_email').val();
			var father_job = $('#father_job').val();
			var father_year = $('#father_year').val();
			var mother = $('#mother').val();
			var mother_tel = $('#mother_tel').val();
			var mother_email = $('#mother_email').val();
			var mother_job = $('#mother_job').val();
			var mother_year = $('#mother_year').val();
			var _class = $('#class').val();
			var detail_class = $('#detail_class').val();
			
			$.ajax({
				url: 'https://docs.google.com/forms/d/e/1FAIpQLSdj9BRYo25qDuRVec-HQUE1b9g5xiAxWANYRmZw76GJT2YUwQ/formResponse',
				data: {
					'entry.1622976001': student,
					'entry.1903721350': student_year,
					'entry.1363851570': address,
					'entry.1857813710': father,
					'entry.1367421355': father_tel,
					'entry.2088293556': father_email,
					'entry.773265881': father_job,
					'entry.2011119932': father_year,
					'entry.2090706569': mother,
					'entry.70076041': mother_tel,
					'entry.1897484410': mother_email,
					'entry.2035099152': mother_job,
					'entry.2035099152': mother_year,
					'entry.707904526': _class,
					'entry.1410457534': detail_class,
				},
				type: 'POST',
				dataType: 'xml',
				// success: function(d){},
				statusCode: {
					0: function() {
							//Success
					},
					200: function() {
							//Success
					}
				},
				// error: function(x, y, z) {
				// 	$('#success-msg').show();
				// }
			});

			var data_object = $('form#fluentform_3').serializeObject();
			console.log('hungnv2 -> data_object:', data_object);

			$.ajax({
				url: '/information/register/sendMail',
				type: 'POST',
				data: data_object,
				beforeSend: function() {
					$('#loading').show()
				},
				success: function(json) {
					console.log('hungnv2 -> json:', json);
					$('#loading').hide();
					$('#required').hide();
					$('#success-msg').show();
				}
			})

			// $('#fluentform_3').submit();
		
			return false;
		}
		";
		$this->document->addExScript($script);
		$this->template = 'information/register.tpl';

		$this->render();
	}

	public function sendMail() {
		$data = array();
		try {
			if (($this->request->server['REQUEST_METHOD'] == 'POST') /* && $this->validate() */) {
				$this->session->data['mail'] = $this->request->post;
				$mail = new Mail();
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->hostname = $this->config->get('config_smtp_host');
				$mail->username = $this->config->get('config_smtp_username');
				$mail->password = $this->config->get('config_smtp_password');
				$mail->port = $this->config->get('config_smtp_port');
				$mail->timeout = $this->config->get('config_smtp_timeout');
				// $mail->setTo($this->config->get('config_email_tuyensinh'));
				// $mail->setTo(array($this->config->get('config_email_tuyensinh'), 'hungnv157@gmail.com'));
				$mail->setTo(explode(",", $this->config->get('config_email_tuyensinh')));
				if(!empty($this->config->get('config_mail_from'))){
					$mail->setFrom($this->config->get('config_mail_from'));
				}else{
					$mail->setFrom($this->config->get('config_email'));
				}
				$mail->setReplyTo($this->config->get('config_email_tuyensinh'));
				$mail->setSender('Thông tin đăng ký tuyển sinh từ Web');
				$mail->setSubject(html_entity_decode(sprintf('Đăng ký tuyển sinh cho học sinh %s (%s) lớp %s', $this->request->post['student'], $this->request->post['student_year'], $this->request->post['class']), ENT_QUOTES, 'UTF-8'));
				// $mail->setText(strip_tags(html_entity_decode("Tên người liên hệ: ". $this->request->post['name'] . "\r\nEmail: ". $this->request->post['email2'] . "\r\nĐiện thoại: " . $this->request->post['phone'] ."\r\n \r\nNội dung: " . $this->request->post['enquiry'], ENT_QUOTES, 'UTF-8')));
				// $content = '
				// 	<table s>
				// 		<tr>
				// 			<th>Học sinh</th>
				// 			<td>'. $this->request->post['student'] .'</td>
				// 		</tr>
				// 	</table>
				// ';
				$content = '
				<div id="email" style="width:600px;margin: auto;background:white;">
				<table role="presentation" border="0" align="right" cellspacing="0">
				  <tr>
					<td>
					  <a href="https://docs.google.com/spreadsheets/d/1CcMU1p92szf7pYIVkomVsUyTVPeyY-RXnp4r8zkSkFA/edit?resourcekey#gid=696948428" style="font-size: 9px; text-transform:uppercase; letter-spacing: 1px; color: #99ACC2;  font-family:Avenir;">Xem danh sách</a>
					</td>
				  </tr>
				</table>
				
				<!-- Header --> 
				<table role="presentation" border="1" width="100%" cellspacing="0">
					<tr>
					<td bgcolor="#173865" align="center" style="color: white;">
						<h1 style="font-size: 30px; margin:0; font-family:Avenir;"> Đăng ký tuyển sinh!</h1>
					</td>
					</tr>
				</table>
				
				<!-- Body 2--> 
				<table role="presentation" border="1" width="100%" cellspacing="0">
					<tr style="background: #ddd">
						<th style="text-align: left; padding: 7px 10px">Học sinh</th>
					  	<td style="text-align: left; padding: 7px 10px">'. $this->request->post['student'] .'</td>
					</tr>       
					<tr>
						<th style="text-align: left; padding: 7px 10px">Năm sinh</th>
					  	<td style="text-align: left; padding: 7px 10px">'. $this->request->post['student_year'] .'</td>
					</tr>       
					<tr style="background: #ddd">
						<th style="text-align: left; padding: 7px 10px">Địa chỉ</th>
					  	<td style="text-align: left; padding: 7px 10px">'. $this->request->post['address'] .'</td>
					</tr>       
					<tr>
						<th style="text-align: left; padding: 7px 10px">Họ và tên cha</th>
					  	<td style="text-align: left; padding: 7px 10px">'. $this->request->post['father'] .'</td>
					</tr>       
					<tr style="background: #ddd">
						<th style="text-align: left; padding: 7px 10px">Số điện thoại cha</th>
					  	<td style="text-align: left; padding: 7px 10px">'. $this->request->post['father_tel'] .'</td>
					</tr>       
					<tr>
						<th style="text-align: left; padding: 7px 10px">Email cha</th>
					  	<td style="text-align: left; padding: 7px 10px">'. $this->request->post['father_email'] .'</td>
					</tr>       
					<tr style="background: #ddd">
						<th style="text-align: left; padding: 7px 10px">Nghề nghiệp cha</th>
					  	<td style="text-align: left; padding: 7px 10px">'. $this->request->post['father_job'] .'</td>
					</tr>       
					<tr style="background: #ddd">
						<th style="text-align: left; padding: 7px 10px">Ngày sinh của cha</th>
					  	<td style="text-align: left; padding: 7px 10px">'. $this->request->post['father_year'] .'</td>
					</tr>       
					<tr>
						<th style="text-align: left; padding: 7px 10px">Họ và tên mẹ</th>
					  	<td style="text-align: left; padding: 7px 10px">'. $this->request->post['mother'] .'</td>
					</tr>       
					<tr style="background: #ddd">
						<th style="text-align: left; padding: 7px 10px">Số điện thoại mẹ</th>
					  	<td style="text-align: left; padding: 7px 10px">'. $this->request->post['mother_tel'] .'</td>
					</tr>       
					<tr>
						<th style="text-align: left; padding: 7px 10px">Email mẹ</th>
					  	<td style="text-align: left; padding: 7px 10px">'. $this->request->post['mother_email'] .'</td>
					</tr>       
					<tr style="background: #ddd">
						<th style="text-align: left; padding: 7px 10px">Nghề nghiệp mẹ</th>
					  	<td style="text-align: left; padding: 7px 10px">'. $this->request->post['mother_job'] .'</td>
					</tr>        
					<tr style="background: #ddd">
						<th style="text-align: left; padding: 7px 10px">Ngày sinh của mẹ</th>
					  	<td style="text-align: left; padding: 7px 10px">'. $this->request->post['mother_year'] .'</td>
					</tr>       
					<tr>
						<th style="text-align: left; padding: 7px 10px">Đăng ký chương trình học</th>
					  	<td style="text-align: left; padding: 7px 10px">'. $this->request->post['class'] .'</td>
					</tr>       
					<tr>
						<th style="text-align: left; padding: 7px 10px">Đăng ký nhập học lớp</th>
					  	<td style="text-align: left; padding: 7px 10px">'. $this->request->post['detail_class'] .'</td>
					</tr>       
			  	</table>
				
			   	<!-- Footer -->
				<table role="presentation" border="1" width="100%" cellspacing="0">
					<tr>
						<td bgcolor="#F5F8FA" style="padding: 15px;">
						  <p style="margin:0; font-size:16px; line-height:24px; color: #99ACC2; font-family:Avenir"> Made with &hearts; at JIS </p>    
						</td>
					</tr>
				</table> 
			  	</div>
				';
				$mail->setHtml($content);
				$mail->send();

				// $this->redirect($this->url->link('information/contact/success'));
				// $this->load->model('account/contact');
				// $this->model_account_contact->addContact($this->request->post);
				$data[] = array(
					'data' => 'done',
					'error' => null,
					'status' => '200'
				);
			}
		} catch (\Throwable $th) {
			//throw $th;
			$data[] = array(
				'error' => $th,
				'data' => null,
				'status' => '500'
			);
		}
		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($data));
	}
}
?>
