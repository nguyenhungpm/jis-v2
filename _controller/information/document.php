<?php
class ControllerInformationdocument extends Controller
{
    public function index()
    {
        $this->document->addStyle('static/css/document.css');
        $this->language->load('information/document');
        $this->load->model('catalog/document');
        $this->load->model('tool/image');
        $this->data['heading_title'] = $this->language->get('heading_title');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->data['breadcrumbs'] = array();
        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home'),
            'separator' => false,
        );

        if (isset($this->request->get['page'])) {
            $this->data['page'] = $page = $this->request->get['page'];
        } else {
            $this->data['page'] = $page = 1;
        }

        if (isset($this->request->get['limit'])) {
            $limit = $this->request->get['limit'];
        } else {
            $limit = $this->config->get('config_news_limit');
        }

        if (isset($this->request->get['filter_name'])) {
            $this->data['filter_name'] = $filter_name = $this->request->get['filter_name'];
        } else {
            $this->data['filter_name'] = $filter_name = false;
        }

        if (isset($this->request->get['doccat_id'])) {
            $this->data['doccat_id'] = $doccat_id = (int) $this->request->get['doccat_id'];
            $this->data['all_active'] = 'cat__item';
        } else {
            $this->data['doccat_id'] = $doccat_id = 0;
            $this->data['all_active'] = 'cat__item cat__item--active';
        }

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('information/document'),
            'separator' => $this->language->get('text_separator'),
        );

        if ($doccat_id > 0) {
            $doccat = $this->model_catalog_document->getdoccat($doccat_id);
            $this->data['breadcrumbs'][] = array(
                'text' => $doccat['name'],
                'href' => $this->url->link('information/document', '&doccat_id=' . $doccat_id),
                'separator' => $this->language->get('text_separator'),
            );
            $this->document->setTitle($doccat['name']);
            $this->data['heading_title'] = $doccat['name'];
        } else {
            $this->data['heading_title'] = $this->language->get('heading_title');
        }

        $this->data['text_readmore'] = $this->language->get('text_readmore');
        $this->data['text_doccat'] = $this->language->get('text_doccat');
        $this->data['text_alldocs'] = $this->language->get('text_alldocs');
        $this->data['text_filtername'] = $this->language->get('text_filtername');
        $this->data['text_empty'] = $this->language->get('text_empty');
        $this->data['text_added'] = $this->language->get('text_added');

        $this->data['cats'] = array();
        $cats = $this->model_catalog_document->getdocumentCats();
        if ($cats) {
            foreach ($cats as $result) {
                if ($doccat_id == $result['doccat_id']) {
                    $active = 'cat__item cat__item--active';
                } else {
                    $active = 'cat__item';
                }
                $this->data['cats'][] = array(
                    'name' => $result['name'],
                    'active' => $active,
                    'href' => $this->url->link('information/document', 'doccat_id=' . $result['doccat_id']),
                );
            }
        }

        $this->data['documents'] = array();
        $data = array(
            'filter_name' => $filter_name,
            'doccat_id' => $doccat_id,
            'start' => ($page - 1) * $limit,
            'limit' => $limit,
        );
        $this->data['width'] = $this->config->get('config_image_news_width');
        $this->data['height'] = $this->config->get('config_image_news_height');
        $total = $this->model_catalog_document->getTotaldocuments($data);
        $results = $this->model_catalog_document->getdocuments($data);
        // print_r($results);
        foreach ($results as $result) {
            if ($result['image']) {
                $image = $this->model_tool_image->cropsize($result['image'], $this->config->get('config_image_news_width'), $this->config->get('config_image_news_height'));
            } else {
                $firstImgNews = $this->catchFirstImage(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'));
                if ($firstImgNews == 'no_image.jpg') {
                    $image = $this->model_tool_image->cropsize('no_image.jpg', $this->config->get('config_image_news_width'), $this->config->get('config_image_news_height'));
                } else {
                    $image = $firstImgNews;
                    $width = 'width="' . $this->config->get('config_image_news_width') . '"';
                    $height = 'height="' . $this->config->get('config_image_news_height') . '"';
                }
            }
            $this->data['documents'][] = array(
                'name' => $result['title'],
                'catname' => $result['catname'],
                'doccat_id' => $result['doccat_id'],
                'thumb' => $image,
                'short_description' => $result['short_description'],
                'date_added' => date('d-m-Y', strtotime($result['date_added'])),
                'href' => $this->url->link('information/document/info', 'document_id=' . $result['document_id']),
                //'href'              => ($result['filename']=='')?'':HTTP_DOWNLOAD . $result['filename']
            );
        }

        $url = '';

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }
        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . $this->request->get['filter_name'];
        }

        if (isset($this->request->get['doccat_id'])) {
            $url .= '&doccat_id=' . (int) $this->request->get['doccat_id'];
        }

        $pagination = new Pagination();
        $pagination->total = $total;
        $pagination->page = $page;
        $pagination->limit = $limit;
        $pagination->text = $this->language->get('text_pagination');
        $pagination->url = $this->url->link('information/document', '&page={page}');

        $this->document->addStyle('static/casestudy.css');
        $this->data['pagination'] = $pagination->render();
        $this->template = 'information/document_list.tpl';

        $this->children = array(
            'common/column_left',
            'common/column_right',
            'common/content_top',
            'common/content_bottom',
            'common/footer',
            'common/header',
        );

        $this->response->setOutput($this->render());
    }

    public function info()
    {
		$this->document->addStyle('static/css/document.css');
        $this->language->load('information/document');
        $this->load->model('catalog/document');
        $this->load->model('tool/image');

        $this->load->library('user');
        $this->user = new User($this->registry);

        $this->data['admin_logged'] = false;
        if ($this->user->isLogged()) {
            $this->data['admin_logged'] = true;
            $this->data['edit'] = HTTP_SERVER . "oadmin/index.php?route=catalog/admission/update&token=" . $this->session->data['token'] . "&admission_id=" . $this->request->get['admission_id'];
            $this->data['token'] = $this->session->data['token'];
        }

        if (isset($this->request->get['document_id'])) {
            $this->data['document_id'] = $document_id = (int) $this->request->get['document_id'];
        } else {
            $this->data['document_id'] = $document_id = 0;
        }

        $this->data['breadcrumbs'] = array();
        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home'),
            'separator' => false,
        );

        if (isset($this->request->get['doccat_id'])) {
            $this->data['doccat_id'] = $doccat_id = (int) $this->request->get['doccat_id'];
            $this->data['all_active'] = 'cat__item';
        } else {
            $this->data['doccat_id'] = $doccat_id = 0;
            $this->data['all_active'] = 'cat__item cat__item--active';
        }

        $this->data['cats'] = array();
        $cats = $this->model_catalog_document->getdocumentCats();
        if ($cats) {
            foreach ($cats as $result) {
                if ($doccat_id == $result['doccat_id']) {
                    $active = 'cat__item cat__item--active';
                } else {
                    $active = 'cat__item';
                }
                $this->data['cats'][] = array(
                    'name' => $result['name'],
                    'active' => $active,
                    'href' => $this->url->link('information/document', 'doccat_id=' . $result['doccat_id']),
                );
            }
        }
        if (isset($this->request->get['document_id'])) {
            $document_id = (int) $this->request->get['document_id'];
            $this->document->addLink($this->url->link('information/document/info', 'document_id=' . $this->request->get['document_id']), 'canonical');
        } else {
            $document_id = 0;
        }

        // OSD with love. Edit in front_end
        $this->load->library('user');
        $this->user = new User($this->registry);

        $this->data['admin_logged'] = false;
        if ($this->user->isLogged() && isset($this->request->get['document_id'])) {
            $this->data['admin_logged'] = true;
            $this->data['edit'] = HTTP_SERVER . "oadmin/index.php?route=catalog/document/update&token=" . $this->session->data['token'] . "&document_id=" . $this->request->get['document_id'];
            $this->data['token'] = $this->session->data['token'];
        }
        // OSD with love. Edit in front_end
        $this->data['documents'] = array();

        $data = array(
            'start' => 0,
            'limit' => 10,
        );

        $others = $this->model_catalog_document->getOtherdocuments($data, $document_id);
        foreach ($others as $result) {
            if ($result['image']) {
                $image = $this->model_tool_image->cropsize($result['image'], 120, 90);
            } else {
                $image = $this->model_tool_image->cropsize('no_image.jpg', 120, 90);
            }
            $this->data['documents'][] = array(
                'name' => $result['title'],
                'thumb' => $image,
                'href' => $this->url->link('information/document/info', 'document_id=' . $result['document_id']),
            );
        }

        $document_info = $this->model_catalog_document->getdocument($document_id);

        if ($document_info) {
            if (strlen($document_info['meta_title']) > 1) {
                $this->document->setTitle($document_info['meta_title']);
            } else {
                $this->document->setTitle($document_info['title']);
            }
            $this->document->setMetapros('description', $document_info['meta_description']);
            $this->document->setMetapros('keywords', $document_info['meta_keyword']);

            if (strlen($document_info['robots']) > 1) {
                $this->document->setMetapros('robots', $document_info['robots']);
            }
            $this->data['breadcrumbs'][] = array(
                'text' => $this->language->get('heading_title'),
                'href' => $this->url->link('information/document'),
                'separator' => $this->language->get('text_separator'),
            );
            $this->data['breadcrumbs'][] = array(
                'text' => $document_info['catname'],
                'href' => $this->url->link('information/document', 'doccat_id=' . $document_info['doccat_id']),
                'separator' => $this->language->get('text_separator'),
            );

            $this->data['heading_title'] = $document_info['title'];

            $this->data['button_continue'] = $this->language->get('button_continue');

            $this->data['text_linkdown'] = $this->language->get('text_linkdown');
            $this->data['text_alldocs'] = $this->language->get('text_alldocs');
            $this->data['text_request'] = $this->language->get('text_request');
            $this->data['text_added'] = $this->language->get('text_added');
            $this->data['text_has'] = $this->language->get('text_has');
            $this->data['text_download'] = $this->language->get('text_download');
            $this->data['text_others'] = $this->language->get('text_others');

            $this->data['entry_phone'] = $this->language->get('entry_phone');
            $this->data['entry_name'] = $this->language->get('entry_name');
            $this->data['entry_email'] = $this->language->get('entry_email');
            $this->data['entry_title'] = $this->language->get('entry_title');
            $this->data['entry_office'] = $this->language->get('entry_office');
            $this->data['text_inquiry'] = $this->language->get('text_inquiry');
            $this->data['entry_inquiry_desc'] = $this->language->get('entry_inquiry_desc');
            $this->data['button_submit'] = $this->language->get('button_submit');

            if ($document_info['image']) {
                $this->data['image'] = $this->model_tool_image->cropsize($document_info['image'], 920, 300);
            } else {
                $this->data['image'] = $this->model_tool_image->cropsize('no_image.jpg', 920, 300);
            }
            $this->data['short_description'] = html_entity_decode($document_info['short_description'], ENT_QUOTES, 'UTF-8');
            $this->data['description'] = html_entity_decode($document_info['description'], ENT_QUOTES, 'UTF-8');

            $this->data['catname'] = $document_info['catname'];
            $this->data['doccat_id'] = $document_info['doccat_id'];
            $this->data['hash'] = $document_info['hash'];
            $this->data['type'] = $document_info['type'];
            $this->data['linkdown'] = $document_info['linkdown'];
            $this->data['filename'] = $document_info['filename'];
            $this->data['invisible'] = $document_info['invisible'];
            $this->data['notify'] = $document_info['notify'];
            $this->data['hash'] = $document_info['hash'];
            $this->data['document_id'] = $document_info['document_id'];
            $this->data['downloads'] = $document_info['download'] + 1016;

            $this->data['date_added'] = $document_info['date_added'] != '0000-00-00 00:00:00' ? date($this->language->get('date_format_short'), strtotime($document_info['date_added'])) : '';
            $this->data['continue'] = $this->url->link('common/home');

            $this->document->addStyle('static/casestudy.css');
            $this->document->addScript('static/regdoc.js');

            $this->template = 'information/document.tpl';

            $this->children = array(
                'common/column_left',
                'common/column_right',
                'common/slideside',
                'common/content_top',
                'common/content_bottom',
                'common/footer',
                'common/header',
            );

            $this->response->setOutput($this->render());
        } else {
            $this->data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_error'),
                'href' => $this->url->link('information/document', 'document_id=' . $document_id),
                'separator' => $this->language->get('text_separator'),
            );
            $this->document->setTitle($this->language->get('text_error'));
            $this->data['heading_title'] = $this->language->get('text_error');
            $this->data['text_error'] = $this->language->get('text_error');
            $this->data['button_continue'] = $this->language->get('button_continue');
            $this->data['continue'] = $this->url->link('common/home');
            $this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . '/1.1 404 Not Found');
            $this->template = 'error/not_found.tpl';

            $this->children = array(
                'common/column_left',
                'common/column_right',
                'common/content_top',
                'common/content_bottom',
                'common/footer',
                'common/header',
            );

            $this->response->setOutput($this->render());
        }
    }

    public function load()
    {

        $this->load->model('catalog/document');
        if (isset($this->request->get['filter_name'])) {
            $filter_name = $this->request->get['filter_name'];
        } else {
            $filter_name = '';
        }

        if (isset($this->request->get['doccat_id'])) {
            $doccat_id = (int) $this->request->get['doccat_id'];
        } else {
            $doccat_id = 0;
        }

        if (isset($this->request->get['page'])) {
            $this->data['page'] = $page = (int) $this->request->get['page'];
        } else {
            $this->data['page'] = $page = 1;
        }

        if (isset($this->request->get['limit'])) {
            $limit = $this->request->get['limit'];
        } else {
            $limit = $this->config->get('config_news_limit');
        }

        $data = array(
            'doccat_id' => $doccat_id,
            'filter_name' => $filter_name,
            'start' => ($page - 1) * $limit,
            'limit' => $limit,
        );

        $this->data['documents'] = array();
        $document_total = $this->model_catalog_document->getTotaldocuments($data);
        $results = $this->model_catalog_document->getdocuments($data);
        foreach ($results as $result) {
            if ($result['sort_order'] !== '-1') {
                $this->data['documents'][] = array(
                    'name' => $result['title'],
                    'description' => nl2br($result['short_description']),
                    'date_added' => date('d-m-Y', strtotime($result['date_added'])),
                    'href' => $this->url->link('information/document/info', 'document_id=' . $result['document_id']),
                );
            }
        }

        $pagination = new Pagination();
        $pagination->total = $document_total;
        $pagination->page = $page;
        $pagination->limit = $limit;
        $pagination->text = $this->language->get('text_pagination');
        $pagination->url = $this->url->link('information/document', '&page={page}');
        $this->data['pagination'] = $pagination->render();
        $this->template = 'information/load_document.tpl';
        $this->response->setOutput($this->render());
    }

    public function download()
    {
        if (isset($_GET["hash"])) {
            $hash = $_GET["hash"];
            $opts = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true,
                ),
            );
            $context = stream_context_create($opts);
            $this->load->model('catalog/document');
            $this->model_catalog_document->updateDownload($hash);
            $document = $this->model_catalog_document->getDocumentByHash($hash);
            $filename = DIR_DOWNLOAD . $document['filename'];
            $realfilename = $document['mask'];
            if ($realfilename) {
                ob_end_clean();
                header("Content-Type: application/octet-stream; ");
                header("Content-Transfer-Encoding: binary");
                header("Content-Length: " . filesize($filename) . ";");
                header("Content-disposition: attachment; filename=" . $realfilename);
                readfile($filename, false, $context);
                die();
            } else {

            }
        }
    }

    public function view()
    {
        if (isset($_GET["hash"])) {
            $hash = $_GET["hash"];
            $context = stream_context_create($opts);
            $this->load->model('catalog/document');
            $this->model_catalog_document->updateDownload($hash);
            $document = $this->model_catalog_document->getDocumentByHash($hash);
            header('Location:' . $document['linkdown']);
        }
    }
    public function catchFirstImage($content)
    {
        $first_img = '';
        $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $content, $matches);
        if (isset($matches[1][0])) {
            $first_img = $matches[1][0];
        }
        if (empty($first_img)) { //Defines a default image
            $first_img = "no_image.jpg";
        }
        return $first_img;
    }
}
