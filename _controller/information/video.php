<?php 
class ControllerInformationVideo extends Controller {
	public function index() {  
		$this->language->load('information/video');
		
		$this->data['text_intro'] = $this->language->get('text_intro');
		$this->data['text_empty'] = $this->language->get('text_empty');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_video'] = $this->language->get('text_video');
		$this->data['text_gallery'] = $this->language->get('text_gallery');
		
		$this->document->setTitle($this->data['heading_title']);

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),
			'separator' => false
		);
		
		$this->load->model('design/banner');
		$this->load->model('tool/image');
		
		$this->data['videos'] = array();
		
		foreach($this->model_design_banner->getVideos() as $rs){
			$this->data['videos'][] = array(
				'name' => $rs['title'],
				'thumb' => 'https://img.youtube.com/vi/'. $rs['link'] .'/mqdefault.jpg',
				'href' => 'https://www.youtube.com/watch?v='.$rs['link']
			);
		}
		
		$this->document->addScript('static/mediabox.js');
		
		$this->template = 'information/video.tpl';

		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/slideside',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'
		);

		$this->response->setOutput($this->render());
	}
}
?>