<?php
class ControllerInformationVisit extends Controller {
	public function index() {
		$this->document->setTitle('Đăng ký tham quan');
		$this->document->addStyle('static/css/recruitment.css');
		$this->document->addStyle('static/css/tuyensinh.css');
		$this->document->addStyle('static/css/visit.css');

		$this->load->model('catalog/news_category');
		$this->load->model('catalog/admission');
		$this->load->model('catalog/news');

		$this->data['news_tab'] = array();

		$results2 = $this->model_catalog_admission->getAdmissions();
		foreach ($results2 as $result) {
			$this->data['news_tab'][] = array(
				'admission_id' => $result['admission_id'],
				'name' => $result['title'],
				'href' => $this->url->link('information/admission', '&admission_id=' . $result['admission_id']),
			);
		}

		$script = "
		function postToGoogle() {
			var parent = $('#parent').val();
			var parent_tel = $('#parent_tel').val();
			var parent_email = $('#parent_email').val();
			var time = $('#time').val();
			var note = $('#note').val();
			
			$.ajax({
				url: 'https://docs.google.com/forms/d/e/1FAIpQLSf4IQjto-IA_Lz4051wjvY_QVwoChEn7Sn9HdoZFP4BUExFyA/formResponse',
				data: {
					'entry.1472049591': parent,
					'entry.313185851': parent_tel,
					'entry.969812832': parent_email,
					'entry.1211737826': time,
					'entry.1251188244': note,
				},
				type: 'POST',
				dataType: 'xml',
				// success: function(d){},
				statusCode: {
					0: function() {
							//Success
					},
					200: function() {
							//Success
					}
				},
				// error: function(x, y, z) {
				// 	$('#success-msg').show();
				// }
			});

			var data_object = $('form#form-visit').serializeObject();

			$.ajax({
				url: '/information/visit/sendMail',
				type: 'POST',
				data: data_object,
				beforeSend: function() {
					$('#loading').show()
				},
				success: function(json) {
					console.log('hungnv2 -> json:', json);
					$('#loading').hide();
					$('#required').hide();
					$('#success-msg').show();
				}
			})
		
			return false;
		}
		";
		$this->document->addExScript($script);
		$this->template = 'information/visit.tpl';

		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'
		);

		$this->response->setOutput($this->render());
	}

	public function sendMail() {
		$data = array();
		try {
			if (($this->request->server['REQUEST_METHOD'] == 'POST') /* && $this->validate() */) {
				$this->session->data['mail'] = $this->request->post;
				$mail = new Mail();
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->hostname = $this->config->get('config_smtp_host');
				$mail->username = $this->config->get('config_smtp_username');
				$mail->password = $this->config->get('config_smtp_password');
				$mail->port = $this->config->get('config_smtp_port');
				$mail->timeout = $this->config->get('config_smtp_timeout');
				// $mail->setTo($this->config->get('config_email_tuyensinh'));
				$mail->setTo(explode(",", $this->config->get('config_email_tuyensinh')));
				if(!empty($this->config->get('config_mail_from'))){
					$mail->setFrom($this->config->get('config_mail_from'));
				}else{
					$mail->setFrom($this->config->get('config_email'));
				}
				$mail->setReplyTo($this->config->get('config_email_tuyensinh'));
				$mail->setSender('Thông tin đăng ký tham quan từ Web');
				$mail->setSubject(html_entity_decode(sprintf('Đăng ký tham quan phụ huynh %s', $this->request->post['parent']), ENT_QUOTES, 'UTF-8'));
				// $mail->setText(strip_tags(html_entity_decode("Tên người liên hệ: ". $this->request->post['name'] . "\r\nEmail: ". $this->request->post['email2'] . "\r\nĐiện thoại: " . $this->request->post['phone'] ."\r\n \r\nNội dung: " . $this->request->post['enquiry'], ENT_QUOTES, 'UTF-8')));
				// $content = '
				// 	<table s>
				// 		<tr>
				// 			<th>Học sinh</th>
				// 			<td>'. $this->request->post['student'] .'</td>
				// 		</tr>
				// 	</table>
				// ';
				$content = '
				<div id="email" style="width:600px;margin: auto;background:white;">
				<table role="presentation" border="0" align="right" cellspacing="0">
				  <tr>
					<td>
					  <a href="https://docs.google.com/spreadsheets/d/1JAKeyTvUs47GjL_qtRAyjEMWFUqRHezBs91DdBykqxc/edit?resourcekey#gid=1399457973" style="font-size: 9px; text-transform:uppercase; letter-spacing: 1px; color: #99ACC2;  font-family:Avenir;">Xem danh sách</a>
					</td>
				  </tr>
				</table>
				
				<!-- Header --> 
				<table role="presentation" border="1" width="100%" cellspacing="0">
					<tr>
					<td bgcolor="#173865" align="center" style="color: white;">
						<h1 style="font-size: 30px; margin:0; font-family:Avenir;"> Đăng ký tham quan!</h1>
					</td>
					</tr>
				</table>
				
				<!-- Body 2--> 
				<table role="presentation" border="1" width="100%" cellspacing="0">
					<tr style="background: #ddd">
						<th style="text-align: left; padding: 7px 10px">Phụ huynh</th>
					  	<td style="text-align: left; padding: 7px 10px">'. $this->request->post['parent'] .'</td>
					</tr>       
					<tr>
						<th style="text-align: left; padding: 7px 10px">Điện thoại liên hệ</th>
					  	<td style="text-align: left; padding: 7px 10px">'. $this->request->post['parent_tel'] .'</td>
					</tr>    
					<tr>
						<th style="text-align: left; padding: 7px 10px">Email</th>
					  	<td style="text-align: left; padding: 7px 10px">'. $this->request->post['parent_email'] .'</td>
					</tr>       
					<tr style="background: #ddd">
						<th style="text-align: left; padding: 7px 10px">Thời gian tham quan</th>
					  	<td style="text-align: left; padding: 7px 10px">'. $this->request->post['time'] .'</td>
					</tr>       
					<tr>
						<th style="text-align: left; padding: 7px 10px">Ghi chú</th>
					  	<td style="text-align: left; padding: 7px 10px">'. $this->request->post['note'] .'</td>
					</tr>
			  	</table>
				
			   	<!-- Footer -->
				<table role="presentation" border="1" width="100%" cellspacing="0">
					<tr>
						<td bgcolor="#F5F8FA" style="padding: 15px;">
						  <p style="margin:0; font-size:16px; line-height:24px; color: #99ACC2; font-family:Avenir"> Made with &hearts; at JIS </p>    
						</td>
					</tr>
				</table> 
			  	</div>
				';
				$mail->setHtml($content);
				$mail->send();

				// $this->redirect($this->url->link('information/contact/success'));
				// $this->load->model('account/contact');
				// $this->model_account_contact->addContact($this->request->post);
				$data[] = array(
					'data' => 'done',
					'error' => null,
					'status' => '200'
				);
			}
		} catch (\Throwable $th) {
			//throw $th;
			$data[] = array(
				'error' => $th,
				'data' => null,
				'status' => '500'
			);
		}
		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($data));
	}
}
?>
