<?php
class ControllerInformationRecruitment extends Controller
{
    public function index()
    {
        $this->language->load('information/recruitment');

        $this->load->model('catalog/recruitment');

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home'),
            'separator' => false,
        );

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        if (isset($this->request->get['limit'])) {
            $limit = $this->request->get['limit'];
        } else {
            $limit = 6;
        }

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('information/recruitment'),
            'separator' => $this->language->get('text_separator'),
        );

        $this->load->model('tool/image');
        $this->data['banner'] = $this->model_tool_image->cropsize($this->config->get('config_banner_tuyendung'), 1366, 350);
        $this->data['banner_small'] = $this->model_tool_image->cropsize($this->config->get('config_banner_tuyendung'), 600, 350);
        $this->document->setTitle($this->language->get('heading_title'));

        $this->data['heading_title'] = $this->language->get('heading_title');
        $this->data['text_industry'] = $this->language->get('text_industry');
        $this->data['text_readmore'] = $this->language->get('text_readmore');
        $this->data['text_position'] = $this->language->get('text_position');
        $this->data['text_location'] = $this->language->get('text_location');
        $this->data['text_salary'] = $this->language->get('text_salary');
        $this->data['text_expired'] = $this->language->get('text_expired');
        $this->data['text_empty'] = $this->language->get('text_empty');
        $this->data['text_added'] = $this->language->get('text_added');

        $this->data['recruitments'] = array();

        $data = array(
            'start' => ($page - 1) * $limit,
            'limit' => $limit,
        );

        $this->data['locations'] = $this->model_catalog_recruitment->getLocations();
        $this->data['reccats'] = $this->model_catalog_recruitment->getReccats();
		// print_r($this->data['locations']);
        $total = $this->model_catalog_recruitment->getTotalRecruitments($data);
        $results = $this->model_catalog_recruitment->getRecruitments($data);
		$this->data['jobtypes'][1] = $this->language->get('text_jt_fulltime');
		$this->data['jobtypes'][2] = $this->language->get('text_jt_parttime');
		$this->data['jobtypes'][3] = $this->language->get('text_jt_intern');
        foreach ($results as $result) {
            if ($result['sort_order'] !== '-1') {
                $this->data['recruitments'][] = array(
                    'name' => $result['title'],
                    'hot' => $result['hot'],
                    'reccat_name' => $result['reccat_name'],
                    'jobtype' => $result['jobtype'],
                    'benefit' => $result['benefit'],
                    'industry' => $result['industry'],
                    'location' => $result['location'],
                    'date_end' => date('d-m-Y', strtotime($result['date_end'])),
                    'href' => $this->url->link('information/recruitment', 'recruitment_id=' . $result['recruitment_id']),
                );
            }
        }

        $this->document->addStyle('static/css/recruitment.css');

        $pagination = new Pagination();
        $pagination->total = $total;
        $pagination->page = $page;
        $pagination->limit = $limit;
        $pagination->text = $this->language->get('text_pagination');
        $pagination->url = $this->url->link('information/recruitment', '&page={page}');

        $this->data['pagination'] = $pagination->render();
        $this->template = 'information/recruitment_list.tpl';

        $this->children = array(
            'common/slideside',
            'common/column_left',
            'common/column_right',
            'common/content_top',
            'common/content_bottom',
            'common/footer',
            'common/header',
        );

        $this->response->setOutput($this->render());
    }

    public function info()
    {
        $this->document->addStyle('static/css/recruitment.css');
        $this->language->load('information/recruitment');
        $this->load->model('catalog/recruitment');

        if (isset($this->request->get['recruitment_id'])) {
            $this->data['recruitment_id'] = $recruitment_id = (int) $this->request->get['recruitment_id'];
        } else {
            $this->data['recruitment_id'] = $recruitment_id = 0;
        }
        $this->data['breadcrumbs'] = array();
        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home'),
            'separator' => false,
        );

        if (isset($this->request->get['recruitment_id'])) {
            $recruitment_id = (int) $this->request->get['recruitment_id'];
            $this->document->addLink($this->url->link('information/recruitment', 'recruitment_id=' . $this->request->get['recruitment_id']), 'canonical');
        } else {
            $recruitment_id = 0;
        }

        $this->load->model('tool/image');
        $this->data['banner'] = $this->model_tool_image->cropsize($this->config->get('config_banner_tuyendung'), 1366, 350);
        $this->data['banner_small'] = $this->model_tool_image->cropsize($this->config->get('config_banner_tuyendung'), 600, 350);

        // OSD with love. Edit in front_end
        $this->load->library('user');
        $this->user = new User($this->registry);

        $this->data['admin_logged'] = false;
        if ($this->user->isLogged() && isset($this->request->get['recruitment_id'])) {
            $this->data['admin_logged'] = true;
            $this->data['edit'] = HTTP_SERVER . "oadmin/index.php?route=catalog/recruitment/update&token=" . $this->session->data['token'] . "&recruitment_id=" . $this->request->get['recruitment_id'];
            $this->data['token'] = $this->session->data['token'];
        }
        // OSD with love. Edit in front_end
        $this->data['recruitments'] = array();
        $data = array(
            'limit' => 10,
            'start' => 0,
        );
        foreach ($this->model_catalog_recruitment->getRecruitments($data) as $result) {
            if ($result['sort_order'] !== '-1') {
                $this->data['recruitments'][] = array(
                    'title' => $result['title'],
                    'href' => $this->url->link('information/recruitment', 'recruitment_id=' . $result['recruitment_id']),
                );
            }
        }

        $recruitment_info = $this->model_catalog_recruitment->getRecruitment($recruitment_id);
        if ($recruitment_info) {
            if (strlen($recruitment_info['meta_title']) > 1) {
                $this->document->setTitle($recruitment_info['meta_title']);
            } else {
                $this->document->setTitle($recruitment_info['title']);
            }
            $this->document->setMetapros('description', $recruitment_info['meta_description']);
            $this->document->setMetapros('keywords', $recruitment_info['meta_keyword']);

            if (strlen($recruitment_info['robots']) > 1) {
                $this->document->setMetapros('robots', $recruitment_info['robots']);
            }
            $this->data['breadcrumbs'][] = array(
                'text' => $this->language->get('heading_title'),
                'href' => $this->url->link('information/recruitment'),
                'separator' => $this->language->get('text_separator'),
            );
            /*
            $this->data['breadcrumbs'][] = array(
            'text'      => $recruitment_info['title'],
            'href'      => $this->url->link('information/recruitment', 'recruitment_id=' .  $recruitment_id),
            'separator' => $this->language->get('text_separator')
            );
             */
            $this->data['heading_title'] = $recruitment_info['title'];
            $this->data['button_continue'] = $this->language->get('button_continue');
            $this->data['text_position'] = $this->language->get('text_position');
            $this->data['text_location'] = $this->language->get('text_location');
            $this->data['text_salary'] = $this->language->get('text_salary');
            $this->data['text_expired'] = $this->language->get('text_expired');
            $this->data['text_other'] = $this->language->get('text_other');
            $this->data['text_industry'] = $this->language->get('text_industry');
            $this->data['text_apply'] = $this->language->get('text_apply');
            $this->data['text_added'] = $this->language->get('text_added');
            $this->data['entry_name'] = $this->language->get('entry_name');
            $this->data['entry_email'] = $this->language->get('entry_email');
            $this->data['entry_phone'] = $this->language->get('entry_phone');
            $this->data['entry_file'] = $this->language->get('entry_file');
            $this->data['entry_file_allow'] = $this->language->get('entry_file_allow');
            $this->data['entry_intro'] = $this->language->get('entry_intro');
            $this->data['entry_apply_title'] = $this->language->get('entry_apply_title');
            $this->data['entry_apply_desc'] = $this->language->get('entry_apply_desc');
            $this->data['button_submit'] = $this->language->get('button_submit');
            $this->data['button_upload'] = $this->language->get('button_upload');
            $this->data['button_close'] = $this->language->get('button_close');

            $this->data['requirement'] = html_entity_decode($recruitment_info['requirement'], ENT_QUOTES, 'UTF-8');
            $this->data['benefit'] = html_entity_decode($recruitment_info['benefit'], ENT_QUOTES, 'UTF-8');
            $this->data['position'] = nl2br($recruitment_info['position']);
            $this->data['location'] = $recruitment_info['location'];
            $this->data['industry'] = $recruitment_info['industry'];

            $this->data['date_end'] = $recruitment_info['date_end'] != '0000-00-00 00:00:00' ? date($this->language->get('date_format_short'), strtotime($recruitment_info['date_end'])) : '';

            $today = date('Y-m-d');
            $date_end = date('Y-m-d', strtotime($recruitment_info['date_end']));
            if ($today > $date_end) {
                $this->data['show'] = false;
            } else {
                $this->data['show'] = true;
            }
            $this->data['date_added'] = $recruitment_info['date_added'] != '0000-00-00 00:00:00' ? date($this->language->get('date_format_short'), strtotime($recruitment_info['date_added'])) : '';

            $this->data['continue'] = $this->url->link('common/home');
            $this->document->addScript('static/job.js');

            $this->template = 'information/recruitment.tpl';

            $this->children = array(
                'common/column_left',
                'common/column_right',
                'common/slideside',
                'common/content_top',
                'common/content_bottom',
                'common/footer',
                'common/header',
            );

            $this->response->setOutput($this->render());
        } else {

            $this->data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_error'),
                'href' => $this->url->link('information/recruitment', 'recruitment_id=' . $recruitment_id),
                'separator' => $this->language->get('text_separator'),
            );

            $this->document->setTitle($this->language->get('text_error'));

            $this->data['heading_title'] = $this->language->get('text_error');

            $this->data['text_error'] = $this->language->get('text_error');

            $this->data['button_continue'] = $this->language->get('button_continue');

            $this->data['continue'] = $this->url->link('common/home');
            $this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . '/1.1 404 Not Found');
            $this->template = 'error/not_found.tpl';

            $this->children = array(
                'common/column_left',
                'common/column_right',
                'common/content_top',
                'common/content_bottom',
                'common/footer',
                'common/header',
            );

            $this->response->setOutput($this->render());
        }
    }

    public function applyJob()
    {
        $this->language->load('information/recruitment');
        header("Content-Type: application/json");
        $json = array();
        $v = json_decode(stripslashes(file_get_contents("php://input")), true);
        if (isset($v['job_phone']) && isset($v['job_email']) && isset($v['job_name'])) {
            $skip = true;

            if ((strlen(utf8_decode($v['job_phone'])) < 6) || (strlen(utf8_decode($v['job_phone'])) > 25)) {
                $json['error_phone'] = $this->language->get('error_phone');
                $skip = false;
            }

            if ((strlen(utf8_decode($v['job_name'])) < 2) || (strlen(utf8_decode($v['job_name'])) > 64)) {
                $json['error_name'] = $this->language->get('error_name');
                $skip = false;
            }

            if ((strlen(utf8_decode($v['job_email'])) > 96) || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $v['job_email'])) {
                $json['error_email'] = $this->language->get('error_email');
                $skip = false;
            }

            if ($skip == true) {
                $this->load->model('form/job');
                $this->model_form_job->addJob($v);
                $cv = $this->model_form_job->getCV($v['code']);
                $store_name = $this->data['store'] = nl2br($this->config->get('config_owner')[$this->config->get('config_language_id')]);
                $content = 'Có 1 hồ sơ mới được upload lên hệ thống với chi tiết như sau:';
                $content .= '<br /><b>Vị trí ứng tuyển:</b> ' . $v['position'];
                $content .= '<br /><b>Tên ứng viên:</b> ' . $v['job_name'];
                $content .= '<br /><b>Điện thoại:</b> ' . $v['job_phone'];
                $content .= '<br /><b>Email:</b> ' . $v['job_email'];
                $content .= '<br /><b>Giới thiệu:</b> ' . $v['job_intro'];
                if ($cv) {
                    $content .= '<br /><b>CV:</b> <a href="https://cmcts.com.vn/down.php?filename=' . $cv['filename'] . '&realfilename=' . $cv['name'] . '">Click để download</a>';
                }
                $content .= '<br /><br />Vui lòng kiểm tra thêm thông tin trong giao diện quản trị website';

                $mail = new Mail();
                $mail->protocol = $this->config->get('config_mail_protocol');
                $mail->parameter = $this->config->get('config_mail_parameter');
                $mail->hostname = trim($this->config->get('config_smtp_host'));
                $mail->username = trim($this->config->get('config_smtp_username'));
                $mail->password = $this->config->get('config_smtp_password');
                $mail->port = $this->config->get('config_smtp_port');
                $mail->timeout = $this->config->get('config_smtp_timeout');
                $mail->setTo($this->config->get('config_email_tuyendung'));
                if (!empty($this->config->get('config_mail_from'))) {
                    $mail->setFrom($this->config->get('config_mail_from'));
                } else {
                    $mail->setFrom($this->config->get('config_email'));
                }
                $mail->setReplyTo($v['job_email']);
                $mail->setSender($store_name);
                $mail->setSubject('Hồ sơ mới được nộp tại website');
                $mail->setHTML(html_entity_decode($content, ENT_QUOTES, 'UTF-8'));
                $mail->send();
                $json['success'] = $this->language->get('text_success');
            }

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
        }
    }

}
