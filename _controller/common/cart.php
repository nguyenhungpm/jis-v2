<?php
class ControllerCommonCart extends Controller {
    private $error = array();

    public function index() {
		$this->load->model('catalog/product');
		$this->load->model('module/dathang');

		$this->data['heading_title'] = 'Giỏ hàng của bạn';

		$this->document->setTitle('Giỏ hàng');

		// echo $this->session->data['cart'];
		// print_r($this->session->data['d']);

		if(isset($this->request->get['type'])){
			$arr = explode('-',$this->request->cookie[$this->request->get['type']]);
			if(($key = array_search($this->request->get['remove'], $arr)) !== false) {
				unset($arr[$key]);
			}

			$value = join('-',$arr);
			setcookie($this->request->get['type'], $value, time() + 60 * 60 * 24 * 30, '/', $this->request->server['HTTP_HOST']);
			$this->redirect($this->url->link('common/cart'));
		}
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

			$this->model_module_dathang->addOrder($this->request->post);

            if($this->config->get('config_order_email')){
			
				$message .= "Khách hàng: ".$this->request->post['name']."<br />";
				$message .= "Email: ".$this->request->post['email']."<br />";
				$message .= "Điện thoại: ".$this->request->post['telephone']."<br />";
				$message .= "Ghi chú: ".$this->request->post['note']."<br />";
				
				$message .= 'Các sản phẩm đặt hàng:<br />';
				foreach($this->request->post['detail'] as $product_id => $product){
					$message .= "\t-".$product['name']." - Số lượng: ". $product['quantity'] ."<br />";
				}
				
				$mail = new Mail();
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->hostname = $this->config->get('config_smtp_host');
				$mail->username = $this->config->get('config_smtp_username');
				$mail->password = $this->config->get('config_smtp_password');
				$mail->port = $this->config->get('config_smtp_port');
				$mail->timeout = $this->config->get('config_smtp_timeout');
				// $mail->setTo($this->config->get('config_email'));
				$mail->setTo('nguyenhung537@gmail.com');
				$mail->setFrom($this->request->post['email']);
				$mail->setSender($this->request->post['name']);
				$mail->setSubject(html_entity_decode('Đơn đặt hàng', ENT_QUOTES, 'UTF-8'));
				$mail->setHtml(strip_tags(html_entity_decode($message, ENT_QUOTES, 'UTF-8')));
				$mail->send();
			}
			unset($this->session->data['cart']);
            $this->redirect($this->url->link('common/success'));
        }

		if(isset($this->session->data['cart']['detail'])){
			$this->data['detail'] = $this->session->data['cart']['detail'];
		}
		
		// print_r($this->data['detail']);
		
		if (isset($this->request->post['name'])) {
            $this->data['name'] = $this->request->post['name'];
        } elseif (isset($this->session->data['cart']['name'])) {
            $this->data['name'] = $this->session->data['cart']['name'];
        } else {
            $this->data['name'] = '';
        }

        if (isset($this->request->post['email'])) {
            $this->data['email'] = $this->request->post['email'];
        } elseif (isset($this->session->data['cart']['email'])) {
            $this->data['email'] = $this->session->data['cart']['email'];
        } else {
            $this->data['email'] = '';
        }

        if (isset($this->request->post['telephone'])) {
            $this->data['telephone'] = $this->request->post['telephone'];
        } elseif (isset($this->session->data['cart']['telephone'])) {
            $this->data['telephone'] = $this->session->data['cart']['telephone'];
        } else {
            $this->data['telephone'] = '';
        }

        if (isset($this->request->post['note'])) {
            $this->data['note'] = $this->request->post['note'];
        } elseif (isset($this->session->data['cart']['note'])) {
            $this->data['note'] = $this->session->data['cart']['note'];
        } else {
            $this->data['note'] = '';
        }

		if (isset($this->error['name'])) {
            $this->data['error_name'] = $this->error['name'];
        } else {
            $this->data['error_name'] = '';
        }

        if (isset($this->error['email'])) {
            $this->data['error_email'] = $this->error['email'];
        } else {
            $this->data['error_email'] = '';
        }

        if (isset($this->error['telephone'])) {
            $this->data['error_telephone'] = $this->error['telephone'];
        } else {
            $this->data['error_telephone'] = '';
        }

		$this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home'),
            'separator' => false
        );

		$this->data['checkcart'] = 0;
		if(isset($this->request->cookie['domain']) || isset($this->request->cookie['model'])){
			$this->data['checkcart'] = 1;
		}

		$hosting = array();
		if(!empty($this->request->cookie['model'])){
			$hosting = explode('-',$this->request->cookie['model']);
		}
		$hosting = array_filter($hosting);
		$this->data['hosting'] = array();
		foreach($hosting as $model){
			$this->data['hosting'][] = $this->model_catalog_product->getProduct($model);
		}
		// print_r($hosting);
		$results = $this->model_catalog_product->getProducts(array());
		foreach ($results as $result) {
			if(in_array($result['product_id'], $hosting)) continue;
			$this->data['products'][] = array( 
				'product_id' => $result['product_id'],
				'name' => $result['name']
			);
		}

		$this->template = 'common/cart.tpl';

		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'
		);

		$this->response->setOutput($this->render());
	}

	protected function validate() {
        if ((utf8_strlen($this->request->post['telephone']) < 1) || (utf8_strlen($this->request->post['telephone']) > 15)) {
            $this->error['telephone'] = 'Số điện thoại không đúng!';
        }

        if (empty($this->request->post['name'])) {
            $this->error['name'] = 'Vui lòng nhập tên.';
        }

        if (!preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['email'])) {
            $this->error['email'] = 'Vui lòng nhập đúng địa chỉ email';
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

	public function cart(){
		$json = 1;

		if(isset($this->request->post['model']) && !empty($this->request->post['model'])){
			if(isset($this->request->post['type']) && !empty($this->request->post['type'])){
				$type = $this->request->post['type'];
			}else{
				$type = 'model';
			}

			$arr = explode('-',$this->request->cookie[$this->request->post['type']]);
			if(!in_array($this->request->post['model'], $arr)){
				$value = $this->request->cookie[$this->request->post['type']] . '-' . $this->request->post['model'];
			}else{
				$value = $this->request->cookie[$this->request->post['type']];
			}

			setcookie($type, $value, time() + 60 * 60 * 24 * 30, '/', $this->request->server['HTTP_HOST']);
			$json = 2;
		}
		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
	}

	public function calculateTotal(){
    	$json = array();
		$this->session->data['cart'] = $this->request->post;
    	$total = 0;
    	foreach ($this->request->post['detail'] as $product_id => $value) {
    		$total += $value['price'] * $value['quantity'];
    	}
        $json['total'] = $total;

		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

	public function remove(){
    	$json = array();
		$product_id = $this->request->post['product_id'];
		unset($this->session->data['cart']['detail'][$product_id]);
    	if(!empty($this->request->cookie['model'])){
			$cart = explode('-',$this->request->cookie['model']);
		}
		$cart = array_filter($cart);
		$arr_remove = array();
		$arr_remove[] = $product_id;
		$cart = array_diff($cart, $arr_remove);
		$value = join("-", $cart);
		
		setcookie('model', $value, time() + 60 * 60 * 24 * 30, '/', $this->request->server['HTTP_HOST']);
		$json = $cart;

		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
}
?>
