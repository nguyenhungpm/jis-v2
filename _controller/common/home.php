<?php
class ControllerCommonHome extends Controller {
	public function index() {

		if(strlen($this->config->get('config_title')>1)){
			$this->document->setTitle($this->config->get('config_title'));
		}else{
			$this->document->setTitle($this->config->get('config_name')[$this->config->get('config_language_id')]);
		}
		$meta_description = nl2br($this->config->get('config_meta_description')[$this->config->get('config_language_id')]);
		$meta_keyword = nl2br($this->config->get('config_meta_keyword')[$this->config->get('config_language_id')]);

		if($this->config->get('config_secure')==1){
			$this->document->addLink(HTTPS_SERVER, 'canonical');
		}else{
			$this->document->addLink(HTTP_SERVER, 'canonical');
		}

		$this->document->setMetanames('description',$meta_description);
		$this->document->setMetanames('keywords',$meta_keyword);

		$this->load->model('tool/image');
		if ($this->config->get('config_logo') && file_exists(DIR_IMAGE . $this->config->get('config_logo'))) {
			$this->data['logo'] = $this->config->get('config_logo');
		} else {
			$this->data['logo'] = '';
		}
		// echo $this->data['logo'];
		$image_og = $this->model_tool_image->cropsize($this->data['logo'], 1200,630);
		// echo $image_og;
		$this->document->setMetapros('og:image',$image_og);

		$this->data['hotline'] = $this->config->get('config_hotline');

		$this->data['heading_title'] = $this->config->get('config_name')[$this->config->get('config_language_id')];
	    // $this->load->model('catalog/type_car');
	    // $this->data['listCars'] = $this->model_catalog_type_car->getTypeCars(array());
		$this->document->addScript('/static/js/owl.carousel.min.js');
		// $this->document->addScript('/static/js/bootstrap.js');


		$this->load->model('catalog/information');
		$this->data['informations'] = $this->model_catalog_information->getSpecInformations();

			$this->load->model('catalog/news');
			$this->load->model('catalog/news_category');

			$this->load->model('tool/image');
			$data = array(
				'sort'  => 'n.viewed',
				'order' => 'DESC',
				'start' => 0,
				'limit' => 12
			);
			$this->data['newss'] = array();
			$results = $this->model_catalog_news->getNews($data);

			foreach ($results as $result) {
				$width = ''; $height = '';
				if ($result['image']) {
					$image = $this->model_tool_image->cropsize($result['image'], 324, 160);
				} else {
					$firstImgNews = $this->catchFirstImage(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'));
					if($firstImgNews == 'no_image.jpg'){
						$image = $this->model_tool_image->cropsize('no_image.jpg', 324, 160);
					} else {
						$image = $firstImgNews;
						$width = 'width="' . 324 . '"';
						$height = 'height="' . 160 . '"';
					}
				}

				$this->data['newss'][] = array(
					'news_id' 		=> $result['news_id'],
					'category_info' 		=> $this->model_catalog_news_category->getNewsCategory($result['main_cat_id']),
					'viewed' 		=> $result['viewed'],
					'author' 		=> $result['author'],
					'thumb'   	 	=> $image,
					'width'       => $width,
					'height'       => $height,
					'date_added' => $result['date_available'] != '0000-00-00 00:00:00' ? date($this->language->get('date_format_month'), strtotime($result['date_available'])) : date($this->language->get('date_format_month'), strtotime($result['date_added'])),
					'image' 		=> $result['image'],
					'name'    	 	=> $result['name'],
					'href'    	 	=> $this->url->link('news/news', 'news_id=' . $result['news_id']),
				);
			}


			// print_r($this->config->get('config_introduce'));

		// print_r($this->data['listCars']);


		$this->template = 'common/home.tpl';

		$this->children = array(
			'information/register',
			'common/content_top',
			'common/adv_top',
			'common/content_bottom',
			'common/column_left',
			'common/column_right',
			'common/slideside',
			'common/footer',
			'common/header'
		);

		$this->response->setOutput($this->render());
	}

	function catchFirstImage($content) {
		  $first_img = '';
		  $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $content, $matches);
		  if(isset($matches[1][0])){
			$first_img = $matches[1][0];
			}
		  if(empty($first_img)){ //Defines a default image
			$first_img = "no_image.jpg";
		  }
		  return $first_img;
		} //end function

	}
	
	function renewToken(){
		$json = array();
		
		$token = $this->session->data['token'];
		// $this->session->data['token'] = md5(mt_rand());
		$this->session->data['token'] = $token;

		$json['token'] = $this->session->data['token'];

		// $this->session->data['price_shipping'] = $this->request->post['price_shipping'];

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
?>
