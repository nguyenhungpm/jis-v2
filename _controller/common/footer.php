<?php
class ControllerCommonFooter extends Controller {
	protected function index() {
		$this->language->load('common/footer');

		$this->data['script_footer'] = html_entity_decode($this->config->get('config_script_footer'), ENT_QUOTES, 'UTF-8');
		$this->document->addStyle('/static/css/social.css');
		$this->data['scripts'] = $this->document->getScripts();
		$this->data['exscripts'] = $this->document->getExScripts();

		$this->data['address'] = nl2br($this->config->get('config_address')[$this->config->get('config_language_id')]);
		$this->data['giayphep'] = html_entity_decode($this->config->get('config_giayphep'), ENT_QUOTES, 'UTF-8');
		$this->data['ghichu'] = nl2br($this->config->get('config_ghichu'));
		$this->data['chinhanh'] = nl2br($this->config->get('config_chinhanh'));
		$this->data['store'] = nl2br($this->config->get('config_owner')[$this->config->get('config_language_id')]);
		$this->data['owner'] = $this->config->get('config_owner')[$this->config->get('config_language_id')];
		if(isset($this->request->get['route'])){
			$this->data['route'] = $this->request->get['route'];
		}else{
			$this->data['route'] = 'common/home';
		}

		$this->data['footer_logo'] = 'media/'.$this->config->get('config_footer_logo');
		$this->data['fax'] = $this->config->get('config_fax');
		$this->data['telephone'] = $this->config->get('config_telephone');
		// $this->data['email'] = $this->config->get('config_email');
		$this->data['email'] = preg_split('/\r\n|[\r\n]/', $this->config->get('config_email'));
		$this->data['config_name'] = $this->config->get('config_name')[$this->config->get('config_language_id')];
		$this->data['tw'] = $this->config->get('config_twitter');
		$this->data['fb'] = $this->config->get('config_facebook');
		$this->data['yt'] = $this->config->get('config_youtube');
		$this->data['in'] = $this->config->get('config_pinterest');
		$this->data['gl'] = $this->config->get('config_googleplus');
		$this->data['hotline'] = $this->config->get('config_hotline');
		$this->data['google_analytics'] = $this->config->get('config_google_analytics');

		// Whos Online
		if ($this->config->get('config_customer_online')) {
			$this->load->model('tool/online');

			if (isset($this->request->server['REMOTE_ADDR'])) {
				$ip = $this->request->server['REMOTE_ADDR'];
			} else {
				$ip = '';
			}

			if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {
				$url = 'http://' . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];
			} else {
				$url = '';
			}

			if (isset($this->request->server['HTTP_REFERER'])) {
				$referer = $this->request->server['HTTP_REFERER'];
			} else {
				$referer = '';
			}

			$this->model_tool_online->whosonline($ip, $this->customer->getId(), $url, $referer);
		}

		$this->template = 'common/footer.tpl';
		$this->children = array(
		    'common/bottom',
		    'common/footer_menu'
		);
		$this->render();
	}
}
?>
