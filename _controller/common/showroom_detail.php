<?php

class ControllerCommonShowroomDetail extends Controller {

    public function index() {
        $this->language->load('common/showroom');
        $this->load->model('catalog/showroom');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->data['text_home'] = $this->language->get('text_home');
        $this->data['text_north'] = $this->language->get('text_north');
        $this->data['text_south'] = $this->language->get('text_south');
        $this->data['text_central'] = $this->language->get('text_central');
        $this->data['text_select'] = $this->language->get('text_select');
        $this->data['text_name'] = $this->language->get('text_name');
        $this->data['text_email'] = $this->language->get('text_email');
        $this->data['text_phone'] = $this->language->get('text_phone');
        $this->data['text_address'] = $this->language->get('text_address');
        $this->data['text_district'] = $this->language->get('text_district');
        $this->data['heading_title'] = $this->language->get('heading_title');
        $this->data['no_result'] = $this->language->get('no_result');

        // order
        $this->language->load('information/contactproduct');
        if ($this->config->get('config_language_id') == 2) {
            $this->data['text'] = html_entity_decode($this->config->get('contact_textvn'));
        } else {
            $this->data['text'] = html_entity_decode($this->config->get('contact_texten'));
        }
        $this->data['telephone_support'] = html_entity_decode($this->config->get('contact_telephone'));
        $this->data['contact_buy'] = html_entity_decode($this->config->get('contact_buy'));
        $this->data['contact_free'] = html_entity_decode($this->config->get('contact_free'));
        $this->data['config_special'] = html_entity_decode($this->config->get('config_special'));
        $this->data['frees'] = $this->config->get('contact_module');

        $this->data['text_intro'] = $this->language->get('text_intro');
        $this->data['text_call'] = sprintf($this->language->get('text_call'), $this->config->get('contact_telephone'));
        $this->data['text_go'] = $this->language->get('text_go');
        $this->data['text_consult'] = $this->language->get('text_consult');
        $this->data['text_method'] = $this->language->get('text_method');
        $this->data['text_quantity'] = $this->language->get('text_quantity');
        $this->data['button_order'] = $this->language->get('button_order');
        $this->data['text_order'] = $this->language->get('text_order');
        $this->data['text_order_intro'] = $this->language->get('text_order_intro');
        $this->data['url_order'] = $this->url->link('information/contactproduct', '');
        // end order

        $provincies = $this->model_catalog_showroom->getProvinceByArea(null);
        /* var_dump($provincies); */
        $this->data['provincies'] = array();
        foreach ($provincies as $province) {
            $this->data['provincies'][] = array(
                'province_id' => $province['provinceid'],
                'name' => $province['name'],
                'name_url' => $this->url->link('common/showroom_detail')
            );
        }
        /* var_dump($this->data['provincies']); */



        if (isset($this->request->get['province_id'])) {
            $province_id = $this->request->get['province_id'];
            $this->data['province_id'] = $this->request->get['province_id'];
            $province = $this->model_catalog_showroom->getNameProvinceById($province_id);
            $this->data['province_name'] = $province['name'];
            $this->data['province_url'] = $this->url->link('common/showroom_detail', 'province_id=' . $province['provinceid']);
            $this->data['showroom_banner'] = 'media/'.$this->config->get('config_showroom_banner');
            $this->data['districts'] = array();
            $data_arr = array();
            $districts = $this->model_catalog_showroom->getDistrictByProvince($province_id);
            foreach ($districts as $district) {
                $data_arr = array(
                    'district_id' => $district['district_id']
                );
                $total_dealer = $this->model_catalog_showroom->getTotalDealer($data_arr);
                if ($total_dealer != 0) {
                    $this->data['districts'][] = array(
                        'name' => $district['name'],
                        'href' => $this->url->link('common/showroom_detail', 'district_id=' . $district['district_id'])
                    );
                }
            }

            $this->template = 'common/showroom_province.tpl';
        }
        if (isset($this->request->get['district_id'])) {
            $district_id = $this->request->get['district_id'];
            $this->data['district_id'] = $this->request->get['district_id'];

            $province = $this->model_catalog_showroom->getNameProvinceByDistrict($district_id);
            $this->data['province_name'] = $province['name'];
            $this->data['province_url'] = $this->url->link('common/showroom_detail', 'province_id=' . $province['province_id']);
            $this->data['province_id'] = $province['province_id'];

            $district = $this->model_catalog_showroom->getNameDistrictById($district_id);
            $this->data['district_name'] = $district['name'];
            $this->data['district_url'] = $this->url->link('common/showroom_detail', 'district_id=' . $district['district_id']);

            $dealers_rs = $this->model_catalog_showroom->getDealerByDistrict($district_id);
            $dealer = array();
            foreach ($dealers_rs as $dealer) {
                $this->data['dealers'][] = array(
                    'name' => $dealer['name'],
                    'address' => $dealer['address'],
                    'telephone' => $dealer['telephone'],
                    'email' => $dealer['email']
                );
            }
            $this->template = 'common/showroom_detail.tpl';
        }




        $this->children = array(
            'common/column_left',
            'common/column_right',
            'common/content_top',
            'common/content_bottom',
            'common/footer',
            'common/header'
        );

        $this->response->setOutput($this->render());
    }

}

?>
