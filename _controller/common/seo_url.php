<?php
class ControllerCommonSeoUrl extends Controller {
	public function index() {
		// Add rewrite to url class
		$this->load->model('module/redirect');

		$this->model_module_redirect->detect301Status();
		if ($this->config->get('config_seo_url')) {
			$this->url->addRewrite($this);
		}

		// Decode URL
		if (isset($this->request->get['_route_'])) {
			$parts = explode('/', $this->request->get['_route_']);
			$parts = array_filter($parts);
			$amp = in_array('amp-page', $parts) ? true : false;
			$k=0;
			$share=false;

			foreach ($parts as $part) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE keyword = '" . $this->db->escape($part) . "'");

				if ($query->num_rows) {
					$url = explode('=', $query->row['query']);
					$k++;

					if ($url[0] == 'product_id') {
						$this->request->get['product_id'] = $url[1];
					}

					if ($url[0] == 'faq_id') {
						$this->request->get['faq_id'] = $url[1];
					}

					if ($url[0] == 'category_id') {
						if (!isset($this->request->get['path'])) {
							$this->request->get['path'] = $url[1];
						} else {
							$this->request->get['path'] .= '_' . $url[1];
						}
					}
					// --- START : OSD.VN CUSTOMIZATION [1]
					if ($url[0] == 'recruitment_id') {
						$this->request->get['recruitment_id'] = $url[1];
					}
					if ($url[0] == 'document_id') {
						$this->request->get['document_id'] = $url[1];
					}
					if ($url[0] == 'doccat_id') {
						$this->request->get['doccat_id'] = $url[1];
					}
					if ($url[0] == 'news_id') {
						$this->request->get['news_id'] = $url[1];
					}

					if ($url[0] == 'news_category_id') {
						if (!isset($this->request->get['cat_id'])) {
							$this->request->get['cat_id'] = $url[1];
						} else {
							$this->request->get['cat_id'] .= '_' . $url[1];
						}
					}
					// --- END : OSD.VN CUSTOMIZATION [1] */
					if ($url[0] == 'manufacturer_id') {
						$this->request->get['manufacturer_id'] = $url[1];
					}

					if ($url[0] == 'information_id') {
						$this->request->get['information_id'] = $url[1];
					}

					if ($url[0] == 'admission_id') {
						$this->request->get['admission_id'] = $url[1];
					}
				} elseif(strpos($part, '@') !== false) {
					$this->request->get['route'] = 'error/not_found';
					$this->request->get['email'] = $part;
					$share = true;
				} else {
					$this->request->get['route'] = 'error/not_found';
				}
			}
			if (isset($this->request->get['product_id'])) {
				if($amp){
					$this->request->get['route'] = 'amp/product';
				}else{
					$this->request->get['route'] = 'product/product';
				}
			} elseif (isset($this->request->get['faq_id'])) {
				$this->request->get['route'] = 'information/faqs/detail';
			} elseif ($this->request->get['_route_'] ==  'faqs') {
				$this->request->get['route'] =  'information/faqs';
			} elseif ($this->request->get['_route_'] ==  'sitemap') {
				$this->request->get['route'] =  'information/sitemap';
			} elseif ($this->request->get['_route_'] ==  'contact') {
				$this->request->get['route'] =  'information/contact';
			} elseif ($this->request->get['_route_'] ==  'visit') {
				$this->request->get['route'] =  'information/visit';
			} elseif ($this->request->get['_route_'] ==  'insights') {
				$this->request->get['route'] =  'information/document';
			} elseif ($this->request->get['_route_'] ==  'jobs') {
				$this->request->get['route'] =  'information/recruitment';
			} elseif ($this->request->get['_route_'] ==  'tuyen-dung') {
				$this->request->get['route'] =  'information/recruitment';
			} elseif ($this->request->get['_route_'] ==  'search') {
				$this->request->get['route'] =  'product/search';
			} elseif (isset($this->request->get['path'])) {
				$this->request->get['route'] = 'product/category';
			} elseif (isset($this->request->get['news_id'])) {
				if($amp){
					$this->request->get['route'] = 'amp/news';
				}else{
					$this->request->get['route'] = 'news/news';
				}

			} elseif (isset($this->request->get['cat_id'])) {
				$this->request->get['route'] = 'news/news_category';
			} elseif (isset($this->request->get['recruitment_id'])) {
				$this->request->get['route'] = 'information/recruitment/info';
			} elseif (isset($this->request->get['document_id'])) {
				$this->request->get['route'] = 'information/document/info';
			} elseif (isset($this->request->get['manufacturer_id'])) {
				$this->request->get['route'] = 'product/manufacturer/info';
			} elseif (isset($this->request->get['information_id'])) {
				if($amp){
					$this->request->get['route'] = 'amp/information';
				}else{
					$this->request->get['route'] = 'information/information';
				}
			} elseif (isset($this->request->get['admission_id'])) {
				if($amp){
					$this->request->get['route'] = 'amp/admission';
				}else{
					$this->request->get['route'] = 'information/admission';
				}

			} else {
				$this->request->get['route'] = $this->request->get['_route_'];
			}

			if($k != count($parts) && $k!=0 && !$amp && !$share){
				$this->request->get['route'] = 'error/not_found';
			}
			// echo $this->request->get['route'];

			if (isset($this->request->get['route'])) {
				return $this->forward($this->request->get['route']);
			}
		}
	}

	public function rewrite($link) {
		$url_info = parse_url(str_replace('&amp;', '&', $link));

		$url = '';
		$share = '';

		$data = array();

		if(isset($url_info['query'])){
			parse_str($url_info['query'], $data);
		}

		foreach ($data as $key => $value) {
			if ($key == 'email') { $share = $value; }
			if (isset($data['route'])) {
				if (($data['route'] == 'product/product' && $key == 'product_id') || (($data['route'] == 'product/manufacturer/info' || $data['route'] == 'product/product') && $key == 'manufacturer_id') || ($data['route'] == 'information/information' && $key == 'information_id') || ($data['route'] == 'information/admission' && $key == 'admission_id')/*osd.vn*/ || ($data['route'] == 'news/news' && $key == 'news_id') /* osd.vn*/) {
					$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = '" . $this->db->escape($key . '=' . (int)$value) . "'");

					if ($query->num_rows) {
						//$url .= '/' . $query->row['keyword'];
						// remove category from URL
						$url = '/' . $query->row['keyword'];

						unset($data[$key]);
					}
					} elseif (isset($data['route']) && $data['route'] ==   'common/home') {
                $url .=  '/';
                } elseif (isset($data['route']) && $data['route'] ==   'information/contact') {
                $url .=  '/contact';
                } elseif (isset($data['route']) && $data['route'] ==   'information/visit') {
                $url .=  '/visit';
                } elseif (isset($data['route']) && $data['route'] ==   'information/sitemap') {
                $url .=  '/sitemap';
                } elseif (isset($data['route']) && $data['route'] ==   'information/faqs') {
                $url .=  '/faqs';
                } elseif (isset($data['route']) && $data['route'] ==   'product/search') {
                $url .=  '/search';
				} elseif ($key == 'path') {
					$categories = explode('_', $value);

					foreach ($categories as $category) {
						$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = 'category_id=" . (int)$category . "'");

						if ($query->num_rows) {
							$url = '/' . $query->row['keyword'];
						}
					}

					unset($data[$key]);
				}// --- START : OSD.VN CUSTOMIZATION [3]
				elseif ($key == 'cat_id') {
					$news_category = explode('_', $value);

					foreach ($news_category as $category) {
						$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = 'news_category_id=" . (int)$category . "'");

						if ($query->num_rows) {
							$url .= '/' . $query->row['keyword'];
						}
					}

					unset($data[$key]);
				} elseif (($data['route'] == 'product/amp_product' && $key == 'product_id') || ($data['route'] == 'news/news_amp' && $key == 'news_id') || ($data['route'] == 'information/info_amp' && $key == 'information_id') || ($data['route'] == 'information/info_amp' && $key == 'admission_id')) {
					$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = '" . $this->db->escape($key . '=' . (int)$value) . "'");
					if(isset($query->row['keyword'])){
						$url .= '/amp-page/' . $query->row['keyword'];
					}else{
						$url .= '/amp-page&' . $key . '=' . $value;
					}

					unset($data[$key]);
				}
				// --- END : OSD.VN CUSTOMIZATION [3] /
			}
		}

		if (!empty($share)) {
			$url = '/'.$share.$url;
		}
		// echo $share;

		if ($url) {
			unset($data['route']);

			$query = '';

			if ($data) {
				foreach ($data as $key => $value) {
					if($key != 'email'){
						$query .= '&' . $key . '=' . $value;
					}
				}

				if ($query) {
					$query = '?' . trim($query, '&');
				}
			}

			return $url_info['scheme'] . '://' . $url_info['host'] . (isset($url_info['port']) ? ':' . $url_info['port'] : '') . str_replace('/index.php', '', $url_info['path']) . $url . $query;
		} else {

			if(strpos($link,'index.php?route=common/home')){
				$link = str_replace('index.php?route=common/home','',$link);
			}
				$link = str_replace('index.php?route=', '', $link);
			return $link;
		}
	}
}
?>
