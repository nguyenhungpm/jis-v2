<?php 
class ControllerAccountLogin extends Controller {
	private $error = array();

	public function index() {
		
		$json = '';

		$this->load->model('account/customer');

		$error = [];

		if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['email'])) {
			$json .= '1,';
			$error[] = 'Định dạng email chưa đúng';
		}

		if ((utf8_strlen($this->request->post['password']) < 4) || (utf8_strlen($this->request->post['password']) > 20)) {
			$json .= '2,';
			$error[] = 'Mật khẩu có độ dài trong khoảng từ 3-20 ký tự';
		}

		if($json=='' && !$this->customer->login($this->request->post['email'], $this->request->post['password'])){
			$json .= '6,';
			$error[] = 'Email hoặc mật khẩu không đúng';
		}

		if($json==''){

			$this->customer->login($this->request->post['email'], $this->request->post['password']);

			unset($this->session->data['guest']);
		}
		
		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($error));

	}
	
	public function info() {
		
		// $json = 'hungnv';

		// $this->load->model('account/customer');

		// if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['email'])) {
			// $json .= '1,';
		// }

		// if ((utf8_strlen($this->request->post['password']) < 4) || (utf8_strlen($this->request->post['password']) > 20)) {
			// $json .= '2,';
		// }

		// if($json=='' && !$this->customer->login($this->request->post['email'], $this->request->post['password'])){
			// $json .= '6,';
		// }

		// if($json==''){

			// $this->customer->login($this->request->post['email'], $this->request->post['password']);

			// unset($this->session->data['guest']);
		// }
		
		$json = $this->request->post['password'];
		
		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));

	}
}
?>