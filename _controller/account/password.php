<?php 
class ControllerAccountPassword extends Controller {
	private $error = array();

	public function index() {
		
		$this->language->load('account/password');
		$this->load->model('account/customer');

		$this->document->setTitle('Tài khoản'); 
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			if($this->customer->isLogged()){
				$this->model_account_customer->editPassword($this->customer->getEmail(), $this->request->post['password']);
			}else{
				$this->language->load('mail/forgotten');

				$password = substr(sha1(uniqid(mt_rand(), true)), 0, 10);

				$this->model_account_customer->editPassword($this->request->post['email'], $password);

				$subject = sprintf($this->language->get('text_subject'), $this->config->get('config_name')[$this->config->get('config_language_id')]);

				$message  = sprintf($this->language->get('text_greeting'), $this->config->get('config_name')[$this->config->get('config_language_id')]) . "\n\n";
				$message .= $this->language->get('text_password') . "\n\n";
				$message .= $password;

				$mail = new Mail();
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->hostname = $this->config->get('config_smtp_host');
				$mail->username = $this->config->get('config_smtp_username');
				$mail->password = $this->config->get('config_smtp_password');
				$mail->port = $this->config->get('config_smtp_port');
				$mail->timeout = $this->config->get('config_smtp_timeout');				
				$mail->setTo($this->request->post['email']);
				$mail->setFrom($this->config->get('config_email'));
				$mail->setSender($this->config->get('config_name')[$this->config->get('config_language_id')]);
				$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
				$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
				$mail->send();
			}
			
			$this->redirect($this->url->link('account/password/success', '', 'SSL'));
		}
		
		if (isset($this->error['old_password'])) { 
			$this->data['error_old_password'] = $this->error['old_password'];
		} else {
			$this->data['error_old_password'] = '';
		}
		
		if (isset($this->error['password'])) { 
			$this->data['error_password'] = $this->error['password'];
		} else {
			$this->data['error_password'] = '';
		}

		if (isset($this->error['warning'])) { 
			$this->data['error_email'] = $this->error['warning'];
		} else {
			$this->data['error_email'] = '';
		}

		if (isset($this->error['confirm'])) { 
			$this->data['error_confirm'] = $this->error['confirm'];
		} else {
			$this->data['error_confirm'] = '';
		}

		$this->data['action'] = $this->url->link('account/password', '', 'SSL');

		if (isset($this->request->post['old_password'])) {
			$this->data['old_password'] = $this->request->post['old_password'];
		} else {
			$this->data['old_password'] = '';
		}

		if (isset($this->request->post['email'])) {
			$this->data['email'] = $this->request->post['email'];
		} else {
			$this->data['email'] = '';
		}

		if (isset($this->request->post['password'])) {
			$this->data['password'] = $this->request->post['password'];
		} else {
			$this->data['password'] = '';
		}

		if (isset($this->request->post['confirm'])) {
			$this->data['confirm'] = $this->request->post['confirm'];
		} else {
			$this->data['confirm'] = '';
		}
		
		

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Xác thực tài khoản',
			'href'      => $this->url->link('information/contact'),
			'separator' => $this->language->get('text_separator')
		);

		$this->data['heading_title'] = 'Tài khoản';

		

		$this->data['button_continue'] = $this->language->get('button_continue');

		$this->data['continue'] = $this->url->link('common/home');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/password.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/account/password.tpl';
		} else {
			$this->template = 'default/template/account/password.tpl';
		}

		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/slideside',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'
		);

		$this->response->setOutput($this->render());

	}
	
	protected function validate() {
		
		if($this->customer->isLogged()){
			if(!$this->model_account_customer->checkPassword($this->request->post['old_password'])){
				$this->error['old_password'] = $this->language->get('error_old_password');
			}
			
			if ((utf8_strlen($this->request->post['password']) < 4) || (utf8_strlen($this->request->post['password']) > 20)) {
				$this->error['password'] = $this->language->get('error_password');
			}
			
			if ($this->request->post['confirm'] != $this->request->post['password']) {
				$this->error['confirm'] = $this->language->get('error_confirm');
			}
		}else{
			if (!isset($this->request->post['email'])) {
				$this->error['warning'] = $this->language->get('error_email');
			} elseif (!$this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
				$this->error['warning'] = $this->language->get('error_email');
			}
		}

		// if (empty($this->session->data['captcha']) || ($this->session->data['captcha'] != $this->request->post['captcha'])) {
			// $this->error['captcha'] = $this->language->get('error_captcha');
		// }

		if (!$this->error) {
			return true;
		} else {
			return false;
		}  	  
	}
	
	public function success(){
		
		if($this->customer->isLogged()){
			$this->language->load('account/password');
		}else{
			$this->language->load('account/forgotten');
		}
		
		$this->data['text_message'] = $this->language->get('text_message');

		$this->document->setTitle('Thành công'); 

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('information/contact'),
			'separator' => $this->language->get('text_separator')
		);

		$this->data['heading_title'] = $this->language->get('heading_title');

		

		$this->data['button_continue'] = $this->language->get('button_continue');

		$this->data['continue'] = $this->url->link('common/home');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/success.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/common/success.tpl';
		} else {
			$this->template = 'default/template/common/success.tpl';
		}

		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/slideside',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'
		);

		$this->response->setOutput($this->render());
	}
}
?>