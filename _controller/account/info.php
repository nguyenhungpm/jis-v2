<?php
class ControllerAccountInfo extends Controller {
	public function index() {
		$this->language->load('account/info');

		$this->load->model('account/group');
    if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}
    $this->data['base'] = $server;

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),
			'separator' => false
		);

		$this->data['groups'] = array();
    $results = $this->model_account_group->getGroups(0);
    foreach ($results as $result) {
        $this->data['groups'][] = array(
            'name'   => $result['name'],
            'href'   => $this->url->link('account/test', 'group_id=' . $result['group_id'])
        );
    }


		$this->template = 'account/info.tpl';

		$this->children = array(
			'module/account',
			'common/column_left',
			'common/column_right',
			'common/slideside',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'
		);

		$this->response->setOutput($this->render());
	}
}
?>
