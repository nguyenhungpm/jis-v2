<?php
class ControllerAccountRegister extends Controller {
	private $error = array();

	public function index() {

		$json = '';
		$error = [];

		$this->load->model('account/customer');

		if ((utf8_strlen($this->request->post['lastname']) < 1) || (utf8_strlen($this->request->post['lastname']) > 32)) {
			$json .= '4,';
			$error[] = $this->language->get('error_lastname');
		}

		if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['email'])) {
			$json .= '1,';
			$error[] = $this->language->get('error_email');
		}

		if ($this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
			$json .= '8,';
			$error[] = $this->language->get('error_email_duplicate');
		}

		if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
			$json .= '5,';
			$error[] = $this->language->get('error_telephone');
		}

		if ((utf8_strlen($this->request->post['password']) < 4) || (utf8_strlen($this->request->post['password']) > 20)) {
			$json .= '2,';
			$error[] = $this->language->get('error_password');
		}

		if ($this->request->post['confirm'] != $this->request->post['password']) {
			$json .= '3,';
			$error[] = $this->language->get('error_confirm');
		}

		// if (empty($this->session->data['captcha']) || ($this->session->data['captcha'] != $this->request->post['captcha'])) {
		// 	$json .= '7,';
		// }

		if($json==''){
			// $this->model_account_customer->addCustomer($this->request->post);

			// $this->customer->login($this->request->post['email'], $this->request->post['password']);

			// unset($this->session->data['guest']);
		}
		if (empty($error)) {
			$this->model_account_customer->addCustomer($this->request->post);
		}

		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($error));

	}
}
?>
