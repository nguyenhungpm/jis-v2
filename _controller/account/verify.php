<?php 
class ControllerAccountVerify extends Controller {
	private $error = array();

	public function index() {
		
		$this->language->load('information/contact');
		
		if(isset($this->request->get['token'])){
			$this->load->model('account/customer');
			if($this->model_account_customer->verify($this->request->get['token'])){
				$this->data['text_message'] = 'Tài khoản của bạn đã được xác thực thành công. Vui lòng đăng nhập để tiếp tục sử dụng website của chúng tôi.';
			}else{
				$this->data['text_message'] = 'Xác thực tài khoản không thành công. Vui lòng <a class="color-orange" href="information/contact">liên hệ với chúng tôi</a> để xác thực tài khoản.';
			}
		}else{
			$this->data['text_message'] = 'Xác thực tài khoản không thành công. Vui lòng <a class="color-orange" href="information/contact">liên hệ với chúng tôi</a> để xác thực tài khoản.';
		}

		$this->document->setTitle('Xác thực tài khoản'); 

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Xác thực tài khoản',
			'href'      => $this->url->link('information/contact'),
			'separator' => $this->language->get('text_separator')
		);

		$this->data['heading_title'] = 'Xác thực tài khoản';

		

		$this->data['button_continue'] = $this->language->get('button_continue');

		$this->data['continue'] = $this->url->link('common/home');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/success.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/common/success.tpl';
		} else {
			$this->template = 'default/template/common/success.tpl';
		}

		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/slideside',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'
		);

		$this->response->setOutput($this->render());

	}
}
?>