<?php
class ControllerAccountTest extends Controller
{
    public function index()
    {
        $this->language->load('account/info');
        
        $this->load->model('account/group');
        $this->load->model('account/quiz');
        $this->load->model('tool/t2vn');
        if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
            $server = $this->config->get('config_ssl');
        } else {
            $server = $this->config->get('config_url');
        }
        $this->data['base'] = $server;
        
        $this->data['breadcrumbs'] = array();
        
        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home'),
            'separator' => false
        );
        
        
        
        
        $group_id = $this->request->get['group_id'];
        
        $group_info               = $this->model_account_group->getGroup($group_id);
        $this->data['group_info'] = $group_info;
        $this->data['time_test']  = $group_info['time_test'];
        $this->data['group_id']   = $group_id;
        
        $this->data['quizs'] = array();
        
        $data = array(
            'filter_group_id' => $group_id,
            'filter_sub_group' => true
        );
        
        $results = $this->model_account_quiz->getQuizs($data);
        
        foreach ($results as $result) {
            if ($result['image']) {
                $image1 = 'media/' . $result['image'];
            } else {
                $image1 = false;
            }
            
            $limit_text = 120;
            
            if ($result['short_description']) {
                $short_description = $this->model_tool_t2vn->cut_string(html_entity_decode($result['short_description'], ENT_QUOTES, 'UTF-8'), $limit_text);
            } else {
                $short_description = $this->model_tool_t2vn->cut_string(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'), $limit_text);
            }
            
            $questions = array();
            $i_results = $this->model_account_quiz->getQuizImages($result['quiz_id']);
            foreach ($i_results as $question) {
                $questions[] = array(
                    'dungsai' => $question['dungsai'],
                    'question' => $question['question'],
                    'sort_order' => $question['sort_order'],
                    'quiz_id' => $question['quiz_id'],
                    'question_id' => $question['quiz_image_id']
                );
            }
            
            $this->data['quizs'][] = array(
                'quiz_id' => $result['quiz_id'],
                'thumb' => $image1,
                'questions' => $questions,
                'name' => $result['name'],
                'description' => $short_description
            );
        }
        
        // print_r($this->data['quizs']);
        
        
        $this->template = 'account/test.tpl';
        
        $this->children = array(
            'common/column_left',
            'common/column_right',
            'common/slideside',
            'common/content_top',
            'common/content_bottom',
            'common/footer',
            'common/header'
        );
        
        $this->response->setOutput($this->render());
    }
    
    public function submit()
    {
        $json = array();
        
        $json['query'] = $this->request->post;
        $json['error'] = array();
        
        // if(utf8_strlen($this->request->post['name']) < 3){
        //   $json['error']['name'] = 'Vui lòng nhập Họ tên người học';
        // }
        //
        // if(utf8_strlen($this->request->post['number']) < 3){
        //   $json['error']['number'] = 'Vui lòng nhập Năm sinh';
        // }
        //
        // if(utf8_strlen($this->request->post['role']) < 1){
        //   $json['error']['role'] = 'Vui lòng chọn Giới tính';
        // }
        //
        // if(utf8_strlen($this->request->post['company']) < 3){
        //   $json['error']['company'] = 'Vui lòng nhập Tên trường/lớp hiện tại';
        // }
        //
        // if(utf8_strlen($this->request->post['address']) < 3){
        //   $json['error']['address'] = 'Vui lòng nhập Địa chỉ';
        // }
        //
        // if(utf8_strlen($this->request->post['knowfrom']) < 3){
        //   $json['error']['knowfrom'] = 'Vui lòng chọn Kênh thông tin';
        // }
        //
        // if(utf8_strlen($this->request->post['note']) < 3){
        //   $json['error']['note'] = 'Vui lòng nhập Ghi chú';
        // }
        //
        // if(!isset($this->request->post['agree']) || $this->request->post['agree'] == 0){
        //   $json['error']['agree'] = $this->request->post['agree'] . ' Vui lòng xem các điều khoản trước khi xác nhận';
        // }
        
        if (empty($json['error'])) {
            $sql = "INSERT INTO " . DB_PREFIX . "answer_submit SET date_added=NOW()";
            // $sql .= ", address = '" . $this->db->escape($this->request->post['address']) . "'";
            // $sql .= ", company = '" . $this->db->escape($this->request->post['company']) . "'";
            // $sql .= ", email = '" . $this->db->escape($this->request->post['email']) . "'";
            // $sql .= ", facebook = '" . $this->db->escape($this->request->post['facebook']) . "'";
            // $sql .= ", parent = '" . $this->db->escape($this->request->post['parent']) . "'";
            // $sql .= ", knowfrom = '" . $this->db->escape($this->request->post['knowfrom']) . "'";
            // $sql .= ", name = '" . $this->db->escape($this->request->post['name']) . "'";
            // $sql .= ", note = '" . $this->db->escape($this->request->post['note']) . "'";
            // $sql .= ", number = '" . $this->db->escape($this->request->post['number']) . "'";
            // $sql .= ", role = '" . $this->db->escape($this->request->post['role']) . "'";
            // $sql .= ", skype = '" . $this->db->escape($this->request->post['skype']) . "'";
            // $sql .= ", phone = '" . $this->db->escape($this->request->post['phone']) . "'";
            $sql .= ", group_id = '" . $this->db->escape($this->request->post['group_id']) . "'";
            $sql .= ", quest = '" . $this->db->escape(serialize($this->request->post['quest'])) . "'";
            $this->db->query($sql);
            
            
            
            // $content = $this->request->post;
            // $this->data['content'] = $this->request->post;
            // $this->session->data['content_email'] = $this->request->post;
            //
            // $this->load->model('catalog/group');
            // $this->load->model('catalog/product');
            // $this->data['products'] = array();
            //
            // $data = array(
            //     // 'filter_group_id' => $content['group_id'],
            //     'filter_group_id' => $this->data['content']['group_id'],
            //     'filter_sub_group' => true
            // );
            // $results = $this->model_catalog_product->getProducts($data);
            // $this->data['empty'] = 0;
            // $this->data['true'] = 0;
            // $this->data['false'] = 0;
            //
            // foreach ($results as $result) {
            //     if ($result['image']) {
            //         $image1 = 'media/'.$result['image'];
            //     } else {
            //         $image1 = false;
            //     }
            //     if(!isset($content['quest'][$result['product_id']])){
            //       $this->data['empty'] +=1;
            //     }
            //
            //     $questions = array();
            //     $i_results = $this->model_catalog_product->getProductImages($result['product_id']);
            //     foreach ($i_results as $question) {
            //       if(!isset($content['quest'][$result['product_id']])){
            //         $color = '333';
            //       }elseif($question['question']==$content['quest'][$result['product_id']] && $question['dungsai'] == 1){
            //         $color = '21ea21';
            //         $this->data['true'] +=1;
            //       }elseif($question['question']==$content['quest'][$result['product_id']] && $question['dungsai'] == 0){
            //         $color = 'ea213d';
            //         $this->data['false'] +=1;
            //       }else{
            //         $color = '333';
            //       }
            //         $questions[] = array(
            //             'dungsai' => $question['dungsai'],
            //             'question' => $question['question'],
            //             'sort_order' => $question['sort_order'],
            //             'product_id' => $question['product_id'],
            //             'question_id' => $question['product_image_id'],
            //             'color' => $color
            //         );
            //     }
            //
            //     // $products[] = array(
            //     $this->data['products'][] = array(
            //         'product_id' => $result['product_id'],
            //         'thumb' => $image1,
            //         'questions' => $questions,
            //         'name' => $result['name'],
            //     );
            // }
            //
            // $this->template = 'product/content_email.tpl';
            //
            // $store_name = $this->data['store'] = nl2br($this->config->get('config_owner')[$this->config->get('config_language_id')]);
            //
            // $content_email = $this->render();
            // $subject = "[Home UK English 4.0] " . $content['name'] . " test '" . $content['group_name'] . "'";
            //
            // $mail = new Mail();
            // $mail->protocol = $this->config->get('config_mail_protocol');
            // $mail->parameter = $this->config->get('config_mail_parameter');
            // $mail->hostname = trim($this->config->get('config_smtp_host'));
            // $mail->username = trim($this->config->get('config_smtp_username'));
            // $mail->password = $this->config->get('config_smtp_password');
            // $mail->port = $this->config->get('config_smtp_port');
            // $mail->timeout = $this->config->get('config_smtp_timeout');
            // $mail->setTo($this->config->get('config_email'));
            // // $mail->setTo('nguyenhung537@gmail.com');
            // if(!empty($this->config->get('config_mail_from'))){
            //   $mail->setFrom($this->config->get('config_mail_from'));
            // }else{
            //   $mail->setFrom($this->config->get('config_email'));
            // }
            // $mail->setReplyTo($this->request->post['email']);
            // $mail->setSender($store_name);
            // $mail->setSubject($subject);
            // $mail->setHtml(html_entity_decode($content_email, ENT_QUOTES, 'UTF-8'));
            // $mail->send();
        }
        
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
}
?>