<?php 
class ControllerAccountLogout extends Controller {
	public function index() {
		$json = '';
		if ($this->customer->isLogged()) {
			$this->customer->logout();
		}

		// $this->response->addHeader('Content-Type: application/json');
        // $this->response->setOutput(json_encode($json));
		$this->redirect($this->url->link('common/home', '', 'SSL'));
	}
}
?>