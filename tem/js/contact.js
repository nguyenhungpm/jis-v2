$(function() {

 $(document).ready(function() {
  //toggle the component with class accordion_body
  $(".accordion_head").click(function() {
    if ($('.accordion_body').is(':visible')) {
      $(".accordion_body").slideUp(300);
      $(".plusminus").text('+');
    }
    if ($(this).next(".accordion_body").is(':visible')) {
      $(this).next(".accordion_body").slideUp(300);
      $(this).children(".plusminus").text('+');
    } else {
      $(this).next(".accordion_body").slideDown(300);
      $(this).children(".plusminus").text('-');
    }
  });
});


// Menu-scroll

 jQuery(window).scroll(function () {
        var top = jQuery(document).scrollTop();
        var height = 100;
        //alert(batas);
        if (top > height) {
            jQuery('.navbar-fixed-top').addClass('menu-scroll animated slideInDown');
        } else {
            jQuery('.navbar-fixed-top').removeClass('menu-scroll animated slideInDown');
        }
    });

// End-Menu-scroll

/////////////////////Search-header
    $('.search-toggle').addClass('closed');
    $('.search-toggle .search-icon').click(function(e) {
      if ($('.search-toggle').hasClass('closed')) {
        $('.search-toggle').removeClass('closed').addClass('opened');
        $('.search-toggle, .search-container').addClass('opened');
        $('#search-terms').focus();
    } else {
        $('.search-toggle').removeClass('opened').addClass('closed');
        $('.search-toggle, .search-container').removeClass('opened');
    }
});
/////////////////////End-Search-header


// SLIDE-BANNER-TOP SLIDE-BANNER-TOP SLIDE-BANNER-TOP SLIDE-BANNER-TOP////////////////////////////

(function() {

  var $$ = function(selector, context) {
    var context = context || document;
    var elements = context.querySelectorAll(selector);
    return [].slice.call(elements);
  };

  function _fncSliderInit($slider, options) {
    var prefix = ".fnc-";

    var $slider = $slider;
    var $slidesCont = $slider.querySelector(prefix + "slider__slides");
    var $slides = $$(prefix + "slide", $slider);
    var $controls = $$(prefix + "nav__control", $slider);
    var $controlsBgs = $$(prefix + "nav__bg", $slider);
    var $progressAS = $$(prefix + "nav__control-progress", $slider);

    var numOfSlides = $slides.length;
    var curSlide = 1;
    var sliding = false;
    var slidingAT = +parseFloat(getComputedStyle($slidesCont)["transition-duration"]) * 1000;
    var slidingDelay = +parseFloat(getComputedStyle($slidesCont)["transition-delay"]) * 1000;

    var autoSlidingActive = true;
    var autoSlidingTO;
    var autoSlidingDelay = 3000; // default autosliding delay value
    var autoSlidingBlocked = false;

    var $activeSlide;
    var $activeControlsBg;
    var $prevControl;

    function setIDs() {
      $slides.forEach(function($slide, index) {
        $slide.classList.add("fnc-slide-" + (index + 1));
      });

      $controls.forEach(function($control, index) {
        $control.setAttribute("data-slide", index + 1);
        $control.classList.add("fnc-nav__control-" + (index + 1));
      });

      $controlsBgs.forEach(function($bg, index) {
        $bg.classList.add("fnc-nav__bg-" + (index + 1));
      });
    };

    setIDs();

    function afterSlidingHandler() {
      $slider.querySelector(".m--previous-slide").classList.remove("m--active-slide", "m--previous-slide");
      $slider.querySelector(".m--previous-nav-bg").classList.remove("m--active-nav-bg", "m--previous-nav-bg");

      $activeSlide.classList.remove("m--before-sliding");
      $activeControlsBg.classList.remove("m--nav-bg-before");
      $prevControl.classList.remove("m--prev-control");
      $prevControl.classList.add("m--reset-progress");
      var triggerLayout = $prevControl.offsetTop;
      $prevControl.classList.remove("m--reset-progress");

      sliding = false;
      var layoutTrigger = $slider.offsetTop;

      if (autoSlidingActive && !autoSlidingBlocked) {
        setAutoslidingTO();
      }
    };

    function performSliding(slideID) {
      if (sliding) return;
      sliding = true;
      window.clearTimeout(autoSlidingTO);
      curSlide = slideID;

      $prevControl = $slider.querySelector(".m--active-control");
      $prevControl.classList.remove("m--active-control");
      $prevControl.classList.add("m--prev-control");
      $slider.querySelector(prefix + "nav__control-" + slideID).classList.add("m--active-control");

      $activeSlide = $slider.querySelector(prefix + "slide-" + slideID);
      $activeControlsBg = $slider.querySelector(prefix + "nav__bg-" + slideID);

      $slider.querySelector(".m--active-slide").classList.add("m--previous-slide");
      $slider.querySelector(".m--active-nav-bg").classList.add("m--previous-nav-bg");

      $activeSlide.classList.add("m--before-sliding");
      $activeControlsBg.classList.add("m--nav-bg-before");

      var layoutTrigger = $activeSlide.offsetTop;

      $activeSlide.classList.add("m--active-slide");
      $activeControlsBg.classList.add("m--active-nav-bg");

      setTimeout(afterSlidingHandler, slidingAT + slidingDelay);
    };

    function controlClickHandler() {
      if (sliding) return;
      if (this.classList.contains("m--active-control")) return;
      if (options.blockASafterClick) {
        autoSlidingBlocked = true;
        $slider.classList.add("m--autosliding-blocked");
      }

      var slideID = +this.getAttribute("data-slide");

      performSliding(slideID);
    };

    $controls.forEach(function($control) {
      $control.addEventListener("click", controlClickHandler);
    });

    function setAutoslidingTO() {
      window.clearTimeout(autoSlidingTO);
      var delay = +options.autoSlidingDelay || autoSlidingDelay;
      curSlide++;
      if (curSlide > numOfSlides) curSlide = 1;

      autoSlidingTO = setTimeout(function() {
        performSliding(curSlide);
      }, delay);
    };

    if (options.autoSliding || +options.autoSlidingDelay > 0) {
      if (options.autoSliding === false) return;

      autoSlidingActive = true;
      setAutoslidingTO();

      $slider.classList.add("m--with-autosliding");
      var triggerLayout = $slider.offsetTop;

      var delay = +options.autoSlidingDelay || autoSlidingDelay;
      delay += slidingDelay + slidingAT;

      $progressAS.forEach(function($progress) {
        $progress.style.transition = "transform " + (delay / 1000) + "s";
      });
    }

    $slider.querySelector(".fnc-nav__control:first-child").classList.add("m--active-control");

  };

  var fncSlider = function(sliderSelector, options) {
    var $sliders = $$(sliderSelector);

    $sliders.forEach(function($slider) {
      _fncSliderInit($slider, options);
    });
  };

  window.fncSlider = fncSlider;
}());

fncSlider(".example-slider", {
  autoSlidingDelay: 3000
});

var $demoCont = document.querySelector(".demo-cont");

// [].slice.call(document.querySelectorAll(".fnc-slide__action-btn")).forEach(function($btn) {
//   $btn.addEventListener("click", function() {
//     $demoCont.classList.toggle("credits-active");
//   });
// });

// document.querySelector(".demo-cont__credits-close").addEventListener("click", function() {
//   $demoCont.classList.remove("credits-active");
// });

// document.querySelector(".js-activate-global-blending").addEventListener("click", function() {
//   document.querySelector(".example-slider").classList.toggle("m--global-blending-active");
// });

// END-SLIDE-BANNER-TOP // END-SLIDE-BANNER-TOP // END-SLIDE-BANNER-TOP // END-SLIDE-BANNER-TOP







    $("input,textarea").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // additional error messages or events
        },
        submitSuccess: function($form, event) {
            event.preventDefault(); // prevent default submit behaviour
            // get values from FORM
            var name = $("input#name").val();
            var email = $("input#email").val();
            var phone = $("input#phone").val();
            var message = $("textarea#message").val();
            var firstName = name; // For Success/Failure Message
            // Check for white space in name for Success/Fail message
            if (firstName.indexOf(' ') >= 0) {
                firstName = name.split(' ').slice(0, -1).join(' ');
            }
            $.ajax({
                url: "../submit.php",
                type: "POST",
                data: {
                    name: name,
                    phone: phone,
                    email: email,
                    message: message
                },
                cache: false,
                success: function() {
                    // Success message
                    $('#success').html("<div class='alert alert-success'>");
                    $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                    .append("</button>");
                    $('#success > .alert-success')
                    .append("<strong>Your message has been sent. </strong>");
                    $('#success > .alert-success')
                    .append('</div>');

                    //clear all fields
                    $('#contact-form').trigger("reset");
                },
                error: function() {
                    // Fail message
                    $('#success').html("<div class='alert alert-danger'>");
                    $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                    .append("</button>");
                    $('#success > .alert-danger').append("<strong>Sorry " + firstName + ", it seems that my mail server is not responding. Please try again later!");
                    $('#success > .alert-danger').append('</div>');
                    //clear all fields
                    $('#contact-form').trigger("reset");
                },
            })
        },
        filter: function() {
            return $(this).is(":visible");
        },
    });

    $("a[data-toggle=\"tab\"]").click(function(e) {
        e.preventDefault();
        $(this).tab("show");
    });










//Scroll_backtoTop

$(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
        $('.scroll_top').fadeIn();
    } else {
        $('.scroll_top').fadeOut();
    }
});
$('.scroll_top').on('click', function() {
    $("html, body").animate({
        scrollTop: 0
    }, 1000);
    return false;
});
$(window).scroll(function() {
    var scroll = $(window).scrollTop();
    if (scroll > 500) {
        $(".intro").hide();
        $(".scroll_down").hide();
    }
});

  


/////////slide-upgrade-account
$('.slide-upgrade').owlCarousel({
    loop: true,
    margin: 10,
    responsiveClass: true,
    nav: false,
    dots: true,
    smartSpeed: 1500,
    autoplay: true,
    // animateOut: 'fadeOut wow fadeInUp',
    // animateIn: 'fadeIn wow zoomIn ',
    autoplayTimeout: 4000,
    navClass: ["slide-prev", "slide-next"],
    navText: ["<i class='arrow_carrot-left'></i>", "<i class='arrow_carrot-right'></i>"],
    responsive: {
        1199: {
            items: 4,
        },
        991: {
            items: 3,
        },
        768: {
            items: 2,
        },
        479: {
            items: 1,
        },
        0: {
            items: 1,
        }
    }
});




/////////slide-upgrade-account
$('.list_videos_slide').owlCarousel({
    loop: true,
    margin: 10,
    responsiveClass: true,
    nav: false,
    dots: true,
    smartSpeed: 1500,
    // autoplay: true,
    autoplayTimeout: 4000,
    navClass: ["slide-prev", "slide-next"],
    navText: ["<i class='arrow_carrot-left'></i>", "<i class='arrow_carrot-right'></i>"],
    responsive: {
        1199: {
            items: 4,
        },
        991: {
            items: 3,
        },
        768: {
            items: 2,
        },
        479: {
            items: 2,
        },
        0: {
            items: 1,
        }
    }
});


});


/*When clicking on Full hide fail/success boxes */
$('#name').focus(function() {
    $('#success').html('');
});


$('.slide-img').owlCarousel({
    loop: true,
    margin: 0,
    responsiveClass: true,
    nav: false,
    dots: true,
    smartSpeed: 1500,
    autoplay: true,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    autoplayTimeout: 2500,
    navClass: ["slide-prev", "slide-next"],
    navText: ["<i class='arrow_carrot-left'></i>", "<i class='arrow_carrot-right'></i>"],
    responsive: {
        1199: {
            items: 1,
        },
        991: {
            items: 1,
        },
        768: {
            items: 1,
        },
        479: {
            items: 1,
        },
        0: {
            items: 1,
        }
    }
});

$('.slide-video-1').owlCarousel({
    loop: true,
    margin: 0,
    responsiveClass: true,
    nav: false,
    dots: true,
    smartSpeed: 1500,
    autoplay: true,
    // animateOut: 'fadeOut',
    // animateIn: 'fadeIn',
    autoplayTimeout: 2500,
    navClass: ["slide-prev", "slide-next"],
    navText: ["<i class='arrow_carrot-left'></i>", "<i class='arrow_carrot-right'></i>"],
    responsive: {
        1199: {
            items: 1,
        },
        991: {
            items: 1,
        },
        768: {
            items: 1,
        },
        479: {
            items: 1,
        },
        0: {
            items: 1,
        }
    }
});

$('.slide-banner-blogList').owlCarousel({
    loop: true,
    margin: 0,
    responsiveClass: true,
    nav: false,
    dots: true,
    smartSpeed: 1500,
    autoplay: true,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    autoplayTimeout: 4000,
    navClass: ["slide-prev", "slide-next"],
    navText: ["<i class='arrow_carrot-left'></i>", "<i class='arrow_carrot-right'></i>"],
    responsive: {
        1199: {
            items: 1,
        },
        991: {
            items: 1,
        },
        768: {
            items: 1,
        },
        479: {
            items: 1,
        },
        0: {
            items: 1,
        }
    }
});


$('.slide-campBox_7').owlCarousel({
    loop: true,
    margin: 0,
    responsiveClass: true,
    nav: false,
    dots: true,
    smartSpeed: 1500,
    autoplay: true,
    // animateOut: 'fadeInRight',
    // animateIn: 'fadeInRight',
    autoplayTimeout: 4000,
    navClass: ["slide-prev", "slide-next"],
    navText: ["<i class='arrow_carrot-left'></i>", "<i class='arrow_carrot-right'></i>"],
    responsive: {
        1199: {
            items: 1,
        },
        991: {
            items: 1,
        },
        768: {
            items: 1,
        },
        479: {
            items: 1,
        },
        0: {
            items: 1,
        }
    }
});


$('.slide_camp_box3').owlCarousel({
    loop: true,
    margin: 0,
    responsiveClass: true,
    nav: false,
    dots: true,
    smartSpeed: 1500,
    autoplay: true,
    // animateOut: 'fadeInRight',
    // animateIn: 'fadeInRight',
    autoplayTimeout: 4000,
    navClass: ["slide-prev", "slide-next"],
    navText: ["<i class='arrow_carrot-left'></i>", "<i class='arrow_carrot-right'></i>"],
    responsive: {
        1199: {
            items: 1,
        },
        991: {
            items: 1,
        },
        768: {
            items: 1,
        },
        479: {
            items: 1,
        },
        0: {
            items: 1,
        }
    }
});

$(document).ready(function(){
   
    //Show carousel-control
    
    $("#myCarousel").mouseover(function(){
      $("#myCarousel .carousel-control").show();
    });

    $("#myCarousel").mouseleave(function(){
      $("#myCarousel .carousel-control").hide();
    });
    
    //Active thumbnail
    
    $("#thumbCarousel .thumb").on("click", function(){
      $(this).addClass("active");
      $(this).siblings().removeClass("active");
    
    });
    
    //When the carousel slides, auto update
    
    $('#myCarousel').on('slid.bs.carousel', function(){
       var index = $('.carousel-inner .item.active').index();
       //console.log(index);
       var thumbnailActive = $('#thumbCarousel .thumb[data-slide-to="'+index+'"]');
       thumbnailActive.addClass('active');
       $(thumbnailActive).siblings().removeClass("active");
       //console.log($(thumbnailActive).siblings()); 
    });
   });


// const video = document.querySelector('.video-src');
// const canvases = [...document.querySelectorAll('.video-frame > canvas')];

// video.addEventListener('play', () => {

//     canvases.forEach(item => {
//         const context = item.getContext('2d');

//         function loop() {
//             context.drawImage(video, 0, 0, item.width, item.height);
//             requestAnimationFrame(loop);
//         }
//         requestAnimationFrame(loop);
//     });
// });


// video.addEventListener('error', videoError, true);

// function videoError() {
//     const image = new Image();
//     image.src = video.poster;

//     canvases.forEach(item => {
//         const context = item.getContext('2d');
//         item.image = image;

//         function loop() {
//             context.drawImage(item.image, 0, 0, item.width, item.height);
//             requestAnimationFrame(loop);
//         }
//         requestAnimationFrame(loop);
//     });
// }




// Scroll to TOP

$(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
        $('.scroll_top').fadeIn();
    } else {
        $('.scroll_top').fadeOut();
    }
});
$('.scroll_top').on('click', function() {
    $("html, body").animate({
        scrollTop: 0
    }, 1000);
    return false;
});
$(window).scroll(function() {
    var scroll = $(window).scrollTop();
    if (scroll > 500) {
        $(".intro").hide();
        $(".scroll_down").hide();
    }
});




// Popup/Click-messenger

$(document).ready(function ()
{
//Fade in delay for the background overlay (control timing here)
$("#bkgOverlay").delay(3000).fadeIn(400);
//Fade in delay for the popup (control timing here)
$("#delayedPopup").delay(3000).fadeIn(400);

//Hide dialouge and background when the user clicks the close button
$("#btnClose").click(function (e)
{
    HideDialog();
    e.preventDefault();
});
});
//Controls how the modal popup is closed with the close button
function HideDialog()
{
    $("#bkgOverlay").fadeOut(400);
    $("#delayedPopup").fadeOut(300);
}





// Click_language
function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}
window.onclick = function(event) {
    if (!event.target.matches('.dropbtn')) {
        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
        }
    }
}







////////////Canvas
$(document).on("click", ".naccs .menu div", function() {
    var numberIndex = $(this).index();

    if (!$(this).is("active")) {
        $(".naccs .menu div").removeClass("active");
        $(".naccs ul li").removeClass("active");

        $(this).addClass("active");
        $(".naccs ul").find("li:eq(" + numberIndex + ")").addClass("active");

        var listItemHeight = $(".naccs ul")
            .find("li:eq(" + numberIndex + ")")
            .innerHeight();
        $(".naccs ul").height(listItemHeight + "px");
    }
})



$('#tab-content div').hide();
$('#tab-content div:first').show();

$('#nav li').click(function() {
    $('#nav li a').removeClass("active");
    $(this).find('a').addClass("active");
    $('#tab-content div').hide();

    var indexer = $(this).index(); //gets the current index of (this) which is #nav li
    $('#tab-content div:eq(' + indexer + ')').fadeIn(); //uses whatever index the link has to open the corresponding box 
});