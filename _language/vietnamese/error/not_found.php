<?php
// Heading
$_['heading_title'] = 'Trang bạn yêu cầu không tồn tại!';

// Text
$_['text_error']    = 'Địa chỉ bạn đang truy cập đã không còn tồn tại hoặc được nhập không chính xác.<br/> Vui lòng kiểm tra lại thông tin hoặc sử dụng chức năng tìm kiếm của website';
?>