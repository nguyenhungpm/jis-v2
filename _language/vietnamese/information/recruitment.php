<?php
// Text
$_['heading_title'] = 'Tuyển dụng';
$_['text_location'] = 'Nơi làm việc';
$_['text_position'] = 'Vị trí cần tuyển';
$_['text_expired'] 	= 'Hạn tuyển';
$_['text_salary'] 	= 'Đãi ngộ';
$_['text_other'] 	= 'Tin tuyển dụng khác';
$_['text_apply'] 	= 'Ứng tuyển';
$_['text_empty'] 	= 'Chưa có công việc nào được đưa lên';
$_['entry_apply_title'] 	= 'Nộp hồ sơ ứng tuyển';
$_['entry_apply_desc'] 	= 'Vui lòng khai báo thông tin ứng viên';
$_['entry_name'] 	= 'Tên ứng viên';
$_['entry_email'] 	= 'Thư điện tử';
$_['entry_phone'] 	= 'Điện thoại';
$_['text_industry']	= 'Ngành nghề';
$_['text_added']	= 'Ngày đăng';
$_['entry_intro'] 	= 'Đôi lời giới thiệu về bản thân';
$_['entry_file'] 	= 'CV ứng viên';
$_['entry_file_allow']= 'Chấp nhận loại file: zip, doc, docx, pdf. Tối đa 5Mb';
$_['button_submit'] = 'Gửi đi';
$_['button_close']  = 'Đóng lại';
$_['button_upload'] = 'Upload CV của bạn';

$_['error_phone']	= 'Số điện thoại phải lớn hơn 6 và nhỏ hơn 14 ký tự';
$_['error_name']	= 'Tên của bạn phải lớn hơn 2 và nhỏ hơn 64 ký tự';
$_['error_email']	= 'Sai định dạng email';
$_['text_success']	= 'Thông tin ứng tuyển của bạn đã được gửi thành công tới chúng tôi';

?>
