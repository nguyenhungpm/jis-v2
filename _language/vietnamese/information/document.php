<?php
// Text
$_['heading_title'] = 'Thư viện tài liệu';
$_['text_location'] = 'Cơ quan ban hành';
$_['text_position'] = 'Tên tài liệu';
$_['text_reccat'] 	= 'Thư viện tài liệu';
$_['text_filtername'] = 'Tìm kiếm nhanh...';
$_['text_alldocs'] 	= 'Tất cả';
$_['text_readmore'] = 'Xem chi tiết';
$_['text_apply'] 	  = 'Ứng tuyển';
$_['text_empty'] 	  = 'Không có bản tin nào';
$_['text_added']	  = 'Ngày tải lên';

$_['entry_phone']	  = 'Điện thoại';
$_['entry_name']	  = 'Tên của bạn';
$_['entry_email']	  = 'Email';
$_['entry_title']	  = 'Chức danh';
$_['entry_office']	  = 'Doanh nghiệp/ Cơ quan';
$_['button_submit']	  = 'Đăng ký';
$_['text_inquiry']	  = 'Đăng ký nhận tài liệu';
$_['entry_inquiry_desc']	  = 'Quý khách vui lòng điền chính xác thông tin để nhận bộ tài liệu qua email.';

$_['text_others']	  = 'Cùng chủ đề';
$_['text_request']	= 'Đăng ký nhận tài liệu';
$_['text_linkdown']	= 'Tải về tài liệu';
$_['text_download']	= 'lượt tải';
$_['text_has']	= 'Đã có';

$_['error_phone']	= 'Số điện thoại phải lớn hơn 10 và nhỏ hơn 25 ký tự';
$_['error_name']	= 'Tên của bạn phải lớn hơn 2 và nhỏ hơn 64 ký tự';
$_['error_email']	= 'Sai định dạng email';
$_['text_success']	= 'Bạn đã đăng ký nhận thành công!';

?>
