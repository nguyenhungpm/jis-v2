<?php
// Heading
$_['heading_title']  	= 'Đặt hàng thành công';

// Text 
$_['text_home']  		= 'Trang chủ';
$_['button_continue']  		= 'Tiếp tục';
$_['text_message']  		= 'Bạn đã đặt hàng thành công. Chúng tôi sẽ liên hệ với bạn trong thời gian sớm nhất. Xin cảm ơn!';
?>