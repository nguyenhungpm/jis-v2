<?php
// Heading
$_['heading_title'] = 'Liên hệ';
$_['text_intro'] = 'Các chuyên gia của chúng tôi sẵn sàng trả lời bất kỳ câu hỏi nào về nghiệp vụ và sản phẩm';
$_['error_email'] = 'Địa chỉ email không hợp lệ';
$_['error_tel'] = 'Vui lòng nhập lại số điện thoại';
$_['error_name'] = 'Vui lòng nhập lại tên';
$_['error_captcha'] = 'Chưa nhập mã captcha hoặc nhập sai';
$_['text_submit'] = 'Gửi';
$_['text_name'] = 'Tên của bạn';
$_['text_tel'] = 'Điện thoại';
$_['text_email'] = 'Email';
$_['text_content'] = 'Nội dung';
$_['text_captcha'] = 'Mã xác nhận';
$_['text_success'] = 'Thông tin liên hệ đã được gửi đi. Chúng tôi sẽ phản hồi bạn trong thời gian sớm nhất';

// Text
$_['text_comments']  = 'Dựa trên %s đánh giá.';
?>