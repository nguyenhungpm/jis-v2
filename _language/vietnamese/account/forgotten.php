<?php
// Heading 
$_['heading_title']  = 'Quên mật khẩu';

// Text
$_['text_message']   = 'Mật khẩu mới được tạo ngẫu nhiên đã được gửi qua email. Vui lòng kiểm tra email!';
$_['text_account']   = 'Account';
$_['text_password']  = 'Your Password';
$_['text_success']   = 'Success: Your password has been successfully updated.';

// Entry
$_['entry_password'] = 'Password:';
$_['entry_confirm']  = 'Password Confirm:';

// Error
$_['error_password'] = 'Mật khẩu từ 4 đến 20 ký tự!';
$_['error_old_password'] = 'Không đúng mật khẩu hiện tại!';
$_['error_confirm']  = 'Mật khẩu không khớp!';
$_['error_email']  = 'Email không đúng!';
?>