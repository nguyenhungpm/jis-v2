<?php
// Text
$_['heading_title'] = 'Documents';
$_['text_position'] = 'Document name';
$_['text_reccat'] 	= 'Document category';
$_['text_filtername'] = 'Search...';
$_['text_alldocs'] 	= 'Document library';
$_['text_apply'] 	= 'Get Download Link';
$_['text_readmore'] 	= 'View details';
$_['text_empty'] 	= 'No document.';
$_['text_added']	= 'Date added';

$_['entry_phone']	  = 'Phone';
$_['entry_name']	  = 'Your name';
$_['entry_email']	  = 'Email';
$_['entry_title']	  = 'Your Title';
$_['entry_office']	  = 'Company Name';
$_['button_submit']	  = 'Submit';
$_['text_inquiry']	  = 'Request document link';
$_['entry_inquiry_desc']	  = 'Please fill your information before we can send document into your email.';

$_['text_others']	  = 'In the topic';
$_['text_request']	= 'Request link download';
$_['text_linkdown']	= 'Download document';
$_['text_download']	= 'lượt tải';
$_['text_has']	= 'Đã có';

$_['error_phone']	= 'Invalid phone number';
$_['error_name']	= 'Your name is required';
$_['error_email']	= 'Invalid email';
$_['text_success']	= 'Your email was registerd successfully!';

?>
