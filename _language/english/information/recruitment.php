<?php
// Text
$_['heading_title'] = 'Jobs';
$_['text_location'] = 'Location';
$_['text_position'] = 'Position';
$_['text_expired'] 	= 'Expired date';
$_['text_salary'] 	= 'Benefits';
$_['text_readmore'] = 'Read more';
$_['text_apply'] 	= 'Apply now';
$_['text_other'] 	= 'Other Jobs';
$_['text_empty'] 	= 'No new jobs has posted';
$_['entry_apply_title'] 	= 'Apply this job';
$_['entry_apply_desc'] 	= 'Please fill your information';
$_['entry_name'] 	= 'Your name';
$_['text_industry']= 'Industry';
$_['text_added'] = 'Date added';
$_['entry_email'] 	= 'Email';
$_['entry_phone'] 	= 'Phone';
$_['entry_intro'] 	= 'Introduce something about youself';
$_['entry_file'] 	= 'CV';
$_['entry_file_allow']= 'Allow file types: zip, doc, docx, pdf. Max. 5Mb';
$_['button_submit'] = 'Submit';
$_['button_close']  = 'Close';
$_['button_upload']	= 'Choose your CV';
$_['error_phone']	= 'Phone number must be between 6 and 14 characters';
$_['error_name']	= 'Name must be between 6 and 64 characters';
$_['error_email']	= 'Invalid email';
$_['text_success']	= 'Your information has been applied successfully';

?>