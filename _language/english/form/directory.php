<?php
// Heading
$_['heading_title']      = 'Directory';
$_['text_name']      = 'Company name';
$_['text_address']      = 'Addess';
$_['text_telephone']      = 'Telephone';
$_['text_tax']      = 'Tax';
$_['text_fax']      = 'Fax';
$_['text_year']      = 'Establish';
$_['text_zone']      = 'Zone';
$_['text_type']      = 'Type of bussiness';
$_['text_size']      = 'Size of bussiness';
$_['text_person']      = 'Staff';
$_['text_product']      = 'Product';
$_['text_certificat_standard']      = 'Certificate standard';
$_['text_certificat_origin']      = 'Certificate Origin';
$_['text_person_name']      = 'Name';
$_['text_person_staff']      = 'Position';
$_['text_person_telephone']      = 'Telephone';
$_['text_person_email']      = 'Email';
$_['text_catalog']      = 'Sector';
$_['text_association']      = 'Association';
$_['text_capacity']      = 'Capacity';
$_['text_export_market']      = 'Export market';
$_['text_list_product']      = 'Related product';
$_['text_list_news']      = 'Related article';

?>