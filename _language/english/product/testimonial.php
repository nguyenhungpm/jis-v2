<?php
// Heading
$_['heading_title'] 	= 'Khách hàng đánh giá';

$_['text_average']      = 'Bầu chọn trung bình:';
$_['text_stars']        = '%s out of 5 Stars!';
$_['text_no_rating']    = 'Không xếp hạng';

// Text
$_['text_error'] 		= 'Testimonial: No testimonials found!';
$_['text_pagination'] 	= 'Hiển thị từ {start}  đến {total} ({pages} page)';
$_['text_showall'] 	= 'Show all';
$_['text_write'] 		= 'Write testimonial';
$_['text_auteur'] 	= ' ( Author: %s )';
$_['text_city'] 		= '<i> from %s</i>';
?>