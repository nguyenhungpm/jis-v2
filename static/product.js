$(document).ready(function() {
	$('.jqzoom').jqzoom({
		zoomType: 'innerzoom',
		preloadImages: false,
		alwaysOn:false,
		title: false
	});
});
$('.thumb-list').owlCarousel({
	items: 5,
	itemsDesktop: [1000, 4], 
    itemsDesktopSmall: [900, 4], 
    itemsTablet: [600, 4], 
    itemsMobile: [480, 4],
	autoPlay: 3000,
	navigation: true,
	navigationText: ['<i class=\"fa fa-chevron-left fa-5x\"></i>', '<i class=\"fa fa-chevron-right fa-5x\"></i>'],
	pagination: true
});