! function a(i, s, u) {
    function f(n, e) {
        if (!s[n]) {
            if (!i[n]) {
                var r = "function" == typeof require && require;
                if (!e && r) return r(n, !0);
                if (d) return d(n, !0);
                var t = new Error("Cannot find module '" + n + "'");
                throw t.code = "MODULE_NOT_FOUND", t
            }
            var o = s[n] = {
                exports: {}
            };
            i[n][0].call(o.exports, function(e) {
                return f(i[n][1][e] || e)
            }, o, o.exports, a, i, s, u)
        }
        return s[n].exports
    }
    for (var d = "function" == typeof require && require, e = 0; e < u.length; e++) f(u[e]);
    return f
}({
    "/home/gettransfer/gettransfer/releases/20180813151937/assets/scripts/app/pageLoadReporter.js": [function(e, n, r) {
        "use strict";
        var t = Date.now(),
            o = !1,
            a = function() {
                var e = 0 < arguments.length && void 0 !== arguments[0] && arguments[0];
                if (!o && "Raven" in window) {
                    o = !0;
                    var n = Date.now() - t,
                        r = Math.floor(n / 1e3);
                    if (!(r < 10)) {
                        Raven.captureMessage("Loading document took more than 10 seconds.", {
                            level: "info",
                            logger: "performance",
                            extra: {
                                seconds_passed: r,
                                left_the_page: e
                            }
                        })
                    }
                }
            };
        window.queue.afterAppLoaded(function() {
            "$" in window ? $(a.bind(null, !1)) : document.addEventListener("DOMContentLoaded", a.bind(null, !1))
        }), window.onunload = a.bind(null, !0)
    }, {}]
}, {}, ["/home/gettransfer/gettransfer/releases/20180813151937/assets/scripts/app/pageLoadReporter.js"]);
//# sourceMappingURL=maps/pageLoadReporter.js.map